#
# iparapheur Core
# Copyright (C) 2018-2025 Libriciel-SCOP
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-only
#

import yaml

with open("db.changelog-temp.yaml") as f:
    changelog = yaml.safe_load(f)

changeset_list = changelog["databaseChangeLog"]
changes_list = []

for changeset in changeset_list:
    target_change_obj = changeset['changeSet']['changes']
    changes_list.append(target_change_obj)

newChangeSet = {'changeSet': {'author': 'Libriciel-SCOP', 'changes': changes_list}}
changelog['databaseChangeLog'] = newChangeSet

with open("db.changelog-crunched.yaml", "w") as f:
    yaml.dump(changelog, f)
