# iparapheur Core

[![pipeline status](https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/badges/develop/pipeline.svg)](https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/commits/develop) [![coverage report](https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/badges/develop/coverage.svg)](https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/commits/develop)

Cœur applicatif du iparapheur.  
Sert de lien entre les différentes briques, et intègre le code purement métier.

## Prérequis

Les variables d'environnement suivantes sont nécessaires au démarrage de l'application :

```conf
APPLICATION_PROTOCOL=http
APPLICATION_HOST=iparapheur.dom.local

CONTENT_HOST=alfresco
CONTENT_PORT=8080

POSTGRES_HOST=postgres
POSTGRES_PORT=5432
POSTGRES_USER=root
POSTGRES_PASSWORD=libriciel2k18

KEYCLOAK_HOST=keycloak
KEYCLOAK_PORT=8080
KEYCLOAK_REALM=api
KEYCLOAK_CLIENT_ID=ipcore-api
KEYCLOAK_WEB_CLIENT_ID=ipcore-web

KEYCLOAK_DB_DATABASE=keycloak
KEYCLOAK_DB_USER=keycloak
KEYCLOAK_DB_PASSWORD=keycloak

KEYCLOAK_CLIENT_SECRET=<token>

SECRET_PROTOCOL=http
SECRET_HOST=vault
SECRET_PORT=8200
VAULT_UNSEAL_KEY=<unseal_key>
VAULT_TOKEN=<token>

MATOMO_TOKEN=<token>
```

Les valeurs fournies ici à titre d'exemple sont valable dans le contexte classique d'un parapheur ayant tous ses services lancés par docker-compose.

Pour utiliser ip-core en local, il faut renseigner l'adresse et le port réels des services (généralement 'localhost' pour tous les host), et utiliser le profile spring adéquat (`dev-local`) au lancement : 
`java -Dspring.profiles.active=dev-local -jar ip-core.jar` 

## Swagger

Lancer l'application, Swagger sera disponible ici :  `http://iparapheur.dom.local/api/swagger-ui.html`  
*Attention, si vous passez par `localhost` ou `127.0.0.1`, le login sera impossible.*

## Packaging

*Pré-requis :*

* Java 17
* Maven 3
* Gradle 7

Lancer la commande suivante à la racine du projet :

```bash
gradle bootJar
```

Le jar packagé sera disponible dans `build/libs/ip-core-VERSION.jar`.



## Docker

Une image docker est construite à chaque commit. Cette image ne possède aucune dépendance.

Il suffira de lancer l'image docker du projet `ip-core` de la façon suivante :

```bash
docker run --name ip-core -p 8080:8080 -d gitlab.libriciel.fr:4567/i-parapheur/components/ip-core
```

L'application sera ensuite accessible sur l'adresse [http://localhost:8080/](http://localhost:8080/).

## Ipng

### Base configuration
Comme tous les services externes, le service IPNG a besoin d'avoir
ses paramètres de base renseignés dans les propriétés de l'app.   
Cependant, en plus des champs `provider`, `host` et `port`, IPNG a également besoin de la propriété
`bindings-provider`, dont la valeur peut être `static` ( => fichier de conf) ou `db` (=> en BDD). 
Par défaut, le mode "db" est utilisé.

```yaml
services:
  ipng:
    provider: ipng
    host: localhost
    port: 8081
    bindings-provider: db
```

### Explicit Mapping
Le mapping entre un tenant IP et une entité IPNG doit pour le moment être défini explicitement
dans le fichier de properties, y compris avec des binding en mode BDD.  
exemple de conf minimale à ajouter à application.yml :  
```yaml
ipng:
  tenantToEntities:
    # ID du tenant IP                     #ID de l'entité IPNG
    1c6996ed-3cee-4ea3-bb77-afbc0f22bc44: montpellierEntityId
    2dea654b-beee-4745-bb21-5813e25bda1f: libricielEntityId
```

Dans le cas d'un binding en mode "static",
tous les mapping doivent être défini en fichier de propriété, détaillés pour chaque tenant :  

```yaml
ipng:
  tenantToEntities:
    60c08ad6-f4d5-4e55-ac28-adc6c262d5a8: conseilregionadmin141
    441f9d50-8f24-4c07-b160-c92033922332: conseilregionadmin241
  tenantConfigurations:
    441f9d50-8f24-4c07-b160-c92033922332:
      deskToDeskbox:
        aecfbd42-19d7-4193-88f5-b034832bddcb: deskboxcr41
      innerTypesToOutgoingIpngTypes:
        2c3c5f63-0dee-4301-bedc-83f3d92d4721: 100-common-contract

    60c08ad6-f4d5-4e55-ac28-adc6c262d5a8:
      deskToDeskbox:
        7ee3e753-3343-4b5c-8539-e50b54c759f1: deskbox141
      incomingIpngTypesToInnerTypes:
        100-common-contract: f5fbfc35-8623-4088-8d31-24dfe61a37ca
```

## Stats

#### Custom controller

Par défaut, les statistiques sont agrégées par un moteur Matomo interne.  
Il est possible de lier un service externe, pour le "nourrir" des actions internes du iparapheur.

Exemple :

```yaml
services:
  stats:
    provider: custom
    scheme: http
    baseUrl: matomo.macollectivite.fr
    port: 80
    httpMethod: get
    adminAction:
      path: "matomo.php"
      params:
        - apiv1: "1"
        - rec: "1"
        - idsite: "@{i_Parapheur_internal_tenant_id}"
        - e_c: "@{i_Parapheur_internal_category}"
        - e_a: "@{i_Parapheur_internal_action_name}"
        - e_v: "@{i_Parapheur_internal_target}"
        - rand: "@{i_Parapheur_internal_random_number}"
    folderAction:
      path: "matomo.php"
      params:
        - apiv1: "1"
        - rec: "1"
        - idsite: "@{i_Parapheur_internal_tenant_id}"
        - e_c: "@{i_Parapheur_internal_folder_name}"
        - e_a: "@{i_Parapheur_internal_action_name}"
        - e_v: "@{i_Parapheur_internal_time_to_complete_in_hours}"
        - rand: "@{i_Parapheur_internal_random_number}"
    valuesSubstitutions:
    # (...)
```

#### Les valeurs remplacées automatiquement

Les valeurs suivantes seront automatiquement remplacées, pour chaque `adminAction.params`.  
Les évènements capturés ici concerneront les changements de paramétrage du iparapheur  
(l'édition d'un Bureau, la suppression d'un utilisateur...).

* `i_Parapheur_internal_category`, par une valeur de la forme `Entité`, `Bureau`, `Dossier`...
* `i_Parapheur_internal_action_name`, par une valeur de la forme `Création`, `Suppression`, `Visa`, `Signature`...
* `i_Parapheur_internal_target`, par l'Id de la ressource ciblée par l'action
* `i_Parapheur_internal_tenant_id`
* `i_Parapheur_internal_tenant_name`
* `i_Parapheur_internal_random_number`, un nombre entier au hasard, entre 0 et 1000000

Le `folderAction.params` disposera, en plus des précédents, des remplacements suivants.  
Les évènements capturés concerneront ici les actions propres à un dossier.

* `i_Parapheur_internal_time_to_complete_in_hours`, le temps en heure, du traitement de l'action.
* `i_Parapheur_internal_desk_id`
* `i_Parapheur_internal_desk_name`
* `i_Parapheur_internal_folder_id`
* `i_Parapheur_internal_folder_name`
* `i_Parapheur_internal_type_id`
* `i_Parapheur_internal_type_name`
* `i_Parapheur_internal_subtype_id`
* `i_Parapheur_internal_subtype_name`

#### Table de mapping

Il sera parfois nécessaire de gérer des ID externes.  
L'Id d'une entité peut-être `123e4567-e89b-12d3-a456-111111111111` dans le iparapheur, mais un simple `1` dans un service externe.

Le champ `valuesSubstitutions` permettra de "mapper" ces valeurs à la volée.  
Le même champ peut-être utilisé pour corriger une préférence de mot-clef :

```yaml
    # (...)
    valuesSubstitutions:
      123e4567-e89b-12d3-a456-111111111111: "1"
      123e4567-e89b-12d3-a456-222222222222: "2"
      Visa: "Validé"
```
