#
# iparapheur Core
# Copyright (C) 2018-2025 Libriciel-SCOP
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-only
#

FROM hubdocker.libriciel.fr/eclipse-temurin:17.0.13_11-jre-alpine@sha256:8164ba7f332faf27462e055013c2aa518a2eba1ce6984e9ca60dadb899cab6bd

HEALTHCHECK --interval=5s --timeout=2s --retries=60 \
  CMD wget --quiet --tries=1 --proxy off -O- http://localhost:8080/api/actuator/health | grep UP || exit 1

# Open Containers Initiative parameters
ARG CI_COMMIT_REF_NAME=""
ARG CI_COMMIT_SHA=""
ARG CI_PIPELINE_CREATED_AT=""
# Non-standard and/or deprecated variables, that are still widely used.
# If it is already set in the FROM image, it has to be overridden.
MAINTAINER Libriciel SCOP
LABEL maintainer="Libriciel SCOP"
LABEL org.label-schema.build-date="$CI_PIPELINE_CREATED_AT"
LABEL org.label-schema.name="ip-core"
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.vendor="Libriciel SCOP"
# Open Containers Initiative's image specifications
LABEL org.opencontainers.image.authors="Libriciel SCOP"
LABEL org.opencontainers.image.created="$CI_PIPELINE_CREATED_AT"
LABEL org.opencontainers.image.description="iparapheur main entrypoint, used to link every other elements"
LABEL org.opencontainers.image.licenses="GNU Affero GPL v3"
LABEL org.opencontainers.image.revision="$CI_COMMIT_SHA"
LABEL org.opencontainers.image.source="registry.libriciel.fr:443/public/signature/ip-core"
LABEL org.opencontainers.image.title="ip-core"
LABEL org.opencontainers.image.vendor="Libriciel SCOP"
LABEL org.opencontainers.image.version="$CI_COMMIT_REF_NAME"

# The Docker source image already contains tzdata timezones by default.
# From the Alpine doc, we just have to link and reference it : https://wiki.alpinelinux.org/wiki/Setting_the_timezone
# TODO: Set this logic in the docker-entrypoint, since the TZ will be asked to be set through a .env, AFTER the image build.
ENV TZ="Europe/Paris"
RUN cp /usr/share/zoneinfo/$TZ /etc/localtime
RUN echo $TZ > /etc/timezone

# The Docker source image already contains musl-locales by default.
ENV LANG="fr_FR.UTF-8"
ENV LANGUAGE="fr_FR:en_US:en"
ENV LC_ALL="fr_FR.UTF-8"

ENV JAVA_OPTS=""

VOLUME /tmp

ADD build/libs/ip-core-*.jar core.jar

COPY docker-entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/docker-entrypoint.sh
RUN ln -s /usr/local/bin/docker-entrypoint.sh /

ENTRYPOINT [ "docker-entrypoint.sh" ]
