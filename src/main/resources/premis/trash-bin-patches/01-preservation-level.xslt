<?xml version="1.0" encoding="UTF-8"?>

<!--
  ~ iparapheur Core
  ~ Copyright (C) 2018-2025 Libriciel-SCOP
  ~
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as
  ~ published by the Free Software Foundation, version 3.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~ GNU Affero General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program.  If not, see <https://www.gnu.org/licenses/>.
  ~
  ~ SPDX-License-Identifier: AGPL-3.0-only
  -->

<xsl:stylesheet
        version="1.0"
        xmlns:premis="http://www.loc.gov/premis/v3"
        xsi:schemaLocation="http://www.loc.gov/premis/v3 http://www.loc.gov/standards/premis/v3/premis.xsd"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

    <xsl:output indent="yes" encoding="UTF-8"/>
    <xsl:strip-space elements="*"/>

    <!-- Copy the entire XML -->

    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
    </xsl:template>


    <!-- callActivity content -->

    <!-- Transformation spécifique pour le nœud 'object' -->

    <!-- Transformation spécifique pour le nœud 'object' -->
    <xsl:template match="premis:object[@xsi:type='file']">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>

            <xsl:if test="not(premis:significantProperties)">
                <xsl:choose>
                    <xsl:when test="not(premis:preservationLevel/preservationLevelValue = 'mainDocument') and premis:preservationLevel">
                        <significantProperties>
                            <significantPropertiesType>i_Parapheur_reserved_mainDocument</significantPropertiesType>
                            <significantPropertiesValue>false</significantPropertiesValue>
                        </significantProperties>
                    </xsl:when>
                    <xsl:otherwise>
                        <significantProperties>
                            <significantPropertiesType>i_Parapheur_reserved_mainDocument</significantPropertiesType>
                            <significantPropertiesValue>true</significantPropertiesValue>
                        </significantProperties>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:if>

        </xsl:copy>
    </xsl:template>

    <!-- Removes the previously used field -->
    <xsl:template match="premis:preservationLevel"/>

</xsl:stylesheet>
