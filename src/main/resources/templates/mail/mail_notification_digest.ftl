<!DOCTYPE html>

<!--
  iparapheur
  Copyright (C) 2018-2025 Libriciel. All rights reserved.

  This work is licensed under the terms of the MIT license.
  For a copy, see <https://opensource.org/licenses/MIT>.

  SPDX-License-Identifier: MIT
-->

<#setting datetime_format="short">

<#-- @ftlvariable name="i_Parapheur_internal_task_list" type="java.util.List<coop.libriciel.ipcore.model.mail.MailContent>" -->
<#-- @ftlvariable name="i_Parapheur_internal_main_url" type="java.lang.String" -->
<#-- @ftlvariable name="i_Parapheur_internal_footer" type="java.lang.String" -->

<html lang="fr">

<head>
    <title>e-mail r&eacute;capitulatif</title>
    <style>

        body {
            margin: 0;
            display: block;
            position: relative;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-size: 14px;
            line-height: 1.42857143;
            color: #333;
            background-color: #fff;
            word-wrap: break-word;
        }

        h1, h2, h3 {
            margin-top: 20px;
            margin-bottom: 10px;
            font-family: inherit;
            font-weight: 500;
            line-height: 1.1;
            color: inherit;
        }

        h1 {
            font-size: 20px;
        }

        h2 {
            font-size: 18px;
        }

        h1 small, h2 small, h3 small {
            font-size: 65%;
            font-weight: 400;
            line-height: 1;
            color: #999;
        }

        div {
            display: block;
        }

        p {
            margin: 0 0 10px;
        }

        a {
            text-decoration: none;
            color: black;
        }

        .container {
            width: 920px;
            margin-right: auto;
            margin-left: auto;
            padding-left: 15px;
            padding-right: 15px;
        }

        .bg-primary {
            background-color: #428bca;
        }

        .table {
            width: 100%;
            margin-bottom: 20px;
            border-collapse: collapse;
            border-spacing: 0;
            display: table;
        }

        .table-bordered {
            border: 1px solid #ddd;
        }

        .table-striped > tbody > tr:nth-child(odd) > td, .table-striped > tbody > tr:nth-child(odd) > th {
            background-color: #f9f9f9;
        }

    </style>
</head>

<body>

<div class="container">

    <h1 class="bg-primary" style="color:white; padding-left:15px">
        iparapheur | e-mail r&eacute;capitulatif
    </h1>

    <h3 class="bg-primary" style="color:white; padding-left:15px">
        Dossiers en attente de traitement
    </h3>

    <table class="table table-bordered table-striped">

        <thead>
        <tr style="text-align:left;">
            <th>Nom</th>
            <th>Lien</th>
            <th>Date</th>
            <th>Bureau</th>
            <th>Action cible</th>
            <th>Annotation</th>
        </tr>
        </thead>

        <tbody>
        <#list i_Parapheur_internal_task_list as ip_task>
            <#if ip_task.notificationType?? && (ip_task.notificationType == "NEW_ON_DESK" || ip_task.notificationType == "LATE")>
            <#--            <#if ! i_Parapheur_internal_task.byDelegation  && ! i_Parapheur_internal_task.asFollower>-->
                <tr>
                    <td>${ip_task.folderName ! ""}</td>
                    <td>
                        <#if ip_task.tenantId?? && ip_task.deskId?? && ip_task.folderId??>
                            <#if ip_task.byDelegation>
                                <a href="${i_Parapheur_internal_main_url}/tenant/${ip_task.tenantId}/desk/${ip_task.deskId}/folder/${ip_task.folderId}%3FasDeskId%3D${ip_task.delegatingDeskId}"
                                   target="_blank">
                                    Acc&eacute;der au dossier
                                </a>
                            <#else>
                                <a href="${i_Parapheur_internal_main_url}/tenant/${ip_task.tenantId}/desk/${ip_task.deskId}/folder/${ip_task.folderId}%3FasDeskId%3D${ip_task.deskId}"
                                   target="_blank">
                                    Acc&eacute;der au dossier
                                </a>
                            </#if>
                        </#if>
                    </td>
                    <td>
                        <#if ip_task.date??>
                            ${ip_task.date?datetime ! ""}
                        </#if>
                    </td>
                    <td>
                        <#if ip_task.byDelegation>
                            ${ip_task.delegatingDeskName ! ""}
                        <#else>
                            ${ip_task.deskName ! ""}
                        </#if>
                    </td>
                    <td>${ip_task.taskActionString ! ""}</td>
                    <td>${ip_task.publicAnnotation ! ""}</td>
                </tr>
            </#if>
        </#list>
        </tbody>
    </table>


    <h3 class="bg-primary" style="color:white; padding-left:15px">
        Dossiers trait&eacute;s
    </h3>

    <table class="table table-bordered table-striped">

        <thead>
        <tr style="text-align:left;">
            <th>Nom</th>
            <th>Lien</th>
            <th>Date</th>
            <th>Acteur</th>
            <th>Action effectu&eacute;e</th>
            <th>Annotation</th>
        </tr>
        </thead>

        <tbody>
        <#list i_Parapheur_internal_task_list as ip_task>
            <#if ip_task.notificationType?? && ip_task.notificationType == "ACTION_PERFORMED">
            <#--            <#if ip_task.byDelegation  && ! ip_task.asFollower>-->
                <tr>
                    <td>${ip_task.folderName ! ""}</td>
                    <td>
                        <#if !ip_task.asFollower && (ip_task.tenantId?? && ip_task.deskId?? && ip_task.folderId??)>
                            <a href="${i_Parapheur_internal_main_url}/tenant/${ip_task.tenantId}/desk/${ip_task.deskId}/folder/${ip_task.folderId}%3FasDeskId%3D${ip_task.deskId}"
                               target="_blank">
                                Acc&eacute;der au dossier
                            </a>
                        </#if>
                    </td>
                    <td>
                        <#if ip_task.date??>
                            ${ip_task.date?datetime ! ""}
                        </#if>
                    </td>
                    <td>
                        <#if ip_task.deskName?? || ip_task.taskUserFirstName?? || ip_task.taskUserLastName??>
                            ${ip_task.deskName ! ""} ${ip_task.taskUserFirstName ! ""} ${ip_task.taskUserLastName ! ""}
                        </#if>
                    </td>
                    <td>${ip_task.taskActionPastString ! ""}</td>
                    <td>${ip_task.publicAnnotation ! ""}</td>
                </tr>
            </#if>
        </#list>
        </tbody>
    </table>

</div>

<p><small>${i_Parapheur_internal_footer}</small></p>

</body>

</html>
