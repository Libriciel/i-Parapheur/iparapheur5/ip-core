<!DOCTYPE html>

<!--
  iparapheur
  Copyright (C) 2018-2025 Libriciel. All rights reserved.

  This work is licensed under the terms of the MIT license.
  For a copy, see <https://opensource.org/licenses/MIT>.

  SPDX-License-Identifier: MIT
-->

<#-- @ftlvariable name="i_Parapheur_internal_task_list" type="java.util.List<coop.libriciel.ipcore.model.mail.MailContent>" -->
<#-- @ftlvariable name="i_Parapheur_internal_from_user_id" type="java.lang.String" -->
<#-- @ftlvariable name="i_Parapheur_internal_from_username" type="java.lang.String" -->
<#-- @ftlvariable name="i_Parapheur_internal_from_first_name" type="java.lang.String" -->
<#-- @ftlvariable name="i_Parapheur_internal_from_last_name" type="java.lang.String" -->
<#-- @ftlvariable name="i_Parapheur_internal_message" type="java.lang.String" -->
<#-- @ftlvariable name="i_Parapheur_internal_main_url" type="java.lang.String" -->
<#-- @ftlvariable name="i_Parapheur_internal_footer" type="java.lang.String" -->
<#assign i_Parapheur_internal_task = i_Parapheur_internal_task_list[0]>

<html lang="fr">
<head>

    <title>${i_Parapheur_internal_task.folderName}</title>

    <style>

        body {
            margin: 0;
            display: block;
            position: relative;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-size: 14px;
            line-height: 1.42857143;
            color: #333;
            background-color: #fff;
        }

        h1, h2, h3 {
            margin-top: 20px;
            margin-bottom: 10px;
            font-family: inherit;
            font-weight: 500;
            line-height: 1.1;
            color: inherit;
        }

        h1 {
            font-size: 20px;
        }

        h2 {
            font-size: 18px;
        }

        h1 small, h2 small, h3 small {
            font-size: 65%;
            font-weight: 400;
            line-height: 1;
            color: #999;
        }

        div {
            display: block;
        }

        p {
            margin: 0 0 10px;
        }

        .container {
            width: 620px;
            margin-right: auto;
            margin-left: auto;
            padding-left: 15px;
            padding-right: 15px;
        }

        .well {
            min-height: 20px;
            padding: 19px;
            margin-bottom: 20px;
            background-color: #f5f5f5;
            border: 1px solid #e3e3e3;
            border-radius: 4px;
        }

        .bg-primary {
            background-color: #428bca;
        }

        .lead {
            margin-bottom: 20px;
            font-size: 16px;
            font-weight: 200;
            line-height: 1.4;
        }

    </style>
</head>

<body>

<div class="container">

    <h1 class="bg-primary" style="color:white; padding-left:15px">
        iparapheur
    </h1>
    <div class="well">
        <h2>
            ${i_Parapheur_internal_task.folderName}
        </h2>

        <p class="lead">
            Le dossier '${i_Parapheur_internal_task.folderName}' vous a &eacute;t&eacute; envoy&eacute; par :
            ${i_Parapheur_internal_from_first_name} ${i_Parapheur_internal_from_last_name}.
        </p>
        <#if (i_Parapheur_internal_message??) && (i_Parapheur_internal_message != "") && (i_Parapheur_internal_message != " ")>
            <p>${i_Parapheur_internal_message}</p>
        </#if>

        <#if (i_Parapheur_internal_footer??) && (i_Parapheur_internal_footer != "")>
            <p>
                <small>${i_Parapheur_internal_footer}</small>
            </p>
        </#if>

    </div>

</div>

</body>
</html>
