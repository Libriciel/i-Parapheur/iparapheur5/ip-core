<!DOCTYPE html>

<!--
  iparapheur
  Copyright (C) 2018-2025 Libriciel. All rights reserved.

  This work is licensed under the terms of the MIT license.
  For a copy, see <https://opensource.org/licenses/MIT>.

  SPDX-License-Identifier: MIT
-->

<#setting datetime_format="short">

<#-- @ftlvariable name="i_Parapheur_internal_expired_seal_certificate_list" type="java.util.List<coop.libriciel.ipcore.model.crypto.SealCertificateRepresentation>" -->
<#-- @ftlvariable name="i_Parapheur_internal_tenant" type="coop.libriciel.ipcore.model.database.TenantRepresentation" -->
<#-- @ftlvariable name="i_Parapheur_internal_footer" type="java.lang.String" -->

<html lang="fr">

<head>
    <title>Alerte d'expiration de certificat de cachet serveur</title>
    <style>

        body {
            margin: 0;
            display: block;
            position: relative;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-size: 14px;
            line-height: 1.42857143;
            color: #333;
            background-color: #fff;
            word-wrap: break-word;
        }

        h1, h2, h3 {
            margin-top: 20px;
            margin-bottom: 10px;
            font-family: inherit;
            font-weight: 500;
            line-height: 1.1;
            color: inherit;
        }

        h1 {
            font-size: 20px;
        }

        h2 {
            font-size: 18px;
        }

        h1 small, h2 small, h3 small {
            font-size: 65%;
            font-weight: 400;
            line-height: 1;
            color: #999;
        }

        div {
            display: block;
        }

        p {
            margin: 0 0 10px;
        }

        a {
            text-decoration: none;
            color: black;
        }

        .container {
            width: 920px;
            margin-right: auto;
            margin-left: auto;
            padding-left: 15px;
            padding-right: 15px;
        }

        .bg-primary {
            background-color: #428bca;
        }

        .table {
            width: 100%;
            margin-bottom: 20px;
            border-collapse: collapse;
            border-spacing: 0;
            display: table;
        }

        .table-bordered {
            border: 1px solid #ddd;
        }

        .table-striped > tbody > tr:nth-child(odd) > td, .table-striped > tbody > tr:nth-child(odd) > th {
            background-color: #f9f9f9;
        }

    </style>
</head>

<body>

<div class="container">

    <h1 class="bg-primary" style="color:white; padding-left:15px">
        iparapheur | Alerte d'expiration de certificat de cachet serveur
    </h1>

    <table class="table table-bordered table-striped">
        <thead>
        <tr style="text-align:left;">
            <th>Entit&eacute;</th>
            <th>Certificat</th>
            <th>Date d'expiration</th>
        </tr>
        </thead>

        <tbody>
        <#list i_Parapheur_internal_expired_seal_certificate_list as certificate>
            <tr>
                <td>${i_Parapheur_internal_tenant.name}</td>
                <td>${certificate.name}</td>
                <td>${certificate.expirationDate?datetime ! ""}</td>
            </tr>
        </#list>
        </tbody>
    </table>

</div>

<p><small>${i_Parapheur_internal_footer}</small></p>

</body>

</html>
