<!DOCTYPE html>

<!--
  iparapheur
  Copyright (C) 2018-2025 Libriciel. All rights reserved.

  This work is licensed under the terms of the MIT license.
  For a copy, see <https://opensource.org/licenses/MIT>.

  SPDX-License-Identifier: MIT
-->

<#-- @ftlvariable name="i_Parapheur_internal_task_list" type="java.util.List<coop.libriciel.ipcore.model.mail.MailContent>" -->
<#-- @ftlvariable name="i_Parapheur_internal_from_user_id" type="java.lang.String" -->
<#-- @ftlvariable name="i_Parapheur_internal_from_username" type="java.lang.String" -->
<#-- @ftlvariable name="i_Parapheur_internal_from_first_name" type="java.lang.String" -->
<#-- @ftlvariable name="i_Parapheur_internal_from_last_name" type="java.lang.String" -->
<#-- @ftlvariable name="i_Parapheur_internal_main_url" type="java.lang.String" -->
<#-- @ftlvariable name="i_Parapheur_internal_footer" type="java.lang.String" -->
<#assign i_Parapheur_internal_task = i_Parapheur_internal_task_list[0]>

<html lang="fr">

<head>

    <title>${i_Parapheur_internal_task.folderName}</title>

    <style>

        body {
            margin: 0;
            display: block;
            position: relative;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-size: 14px;
            line-height: 1.42857143;
            color: #333;
            background-color: #fff;
            word-wrap: break-word;
        }

        h1, h2, h3 {
            margin-top: 20px;
            margin-bottom: 10px;
            font-family: inherit;
            font-weight: 500;
            line-height: 1.1;
            color: inherit;
        }

        h1 {
            font-size: 20px;
        }

        h2 {
            font-size: 18px;
        }

        h1 small, h2 small, h3 small {
            font-size: 65%;
            font-weight: 400;
            line-height: 1;
            color: #999;
        }

        div {
            display: block;
        }

        p {
            margin: 0 0 10px;
        }

        a {
            text-decoration: none;
            color: black;
        }

        .container {
            width: 620px;
            margin-right: auto;
            margin-left: auto;
            padding-left: 15px;
            padding-right: 15px;
        }

        .well {
            min-height: 20px;
            padding: 19px;
            margin-bottom: 20px;
            background-color: #f5f5f5;
            border: 1px solid #e3e3e3;
            border-radius: 4px;
        }

        .bg-primary {
            background-color: #428bca;
        }

        .lead {
            margin-bottom: 20px;
            font-size: 16px;
            font-weight: 200;
            line-height: 1.4;
        }

        .btn-success {
            color: #fff;
            background-color: #5cb85c;
            border-color: #4cae4c;
        }

        .btn {
            display: inline-block;
            margin-bottom: 0;
            font-weight: 400;
            text-align: center;
            vertical-align: middle;
            cursor: pointer;
            background-image: none;
            border: 1px solid transparent;
            white-space: nowrap;
            padding: 6px 12px;
            font-size: 16px;
            line-height: 1.42857143;
            border-radius: 4px;
            text-transform: none;
        }

        #btn {
            display: inline-block;
            margin-bottom: 0;
            font-weight: 400;
            text-align: center;
            vertical-align: middle;
            cursor: pointer;
            background-image: none;
            white-space: nowrap;
            padding: 6px 12px;
            font-size: 16px;
            line-height: 1.42857143;
            border-radius: 4px;
            text-transform: none;
            color: #fff;
            background-color: #5cb85c;
            border: 1px solid #4cae4c;
        }

    </style>

</head>

<body>

<div class="container">

    <h1 class="bg-primary" style="color:white; padding-left:15px">
        iparapheur
    </h1>
    <div class="well">
        <h2>
            ${i_Parapheur_internal_task.folderName}
            <small>
                ${i_Parapheur_internal_task.deskName}
            </small>
        </h2>

        <p class="lead">
            <#if i_Parapheur_internal_task.notificationType?? && i_Parapheur_internal_task.notificationType == "ACTION_PERFORMED">
                Le dossier ${i_Parapheur_internal_task.folderName} vient d'&ecirc;tre ${i_Parapheur_internal_task.taskActionPastString}
                par ${i_Parapheur_internal_from_first_name} ${i_Parapheur_internal_from_last_name}.
            <#elseif i_Parapheur_internal_task.notificationType?? && i_Parapheur_internal_task.notificationType == "NEW_ON_DESK">
                <span>Le dossier ${i_Parapheur_internal_task.folderName} vient d'arriver sur votre bureau ${i_Parapheur_internal_task.deskName},
                pour l'action ${i_Parapheur_internal_task.taskActionString}
                    <#if i_Parapheur_internal_task.byDelegation>
                        , en l'absence de ${i_Parapheur_internal_task.delegatingDeskName}.
                    <#else>.
                    </#if>
                    </span>

            <#elseif i_Parapheur_internal_task.notificationType?? && i_Parapheur_internal_task.notificationType == "LATE">
                Le dossier ${i_Parapheur_internal_task.folderName} sur le bureau ${i_Parapheur_internal_task.deskName} est en retard.
            <#else>
                Inconnu
            </#if>
        </p>

        <#if i_Parapheur_internal_task.publicAnnotation?has_content>
            <p style="word-wrap: break-word;">
                L'annotation suivante a &eacute;t&eacute; d&eacute;pos&eacute;e : <br/>
                ${i_Parapheur_internal_task.publicAnnotation}
            </p>
        </#if>

        <span>Vous pouvez y acc&eacute;der via le parapheur &eacute;lectronique :</span>
        <span id="btn" class="btn btn-success">
            <#if i_Parapheur_internal_task.byDelegation>
                <a href="${i_Parapheur_internal_main_url}/tenant/${i_Parapheur_internal_task.tenantId}/desk/${i_Parapheur_internal_task.deskId}/folder/${i_Parapheur_internal_task.folderId}%3FasDeskId%3D${i_Parapheur_internal_task.delegatingDeskId}"
                   target="_blank">
                    Acc&eacute;der au dossier
                </a>
            <#else>
                <a href="${i_Parapheur_internal_main_url}/tenant/${i_Parapheur_internal_task.tenantId}/desk/${i_Parapheur_internal_task.deskId}/folder/${i_Parapheur_internal_task.folderId}%3FasDeskId%3D${i_Parapheur_internal_task.deskId}"
                   target="_blank">
                    Acc&eacute;der au dossier
                </a>
            </#if>
        </span>
    </div>

    <p><small>${i_Parapheur_internal_footer}</small></p>

</div>

</body>

</html>
