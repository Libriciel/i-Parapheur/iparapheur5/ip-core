<?xml version="1.0" encoding="UTF-8"?>

<!--
  ~ iparapheur Core
  ~ Copyright (C) 2018-2025 Libriciel-SCOP
  ~
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as
  ~ published by the Free Software Foundation, version 3.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~ GNU Affero General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program.  If not, see <https://www.gnu.org/licenses/>.
  ~
  ~ SPDX-License-Identifier: AGPL-3.0-only
  -->

<!--suppress XmlUnusedNamespaceDeclaration -->
<xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL"
    xmlns:flowable="http://flowable.org/bpmn"
    xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI"
    xmlns:omgdi="http://www.omg.org/spec/DD/20100524/DI"
    xmlns:omgdc="http://www.omg.org/spec/DD/20100524/DC"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

  <xsl:output
      indent="yes"
      encoding="UTF-8"/>


  <!-- Copy the entire XML -->

  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>


  <!-- Set executable -->

  <xsl:template match="bpmn:process/@isExecutable">

    <xsl:attribute name="isExecutable">
      <xsl:value-of select="'true'"/>
    </xsl:attribute>

  </xsl:template>


  <!-- Adding name -->

  <xsl:template match="bpmn:process[not(@name)]/@id">

    <xsl:attribute name="name">
      <xsl:value-of select="."/>
    </xsl:attribute>

    <xsl:attribute name="id">
      <xsl:value-of select="."/>
    </xsl:attribute>

  </xsl:template>


</xsl:stylesheet>
