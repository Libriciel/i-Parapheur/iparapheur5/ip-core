<?xml version="1.0" encoding="UTF-8"?>

<!--
  ~ iparapheur Core
  ~ Copyright (C) 2018-2025 Libriciel-SCOP
  ~
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as
  ~ published by the Free Software Foundation, version 3.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~ GNU Affero General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program.  If not, see <https://www.gnu.org/licenses/>.
  ~
  ~ SPDX-License-Identifier: AGPL-3.0-only
  -->

<!--suppress XmlUnusedNamespaceDeclaration -->
<xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL">

  <xsl:output
      indent="yes"
      encoding="UTF-8"/>


  <!--Flowable XMLNS-->

  <xsl:template match="/*[local-name()='definitions']">
    <bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:flowable="http://flowable.org/bpmn">
      <xsl:copy-of select="node()|@*"/>
    </bpmn:definitions>
  </xsl:template>


</xsl:stylesheet>
