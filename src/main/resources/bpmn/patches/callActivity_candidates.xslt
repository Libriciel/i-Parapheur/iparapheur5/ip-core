<?xml version="1.0" encoding="UTF-8"?>

<!--
  ~ iparapheur Core
  ~ Copyright (C) 2018-2025 Libriciel-SCOP
  ~
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as
  ~ published by the Free Software Foundation, version 3.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~ GNU Affero General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program.  If not, see <https://www.gnu.org/licenses/>.
  ~
  ~ SPDX-License-Identifier: AGPL-3.0-only
  -->

<!--suppress XmlUnusedNamespaceDeclaration -->
<xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL"
    xmlns:flowable="http://flowable.org/bpmn"
    xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI"
    xmlns:omgdi="http://www.omg.org/spec/DD/20100524/DI"
    xmlns:omgdc="http://www.omg.org/spec/DD/20100524/DC"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

  <xsl:output
      indent="yes"
      encoding="UTF-8"/>


  <!-- Copy the entire XML -->

  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>


  <!-- callActivity content -->

  <xsl:template match="bpmn:callActivity[starts-with(@name, 'visa') or starts-with(@name, 'signature')]">


    <!-- Finding previous task -->

    <xsl:variable name="currentTaskId" select="@id"/>

    <xsl:variable name="currentTaskName" select="@name"/>

    <xsl:variable name="previousTaskId"
                  select="//bpmn:definitions/bpmn:process/bpmn:sequenceFlow[@targetRef = $currentTaskId]/@sourceRef"/>

    <xsl:variable name="previousTaskName"
                  select="//bpmn:definitions/bpmn:process/bpmn:callActivity[@id = $previousTaskId]/@name"/>

    <xsl:variable name="isPreviousTaskVisa"
                  select="boolean(//bpmn:definitions/bpmn:process/bpmn:callActivity[(@id = $previousTaskId) and starts-with(@name, 'visa')])"/>

    <xsl:variable name="isPreviousTaskSignature"
                  select="boolean(//bpmn:definitions/bpmn:process/bpmn:callActivity[(@id = $previousTaskId) and starts-with(@name, 'signature')])"/>

    <xsl:variable name="isPreviousTaskRegular" select="$isPreviousTaskVisa or $isPreviousTaskSignature"/>

    <xsl:variable name="previousTaskCandidates">
      <xsl:choose>
        <xsl:when test="$isPreviousTaskVisa">
          <xsl:value-of select="substring-after($previousTaskName, 'visa ')"/>
        </xsl:when>
        <xsl:when test="$isPreviousTaskSignature">
          <xsl:value-of select="substring-after($previousTaskName, 'signature ')"/>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="currentTaskCandidates">
      <xsl:choose>
        <xsl:when test="starts-with($currentTaskName, 'visa')">
          <xsl:value-of select="substring-after($currentTaskName, 'visa ')"/>
        </xsl:when>
        <xsl:when test="starts-with($currentTaskName, 'signature')">
          <xsl:value-of select="substring-after($currentTaskName, 'signature ')"/>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>


    <!-- Update candidates -->

    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>

      <xsl:element name="bpmn:extensionElements">

        <xsl:element name="flowable:in">

          <xsl:attribute name="sourceExpression">
            <!--<xsl:value-of select="concat('${', $currentTaskId, '_candidates}')"/>-->
            <xsl:value-of select="string($currentTaskCandidates)"/>
          </xsl:attribute>

          <xsl:attribute name="target">
            <xsl:value-of select="'current_candidate_groups'"/>
          </xsl:attribute>

        </xsl:element>

        <xsl:element name="flowable:in">

          <xsl:attribute name="sourceExpression">
            <xsl:choose>
              <xsl:when test="$isPreviousTaskRegular">
                <!--<xsl:value-of select="concat('${', $previousTaskId, '_candidates}')"/>-->
                <xsl:value-of select="string($previousTaskCandidates)"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="'admins'"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>

          <xsl:attribute name="target">
            <xsl:value-of select="'previous_candidate_groups'"/>
          </xsl:attribute>
        </xsl:element>

        <xsl:element name="flowable:in">

          <xsl:attribute name="sourceExpression">
            <xsl:choose>
              <xsl:when test="$isPreviousTaskVisa">
                <xsl:value-of select="'visa'"/>
              </xsl:when>
              <xsl:when test="$isPreviousTaskSignature">
                <xsl:value-of select="'signature'"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="'unknown'"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>

          <xsl:attribute name="target">
            <xsl:value-of select="'previous_action'"/>
          </xsl:attribute>
        </xsl:element>

      </xsl:element>
    </xsl:copy>

  </xsl:template>


</xsl:stylesheet>
