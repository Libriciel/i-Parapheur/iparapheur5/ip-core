/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
package coop.libriciel.ipcore.services.transformation;

import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.layout.Canvas;
import com.itextpdf.layout.element.Image;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.utils.FileUtils;
import coop.libriciel.ipcore.utils.ForcedExceptionsConnector;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.RequestUtils;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.http.MediaType;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Set;

import static coop.libriciel.ipcore.services.content.ContentServiceInterface.FILE_TRANSFER_BUFFER_SIZE;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.MediaType.*;


/**
 * Weird things happening here :
 * <p>
 * We may use the internal Alfresco transformation, but we want to keep a separate service, like we did with Flowable.
 * Alfresco will probably not always be our default content service, yet we'll still want a transformation service.
 * <p>
 * So we'll call the transformation service directly from Core, its API is pretty straightforward.
 * Note that LibreOffice (at this time) does not have an official Docker image.
 * <p>
 * The packaged one from Alfresco is pretty neat, and gives us some inside logs and tests, through its Web access.
 */
@Log4j2
@Service(TransformationServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = TransformationServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = "alfresco")
public class AlfrescoTransformAllInOneService implements TransformationServiceInterface {


    private static final String TRANSFORMATION_PATH = "transform";
    private static final Set<MediaType> IMAGES_MEDIA_TYPES = Set.of(IMAGE_GIF, IMAGE_JPEG, IMAGE_PNG);


    // <editor-fold desc="LifeCycle">


    private final TransformationServiceProperties properties;


    @Autowired
    public AlfrescoTransformAllInOneService(TransformationServiceProperties properties) {
        this.properties = properties;
    }


    // </editor-fold desc="LifeCycle">


    /**
     * For some reason, Alfresco Transformation Service does not recognize every mime-type.
     * We can force additional (compatible) transformations by forcing the mime-type.
     */
    private static MediaType compatibilityFix(@NotNull MediaType mediaType, @NotNull String fileName) {
        log.debug("compatibilityFix media-type:{}/{}", mediaType.getType(), mediaType.getSubtype());

        // Google Chrome on Windows 10 VirtualBox VMs break everything up, forcing some non-standards media-type as octet-stream.
        // PDFs and image are correctly set upon upload requests.
        // This is to fix our tests cases, maybe we won't need every other possible mimetype.

        if (StringUtils.equals(mediaType.toString(), APPLICATION_OCTET_STREAM_VALUE)) {
            return FileUtils.getMediaTypeFromFileName(fileName);
        }

        // Should be accepted, but refused by the transformation service for some reason

        if (StringUtils.equals(mediaType.toString(), "text/rtf")) {
            return new MediaType("application", "rtf");
        }

        return mediaType;
    }


    /**
     * Alfresco All-in-One service does not manage JPG to PDF.
     * Since we're managing images one at a time, it is way easier to do it here,
     * than add some plug-in into the Alfresco Transformer native Docker image.
     *
     * @return
     */
    static DocumentBuffer imageToPdf(DocumentBuffer documentBuffer) {

        DocumentBuffer result = new DocumentBuffer();
        result.setName(FilenameUtils.removeExtension(documentBuffer.getName()) + ".pdf");

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
             PdfWriter pdfWriter = new PdfWriter(outputStream);
             InputStream sourceInputStream = RequestUtils.bufferToInputStream(documentBuffer)) {

            byte[] imageBytes = sourceInputStream.readAllBytes();
            ImageData imageData = ImageDataFactory.create(imageBytes);
            Image pdfImg = new Image(imageData);

            Rectangle rectangle = new Rectangle(0, 0, imageData.getWidth(), imageData.getHeight());
            try (PdfDocument pdfDocument = new PdfDocument(pdfWriter)) {
                PdfPage page = pdfDocument.addNewPage();
                page.setMediaBox(rectangle);
                PdfCanvas pdfCanvas = new PdfCanvas(page);
                try (Canvas canvas = new Canvas(pdfCanvas, rectangle)) {canvas.add(pdfImg);}
            }

            byte[] pdfResultBytes = outputStream.toByteArray();
            try (InputStream resultPdfIs = new ByteArrayInputStream(pdfResultBytes)) {
                Flux<DataBuffer> buffer = DataBufferUtils.readInputStream(() -> resultPdfIs, new DefaultDataBufferFactory(), FILE_TRANSFER_BUFFER_SIZE);
                result.setContentFlux(buffer);
            }

        } catch (IOException e) {
            log.error("Error performing image to pdf transformation: {}", e.getMessage());
            log.debug("Error details : ", e);
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.error_on_image_to_pdf_transform");
        }

        return result;
    }


    @Override
    public @NotNull DocumentBuffer toPdf(@NotNull DocumentBuffer documentBuffer) {
        log.debug("AlfrescoTransformAllInOneService toPdf: {} mediaType: {}", documentBuffer.getName(), documentBuffer.getMediaType());

        MediaType fixedMediaType = compatibilityFix(documentBuffer.getMediaType(), documentBuffer.getName());
        String mediaTypeWithoutCharset = String.format("%s/%s", fixedMediaType.getType(), fixedMediaType.getSubtype());

        // Special case, on image documents

        if (IMAGES_MEDIA_TYPES.stream().anyMatch(fixedMediaType::isCompatibleWith)) {
            log.debug("AlfrescoTransformAllInOneService image: {}", fixedMediaType);
            return imageToPdf(documentBuffer);
        }

        // Alfresco transform

        log.debug("AlfrescoTransformAllInOneService AiO: {}", fixedMediaType);

        MultipartBodyBuilder builder = new MultipartBodyBuilder();
        builder.part("sourceMimetype", mediaTypeWithoutCharset);
        builder.part("targetExtension", "pdf");
        builder.part("targetMimetype", APPLICATION_PDF_VALUE);

        // For some reason, header seems mandatory for the asyncPart, and not for the other ones.
        // Cheers to this article : https://dev.to/shavz/sending-multipart-form-data-using-spring-webtestclient-2gb7
        builder.asyncPart("file", documentBuffer.getContentFlux(), DataBuffer.class)
                .header(CONTENT_DISPOSITION, "form-data; name=file; filename=" + documentBuffer.getName())
                .header(CONTENT_TYPE, mediaTypeWithoutCharset);

        // Upload content

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(properties.getScheme()).host(properties.getHost()).port(properties.getPort())
                .path(TRANSFORMATION_PATH)
                .build().normalize().toUri();

        DocumentBuffer result = new DocumentBuffer();
        result.setName(documentBuffer.getName() + ".pdf");
        result.setContentFlux(WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
                .post().uri(requestUri)
                .contentType(MULTIPART_FORM_DATA).accept(APPLICATION_OCTET_STREAM)
                .body(BodyInserters.fromMultipartData(builder.build()))
                .exchangeToFlux(rep -> RequestUtils.clientResponseToDataBufferFlux(rep, result, result.getName()))
                // Result
                .doOnError(e -> {
                    log.error("Error on calling transformation service: {}", e.getMessage());
                    log.debug("Error details : ", e);
                    throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_transformation_service");
                })
        );

        return result;
    }


}
