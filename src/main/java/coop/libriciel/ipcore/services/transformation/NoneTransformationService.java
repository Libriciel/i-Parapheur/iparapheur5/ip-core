/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.transformation;

import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import org.jetbrains.annotations.NotNull;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.io.Serial;

import static coop.libriciel.ipcore.utils.TextUtils.NONE_SERVICE;
import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;


@Service(TransformationServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = TransformationServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = NONE_SERVICE)
public class NoneTransformationService implements TransformationServiceInterface {


    private static class NoTransformationServiceError extends LocalizedStatusException {

        private static final @Serial long serialVersionUID = 6927355390438936563L;


        public NoTransformationServiceError() {
            super(SERVICE_UNAVAILABLE, "message.secure_mail_service_not_available");
        }

    }


    @Override
    public @NotNull DocumentBuffer toPdf(@NotNull DocumentBuffer documentBuffer) {
        throw new NoTransformationServiceError();
    }


}
