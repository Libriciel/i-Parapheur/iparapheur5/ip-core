/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.crypto;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import coop.libriciel.crypto.api.model.*;
import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.content.DetachedSignature;
import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.crypto.DocumentDataToSignHolder;
import coop.libriciel.ipcore.model.crypto.PdfSignaturePosition;
import coop.libriciel.ipcore.model.crypto.SignatureFormat;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.database.Type;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.utils.FileUtils;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.TextUtils;
import coop.libriciel.ipcore.utils.UserUtils;
import jakarta.validation.Validation;
import jakarta.validation.ValidatorFactory;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringSubstitutor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.util.*;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static coop.libriciel.crypto.api.model.CertificationPermission.CHANGES_PERMITTED;
import static coop.libriciel.crypto.api.model.DigestAlgorithm.SHA256;
import static coop.libriciel.crypto.api.model.SignaturePackaging.DETACHED;
import static coop.libriciel.crypto.api.model.SignaturePackaging.ENVELOPED;
import static coop.libriciel.ipcore.model.crypto.SignatureFormat.*;
import static coop.libriciel.ipcore.model.pdfstamp.Origin.CENTER;
import static coop.libriciel.ipcore.utils.CryptoUtils.*;
import static coop.libriciel.ipcore.utils.TextUtils.MESSAGE_BUNDLE;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.singletonList;
import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.firstNonEmpty;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.MediaType.APPLICATION_PDF;


public interface CryptoServiceInterface {


    Logger log = LogManager.getLogger(ContentServiceInterface.class);

    String BEAN_NAME = "cryptoService";
    String PREFERENCES_PROVIDER_KEY = "services.crypto.provider";
    String DEFAULT_SIGNATURE_LOGO_PNG_RESOURCE_NAME = "templates/signature/default_signature_logo.png";

    Pattern COMPLEMENTARY_INFO_TITLE_PATTERN = Pattern.compile("TITRE=\"(.*?)\"");
    Pattern COMPLEMENTARY_INFO_CITY_PATTERN = Pattern.compile("VILLE=\"(.*?)\"");
    Pattern COMPLEMENTARY_INFO_ZIP_CODE_PATTERN = Pattern.compile("CODE_?POSTAL=\"(.*?)\"");


    // <editor-fold desc="Utils">


    default CadesParameters generateCadesParameters(@NotNull Folder folder,
                                                    @NotNull Desk desk,
                                                    @NotNull Tenant tenant,
                                                    @NotNull String certBase64,
                                                    @NotNull List<DataToSign> dataToSignList,
                                                    long signatureDateTime,
                                                    @Nullable User user) {

        // Default fields
        CadesParameters cadesParams = new CadesParameters();
        cadesParams.setDataToSignList(dataToSignList);
        cadesParams.setPublicCertificateBase64(certBase64);
        cadesParams.setSignatureDateTime(signatureDateTime);
        cadesParams.setDigestAlgorithm(SHA256);

        overrideParamsWithComplementaryInfo(
                user,
                desk,
                folder.getType(),
                tenant,
                cadesParams::setClaimedRoles,
                cadesParams::setCountry,
                cadesParams::setCity,
                cadesParams::setZipCode
        );

        return cadesParams;
    }


    default XadesParameters generateXadesParameters(@NotNull Folder folder,
                                                    @NotNull Desk desk,
                                                    @NotNull Tenant tenant,
                                                    @NotNull String certBase64,
                                                    @NotNull List<DataToSign> dataToSignList,
                                                    long signatureDateTime,
                                                    @Nullable User user,
                                                    boolean isEnveloped) {

        // Default fields
        XadesParameters xadesParams = new XadesParameters();
        xadesParams.setDataToSignList(dataToSignList);
        xadesParams.setPublicCertificateBase64(certBase64);
        xadesParams.setSignatureDateTime(signatureDateTime);
        xadesParams.setDigestAlgorithm(SHA256);

        overrideParamsWithComplementaryInfo(
                user,
                desk,
                folder.getType(),
                tenant,
                xadesParams::setClaimedRoles,
                xadesParams::setCountry,
                xadesParams::setCity,
                xadesParams::setZipCode
        );

        // XAdES specific fields
        xadesParams.setSignaturePackaging(isEnveloped ? ENVELOPED : DETACHED);

        return xadesParams;
    }


    default PadesParameters generatePadesParameters(@NotNull Tenant tenant,
                                                    @NotNull Folder folder,
                                                    @NotNull Desk desk,
                                                    @NotNull String certBase64,
                                                    @NotNull List<DataToSign> dataToSignList,
                                                    @NotNull User user,
                                                    @NotNull String defaultUserName,
                                                    @NotNull PdfSignaturePosition signaturePosition,
                                                    long signatureDateTime,
                                                    boolean isSealStep,
                                                    @NotNull Resource templateResource,
                                                    @Nullable String signatureImageBase64,
                                                    @Nullable String customSignatureField,
                                                    @Nullable Desk delegatingDesk) {

        Date signatureDate = new Date(signatureDateTime);

        // Default fields

        PadesParameters padesParams = new PadesParameters();
        padesParams.setDataToSignList(dataToSignList);
        padesParams.setPublicCertificateBase64(certBase64);
        padesParams.setSignatureDateTime(signatureDateTime);
        padesParams.setDigestAlgorithm(SHA256);

        overrideParamsWithComplementaryInfo(
                user,
                desk,
                folder.getType(),
                tenant,
                padesParams::setClaimedRoles,
                padesParams::setCountry,
                padesParams::setCity,
                padesParams::setZipCode
        );

        // PAdES specific fields

        String userFullName = Optional.of(user)
                .map(UserUtils::prettyPrint)
                .filter(StringUtils::isNotEmpty)
                .orElse(defaultUserName)
                .trim(); // For some reason, trailing spaces breaks the YML signature generation

        String displayDeskName = padesParams.getClaimedRoles().stream()
                .findFirst()
                .or(() -> Optional.ofNullable(desk.getName()))
                .orElse(EMPTY);

        String delegatingDeskMessage = ofNullable(delegatingDesk)
                .map(Desk::getName)
                .map(deskName -> {
                    ResourceBundle messageResourceBundle = ResourceBundle.getBundle(MESSAGE_BUNDLE, Locale.getDefault());
                    String delegatingFromMessageFormat = messageResourceBundle.getString("message.delegating_from");
                    String escapedMessageFormat = TextUtils.escapeMessageFormatSpecialChars(delegatingFromMessageFormat);
                    return MessageFormat.format(escapedMessageFormat, deskName);
                })
                .orElse(EMPTY);

        Map<String, String> dummyMapping = new HashMap<>();
        dummyMapping.put(SIGNATURE_PLACEHOLDER_SIGNATURE_IMAGE_BASE64, ofNullable(signatureImageBase64).orElseGet(this::getDefaultPdfSignatureBase64));
        dummyMapping.put(SIGNATURE_PLACEHOLDER_USER_NAME, userFullName);
        dummyMapping.put(SIGNATURE_PLACEHOLDER_DESK_NAME, displayDeskName);
        dummyMapping.put(SIGNATURE_PLACEHOLDER_DESK_SHORT_NAME, Optional.ofNullable(desk.getShortName()).orElse(EMPTY));
        dummyMapping.put(SIGNATURE_PLACEHOLDER_TENANT_NAME, Optional.ofNullable(tenant.getName()).orElse(EMPTY));
        dummyMapping.put(SIGNATURE_PLACEHOLDER_SIGNATURE_DATE, DateFormat.getDateInstance(DateFormat.MEDIUM).format(signatureDate));
        dummyMapping.put(SIGNATURE_PLACEHOLDER_SIGNATURE_TIME, DateFormat.getTimeInstance(DateFormat.MEDIUM).format(signatureDate));
        dummyMapping.put(SIGNATURE_PLACEHOLDER_CUSTOM_SIGNATURE_FIELD, ofNullable(customSignatureField).orElse(EMPTY));
        dummyMapping.put(SIGNATURE_PLACEHOLDER_DELEGATION_FROM, delegatingDeskMessage);
        dummyMapping.put(SIGNATURE_PLACEHOLDER_COUNTRY_NAME, EMPTY);
        dummyMapping.put(SIGNATURE_PLACEHOLDER_CITY_NAME, ofNullable(padesParams.getCity()).orElse(EMPTY));
        dummyMapping.put(SIGNATURE_PLACEHOLDER_ZIPCODE_NAME, ofNullable(padesParams.getZipCode()).orElse(EMPTY));

        // On seal action, we "seal" the document
        padesParams.setCertificationPermission(isSealStep ? CHANGES_PERMITTED : null);
        padesParams.setStamp(getPdfSignatureStamp(dummyMapping, templateResource));

        PdfSignatureStamp pdfSignatureStamp = ofNullable(padesParams.getStamp())
                .orElseThrow(() -> new LocalizedStatusException(BAD_REQUEST, "message.bad_request_no_signature_stamp"));
        boolean hasToBeCentered = (signaturePosition.getOrigin() == CENTER);

        Float width = ofNullable(pdfSignatureStamp.getWidth())
                .orElseThrow(() -> new LocalizedStatusException(BAD_REQUEST, "message.bad_request_stamp_without_width"));
        Float height = ofNullable(pdfSignatureStamp.getHeight())
                .orElseThrow(() -> new LocalizedStatusException(BAD_REQUEST, "message.bad_request_stamp_without_height"));

        pdfSignatureStamp.setX(signaturePosition.getX() - (hasToBeCentered ? (width / 2) : 0));
        pdfSignatureStamp.setY(signaturePosition.getY() - (hasToBeCentered ? (height / 2) : 0));
        pdfSignatureStamp.setPage(signaturePosition.getPage());

        return padesParams;
    }


    default void overrideParamsWithComplementaryInfo(@Nullable User user,
                                                     @NotNull Desk desk,
                                                     @Nullable Type type,
                                                     @NotNull Tenant tenant,
                                                     @NotNull Consumer<List<String>> claimedRolesSetter,
                                                     @NotNull Consumer<String> countrySetter,
                                                     @NotNull Consumer<String> citySetter,
                                                     @NotNull Consumer<String> zipCodeSetter) {

        // User specific fields

        Optional<String> userComplementaryField = ofNullable(user)
                .map(User::getComplementaryField)
                .filter(StringUtils::isNotEmpty);

        String userTitle = userComplementaryField
                .map(COMPLEMENTARY_INFO_TITLE_PATTERN::matcher)
                .filter(Matcher::find)
                .map(matcher -> matcher.group(1))
                .orElse(null);

        String userCity = userComplementaryField
                .map(COMPLEMENTARY_INFO_CITY_PATTERN::matcher)
                .filter(Matcher::find)
                .map(matcher -> matcher.group(1))
                .orElse(null);

        String userZipCode = userComplementaryField
                .map(COMPLEMENTARY_INFO_ZIP_CODE_PATTERN::matcher)
                .filter(Matcher::find)
                .map(matcher -> matcher.group(1))
                .orElse(null);

        // Properties fallback
        // We want to filter out empty strings here

        String typeCity = ofNullable(type)
                .map(Type::getSignatureLocation)
                .orElse(null);

        String typeZipCode = ofNullable(type)
                .map(Type::getSignatureZipCode)
                .orElse(null);

        // Properties fallback

        String propertiesCountry = Optional.of(getProperties())
                .map(CryptoServiceProperties::getDefaultLocation)
                .map(CryptoServiceProperties.SignatureLocation::getCountry)
                .orElse(null);

        String propertiesCity = Optional.of(getProperties())
                .map(CryptoServiceProperties::getDefaultLocation)
                .map(CryptoServiceProperties.SignatureLocation::getCity)
                .orElse(null);

        String propertiesZipCode = Optional.of(getProperties())
                .map(CryptoServiceProperties::getDefaultLocation)
                .map(CryptoServiceProperties.SignatureLocation::getZipCode)
                .orElse(null);

        // (Note that the firstNonEmpty method may return null if everything is actually null)

        String computedRole = firstNonEmpty(userTitle, desk.getName());
        String computedCity = firstNonEmpty(userCity, typeCity, propertiesCity);
        String computedZipCode = firstNonEmpty(userZipCode, typeZipCode, tenant.getZipCode(), propertiesZipCode);

        log.debug("signatureParams...");
        log.debug("    propertiesCountry: {}", propertiesCountry);
        log.debug("    userTitle: {} deskName: {} -> computed role: {}", userTitle, desk.getName(), computedRole);
        log.debug("    city user: {} type: {} properties: {} -> {}", userCity, typeCity, propertiesCity, computedCity);
        log.debug("    zipCode user: {} type: {} tenant: {} properties: {} -> {}", userZipCode, typeZipCode, propertiesZipCode, tenant.getZipCode(), computedZipCode);

        claimedRolesSetter.accept(singletonList(computedRole));
        countrySetter.accept(propertiesCountry);
        citySetter.accept(computedCity);
        zipCodeSetter.accept(computedZipCode);
    }


    default @NotNull PdfSignatureStamp getPdfSignatureStamp(@NotNull Map<String, String> substitutions,
                                                            @NotNull Resource templateResource) {

        ObjectMapper yamlReader = new ObjectMapper(new YAMLFactory());
        try (InputStream inputStream = templateResource.getInputStream();
             ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory()) {

            // TODO: This is done the wrong way.
            //  We shall instantiate the YAML directly with the InputStream,
            //  Then, replace the values in the PdfSignatureStamp's Elements objects.
            //  That will avoid the need to read the entire file in memory.
            String yamlString = new String(inputStream.readAllBytes(), UTF_8);

            PdfSignatureStamp stamp = yamlReader.readValue(yamlString, PdfSignatureStamp.class);

            stamp.getElements().forEach(element -> element.setValue(StringSubstitutor.replace(element.getValue(), substitutions)));

            validatorFactory.getValidator()
                    .validate(stamp)
                    .stream()
                    .findFirst()
                    .ifPresent(violation -> {
                        log.debug("error in validation constraint : {}", violation);
                        throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, violation.getMessageTemplate());
                    });

            return stamp;

        } catch (IOException exception) {
            log.error("Error substituting values in signature stamp : {}", exception.getMessage());
            log.debug("Error details : ", exception);
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.could_not_read_template_content", exception);
        }
    }


    default @NotNull String getDefaultPdfSignatureBase64() {

        ResourceLoader resourceLoader = new DefaultResourceLoader();
        Resource defaultSignatureLogoResource = FileUtils.getOverridableResource(resourceLoader, DEFAULT_SIGNATURE_LOGO_PNG_RESOURCE_NAME);

        try (InputStream inputStream = defaultSignatureLogoResource.getInputStream()) {
            // TODO : find a way to stream these bytes into the final String, without going through the entire byte array in memory.
            // Base64OutputStream seems the way to go.
            return Base64.getEncoder().encodeToString(inputStream.readAllBytes());
        } catch (IOException exception) {
            throw new ResponseStatusException(INTERNAL_SERVER_ERROR, "cannot read local signature YAML", exception);
        }
    }


    default @NotNull SignatureFormat getSignatureFormat(@NotNull Type type, @NotNull List<Document> documentList) {

        // Default case

        if (type.getSignatureFormat() != AUTO) {
            return type.getSignatureFormat();
        }

        // AUTO format logic

        SignatureFormat result;

        boolean areAllPdf = documentList.stream()
                .filter(Document::isMainDocument)
                .map(Document::getMediaType)
                .allMatch(APPLICATION_PDF::equalsTypeAndSubtype);
        boolean isSingleDocument = documentList.size() == 1;
        boolean areAllXml = documentList.stream().allMatch(document -> FileUtils.isXml(document.getMediaType()));

        DetachedSignature detachedSignature = documentList.stream()
                .filter(Document::isMainDocument)
                .map(Document::getDetachedSignatures)
                .filter(CollectionUtils::isNotEmpty)
                .flatMap(Collection::stream)
                .findAny()
                .orElse(null);

        if (detachedSignature != null) {
            result = FileUtils.isXml(detachedSignature.getMediaType()) ? XADES_DETACHED : PKCS7;
        } else if (areAllPdf) {
            result = PADES;
        } else if (isSingleDocument && areAllXml) {
            result = PES_V2;
        } else {
            result = getProperties().getAutoFormatDefaultValue().getDoc();
        }

        return result;
    }


    // </editor-fold desc="Utils">


    CryptoServiceProperties getProperties();


    default DocumentDataToSignHolder getDataToSign(@NotNull Tenant tenant,
                                                   @NotNull Desk desk,
                                                   @NotNull Folder folder,
                                                   @NotNull User user,
                                                   @NotNull DocumentBuffer docBuffer,
                                                   @NotNull SignatureFormat signatureFormat,
                                                   @NotNull PdfSignaturePosition pdfSignaturePosition,
                                                   @NotNull String certificateBase64,
                                                   @NotNull String defaultUserName,
                                                   long signatureDateTime,
                                                   boolean isSealStep,
                                                   @NotNull Resource templateResource,
                                                   @Nullable String signatureImageBase64,
                                                   @Nullable String customSignatureField,
                                                   @Nullable Desk delegatingDesk) {

        switch (signatureFormat) {
            case AUTO -> {
                log.error("getDataToSign folderId:{} typeId:{}", folder.getId(), folder.getType().getId());
                throw new RuntimeException("The signature format should have been evaluated before the getDataToSign call");
            }
            case XADES_DETACHED, PES_V2 -> {
                XadesParameters xadesParams = generateXadesParameters(
                        folder,
                        desk,
                        tenant,
                        certificateBase64,
                        new ArrayList<>(),
                        signatureDateTime,
                        user,
                        signatureFormat.isEnvelopedSignature()
                );
                return getDataToSign(docBuffer, xadesParams);
            }
            case PADES -> {
                PadesParameters padesParams = generatePadesParameters(
                        tenant,
                        folder,
                        desk,
                        certificateBase64,
                        new ArrayList<>(),
                        user,
                        defaultUserName,
                        pdfSignaturePosition,
                        signatureDateTime,
                        isSealStep,
                        templateResource,
                        signatureImageBase64,
                        customSignatureField,
                        delegatingDesk);
                return getDataToSign(docBuffer, padesParams);
            }
            default -> {
                CadesParameters cadesParams = generateCadesParameters(
                        folder,
                        desk,
                        tenant,
                        certificateBase64,
                        new ArrayList<>(),
                        signatureDateTime,
                        user
                );
                return getDataToSign(docBuffer, cadesParams);
            }
        }
    }


    DocumentDataToSignHolder getDataToSign(@NotNull DocumentBuffer documentBuffer, @NotNull XadesParameters params);


    DocumentDataToSignHolder getDataToSign(@NotNull DocumentBuffer documentBuffer, @NotNull PadesParameters params);


    DocumentDataToSignHolder getDataToSign(@NotNull DocumentBuffer documentBuffer, @NotNull CadesParameters params);


    default DocumentBuffer signDocument(@NotNull Tenant tenant,
                                        @NotNull Desk desk,
                                        @NotNull Folder folder,
                                        @NotNull User user,
                                        @NotNull DataToSignHolder dataToSignHolder,
                                        @NotNull SignatureFormat signatureFormat,
                                        @NotNull PdfSignaturePosition pdfSignaturePosition,
                                        @NotNull String certificateBase64,
                                        @NotNull String defaultUserName,
                                        boolean isSealStep,
                                        @NotNull Resource templateResource,
                                        @Nullable DocumentBuffer docBuffer,
                                        @Nullable String signatureImageBase64,
                                        @Nullable String customSignatureField,
                                        @Nullable Desk delegatingDesk) {


        List<DataToSign> dataToSignList = ofNullable(dataToSignHolder.getDataToSignList())
                .orElseThrow(() -> new LocalizedStatusException(BAD_REQUEST, "message.bad_request_no_data_to_sign"));
        long signatureTime = ofNullable(dataToSignHolder.getSignatureDateTime())
                .orElseThrow(() -> new LocalizedStatusException(BAD_REQUEST, "message.bad_request_no_signature_date_time"));

        switch (signatureFormat) {
            case AUTO -> {
                log.error("signDocument folderId:{} typeId:{}", folder.getId(), folder.getType().getId());
                throw new RuntimeException("The signature format should have been evaluated before the signDocument call");
            }
            case XADES_DETACHED, PES_V2 -> {
                XadesParameters xadesParams = generateXadesParameters(
                        folder,
                        desk,
                        tenant,
                        certificateBase64,
                        dataToSignList,
                        signatureTime,
                        user,
                        signatureFormat.isEnvelopedSignature()
                );
                return signDocument(
                        ofNullable(docBuffer).orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.error_missing_document")),
                        xadesParams
                );
            }
            case PADES -> {
                PadesParameters padesParams = generatePadesParameters(
                        tenant,
                        folder,
                        desk,
                        certificateBase64,
                        dataToSignList,
                        user,
                        defaultUserName,
                        pdfSignaturePosition,
                        signatureTime,
                        isSealStep,
                        templateResource,
                        signatureImageBase64,
                        customSignatureField,
                        delegatingDesk
                );
                return signDocument(
                        ofNullable(docBuffer).orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.error_missing_document")),
                        padesParams
                );
            }
            default -> {
                CadesParameters cadesParams = generateCadesParameters(
                        folder,
                        desk,
                        tenant,
                        certificateBase64,
                        dataToSignList,
                        signatureTime,
                        user
                );
                return signDocument(cadesParams);
            }
        }
    }


    DocumentBuffer signDocument(@NotNull CadesParameters params);


    DocumentBuffer signDocument(@NotNull DocumentBuffer documentBuffer, @NotNull XadesParameters params);


    DocumentBuffer signDocument(@NotNull DocumentBuffer documentBuffer, @NotNull PadesParameters params);


}
