/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.crypto;

import coop.libriciel.crypto.api.model.*;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.crypto.DocumentDataToSignHolder;
import coop.libriciel.ipcore.utils.IpInternalException;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.RequestUtils;
import coop.libriciel.ipcore.utils.connector.IpDefaultConnector;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;

import static coop.libriciel.ipcore.utils.RequestUtils.SUBSERVICE_REQUEST_LONG_TIMEOUT;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.MediaType.ALL;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA;
import static org.springframework.security.config.Elements.HTTP;


@Log4j2
@Service(CryptoServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = CryptoServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = "crypto")
public class CryptoService implements CryptoServiceInterface {


    private static final String API_PATH = "crypto/api/v3";
    private static final String CADES_PATH = "cades";
    private static final String PADES_PATH = "pades";
    private static final String XADES_PATH = "xades";
    private static final String DATA_TO_SIGN_PATH = "generateDataToSign";
    private static final String SIGNATURE_PATH = "generateSignature";


    // <editor-fold desc="Beans">


    private final @Getter CryptoServiceProperties properties;


    @Autowired
    public CryptoService(CryptoServiceProperties properties) {
        this.properties = properties;
    }


    // </editor-fold desc="Beans">


    @Override
    public DocumentDataToSignHolder getDataToSign(@NotNull DocumentBuffer documentBuffer, @NotNull CadesParameters params) {
        return fetchDataToSign(CADES_PATH, documentBuffer, params);
    }


    @Override
    public DocumentDataToSignHolder getDataToSign(@NotNull DocumentBuffer documentBuffer, @NotNull XadesParameters params) {
        return fetchDataToSign(XADES_PATH, documentBuffer, params);
    }


    @Override
    public DocumentDataToSignHolder getDataToSign(@NotNull DocumentBuffer documentBuffer, @NotNull PadesParameters params) {
        return fetchDataToSign(PADES_PATH, documentBuffer, params);
    }


    @Override
    public DocumentBuffer signDocument(@NotNull CadesParameters params) {
        return fetchSignedDocument(CADES_PATH, null, params);
    }


    @Override
    public DocumentBuffer signDocument(@NotNull DocumentBuffer documentBuffer, @NotNull XadesParameters params) {
        return fetchSignedDocument(XADES_PATH, documentBuffer, params);
    }


    @Override
    public DocumentBuffer signDocument(@NotNull DocumentBuffer documentBuffer, @NotNull PadesParameters params) {
        return fetchSignedDocument(PADES_PATH, documentBuffer, params);
    }


    private DocumentDataToSignHolder fetchDataToSign(@NotNull String urlPath, @NotNull DocumentBuffer documentBuffer, @NotNull Object params) {

        MultipartBodyBuilder builder = new MultipartBodyBuilder();
        builder.part("model", params);

        // For some reason, header seems mandatory for the asyncPart, and not for the other ones.
        // Cheers to this article : https://dev.to/shavz/sending-multipart-form-data-using-spring-webtestclient-2gb7
        builder.asyncPart("file", documentBuffer.getContentFlux(), DataBuffer.class)
                .header(CONTENT_DISPOSITION, "form-data; name=file; filename=" + documentBuffer.getName());

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(API_PATH).pathSegment(urlPath).pathSegment(DATA_TO_SIGN_PATH)
                .build().normalize().toUri();

        DocumentDataToSignHolder cryptoResult = WebClient.builder()
                .clientConnector(new IpDefaultConnector())
                .build()
                .post().uri(requestUri)
                .contentType(MULTIPART_FORM_DATA).accept(ALL)
                .body(BodyInserters.fromMultipartData(builder.build()))
                .exchangeToMono(clientResponse -> {
                    if (clientResponse.statusCode().isError()) {
                        return clientResponse.bodyToMono(ErrorResponse.class)
                                .doOnNext(errorResponse -> log.debug("fetchDataToSign - raw error from crypto : {}", errorResponse))
                                .flatMap(errorResponse -> Mono.error(new IpInternalException(errorResponse.getMessage())));
                    } else {
                        return clientResponse.bodyToMono(DataToSignHolder.class);
                    }
                })
                .doOnError(err -> {
                    log.error("An error was returned by the crypto service : {}", err.getMessage());
                    throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.error_from_crypto_service", err.getMessage());
                })
                .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT)
                .map(holder -> new DocumentDataToSignHolder(holder, documentBuffer.getId()))
                .orElseThrow(() -> {
                    log.error("fetchDataToSign - an error occurred while requesting crypto");
                    return new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_crypto_service");
                });

        log.debug("createDataToSign result:{}", cryptoResult);
        // TODO "when we have time" : this (family of) method(s) should return an optional directly to callers.
        return cryptoResult;
    }


    private DocumentBuffer fetchSignedDocument(@NotNull String urlPath, @Nullable DocumentBuffer documentBuffer, @NotNull Object params) {

        MultipartBodyBuilder builder = new MultipartBodyBuilder();
        builder.part("model", params);

        String headerDocName = "signed-doc";
        if (documentBuffer != null) {
            // For some reason, header seems mandatory for the asyncPart, and not for the other ones.
            // Cheers to this article : https://dev.to/shavz/sending-multipart-form-data-using-spring-webtestclient-2gb7
            builder.asyncPart("file", documentBuffer.getContentFlux(), DataBuffer.class)
                    .header(CONTENT_DISPOSITION, "form-data; name=file; filename=" + documentBuffer.getName());
            headerDocName = documentBuffer.getName();
        }
        final String defaultDocName = headerDocName;


        DocumentBuffer result = new DocumentBuffer();

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(API_PATH).pathSegment(urlPath).pathSegment(SIGNATURE_PATH)
                .build().normalize().toUri();

        result.setContentFlux(WebClient.builder().clientConnector(new IpDefaultConnector()).build()
                .post().uri(requestUri)
                .contentType(MULTIPART_FORM_DATA).accept(ALL)
                .body(BodyInserters.fromMultipartData(builder.build()))
                .exchangeToFlux(clientResponse -> {
                    if (clientResponse.statusCode().isError()) {
                        return clientResponse.bodyToFlux(ErrorResponse.class)
                                .doOnNext(errorResponse -> log.debug("fetchDataToSign - raw error from crypto : {}", errorResponse))
                                .flatMap(errorResponse -> Flux.error(new IpInternalException(errorResponse.getMessage())));
                    } else {
                        return RequestUtils.clientResponseToDataBufferFlux(clientResponse, result, defaultDocName);
                    }
                })
                // Result
                .doOnError(err -> {
                    log.error("An error was returned by the crypto service : {}", err.getMessage());
                    throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.error_from_crypto_service", err.getMessage());
                })
        );

        log.debug("signDocument result:{}", result);
        return result;
    }


}
