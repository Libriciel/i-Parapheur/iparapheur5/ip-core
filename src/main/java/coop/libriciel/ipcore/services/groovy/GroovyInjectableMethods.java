/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.groovy;

import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.Nullable;

import javax.script.ScriptEngine;


/**
 * An instance of this will be passed to a Groovy's {@link ScriptEngine}.
 * This allows us to reduce Groovy's access
 */
@Log4j2
@AllArgsConstructor
public class GroovyInjectableMethods {


    private final AuthServiceInterface authService;
    private final String tenantId;


    /**
     * This will be called reflexively from the engine.
     */
    public boolean isDesk(@Nullable String deskId) {
        log.debug("Selection script isDesk() tenantId:{} isDesk:{}", tenantId, deskId);
        return authService.findDeskByIdOrShortName(tenantId, deskId) != null;
    }


    /**
     * This will be called reflexively from the engine.
     */
    public void log(@Nullable String message) {
        log.warn("Selection script tenantId:{} log:{}", tenantId, message);
    }


}
