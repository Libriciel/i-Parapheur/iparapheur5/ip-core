/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.groovy;

import coop.libriciel.ipcore.utils.WorkflowUtils;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.groovy.runtime.GStringImpl;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.script.ScriptEngine;
import java.util.List;
import java.util.Optional;

import static java.util.Collections.emptyList;


/**
 * An instance of this will be passed to a Groovy's {@link ScriptEngine}.
 * A convenient way to catch and retrieve the script's results.
 */
@Data
@Log4j2
@NoArgsConstructor
public class GroovyResultCatcher {


    private String workflowDefinitionKey;
    private List<String> variableDesks;


    /**
     * This will be called reflexively from the engine.
     *
     * @param catchWorkflowDefinitionKey The workflow definition catch
     * @param catchVariableDesks         The variable desks catch, that can be either a {@link String} or a {@link GStringImpl}
     */
    public void setResult(@NotNull String catchWorkflowDefinitionKey, @Nullable List<Object> catchVariableDesks) {

        // We only want a single result.
        // Only the first one will count, we'll ignore the following ones
        if (StringUtils.isNotEmpty(workflowDefinitionKey)) {
            return;
        }

        workflowDefinitionKey = catchWorkflowDefinitionKey;

        // Check if the catch element is actually a name, and switch it to ease v4 to v5 migrations
        // Yet, it may be a genuine typo, so we need to log some kind of warn.
        String cleanedWorkflowDefinitionKey = WorkflowUtils.nameToKey(catchWorkflowDefinitionKey);
        if (!StringUtils.equals(cleanedWorkflowDefinitionKey, workflowDefinitionKey)) {
            log.warn("The selection script seems to have forbidden characters. Maybe it is a v4 imported one ?");
            log.warn("    We'll try to fix it to continue : '{}' => '{}'", catchWorkflowDefinitionKey, cleanedWorkflowDefinitionKey);
            workflowDefinitionKey = cleanedWorkflowDefinitionKey;
        }

        variableDesks = Optional
                .ofNullable(catchVariableDesks)
                .orElse(emptyList())
                // Forces the Groovy's GStringImpl values ("${my_metadata}") to be evaluated and turned into a real String
                .stream()
                .map(Object::toString)
                // Then collect the results to a true String List
                .toList();
    }


}
