/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.groovy;

import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.Metadata;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.WorkflowDefinition;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.database.MetadataRepository;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.TextUtils;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;

import static coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface.META_VARIABLEDESK_PATTERN;
import static coop.libriciel.ipcore.utils.CollectionUtils.toMutableList;
import static coop.libriciel.ipcore.utils.TextUtils.INTERNAL_PREFIX;
import static org.springframework.data.domain.Pageable.unpaged;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.util.CollectionUtils.isEmpty;


@Log4j2
@Service("groovyService")
public class GroovyService {


    private static final String RESULT_CATCHER = INTERNAL_PREFIX + "groovy_result_catcher";
    private static final String METHODS_INTERFACE = INTERNAL_PREFIX + "groovy_methods_interface";

    /**
     * In the iparapheur v4, the Groovy engine was mapped to some sort of Groovy classes declarations.
     * It had to be called reflexively, that was a little bit too complex.
     * <p>
     * We actually just have to declare those functions, to insure the retro-compatibility.
     * The result will be the first line to be returned.
     */
    private static final String METHOD_CALLBACK_WITH_VAR_DESKS = "def circuit(result, array) { " + RESULT_CATCHER + ".setResult(result, array) }\n";
    private static final String METHOD_CALLBACK = "def circuit(result) { " + RESULT_CATCHER + ".setResult(result, null) }\n";
    private static final String METHOD_LOGGER = "def log(message) { " + METHODS_INTERFACE + ".log(message) }\n";
    private static final String METHOD_LEGACY_WRAPPER = "def selectionCircuit(closure) { closure.call() }\n";
    private static final String METHOD_IS_DESK = "def isBureau(deskId) { " + METHODS_INTERFACE + ".isDesk(deskId) }\n";

    private static final String GROOVY_MAIN_CONTEXT =
            METHOD_CALLBACK_WITH_VAR_DESKS + METHOD_CALLBACK + METHOD_LEGACY_WRAPPER + METHOD_IS_DESK + METHOD_LOGGER;


    private final ScriptEngineManager factory = new ScriptEngineManager();


    // <editor-fold desc="Beans">


    private final AuthServiceInterface authService;
    private final MetadataRepository metadataRepository;
    private final WorkflowServiceInterface workflowService;


    @Autowired
    public GroovyService(AuthServiceInterface authService,
                         MetadataRepository metadataRepository,
                         WorkflowServiceInterface workflowService) {
        this.authService = authService;
        this.metadataRepository = metadataRepository;
        this.workflowService = workflowService;
    }


    // </editor-fold desc="Beans">


    /**
     * TODO : Move/split this logic into a business service
     */
    public void updateWorkflowDataFromScript(@NotNull String tenantId, @NotNull Folder folder, @NotNull Map<Integer, String> variableDeskIds) {

        String originalValidationWorkflowKey = folder.getSubtype().getValidationWorkflowId();

        Optional<GroovyResultCatcher> scriptResultWrapped = computeSelectionScriptForFolder(tenantId, folder);
        String overrideValidationWorkflowKey = scriptResultWrapped
                .map(GroovyResultCatcher::getWorkflowDefinitionKey)
                .filter(StringUtils::isNotEmpty)
                .orElse(null);

        log.debug("updateWorkflowDataFromScript folder:{}", folder.getId());
        log.debug("      originalValidationWorkflowKey:{}", originalValidationWorkflowKey);
        log.debug("      overrideValidationWorkflowKey:{}", overrideValidationWorkflowKey);
        log.debug("      scriptResult.getVariableDesks:{}", scriptResultWrapped.map(GroovyResultCatcher::getVariableDesks).orElse(null));

        String finalValidationWorkflowKey = Optional.ofNullable(overrideValidationWorkflowKey)
                .or(() -> Optional.ofNullable(originalValidationWorkflowKey))
                .orElseThrow(() -> new LocalizedStatusException(NOT_ACCEPTABLE, "message.no_result_from_workflow_selection_script"));

        WorkflowDefinition definition = workflowService
                .getWorkflowDefinitionByKey(tenantId, finalValidationWorkflowKey)
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_workflow_definition_key"));

        if (StringUtils.isNotEmpty(overrideValidationWorkflowKey)) {
            GroovyResultCatcher scriptResult = scriptResultWrapped.get();
            log.debug("updateWorkflowDataFromScript overriddenWorkflow:{}", definition.getName());
            updateVariableDeskMapFromScriptResult(tenantId, scriptResult, definition, variableDeskIds);
            log.debug("variableDeskIds after substitution : {}", variableDeskIds);
        }

        folder.setFinalDesk(definition.getFinalDesk());
        folder.setValidationWorkflowDefinitionKey(finalValidationWorkflowKey);
    }


    public Optional<GroovyResultCatcher> computeSelectionScriptForFolder(@NotNull String tenantId, @NotNull Folder folder) {
        log.debug("computeSelectionScriptForFolder - folder name:{}", folder.getName());

        Map<String, Object> scriptMetadata = new HashMap<>();
        Page<Metadata> metadataModelsPage = metadataRepository
                .findAllByTenantIdAndKeyIn(tenantId, folder.getMetadata().keySet(), unpaged());

        // This filters out any metadata that is not a registered tenant metadata.
        List<String> metadataIds = metadataModelsPage.get().map(Metadata::getId).toList();
        log.trace("computeSelectionScriptForFolder - tenantId:{} metadataIds:{}", tenantId, metadataIds);
        metadataRepository
                .findAllByIdAndTenant_IdOrTenantLess(metadataIds, tenantId)
                .forEach(metadataModel -> {
                    String strValue = folder.getMetadata().get(metadataModel.getKey());
                    Object finalValue = StringUtils.isEmpty(strValue)
                                        ? null
                                        : switch (metadataModel.getType()) {
                                            case TEXT, URL, DATE -> strValue;
                                            case BOOLEAN -> Boolean.valueOf(strValue);
                                            case INTEGER -> Integer.valueOf(strValue);
                                            case FLOAT -> TextUtils.parseLocalizedDouble(strValue);
                                        };
                    scriptMetadata.put(metadataModel.getKey(), finalValue);
                });

        return Optional.ofNullable(folder.getSubtype())
                .map(Subtype::getWorkflowSelectionScript)
                .filter(StringUtils::isNotEmpty)
                .map(s -> executeSelectionScript(tenantId, s, scriptMetadata));
    }


    /**
     * The Groovy script engine has some concurrency defects.
     * It has some multi-thread declarations, but default bindings analysis says otherwise.
     * <p>
     * Since the method is rather quick (hopefully, we don't have any exponential complexity in user scripts...),
     * The easiest way to fix it is to make the method synchronized.
     *
     * @param tenantId
     * @param groovyScript
     * @param metadata
     * @return
     */
    synchronized @Nullable GroovyResultCatcher executeSelectionScript(@NotNull String tenantId, String groovyScript, Map<String, Object> metadata) {
        log.debug("Executing Groovy script : \n{}", groovyScript);
        log.debug("With metadata : \n{}", metadata);

        if (StringUtils.isEmpty(groovyScript)) {return null;}

        ScriptEngine engine = factory.getEngineByName("groovy");

        GroovyResultCatcher resultCatcher = new GroovyResultCatcher();
        metadata.forEach(engine::put);
        engine.put(RESULT_CATCHER, resultCatcher);
        engine.put(METHODS_INTERFACE, new GroovyInjectableMethods(authService, tenantId));

        String groovyScriptWithContext = GROOVY_MAIN_CONTEXT + groovyScript;

        try {
            engine.eval(groovyScriptWithContext);
        } catch (ScriptException exception) {
            log.debug(exception.getMessage());
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, exception, "message.error_on_workflow_selection_script");
        }

        if (StringUtils.isEmpty(resultCatcher.getWorkflowDefinitionKey())) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.no_result_from_workflow_selection_script");
        }

        log.debug("Groovy resultCatcher:{}", resultCatcher);
        return resultCatcher;
    }


    public void updateVariableDeskMapFromScriptResult(String tenantId,
                                                      @NotNull GroovyResultCatcher scriptResult,
                                                      WorkflowDefinition definition,
                                                      Map<Integer, String> variableDeskIds) {

        if (isEmpty(scriptResult.getVariableDesks())) {
            return;
        }

        List<String> variableDeskIdsFromScriptResolved = scriptResult.getVariableDesks().stream()
                .map(deskShortName -> authService.findDeskByIdOrShortName(tenantId, deskShortName))
                .filter(Objects::nonNull)
                .map(Desk::getId)
                .collect(toMutableList());

        // TODO : send an error if every variable desk is not set
        AtomicInteger stepIdx = new AtomicInteger(0);
        definition.getSteps().forEach(step -> {
            int stepIndex = stepIdx.getAndIncrement();
            step.getValidatingDesks().stream()
                    .map(DeskRepresentation::getId)
                    .forEach(deskId -> placeDeskIdInVariableDeskMap(deskId, variableDeskIds, variableDeskIdsFromScriptResolved, stepIndex));
        });

        placeDeskIdInVariableDeskMap(definition.getFinalDesk().getId(), variableDeskIds, variableDeskIdsFromScriptResolved, stepIdx.get());
    }


    private static void placeDeskIdInVariableDeskMap(String deskId,
                                                     Map<Integer, String> variableDeskIds,
                                                     List<String> variableDeskIdsFromScriptResolved,
                                                     int stepIndex) {
        Matcher variableDeskIdMatcher = META_VARIABLEDESK_PATTERN.matcher(deskId);
        if (variableDeskIdMatcher.matches() && !isEmpty(variableDeskIdsFromScriptResolved)) {
            String targetDeskId = variableDeskIdsFromScriptResolved.get(0);
            log.debug("placeDeskIdInVariableDeskMap - step:{}, placeholder deskId:{}, targetDeskId:{}", stepIndex, deskId, targetDeskId);
            variableDeskIds.put(stepIndex, targetDeskId);
            variableDeskIdsFromScriptResolved.remove(0);
        }
    }


}
