/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.signatureValidation;

import coop.libriciel.chorus.lib.generator.e.signature.lib.model.ReportContent;
import coop.libriciel.chorus.lib.generator.e.signature.lib.model.ResponseReport;
import coop.libriciel.chorus.lib.generator.e.signature.lib.model.ResponseReports;
import coop.libriciel.chorus.lib.generator.e.signature.lib.model.VerificationResponse;
import coop.libriciel.ipcore.utils.exceptions.ExternalException;
import coop.libriciel.ipcore.model.signatureValidation.ValidationServiceConfiguration;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.services.secret.SecretServiceInterface;
import coop.libriciel.ipcore.utils.IpInternalException;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.connector.IpDefaultConnector;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.MediaType;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.*;

import static coop.libriciel.ipcore.utils.RequestUtils.SUBSERVICE_REQUEST_LONG_TIMEOUT;
import static coop.libriciel.ipcore.utils.TextUtils.MESSAGE_BUNDLE;
import static org.springframework.http.HttpHeaders.*;
import static org.springframework.http.HttpStatus.*;


@Log4j2
public abstract class ChorusProAbstractService implements SignatureValidationServiceInterface {

    protected static final String GRANT_TYPE = "grant_type";
    protected static final String CLIENT_ID = "client_id";
    protected static final String CLIENT_SECRET = "client_secret";
    protected static final String SCOPE = "scope";
    public static final String PDF = "pdf";
    public static final String XML = "xml";
    protected String TOKEN_URL_VALUE;
    protected String REPORT_URL_VALUE;
    protected static final String END_TOKEN_URL_VALUE = "oauth.piste.gouv.fr/api/oauth/token";
    protected static final String END_REPORT_URL_VALUE = "api.piste.gouv.fr/esignature/v1/reports";
    protected static final String GRANT_TYPE_VALUE = "client_credentials";
    protected static final String SCOPE_VALUE = "tncp.esignature";
    protected static final String REPORT_TYPE_VALUE = "ALL";

    public static final String TMP_SIGNATURE_PROOF_PREFIX = "ip_signature_proof_";
    public static final String TMP_SIGNATURE_PROOF_PDF_SUFFIX = ".pdf";

    private final WebClient.Builder webClientBuilder;
    private final SecretServiceInterface secretServiceInterface;
    private ChorusProToken chorusProToken;
    private String clientId;

    private final ResourceBundle message = ResourceBundle.getBundle(MESSAGE_BUNDLE, Locale.getDefault());


    public ChorusProAbstractService(SecretServiceInterface secretServiceInterface,
                                    WebClient.Builder webClientBuilder) {
        this.secretServiceInterface = secretServiceInterface;
        this.webClientBuilder = webClientBuilder;
        setBaseUrl();
    }


    public abstract void setBaseUrl();


    @Override
    public void testConfiguration(@NotNull ValidationServiceConfiguration validationServiceConfiguration) {
        generateToken(validationServiceConfiguration);
    }


    @Override
    public ValidationServiceConfiguration getSignatureValidationConfiguration() {
        ValidationServiceConfiguration validationServiceConfiguration = secretServiceInterface.getValidationServiceConfiguration();
        if (validationServiceConfiguration == null) {
            log.debug("No identification details found for Chorus Pro");
        }
        return validationServiceConfiguration;
    }


    @NotNull
    private static MultiValueMap<String, String> getDataMultiValueMap(@NotNull ValidationServiceConfiguration validationServiceConfiguration) {
        MultiValueMap<String, String> data = new LinkedMultiValueMap<>();
        data.add(GRANT_TYPE, GRANT_TYPE_VALUE);
        data.add(CLIENT_ID, validationServiceConfiguration.getClientId());
        data.add(CLIENT_SECRET, validationServiceConfiguration.getClientSecret());
        data.add(SCOPE, SCOPE_VALUE);
        return data;
    }


    @Override
    public Map<String, String> getSignatureValidation(DocumentBuffer signedDocument, Collection<DocumentBuffer> originalFiles) throws ExternalException {
        verifyToken();

        VerificationResponse res = verifySignatureUsingTheApi(signedDocument, originalFiles);
        Map<String, String> content = new HashMap<>();

        content.put(PDF, Optional.ofNullable(res.getReports())
                .map(ResponseReports::getNewReports)
                .map(ResponseReport::getPdfReport)
                .map(ReportContent::getContent)
                .orElseThrow()
        );

        content.put(XML, Optional.ofNullable(res.getReports())
                .map(ResponseReports::getNewReports)
                .map(ResponseReport::getXmlReport)
                .map(ReportContent::getContent)
                .orElseThrow()
        );

        return content;
    }


    private void verifyToken() throws ExternalException {
        if (chorusProToken == null || new Date().after(chorusProToken.getExpirationTime())) {
            ValidationServiceConfiguration config = this.getSignatureValidationConfiguration();
            if (config == null) {
                throw new LocalizedStatusException(NOT_FOUND, "message.error_service_identification_missing");
            } else {
                chorusProToken = generateToken(config);
            }
            log.debug("ChorusPro Service: new token generated");
        }
        log.debug("ChorusPro Service: the token expires at " + chorusProToken.getExpirationTime());
    }


    private ChorusProToken generateToken(ValidationServiceConfiguration validationServiceConfiguration) throws ExternalException {

        clientId = validationServiceConfiguration.getClientId();
        MultiValueMap<String, String> data = getDataMultiValueMap(validationServiceConfiguration);

        WebClient webClient = webClientBuilder
                .baseUrl(TOKEN_URL_VALUE)
                .clientConnector(new IpDefaultConnector())
                .build();

        return webClient.post()
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .accept(MediaType.ALL)
                .body(BodyInserters.fromFormData(data))
                .exchangeToMono(clientResponse -> {
                    if (clientResponse.statusCode().isError()) {
                        return clientResponse.bodyToMono(ChorusProTokenErrorResponse.class)
                                .doOnNext(errorResponse -> {
                                    log.error("An error occurred during Chorus Pro authentification");
                                    log.debug("error url: {}, error: {}", TOKEN_URL_VALUE, errorResponse.getErrorDescription());
                                })
                                .flatMap(errorResponse -> Mono.error(new IpInternalException(errorResponse.getErrorDescription())));
                    } else {
                        log.trace("Chorus Pro authentification successful");
                        return clientResponse.bodyToMono(ChorusProToken.class);
                    }
                })
                .doOnError(err -> {
                    String errorMessage = err.getMessage();
                    log.error("An error was returned by the Chorus Pro service: {}", errorMessage);
                    if (errorMessage.contains("401")) {
                        throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.error_authentification_failed");
                    } else {
                        throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.error_from_chorus_pro_service", errorMessage);
                    }
                })
                .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT)
                .orElseThrow(() -> {
                    log.error("testChorusConfiguration - an error occurred while requesting Chorus Pro");
                    return new ExternalException(message.getString("message.cannot_reach_chorus_pro_service"));
                });
    }


    // TODO : Use when changes in the swagger regarding parameters and response language are done.
    //    /**
    //     * This method uses the generated library from the Piste swagger.
    //     * The lib requires AbstractResource for the document parameters.
    //     * The API returns English response and the lib expects French response.
    //     */
    //    private VerificationResponse verifySignatureUsingTheLib(AbstractResource signedDocument, Collection<AbstractResource> originalFiles) {
    //        ApiVerificationControllerApi apiInstance = new ApiVerificationControllerApi(apiClient);
    //        return apiInstance.verifyUsingPOST(
    //                        clientId,
    //                        REPORT_TYPE_VALUE,
    //                        signedDocument,
    //                        originalFiles)
    //                .block();
    //    }


    private VerificationResponse verifySignatureUsingTheApi(DocumentBuffer document, Collection<DocumentBuffer> originalFiles) {

        MultipartBodyBuilder builder = new MultipartBodyBuilder();
        builder.part("clientIdentification", clientId);
        builder.part("reportType", REPORT_TYPE_VALUE);
        if (originalFiles != null) {
            for (DocumentBuffer originalFile : originalFiles) {
                builder.asyncPart("originalFiles", originalFile.getContentFlux(), DataBuffer.class)
                        .header(CONTENT_DISPOSITION, "form-data; name=originalFiles; filename=\"" + originalFile.getName() + "\"");
            }
        }
        // For some reason, header seems mandatory for the asyncPart, and not for the other ones.
        // Cheers to this article : https://dev.to/shavz/sending-multipart-form-data-using-spring-webtestclient-2gb7
        builder.asyncPart("signedDocument", document.getContentFlux(), DataBuffer.class)
                .header(CONTENT_DISPOSITION, "form-data; name=signedDocument; filename=\"" + document.getName() + "\"");

        WebClient webClient = webClientBuilder
                .baseUrl(REPORT_URL_VALUE)
                .clientConnector(new IpDefaultConnector())
                .build();

        return webClient
                .post()
                .header(AUTHORIZATION, "Bearer " + chorusProToken.getAccessToken())
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .accept(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromMultipartData(builder.build()))
                .exchangeToMono(clientResponse -> {
                    if (clientResponse.statusCode().isError()) {
                        return clientResponse.bodyToMono(ChorusProReportErrorResponse.class)
                                .doOnNext(errorResponse -> log.debug("verifySignature - raw error from Chorus pro : {}", errorResponse))
                                .flatMap(errorResponse -> Mono.error(new ExternalException(errorResponse.getMessage())));
                    } else {
                        return clientResponse.bodyToMono(VerificationResponse.class);
                    }
                })
                .doOnError(err -> {
                    log.error("An error was returned by the Chorus Pro service : {}", err.getMessage());
                    throw new ExternalException(err.getMessage());
                })
                .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT)
                .orElseThrow(() -> {
                    log.error("verifySignature - an error occurred while requesting Chorus Pro");
                    return new ExternalException(message.getString("message.cannot_reach_chorus_pro_service"));
                });
    }
}
