/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.signatureValidation;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import coop.libriciel.ipcore.services.secret.SecretServiceInterface;
import org.springframework.web.reactive.function.client.WebClient;

@Log4j2
@Service(SignatureValidationServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = SignatureValidationServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = "chorusProProd")
public class ChorusProProdService extends ChorusProAbstractService {

    private static final String BASE_URL_VALUE = "https://";

    public ChorusProProdService(SecretServiceInterface secretServiceInterface,
                                WebClient.Builder webClientBuilder) {
        super(secretServiceInterface, webClientBuilder);
    }


    @Override
    public void setBaseUrl() {
        super.TOKEN_URL_VALUE = BASE_URL_VALUE + END_TOKEN_URL_VALUE;
        super.REPORT_URL_VALUE = BASE_URL_VALUE + END_REPORT_URL_VALUE;
    }
}
