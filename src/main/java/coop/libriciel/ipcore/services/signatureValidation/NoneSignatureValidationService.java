/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.signatureValidation;

import coop.libriciel.ipcore.model.signatureValidation.ValidationServiceConfiguration;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.io.Serial;
import java.util.Collection;
import java.util.Map;

import static coop.libriciel.ipcore.utils.TextUtils.NONE_SERVICE;
import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;

@Service(SignatureValidationServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = SignatureValidationServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = NONE_SERVICE)
public class NoneSignatureValidationService implements SignatureValidationServiceInterface {

    private static class NoSignatureValidationServiceError extends LocalizedStatusException {

        private static final @Serial long serialVersionUID = -9220649532638837834L;

        public NoSignatureValidationServiceError() {
            super(SERVICE_UNAVAILABLE, "message.signature_validation_service_not_available");
        }
    }


    @Override
    public Map<String, String> getSignatureValidation(DocumentBuffer document, Collection<DocumentBuffer> originalFiles) {
        throw new NoSignatureValidationServiceError();
    }


    @Override
    public void testConfiguration(ValidationServiceConfiguration validationServiceConfiguration) {
        throw new NoSignatureValidationServiceError();
    }


    @Override
    public ValidationServiceConfiguration getSignatureValidationConfiguration() {
        return null;
    }
}
