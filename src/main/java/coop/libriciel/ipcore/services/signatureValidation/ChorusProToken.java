/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.signatureValidation;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;
import org.apache.commons.lang3.time.DateUtils;

import java.util.Date;

@Data
public class ChorusProToken {


    private @JsonAlias("access_token") String accessToken;
    private @JsonAlias("token_type") String tokenType;
    private @JsonAlias("expires_in") int expiresIn;
    private String scope;
    private Date expirationTime;


    protected void setExpiresIn(int expiresIn) {
        this.expiresIn = expiresIn;
        setExpirationTime(expiresIn);
    }


    private void setExpirationTime(int expiresIn) {
        expirationTime = DateUtils.addSeconds(new Date(), expiresIn);
    }
}
