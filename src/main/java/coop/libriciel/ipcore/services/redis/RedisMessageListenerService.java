/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.redis;

import com.fasterxml.jackson.databind.ObjectMapper;
import coop.libriciel.ipcore.business.workflow.WorkflowBusinessService;
import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.externalsignature.ExternalSignatureParams;
import coop.libriciel.ipcore.model.externalsignature.SignRequestMember;
import coop.libriciel.ipcore.model.mail.MailNotification;
import coop.libriciel.ipcore.model.workflow.Action;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.model.workflow.ipWorkflow.IpWorkflowInstance;
import coop.libriciel.ipcore.model.workflow.ipWorkflow.NotifiedIpWorkflowTask;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.database.SubtypeRepository;
import coop.libriciel.ipcore.services.mail.NotificationServiceInterface;
import coop.libriciel.ipcore.services.websocket.WebsocketService;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.*;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static coop.libriciel.ipcore.model.externalsignature.ExternalSignatureParams.*;
import static coop.libriciel.ipcore.model.workflow.Action.*;
import static coop.libriciel.ipcore.model.workflow.NotificationType.NEW_ON_DESK;
import static coop.libriciel.ipcore.model.workflow.State.*;
import static coop.libriciel.ipcore.utils.CollectionUtils.toMutableSet;
import static coop.libriciel.ipcore.utils.UserUtils.AUTOMATIC_TASK_USER_ID;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.singletonList;
import static org.springframework.http.HttpStatus.NOT_FOUND;


@Log4j2
@Service
public class RedisMessageListenerService implements MessageListener {


    // <editor-fold desc="Beans">


    private final AuthServiceInterface authService;
    private final ContentServiceInterface contentService;
    private final NotificationServiceInterface notificationService;
    private final TransactionTemplate transactionTemplate;
    private final SubtypeRepository subtypeRepository;
    private final WebsocketService websocketService;
    private final WorkflowBusinessService workflowBusinessService;
    private final WorkflowServiceInterface workflowService;


    public RedisMessageListenerService(AuthServiceInterface authService,
                                       ContentServiceInterface contentService,
                                       NotificationServiceInterface notificationService,
                                       PlatformTransactionManager platformTransactionManager,
                                       SubtypeRepository subtypeRepository,
                                       WebsocketService websocketService,
                                       WorkflowBusinessService workflowBusinessService,
                                       WorkflowServiceInterface workflowService) {
        this.authService = authService;
        this.contentService = contentService;
        this.notificationService = notificationService;
        this.transactionTemplate = new TransactionTemplate(platformTransactionManager);
        this.subtypeRepository = subtypeRepository;
        this.websocketService = websocketService;
        this.workflowBusinessService = workflowBusinessService;
        this.workflowService = workflowService;
    }


    // </editor-fold desc="Beans">


    @Override
    public void onMessage(Message message, byte[] pattern) {

        Folder folder;
        NotifiedIpWorkflowTask task;

        try {
            String messageString = new String(message.getBody(), UTF_8);
            NotifiedIpWorkflowTask ipWorkflowTask = new ObjectMapper().readValue(messageString, NotifiedIpWorkflowTask.class);
            folder = new IpWorkflowInstance(ipWorkflowTask);
            task = ipWorkflowTask;
        } catch (IOException e) {
            log.warn("Something went wrong parsing mail notification", e);
            return;
        }

        // We need to give a little delay to Workflow to build up the task.
        try {Thread.sleep(5000);} catch (InterruptedException ie) { /* Not Used */ }

        log.debug("Message received folder:{}", folder);

        Subtype subtype = subtypeRepository
                .findById(folder.getSubtype().getId())
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_subtype_id"));

        Tenant tenant = subtype.getTenant();

        this.sendInternalNotification(tenant, task);

        log.debug("onMessage - folder state : {}", folder.getState());
        if (folder.getState() == DRAFT && task.getNotificationType() == NEW_ON_DESK) {
            log.debug("ignoring 'new draft' folder notification");
            return;
        }

        // Tests for auto SEAL or EXTERNAL_SIGNATURE

        boolean isExternalSignature = task.getAction() == EXTERNAL_SIGNATURE;
        boolean canPerformExternalSignature = subtype.isExternalSignatureAutomatic();
        boolean doPerformAutoExternalSignature = isExternalSignature && canPerformExternalSignature;

        boolean isSeal = task.getAction() == SEAL;
        boolean isNotValidated = task.getState() != VALIDATED;
        boolean isSealAutomatic = subtype.isSealAutomatic();
        boolean doPerformAutoSeal = isSeal && isNotValidated && isSealAutomatic;

        if (doPerformAutoExternalSignature || doPerformAutoSeal) {

            // Content id is absent from the Redis message, we have to fetch the folder again
            folder = workflowService.getFolder(folder.getId(), tenant.getId(), true);
            contentService.populateFolderWithAllDocumentTypes(folder);

            Desk validatorDesk = task.getDesks().stream()
                    .findFirst()
                    .map(DeskRepresentation::getId)
                    .map(id -> authService.findDeskByIdNoException(tenant.getId(), id))
                    .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_desk_id"));

            if (doPerformAutoSeal) {

                final Folder finalFolder = folder;
                // The transaction prevent a LazyLoadingException (required since we are not in the context of a request session)
                transactionTemplate.execute((TransactionCallback<Void>) transactionStatus -> {
                    workflowBusinessService.performRetryableAutoSeal(tenant, validatorDesk, finalFolder, task);
                    return null;
                });

                // We don't want to notify anyone here,
                // since the action was already done.

            } else { // Auto external signature

                // Check appropriate metadata presence

                long externalSignatureCount = FolderUtils.countStepsWithAction(folder.getStepList(), EXTERNAL_SIGNATURE);
                Map<Long, SignRequestMember> signRequestMemberMap = CryptoUtils.getExternalSignatureMetadataMap(folder.getMetadata());

                boolean doesHaveDefaultIndex = signRequestMemberMap.containsKey(0L);
                boolean doesHaveFirstIndex = signRequestMemberMap.containsKey(1L);
                boolean leapsIndex = !CollectionUtils.isSequential(signRequestMemberMap.keySet());
                boolean incorrectIndexSequence = leapsIndex || !doesHaveFirstIndex;
                if (incorrectIndexSequence && !doesHaveDefaultIndex){
                    log.warn("External signature tag indexes have missing values. Cancelling...");
                    return;
                }

                if (signRequestMemberMap.size() == externalSignatureCount) {
                    log.debug("External signature configurations matches the total step count. Automatically set one per step.");

                    Long currentExternalSignatureIndex = FolderUtils.getCurrentExternalSignatureIndex(folder.getStepList());
                    SignRequestMember currentMemberConfiguration = Optional
                            .ofNullable(signRequestMemberMap.get(currentExternalSignatureIndex))
                            .orElse(signRequestMemberMap.get(0L));

                    if (currentMemberConfiguration == null) {
                        log.warn("No external signature found for the current step. Cancelling...");
                        return;
                    }
                    if (!CryptoUtils.isConfigurationComplete(currentMemberConfiguration)) {
                        log.warn("Incomplete external signature configuration. Cancelling...");
                        return;
                    }

                    ExternalSignatureParams externalSignatureParams =  new ExternalSignatureParams(
                            folder.getName(),
                            task.getId(),
                            singletonList(currentMemberConfiguration),
                            new HashMap<>(),
                            new ArrayList<>()
                    );
                    workflowBusinessService.performRetryableAutoExternalSignature(tenant, validatorDesk, folder, task, externalSignatureParams);

                } else if (externalSignatureCount == 1) {
                    log.debug("External signature configurations mapped to a single step. Every conf to the single step.");

                    if (signRequestMemberMap.values().stream().anyMatch(conf -> !CryptoUtils.isConfigurationComplete(conf))) {
                        log.warn("One of the external signature configuration is incomplete. Cancelling...");
                        return;
                    }

                    ExternalSignatureParams externalSignatureParams =  new ExternalSignatureParams(
                            folder.getName(),
                            task.getId(),
                            signRequestMemberMap.values(),
                            new HashMap<>(),
                            new ArrayList<>()
                    );
                    workflowBusinessService.performRetryableAutoExternalSignature(tenant, validatorDesk, folder, task, externalSignatureParams);

                } else {
                    log.warn("External signature configuration mismatch: {} conf(s) for {} steps. Cancelling...");
                    return;
                }

                // We don't want to notify anyone here,
                // since the action was already done.
            }

            return;
        }

        notify(tenant, folder, task);
    }


    private void notify(Tenant tenant, Folder folder, NotifiedIpWorkflowTask task) {

        boolean isReject = task.getPerformedAction() == REJECT;

        if (task.getPerformedAction() == START) {
            return;
        }

        Task populatedCurrentTask = this.getPopulatedCurrentTask(tenant.getId(), folder.getId(), isReject);

        User sender = Optional.ofNullable(populatedCurrentTask)
                .map(Task::getUser)
                .map(u -> StringUtils.equals(u.getId(), AUTOMATIC_TASK_USER_ID)
                          ? UserUtils.getAutomaticUser()
                          : authService.findUserById(u.getId())
                )
                .orElse(null);

        Action performedAction = isReject ? REJECT : task.getAction();
        MailNotification mailNotification = new MailNotification();
        mailNotification.setInstanceId(folder.getId());
        mailNotification.setInstanceName(folder.getName());
        mailNotification.setTenantId(tenant.getId());
        mailNotification.setTaskAction(performedAction);
        mailNotification.setNotificationType(task.getNotificationType());
        mailNotification.setTaskId(
                Optional.ofNullable(task.getId())
                        .orElse(Optional.ofNullable(populatedCurrentTask)
                                .map(Task::getId)
                                .orElse(null)
                        )
        );
        if (sender != null) {
            mailNotification.setTaskUserId(sender.getId());
            mailNotification.setTaskUserFirstName(sender.getFirstName());
            mailNotification.setTaskUserLastName(sender.getLastName());
        }

        Set<String> directDesksToNotify = task
                .getDesks()
                .stream()
                .map(DeskRepresentation::getId)
                .collect(toMutableSet());

        mailNotification.setDeskIdsToNotify(directDesksToNotify);

        HashSet<String> additionalDesksToNotify = task
                .getNotifiedDesks()
                .stream()
                .map(DeskRepresentation::getId)
                .collect(toMutableSet());

        mailNotification.setAdditionalDeskIdsToNotify(additionalDesksToNotify);

        notificationService.processNotification(mailNotification, sender);
    }


    private void sendInternalNotification(Tenant tenant, NotifiedIpWorkflowTask task) {
        Set<String> directDesksToNotify = task
                .getDesks()
                .stream()
                .map(DeskRepresentation::getId)
                .collect(toMutableSet());

        Map<String, String> deskToNotifByDelegation = notificationService.getDesksToNotifyByDelegation(tenant.getId(), directDesksToNotify);
        directDesksToNotify.addAll(deskToNotifByDelegation.keySet());

        directDesksToNotify.forEach(deskId -> websocketService.sendNewCountsForDesk(tenant.getId(), deskId));
    }


    private Task getPopulatedCurrentTask(String tenantId, String folderId, boolean lastTaskWasRejected) {

        Folder populatedFolder = this.workflowService.getFolder(folderId, tenantId, true);
        List<Task> validatedTasks = populatedFolder.getStepList().stream()
                .filter(t -> t.getState().equals(lastTaskWasRejected ? REJECTED : VALIDATED))
                .toList();

        Task task = null;
        if (!validatedTasks.isEmpty()) {
            task = validatedTasks
                    .stream()
                    .skip(validatedTasks.size() - 1L)
                    .findFirst()
                    .orElse(null);
        }

        return task;
    }


}
