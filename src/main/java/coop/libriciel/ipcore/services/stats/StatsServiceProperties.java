/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
package coop.libriciel.ipcore.services.stats;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.HashMap;
import java.util.Map;


@Data
@AllArgsConstructor
@ConfigurationProperties(prefix = "services.stats")
public class StatsServiceProperties {


    private String provider;
    private String scheme;
    private String url;
    private int port;
    private String httpMethod;
    private QueryPath adminAction;
    private QueryPath folderAction;
    private Map<String, String> valuesSubstitutions;
    private String token;


    @Data
    @NoArgsConstructor
    public static class QueryPath {

        private String path;
        private Map<String, String> queryParams = new HashMap<>();

    }


}
