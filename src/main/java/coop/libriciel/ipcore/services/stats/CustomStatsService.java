/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.stats;

import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.database.TypologyEntity;
import coop.libriciel.ipcore.model.stats.StatsCategory;
import coop.libriciel.ipcore.model.workflow.Action;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.utils.ForcedExceptionsConnector;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.text.StringSubstitutor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatusCode;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.*;

import static coop.libriciel.ipcore.model.stats.StatsCategory.FOLDER;
import static coop.libriciel.ipcore.utils.RequestUtils.SUBSERVICE_REQUEST_SHORT_TIMEOUT;
import static coop.libriciel.ipcore.utils.TextUtils.INTERNAL_PREFIX;
import static coop.libriciel.ipcore.utils.TextUtils.MESSAGE_BUNDLE;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.emptyMap;
import static jakarta.ws.rs.HttpMethod.POST;
import static jakarta.ws.rs.HttpMethod.PUT;
import static java.util.Comparator.naturalOrder;
import static java.util.Comparator.nullsFirst;
import static java.util.Locale.ROOT;
import static java.util.Map.Entry.comparingByKey;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import static org.apache.commons.lang3.StringUtils.EMPTY;


@Data
@Log4j2
@Service(StatsServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = StatsServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = "custom")
public class CustomStatsService implements StatsServiceInterface {

    // Spring/SpringBoot doesn't allow to escape the "$" sign in the `application.properties`.
    // Ugly hacks do exists, but it feels more elegant to simply override the default character.
    private static final String PLACEHOLDER_PREFIX = "@{";
    private static final String PLACEHOLDER_SUFFIX = "}";

    private static final String PLACEHOLDER_RANDOM_NUMBER = INTERNAL_PREFIX + "random_number";
    private static final String PLACEHOLDER_CATEGORY = INTERNAL_PREFIX + "category";
    private static final String PLACEHOLDER_ACTION = INTERNAL_PREFIX + "action";
    private static final String PLACEHOLDER_ACTION_NAME = INTERNAL_PREFIX + "action_name";
    private static final String PLACEHOLDER_TARGET = INTERNAL_PREFIX + "target";
    private static final String PLACEHOLDER_TIME_TO_COMPLETE_IN_HOURS = INTERNAL_PREFIX + "time_to_complete_in_hours";
    private static final String PLACEHOLDER_TENANT_ID = INTERNAL_PREFIX + "tenant_id";
    private static final String PLACEHOLDER_TENANT_NAME = INTERNAL_PREFIX + "tenant_name";
    private static final String PLACEHOLDER_DESK_ID = INTERNAL_PREFIX + "desk_id";
    private static final String PLACEHOLDER_DESK_NAME = INTERNAL_PREFIX + "desk_name";
    private static final String PLACEHOLDER_FOLDER_ID = INTERNAL_PREFIX + "folder_id";
    private static final String PLACEHOLDER_FOLDER_NAME = INTERNAL_PREFIX + "folder_name";
    private static final String PLACEHOLDER_TYPE_ID = INTERNAL_PREFIX + "type_id";
    private static final String PLACEHOLDER_TYPE_NAME = INTERNAL_PREFIX + "type_name";
    private static final String PLACEHOLDER_SUBTYPE_ID = INTERNAL_PREFIX + "subtype_id";
    private static final String PLACEHOLDER_SUBTYPE_NAME = INTERNAL_PREFIX + "subtype_name";


    // <editor-fold desc="Beans">


    private final StatsServiceProperties properties;


    public CustomStatsService(StatsServiceProperties properties) {
        this.properties = properties;
    }


    // </editor-fold desc="Beans">


    // <editor-fold desc="Registering actions">


    @Override
    public void registerFolderAction(@NotNull Tenant tenant, @NotNull Action action, @NotNull Folder folder, @NotNull DeskRepresentation deskEntity,
                                     @Nullable Long timeToCompleteInHours) {

        String folderCategoryName = ResourceBundle.getBundle(MESSAGE_BUNDLE, Locale.getDefault()).getString(FOLDER.getMessageKey());
        String actionName = ResourceBundle.getBundle(MESSAGE_BUNDLE, Locale.getDefault()).getString(action.getMessageKey());

        // Building the substitutes map

        Map<String, String> placeholderSubstitutes = new HashMap<>(folder.getMetadata());

        placeholderSubstitutes.put(PLACEHOLDER_RANDOM_NUMBER, String.valueOf(new Random().nextInt(1000000)));
        placeholderSubstitutes.put(PLACEHOLDER_CATEGORY, folderCategoryName);
        placeholderSubstitutes.put(PLACEHOLDER_ACTION, action.toString());
        placeholderSubstitutes.put(PLACEHOLDER_ACTION_NAME, actionName);
        placeholderSubstitutes.put(PLACEHOLDER_TARGET, folder.getId());
        placeholderSubstitutes.put(PLACEHOLDER_TENANT_ID, tenant.getId());
        placeholderSubstitutes.put(PLACEHOLDER_TENANT_NAME, tenant.getName());

        placeholderSubstitutes.put(PLACEHOLDER_TIME_TO_COMPLETE_IN_HOURS, Optional.ofNullable(timeToCompleteInHours).map(String::valueOf).orElse(EMPTY));
        placeholderSubstitutes.put(PLACEHOLDER_DESK_ID, deskEntity.getId());
        placeholderSubstitutes.put(PLACEHOLDER_DESK_NAME, deskEntity.getName());
        placeholderSubstitutes.put(PLACEHOLDER_FOLDER_ID, folder.getId());
        placeholderSubstitutes.put(PLACEHOLDER_FOLDER_NAME, folder.getName());
        placeholderSubstitutes.put(PLACEHOLDER_TYPE_ID, Optional.ofNullable(folder.getType()).map(TypologyEntity::getId).orElse(EMPTY));
        placeholderSubstitutes.put(PLACEHOLDER_TYPE_NAME, Optional.ofNullable(folder.getType()).map(TypologyEntity::getName).orElse(EMPTY));
        placeholderSubstitutes.put(PLACEHOLDER_SUBTYPE_ID, Optional.ofNullable(folder.getSubtype()).map(TypologyEntity::getId).orElse(EMPTY));
        placeholderSubstitutes.put(PLACEHOLDER_SUBTYPE_NAME, Optional.ofNullable(folder.getSubtype()).map(TypologyEntity::getName).orElse(EMPTY));

        Map<String, String> substitutions = firstNonNull(properties.getValuesSubstitutions(), emptyMap());
        placeholderSubstitutes.entrySet()
                .stream()
                .filter(entry -> substitutions.containsKey(entry.getValue()))
                .forEach(entry -> placeholderSubstitutes.put(entry.getKey(), substitutions.get(entry.getValue())));

        StringSubstitutor substitutor = new StringSubstitutor(placeholderSubstitutes, PLACEHOLDER_PREFIX, PLACEHOLDER_SUFFIX);

        // Building the URL

        UriComponentsBuilder requestUriBuilder = UriComponentsBuilder.newInstance()
                .encode(UTF_8)
                .scheme(properties.getScheme())
                .host(properties.getUrl())
                .port(properties.getPort())
                .path(properties.getFolderAction().getPath());

        properties.getFolderAction()
                .getQueryParams()
                .entrySet()
                .stream()
                .sorted(comparingByKey(nullsFirst(naturalOrder())))  // Sorting alphabetically to ease unit-tests
                .forEach(entry -> requestUriBuilder.queryParam(
                        substitutor.replace(entry.getKey()),
                        substitutor.replace(entry.getValue())
                ));

        // Sending the result

        sendRequest(requestUriBuilder.build().normalize().toUri());
    }


    @Override
    public void registerAdminAction(@Nullable Tenant tenant, @NotNull StatsCategory category, @NotNull Action action, @NotNull String target) {

        String actionName = ResourceBundle.getBundle(MESSAGE_BUNDLE, Locale.getDefault()).getString(action.getMessageKey());

        // Building the substitutes map

        Map<String, String> placeholderSubstitutes = new HashMap<>();
        placeholderSubstitutes.put(PLACEHOLDER_RANDOM_NUMBER, String.valueOf(new Random().nextInt(1000000)));
        placeholderSubstitutes.put(PLACEHOLDER_CATEGORY, category.name());
        placeholderSubstitutes.put(PLACEHOLDER_ACTION, action.toString());
        placeholderSubstitutes.put(PLACEHOLDER_ACTION_NAME, actionName);
        placeholderSubstitutes.put(PLACEHOLDER_TARGET, target);

        if (tenant != null) {
            placeholderSubstitutes.put(PLACEHOLDER_TENANT_ID, tenant.getId());
            placeholderSubstitutes.put(PLACEHOLDER_TENANT_NAME, tenant.getName());
        }

        Map<String, String> substitutions = firstNonNull(properties.getValuesSubstitutions(), emptyMap());
        placeholderSubstitutes.entrySet()
                .stream()
                .filter(entry -> substitutions.containsKey(entry.getValue()))
                .forEach(entry -> placeholderSubstitutes.put(entry.getKey(), substitutions.get(entry.getValue())));

        StringSubstitutor substitutor = new StringSubstitutor(placeholderSubstitutes, PLACEHOLDER_PREFIX, PLACEHOLDER_SUFFIX);

        // Building the URL

        UriComponentsBuilder requestUriBuilder = UriComponentsBuilder.newInstance()
                .scheme(properties.getScheme())
                .host(properties.getUrl())
                .port(properties.getPort())
                .path(properties.getAdminAction().getPath());

        properties.getAdminAction()
                .getQueryParams()
                .entrySet()
                .stream()
                .sorted(comparingByKey(nullsFirst(naturalOrder())))  // Sorting alphabetically to ease unit-tests
                .forEach(entry -> requestUriBuilder.queryParam(
                        substitutor.replace(entry.getKey()),
                        substitutor.replace(entry.getValue())
                ));

        // Sending the result

        sendRequest(requestUriBuilder.build().normalize().toUri());
    }


    private void sendRequest(URI requestUri) {
        log.debug("Custom stat event, action:{} on URI:{}", properties.getHttpMethod(), requestUri);

        WebClient webClient = WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build();
        WebClient.RequestHeadersUriSpec<?> request = switch (properties.getHttpMethod().toUpperCase(ROOT)) {
            case POST -> webClient.post();
            case PUT -> webClient.put();
            default -> webClient.get();
        };

        request.uri(requestUri)
                .exchangeToMono(clientResponse -> Mono.just(clientResponse.statusCode()))
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .filter(HttpStatusCode::is2xxSuccessful)
                .orElseGet(() -> {
                    log.warn(ResourceBundle.getBundle(MESSAGE_BUNDLE, ROOT).getString("message.cannot_reach_stats_service"));
                    return null;
                });
    }


    // </editor-fold desc="Registering actions">


}
