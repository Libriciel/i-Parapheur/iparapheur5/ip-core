/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
package coop.libriciel.ipcore.services.stats;

import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.stats.StatsCategory;
import coop.libriciel.ipcore.model.workflow.Action;
import coop.libriciel.ipcore.model.workflow.Folder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.UUID;


@Service(StatsServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = StatsServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = "none")
public class NoneStatsService implements StatsServiceInterface {


    // <editor-fold desc="Registering actions">


    @Override
    public void registerFolderAction(@NotNull Tenant tenant, @NotNull Action action, @NotNull Folder folder, @NotNull DeskRepresentation deskEntity,
                                     @Nullable Long timeToCompleteInHours) {}


    @Override
    public void registerAdminAction(@Nullable Tenant tenant, @NotNull StatsCategory category, @NotNull Action action, @NotNull String target) {}


    // </editor-fold desc="Registering actions">


}
