/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.permission;

import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.permission.Delegation;
import coop.libriciel.ipcore.model.workflow.DelegationRule;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.io.Serial;
import java.util.*;

import static coop.libriciel.ipcore.utils.TextUtils.NONE_SERVICE;
import static java.util.Collections.emptySet;
import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;


@Service(PermissionServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = PermissionServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = NONE_SERVICE)
public class NonePermissionService implements PermissionServiceInterface {


    private static class NoPermissionServiceError extends LocalizedStatusException {

        private static final @Serial long serialVersionUID = -3026283572919934856L;


        public NoPermissionServiceError() {
            super(SERVICE_UNAVAILABLE, "message.permission_service_not_available");
        }

    }


    @Override
    public void deletePermission(@NotNull String resourceId) {
        throw new NoPermissionServiceError();
    }


    @Override
    public void createDeskResource(@NotNull String tenantId, @NotNull Desk desk) {
        throw new NoPermissionServiceError();
    }


    @Override
    public void retrieveAndRefreshDeskPermissions(@NotNull String tenantId, @NotNull Desk desk) {
        throw new NoPermissionServiceError();
    }


    @Override
    public void editDeskPermission(@NotNull String tenantId, @NotNull Desk desk) {
        throw new NoPermissionServiceError();
    }


    @Override
    public boolean currentUserHasDirectViewingRightOnSomeDeskIn(@NotNull String tenantId,
                                                                @NotNull Set<String> deskIds,
                                                                @Nullable String typeId,
                                                                @Nullable String subtypeId) {
        throw new NoPermissionServiceError();
    }


    @Override
    public boolean currentUserHasViewingRightOnSomeDeskIn(@NotNull String tenantId,
                                                          @NotNull Set<String> deskIds,
                                                          @Nullable String typeId,
                                                          @Nullable String subtypeId) {
        throw new NoPermissionServiceError();
    }


    @Override
    public boolean currentUserHasArchivingRightOnSomeDeskIn(@NotNull String tenantId,
                                                            @NotNull Set<String> deskIds,
                                                            @Nullable String typeId,
                                                            @Nullable String subtypeId) {
        throw new NoPermissionServiceError();
    }


    @Override
    public boolean currentUserHasChainingRightOnSomeDeskIn(@NotNull String tenantId,
                                                           @NotNull Set<String> deskIds,
                                                           @Nullable String typeId,
                                                           @Nullable String subtypeId) {
        throw new NoPermissionServiceError();
    }


    @Override
    public boolean currentUserHasFolderCreationRightOnSomeDeskIn(@NotNull String tenantId, @NotNull Set<String> deskIds) {
        throw new NoPermissionServiceError();
    }


    @Override
    public boolean currentUserHasAdminRightOnDesk(@NotNull String tenantId, @NotNull String deskIds) {
        throw new NoPermissionServiceError();
    }


    @Override
    public boolean currentUserHasAdminRightsOnTenant(@NotNull String tenantId) {
        throw new NoPermissionServiceError();
    }


    @Override
    public boolean currentUserHasFolderActionRightOnSomeDeskIn(@NotNull String tenantId,
                                                               @NotNull Set<String> deskIds,
                                                               @Nullable String typeId,
                                                               @Nullable String subtypeId) {
        throw new NoPermissionServiceError();
    }


    @Override
    public boolean currentUserHasDelegationOnSomeDeskIn(@NotNull String tenantId,
                                                        @NotNull Set<String> deskIds,
                                                        @Nullable String typeId,
                                                        @Nullable String subtypeId) {
        throw new NoPermissionServiceError();
    }


    @Override
    public @NotNull Map<String, Set<DelegationRule>> getActiveDelegationsToDesksForCurrentUser(@NotNull Set<String> deskIds) {
        throw new NoPermissionServiceError();
    }


    @Override
    public @NotNull Map<String, Set<DelegationRule>> getActiveDelegationsForDesks(@NotNull String tenantId, @NotNull Set<String> deskIds) {
        throw new NoPermissionServiceError();
    }


    @Override
    public void createMetadataResource(@NotNull String tenantId, @NotNull String metadataId) {
        throw new NoPermissionServiceError();
    }


    @Override
    public @NotNull List<String> getCurrentUserViewableDeskIds() {
        throw new NoPermissionServiceError();
    }

    
    @Override
    public @NotNull Set<String> getAllDesksHavingDelegationFrom(@NotNull String tenantId, @NotNull Set<String> deskIds) {
        return emptySet();
    }


    @Override
    public @NotNull List<DeskRepresentation> getAllDesksHavingDelegationTo(@NotNull Set<String> deskIds) {
        return Collections.emptyList();
    }


    @Override
    public void createSubtypeResource(@NotNull String tenantId, @NotNull String subtypeId) {
        throw new NoPermissionServiceError();
    }


    @Override
    public void addDelegation(@NotNull String tenantId,
                              @NotNull String substituteDesk,
                              @NotNull String delegatingDesk,
                              @Nullable String typeId,
                              @Nullable String subtypeId,
                              @Nullable Date beginDate,
                              @Nullable Date endDate) {
        throw new NoPermissionServiceError();
    }


    @Override
    public @Nullable Delegation getDelegation(@NotNull String deskId, @NotNull String id) {
        return null;
    }


    @Override
    public void deleteDelegation(@NotNull String delegationId,
                                 @NotNull String tenantId,
                                 @NotNull String substituteDeskId,
                                 @NotNull String delegatingDeskId,
                                 @Nullable String typeId,
                                 @Nullable String subtypeId,
                                 @Nullable Date beginDate,
                                 @Nullable Date endDate) {
        throw new NoPermissionServiceError();
    }


    @Override
    public @NotNull List<Delegation> getDelegations(@NotNull String tenantId, @NotNull String delegatingDeskId) {
        throw new NoPermissionServiceError();
    }


    @Override
    public @NotNull List<DeskRepresentation> getDelegatingDesks(@NotNull String targetDesk) {
        throw new NoPermissionServiceError();
    }


    @Override
    public void setMetadataDelegations(@NotNull String deskId, @NotNull Collection<DelegationRule> metadataDelegations) {
        throw new NoPermissionServiceError();
    }


    @Override
    public void addSupervisorsToDesk(@NotNull String tenantId, @NotNull String targetDeskId, @NotNull Collection<String> userIds) {
        throw new NoPermissionServiceError();
    }


    @Override
    public void removeSupervisorsFromDesk(@NotNull String tenantId, @NotNull String targetDeskId, @NotNull Collection<String> userIds) {
        throw new NoPermissionServiceError();
    }


    @Override
    public void removeAllSupervisedDesks(@NotNull String tenantId, @NotNull String userId) {
        throw new NoPermissionServiceError();
    }


    @Override
    public @NotNull List<String> getSupervisorIdsFromDesk(@NotNull String tenantId, @NotNull String deskId) {
        throw new NoPermissionServiceError();
    }


    @Override
    public @NotNull List<DeskRepresentation> getSupervisedDesks(@NotNull String userId, @Nullable String tenantId) {
        throw new NoPermissionServiceError();
    }


    @Override
    public void addDelegationManagersToDesk(@NotNull String tenantId, @NotNull String targetDeskId, @NotNull Collection<String> userIds) {
        throw new NoPermissionServiceError();
    }


    @Override
    public void removeDelegationManagersFromDesk(@NotNull String tenantId, @NotNull String sourceDeskId, @NotNull Collection<String> userIds) {
        throw new NoPermissionServiceError();
    }


    @Override
    public void removeAllDelegationManagedDesks(@NotNull String tenantId, @NotNull String userId) {
        throw new NoPermissionServiceError();
    }


    @Override
    public @NotNull List<String> getDelegationManagerIdsFromDesk(@NotNull String tenantId, @NotNull String deskId) {
        throw new NoPermissionServiceError();
    }


    @Override
    public @NotNull List<DeskRepresentation> getDelegationManagedDesks(@NotNull String userId, @Nullable String tenantId) {
        throw new NoPermissionServiceError();
    }


    @Override
    public void setFunctionalAdmin(@NotNull String tenantId, @NotNull String targetDeskId, @NotNull String userId) {
        throw new NoPermissionServiceError();
    }


    @Override
    public void removeFunctionalAdmin(@NotNull String tenantId, @NotNull String sourceDeskId, @NotNull String userId) {
        throw new NoPermissionServiceError();
    }


    @Override
    public void removeAllAdministeredDesks(@NotNull String tenantId, @NotNull String userId) {
        throw new NoPermissionServiceError();
    }


    @Override
    public @NotNull List<DeskRepresentation> getAdministeredDesks(@NotNull String userId, @Nullable String tenantId) {
        throw new NoPermissionServiceError();
    }


    @Override
    public void associateDesks(@NotNull String tenantId, @NotNull String sourceDeskId, @NotNull Collection<String> associateDesks) {
        throw new NoPermissionServiceError();
    }


    @Override
    public void removeAssociatedDesks(@NotNull String tenantId, @NotNull String sourceDeskId, @NotNull Collection<String> associateDesksIds) {
        throw new NoPermissionServiceError();
    }


    @Override
    public @NotNull List<String> getAssociatedDeskIds(@NotNull String tenantId, @NotNull String sourceDeskId) {
        throw new NoPermissionServiceError();
    }


    @Override
    public void addFilterableMetadata(@NotNull String tenantId, @NotNull String sourceDeskId, @NotNull Collection<String> metadataIds) {
        throw new NoPermissionServiceError();
    }


    @Override
    public void removeFilterableMetadata(@NotNull String tenantId, @NotNull String sourceDeskId, @NotNull Collection<String> metadataIds) {
        throw new NoPermissionServiceError();
    }


    @Override
    public @NotNull List<String> getFilterableMetadataIds(@NotNull String tenantId, @NotNull String sourceDeskId) {
        throw new NoPermissionServiceError();
    }


    @Override
    public void setSubtypeCreationPermittedDeskIds(@NotNull String tenantId, @NotNull String subtypeId, @NotNull Collection<String> deskIds) {
        throw new NoPermissionServiceError();
    }


    @Override
    public @Nullable List<String> getSubtypePermittedDeskIds(@NotNull String tenantId, @NotNull String subtypeId) {
        throw new NoPermissionServiceError();
    }


    @Override
    public @NotNull List<String> getAllowedSubtypeIds(@NotNull String tenantId, @NotNull String sourceDeskId) {
        throw new NoPermissionServiceError();
    }


    @Override
    public void setSubtypeFilterableByDeskIds(@NotNull String tenantId, @NotNull String subtypeId, @NotNull Collection<String> deskIds) {
        throw new NoPermissionServiceError();
    }


    @Override
    public @Nullable List<String> getSubtypeFilterableByDeskIds(@NotNull String tenantId, @NotNull String subtypeId) {
        throw new NoPermissionServiceError();
    }


    @Override
    public @NotNull List<String> getFilterableSubtypeIds(@NotNull String tenantId, @NotNull String sourceDeskId) {
        throw new NoPermissionServiceError();
    }


}
