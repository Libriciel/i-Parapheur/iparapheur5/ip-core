/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.permission;

import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.permission.Delegation;
import coop.libriciel.ipcore.model.workflow.DelegationRule;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;


public interface PermissionServiceInterface {


    String BEAN_NAME = "permissionService";
    String PREFERENCES_PROVIDER_KEY = "services.permission.provider";

    int MAX_DELEGATION_PER_DESK = 50;
    int MAX_ASSOCIATED_DESK_PER_DESK = 50;
    int MAX_FUNCTIONAL_ADMIN_PER_DESK = 50;
    int MAX_SUPERVISOR_PER_DESK = 50;
    int MAX_DELEGATION_MANAGER_PER_DESK = 50;
    int MAX_FILTERABLE_METADATA_PER_DESK = 50;
    int MAX_FILTERABLE_SUBTYPE_PER_DESK = 250;
    int MAX_USABLE_SUBTYPE_PER_DESK = 250;

    String URN_TEMPLATE_3 = "urn:%s:%s:%s";
    String NAMESPACE = "ipcore";
    String SCOPES = "scopes";
    String RESOURCES = "resources";
    String TYPE = "type";
    String SUBTYPE = "subtype";

    String SCOPE_VIEW = URN_TEMPLATE_3.formatted(NAMESPACE, SCOPES, "view");
    String SCOPE_USE = URN_TEMPLATE_3.formatted(NAMESPACE, SCOPES, "use");
    String SCOPE_PUBLIC_USE = URN_TEMPLATE_3.formatted(NAMESPACE, SCOPES, "public_use");
    String SCOPE_FILTER = URN_TEMPLATE_3.formatted(NAMESPACE, SCOPES, "filter");
    String SCOPE_PUBLIC_FILTER = URN_TEMPLATE_3.formatted(NAMESPACE, SCOPES, "public_filter");
    String SCOPE_FOLDER_CREATION = URN_TEMPLATE_3.formatted(NAMESPACE, SCOPES, "folder_creation");
    String SCOPE_FOLDER_ACTION = URN_TEMPLATE_3.formatted(NAMESPACE, SCOPES, "folder_action");
    String SCOPE_FOLDER_ARCHIVE = URN_TEMPLATE_3.formatted(NAMESPACE, SCOPES, "folder_archive");
    String SCOPE_FOLDER_CHAIN = URN_TEMPLATE_3.formatted(NAMESPACE, SCOPES, "folder_chain");
    String SCOPE_DESK_ASSOCIATION = URN_TEMPLATE_3.formatted(NAMESPACE, SCOPES, "association");
    String SCOPE_DESK_DELEGATION = URN_TEMPLATE_3.formatted(NAMESPACE, SCOPES, "delegation");
    String SCOPE_PARTIAL_DELEGATION = "%s:tenant:%%s:%%s:%%s".formatted(SCOPE_DESK_DELEGATION);
    String SCOPE_DESK_SUPERVISOR = URN_TEMPLATE_3.formatted(NAMESPACE, SCOPES, "supervisor");
    String SCOPE_DESK_DELEGATION_MANAGER = URN_TEMPLATE_3.formatted(NAMESPACE, SCOPES, "delegation_manager");
    String SCOPE_DESK_FUNCTIONAL_ADMIN = URN_TEMPLATE_3.formatted(NAMESPACE, SCOPES, "functional_admin");

    Set<String> DESK_OWNER_SCOPES = Set
            .of(SCOPE_VIEW, SCOPE_FOLDER_CREATION, SCOPE_FOLDER_ACTION, SCOPE_FOLDER_ARCHIVE, SCOPE_FOLDER_CHAIN, SCOPE_DESK_DELEGATION_MANAGER);
    Set<String> DESK_OPTIONAL_SCOPES = Set
            .of(SCOPE_FILTER, SCOPE_DESK_ASSOCIATION, SCOPE_DESK_DELEGATION, SCOPE_DESK_SUPERVISOR, SCOPE_DESK_FUNCTIONAL_ADMIN);
    Set<String> METADATA_SCOPES = Set
            .of(SCOPE_FILTER);
    Set<String> SUBTYPE_SCOPES = Set
            .of(SCOPE_USE, SCOPE_FILTER, SCOPE_PUBLIC_USE, SCOPE_PUBLIC_FILTER);

    String RESOURCE_TYPE_DESK = String.format(URN_TEMPLATE_3, NAMESPACE, RESOURCES, "desk");
    String RESOURCE_TYPE_METADATA = String.format(URN_TEMPLATE_3, NAMESPACE, RESOURCES, "metadata");
    String RESOURCE_TYPE_SUBTYPE = String.format(URN_TEMPLATE_3, NAMESPACE, RESOURCES, "subtype");

    Set<String> ALL_RESOURCE_TYPES = Set.of(RESOURCE_TYPE_DESK, RESOURCE_TYPE_METADATA, RESOURCE_TYPE_SUBTYPE);


    default @NotNull String generatePartialDelegationScopeType(@NotNull String tenantId, String typeId) {
        return SCOPE_PARTIAL_DELEGATION.formatted(tenantId, TYPE, typeId);
    }


    default @NotNull String generatePartialDelegationScopeSubtype(@NotNull String tenantId, String subtypeId) {
        return SCOPE_PARTIAL_DELEGATION.formatted(tenantId, SUBTYPE, subtypeId);
    }


    default @NotNull Set<String> generateScopeUrnSet(@NotNull Desk desk) {

        Set<String> result = new HashSet<>(DESK_OWNER_SCOPES);

        if (!desk.isFolderCreationAllowed()) {
            result.remove(SCOPE_FOLDER_CREATION);
        }
        if (!desk.isActionAllowed()) {
            result.remove(SCOPE_FOLDER_ACTION);
        }
        if (!desk.isArchivingAllowed()) {
            result.remove(SCOPE_FOLDER_ARCHIVE);
        }
        if (!desk.isChainAllowed()) {
            result.remove(SCOPE_FOLDER_CHAIN);
        }

        return result;
    }


    void deletePermission(@NotNull String resourceId);


    // <editor-fold desc="Desk resource CRUDL">


    void createDeskResource(@NotNull String tenantId, @NotNull Desk desk);


    void retrieveAndRefreshDeskPermissions(@NotNull String tenantId, @NotNull Desk desk);


    void editDeskPermission(@NotNull String tenantId, @NotNull Desk desk);


    boolean currentUserHasDirectViewingRightOnSomeDeskIn(@NotNull String tenantId,
                                                         @NotNull Set<String> deskIds,
                                                         @Nullable String typeId,
                                                         @Nullable String subtypeId);

    boolean currentUserHasViewingRightOnSomeDeskIn(@NotNull String tenantId,
                                                   @NotNull Set<String> deskIds,
                                                   @Nullable String typeId,
                                                   @Nullable String subtypeId);

    boolean currentUserHasArchivingRightOnSomeDeskIn(@NotNull String tenantId,
                                                     @NotNull Set<String> deskIds,
                                                     @Nullable String typeId,
                                                     @Nullable String subtypeId);

    boolean currentUserHasChainingRightOnSomeDeskIn(@NotNull String tenantId,
                                                    @NotNull Set<String> deskIds,
                                                    @Nullable String typeId,
                                                    @Nullable String subtypeId);

    boolean currentUserHasFolderCreationRightOnSomeDeskIn(@NotNull String tenantId,
                                                          @NotNull Set<String> deskIds);

    boolean currentUserHasAdminRightOnDesk(@NotNull String tenantId, @NotNull String deskIds);

    boolean currentUserHasAdminRightsOnTenant(@NotNull String tenantId);

    boolean currentUserHasFolderActionRightOnSomeDeskIn(@NotNull String tenantId,
                                                        @NotNull Set<String> deskIds,
                                                        @Nullable String typeId,
                                                        @Nullable String subtypeId);


    boolean currentUserHasDelegationOnSomeDeskIn(@NotNull String tenantId,
                                                 @NotNull Set<String> deskIds,
                                                 @Nullable String typeId,
                                                 @Nullable String subtypeId);


    // </editor-fold desc="Desk resource CRUDL">


    // <editor-fold desc="Evaluation">


    @NotNull Map<String, Set<DelegationRule>> getActiveDelegationsToDesksForCurrentUser(@NotNull Set<String> deskIds);

    /**
     * Return the delegations currently active for the passed set of desks
     *
     * @param tenantId the
     * @param deskIds  the set of desk ids (not full role name) for which to retrieve the active delegations
     * @return
     */
    @NotNull Map<String, Set<DelegationRule>> getActiveDelegationsForDesks(@NotNull String tenantId, @NotNull Set<String> deskIds);


    // </editor-fold desc="Evaluation">


    // <editor-fold desc="Metadata resource CRUDL">


    void createMetadataResource(@NotNull String tenantId, @NotNull String metadataId);


    // </editor-fold desc="Metadata resource CRUDL">


    // <editor-fold desc="Metadata resource CRUDL">


    @NotNull List<String> getCurrentUserViewableDeskIds();

    
    @NotNull Set<String> getAllDesksHavingDelegationFrom(@NotNull String tenantId, @NotNull Set<String> deskIds);

    @NotNull List<DeskRepresentation> getAllDesksHavingDelegationTo(@NotNull Set<String> deskIds);

    void createSubtypeResource(@NotNull String tenantId, @NotNull String subtypeId);


    // </editor-fold desc="Metadata resource CRUDL">


    // <editor-fold desc="Delegations CRUDL">


    void addDelegation(@NotNull String tenantId, @NotNull String substituteDesk, @NotNull String delegatingDesk,
                       @Nullable String typeId, @Nullable String subtypeId, @Nullable Date beginDate, @Nullable Date endDate);


    @Nullable Delegation getDelegation(@NotNull String deskId, @NotNull String id);


    void deleteDelegation(@NotNull String delegationId,
                          @NotNull String tenantId,
                          @NotNull String substituteDeskId,
                          @NotNull String delegatingDeskId,
                          @Nullable String typeId,
                          @Nullable String subtypeId,
                          @Nullable Date beginDate,
                          @Nullable Date endDate);


    @NotNull List<Delegation> getDelegations(@NotNull String tenantId, @NotNull String delegatingDeskId);


    // </editor-fold desc="Delegations CRUDL">


    @NotNull List<DeskRepresentation> getDelegatingDesks(@NotNull String targetDeskId);


    void setMetadataDelegations(@NotNull String deskId, @NotNull Collection<DelegationRule> metadataDelegations);


    // <editor-fold desc="Supervisor CRUDL">


    void addSupervisorsToDesk(@NotNull String tenantId, @NotNull String targetDeskId, @NotNull Collection<String> userIds);


    void removeSupervisorsFromDesk(@NotNull String tenantId, @NotNull String sourceDeskId, @NotNull Collection<String> userIds);


    void removeAllSupervisedDesks(@NotNull String tenantId, @NotNull String userId);


    @NotNull List<String> getSupervisorIdsFromDesk(@NotNull String tenantId, @NotNull String deskId);


    @NotNull List<DeskRepresentation> getSupervisedDesks(@NotNull String userId, @Nullable String tenantId);


    // </editor-fold desc="Supervisor CRUDL">


    // <editor-fold desc="Delegation manager CRUDL">


    void addDelegationManagersToDesk(@NotNull String tenantId, @NotNull String targetDeskId, @NotNull Collection<String> userIds);


    void removeDelegationManagersFromDesk(@NotNull String tenantId, @NotNull String sourceDeskId, @NotNull Collection<String> userIds);


    void removeAllDelegationManagedDesks(@NotNull String tenantId, @NotNull String userId);


    @NotNull List<String> getDelegationManagerIdsFromDesk(@NotNull String tenantId, @NotNull String deskId);


    @NotNull List<DeskRepresentation> getDelegationManagedDesks(@NotNull String userId, @Nullable String tenantId);


    // </editor-fold desc="Delegation manager CRUDL">


    // <editor-fold desc="Functional admin CRUDL">


    void setFunctionalAdmin(@NotNull String tenantId, @NotNull String targetDeskId, @NotNull String userId);


    void removeFunctionalAdmin(@NotNull String tenantId, @NotNull String sourceDeskId, @NotNull String userId);


    void removeAllAdministeredDesks(@NotNull String tenantId, @NotNull String userId);


    @NotNull List<DeskRepresentation> getAdministeredDesks(@NotNull String userId, @Nullable String tenantId);


    // </editor-fold desc="Functional admin CRUDL">


    // <editor-fold desc="Associates CRUDL">


    void associateDesks(@NotNull String tenantId, @NotNull String sourceDeskId, @NotNull Collection<String> associateDesks);


    void removeAssociatedDesks(@NotNull String tenantId, @NotNull String sourceDeskId, @NotNull Collection<String> associateDesksIds);


    @NotNull List<String> getAssociatedDeskIds(@NotNull String tenantId, @NotNull String sourceDeskId);


    // </editor-fold desc="Associates CRUDL">


    // <editor-fold desc="Filterable metadata CRUDL">


    void addFilterableMetadata(@NotNull String tenantId, @NotNull String sourceDeskId, @NotNull Collection<String> metadataIds);


    void removeFilterableMetadata(@NotNull String tenantId, @NotNull String sourceDeskId, @NotNull Collection<String> metadataIds);


    @NotNull List<String> getFilterableMetadataIds(@NotNull String tenantId, @NotNull String sourceDeskId);


    // </editor-fold desc="Filterable metadata CRUDL">


    // <editor-fold desc="Subtype usage allowed CRUDL">


    void setSubtypeCreationPermittedDeskIds(@NotNull String tenantId, @NotNull String subtypeId, @NotNull Collection<String> deskIds);


    /**
     * @param tenantId  the source tenant id
     * @param subtypeId the target subtype id
     * @return `null` if it is a public subtype, a list of desks ids otherwise (it can be empty)
     */
    @Nullable List<String> getSubtypePermittedDeskIds(@NotNull String tenantId, @NotNull String subtypeId);


    @NotNull List<String> getAllowedSubtypeIds(@NotNull String tenantId, @NotNull String sourceDeskId);


    // </editor-fold desc="Subtype usage allowed CRUDL">


    // <editor-fold desc="Filterable subtype CRUDL">


    void setSubtypeFilterableByDeskIds(@NotNull String tenantId, @NotNull String subtypeId, @NotNull Collection<String> deskIds);


    @Nullable List<String> getSubtypeFilterableByDeskIds(@NotNull String tenantId, @NotNull String subtypeId);


    @NotNull List<String> getFilterableSubtypeIds(@NotNull String tenantId, @NotNull String sourceDeskId);


    // </editor-fold desc="Filterable subtype CRUDL">


}
