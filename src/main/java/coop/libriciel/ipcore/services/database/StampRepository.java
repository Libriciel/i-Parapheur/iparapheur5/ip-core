/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.database;

import coop.libriciel.ipcore.model.pdfstamp.Stamp;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;


@Repository
public interface StampRepository extends PagingAndSortingRepository<Stamp, String>, CrudRepository<Stamp, String> {

    Stamp save(@NotNull Stamp stamp);

    @Transactional
    void deleteById(@NotNull String stampId);

    void deleteAllByIdIn(@NotNull Collection<String> stampIds);

    Optional<Stamp> findByIdAndParentLayer_Id(@NotNull String stampId, @NotNull String layerId);

    Optional<Stamp> findByParentLayer_Tenant_IdAndParentLayer_IdAndId(@NotNull String tenantId, @NotNull String layerId, @NotNull String stampId);
}
