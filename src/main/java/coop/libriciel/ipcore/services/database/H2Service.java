/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
package coop.libriciel.ipcore.services.database;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;


@Service(DatabaseServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = DatabaseServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = "org.h2.Driver")
public class H2Service extends PostgresService {


    /**
     * The original {@link PostgresService#getTenantIndexAvailable} relies on a specific PostgreSQL method,
     * and has to be mocked for the test's H2 database.
     */
    @Override
    public long getTenantIndexAvailable() {
        return 0;
    }


    /**
     * The original {@link PostgresService#getUserDataGroupIndexAvailable} relies on a specific PostgreSQL method,
     * and has to be mocked for the test's H2 database.
     */
    @Override
    public int getUserDataGroupIndexAvailable() {
        return 0;
    }


}
