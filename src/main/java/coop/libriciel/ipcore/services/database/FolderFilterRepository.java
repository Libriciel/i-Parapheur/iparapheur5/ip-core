/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.database;

import coop.libriciel.ipcore.model.database.userPreferences.FolderFilter;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface FolderFilterRepository extends PagingAndSortingRepository<FolderFilter, String> {


    FolderFilter save(@NotNull FolderFilter folderFilter);

    void delete(@NotNull FolderFilter folderFilter);

    Optional<FolderFilter> findByIdAndUserId(String id, String userId);

    List<FolderFilter> findAllByUserId(String userId);

    List<FolderFilter> findAllBySubtypeIdIsIn(List<String> subtypeIds);

    List<FolderFilter> findAllByTypeId(String typeId);


}
