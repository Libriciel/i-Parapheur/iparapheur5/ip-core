/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
package coop.libriciel.ipcore.services.database;

import coop.libriciel.ipcore.model.pdfstamp.Layer;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;


@Repository
public interface LayerRepository extends PagingAndSortingRepository<Layer, String> {

    Layer save(@NotNull Layer layer);

    void delete(@NotNull Layer layer);

    void deleteById(@NotNull String layerId);

    Collection<Layer> findAll();

    void deleteAll();

    Optional<Layer> findById(@NotNull String layerId);

    Optional<Layer> findByIdAndTenant_Id(@NotNull String layerId, @NotNull String tenantId);

    long countAllByTenant_Id(@NotNull String tenantId);

    Page<Layer> findAllByTenant_Id(@NotNull String tenantId, @NotNull Pageable pageable);

    Page<Layer> findAllByTenant_IdAndIdIn(@NotNull String tenantId, @NotNull Collection<String> ids, @NotNull Pageable pageable);


}
