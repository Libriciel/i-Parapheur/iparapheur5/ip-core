/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.database;

import coop.libriciel.ipcore.model.database.Tenant;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface TenantRepository extends PagingAndSortingRepository<Tenant, String> {


    List<Tenant> findAll();


    Tenant save(@NotNull Tenant tenant);


    void delete(@NotNull Tenant tenant);


    void deleteAll();


    Optional<Tenant> findById(@NotNull String id);



    long count();


    @Query(
            value = "SELECT t FROM Tenant t WHERE UPPER(t.name) LIKE UPPER(:searchTerm)",
            countQuery = "SELECT COUNT(t) FROM Tenant t WHERE UPPER(t.name) LIKE UPPER(:searchTerm)"
    )
    Page<Tenant> findAllWithSearchTerm(@Param("searchTerm") String wrappedSearchTerm, Pageable pageable);


    Page<Tenant> findDistinctBy(Pageable pageable);


    @NotNull List<Tenant> findAllById(@NotNull Iterable<String> ids);

}
