/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.database;

import coop.libriciel.ipcore.model.database.Type;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;


@Repository
@SuppressWarnings("java:S100") // Sonarlint naming convention forbids underscores, that are mandatory in JPA
public interface TypeRepository extends PagingAndSortingRepository<Type, String>, CrudRepository<Type, String> {


    @NotNull Optional<Type> findById(@NotNull String typeId);


    @NotNull Collection<Type> findAll();


    @NotNull Type save(@NotNull Type type);


    void delete(@NotNull Type type);


    void deleteAll();


    long count();


    Optional<Type> findByIdAndTenant_Id(@NotNull String typeId, @NotNull String tenantId);


    Optional<Type> findByNameIgnoreCaseAndTenant_Id(@NotNull String typeName, @NotNull String tenantId);


    @Query("""
           SELECT DISTINCT t
           FROM Type t
           INNER JOIN t.subtypes st
           WHERE st.id IN :subtypeIds
           AND t.tenant.id = :tenantId
           """)
    Page<Type> findAllByTenantIdAndSubtypesIn(@Param("tenantId")
                                              @NotNull String tenantId,
                                              @Param("subtypeIds")
                                              @NotNull Collection<String> subtypeIds,
                                              @NotNull Pageable pageable);


    Page<Type> findAllByTenant_IdAndIdIn(@NotNull String tenantId, @NotNull Collection<String> typeIds, @NotNull Pageable pageable);


    Page<Type> findAllByTenantId(@NotNull String tenantId, @NotNull Pageable pageable);


    Page<Type> findAllByTenantIdAndNameIgnoreCase(@NotNull String tenantId, @NotNull String typeName, @NotNull Pageable pageable);


}
