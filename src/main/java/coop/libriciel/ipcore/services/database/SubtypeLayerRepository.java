/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.database;

import coop.libriciel.ipcore.model.database.SubtypeLayer;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;


@Repository
public interface SubtypeLayerRepository extends PagingAndSortingRepository<SubtypeLayer, String>, CrudRepository<SubtypeLayer, String> {


    @NotNull SubtypeLayer save(@NotNull SubtypeLayer subtypeLayer);

    @NotNull Collection<SubtypeLayer> findAll();

    @Transactional
    void deleteAllByIdIn(@NotNull Collection<SubtypeLayer.CompositeId> subtypeLayerIds);

    void deleteAll();

    @NotNull Page<SubtypeLayer> findBySubtype_Id(@NotNull String subtypeId, @NotNull Pageable pageable);

    void deleteAllBySubtype_Id(@NotNull String subtypeId);

    @NotNull Optional<SubtypeLayer> findBySubtype_IdAndLayer_id(@NotNull String subtypeId, @NotNull String layerId);


}
