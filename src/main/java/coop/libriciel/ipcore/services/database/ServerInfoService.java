/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.database;

import coop.libriciel.ipcore.model.database.serverInfo.ServerInfo;
import coop.libriciel.ipcore.services.gdpr.GdprInformationDetailsDto;
import org.apache.commons.collections4.MapUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import static coop.libriciel.ipcore.model.database.serverInfo.ServerInfo.Key.*;
import static coop.libriciel.ipcore.model.database.serverInfo.ServerInfo.Key.Constants.GDPR_KEYS;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;


@Service(ServerInfoService.BEAN_NAME)
public class ServerInfoService {


    public static final String BEAN_NAME = "serverInfoService";


    // <editor-fold desc="Beans">


    private final GdprInformationDetailsDto gdprInformationDetailsDtoFromEnv;
    private final ServerInfoRepository serverInfoRepository;


    @Autowired
    public ServerInfoService(GdprInformationDetailsDto gdprInformationDetailsDto,
                             ServerInfoRepository serverInfoRepository) {
        this.gdprInformationDetailsDtoFromEnv = gdprInformationDetailsDto;
        this.serverInfoRepository = serverInfoRepository;
    }


    // </editor-fold desc="Beans">


    static @NotNull Map<ServerInfo.Key, String> serializeGdpr(@NotNull GdprInformationDetailsDto gdprDto) {
        HashMap<ServerInfo.Key, String> result = new HashMap<>();

        result.put(ENTITY_NAME, gdprDto.getDeclaringEntity().getName());
        result.put(ENTITY_ADDRESS, gdprDto.getDeclaringEntity().getAddress());
        result.put(ENTITY_SIRET, gdprDto.getDeclaringEntity().getSiret());
        result.put(ENTITY_APE_CODE, gdprDto.getDeclaringEntity().getApeCode());
        result.put(ENTITY_PHONE, gdprDto.getDeclaringEntity().getPhoneNumber());
        result.put(ENTITY_MAIL, gdprDto.getDeclaringEntity().getMail());
        result.put(RESPONSIBLE_NAME, gdprDto.getDeclaringEntity().getResponsible().getName());
        result.put(RESPONSIBLE_TITLE, gdprDto.getDeclaringEntity().getResponsible().getTitle());
        result.put(DPO_NAME, gdprDto.getDeclaringEntity().getDpo().getName());
        result.put(DPO_MAIL, gdprDto.getDeclaringEntity().getDpo().getMail());
        result.put(HOSTING_NAME, gdprDto.getHostingEntity().getName());
        result.put(HOSTING_ADDRESS, gdprDto.getHostingEntity().getAddress());
        result.put(HOSTING_SIRET, gdprDto.getHostingEntity().getSiret());
        result.put(HOSTING_COMMENT, gdprDto.getHostingEntityComments());
        result.put(MAINTENANCE_NAME, gdprDto.getMaintenanceEntity().getName());
        result.put(MAINTENANCE_ADDRESS, gdprDto.getMaintenanceEntity().getAddress());
        result.put(MAINTENANCE_SIRET, gdprDto.getMaintenanceEntity().getSiret());

        return result;
    }


    static @NotNull GdprInformationDetailsDto deserializeGdpr(@NotNull Map<ServerInfo.Key, String> infoMap) {
        GdprInformationDetailsDto result = new GdprInformationDetailsDto();

        result.getDeclaringEntity().setName(MapUtils.getString(infoMap, ENTITY_NAME));
        result.getDeclaringEntity().setAddress(MapUtils.getString(infoMap, ENTITY_ADDRESS));
        result.getDeclaringEntity().setSiret(MapUtils.getString(infoMap, ENTITY_SIRET));
        result.getDeclaringEntity().setApeCode(MapUtils.getString(infoMap, ENTITY_APE_CODE));
        result.getDeclaringEntity().setPhoneNumber(MapUtils.getString(infoMap, ENTITY_PHONE));
        result.getDeclaringEntity().setMail(MapUtils.getString(infoMap, ENTITY_MAIL));
        result.getDeclaringEntity().getResponsible().setName(MapUtils.getString(infoMap, RESPONSIBLE_NAME));
        result.getDeclaringEntity().getResponsible().setTitle(MapUtils.getString(infoMap, RESPONSIBLE_TITLE));
        result.getDeclaringEntity().getDpo().setName(MapUtils.getString(infoMap, DPO_NAME));
        result.getDeclaringEntity().getDpo().setMail(MapUtils.getString(infoMap, DPO_MAIL));
        result.getHostingEntity().setName(MapUtils.getString(infoMap, HOSTING_NAME));
        result.getHostingEntity().setAddress(MapUtils.getString(infoMap, HOSTING_ADDRESS));
        result.getHostingEntity().setSiret(MapUtils.getString(infoMap, HOSTING_SIRET));
        result.setHostingEntityComments(MapUtils.getString(infoMap, HOSTING_COMMENT));
        result.getMaintenanceEntity().setName(MapUtils.getString(infoMap, MAINTENANCE_NAME));
        result.getMaintenanceEntity().setAddress(MapUtils.getString(infoMap, MAINTENANCE_ADDRESS));
        result.getMaintenanceEntity().setSiret(MapUtils.getString(infoMap, MAINTENANCE_SIRET));

        return result;
    }


    public void saveGdprInformationDetails(GdprInformationDetailsDto gdprDto) {

        Set<ServerInfo> dbData = serializeGdpr(gdprDto).entrySet().stream()
                .map(e -> new ServerInfo(e.getKey(), e.getValue()))
                .collect(toSet());

        serverInfoRepository.saveAll(dbData);
    }


    public GdprInformationDetailsDto getGdprInformationDetails() {

        Set<ServerInfo> serverInfoSet = serverInfoRepository.getAllByColumnKeyIn(GDPR_KEYS);
        boolean serverInfoSetInDb = serverInfoSet.stream().anyMatch(s -> s.getValue() != null);
        if (serverInfoSetInDb) {
            Map<ServerInfo.Key, String> serverInfosMap = serverInfoSet
                    .stream()
                    .filter(s -> s.getValue() != null)
                    .collect(toMap(ServerInfo::getKey, ServerInfo::getValue));
            return deserializeGdpr(serverInfosMap);
        } else {
            return gdprInformationDetailsDtoFromEnv;
        }
    }


}
