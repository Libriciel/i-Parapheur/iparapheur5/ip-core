/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.database;

import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.database.MetadataRepresentation;
import coop.libriciel.ipcore.model.database.TenantRepresentation;
import coop.libriciel.ipcore.model.database.TypologyRepresentation;
import coop.libriciel.ipcore.model.workflow.DelegationRule;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.State;
import coop.libriciel.ipcore.model.workflow.requests.ColumnedTaskListRequest;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Set;


@Service
public interface DatabaseServiceInterface {


    String BEAN_NAME = "databaseService";
    String PREFERENCES_PROVIDER_KEY = "spring.datasource.driver-class-name";


    long getTenantIndexAvailable();


    int getUserDataGroupIndexAvailable();


    @NotNull
    Page<TenantRepresentation> listTenantsForUser(@NotNull String userId,
                                                  @NotNull Pageable pageable,
                                                  @Nullable String searchTerm,
                                                  boolean reverse,
                                                  boolean adminOnly,
                                                  boolean addFunctionalAdministeredTenants);


    @NotNull
    List<String> listAdminUsersIdForTenant(@NotNull TenantRepresentation tenant);


    @NotNull
    List<TypologyRepresentation> getTypologyHierarchy(@NotNull String tenantId,
                                                      @NotNull Pageable pageable,
                                                      boolean collapseAll,
                                                      @Nullable Set<String> reverseIdList,
                                                      @Nullable String searchTerm);


    Integer getTypologyHierarchyListTotal(@NotNull String tenantId, boolean collapseAll, @Nullable Set<String> reverseIdList, @Nullable String searchTerm);


    @NotNull
    List<Folder> getFoldersSortedByTypologyName(@NotNull Desk desk, @NotNull State state, @NotNull Pageable pageable);


    @NotNull
    List<Folder> getColumnedFolderList(@NotNull String tenantId,
                                       @NotNull List<String> groupIds,
                                       @NotNull List<DelegationRule> delegation,
                                       @NotNull String currentUserId,
                                       @NotNull ColumnedTaskListRequest columnedTaskListRequest,
                                       @NotNull Collection<MetadataRepresentation> includedMetadata,
                                       @NotNull Pageable pageable);


    int getColumnedFolderCount(@NotNull String tenantId,
                               @NotNull List<String> groupIds,
                               @NotNull List<DelegationRule> delegations,
                               @NotNull String currentUserId,
                               @NotNull ColumnedTaskListRequest columnedTaskListRequest,
                               @NotNull Collection<MetadataRepresentation> includedMetadata);

}
