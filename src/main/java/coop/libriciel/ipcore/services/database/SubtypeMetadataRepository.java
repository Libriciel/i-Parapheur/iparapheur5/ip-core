/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.database;

import coop.libriciel.ipcore.model.database.SubtypeMetadata;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;


@Repository
public interface SubtypeMetadataRepository extends PagingAndSortingRepository<SubtypeMetadata, String>, CrudRepository<SubtypeMetadata, String> {


    @NotNull SubtypeMetadata save(@NotNull SubtypeMetadata subtypeMetadata);

    @NotNull Collection<SubtypeMetadata> findAll();

    @Transactional
    void deleteAllByIdIn(@NotNull Collection<SubtypeMetadata.CompositeId> subtypeMetadataIds);

    void deleteAll();

    @NotNull Page<SubtypeMetadata> findBySubtype_Id(@NotNull String subtypeId, @NotNull Pageable pageable);

    long countAllByMetadata_Id(@NotNull String subtypeId);

    void deleteAllBySubtype_Id(@NotNull String subtypeId);

    @NotNull Optional<SubtypeMetadata> findBySubtype_IdAndMetadata_id(@NotNull String subtypeId, @NotNull String MetadataId);

    Page<SubtypeMetadata> findByMetadata_Id(@Param("metadataId") @NotNull String metadataId, @NotNull Pageable pageable);

    Page<SubtypeMetadata> findByMetadata_IdAndDefaultValueEquals(@NotNull String metadataId, @NotNull String value, @NotNull Pageable pageable);
}
