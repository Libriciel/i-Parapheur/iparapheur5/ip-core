/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.database;

import coop.libriciel.ipcore.model.database.IdCount;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Tenant;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;


@Repository
@SuppressWarnings("java:S100") // Sonarlint naming convention forbids underscores, that are mandatory in JPA
public interface SubtypeRepository extends PagingAndSortingRepository<Subtype, String>, JpaSpecificationExecutor<Subtype> {


    @NotNull Subtype save(@NotNull Subtype subtype);


    void delete(@NotNull Subtype subtype);


    void deleteById(@NotNull String id);


    void deleteAll();


    long count();


    @NotNull Optional<Subtype> findById(@NotNull String subtypeId);


    Collection<Subtype> findAllByIdIn(@NotNull Collection<String> subtypeIds);


    @NotNull Collection<Subtype> findAll();


    Optional<Subtype> findByIdAndTenant_Id(@NotNull String id, @NotNull String tenantId);


    Optional<Subtype> findByNameIgnoreCaseAndParentType_IdAndTenant_Id(@NotNull String id, @NotNull String typeId, @NotNull String tenantId);


    Optional<Subtype> findByIdAndParentType_IdAndTenant_Id(@NotNull String id, @NotNull String typeId, @NotNull String tenantId);


    Page<Subtype> findAllByIdIn(@NotNull Collection<String> idList, @NotNull Pageable pageable);


    Page<Subtype> findAllByTenant_IdAndIdIn(@NotNull String tenantId, @NotNull Collection<String> idList, @NotNull Pageable pageable);


    Page<Subtype> findAllByTenant_IdAndParentType_Id(@NotNull String tenantId, @NotNull String typeId, @NotNull Pageable pageable);


    Page<Subtype> findAllByTenant_Id(@NotNull String tenantId, @NotNull Pageable pageable);


    Page<Subtype> findByTenant_IdAndSealCertificateId(@NotNull String tenantId, String certificateId, Pageable pageable);


    List<Subtype> findAllByTenant_IdAndValidationWorkflowId(@NotNull String tenantId, @NotNull String validationWorkflowId);


    List<Subtype> findAllByTenant_IdAndCreationWorkflowId(@NotNull String tenantId, @NotNull String creationWorkflowId);


    @Query("""
           SELECT s FROM Subtype s
           WHERE s.tenant.id = :tenantId
           AND (
             s.validationWorkflowId = :validationWorkflowId
             OR s.creationWorkflowId = :creationWorkflowId
             OR s.workflowSelectionScript LIKE CONCAT('%ircuit "', :workflowDefinitionKey, '"%')
             OR s.workflowSelectionScript LIKE CONCAT('%ircuit ''', :workflowDefinitionKey, '''%')
           )
           """)
    Page<Subtype> findAllByTenant_IdAndValidationWorkflowIdOrCreationWorkflowIdOrWorkflowSelectionScriptContaining(@Param("tenantId")
                                                                                                                   @NotNull String tenantId,
                                                                                                                   @Param("validationWorkflowId")
                                                                                                                   @NotNull String validationWorkflowId,
                                                                                                                   @Param("creationWorkflowId")
                                                                                                                   @NotNull String creationWorkflowId,
                                                                                                                   @Param("workflowDefinitionKey")
                                                                                                                   @NotNull String workflowDefinitionKey,
                                                                                                                   @NotNull Pageable pageable);


    Page<Subtype> findAllByTenant_IdAndParentType_IdAndIdIn(@NotNull String tenantId, @NotNull String typeId, @NotNull Collection<String> subtypeIds,
                                                            @NotNull Pageable pageable);


    @Query("""
           SELECT NEW coop.libriciel.ipcore.model.database.IdCount(st.sealCertificateId, COUNT(st))\s
           FROM Subtype st\s
           WHERE st.sealCertificateId IN :sealCertificateIdList\s
           GROUP BY st.sealCertificateId
           """)
    List<IdCount> fetchCountBySealCertificateIdIn(@Param("sealCertificateIdList")
                                                  @NotNull Collection<String> sealCertificateIdList);


    default List<Subtype> getSubtypesReferencingWorkflowKey(@NotNull Tenant tenant, @NotNull String key) {
        List<Subtype> result = findAllByTenant_IdAndCreationWorkflowId(tenant.getId(), key);
        result.addAll(findAllByTenant_IdAndValidationWorkflowId(tenant.getId(), key));
        return result;
    }


    Page<Subtype> findAllByTenant_IdAndParentType_IdAndNameIgnoreCase(@NotNull String tenantId,
                                                                      @NotNull String typeId,
                                                                      @NotNull String subTypeName,
                                                                      @NotNull Pageable pageable);

}
