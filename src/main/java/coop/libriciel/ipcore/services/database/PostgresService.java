/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.database;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.database.Type;
import coop.libriciel.ipcore.model.database.*;
import coop.libriciel.ipcore.model.workflow.DelegationRule;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.FolderSortBy;
import coop.libriciel.ipcore.model.workflow.State;
import coop.libriciel.ipcore.model.workflow.ipWorkflow.IpWorkflowInstance;
import coop.libriciel.ipcore.model.workflow.ipWorkflow.IpWorkflowTask;
import coop.libriciel.ipcore.model.workflow.requests.ColumnedTaskListRequest;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.RequestUtils;
import coop.libriciel.ipcore.utils.TextUtils;
import jakarta.annotation.PostConstruct;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringSubstitutor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jooq.Record;
import org.jooq.*;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.security.InvalidParameterException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

import static coop.libriciel.ipcore.controller.admin.AdminTenantController.MAX_TENANTS_COUNT;
import static coop.libriciel.ipcore.model.auth.User.ATTRIBUTE_CONTENT_GROUP_INDEX;
import static coop.libriciel.ipcore.model.database.TypologyEntity.COLUMN_NAME;
import static coop.libriciel.ipcore.model.workflow.FolderSortBy.*;
import static coop.libriciel.ipcore.model.workflow.FolderSortBy.Constants.*;
import static coop.libriciel.ipcore.model.workflow.State.*;
import static coop.libriciel.ipcore.services.auth.KeycloakService.*;
import static coop.libriciel.ipcore.services.workflow.IpWorkflowService.WORKFLOW_INTERNAL_VAR_PREFIX;
import static coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface.META_SUBTYPE_ID;
import static coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface.META_TYPE_ID;
import static coop.libriciel.ipcore.utils.ApiUtils.MAX_USERS_COUNT;
import static coop.libriciel.ipcore.utils.CollectionUtils.toMutableList;
import static coop.libriciel.ipcore.utils.PaginatedList.MAX_BIG_PAGE_SIZE;
import static coop.libriciel.ipcore.utils.RequestUtils.isFirstOrderAsc;
import static coop.libriciel.ipcore.utils.TextUtils.PSQL_RENDER_SETTINGS;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static java.util.Comparator.naturalOrder;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import static org.jooq.SQLDialect.POSTGRES;
import static org.jooq.conf.ParamType.INLINED;
import static org.jooq.impl.DSL.*;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;


@Log4j2
@Primary
@Service(DatabaseServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = DatabaseServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = "org.postgresql.Driver")
public class PostgresService implements DatabaseServiceInterface {


    public static final String SQL_TASK_NAME = "task_name";
    public static final String SQL_END_DATE = "end_date";
    public static final String SQL_VARIABLES = "variables";
    public static final String SQL_SORT_BY = "SORT_BY";

    public static final Condition TRUE_CONDITION = val(1, Integer.class).eq(val(1, Integer.class));
    private static final Condition FALSE_CONDITION = val(1, Integer.class).eq(val(0, Integer.class));

    private static final String TABLE_RU_EXECUTION = "ACT_RU_EXECUTION";
    private static final String TABLE_RU_IDENTITYLINK = "ACT_RU_IDENTITYLINK";
    private static final String TABLE_RU_TASK = "ACT_RU_TASK";


    private static HikariDataSource dataSource;

    private @Value("${spring.datasource.url}") String dbUrl;
    private @Value("${spring.datasource.username}") String dbUsername;
    private @Value("${spring.datasource.password}") String dbPassword;


    // <editor-fold desc="LifeCycle">


    public PostgresService() {}


    @PostConstruct
    public void init() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(dbUrl);
        config.setUsername(dbUsername);
        config.setPassword(dbPassword);
        config.setMaxLifetime(5 * 60 * 1000);
        dataSource = new HikariDataSource(config);
    }


    // </editor-fold desc="LifeCycle">


    public static @NotNull String buildSqlMetadataName(@NotNull String metadataId) {
        return "metadata_%s".formatted(metadataId.replaceAll("-", "_"));
    }


    /**
     * Tenants have an index, to ease grouping in Alfresco.
     * <p>
     * Those indexes could be a regular number field, auto-incremented with an SQL primitive function,
     * but this method leave gaps, on every tenant deletion.
     * <p>
     * To get the smaller index available, in one request, this do the trick.
     *
     * @return a long
     */
    @Override
    public long getTenantIndexAvailable() {
        try (Connection connection = dataSource.getConnection()) {
            return DSL.using(connection, PSQL_RENDER_SETTINGS)
                    .fetchOptional(buildTenantIndexAvailableRequest())
                    .map(Record1::value1)
                    .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_read_database_response"));
        } catch (SQLException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_database", e);
        }
    }


    static @NotNull Select<Record1<Integer>> buildTenantIndexAvailableRequest() {

        String possibleIndex = "possibleIndex";

        return DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(field(possibleIndex, Integer.class))
                .from(generateSeries(0, MAX_TENANTS_COUNT).as(possibleIndex))
                .where(field(possibleIndex)
                        .notIn(
                                select(field(Tenant.COLUMN_INDEX)).from(Tenant.TABLE_NAME)
                        ))
                .limit(1);
    }


    @Override
    public int getUserDataGroupIndexAvailable() {
        try (Connection connection = dataSource.getConnection()) {
            return DSL.using(connection, PSQL_RENDER_SETTINGS)
                    .fetchOptional(buildUserDataGroupIndexAvailableRequest())
                    .map(Record1::value1)
                    .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_read_database_response"));
        } catch (SQLException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_database", e);
        }
    }


    static @NotNull Select<Record1<Integer>> buildUserDataGroupIndexAvailableRequest() {

        String possibleIndex = "possibleIndex";

        return DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(field(possibleIndex, Integer.class))
                .from(generateSeries(0, MAX_USERS_COUNT).as(possibleIndex))
                .where(field(possibleIndex)
                        .notIn(
                                select(field("value").cast(Integer.class))
                                        .from("user_attribute")
                                        .where(field("name", String.class).equal(ATTRIBUTE_CONTENT_GROUP_INDEX))
                        ))
                .limit(1);
    }


    @Override
    public @NotNull Page<TenantRepresentation> listTenantsForUser(@NotNull String userId,
                                                                  @NotNull Pageable pageable,
                                                                  @Nullable String searchTerm,
                                                                  boolean reverse,
                                                                  boolean adminOnly,
                                                                  boolean includeFunctionalAdminTenants) {

        log.debug("listTenantsForUser searchTerm:{} page:{} pageSize:{}, reverse:{}",
                searchTerm,
                pageable.isPaged() ? pageable.getPageNumber() : "NA",
                pageable.isPaged() ? pageable.getPageSize() : "INF",
                reverse);

        try (Connection connection = dataSource.getConnection()) {

            DSLContext dsl = DSL.using(connection, PSQL_RENDER_SETTINGS);
            List<TenantRepresentation> result = dsl
                    .fetch(adminOnly
                           ? buildListAdministeredTenantsForUserRequest(userId, pageable, searchTerm, includeFunctionalAdminTenants)
                           : buildListTenantsForUserRequest(userId, pageable, searchTerm, reverse)
                    )
                    .stream()
                    .map(TenantRepresentation::new)
                    .collect(toList());

            // Sending back result

            log.info("listTenantsForUser resultSize:{}", result.size());

            long total = dsl
                    .fetchOptional(adminOnly ? buildCountAdministeredTenantsForUser(userId, searchTerm, includeFunctionalAdminTenants)
                                             : buildCountTenantsForUser(userId, searchTerm, reverse))
                    .map(Record1::value1)
                    .orElse(-1);

            return new PageImpl<>(result, pageable, total);

        } catch (SQLException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_database", e);
        }
    }


    static @NotNull Select<Record4<String, String, String, String>>
    buildListAdministeredTenantsForUserRequest(@NotNull String userId,
                                               @NotNull Pageable pageable,
                                               @Nullable String searchTerm,
                                               boolean withFunctionalAdmins) {

        long offset = (pageable != Pageable.unpaged()) ? pageable.getOffset() : 0L;
        long limit = (pageable != Pageable.unpaged()) ? pageable.getPageSize() : MAX_BIG_PAGE_SIZE;

        String sortBy = pageable.getSort().stream().findFirst()
                .map(Sort.Order::getProperty)
                .orElse(Tenant.COLUMN_NAME);

        boolean asc = pageable.getSort().stream().findFirst()
                .map(Sort.Order::getDirection)
                .map(Sort.Direction::isAscending)
                .orElse(true);

        Condition searchCondition = TextUtils.getAsSqlLikeWrapped(searchTerm)
                .map(DSL::upper)
                .map(wrappedSearch -> (Condition) upper(field("t." + Tenant.COLUMN_NAME, String.class)).like(wrappedSearch))
                .orElse(TRUE_CONDITION);

        Select<Record4<String, String, String, String>> result;
        SelectOnConditionStep<Record4<String, String, String, String>> partialQuery = DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(
                        field("t." + Tenant.COLUMN_ID, String.class).as(Tenant.COLUMN_ID),
                        field("t." + Tenant.COLUMN_NAME, String.class).as(Tenant.COLUMN_NAME),
                        field("urma.role_id", String.class).as("admin_link"),
                        field("urma.user_id", String.class).as("user_id")
                )
                .from(table(Tenant.TABLE_NAME).as("t"))

                .innerJoin(table("keycloak_role").as("kra"))
                .on(field("kra.name").equal(concat(
                        val("tenant_", String.class),
                        field("t." + Tenant.COLUMN_ID, String.class),
                        val("_admin", String.class)
                )));

        if (withFunctionalAdmins) {
            partialQuery = partialQuery.innerJoin(table("keycloak_role").as("krf"))
                    .on(field("krf.name").equal(concat(
                            val("tenant_", String.class),
                            field("t." + Tenant.COLUMN_ID, String.class),
                            val("_functional_admin", String.class)
                    )))
                    .leftOuterJoin(table("user_role_mapping").as("urmf"))
                    .on(and(
                            field("urmf.role_id").equal(field("krf.id")),
                            field("urmf.user_id").equal(userId)
                    ));
        }

        result = partialQuery.leftOuterJoin(table("user_role_mapping").as("urma"))
                .on(and(
                        field("urma.role_id").equal(field("kra.id")),
                        field("urma.user_id").equal(userId)
                ))

                .where(and(searchCondition, or(
                        field("urma.role_id").isNotNull(),
                        withFunctionalAdmins ? field("urmf.role_id").isNotNull() : FALSE_CONDITION)
                ))

                .orderBy(asc ? field("t." + sortBy).asc() : field("t." + sortBy).desc())
                .limit(limit)
                .offset(offset);

        log.trace("buildListAdministeredTenantsForUserRequest SQL:{}", result.getSQL(INLINED));
        return result;
    }


    static @NotNull Select<Record4<String, String, String, String>>
    buildListTenantsForUserRequest(@NotNull String userId,
                                   @NotNull Pageable pageable,
                                   @Nullable String searchTerm,
                                   boolean reverse) {

        long offset = (pageable != Pageable.unpaged()) ? pageable.getOffset() : 0L;
        long limit = (pageable != Pageable.unpaged()) ? pageable.getPageSize() : MAX_BIG_PAGE_SIZE;

        String sortBy = pageable.getSort().stream().findFirst()
                .map(Sort.Order::getProperty)
                .orElse(Tenant.COLUMN_NAME);

        boolean asc = pageable.getSort().stream().findFirst()
                .map(Sort.Order::getDirection)
                .map(Sort.Direction::isAscending)
                .orElse(true);

        Condition searchCondition = TextUtils.getAsSqlLikeWrapped(searchTerm)
                .map(DSL::upper)
                .map(wrappedSearch -> (Condition) upper(field("t." + Tenant.COLUMN_NAME, String.class)).like(wrappedSearch))
                .orElse(TRUE_CONDITION);

        Select<Record4<String, String, String, String>> result = DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(
                        field("t." + Tenant.COLUMN_ID, String.class).as(Tenant.COLUMN_ID),
                        field("t." + Tenant.COLUMN_NAME, String.class).as(Tenant.COLUMN_NAME),
                        field("urmu.role_id", String.class).as("user_link"),
                        field("urmu.user_id", String.class).as("user_id")
                )
                .from(table(Tenant.TABLE_NAME).as("t"))

                .innerJoin(table("keycloak_role").as("kru"))
                .on(field("kru.name").equal(concat(
                        val("tenant_", String.class),
                        field("t." + Tenant.COLUMN_ID, String.class)
                )))


                .leftOuterJoin(table("user_role_mapping").as("urmu"))
                .on(and(
                        field("urmu.role_id").equal(field("kru.id")),
                        field("urmu.user_id").equal(userId)
                ))

                .where(and(
                        searchCondition,
                        reverse
                        ? field("urmu.role_id").isNull()
                        : field("urmu.role_id").isNotNull()
                ))

                .orderBy(asc ? field("t." + sortBy).asc() : field("t." + sortBy).desc())
                .limit(limit)
                .offset(offset);

        log.trace("buildListTenantsForUserRequest SQL:{}", result.getSQL(INLINED));
        return result;
    }


    static @NotNull Select<Record1<Integer>>
    buildCountAdministeredTenantsForUser(@NotNull String userId, @Nullable String searchTerm, boolean withFunctionalAdmins) {

        Condition searchCondition = TextUtils.getAsSqlLikeWrapped(searchTerm)
                .map(wrappedSearch -> (Condition) field("t." + Tenant.COLUMN_NAME, String.class).like(wrappedSearch))
                .orElse(TRUE_CONDITION);

        Select<Record1<Integer>> result;
        SelectOnConditionStep<Record1<Integer>> partialQuery = DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .selectCount()
                .from(table(Tenant.TABLE_NAME).as("t"))

                .innerJoin(table("keycloak_role").as("kra"))
                .on(field("kra.name").equal(concat(
                        val("tenant_", String.class),
                        field("t." + Tenant.COLUMN_ID, String.class),
                        val("_admin", String.class)
                )));

        if (withFunctionalAdmins) {
            partialQuery = partialQuery.innerJoin(table("keycloak_role").as("krf"))
                    .on(field("krf.name").equal(concat(
                            val("tenant_", String.class),
                            field("t." + Tenant.COLUMN_ID, String.class),
                            val("_functional_admin", String.class)
                    )))
                    .leftOuterJoin(table("user_role_mapping").as("urmf"))
                    .on(and(
                            field("urmf.role_id").equal(field("krf.id")),
                            field("urmf.user_id").equal(userId)
                    ));
        }

        result = partialQuery.leftOuterJoin(table("user_role_mapping").as("urma"))
                .on(and(
                        field("urma.role_id").equal(field("kra.id")),
                        field("urma.user_id").equal(userId)
                ))

                .where(and(searchCondition, or(
                        field("urma.role_id").isNotNull(),
                        withFunctionalAdmins ? field("urmf.role_id").isNotNull() : FALSE_CONDITION)
                ));

        return result;
    }


    static @NotNull Select<Record1<Integer>>
    buildCountTenantsForUser(@NotNull String userId, @Nullable String searchTerm, boolean reverse) {

        Condition searchCondition = TextUtils.getAsSqlLikeWrapped(searchTerm)
                .map(wrappedSearch -> (Condition) field("t." + Tenant.COLUMN_NAME, String.class).like(wrappedSearch))
                .orElse(TRUE_CONDITION);

        return DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .selectCount()
                .from(table(Tenant.TABLE_NAME).as("t"))

                .innerJoin(table("keycloak_role").as("kru"))
                .on(field("kru.name").equal(concat(
                        val("tenant_", String.class),
                        field("t." + Tenant.COLUMN_ID, String.class)
                )))


                .leftOuterJoin(table("user_role_mapping").as("urmu"))
                .on(and(
                        field("urmu.role_id").equal(field("kru.id")),
                        field("urmu.user_id").equal(userId)
                ))

                .where(and(
                        searchCondition,
                        reverse
                        ? field("urmu.role_id").isNull()
                        : field("urmu.role_id").isNotNull()
                ));
    }


    @Override
    public @NotNull List<String> listAdminUsersIdForTenant(@NotNull TenantRepresentation tenant) {
        log.debug("listAdminUsersForTenant tenant:{}", tenant.getId());
        try (Connection connection = dataSource.getConnection()) {

            DSLContext dsl = DSL.using(connection, PSQL_RENDER_SETTINGS);
            List<String> result = dsl
                    .fetch(buildListAdminUsersIdForTenant(tenant.getId()))
                    .stream()
                    .map(r -> r.get(0, String.class))
                    .toList();

            log.info("listTenantsForUser resultSize:{}", result.size());
            return result;

        } catch (SQLException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_database", e);
        }
    }


    /**
     * Explain : We want to fetch the somehow-tenant-admins here.
     * <p>
     * They are the union between that have the roles :
     * - "tenant_id01_admin" : the native tenant-admin ones
     * - "admin" + "tenant_id01" : the super-admins that are somehow linked to a tenant
     * <p>
     * By design, those two groups cannot overlap.
     * We can do 2 separate requests, merged in an union-all.
     *
     * @param tenantId
     * @return A Record2 selector user_id/role_name
     */
    static @NotNull Select<Record2<String, String>>
    buildListAdminUsersIdForTenant(@NotNull String tenantId) {

        Map<String, String> tenantIdMapping = Map.of(TENANT_PLACEHOLDER, tenantId);
        String tenantAdminRoleName = StringSubstitutor.replace(TENANT_ADMIN_INTERNAL_NAME, tenantIdMapping);
        String tenantUserRoleName = StringSubstitutor.replace(TENANT_INTERNAL_NAME, tenantIdMapping);

        String tenantAdminUserRoleMappingAlias = "urm";
        String tenantAdminRoleAlias = "krta";
        String superAdminUserRoleMappingAlias = "urma";
        String superAdminRoleAlias = "krsa";
        String simpleUserRoleRoleMappingAlias = "urmta";
        String simpleUserRoleAlias = "kru";

        Select<Record2<String, String>> result = DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(
                        field(tenantAdminUserRoleMappingAlias + ".user_id", String.class).as("user_id"),
                        field(tenantAdminRoleAlias + ".name", String.class)
                )
                .from(table("user_role_mapping").as(tenantAdminUserRoleMappingAlias))

                .innerJoin(table("keycloak_role").as(tenantAdminRoleAlias))
                .on(and(
                        field(tenantAdminRoleAlias + ".name", String.class).equal(val(tenantAdminRoleName, String.class)),
                        field(tenantAdminUserRoleMappingAlias + ".role_id", String.class).equal(field(tenantAdminRoleAlias + ".id", String.class))
                ))

                .unionAll(
                        select(
                                field(superAdminUserRoleMappingAlias + ".user_id", String.class).as("user_id"),
                                field(simpleUserRoleAlias + ".name", String.class)
                        )
                                .from(table("user_role_mapping").as(superAdminUserRoleMappingAlias))

                                .innerJoin(table("keycloak_role").as(superAdminRoleAlias))
                                .on(and(
                                        field(superAdminRoleAlias + ".name", String.class).equal(val(SUPER_ADMIN_ROLE_NAME, String.class)),
                                        field(superAdminUserRoleMappingAlias + ".role_id", String.class).equal(field(superAdminRoleAlias + ".id", String.class))
                                ))

                                .innerJoin(table("user_role_mapping").as(simpleUserRoleRoleMappingAlias))
                                .on(field(superAdminUserRoleMappingAlias + ".user_id", String.class).equal(field(simpleUserRoleRoleMappingAlias + ".user_id",
                                        String.class)))

                                .innerJoin(table("keycloak_role").as(simpleUserRoleAlias))
                                .on(and(
                                        field(simpleUserRoleAlias + ".name", String.class).equal(val(tenantUserRoleName, String.class)),
                                        field(simpleUserRoleRoleMappingAlias + ".role_id", String.class).equal(field(simpleUserRoleAlias + ".id", String.class))
                                ))
                );

        log.trace("buildListTenantsForUserRequest SQL:{}", result.getSQL(INLINED));
        return result;
    }


    @Override
    public @NotNull List<TypologyRepresentation> getTypologyHierarchy(@NotNull String tenantId,
                                                                      @NotNull Pageable pageable,
                                                                      boolean collapseAll,
                                                                      @Nullable Set<String> reverseIdList,
                                                                      @Nullable String searchTerm) {

        try (Connection connection = dataSource.getConnection()) {
            return DSL.using(connection, PSQL_RENDER_SETTINGS)
                    .fetch(buildTypologyEntityListSqlRequest(tenantId, pageable, collapseAll, firstNonNull(reverseIdList, emptySet()), searchTerm))
                    .stream()
                    .map(TypologyRepresentation::new)
                    .toList();
        } catch (SQLException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_database", e);
        }
    }


    /**
     * The request is split in 2 sub-requests, merged with an UNION ALL :
     * - {@link Type} that matches the search request, or containing a {@link Subtype} that matches the search request
     * - {@link Subtype} that matches the search request
     *
     * @param tenantId
     * @param pageable
     * @param collapseAll
     * @param reversedIdList
     * @param searchTerm
     * @return
     */
    static @NotNull Select<Record7<String, String, String, String, String, String, Integer>>
    buildTypologyEntityListSqlRequest(@NotNull String tenantId, @NotNull Pageable pageable, boolean collapseAll, @NotNull Set<String> reversedIdList,
                                      @Nullable String searchTerm) {

        if (pageable.getSort().stream()
                .map(Sort.Order::getProperty)
                .anyMatch(property -> !StringUtils.equals(property, TypologySortBy.NAME.getColumnName()))) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.typology_hierarchy_can_only_be_sort_by_name");
        }

        boolean sortAscending = pageable.getSort().stream().findFirst()
                .map(Sort.Order::getDirection)
                .map(Sort.Direction::isAscending)
                .orElse(true);

        String order1 = "order1_name";
        String order2 = "order2_name";

        int limit = pageable.isPaged() ? pageable.getPageSize() : MAX_BIG_PAGE_SIZE;
        long offset = pageable.isPaged() ? pageable.getOffset() : 0;

        return DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(
                        field("t." + Type.COLUMN_ID, String.class).as(TypologyEntity.COLUMN_ID),
                        field("t." + Type.COLUMN_NAME, String.class).as(TypologyEntity.COLUMN_NAME),
                        field("t." + Type.COLUMN_DESCRIPTION, String.class).as(TypologyEntity.COLUMN_DESCRIPTION),
                        field("t." + Type.COLUMN_NAME, String.class).as(order1),
                        val(null, String.class).as(order2),
                        val(null, String.class).as(TypologyRepresentation.COLUMN_PARENT_ID),
                        count(field("st." + Subtype.COLUMN_ID)).as(TypologyRepresentation.COLUMN_CHILDREN_COUNT)
                )

                .from(table(Type.TABLE_NAME).as("t"))

                .leftJoin(table(Subtype.TABLE_NAME).as("st"))
                .on(field("st." + Subtype.COLUMN_PARENT).eq(field("t." + Type.COLUMN_ID)))
                .where(and(
                        field("t." + Type.COLUMN_TENANT_ID).eq(tenantId),
                        TextUtils.getAsSqlLikeWrapped(searchTerm)
                                .map(s -> or(
                                        field("t." + COLUMN_NAME, String.class).likeIgnoreCase(s),
                                        field("st." + COLUMN_NAME, String.class).likeIgnoreCase(s)
                                ))
                                .orElse(val(1).eq(1))
                ))

                .groupBy(field("t." + TypologyEntity.COLUMN_ID),
                        field("t." + TypologyEntity.COLUMN_NAME),
                        field("t." + TypologyEntity.COLUMN_DESCRIPTION),
                        field(TypologyRepresentation.COLUMN_PARENT_ID),
                        field(order1),
                        field(order2)
                )

                .unionAll(
                        select(
                                field("st." + Subtype.COLUMN_ID, String.class).as(TypologyEntity.COLUMN_ID),
                                field("st." + Subtype.COLUMN_NAME, String.class).as(TypologyEntity.COLUMN_NAME),
                                field("st." + Subtype.COLUMN_DESCRIPTION, String.class).as(TypologyEntity.COLUMN_DESCRIPTION),
                                field("t." + Type.COLUMN_NAME, String.class).as(order1),
                                field("st." + Subtype.COLUMN_NAME, String.class).as(order2),
                                field("t." + Type.COLUMN_ID, String.class).as(TypologyRepresentation.COLUMN_PARENT_ID),
                                val(0, Integer.class).as(TypologyRepresentation.COLUMN_CHILDREN_COUNT)
                        ).from(table(Subtype.TABLE_NAME).as("st"))
                                .innerJoin(table(Type.TABLE_NAME).as("t"))
                                .on(field("st." + Subtype.COLUMN_PARENT).equal(field("t." + Type.COLUMN_ID)))
                                .where(and(
                                        field("st." + Subtype.COLUMN_TENANT_ID).equal(tenantId),
                                        collapseAll
                                        ? field("t." + Type.COLUMN_ID).in(reversedIdList)
                                        : field("t." + Type.COLUMN_ID).notIn(reversedIdList),
                                        TextUtils.getAsSqlLikeWrapped(searchTerm)
                                                .map(s -> (Condition) field("st." + COLUMN_NAME, String.class).likeIgnoreCase(s))
                                                .orElse(val(1).eq(1))
                                ))
                )
                .orderBy(
                        sortAscending ? field(order1).asc() : field(order1).desc(),
                        sortAscending ? field(order2).asc().nullsFirst() : field(order2).desc().nullsFirst()
                )
                .limit(limit)
                .offset(offset);
    }


    static @NotNull Select<Record1<Integer>>
    buildTypologyEntityTotalSqlRequest(@NotNull String tenantId, boolean collapseAll, @NotNull Set<String> reversedIdList, @Nullable String searchTerm) {

        Select<Record1<Integer>> result = DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .selectCount()
                .from(
                        selectDistinct(field("t." + Type.COLUMN_ID, String.class).as(Type.COLUMN_ID))
                                .from(table(Type.TABLE_NAME).as("t"))
                                .leftJoin(table(Subtype.TABLE_NAME).as("st"))
                                .on(field("st." + Subtype.COLUMN_PARENT).eq(field("t." + Type.COLUMN_ID)))
                                .where(and(
                                        field("t." + Type.COLUMN_TENANT_ID, String.class).eq(tenantId),
                                        TextUtils.getAsSqlLikeWrapped(searchTerm)
                                                .map(s -> or(
                                                        field("t." + COLUMN_NAME, String.class).likeIgnoreCase(s),
                                                        field("st." + COLUMN_NAME, String.class).likeIgnoreCase(s)
                                                ))
                                                .orElse(val(1).eq(1)))
                                )
                                .unionAll(
                                        select(field("st." + Subtype.COLUMN_ID, String.class).as(Subtype.COLUMN_ID))
                                                .from(table(Subtype.TABLE_NAME).as("st"))
                                                .innerJoin(table(Type.TABLE_NAME).as("t"))
                                                .on(field("st." + Subtype.COLUMN_PARENT).equal(field("t." + Type.COLUMN_ID)))
                                                .where(and(
                                                        field("st." + Subtype.COLUMN_TENANT_ID, String.class).eq(tenantId),
                                                        collapseAll
                                                        ? field("t." + Type.COLUMN_ID).in(reversedIdList)
                                                        : field("t." + Type.COLUMN_ID).notIn(reversedIdList),
                                                        TextUtils.getAsSqlLikeWrapped(searchTerm)
                                                                .map(s -> (Condition) field("st." + COLUMN_NAME, String.class).likeIgnoreCase(s))
                                                                .orElse(val(1).eq(1)))
                                                )
                                ));

        log.trace("buildTypologyEntityTotalSqlRequest:\n{}", result.getSQL(INLINED));
        return result;
    }


    @Override
    public Integer getTypologyHierarchyListTotal(@NotNull String tenantId,
                                                 boolean collapseAll,
                                                 @Nullable Set<String> reverseIdList,
                                                 @Nullable String searchTerm) {

        try (Connection connection = dataSource.getConnection()) {
            return DSL.using(connection, PSQL_RENDER_SETTINGS)
                    .fetchOptional(buildTypologyEntityTotalSqlRequest(tenantId, collapseAll, firstNonNull(reverseIdList, emptySet()), searchTerm))
                    .map(Record1::value1)
                    .orElse(-1);
        } catch (SQLException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_database", e);
        }
    }


    static @NotNull Select<Record10<String, String, String, String, String, Date, Date, String, String[][], String>>
    buildTasksSqlRequest(@NotNull Desk desk, @NotNull State state, @Nullable FolderSortBy sortBy, boolean asc, int page, int pageSize) {

        Condition stateCondition = switch (state) {
            case DRAFT -> field(TABLE_RU_TASK + ".NAME_").equal("workflow_internal_" + DRAFT.toString().toLowerCase());
            case REJECTED -> field(TABLE_RU_TASK + ".NAME_").equal("workflow_internal_" + REJECTED.toString().toLowerCase());
            case FINISHED -> field(TABLE_RU_TASK + ".NAME_").equal("workflow_internal_" + FINISHED.toString().toLowerCase());
            default -> and(
                    field(TABLE_RU_TASK + ".NAME_").notEqual("workflow_internal_" + DRAFT.toString().toLowerCase()),
                    field(TABLE_RU_TASK + ".NAME_").notEqual("workflow_internal_" + REJECTED.toString().toLowerCase()),
                    field(TABLE_RU_TASK + ".NAME_").notEqual("workflow_internal_" + FINISHED.toString().toLowerCase())
            );
        };

        // Build up the SQL request

        SelectForUpdateStep<Record10<String, String, String, String, String, Date, Date, String, String[][], String>> offset = using(POSTGRES,
                PSQL_RENDER_SETTINGS)
                .select(
                        field(TABLE_RU_EXECUTION + ".ROOT_PROC_INST_ID_", String.class).as(FOLDER_ID_VALUE),
                        field("act_ru_exec_root" + ".NAME_", String.class).as(FOLDER_NAME_VALUE),
                        field(TABLE_RU_IDENTITYLINK + ".group_id_", String.class).as(CURRENT_DESK_ID_VALUE),
                        field(TABLE_RU_TASK + ".id_", String.class).as(TASK_ID_VALUE),
                        field(TABLE_RU_TASK + ".NAME_", String.class).as(SQL_TASK_NAME),
                        field(TABLE_RU_TASK + ".due_date_", Date.class).as(LATE_DATE_VALUE),
                        field(TABLE_RU_EXECUTION + ".start_time_", Date.class).as(START_DATE_VALUE),
                        val(null, String.class).as(SQL_END_DATE),
                        field(arrayAggDistinct(array(
                                field("act_ru_variable_md.NAME_", String.class),
                                field("act_ru_variable_md.text_", String.class)))
                        ).as(SQL_VARIABLES),
                        field("typology.name", String.class).as(SQL_SORT_BY))

                .from(table(TABLE_RU_IDENTITYLINK))

                .innerJoin(table(TABLE_RU_TASK))
                .on(field(TABLE_RU_IDENTITYLINK + ".task_id_").equal(field(TABLE_RU_TASK + ".id_")))

                .innerJoin(table(TABLE_RU_EXECUTION))
                .on(field(TABLE_RU_EXECUTION + ".id_").equal(field(TABLE_RU_TASK + ".proc_inst_id_")))

                .innerJoin(table(TABLE_RU_EXECUTION).as("act_ru_exec_root"))
                .on(field("act_ru_exec_root" + ".id_").equal(field(TABLE_RU_EXECUTION + ".ROOT_PROC_INST_ID_")))

                .innerJoin(table("act_ru_variable").as("act_ru_variable_md"))
                .on(field(TABLE_RU_EXECUTION + ".ROOT_PROC_INST_ID_").equal(field("act_ru_variable_md.proc_inst_id_")))

                .leftJoin(table("act_ru_variable").as("act_ru_variables_sb"))
                .on(field(TABLE_RU_EXECUTION + ".ROOT_PROC_INST_ID_").equal(field("act_ru_variables_sb.proc_inst_id_")))
                .and(field("act_ru_variables_sb" + ".name_").equal(sortBy == TYPE_NAME ? "i_Parapheur_internal_type_id" : "i_Parapheur_internal_subtype_id"))

                .leftJoin(table(sortBy == TYPE_NAME ? Type.TABLE_NAME : Subtype.TABLE_NAME).as("typology"))
                .on(field("act_ru_variables_sb" + ".text_").equal(field("typology.id")))

                .leftJoin(table("act_ru_variable").as("act_ru_variables_db"))
                .on(field(TABLE_RU_EXECUTION + ".ROOT_PROC_INST_ID_").equal(field("act_ru_variables_db.proc_inst_id_")))

                .where(field(TABLE_RU_IDENTITYLINK + ".type_").equal("candidate"))
                .and(buildPermissionCondition(desk))
                .and(field(TABLE_RU_TASK + ".NAME_").notEqual("undo"))
                .and(stateCondition)

                .groupBy(
                        field(FOLDER_ID_VALUE),
                        field(FOLDER_NAME_VALUE),
                        field(CURRENT_DESK_ID_VALUE),
                        field(TASK_ID_VALUE),
                        field(SQL_TASK_NAME),
                        field(LATE_DATE_VALUE),
                        field(START_DATE_VALUE),
                        field(SQL_SORT_BY)
                )

                .orderBy(asc ? field(SQL_SORT_BY).asc() : field(SQL_SORT_BY).desc(), field(START_DATE_VALUE).asc())
                .limit(pageSize)
                .offset(pageSize * page);
        return offset;
    }


    static Condition buildPermissionCondition(@NotNull Desk desk) {

        ArrayList<String> deskIds = new ArrayList<>();
        deskIds.add(desk.getId());
        deskIds.addAll(Optional.ofNullable(desk.getDelegatingDesks())
                .orElse(emptyList())
                .stream()
                .map(Desk::getId)
                .toList());
        deskIds.sort(naturalOrder());

        // Building SQL request.

        List<Condition> delegatingConditionList = new ArrayList<>();
        delegatingConditionList.add(field(TABLE_RU_IDENTITYLINK + ".group_id_").in(deskIds));

        Optional.ofNullable(desk.getDelegationRules())
                .stream()
                .flatMap(Collection::stream)
                .map(c -> and(
                        field(TABLE_RU_IDENTITYLINK + ".group_id_").equal(c.getDelegatingGroup()),
                        field("act_ru_variables_db.name_").equal(c.getMetadataKey()),
                        field("act_ru_variables_db.text_").equal(c.getMetadataValue())))
                .forEach(delegatingConditionList::add);

        return or(delegatingConditionList);
    }


    @Override
    public @NotNull List<Folder> getFoldersSortedByTypologyName(@NotNull Desk desk, @NotNull State state, @NotNull Pageable pageable) {

        FolderSortBy sortBy = RequestUtils.getFirstOderSort(pageable, FolderSortBy.class, CREATION_DATE);
        boolean asc = RequestUtils.isFirstOrderAsc(pageable);

        if (!asList(TYPE_NAME, SUBTYPE_NAME).contains(sortBy)) {
            throw new InvalidParameterException("This method only accept sorting by Type/Subtype name");
        }

        try (Connection connection = dataSource.getConnection()) {
            return DSL.using(connection, PSQL_RENDER_SETTINGS)
                    .fetch(buildTasksSqlRequest(desk, state, sortBy, asc, pageable.getPageNumber(), pageable.getPageSize()))
                    .stream()
                    .map(IpWorkflowTask::new)
                    .map(IpWorkflowInstance::new)
                    .map(Folder.class::cast)
                    .toList();
        } catch (SQLException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_database", e);
        }
    }


    static <T extends Record> SelectOnConditionStep<T> buildColumnedFrom(@NotNull SelectSelectStep<T> selectRequest,
                                                                         @NotNull String tenantId,
                                                                         @NotNull String currentUserId,
                                                                         @NotNull String sortBy,
                                                                         @NotNull Collection<MetadataRepresentation> includedMetadata,
                                                                         @Nullable State state) {

        String tableHistTaskInst = "act_hi_taskinst";
        String tableHistProcInst = "act_hi_procinst";
        String tableHiIdentityLink = "act_hi_identitylink";
        String tableRuExecution = "act_ru_execution";
        String tableRuTask = "act_ru_task";
        String tableRuExecutionRootAlias = "act_ru_exec_root";
        String taskStartAlias = "task_start";
        String currentDeskAlias = "current_desk";

        SelectOnConditionStep<T> tableJoinRequest = selectRequest.from(table(tableHiIdentityLink))

                .innerJoin(table(tableRuTask))
                .on(and(
                        field(tableHiIdentityLink + ".TASK_ID_", String.class).equal(field(tableRuTask + ".ID_", String.class)),
                        field(tableHiIdentityLink + ".TYPE_", String.class).eq("candidate")
                ));

        if (state == RETRIEVABLE) {
            tableJoinRequest = tableJoinRequest
                    .innerJoin(table(tableRuTask).as("act_ru_task_read_desk"))
                    .on(and(
                            field(tableRuTask + ".PROC_INST_ID_", String.class).equal(field("act_ru_task_read_desk" + ".PROC_INST_ID_", String.class)),
                            field("act_ru_task_read_desk" + ".NAME_", String.class).equal("read")
                    ))
                    .innerJoin(table(tableHiIdentityLink).as("act_hi_identitylink_read_desk"))
                    .on(and(
                            field("act_hi_identitylink_read_desk" + ".task_id_", String.class).equal(field("act_ru_task_read_desk" + ".ID_", String.class)),
                            field("act_hi_identitylink_read_desk" + ".type_", String.class).equal("candidate")
                    ));
        }

        tableJoinRequest = tableJoinRequest
                .innerJoin(table(tableRuExecution))
                .on(field(tableRuExecution + ".ID_", String.class).equal(field(tableRuTask + ".PROC_INST_ID_", String.class)))

                .innerJoin(table(tableRuExecution).as(tableRuExecutionRootAlias))
                .on(and(
                        field(tableRuExecutionRootAlias + ".ID_", String.class).equal(field(tableRuExecution + ".ROOT_PROC_INST_ID_", String.class)),
                        field(tableRuExecutionRootAlias + ".tenant_id_", String.class).eq(tenantId)
                ))

                .innerJoin(table(tableHistTaskInst).as(taskStartAlias))
                .on(and(
                        field(taskStartAlias + ".name_", String.class).equal(val("main_start", String.class)),
                        field(taskStartAlias + ".tenant_id_", String.class).eq(tenantId)
                ))
                .innerJoin(table(tableHistProcInst).as("task_start_proc"))
                .on(and(
                        field("task_start_proc" + ".ID_", String.class).equal(field(taskStartAlias + ".PROC_INST_ID_", String.class)),
                        field("task_start_proc" + ".business_key_", String.class).equal(field("act_ru_exec_root" + ".business_key_", String.class))
                ))

                .innerJoin(table(tableHiIdentityLink).as("start_identity"))
                .on(and(
                        field("start_identity" + ".TASK_ID_", String.class).equal(field(taskStartAlias + ".ID_", String.class)),
                        field("start_identity" + ".TYPE_", String.class).eq("candidate")
                ))

                .leftOuterJoin(table(tableHistTaskInst).as("task_read"))
                .on(and(
                        field("task_read" + ".PROC_INST_ID_", String.class).equal(field("act_ru_task.PROC_INST_ID_", String.class)),
                        field("task_read" + ".NAME_", String.class).equal(val("read", String.class)),
                        field("task_read" + ".end_time_", Date.class).isNotNull(),
                        field("task_read" + ".assignee_", String.class).equal(currentUserId)
                ))

                .leftOuterJoin(table("ACT_RU_VARIABLE").as("type_metadata"))
                .on(and(
                        field(tableRuExecution + ".ROOT_PROC_INST_ID_").equal(field("type_metadata.PROC_INST_ID_")),
                        field("type_metadata.NAME_", String.class).equal(META_TYPE_ID)
                ))

                .leftOuterJoin(table("ACT_RU_VARIABLE").as("subtype_metadata"))
                .on(and(
                        field(tableRuExecution + ".ROOT_PROC_INST_ID_").equal(field("subtype_metadata.PROC_INST_ID_")),
                        field("subtype_metadata.NAME_", String.class).equal(META_SUBTYPE_ID)
                ));

        tableJoinRequest = switch (sortBy) {
            case CURRENT_DESK_NAME_VALUE, CURRENT_DESK_VALUE -> tableJoinRequest
                    .innerJoin(table("keycloak_role").as(currentDeskAlias))
                    .on(and(
                            field(tableHiIdentityLink + ".GROUP_ID_", String.class).equal(field(currentDeskAlias + ".id", String.class)),
                            field(currentDeskAlias + ".realm_id", String.class).equal(val("i-Parapheur", String.class))
                    ));
            case EMITTER_DESK_NAME_VALUE -> tableJoinRequest
                    .leftOuterJoin(table("keycloak_role").as("start_desk"))
                    .on(and(
                            field("start_identity" + ".GROUP_ID_", String.class).equal(field("start_desk.id", String.class)),
                            field("start_desk.realm_id", String.class).equal(val("i-Parapheur", String.class))
                    ));
            case EMITTER_USER_NAME_VALUE -> tableJoinRequest
                    .leftOuterJoin(table("user_entity").as("start_user"))
                    .on(and(
                            field("start_user" + ".id", String.class).equal(field(taskStartAlias + ".assignee_", String.class)),
                            field("start_user.realm_id", String.class).equal(val("i-Parapheur", String.class))
                    ));
            case TYPE_NAME_VALUE -> tableJoinRequest
                    .leftOuterJoin(table("type").as("type"))
                    .on(and(
                            field("type.id").equal(field("type_metadata.text_")),
                            field("type.tenant_id", String.class).equal(tenantId)
                    ));
            case SUBTYPE_NAME_VALUE -> tableJoinRequest
                    .leftOuterJoin(table("subtype").as("subtype"))
                    .on(and(
                            field("subtype.id").equal(field("subtype_metadata.text_")),
                            field("subtype.tenant_id", String.class).equal(tenantId)
                    ));
            default -> tableJoinRequest;
        };

        if (state == DOWNSTREAM) {

            // The following 3 "upstream" joins are here to catch an upstream already validated task, and filter those on candidates.
            // If the requested tasks are the "downstream" ones, then these are indeed the "upstream" ones.
            String actHistProcessInstanceUpstream = "act_hi_procinst_upstream";
            String actHistTaskInstanceUpstream = "act_hi_taskinst_upstream";
            String actHistIdentityLinkUpstream = "act_hi_identitylink_upstream";

            tableJoinRequest = tableJoinRequest
                    .innerJoin(table(tableHistProcInst).as(actHistProcessInstanceUpstream))
                    .on(field(tableRuExecution + ".business_key_").equal(field(actHistProcessInstanceUpstream + ".business_key_")))

                    .innerJoin(table(tableHistTaskInst).as(actHistTaskInstanceUpstream))
                    .on(and(
                            field(actHistProcessInstanceUpstream + ".ID_").equal(field(actHistTaskInstanceUpstream + ".PROC_INST_ID_")),
                            field(actHistTaskInstanceUpstream + ".tenant_id_", String.class).equal(val(tenantId)),
                            field(actHistTaskInstanceUpstream + ".end_time_", Date.class).isNotNull(),
                            field(actHistTaskInstanceUpstream + ".name_", String.class).notIn("read", "undo")
                    ))

                    .innerJoin(table("act_hi_identitylink").as(actHistIdentityLinkUpstream))
                    .on(and(
                            field(actHistTaskInstanceUpstream + ".ID_").equal(field(actHistIdentityLinkUpstream + ".task_id_")),
                            field(actHistIdentityLinkUpstream + ".TYPE_", String.class).equal(val("candidate"))
                    ));
        }

        AtomicReference<SelectOnConditionStep<T>> atomicTableJoinRequest = new AtomicReference<>(tableJoinRequest);
        includedMetadata.stream()
                .sorted(Comparator.comparing(MetadataRepresentation::getId))
                .forEach(metadata -> atomicTableJoinRequest.getAndUpdate(res -> res
                        .leftJoin(table("ACT_RU_VARIABLE").as(buildSqlMetadataName(metadata.getId())))
                        .on(and(
                                field(tableRuExecution + ".ROOT_PROC_INST_ID_").equal(field(buildSqlMetadataName(metadata.getId()) + ".PROC_INST_ID_")),
                                field(buildSqlMetadataName(metadata.getId()) + ".name_", String.class).equal(metadata.getKey())
                        ))
                ));

        return atomicTableJoinRequest.get();
    }


    static <T extends Record> SelectConditionStep<T> buildColumnedWhere(@NotNull SelectOnConditionStep<T> tableJoinRequest,
                                                                        @NotNull List<String> groupIds,
                                                                        @NotNull List<DelegationRule> delegations,
                                                                        @NotNull ColumnedTaskListRequest columnedTaskListRequest) {

        String tableRuTask = "act_ru_task";
        String tableRuExecutionRootAlias = "act_ru_exec_root";
        AtomicReference<SelectConditionStep<T>> result;
        if (columnedTaskListRequest.state() == DRAFT) {
            result = new AtomicReference<>(tableJoinRequest.where(field(tableRuTask + ".NAME_", String.class).in(
                    WORKFLOW_INTERNAL_VAR_PREFIX + DRAFT.toString().toLowerCase(),
                    "main_start"
            )));
        } else if (columnedTaskListRequest.state() == DOWNSTREAM) {
            result = new AtomicReference<>(tableJoinRequest.where(field(tableRuTask + ".NAME_", String.class).notIn(
                    "workflow_internal_draft",
                    "main_start",
                    "undo"
            )));
        } else if (columnedTaskListRequest.state() == FINISHED) {
            result = new AtomicReference<>(tableJoinRequest.where(field(tableRuTask + ".NAME_", String.class).in(
                    WORKFLOW_INTERNAL_VAR_PREFIX + FINISHED.toString().toLowerCase(),
                    "main_archive"
            )));
        } else if (columnedTaskListRequest.state() == LATE) {
            result = new AtomicReference<>(tableJoinRequest.where(and(
                    field(tableRuTask + ".NAME_", String.class).notIn(
                            WORKFLOW_INTERNAL_VAR_PREFIX + DRAFT.toString().toLowerCase(),
                            "main_start"
                    ),
                    field(tableRuTask + ".NAME_", String.class).notIn(
                            WORKFLOW_INTERNAL_VAR_PREFIX + REJECTED.toString().toLowerCase(),
                            "main_delete"
                    ),
                    field(tableRuTask + ".NAME_", String.class).notIn(
                            WORKFLOW_INTERNAL_VAR_PREFIX + FINISHED.toString().toLowerCase(),
                            "main_archive"
                    ),
                    field(tableRuTask + ".NAME_", String.class).notEqual("undo"),
                    field(tableRuTask + ".due_date_", Date.class).isNotNull(),
                    field(tableRuTask + ".due_date_", Date.class).lessThan(new Date())
            )));
        } else if (columnedTaskListRequest.state() == REJECTED) {
            result = new AtomicReference<>(tableJoinRequest.where(field(tableRuTask + ".NAME_", String.class).in(
                    WORKFLOW_INTERNAL_VAR_PREFIX + REJECTED.toString().toLowerCase(),
                    "main_delete"
            )));
        } else if (columnedTaskListRequest.state() == RETRIEVABLE) {
            result = new AtomicReference<>(tableJoinRequest.where(field(tableRuTask + ".NAME_", String.class).equal("undo")));
        } else if (columnedTaskListRequest.state() == PENDING || columnedTaskListRequest.state() == DELEGATED) {
            result = new AtomicReference<>(tableJoinRequest.where(and(
                    field(tableRuTask + ".NAME_", String.class).notIn(
                            WORKFLOW_INTERNAL_VAR_PREFIX + DRAFT.toString().toLowerCase(),
                            "main_start"
                    ),
                    field(tableRuTask + ".NAME_", String.class).notIn(
                            WORKFLOW_INTERNAL_VAR_PREFIX + REJECTED.toString().toLowerCase(),
                            "main_delete"
                    ),
                    field(tableRuTask + ".NAME_", String.class).notIn(
                            WORKFLOW_INTERNAL_VAR_PREFIX + FINISHED.toString().toLowerCase(),
                            "main_archive"
                    ),
                    field(tableRuTask + ".NAME_", String.class).notEqual("undo")
            )));
        }  else {
            result = new AtomicReference<>(tableJoinRequest.where());
        }

        result.set(result.get().and(field(tableRuTask + ".NAME_").notEqual("read")));

        // Filters

        Optional.ofNullable(columnedTaskListRequest.searchTerm())
                .filter(StringUtils::isNotEmpty)
                .map(s -> String.format("%%%s%%", s))
                .map(w -> (Condition) field(tableRuExecutionRootAlias + ".NAME_", String.class).likeIgnoreCase(w))
                .map(condition -> result.get().and(condition))
                .ifPresent(result::set);

        Optional.ofNullable(columnedTaskListRequest.createdAfter())
                .map(Date::new)
                .map(createAfterDate -> field(tableRuExecutionRootAlias + ".START_TIME_", Date.class).greaterOrEqual(createAfterDate))
                .map(condition -> result.get().and(condition))
                .ifPresent(result::set);

        Optional.ofNullable(columnedTaskListRequest.createdBefore())
                .map(Date::new)
                .map(RequestUtils::toEndOfDay)
                .map(createBeforeDate -> field(tableRuExecutionRootAlias + ".START_TIME_", Date.class).lessOrEqual(createBeforeDate))
                .map(condition -> result.get().and(condition))
                .ifPresent(result::set);

        Optional.ofNullable(columnedTaskListRequest.stillSinceTime())
                .map(Date::new)
                .map(RequestUtils::toEndOfDay)
                .map(stillSinceDate -> field(tableRuExecutionRootAlias + ".START_TIME_", Date.class).lessOrEqual(stillSinceDate))
                .map(condition -> result.get().and(condition))
                .ifPresent(result::set);

        Optional.ofNullable(columnedTaskListRequest.typeId())
                .filter(StringUtils::isNotEmpty)
                .map(typeId -> field("type_metadata.text_", String.class).equal(typeId))
                .map(condition -> result.get().and(condition))
                .ifPresent(result::set);

        Optional.ofNullable(columnedTaskListRequest.subtypeId())
                .filter(StringUtils::isNotEmpty)
                .map(subtypeId -> field("subtype_metadata.text_", String.class).equal(subtypeId))
                .map(condition -> result.get().and(condition))
                .ifPresent(result::set);

        String actHistIdentityLinkUpstream = "act_hi_identitylink_upstream";
        String tableHiIdentityLink = "act_hi_identitylink";

        if (columnedTaskListRequest.state() == DOWNSTREAM && !groupIds.isEmpty()) {
            result.set(result.get().and(field(actHistIdentityLinkUpstream + ".GROUP_ID_", String.class).in(groupIds)));
        } else if (columnedTaskListRequest.state() == DELEGATED) {
            result.set(result.get()
                    .and(or(delegations.stream()
                            .map(delegation -> {
                                if (StringUtils.isEmpty(delegation.getMetadataKey())) {
                                    return field(tableHiIdentityLink + ".GROUP_ID_", String.class).equal(delegation.getDelegatingGroup());
                                } else if (StringUtils.equals(delegation.getMetadataKey(), META_SUBTYPE_ID)) {
                                    return and(
                                            field(tableHiIdentityLink + ".GROUP_ID_", String.class).equal(delegation.getDelegatingGroup()),
                                            field("subtype_metadata.text_", String.class).equal(delegation.getMetadataValue())
                                    );
                                } else if (StringUtils.equals(delegation.getMetadataKey(), META_TYPE_ID)) {
                                    return and(
                                            field(tableHiIdentityLink + ".GROUP_ID_", String.class).equal(delegation.getDelegatingGroup()),
                                            field("type_metadata.text_", String.class).equal(delegation.getMetadataValue())
                                    );
                                } else {
                                    throw new RuntimeException("Invalid metadata key delegation" + delegation.getMetadataKey());
                                }
                            })
                            .toList()
                    )));
        } else if (!groupIds.isEmpty()) {
            result.set(result.get().and(field(tableHiIdentityLink + ".GROUP_ID_", String.class).in(groupIds)));
        }


        return result.get();
    }


    /**
     * @param columnedTaskListRequest The request parameters
     * @return A count record object.
     */
    static @NotNull SelectForUpdateStep<Record1<Integer>> buildColumnedTaskCountRequest(@NotNull String tenantId,
                                                                                        @NotNull List<String> groupIds,
                                                                                        @NotNull List<DelegationRule> delegations,
                                                                                        @NotNull String currentUserId,
                                                                                        @NotNull ColumnedTaskListRequest columnedTaskListRequest,
                                                                                        @NotNull Collection<MetadataRepresentation> includedMetadata) {

        // Build up the SQL request

        SelectSelectStep<Record1<Integer>> selectRequest = using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(countDistinct(field("act_ru_task.id_"), field("act_hi_identitylink.GROUP_ID_")));

        SelectOnConditionStep<Record1<Integer>> tableJoinRequest = buildColumnedFrom(selectRequest,
                tenantId,
                currentUserId,
                FOLDER_ID_VALUE,
                includedMetadata,
                columnedTaskListRequest.state()
        );

        SelectConditionStep<Record1<Integer>> whereRequest = buildColumnedWhere(
                tableJoinRequest,
                groupIds,
                delegations,
                columnedTaskListRequest
        );

        log.trace("buildTasksCountSqlRequest:\n{}", () -> whereRequest.getSQL(INLINED));
        return whereRequest;
    }


    /**
     * @param columnedTaskListRequest The request parameters
     * @param pageable                A standard SpringBoot pageable
     * @return A raw SelectForUpdateStep, the parameterized column output forbid us to fix it.
     */
    static @NotNull SelectForUpdateStep<? extends Record> buildColumnedTaskListRequest(@NotNull String tenantId,
                                                                                       @NotNull List<String> groupIds,
                                                                                       @NotNull List<DelegationRule> delegations,
                                                                                       @NotNull String currentUserId,
                                                                                       @NotNull ColumnedTaskListRequest columnedTaskListRequest,
                                                                                       @NotNull Collection<MetadataRepresentation> includedMetadata,
                                                                                       @NotNull Pageable pageable) {

        // Local variables

        String tableRuExecution = "act_ru_execution";
        String tableRuTask = "act_ru_task";
        String tableRuExecutionRootAlias = "act_ru_exec_root";
        String tableHiIdentityLink = "act_hi_identitylink";
        String taskStartAlias = "task_start";
        String currentDeskAlias = "current_desk";

        // Building sort conditions

        boolean asc = isFirstOrderAsc(pageable);

        String sortBy = pageable.getSort().get().findFirst()
                .map(Sort.Order::getProperty)
                .map(p -> Optional.ofNullable(EnumUtils.getEnum(FolderSortBy.class, p))
                        .map(Object::toString)
                        .orElse(p))
                .orElse(CREATION_DATE_VALUE);

        Field<?> sqlSortColumn = switch (sortBy) {
            case ACTION_TYPE_VALUE -> field(tableRuTask + ".NAME_", String.class);
            case CREATION_DATE_VALUE -> field(tableRuExecutionRootAlias + ".START_TIME_", String.class);
            case CURRENT_DESK_NAME_VALUE, CURRENT_DESK_VALUE -> upper(field(currentDeskAlias + ".description", String.class));
            case IS_READ_VALUE -> field("task_read.id_", String.class).isNotNull();
            case LATE_DATE_VALUE -> field(tableRuTask + ".DUE_DATE_", Date.class);
            case EMITTER_USER_ID_VALUE -> field(tableHiIdentityLink + ".GROUP_ID_", String.class);
            case EMITTER_USER_NAME_VALUE -> concat(
                    field("start_user" + ".first_name", String.class),
                    val(" "),
                    field("start_user" + ".last_name", String.class)
            );
            case EMITTER_DESK_ID_VALUE -> field("start_desk" + ".ID", String.class);
            case EMITTER_DESK_NAME_VALUE -> upper(field("start_desk" + ".description", String.class));
            case FOLDER_ID_VALUE -> field(tableRuExecutionRootAlias + ".ID_", String.class);
            case FOLDER_NAME_VALUE -> upper(field(tableRuExecutionRootAlias + ".NAME_", String.class));
            case START_DATE_VALUE -> field(taskStartAlias + ".end_time_", Date.class);
            case STILL_SINCE_DATE_VALUE -> field(tableRuTask + ".CREATE_TIME_", Date.class);
            case SUBTYPE_ID_VALUE -> field("subtype_metadata.text_", String.class);
            case SUBTYPE_NAME_VALUE -> upper(field("subtype.name", String.class));
            case TASK_ID_VALUE -> field(tableRuTask + ".ID_", String.class);
            case TYPE_ID_VALUE -> field("type_metadata.text_", String.class);
            case TYPE_NAME_VALUE -> upper(field("type.name", String.class));
            // If nothing above matches, sort by metadata
            default -> field(buildSqlMetadataName(sortBy) + ".text_", String.class);
        };

        // Build up the SQL request

        List<Field<?>> selectFields = new ArrayList<>(List.of(
                field(tableRuExecution + ".ROOT_PROC_INST_ID_", String.class).as(FOLDER_ID_VALUE),
                field(tableRuExecutionRootAlias + ".NAME_", String.class).as(FOLDER_NAME_VALUE),
                field(tableHiIdentityLink + ".GROUP_ID_", String.class).as(CURRENT_DESK_ID_VALUE),
                field(tableRuTask + ".ID_", String.class).as(TASK_ID_VALUE),
                field(tableRuTask + ".NAME_", String.class).as(SQL_TASK_NAME),
                field(tableRuTask + ".DUE_DATE_", Date.class).as(LATE_DATE_VALUE),
                field(tableRuExecutionRootAlias + ".START_TIME_", Date.class).as(CREATION_DATE_VALUE),
                val(null, String.class).as(SQL_END_DATE),
                field(taskStartAlias + ".end_time_", Date.class).as(START_DATE_VALUE),
                field(taskStartAlias + ".assignee_", String.class).as(EMITTER_USER_ID_VALUE),
                field("start_identity" + ".GROUP_ID_", String.class).as(EMITTER_DESK_ID_VALUE),
                field("task_read.id_", String.class).isNotNull().as(IS_READ_VALUE),
                field("type_metadata.text_", String.class).as(TYPE_ID_VALUE),
                field("subtype_metadata.text_", String.class).as(SUBTYPE_ID_VALUE),
                (columnedTaskListRequest.state() == RETRIEVABLE)
                ? field("act_hi_identitylink_read_desk.group_id_", String.class).as(READ_TASK_CURRENT_DESK_ID_VALUE)
                : val(null, String.class).as(READ_TASK_CURRENT_DESK_ID_VALUE),
                sqlSortColumn.as(SQL_SORT_BY)
        ));

        includedMetadata.stream()
                .map(MetadataRepresentation::getId)
                .map(PostgresService::buildSqlMetadataName)
                .sorted()
                .map(prefixedMetadata -> field(prefixedMetadata + ".text_", String.class).as(prefixedMetadata))
                .forEach(selectFields::add);

        SelectSelectStep<Record> selectRequest = DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(selectFields);

        SelectOnConditionStep<Record> tableJoinRequest = buildColumnedFrom(
                selectRequest,
                tenantId,
                currentUserId,
                sortBy,
                includedMetadata,
                columnedTaskListRequest.state()
        );
        SelectConditionStep<Record> whereRequest = buildColumnedWhere(
                tableJoinRequest,
                groupIds,
                delegations,
                columnedTaskListRequest
        );

        List<Field<Object>> groupByFields = Stream.of(
                field(FOLDER_ID_VALUE),
                field(FOLDER_NAME_VALUE),
                field(CURRENT_DESK_ID_VALUE),
                field(TASK_ID_VALUE),
                field(SQL_TASK_NAME),
                field(LATE_DATE_VALUE),
                field(CREATION_DATE_VALUE),
                field(IS_READ_VALUE),
                field(SQL_SORT_BY),
                field(START_DATE_VALUE),
                field(EMITTER_USER_ID_VALUE),
                field(EMITTER_DESK_ID_VALUE),
                field(TYPE_ID_VALUE),
                field(SUBTYPE_ID_VALUE),
                field(READ_TASK_CURRENT_DESK_ID_VALUE)
        ).collect(toMutableList());
        includedMetadata.stream()
                .map(MetadataRepresentation::getId)
                .map(PostgresService::buildSqlMetadataName)
                .sorted()
                .map(DSL::field)
                .forEach(groupByFields::add);

        SelectForUpdateStep<Record> orderConditionRequest = whereRequest.groupBy(groupByFields)
                .orderBy(
                        asc ? field(SQL_SORT_BY).asc().nullsFirst() : field(SQL_SORT_BY).desc().nullsLast(),
                        field(CREATION_DATE_VALUE).asc()
                )
                .limit(pageable.getPageSize())
                .offset(pageable.getPageSize() * pageable.getPageNumber());

        log.trace("buildTasksSqlRequest:\n{}", () -> orderConditionRequest.getSQL(INLINED));
        return orderConditionRequest;
    }


    @Override
    public @NotNull List<Folder> getColumnedFolderList(@NotNull String tenantId,
                                                       @NotNull List<String> groupIds,
                                                       @NotNull List<DelegationRule> delegations,
                                                       @NotNull String currentUserId,
                                                       @NotNull ColumnedTaskListRequest columnedTaskListRequest,
                                                       @NotNull Collection<MetadataRepresentation> includedMetadata,
                                                       @NotNull Pageable pageable) {

        try (Connection connection = dataSource.getConnection()) {

            return DSL.using(connection, PSQL_RENDER_SETTINGS)
                    .fetch(buildColumnedTaskListRequest(
                            tenantId,
                            groupIds,
                            delegations,
                            currentUserId,
                            columnedTaskListRequest,
                            includedMetadata,
                            pageable
                    ))
                    .stream()
                    .map(IpWorkflowTask::new)
                    .map(IpWorkflowInstance::new)
                    .map(Folder.class::cast)
                    .toList();

        } catch (SQLException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_database", e);
        }
    }


    @Override
    public int getColumnedFolderCount(@NotNull String tenantId,
                                      @NotNull List<String> groupIds,
                                      @NotNull List<DelegationRule> delegations,
                                      @NotNull String currentUserId,
                                      @NotNull ColumnedTaskListRequest columnedTaskListRequest,
                                      @NotNull Collection<MetadataRepresentation> includedMetadata) {

        try (Connection connection = dataSource.getConnection()) {

            return DSL.using(connection, PSQL_RENDER_SETTINGS)
                    .fetchOptional(buildColumnedTaskCountRequest(
                            tenantId,
                            groupIds,
                            delegations,
                            currentUserId,
                            columnedTaskListRequest,
                            includedMetadata
                    ))
                    .map(r -> r.get(0, Integer.class))
                    .orElse(-1);

        } catch (SQLException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_database", e);
        }
    }


}
