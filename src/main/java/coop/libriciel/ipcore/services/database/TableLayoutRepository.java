/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.database;

import coop.libriciel.ipcore.model.database.userPreferences.TableLayout;
import coop.libriciel.ipcore.model.database.userPreferences.TableName;
import coop.libriciel.ipcore.model.database.userPreferences.UserPreferences;
import jakarta.transaction.Transactional;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface TableLayoutRepository extends PagingAndSortingRepository<TableLayout, TableName>, CrudRepository<TableLayout, TableName> {

    List<TableLayout> findFirstByTableNameAndUserPreferencesAndDeskId(TableName tableName, UserPreferences userPreferences, String deskId);

    TableLayout save(@NotNull TableLayout tableLayout);

    Optional<TableLayout> findById(@NotNull String id);

    @Transactional
    void deleteById(@NotNull String id);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(
            value = "DELETE FROM table_layout_column_list cl where cl.table_layout_column_list = ?1",
            nativeQuery = true
    )
    void deleteAllTableLayoutMetadataColumnByMetadataId(@Param("metadataId") @NotNull String metadataId);
}
