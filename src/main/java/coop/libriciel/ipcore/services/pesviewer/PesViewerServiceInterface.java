/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.pesviewer;

import coop.libriciel.ipcore.model.content.DocumentBuffer;
import org.jetbrains.annotations.NotNull;

import jakarta.servlet.http.HttpServletResponse;


public interface PesViewerServiceInterface {

    String BEAN_NAME = "pesViewerService";
    String PREFERENCES_PROVIDER_KEY = "services.pes-viewer.provider";


    void prepare(@NotNull DocumentBuffer documentBuffer, @NotNull HttpServletResponse response);

}
