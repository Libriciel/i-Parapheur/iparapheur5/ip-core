/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.pesviewer;

import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.utils.ForcedExceptionsConnector;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.IOUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.ResponseCookie;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletResponse;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URI;

import static coop.libriciel.ipcore.utils.RequestUtils.SUBSERVICE_REQUEST_LONG_TIMEOUT;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.HttpHeaders.LOCATION;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.MediaType.*;
import static org.springframework.security.config.Elements.HTTP;


@Log4j2
@Service(PesViewerServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = PesViewerServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = "pesviewer")
public class PesViewerService implements PesViewerServiceInterface {


    private static final String ROOT_CONTEXT = "bl-xemwebviewer";
    private static final String PREPARE = "prepare";


    // <editor-fold desc="Beans">


    private final PesViewerServiceProperties properties;


    @Autowired
    public PesViewerService(PesViewerServiceProperties properties) {
        this.properties = properties;
    }


    // </editor-fold desc="Beans">


    @Override
    public void prepare(@NotNull DocumentBuffer documentBuffer, @NotNull HttpServletResponse response) {

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(ROOT_CONTEXT).pathSegment(PREPARE)
                .build().normalize().toUri();

        log.info("prepare document:{} URI:{}", documentBuffer, requestUri);

        MultipartBodyBuilder builder = new MultipartBodyBuilder();
        // For some reason, header seems mandatory for the asyncPart, and not for the other ones.
        // Cheers to this article : https://dev.to/shavz/sending-multipart-form-data-using-spring-webtestclient-2gb7
        builder.asyncPart("file", documentBuffer.getContentFlux(), DataBuffer.class)
                .header(CONTENT_DISPOSITION, "form-data; name=file; filename=" + documentBuffer.getName());

        String redirection = WebClient.builder()
                .clientConnector(new ForcedExceptionsConnector(false))
                .build()
                .post().uri(requestUri)
                .contentType(MULTIPART_FORM_DATA).accept(ALL)
                .body(BodyInserters.fromMultipartData(builder.build()))
                .exchange()
                // Result
                .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT)
                .flatMap(clientResponse -> {

                    // We have to transfer cookies on the fly, especially the JSESSIONID
                    // to not not-lose the PES-viewer's internal session
                    clientResponse.cookies()
                            .entrySet()
                            .stream()
                            .flatMap(e -> e.getValue().stream())
                            .map(PesViewerService::toJavaxServletCookie)
                            .forEach(response::addCookie);

                    // Getting the redirect URL, that's what we wanted in the first place
                    return clientResponse.headers().header(LOCATION).stream().findFirst();
                })
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_pes_viewer_service"));


        if (!redirection.startsWith("http")) {
            redirection = UriComponentsBuilder.newInstance()
                    .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                    .path(ROOT_CONTEXT)
                    .pathSegment(redirection).build().normalize().toUriString();
        }

        log.debug("prepare redirection:{}", redirection);

        // Sending back the first response
        browse(redirection, response);
    }


    private void browse(@NotNull String url, @NotNull HttpServletResponse response) {

        String result = WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
                .get().uri(url)
                .retrieve()
                .bodyToMono(String.class)
                .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_pes_viewer_service"));

        response.setContentType(TEXT_HTML_VALUE);
        response.setContentLength(result.length());

        try (ByteArrayInputStream bais = new ByteArrayInputStream(result.getBytes())) {
            IOUtils.copyLarge(bais, response.getOutputStream());
            response.flushBuffer();
        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_reading_file");
        }
    }


    private static Cookie toJavaxServletCookie(ResponseCookie responseCookie) {

        Cookie cookie = new Cookie(responseCookie.getName(), responseCookie.getValue());
        cookie.setPath(responseCookie.getPath());

        long maxAgeLong = responseCookie.getMaxAge().getSeconds();
        maxAgeLong = Math.min(Integer.MAX_VALUE, maxAgeLong);
        maxAgeLong = Math.max(60, maxAgeLong);
        cookie.setMaxAge((int) maxAgeLong);

        return cookie;
    }

}
