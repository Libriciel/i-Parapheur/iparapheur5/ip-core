/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.pesviewer;

import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import org.jetbrains.annotations.NotNull;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import jakarta.servlet.http.HttpServletResponse;

import java.io.Serial;

import static coop.libriciel.ipcore.utils.TextUtils.NONE_SERVICE;
import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;


@Service(PesViewerServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = PesViewerServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = NONE_SERVICE)
public class NonePesViewerService implements PesViewerServiceInterface {


    private static class NoPesViewerServiceError extends LocalizedStatusException {

        private static final @Serial long serialVersionUID = 5726001589014908896L;


        public NoPesViewerServiceError() {
            super(SERVICE_UNAVAILABLE, "message.pes_viewer_service_not_available");
        }

    }


    @Override
    public void prepare(@NotNull DocumentBuffer documentBuffer, @NotNull HttpServletResponse response) {
        throw new NoPesViewerServiceError();
    }


}
