/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.monitoring;

import coop.libriciel.ipcore.model.monitoring.JobDataResponse;
import coop.libriciel.ipcore.model.monitoring.JobEnum;
import coop.libriciel.ipcore.model.monitoring.PrometheusResponse;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

import static coop.libriciel.ipcore.utils.RequestUtils.SUBSERVICE_REQUEST_SHORT_TIMEOUT;
import static org.springframework.security.config.Elements.HTTP;


@Log4j2
@Service(MonitoringServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = MonitoringServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = "prometheus")
public class PrometheusService implements MonitoringServiceInterface {


    private static final String PROMETHEUS_BASE_URL = "/api/v1/query";
    private static final String PROMETHEUS_JVM_MEMORY_USED_QUERY = "jvm_memory_used_bytes";
    private static final String PROMETHEUS_JVM_MEMORY_MAX_QUERY = "jvm_memory_max_bytes";
    private static final String PROMETHEUS_SYSTEM_CPU_USAGE_QUERY = "system_cpu_usage";
    private static final String PROMETHEUS_SYSTEM_CPU_COUNT_QUERY = "system_cpu_count";
    private static final String PROMETHEUS_NODE_FILESYSTEM_FREE_BYTES = "node_filesystem_free_bytes";
    private static final String PROMETHEUS_NODE_FILESYSTEM_SIZE_BYTES = "node_filesystem_size_bytes";


    // <editor-fold desc="Beans">


    private final MonitoringServiceProperties properties;


    @Autowired
    public PrometheusService(MonitoringServiceProperties properties) {
        this.properties = properties;
    }


    // </editor-fold desc="Beans">


    @Override
    public Long getLocalSpaceUsage() {
        long totalFreeSpace = this.getFreeSpace().getData().getResult().stream()
                .mapToLong(r -> Long.parseLong(r.getValue().get(1)))
                .sum();

        long totalTotalSpace = this.getTotalSpace().getData().getResult().stream()
                .mapToLong(r -> Long.parseLong(r.getValue().get(1)))
                .sum();

        return totalTotalSpace != 0 ? Math.round((totalTotalSpace - totalFreeSpace) * 100.0 / totalTotalSpace) : null;
    }


    @Override
    public JobDataResponse getJobData(@NotNull JobEnum job) {
        return this.mapPrometheusResponseToJobDataResponse(
                this.getJvmMemoryUsedData(),
                this.getJvmMemoryMaxData(),
                this.getSystemCpuUsageData(),
                this.getSystemCpuCountData(),
                job
        );
    }


    @Override
    public List<JobDataResponse> getJobsData() {
        return this.mapPrometheusResponseToDataResponse(
                this.getJvmMemoryUsedData(),
                this.getJvmMemoryMaxData(),
                this.getSystemCpuUsageData(),
                this.getSystemCpuCountData()
        );
    }


    private PrometheusResponse getFreeSpace() {
        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(PROMETHEUS_BASE_URL)
                .queryParam("query", PROMETHEUS_NODE_FILESYSTEM_FREE_BYTES)
                .build().normalize().toUri();
        return WebClient.builder()
                .build()
                .get()
                .uri(requestUri)
                .retrieve()
                .bodyToMono(PrometheusResponse.class)
                .block(SUBSERVICE_REQUEST_SHORT_TIMEOUT);
    }


    private PrometheusResponse getTotalSpace() {
        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(PROMETHEUS_BASE_URL)
                .queryParam("query", PROMETHEUS_NODE_FILESYSTEM_SIZE_BYTES)
                .build().normalize().toUri();
        return WebClient.builder()
                .build()
                .get()
                .uri(requestUri)
                .retrieve()
                .bodyToMono(PrometheusResponse.class)
                .block(SUBSERVICE_REQUEST_SHORT_TIMEOUT);
    }


    private PrometheusResponse getJvmMemoryUsedData() {
        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(PROMETHEUS_BASE_URL)
                .queryParam("query", PROMETHEUS_JVM_MEMORY_USED_QUERY)
                .build().normalize().toUri();
        return WebClient.builder()
                .build()
                .get()
                .uri(requestUri)
                .retrieve()
                .bodyToMono(PrometheusResponse.class)
                .block(SUBSERVICE_REQUEST_SHORT_TIMEOUT);
    }


    private PrometheusResponse getJvmMemoryMaxData() {
        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(PROMETHEUS_BASE_URL)
                .queryParam("query", PROMETHEUS_JVM_MEMORY_MAX_QUERY)
                .build().normalize().toUri();
        return WebClient.builder()
                .build()
                .get()
                .uri(requestUri)
                .retrieve()
                .bodyToMono(PrometheusResponse.class)
                .block(SUBSERVICE_REQUEST_SHORT_TIMEOUT);
    }


    private PrometheusResponse getSystemCpuUsageData() {
        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(PROMETHEUS_BASE_URL)
                .queryParam("query", PROMETHEUS_SYSTEM_CPU_USAGE_QUERY)
                .build().normalize().toUri();
        return WebClient.builder()
                .build()
                .get()
                .uri(requestUri)
                .retrieve()
                .bodyToMono(PrometheusResponse.class)
                .block(SUBSERVICE_REQUEST_SHORT_TIMEOUT);
    }


    private PrometheusResponse getSystemCpuCountData() {
        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(PROMETHEUS_BASE_URL)
                .queryParam("query", PROMETHEUS_SYSTEM_CPU_COUNT_QUERY)
                .build().normalize().toUri();
        return WebClient.builder()
                .build()
                .get()
                .uri(requestUri)
                .retrieve()
                .bodyToMono(PrometheusResponse.class)
                .block(SUBSERVICE_REQUEST_SHORT_TIMEOUT);
    }


    private List<JobDataResponse> mapPrometheusResponseToDataResponse(@NotNull PrometheusResponse usedMemory,
                                                                      @NotNull PrometheusResponse maxMemory,
                                                                      @NotNull PrometheusResponse cpuUsage,
                                                                      @NotNull PrometheusResponse cpuCount) {

        return Arrays.stream(JobEnum.values())
                .map(j -> this.mapPrometheusResponseToJobDataResponse(usedMemory, maxMemory, cpuUsage, cpuCount, j))
                .toList();
    }


    private JobDataResponse mapPrometheusResponseToJobDataResponse(@NotNull PrometheusResponse usedMemory,
                                                                   @NotNull PrometheusResponse maxMemory,
                                                                   @NotNull PrometheusResponse cpuUsage,
                                                                   @NotNull PrometheusResponse cpuCount,
                                                                   @NotNull JobEnum job) {

        double jvmMemoryUsageTotal = usedMemory.getData().getResult().stream()
                .filter(r -> StringUtils.equals(r.getMetric().getArea(), "heap"))
                .filter(r -> StringUtils.equals(r.getMetric().getJob(), job.getJobName()))
                .mapToDouble(r -> Double.parseDouble(r.getValue().get(1)))
                .sum();

        double jvmMemoryCountTotal = maxMemory.getData().getResult().stream()
                .filter(r -> StringUtils.equals(r.getMetric().getArea(), "heap"))
                .filter(r -> StringUtils.equals(r.getMetric().getJob(), job.getJobName()))
                .mapToDouble(r -> Double.parseDouble(r.getValue().get(1)))
                .sum();

        double systemCpuUsageTotal = cpuUsage.getData().getResult().stream()
                .filter(r -> StringUtils.equals(r.getMetric().getJob(), job.getJobName()))
                .mapToDouble(r -> Double.parseDouble(r.getValue().get(1)))
                .sum();

        double systemCpuCountTotal = cpuCount.getData().getResult().stream()
                .filter(r -> StringUtils.equals(r.getMetric().getJob(), job.getJobName()))
                .mapToDouble(r -> Double.parseDouble(r.getValue().get(1)))
                .sum();

        return new JobDataResponse(
                job,
                Math.round(jvmMemoryUsageTotal * 100 / jvmMemoryCountTotal),
                Math.round(systemCpuUsageTotal * 100 / systemCpuCountTotal)
        );
    }


}
