/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.monitoring;

import coop.libriciel.ipcore.model.monitoring.JobDataResponse;
import coop.libriciel.ipcore.model.monitoring.JobEnum;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import org.jetbrains.annotations.NotNull;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.io.Serial;
import java.util.List;

import static coop.libriciel.ipcore.utils.TextUtils.NONE_SERVICE;
import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;


@Service(MonitoringServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = MonitoringServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = NONE_SERVICE)
public class NoneMonitoringService implements MonitoringServiceInterface {


    private static class NoMonitoringServiceError extends LocalizedStatusException {

        private static final @Serial long serialVersionUID = 2985507191762942736L;


        public NoMonitoringServiceError() {
            super(SERVICE_UNAVAILABLE, "message.monitoring_service_not_available");
        }

    }


    @Override
    public JobDataResponse getJobData(@NotNull JobEnum job) {
        throw new NoMonitoringServiceError();
    }


    @Override
    public List<JobDataResponse> getJobsData() {
        throw new NoMonitoringServiceError();
    }


    @Override
    public Long getLocalSpaceUsage() {
        throw new NoMonitoringServiceError();
    }


}
