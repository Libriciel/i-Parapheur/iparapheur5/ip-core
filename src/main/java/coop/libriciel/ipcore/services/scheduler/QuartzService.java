/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
package coop.libriciel.ipcore.services.scheduler;

import coop.libriciel.ipcore.services.mail.NotificationJob;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.quartz.*;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

import static coop.libriciel.ipcore.services.mail.NotificationJob.DATA_MAP_KEY_USER_ID;
import static coop.libriciel.ipcore.services.mail.NotificationJob.TRIGGER_GROUP;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.impl.matchers.GroupMatcher.triggerGroupEquals;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;


@Log4j2
@Service(SchedulerServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = SchedulerServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = "quartz")
public class QuartzService implements SchedulerServiceInterface {


    // <editor-fold desc="Beans">


    private final Scheduler scheduler;


    public QuartzService(Scheduler scheduler) {
        this.scheduler = scheduler;
    }


    // </editor-fold desc="Beans">


    @Override
    public boolean hasScheduledNotifications(@NotNull String userId) {

        boolean result = false;
        try {
            result = scheduler.getTriggerKeys(triggerGroupEquals(TRIGGER_GROUP))
                    .stream()
                    .map(k -> {try {return scheduler.getTrigger(k);} catch (SchedulerException e) {return null;}})
                    .filter(Objects::nonNull)
                    .map(Trigger::getJobDataMap)
                    .filter(Objects::nonNull)
                    .map(dataMap -> dataMap.getString(DATA_MAP_KEY_USER_ID))
                    .filter(StringUtils::isNotEmpty)
                    .anyMatch(v -> StringUtils.equals(v, userId));
        } catch (SchedulerException e) {
            log.warn("cannot parse scheduler response", e);
        }

        return result;
    }


    @Override
    public void addNotificationScheduler(@NotNull String userId, @NotNull Date date) {
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put(DATA_MAP_KEY_USER_ID, userId);

        JobDetail jobDetail = JobBuilder
                .newJob(NotificationJob.class)
                .storeDurably(true)
                .build();


        Trigger trigger = TriggerBuilder
                .newTrigger()
                .usingJobData(jobDataMap)
                .startAt(date)
                .withIdentity(UUID.randomUUID().toString(), TRIGGER_GROUP)
                // This parameter will allow to restart the job, if missed.
                // By default, anything missed by less than 23h will be re-launched. See `org.quartz.jobStore.misfireThreshold` (in ms).
                // Anything less than a minute will be launched normally, and not be considered late at all by Quartz.
                .withSchedule(simpleSchedule().withMisfireHandlingInstructionFireNow())
                .build();


        try {
            Date targetDate = scheduler.scheduleJob(jobDetail, trigger);
            log.trace("addNotificationScheduler - scheduling job at date :  {}", targetDate);
        } catch (SchedulerException e) {
            log.debug("addNotificationScheduler - error scheduling job : ", e);
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_scheduler", e);
        }
    }


    @Override
    public void deleteExistingNotificationScheduler(@NotNull String userId) {
        log.trace("deleteExistingNotificationScheduler - deleting scheduled notification for user :  {}", userId);
        try {
            scheduler.getTriggerKeys(triggerGroupEquals(TRIGGER_GROUP))
                    .stream()
                    .map(k -> {try {return scheduler.getTrigger(k);} catch (SchedulerException e) {return null;}})
                    .filter(Objects::nonNull)
                    .filter(trigger -> trigger.getJobDataMap() != null)
                    .filter(trigger -> StringUtils.equals(trigger.getJobDataMap().getString(DATA_MAP_KEY_USER_ID), userId))
                    .map(Trigger::getKey)
                    .forEach(triggerKey -> {
                        try {
                            scheduler.unscheduleJob(triggerKey);
                        } catch (SchedulerException e) {
                            log.debug("deleteExistingNotificationScheduler - Error while un-scheduling trigger for user {}", userId);
                            log.trace("error details : ", e);
                        }
                    });
        } catch (SchedulerException e) {
            log.warn("cannot parse scheduler response", e);
        }
    }


}
