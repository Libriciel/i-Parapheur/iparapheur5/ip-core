/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.resolvers;

import coop.libriciel.ipcore.configuration.WebConfig;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import jakarta.servlet.http.HttpServletRequest;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Map;
import java.util.Optional;

import static coop.libriciel.ipcore.model.workflow.Action.UNDO;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.web.servlet.HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE;


/**
 * This is an utility class,
 * registering it in {@link WebConfig} will factorize the {@link Task} retrieving in controller classes.
 */
@Log4j2
public class TaskResolver implements HandlerMethodArgumentResolver {


    @Parameter(hidden = true)
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.PARAMETER)
    public @interface TaskResolved {}


    // <editor-fold desc="Beans">


    private final WorkflowServiceInterface workflowService;


    public TaskResolver(@NotNull WorkflowServiceInterface workflowService) {
        this.workflowService = workflowService;
    }


    // </editor-fold desc="Beans">


    @Override
    public boolean supportsParameter(MethodParameter methodParameter) {
        return methodParameter.getParameterAnnotation(TaskResolved.class) != null;
    }


    @Override
    public Task resolveArgument(@NotNull MethodParameter methodParameter, ModelAndViewContainer modelAndViewContainer,
                                @NotNull NativeWebRequest nativeWebRequest, WebDataBinderFactory webDataBinderFactory) {
        // noinspection unchecked
        Map<String, String> variables = (Map<String, String>) Optional.of(nativeWebRequest)
                .map(r -> r.getNativeRequest(HttpServletRequest.class))
                .map(r -> r.getAttribute(URI_TEMPLATE_VARIABLES_ATTRIBUTE))
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_read_request"));

        String taskId = variables.get(Task.API_PATH);
        log.debug("resolveArgument taskId:{}", taskId);

        Task task = Optional
                .of(workflowService.getTask(taskId))
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_task_id"));

        if (task.getDate() != null) {
            String errorMessageResource = (task.getAction() == UNDO)
                                          ? "message.the_undo_task_has_been_revoked"
                                          : "message.this_task_has_already_been_performed";
            throw new LocalizedStatusException(NOT_ACCEPTABLE, errorMessageResource);
        }

        return task;
    }


}
