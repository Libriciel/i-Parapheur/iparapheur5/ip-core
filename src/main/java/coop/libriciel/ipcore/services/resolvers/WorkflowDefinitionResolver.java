/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.resolvers;

import coop.libriciel.ipcore.configuration.WebConfig;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.workflow.WorkflowDefinition;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import io.swagger.v3.oas.annotations.Parameter;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Map;
import java.util.Optional;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.web.servlet.HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE;


/**
 * This is an utility class,
 * registering it in {@link WebConfig} will factorize the {@link WorkflowDefinition} retrieving in controller classes.
 */
@Log4j2
public class WorkflowDefinitionResolver implements HandlerMethodArgumentResolver {


    @Parameter(hidden = true)
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.PARAMETER)
    public @interface WorkflowDefinitionResolved {}


    // <editor-fold desc="Beans">


    private final WorkflowServiceInterface workflowService;


    public WorkflowDefinitionResolver(@NotNull WorkflowServiceInterface workflowService) {
        this.workflowService = workflowService;
    }


    // </editor-fold desc="Beans">


    @Override
    public boolean supportsParameter(MethodParameter methodParameter) {
        return methodParameter.getParameterAnnotation(WorkflowDefinitionResolved.class) != null;
    }


    @Override
    public WorkflowDefinition resolveArgument(@NotNull MethodParameter methodParameter,
                                              ModelAndViewContainer modelAndViewContainer,
                                              @NotNull NativeWebRequest nativeWebRequest,
                                              WebDataBinderFactory webDataBinderFactory) {
        // noinspection unchecked
        Map<String, String> variables = (Map<String, String>) Optional.of(nativeWebRequest)
                .map(r -> r.getNativeRequest(HttpServletRequest.class))
                .map(r -> r.getAttribute(URI_TEMPLATE_VARIABLES_ATTRIBUTE))
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_read_request"));

        String tenantId = variables.get(Tenant.API_PATH);
        String workflowDefinitionId = variables.get(WorkflowDefinition.API_ID_PATH);
        String workflowDefinitionKey = variables.get(WorkflowDefinition.API_KEY_PATH);
        String currentDeskId = variables.get(DeskRepresentation.API_PATH);
        log.trace("WorkflowDefinitionResolver resolveArgument workflowDefinitionId:{} workflowDefinitionKey:{} currentDeskId:{}",
                workflowDefinitionId, workflowDefinitionKey, currentDeskId);

        if (StringUtils.isNotEmpty(workflowDefinitionId)) {
            return Optional.ofNullable(workflowService.getWorkflowDefinitionById(tenantId, workflowDefinitionId))
                    .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_workflow_definition_id"));
        } else if (StringUtils.isNotEmpty(workflowDefinitionKey)) {
            return workflowService.getWorkflowDefinitionByKeyAndMapPlaceholders(tenantId, workflowDefinitionKey, currentDeskId)
                    .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_workflow_definition_key"));
        } else {
            throw new LocalizedStatusException(NOT_FOUND, "message.unknown_workflow_definition_id");
        }
    }


}
