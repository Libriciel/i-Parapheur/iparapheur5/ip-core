/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.resolvers;

import coop.libriciel.ipcore.configuration.WebConfig;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.workflow.Action;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.State;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.services.resolvers.FolderResolver.FolderResolved.Permission;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.FolderUtils;
import coop.libriciel.ipcore.utils.KeycloakSecurityUtils;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import io.swagger.v3.oas.annotations.Parameter;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.*;

import static coop.libriciel.ipcore.model.workflow.FolderVisibility.PUBLIC;
import static coop.libriciel.ipcore.model.workflow.State.CURRENT;
import static coop.libriciel.ipcore.model.workflow.State.PENDING;
import static coop.libriciel.ipcore.services.resolvers.FolderResolver.FolderResolved.Permission.*;
import static coop.libriciel.ipcore.utils.KeycloakSecurityUtils.getCurrentSessionUserIdNoException;
import static java.util.Collections.singleton;
import static java.util.stream.Collectors.toSet;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.web.servlet.HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE;


/**
 * This is an utility class,
 * registering it in {@link WebConfig} will factorize the {@link Folder} retrieving in controller classes.
 */
@Log4j2
public class FolderResolver implements HandlerMethodArgumentResolver {


    @Parameter(hidden = true)
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.PARAMETER)
    public @interface FolderResolved {

        /**
         * Some access (like performing a task) require a permission check on current tasks.
         * Other access type (like downloading a PDF), only require to be a member of -one of the desks- referenced in the workflow
         */
        enum Permission {CURRENT_DESK, WORKFLOW_DESK, IGNORE}

        Permission permission() default IGNORE;

        boolean withHistory() default true;

    }


    // <editor-fold desc="Beans">


    private final PermissionServiceInterface permissionService;
    private final WorkflowServiceInterface workflowService;


    public FolderResolver(PermissionServiceInterface permissionService,
                          WorkflowServiceInterface workflowService) {
        this.permissionService = permissionService;
        this.workflowService = workflowService;
    }


    // </editor-fold desc="Beans">


    @Override
    public boolean supportsParameter(MethodParameter methodParameter) {
        return methodParameter.getParameterAnnotation(FolderResolved.class) != null;
    }


    @Override
    public Folder resolveArgument(@NotNull MethodParameter methodParameter, ModelAndViewContainer modelAndViewContainer,
                                  @NotNull NativeWebRequest nativeWebRequest, WebDataBinderFactory webDataBinderFactory) {
        // noinspection unchecked
        Map<String, String> variables = (Map<String, String>) Optional.of(nativeWebRequest)
                .map(r -> r.getNativeRequest(HttpServletRequest.class))
                .map(r -> r.getAttribute(URI_TEMPLATE_VARIABLES_ATTRIBUTE))
                .orElseThrow(() -> new LocalizedStatusException(BAD_REQUEST, "message.cannot_read_request"));

        Map<String, String[]> queryParams = Optional.of(nativeWebRequest)
                .map(r -> r.getNativeRequest(HttpServletRequest.class))
                .map(ServletRequest::getParameterMap)
                .orElse(new HashMap<>());

        Permission permissionType = Optional.ofNullable(methodParameter.getParameterAnnotation(FolderResolved.class))
                .map(FolderResolved::permission)
                .orElse(IGNORE);

        boolean withHistory = Optional.ofNullable(methodParameter.getParameterAnnotation(FolderResolved.class))
                .map(FolderResolved::withHistory)
                .orElse(true);
        String[] queryParamValues = queryParams.getOrDefault("asDeskId", new String[0]);

        String tenantId = variables.get(Tenant.API_PATH);
        String folderId = variables.get(Folder.API_PATH);
        String deskId = Optional.ofNullable(variables.get(DeskRepresentation.API_PATH))
                .or(() -> Arrays.stream(queryParamValues).findFirst())
                .orElse(null);

        log.debug("resolveArgument tenantId:{} folderId:{} deskId:{}", tenantId, folderId, deskId);

        if (StringUtils.isAnyEmpty(tenantId, folderId)) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.missing_folder_id");
        }

        Folder folder = workflowService.getFolder(folderId, tenantId, withHistory);

        // If we're looking for a Folder from a specific context, some of the PENDING task should be set as CURRENT.
        // This check is heavily permission-related, and can be done alongside the visibility permission check.

        boolean shouldCheckPermissions = permissionType != IGNORE;

        if (deskId != null && shouldCheckPermissions) {
            Set<String> deskIdSet = singleton(deskId);
            String typeId = folder.getType().getId();
            String subtypeId = folder.getSubtype().getId();

            boolean hasActionPermission = permissionService.currentUserHasFolderActionRightOnSomeDeskIn(tenantId, deskIdSet, typeId, subtypeId);
            boolean hasArchiveActionPermission = permissionService.currentUserHasArchivingRightOnSomeDeskIn(tenantId, deskIdSet, typeId, subtypeId);

            List<Task> pendingTasks = folder.getStepList().stream()
                    .filter(task -> task.getState() == PENDING)
                    .filter(task -> task.getDesks().stream().map(DeskRepresentation::getId).anyMatch(id -> StringUtils.equals(id, deskId)))
                    .toList();

            if (hasActionPermission) {
                pendingTasks.stream()
                        .filter(task -> task.getAction() != Action.ARCHIVE)
                        .forEach(task -> task.setState(CURRENT));
            }

            if (hasArchiveActionPermission) {
                pendingTasks.stream()
                        .filter(task -> task.getAction() == Action.ARCHIVE)
                        .forEach(task -> task.setState(CURRENT));
            }

        }


        // Authorization check

        log.debug("permissionType : {}", permissionType);
        if (permissionType == IGNORE || folder.getVisibility() == PUBLIC) {
            return folder;
        }

        boolean isAdmin = KeycloakSecurityUtils.isSuperAdmin() || KeycloakSecurityUtils.currentUserIsTenantAdmin(tenantId);
        if (isAdmin) {
            return folder;
        }

        if (KeycloakSecurityUtils.isFunctionalAdmin(tenantId)) {

            String userId = getCurrentSessionUserIdNoException();
            List<DeskRepresentation> deskList = permissionService.getAdministeredDesks(userId, tenantId);
            List<DeskRepresentation> deskRepresentationList = folder.getStepList().stream()
                    .filter(s -> s.getState() == State.PENDING || s.getState() == State.CURRENT)
                    .flatMap(s -> s.getDesks().stream()).toList();
            boolean hasDesksRight = deskRepresentationList.stream().anyMatch(deskList::contains);

            if (hasDesksRight) {
                return folder;
            }
        }

        if (permissionType == CURRENT_DESK) {
            Set<String> currentlyConcernedDeskIds = folder.getStepList().stream()
                    .filter(t -> FolderUtils.isActive(t.getState()))
                    .map(Task::getDesks)
                    .flatMap(Collection::stream)
                    .map(DeskRepresentation::getId)
                    .collect(toSet());
            log.debug("currentlyConcernedDeskIds : {}", currentlyConcernedDeskIds);

            boolean isCurrentlyConcerned = permissionService
                    .currentUserHasViewingRightOnSomeDeskIn(tenantId, currentlyConcernedDeskIds, folder.getType().getId(), folder.getSubtype().getId());
            if (!isCurrentlyConcerned) {
                throw new LocalizedStatusException(FORBIDDEN, "message.unauthorized_access");
            }
        }

        if (permissionType == WORKFLOW_DESK) {
            Set<String> workflowConcernedDeskIds = folder.getStepList().stream()
                    .map(Task::getDesks)
                    .flatMap(Collection::stream)
                    .map(DeskRepresentation::getId)
                    .collect(toSet());

            log.debug("workflowConcernedDeskIds : {}", workflowConcernedDeskIds);
            boolean isWorkflowConcerned = permissionService
                    .currentUserHasViewingRightOnSomeDeskIn(tenantId, workflowConcernedDeskIds, folder.getType().getId(), folder.getSubtype().getId());
            if (!isWorkflowConcerned) {
                throw new LocalizedStatusException(FORBIDDEN, "message.unauthorized_access");
            }
        }

        // Return result

        return folder;
    }


}
