/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.externalsignature;

import coop.libriciel.external.signature.connector.api.model.FileWrapper;
import coop.libriciel.external.signature.connector.api.model.PdfVersion;
import coop.libriciel.external.signature.connector.api.model.SignatureInfo;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.content.ValidatedSignatureInformation;
import coop.libriciel.ipcore.model.crypto.PdfExtSignaturePosition;
import coop.libriciel.ipcore.model.crypto.PdfSignaturePosition;
import coop.libriciel.ipcore.model.database.ExternalSignatureConfig;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.userPreferences.UserPreferences;
import coop.libriciel.ipcore.model.externalsignature.ExternalSignatureParams;
import coop.libriciel.ipcore.model.externalsignature.Status;
import coop.libriciel.ipcore.model.externalsignature.response.ExternalSignatureProcedure;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.model.workflow.requests.SimpleTaskParams;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.database.ExternalSignatureConfigRepository;
import coop.libriciel.ipcore.services.database.SubtypeRepository;
import coop.libriciel.ipcore.services.database.TypeRepository;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.CryptoUtils;
import coop.libriciel.ipcore.utils.FolderUtils;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.time.Duration;
import java.util.*;
import java.util.stream.Stream;

import static coop.libriciel.external.signature.connector.api.model.PdfVersion.*;
import static coop.libriciel.ipcore.model.externalsignature.Status.ACTIVE;
import static coop.libriciel.ipcore.model.externalsignature.Status.ERROR;
import static coop.libriciel.ipcore.model.workflow.Action.*;
import static java.net.URLDecoder.decode;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.modelmapper.config.Configuration.AccessLevel.PRIVATE;
import static org.modelmapper.convention.MatchingStrategies.STRICT;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.*;
import static org.springframework.security.config.Elements.HTTP;


@Log4j2
@Service(ExternalSignatureInterface.BEAN_NAME)
@ConditionalOnProperty(name = ExternalSignatureInterface.PREFERENCES_PROVIDER_KEY, havingValue = "externalsignature")
public class ExternalSignatureService implements ExternalSignatureInterface {

    // This duration should be equal to the timeout of the requests on external-signature-connector
    Duration EXTERNAL_SIGNATURE_REQUEST_TIMEOUT = Duration.ofMinutes(2);

    // This duration is used for the procedure creation request as it groups multiple 2 minutes timeout requests on the external-signature-connector's side
    Duration EXTERNAL_SIGNATURE_PROCEDURE_CREATION_REQUEST_TIMEOUT = Duration.ofMinutes(4);


    private static final String EXTERNAL_SIGNATURE_CONNECTOR_BASE_URL = "external-signature-connector";
    private static final String EXTERNAL_SIGNATURE_CONNECTOR_URL_CREATE_PROCEDURE = EXTERNAL_SIGNATURE_CONNECTOR_BASE_URL + "/createProcedure";
    private static final String EXTERNAL_SIGNATURE_CONNECTOR_URL_GET_DOCUMENT = EXTERNAL_SIGNATURE_CONNECTOR_BASE_URL + "/getSignedDocument";
    private static final String EXTERNAL_SIGNATURE_CONNECTOR_URL_GET_PROOF = EXTERNAL_SIGNATURE_CONNECTOR_BASE_URL + "/getProof";
    private static final String EXTERNAL_SIGNATURE_CONNECTOR_URL_GET_PROOF_FILE_EXTENSION = EXTERNAL_SIGNATURE_CONNECTOR_BASE_URL + "/getProofFileExtension";
    private static final String EXTERNAL_SIGNATURE_CONNECTOR_URL_GET_DOCUMENT_ID = EXTERNAL_SIGNATURE_CONNECTOR_BASE_URL + "/getSignedDocumentId";
    private static final String EXTERNAL_SIGNATURE_CONNECTOR_URL_GET_REJECTION_REASON = EXTERNAL_SIGNATURE_CONNECTOR_BASE_URL + "/getRejectionReason";
    private static final String EXTERNAL_SIGNATURE_CONNECTOR_URL_GET_FOLDER_ID = EXTERNAL_SIGNATURE_CONNECTOR_BASE_URL + "/getFolderId";
    private static final String EXTERNAL_SIGNATURE_CONNECTOR_URL_REJECT_PROCEDURE = EXTERNAL_SIGNATURE_CONNECTOR_BASE_URL + "/reject";
    private static final String EXTERNAL_SIGNATURE_CONNECTOR_URL_GET_STATUS = EXTERNAL_SIGNATURE_CONNECTOR_BASE_URL + "/status";
    private static final String EXTERNAL_SIGNATURE_CONNECTOR_URL_GET_PROCEDURE_DATA = EXTERNAL_SIGNATURE_CONNECTOR_BASE_URL + "/getProcedureData";
    private static final String EXTERNAL_SIGNATURE_CONNECTOR_URL_GET_FILE_IDS = EXTERNAL_SIGNATURE_CONNECTOR_BASE_URL + "/fileIds";
    private static final String EXTERNAL_SIGNATURE_CONNECTOR_URL_TEST_SERVICE = EXTERNAL_SIGNATURE_CONNECTOR_BASE_URL + "/testService";


    private static final ModelMapper externalSignatureModelMapper = initializeInternalModelMapper();


    // <editor-fold desc="Beans">


    private final WorkflowServiceInterface workflowService;
    private final SubtypeRepository subtypeRepository;
    private final TypeRepository typeRepository;
    private final ExternalSignatureConfigRepository externalSignatureConfigRepository;
    private final ContentServiceInterface contentService;
    private final ExternalSignatureProperties properties;


    @Autowired
    public ExternalSignatureService(WorkflowServiceInterface workflowService,
                                    SubtypeRepository subtypeRepository,
                                    TypeRepository typeRepository,
                                    ExternalSignatureProperties properties,
                                    ExternalSignatureConfigRepository externalSignatureConfigRepository,
                                    ContentServiceInterface contentService) {
        this.workflowService = workflowService;
        this.subtypeRepository = subtypeRepository;
        this.typeRepository = typeRepository;
        this.externalSignatureConfigRepository = externalSignatureConfigRepository;
        this.properties = properties;
        this.contentService = contentService;
    }


    // </editor-fold desc="Beans">


    /**
     * We don't want to pollute the main ModelMapper with the lib-specific sub-objects.
     * So we confine every setting in here.
     */
    static ModelMapper initializeInternalModelMapper() {

        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration()
                .setFieldMatchingEnabled(true)
                .setFieldAccessLevel(PRIVATE)
                .setMatchingStrategy(STRICT);

        // Specific objects

        // The ModelMapper needs the declaration for every parent and child classes.
        // Casting it on the serialization/deserialization is not enough.
        // The mapper conf can be factorized, though.
        Stream.of(Document.class, DocumentBuffer.class)
                .forEach(clazz -> mapper
                        .typeMap(clazz, FileWrapper.class)
                        .addMappings(m -> m
                                .using((Converter<Float, PdfVersion>) context -> convertToLibPdfVersion(context.getSource()))
                                .map(Document::getMediaVersion, coop.libriciel.external.signature.connector.api.model.FileWrapper::setPdfVersion)
                        )
                        .addMappings(m -> m.map(
                                Document::getEmbeddedSignatureInfos,
                                coop.libriciel.external.signature.connector.api.model.FileWrapper::setEmbeddedSignatures)
                        )
                );

        mapper.typeMap(ValidatedSignatureInformation.class, SignatureInfo.class)
                .addMappings(m -> m.map(
                        ValidatedSignatureInformation::getPrincipalIssuer,
                        SignatureInfo::setName
                ))
                .addMappings(m -> m.map(
                        ValidatedSignatureInformation::getPrincipalSubjectIssuer,
                        SignatureInfo::setIssuerName
                ));

        return mapper;
    }


    static @Nullable PdfVersion convertToLibPdfVersion(@Nullable Float pdfVersion) {
        return Optional.ofNullable(pdfVersion)
                .map(version -> String.valueOf(pdfVersion))
                .map(version -> switch (version) {
                    case "1.0" -> V1_0;
                    case "1.1" -> V1_1;
                    case "1.2" -> V1_2;
                    case "1.3" -> V1_3;
                    case "1.4" -> V1_4;
                    case "1.5" -> V1_5;
                    case "1.6" -> V1_6;
                    case "1.7" -> V1_7;
                    case "2.0" -> V2_0;
                    default -> null;
                })
                .orElse(null);
    }


    @Scheduled(cron = "${services.external-signature.cron:-}")
    public void checkIfFilesAreSigned() {
        checkIfFilesAreSigned(this.externalSignatureConfigRepository);
    }


    @Override
    public @NotNull String createProcedure(@NotNull Folder folder,
                                           @NotNull ExternalSignatureParams externalSignatureParams,
                                           @NotNull List<Document> documents,
                                           @NotNull String subtypeId) {

        ExternalSignatureConfig externalSignatureConfig = this.subtypeRepository
                .findById(subtypeId)
                .map(Subtype::getExternalSignatureConfig)
                .orElseThrow(() -> new LocalizedStatusException(BAD_REQUEST, "message.unknown_extsig_config_id"));

        MultipartBodyBuilder builder = new MultipartBodyBuilder();
        externalSignatureParams.setFolderId(folder.getId());
        builder.part("requestConfig", externalSignatureConfig);
        List<FileWrapper> fileWrappers = new ArrayList<>();
        for (Document document : documents) {

            DocumentBuffer documentBuffer = new DocumentBuffer(document);
            documentBuffer.setContentFlux(this.contentService.retrieveContent(document.getId()).getContentFlux());

            builder.asyncPart("files", documentBuffer.getContentFlux(), DataBuffer.class)
                    .header(CONTENT_DISPOSITION, "form-data; name=files; filename=" + document.getName());

            FileWrapper fileWrapper = externalSignatureModelMapper.map(documentBuffer, FileWrapper.class);
            fileWrappers.add(fileWrapper);
        }

        externalSignatureParams.setFileWrappers(fileWrappers);
        Map<String, PdfSignaturePosition> documentIdsToSignaturePlacementMapping = new HashMap<>();

        folder.setType(typeRepository.findById(folder.getType().getId())
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.unknown_type_id")));

        documents.forEach(document -> documentIdsToSignaturePlacementMapping.put(
                document.getId(),
                new PdfExtSignaturePosition(CryptoUtils.getPosition(folder, document, new UserPreferences(), SIGNATURE))
        ));
        externalSignatureParams.setDocumentIdsToSignaturePlacementMapping(documentIdsToSignaturePlacementMapping);

        builder.part("signRequest", externalSignatureParams);

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(EXTERNAL_SIGNATURE_CONNECTOR_URL_CREATE_PROCEDURE)
                .build().normalize().toUri();
        ExternalSignatureProcedure procedure = WebClient.builder()
                .build()
                .post()
                .uri(requestUri)
                .contentType(MULTIPART_FORM_DATA)
                .accept(APPLICATION_JSON)
                .body(BodyInserters.fromMultipartData(builder.build()))
                .retrieve()
                .onStatus(HttpStatusCode::isError, this::setError)
                .bodyToMono(ExternalSignatureProcedure.class)
                .blockOptional(EXTERNAL_SIGNATURE_PROCEDURE_CREATION_REQUEST_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_external_signature_service"));

        if (Objects.isNull(externalSignatureConfig.getTransactionIds())) {
            externalSignatureConfig.setTransactionIds(new HashSet<>());
        }

        externalSignatureConfig.getTransactionIds().add(procedure.getId());

        externalSignatureConfigRepository.save(externalSignatureConfig);

        return procedure.getId();
    }


    @Override
    public void revokeExternalProcedure(@NotNull ExternalSignatureConfig externalSignatureConfig, @NotNull String procedureId) {
        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(EXTERNAL_SIGNATURE_CONNECTOR_URL_REJECT_PROCEDURE)
                .queryParam("procedureId", procedureId)
                .build().normalize().toUri();
        WebClient.builder()
                .build()
                .post()
                .uri(requestUri)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .bodyValue(externalSignatureConfig)
                .exchangeToMono(clientResponse -> {
                    if (clientResponse.statusCode().isError()) {
                        throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_external_signature_service");
                    } else {
                        return clientResponse.toBodilessEntity();
                    }
                })
                .block(EXTERNAL_SIGNATURE_REQUEST_TIMEOUT);
    }


    @Override
    public @NotNull Status getProcedureStatus(@NotNull ExternalSignatureConfig externalSignatureConfig, @NotNull String procedureId) {
        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(EXTERNAL_SIGNATURE_CONNECTOR_URL_GET_STATUS)
                .queryParam("procedureId", procedureId)
                .build().normalize().toUri();
        return WebClient.builder()
                .build()
                .post()
                .uri(requestUri)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .bodyValue(externalSignatureConfig)
                .retrieve()
                .onStatus(HttpStatusCode::isError, this::setError)
                .bodyToMono(Status.class)
                .onErrorResume(throwable -> Mono.just(ERROR))
                .blockOptional(EXTERNAL_SIGNATURE_REQUEST_TIMEOUT)
                .orElse(ERROR);
    }


    @Override
    public ExternalSignatureProcedure getProcedureData(@NotNull ExternalSignatureConfig externalSignatureConfig, @NotNull String procedureId) {
        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(EXTERNAL_SIGNATURE_CONNECTOR_URL_GET_PROCEDURE_DATA)
                .queryParam("procedureId", procedureId)
                .build().normalize().toUri();
        return WebClient.builder()
                .build()
                .post()
                .uri(requestUri)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .bodyValue(externalSignatureConfig)
                .retrieve()
                .onStatus(HttpStatusCode::isError, this::setError)
                .bodyToMono(ExternalSignatureProcedure.class)
                .blockOptional(EXTERNAL_SIGNATURE_REQUEST_TIMEOUT)
                .orElseThrow();
    }


    @Override
    public List<DocumentBuffer> getSignedDocumentList(@NotNull List<String> fileIds,
                                                      @NotNull ExternalSignatureConfig externalSignatureConfig,
                                                      @NotNull String procedureId) {
        List<DocumentBuffer> documentBuffers = new ArrayList<>();
        for (String fileId : fileIds) {
            documentBuffers.add(getSignedDocument(externalSignatureConfig, fileId, procedureId));
        }
        return documentBuffers;
    }


    @Override
    public void updateFilesAndPerformTask(@NotNull ExternalSignatureConfig externalSignatureConfig,
                                          @NotNull User user,
                                          @NotNull String procedureId) {
        List<DocumentBuffer> documentBuffers = this.getSignedDocumentList(
                this.getProcedureFileIds(externalSignatureConfig, procedureId),
                externalSignatureConfig,
                procedureId
        );

        Folder folder = this.getFolder(externalSignatureConfig, procedureId);

        for (DocumentBuffer documentBuffer : documentBuffers) {
            log.debug("Saving file : {}", documentBuffer.getId());

            if (documentBuffer.getId() == null) {
                List<Document> documents = this.contentService.getDocumentList(folder.getContentId());
                documentBuffer.setId(
                        documents
                                .stream()
                                .filter(document -> document.getName().equals(decode(documentBuffer.getName(), UTF_8)))
                                .findFirst()
                                .map(Document::getId)
                                .orElseThrow(() -> {
                                    log.warn("Cannot map externally signed document to its original");
                                    return new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_find_signed_document_origin");
                                })
                );
            }

            contentService.updateDocument(documentBuffer.getId(), documentBuffer, false);
        }

        Task task = this.getTaskFromFolder(folder, procedureId)
                .orElseThrow(() -> {
                    log.error("Severe error : cannot find task for folder {} and external signature procedure id {}", folder.getId(), procedureId);
                    log.error("Despite that, the documents have been updated with the externally signed ones");
                    return new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_find_task_for_external_signature_procedure");
                });
        addProofFileToFolder(folder, externalSignatureConfig, task.getId(), procedureId);

        List<Document> documents = this.contentService.getDocumentList(folder.getContentId());
        contentService.removeFirstSignaturePlacementAnnotation(documents);

        this.workflowService.performExternalSignature(task, user, folder, null, procedureId);
    }


    @Override
    public void rejectFolder(@NotNull ExternalSignatureConfig externalSignatureConfig, @NotNull User user, @NotNull String procedureId) {

        Folder folder;
        try {
            folder = this.getFolder(externalSignatureConfig, procedureId);
        } catch (LocalizedStatusException e) {
            if (e.getStatusCode() == NOT_FOUND) {
                log.warn("The folder seems to have been deleted, and the external signature status will be skipped...");
                return;
            } else {
                log.error("Error while retrieving a folder for an external signature check...");
                throw e;
            }
        }

        this.getTaskFromFolder(folder, procedureId)
                .ifPresent(t -> {
                            String reason = this.getProcedureRejectionReason(externalSignatureConfig, procedureId);

                            SimpleTaskParams params = new SimpleTaskParams();
                            params.setPublicAnnotation(reason);

                            this.workflowService.performTask(
                                    t,
                                    REJECT,
                                    user.getId(),
                                    folder,
                                    null,
                                    procedureId,
                                    null,
                                    null,
                                    params,
                                    null,
                                    null
                            );
                        }

                );
    }


    @Override
    public @NotNull String getProcedureRejectionReason(@NotNull ExternalSignatureConfig externalSignatureConfig, @NotNull String procedureId) {
        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(EXTERNAL_SIGNATURE_CONNECTOR_URL_GET_REJECTION_REASON)
                .queryParam("procedureId", procedureId)
                .build().normalize().toUri();
        return Optional.ofNullable(WebClient.builder()
                        .build()
                        .post()
                        .uri(requestUri)
                        .contentType(APPLICATION_JSON)
                        .bodyValue(externalSignatureConfig)
                        .retrieve()
                        .onStatus(HttpStatusCode::isError, this::setError)
                        .bodyToMono(String.class)
                        .block(EXTERNAL_SIGNATURE_REQUEST_TIMEOUT))
                .orElse("");
    }


    @Override
    public void rejectExternalProcedure(@NotNull Folder folder, @NotNull Task task) {
        if (task.getAction().equals(EXTERNAL_SIGNATURE)) {
            ExternalSignatureConfig extSigConf = subtypeRepository
                    .findById(folder.getSubtype().getId())
                    .map(Subtype::getExternalSignatureConfig)
                    .orElse(null);

            if (extSigConf == null) {return;}

            folder.getStepList().stream()
                    .map(Task::getExternalSignatureProcedureId)
                    .filter(StringUtils::isNotEmpty)
                    .filter(id -> this.getProcedureStatus(extSigConf, id) == ACTIVE)
                    .forEach(id -> this.revokeExternalProcedure(extSigConf, id));
        }
    }


    @Override
    public void testService(@NotNull ExternalSignatureConfig externalSignatureConfig) {
        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(EXTERNAL_SIGNATURE_CONNECTOR_URL_TEST_SERVICE)
                .build().normalize().toUri();
        ClientResponse clientResponse = WebClient.builder()
                .build()
                .post()
                .uri(requestUri)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .bodyValue(externalSignatureConfig)
                .exchange()
                .block(EXTERNAL_SIGNATURE_REQUEST_TIMEOUT);


        if (clientResponse == null || !clientResponse.statusCode().isError()) {
            return;
        }

        String message = Optional.ofNullable(clientResponse.bodyToMono(Exception.class).block(EXTERNAL_SIGNATURE_REQUEST_TIMEOUT))
                .map(Exception::getMessage)
                .orElse(StringUtils.EMPTY);

        log.debug(
                "An error occurred while testing an external signature config, id: {}, url: {}, service name: {}, error message: {}",
                externalSignatureConfig.getId(),
                externalSignatureConfig.getUrl(),
                externalSignatureConfig.getServiceName(),
                message
        );

        throw new ResponseStatusException(
                INTERNAL_SERVER_ERROR,
                message
        );
    }


    private DocumentBuffer getSignedDocument(ExternalSignatureConfig externalSignatureConfig, String fileId, String procedureId) {
        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(EXTERNAL_SIGNATURE_CONNECTOR_URL_GET_DOCUMENT)
                .queryParam("fileId", fileId)
                .queryParam("procedureId", procedureId)
                .build().normalize().toUri();
        DocumentBuffer result = this.getDocument(requestUri, externalSignatureConfig);
        result.setId(this.getSignedDocumentId(externalSignatureConfig, fileId, procedureId));
        return result;
    }


    private DocumentBuffer getDocument(URI requestUri, ExternalSignatureConfig config) {
        DocumentBuffer result = new DocumentBuffer();

        ClientResponse clientResponse = WebClient.builder()
                .build()
                .post()
                .uri(requestUri)
                .contentType(APPLICATION_JSON)
                .accept(ALL)
                .bodyValue(config)
                .exchange()
                .doOnSuccess(cr -> result.setName(
                        cr.headers()
                                .header(CONTENT_DISPOSITION)
                                .stream()
                                .findFirst()
                                .map(contentDispositionHeaderValue -> {
                                    String quotedFilename = Arrays.stream(contentDispositionHeaderValue.split("; "))
                                            .filter(part -> part.contains("filename="))
                                            .findFirst()
                                            .orElse("")
                                            .replaceFirst("filename=", "");

                                    return quotedFilename.substring(1, quotedFilename.length() - 1);
                                })
                                .orElse(null)))
                .doOnSuccess(cr -> result.setMediaType(cr.headers().contentType().orElse(APPLICATION_OCTET_STREAM)))
                .doOnSuccess(cr -> result.setContentLength(cr.headers().contentLength().orElse(-1L)))
                .blockOptional(EXTERNAL_SIGNATURE_REQUEST_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_external_signature_service"));

        result.setContentFlux(clientResponse.bodyToFlux(DataBuffer.class));
        return result;
    }


    private String getSignedDocumentId(ExternalSignatureConfig externalSignatureConfig, String fileId, String procedureId) {
        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(EXTERNAL_SIGNATURE_CONNECTOR_URL_GET_DOCUMENT_ID)
                .queryParam("fileId", fileId)
                .queryParam("procedureId", procedureId)
                .build().normalize().toUri();
        return WebClient.builder()
                .build()
                .post()
                .uri(requestUri)
                .contentType(APPLICATION_JSON)
                .accept(MULTIPART_FORM_DATA)
                .bodyValue(externalSignatureConfig)
                .retrieve()
                .onStatus(HttpStatusCode::isError, this::setError)
                .bodyToMono(String.class)
                .block(EXTERNAL_SIGNATURE_REQUEST_TIMEOUT);
    }


    private String getFolderId(ExternalSignatureConfig externalSignatureConfig, String procedureId) {
        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(EXTERNAL_SIGNATURE_CONNECTOR_URL_GET_FOLDER_ID)
                .queryParam("procedureId", procedureId)
                .build().normalize().toUri();
        String folderId = WebClient.builder()
                .build()
                .post()
                .uri(requestUri)
                .contentType(APPLICATION_JSON)
                .accept(MULTIPART_FORM_DATA)
                .bodyValue(externalSignatureConfig)
                .retrieve()
                .onStatus(HttpStatusCode::isError, this::setError)
                .bodyToMono(String.class)
                .block(EXTERNAL_SIGNATURE_REQUEST_TIMEOUT);
        log.debug("Folder id received : " + folderId);
        return folderId;
    }


    private List<String> getProcedureFileIds(ExternalSignatureConfig externalSignatureConfig, String procedureId) {
        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(EXTERNAL_SIGNATURE_CONNECTOR_URL_GET_FILE_IDS)
                .queryParam("procedureId", procedureId)
                .build().normalize().toUri();
        return WebClient.builder()
                .build()
                .post()
                .uri(requestUri)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .bodyValue(externalSignatureConfig)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<String>>() {})
                .block(EXTERNAL_SIGNATURE_REQUEST_TIMEOUT);
    }


    private DocumentBuffer getProofFile(ExternalSignatureConfig externalSignatureConfig, String procedureId) {
        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(EXTERNAL_SIGNATURE_CONNECTOR_URL_GET_PROOF)
                .queryParam("procedureId", procedureId)
                .build().normalize().toUri();

        return this.getDocument(requestUri, externalSignatureConfig);
    }


    private String getProofFileExtension(ExternalSignatureConfig externalSignatureConfig) {
        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(EXTERNAL_SIGNATURE_CONNECTOR_URL_GET_PROOF_FILE_EXTENSION)
                .build().normalize().toUri();

        return WebClient.builder()
                .build()
                .post()
                .uri(requestUri)
                .bodyValue(externalSignatureConfig)
                .retrieve()
                .onStatus(HttpStatusCode::isError, this::setError)
                .bodyToMono(String.class)
                .blockOptional(EXTERNAL_SIGNATURE_REQUEST_TIMEOUT)
                .orElse(Strings.EMPTY);
    }


    private void addProofFileToFolder(@NotNull Folder folder,
                                      @NotNull ExternalSignatureConfig externalSignatureConfig,
                                      @NotNull String taskId,
                                      @NotNull String procedureId) {
        DocumentBuffer documentBuffer = this.getProofFile(externalSignatureConfig, procedureId);
        String proofFileExtension = this.getProofFileExtension(externalSignatureConfig);

        if (isEmpty(proofFileExtension)) {
            log.info("message.external_signature_proof_no_content");
            return;
        }

        try {
            documentBuffer.setMediaType(APPLICATION_OCTET_STREAM);
            documentBuffer.setName(properties.getProofFileName() + proofFileExtension);

            String signatureProofId = this.contentService.createSignatureProof(
                    folder.getContentId(),
                    documentBuffer,
                    false,
                    null,
                    taskId
            );

            log.info("Added signature proof {} to folder {}.", signatureProofId, folder.getId());
        } catch (Exception e) {
            log.warn("message.cannot_reach_content_service", e);
        }
    }


    private Mono<ResponseStatusException> setError(ClientResponse clientResponse) {
        return clientResponse
                .bodyToMono(Exception.class)
                .map(exception -> new ResponseStatusException(
                        clientResponse.statusCode(),
                        exception.getMessage()
                ));
    }


    private Optional<Task> getTaskFromFolder(@NotNull Folder folder, String procedureId) {
        return folder
                .getStepList()
                .stream()
                .filter(t -> StringUtils.equals(t.getExternalSignatureProcedureId(), procedureId))
                .filter(t -> FolderUtils.isActive(t.getState()))
                .findFirst();
    }


    private @NotNull Folder getFolder(@NotNull ExternalSignatureConfig externalSignatureConfig, @NotNull String procedureId) {
        String folderId = this.getFolderId(externalSignatureConfig, procedureId);
        return workflowService.getFolder(folderId, externalSignatureConfig.getTenant().getId(), true);
    }


}
