/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.externalsignature;

import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.database.ExternalSignatureConfig;
import coop.libriciel.ipcore.model.externalsignature.ExternalSignatureParams;
import coop.libriciel.ipcore.model.externalsignature.Status;
import coop.libriciel.ipcore.model.externalsignature.response.ExternalSignatureProcedure;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.database.ExternalSignatureConfigRepository;
import coop.libriciel.ipcore.utils.UserUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.util.List;

import static coop.libriciel.ipcore.model.externalsignature.Status.*;
import static coop.libriciel.ipcore.utils.CollectionUtils.streamWithPaginatedRequest;


public interface ExternalSignatureInterface {

    Logger log = LogManager.getLogger(ExternalSignatureInterface.class);

    String BEAN_NAME = "externalSignature";
    String PREFERENCES_PROVIDER_KEY = "services.external-signature.provider";


    default void checkIfFilesAreSigned(@NotNull ExternalSignatureConfigRepository externalSignatureConfigRepository) {

        log.debug("checkIfFilesAreSigned");
        User automaticUser = UserUtils.getAutomaticUser();

        streamWithPaginatedRequest(externalSignatureConfigRepository::findAll, 100)
                .filter(config -> CollectionUtils.isNotEmpty(config.getTransactionIds()))
                .forEach(config ->
                        config.getTransactionIds().forEach(id -> {

                            Status status = getProcedureStatus(config, id);
                            if (status.equals(REFUSED)) {
                                log.debug("Transaction refused : " + id);
                                rejectFolder(config, automaticUser, id);
                            } else if (status.equals(SIGNED)) {
                                log.debug("Transaction signed : " + id);
                                try {
                                    updateFilesAndPerformTask(config, automaticUser, id);
                                } catch (Exception e) {
                                    log.debug(e);
                                }
                            }
                            if (status == SIGNED || status == ERROR || status == REFUSED) {
                                config.getTransactionIds().remove(id);
                                externalSignatureConfigRepository.save(config);
                            }
                        })
                );
    }


    @NotNull String createProcedure(@NotNull Folder folder,
                                    @NotNull ExternalSignatureParams externalSignatureParams,
                                    @NotNull List<Document> documents,
                                    @NotNull String subtypeId);

    void revokeExternalProcedure(@NotNull ExternalSignatureConfig externalSignatureConfig,
                                 @NotNull String procedureId);

    @NotNull Status getProcedureStatus(@NotNull ExternalSignatureConfig externalSignatureConfig,
                                       @NotNull String procedureId);

    ExternalSignatureProcedure getProcedureData(@NotNull ExternalSignatureConfig externalSignatureConfig,
                                                @NotNull String procedureId);

    List<DocumentBuffer> getSignedDocumentList(@NotNull List<String> fileIds,
                                               @NotNull ExternalSignatureConfig externalSignatureConfig,
                                               @NotNull String procedureId);

    void updateFilesAndPerformTask(@NotNull ExternalSignatureConfig externalSignatureConfig,
                                   @NotNull User user,
                                   @NotNull String procedureId);

    void rejectFolder(@NotNull ExternalSignatureConfig externalSignatureConfig,
                      @NotNull User user,
                      @NotNull String procedureId);

    @NotNull String getProcedureRejectionReason(@NotNull ExternalSignatureConfig externalSignatureConfig,
                                                @NotNull String procedureId);

    void rejectExternalProcedure(@NotNull Folder folder,
                                 @NotNull Task task);

    void testService(@NotNull ExternalSignatureConfig externalSignatureConfig);


}
