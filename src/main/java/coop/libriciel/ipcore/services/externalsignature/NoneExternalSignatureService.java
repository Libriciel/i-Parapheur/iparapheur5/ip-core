/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.externalsignature;

import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.database.ExternalSignatureConfig;
import coop.libriciel.ipcore.model.externalsignature.ExternalSignatureParams;
import coop.libriciel.ipcore.model.externalsignature.Status;
import coop.libriciel.ipcore.model.externalsignature.response.ExternalSignatureProcedure;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import org.jetbrains.annotations.NotNull;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.io.Serial;
import java.util.List;

import static coop.libriciel.ipcore.utils.TextUtils.NONE_SERVICE;
import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;


@Service(ExternalSignatureInterface.BEAN_NAME)
@ConditionalOnProperty(name = ExternalSignatureInterface.PREFERENCES_PROVIDER_KEY, havingValue = NONE_SERVICE)
public class NoneExternalSignatureService implements ExternalSignatureInterface {


    private static class NoExternalSignatureServiceError extends LocalizedStatusException {

        private static final @Serial long serialVersionUID = 8879546477973168403L;


        public NoExternalSignatureServiceError() {
            super(SERVICE_UNAVAILABLE, "message.external_signature_service_not_available");
        }

    }


    @Override
    public @NotNull String createProcedure(@NotNull Folder folder,
                                           @NotNull ExternalSignatureParams externalSignatureParams,
                                           @NotNull List<Document> documents,
                                           @NotNull String subtypeId) {
        throw new NoExternalSignatureServiceError();
    }


    @Override
    public void revokeExternalProcedure(@NotNull ExternalSignatureConfig externalSignatureConfig, @NotNull String procedureId) {
        throw new NoExternalSignatureServiceError();
    }


    @Override
    public @NotNull Status getProcedureStatus(@NotNull ExternalSignatureConfig externalSignatureConfig, @NotNull String procedureId) {
        throw new NoExternalSignatureServiceError();
    }


    @Override
    public ExternalSignatureProcedure getProcedureData(@NotNull ExternalSignatureConfig externalSignatureConfig, @NotNull String procedureId) {
        throw new NoExternalSignatureServiceError();
    }


    @Override
    public List<DocumentBuffer> getSignedDocumentList(@NotNull List<String> fileIds,
                                                      @NotNull ExternalSignatureConfig externalSignatureConfig,
                                                      @NotNull String procedureId) {
        throw new NoExternalSignatureServiceError();
    }


    @Override
    public void updateFilesAndPerformTask(@NotNull ExternalSignatureConfig externalSignatureConfig, @NotNull User user, @NotNull String procedureId) {
        throw new NoExternalSignatureServiceError();
    }


    @Override
    public void rejectFolder(@NotNull ExternalSignatureConfig externalSignatureConfig, @NotNull User user, @NotNull String procedureId) {
        throw new NoExternalSignatureServiceError();
    }


    @Override
    public @NotNull String getProcedureRejectionReason(@NotNull ExternalSignatureConfig externalSignatureConfig, @NotNull String procedureId) {
        throw new NoExternalSignatureServiceError();
    }


    @Override
    public void rejectExternalProcedure(@NotNull Folder folder, @NotNull Task task) {
        throw new NoExternalSignatureServiceError();
    }


    @Override
    public void testService(@NotNull ExternalSignatureConfig externalSignatureConfig) {
        throw new NoExternalSignatureServiceError();
    }


}
