/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.ipng;


import io.lettuce.core.StreamMessage;
import io.lettuce.core.XReadArgs;
import io.lettuce.core.api.sync.RedisStreamCommands;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.List;

import static coop.libriciel.ipcore.services.ipng.IpngService.IPNG_ERROR_QUEUE;


@Log4j2
@Service
@ConditionalOnProperty(name = IpngServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = "ipng")
public class IpngRedisStreamReader {


    private final IpngServiceInterface ipngService;
    private final IpngBindingsInterface ipngBindings;
    private final RedisStreamCommands<String, String> redisSyncCommand;


    @Autowired
    public IpngRedisStreamReader(IpngServiceInterface ipngService, IpngBindingsInterface ipngBindings, RedisStreamCommands<String, String> redisSyncCommand) {
        this.ipngService = ipngService;
        this.ipngBindings = ipngBindings;
        this.redisSyncCommand = redisSyncCommand;
    }


    @Scheduled(fixedDelay = 5000)
    void fetchLastStreamMessages() {
        log.trace("fetchLastStreamMessages");

        List<StreamMessage<String, String>> messages = this.redisSyncCommand.xread(
                XReadArgs.Builder.block(Duration.ofSeconds(15)),
                XReadArgs.StreamOffset.from(IpngService.IPNG_PROOF_NOTIFICATION_QUEUE, ipngBindings.getLastRedisMessageId())
        );

        log.trace("fetchLastStreamMessages - message list size : {}", messages.size());
        for (StreamMessage<String, String> message : messages) {
            try {
                log.debug("Latest stream message id : {}", message.getId());
                ipngService.handleRedisMessageMap(message.getBody());

            } catch (Exception e) {
                log.error("Error processing proof-related message from ipng : ", e);
                handleErrorInMessageProcessing(message);
            } finally {
                ipngBindings.setLastRedisMessageId(message.getId());
            }
        }
    }

    private void handleErrorInMessageProcessing(StreamMessage<String, String> message) {
        log.info("Sending redis message to error queue : {}", message);
        this.redisSyncCommand.xadd(IPNG_ERROR_QUEUE, message.getBody());
    }


}
