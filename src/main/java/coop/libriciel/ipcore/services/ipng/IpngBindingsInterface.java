/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.ipng;

import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.MetadataRepresentation;
import coop.libriciel.ipcore.model.ipng.Deskbox;
import coop.libriciel.ipcore.model.ipng.IpngMetadata;
import coop.libriciel.ipcore.model.ipng.IpngType;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Map;


public interface IpngBindingsInterface {

    String BEAN_NAME = "ipngBindings";
    String PREFERENCES_PROVIDER_KEY = "services.ipng.bindings-provider";


    record TypeAssociation(Subtype tenantType, IpngType ipngType){};
    record DeskboxAssociation(DeskRepresentation tenantDesk, Deskbox ipngDeskbox){};
    record MetadataAssociation(MetadataRepresentation tenantMetadata, IpngMetadata ipngMetadata){};



    String getLastRedisMessageId();

    void setLastRedisMessageId(String lastId);

    String getMappedIpngEntityIdForTenant(String tenantId);

    String getMappedTenantIdForIpngEntity(String entityId);


    /*
     *  Desk  <-> Deskboxes
     */

    void addDeskToDeskboxMapping(String tenantId, String deskId, String deskbox);
    String getMappedDeskboxIdForDesk(String tenantId, String deskId);
    String getMappedDeskIdForDeskbox(String tenantId, String deskboxId);
    void deleteDeskToDeskboxMapping(String tenantId, String deskId, String deskbox);
    Map<String, String> getDeskToDeskboxMapping(String tenantId);


    /*
     * Type association for outgoing folders : local IP subtype -> IPNG type
     */

    void addOutgoingTypeMappings(String tenantId, @NotNull List<TypeAssociation> additionalAssociations);
    String getOutgoingIpngTypeIdForLocalType(String tenantId, String subtypeId);
    void deleteOutgoingTypeAssociation(String tenantId, String subtypeId);
    Map<String, String> getOutgoingTypeMapping(String tenantId);


    /*
     * Type association for incoming folders : IPNG type -> : local IP subtype
     */

    void addIncomingTypeMappings(String tenantId, @NotNull List<TypeAssociation> additionalAssociations);
    String getLocalSubtypeIdForIncomingIpngType(String tenantId, String ipngTypeId);
    void deleteIncomingTypeAssociation(String tenantId, String ipngTypeId);
    Map<String, String> getIncomingTypeMapping(String tenantId);


    /*
     * Metadata associations
     */

    void addMetadataMappings(String tenantId, @NotNull List<MetadataAssociation> additionalAssociations);
    String getLocalMetadataKeyForIncomingIpngMetadata(String tenantId, String ipngMetadataKey);
    String getIpngMetadataKeyForOutgoingTenantMetadata(String tenantId, String ipngMetadataKey);
    void deleteMetadataAssociation(String tenantId, String ipngTypeId);
    Map<String, String> getMetadataMapping(String tenantId);

    void logIpngMappings();
}
