/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
package coop.libriciel.ipcore.services.ipng;

import coop.libriciel.ipcore.model.ipng.*;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import org.jetbrains.annotations.NotNull;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.io.Serial;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import static coop.libriciel.ipcore.utils.TextUtils.NONE_SERVICE;
import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;


@Service(IpngServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = IpngServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = NONE_SERVICE, matchIfMissing = true)
public class NoneIpngService implements IpngServiceInterface {


    private static class NoIpngServiceError extends LocalizedStatusException {

        private static final @Serial long serialVersionUID = 7524594311725664561L;


        public NoIpngServiceError() {
            super(SERVICE_UNAVAILABLE, "message.ipng_service_not_available");
        }

    }


    @Override
    public @NotNull List<IpngEntity> listAllEntities() {
        throw new NoIpngServiceError();
    }


    @Override
    public @NotNull IpngEntity getEntity(@NotNull String entityId) {
        throw new NoIpngServiceError();
    }


    @Override
    public @NotNull Deskbox getDeskBox(@NotNull String entityId, @NotNull String id) {
        throw new NoIpngServiceError();
    }


    @Override
    public @NotNull IpngMetadataList getLatestMetadataList(String token) {
        throw new NoIpngServiceError();
    }


    @Override
    public void postProof(@NotNull IpngProof ipngProof, String token, Consumer<IpngProofResult> successCallback) {
        throw new NoIpngServiceError();
    }


    @Override
    public @NotNull IpngProofResult postProof(@NotNull IpngProof ipngProof, String token) {
        throw new NoIpngServiceError();
    }


    @Override
    public @NotNull IpngProofWrap getProof(String proofId) {
        throw new NoIpngServiceError();
    }


    @Override
    public @NotNull IpngTypology getLatestTypology(String token) {
        throw new NoIpngServiceError();
    }


    @Override
    public void handleRedisMessageMap(Map<String, String> msgContent) {
        throw new NoIpngServiceError();
    }


}
