/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.ipng;


import coop.libriciel.ipcore.configuration.IpngStaticConfProperties;
import coop.libriciel.ipcore.model.ipng.RedisStreamIndex;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Log4j2
@Data
@Service(IpngBindingsInterface.BEAN_NAME)
@ConditionalOnProperty(name = IpngBindingsInterface.PREFERENCES_PROVIDER_KEY, havingValue = "static")
public class IpngBindingsFromFile implements IpngBindingsInterface {

    private final RedisStreamIndex.QueueName PROOF_QUEUE_NAME = RedisStreamIndex.QueueName.PROOF_QUEUE;

    private final IpngStaticConfProperties ipngProperties;
    private final RedisIndexRepository redisIndexRepository;

    public IpngBindingsFromFile(IpngStaticConfProperties ipngProperties, RedisIndexRepository redisIndexRepository) {
        this.ipngProperties = ipngProperties;
        this.redisIndexRepository = redisIndexRepository;
    }


    @Override
    public String getLastRedisMessageId() {
        String lastRegisteredId = this.redisIndexRepository.findById(PROOF_QUEUE_NAME)
                .map(RedisStreamIndex::getLastIndex)
                .orElse("0");

        log.info("getLastRedisMessageId - read from db : {}", lastRegisteredId);

        String idxFromProperties = ipngProperties.getLastRedisMessage();
        log.info("getLastRedisMessageId - read from properties : {}", idxFromProperties);

        return StringUtils.equals("0", idxFromProperties) ? lastRegisteredId : idxFromProperties;
    }


    @Override
    public void setLastRedisMessageId(String lastId) {
        RedisStreamIndex newIdx = this.redisIndexRepository.findById(PROOF_QUEUE_NAME).orElse(new RedisStreamIndex(PROOF_QUEUE_NAME, lastId));
        newIdx.setLastIndex(lastId);
        this.redisIndexRepository.save(newIdx);

        this.ipngProperties.setLastRedisMessage(lastId);
    }


    @Override
    public String getMappedDeskboxIdForDesk(String tenantId, String deskId) {
        return ipngProperties.getTenantConfigurations().get(tenantId).getDeskToDeskbox().get(deskId);
    }


    @Override
    public String getMappedDeskIdForDeskbox(String tenantId, String deskboxId) {
        if (deskboxId == null) {
            log.warn("getMappedDeskIdForDeskbox - deskbox id is null");
            return null;
        }
        return ipngProperties.getTenantConfigurations().get(tenantId).getDeskToDeskbox().entrySet().stream()
                .filter(entry -> StringUtils.equals(entry.getValue(), deskboxId))
                .map(Map.Entry::getKey)
                .findFirst().orElse(null);
    }


    @Override
    public void deleteDeskToDeskboxMapping(String tenantId, String deskId, String deskbox) {

    }


    @Override
    public void addDeskToDeskboxMapping(String tenantId, String deskId, String deskbox) {
        log.warn("addDeskToDeskboxMapping - unimplemented for file-based conf");
    }


    @Override
    public Map<String, String> getDeskToDeskboxMapping(String tenantId) {
        return this.ipngProperties.getTenantConfigurations().get(tenantId).getDeskToDeskbox();
    }


    @Override
    public String getMappedIpngEntityIdForTenant(String tenantId) {
        return ipngProperties.getTenantToEntities().get(tenantId);
    }


    @Override
    public String getMappedTenantIdForIpngEntity(String entityId) {
        if (entityId == null) {
            log.warn("getMappedTenantIdForEntity - entityId is null");
            return null;
        }
        return ipngProperties.getTenantToEntities().entrySet().stream()
                .filter(entry -> StringUtils.equals(entry.getValue(), entityId))
                .map(Map.Entry::getKey)
                .findFirst().orElse(null);
    }


    @Override
    public String getOutgoingIpngTypeIdForLocalType(String tenantId, String subtypeId) {

        return ipngProperties.getTenantConfigurations().get(tenantId).getInnerTypesToOutgoingIpngTypes().get(subtypeId); // "100-common-contract";
    }


    @Override
    public void deleteOutgoingTypeAssociation(String tenantId, String ipngTypeId) {

    }


    @Override
    public void addOutgoingTypeMappings(String tenantId, List<TypeAssociation> additionalAssociations) {
        log.warn("addOutgoingTypeMappings - unimplemented for file-based conf");
    }


    @Override
    public Map<String, String> getOutgoingTypeMapping(String tenantId) {
        return ipngProperties.getTenantConfigurations().get(tenantId).getInnerTypesToOutgoingIpngTypes();
    }


    @Override
    public String getLocalSubtypeIdForIncomingIpngType(String tenantId, String ipngTypeId) {
        if (ipngTypeId == null) {
            log.warn("getMappedlocalSubtypeIdForIpngType - ipngTypeId is null");
            return null;
        }

        return ipngProperties.getTenantConfigurations().get(tenantId).getIncomingIpngTypesToInnerTypes().get(ipngTypeId);

//        return ipngProperties.getTenantConfigurations().get(tenantId).getInnerTypesToOutgoingIpngTypes().entrySet().stream()
//                .filter(entry -> StringUtils.equals(entry.getValue(), ipngTypeId))
//                .map(Map.Entry::getKey)
//                .findFirst().orElse(null);// "100-common-contract";
    }


    @Override
    public void deleteIncomingTypeAssociation(String tenantId, String ipngTypeId) {

    }


    @Override
    public Map<String, String> getIncomingTypeMapping(String tenantId) {
        return ipngProperties.getTenantConfigurations().get(tenantId).getIncomingIpngTypesToInnerTypes();
    }


    @Override
    public void addMetadataMappings(String tenantId, List<MetadataAssociation> additionalAssociations) {

    }


    @Override
    public String getLocalMetadataKeyForIncomingIpngMetadata(String tenantId, String ipngMetadataKey) {
        return null;
    }


    @Override
    public String getIpngMetadataKeyForOutgoingTenantMetadata(String tenantId, String ipngMetadataKey) {
        return null;
    }


    @Override
    public void deleteMetadataAssociation(String tenantId, String ipngTypeId) {

    }


    @Override
    public Map<String, String> getMetadataMapping(String tenantId) {
        return null;
    }


    @Override
    public void addIncomingTypeMappings(String tenantId, List<TypeAssociation> additionalAssociations) {
        log.warn("addIncomingTypeMappings - unimplemented for file-based conf");
    }


    @Override
    public void logIpngMappings() {
        log.info("::logIpngMappings");
        log.info("ipngProperties.getTenantToEntities() : {}", ipngProperties.getTenantToEntities());
        log.info("ipngProperties.getTenantConfigurations() : {}", ipngProperties.getTenantConfigurations());

        log.info("full ipngProperties : {}", ipngProperties);
    }

}
