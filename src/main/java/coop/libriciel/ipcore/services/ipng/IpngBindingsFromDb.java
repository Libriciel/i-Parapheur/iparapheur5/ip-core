/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.ipng;


import coop.libriciel.ipcore.configuration.IpngDbConfProperties;
import coop.libriciel.ipcore.model.ipng.IpngMappings;
import coop.libriciel.ipcore.model.ipng.RedisStreamIndex;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collections;
import java.util.List;
import java.util.Map;


@Log4j2
@Data
@Service(IpngBindingsInterface.BEAN_NAME)
@ConditionalOnProperty(name = IpngBindingsInterface.PREFERENCES_PROVIDER_KEY, havingValue = "db", matchIfMissing = true)
public class IpngBindingsFromDb implements IpngBindingsInterface {

    private final RedisStreamIndex.QueueName PROOF_QUEUE_NAME = RedisStreamIndex.QueueName.PROOF_QUEUE;
    private final RedisIndexRepository redisIndexRepository;
    private final IpngMappingRepository ipngMappingRepository;

    private final IpngDbConfProperties ipngProperties;


    public IpngBindingsFromDb(RedisIndexRepository redisIndexRepository, IpngMappingRepository ipngMappingRepository, IpngDbConfProperties ipngProperties) {
        this.redisIndexRepository = redisIndexRepository;
        this.ipngMappingRepository = ipngMappingRepository;
        this.ipngProperties = ipngProperties;
    }


    @Override
    public String getLastRedisMessageId() {
        String lastRegisteredId = this.redisIndexRepository.findById(PROOF_QUEUE_NAME)
                .map(RedisStreamIndex::getLastIndex)
                .orElse("0");

        log.info("getLastRedisMessageId - read from db : {}", lastRegisteredId);


        String idxFromProperties = ipngProperties.getLastRedisMessage();
        log.info("getLastRedisMessageId - read from properties : {}", idxFromProperties);

        return StringUtils.equals("0", idxFromProperties) ? lastRegisteredId : idxFromProperties;
    }


    @Override
    public void setLastRedisMessageId(String lastId) {
        RedisStreamIndex newIdx = this.redisIndexRepository.findById(PROOF_QUEUE_NAME).orElse(new RedisStreamIndex(PROOF_QUEUE_NAME, lastId));
        newIdx.setLastIndex(lastId);
        this.redisIndexRepository.save(newIdx);

        this.ipngProperties.setLastRedisMessage(lastId);
    }


    @Override
    public void addDeskToDeskboxMapping(String tenantId, String deskId, String deskbox) {
        IpngMappings tenantMapping = this.ipngMappingRepository.findById(tenantId).orElse(new IpngMappings(tenantId));
        tenantMapping.getDeskToDeskbox().put(deskId, deskbox);
        this.ipngMappingRepository.save(tenantMapping);
    }


    @Override
    public String getMappedDeskboxIdForDesk(String tenantId, String deskId) {
        return this.ipngMappingRepository.findById(tenantId)
                .map(IpngMappings::getDeskToDeskbox)
                .map(map -> map.get(deskId))
                .orElse(null);
    }


    @Override
    public String getMappedDeskIdForDeskbox(String tenantId, String deskboxId) {
        Map<String, String> deskMap = this.ipngMappingRepository.findById(tenantId)
                .map(IpngMappings::getDeskToDeskbox).orElse(Collections.emptyMap());

        return deskMap.entrySet().stream()
                .filter(entry -> StringUtils.equals(entry.getValue(), deskboxId))
                .map(Map.Entry::getKey)
                .findFirst().orElse(null);
    }


    @Override
    public void deleteDeskToDeskboxMapping(String tenantId, String deskId, String deskboxId) {
        IpngMappings tenantMapping = this.ipngMappingRepository.findById(tenantId).orElse(new IpngMappings(tenantId));
        Map<String, String> deskMap = tenantMapping.getDeskToDeskbox();

        String mappedDeskboxId = deskMap.get(deskId);
        if (mappedDeskboxId == null) { //|| !StringUtils.equals(mappedDeskboxId, deskboxId)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("No mapping found for deskId %s", deskId));
        }

        deskMap.remove(deskId);
        this.ipngMappingRepository.save(tenantMapping);
    }


    @Override
    public Map<String, String> getDeskToDeskboxMapping(String tenantId) {
        return this.ipngMappingRepository.findById(tenantId).map(IpngMappings::getDeskToDeskbox).orElse(Collections.emptyMap());
    }


    @Override
    public String getMappedIpngEntityIdForTenant(String tenantId) {
//        return this.ipngMappingRepository.findById(tenantId)
//                .map(IpngMappings::getDeskToDeskbox);
        return this.ipngProperties.getTenantToEntities().get(tenantId);

    }


    @Override
    public String getMappedTenantIdForIpngEntity(String entityId) {
        if (entityId == null) {
            log.warn("getMappedTenantIdForEntity - entityId is null");
            return null;
        }
        return this.ipngProperties.getTenantToEntities().entrySet().stream()
                .filter(entry -> StringUtils.equals(entry.getValue(), entityId))
                .map(Map.Entry::getKey)
                .findFirst().orElse(null);
    }


    @Override
    public void addOutgoingTypeMappings(String tenantId, @NotNull List<TypeAssociation> additionalAssociations) {
        IpngMappings tenantMapping = this.ipngMappingRepository.findById(tenantId).orElse(new IpngMappings(tenantId));
        Map<String, String> outgoingTypeMapping = tenantMapping.getInnerTypesToOutgoingIpngTypes();
        additionalAssociations.forEach(typeAssociation -> {
            outgoingTypeMapping.put(typeAssociation.tenantType().getId(), typeAssociation.ipngType().getKey());
        });
        this.ipngMappingRepository.save(tenantMapping);
    }


    @Override
    public String getOutgoingIpngTypeIdForLocalType(String tenantId, String subtypeId) {
        return this.ipngMappingRepository.findById(tenantId)
                .map(IpngMappings::getInnerTypesToOutgoingIpngTypes)
                .map(map -> map.get(subtypeId))
                .orElse(null);
    }


    @Override
    public void deleteOutgoingTypeAssociation(String tenantId, String subtypeId) {
        IpngMappings tenantMapping = this.ipngMappingRepository.findById(tenantId).orElse(new IpngMappings(tenantId));
        Map<String, String> outgoingTypesMap = tenantMapping.getInnerTypesToOutgoingIpngTypes();

        String mappedIpngTypeId = outgoingTypesMap.get(subtypeId);
        if (mappedIpngTypeId == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("No mapping found for subtypeId %s", subtypeId));
        }

        outgoingTypesMap.remove(subtypeId);
        this.ipngMappingRepository.save(tenantMapping);
    }


    @Override
    public Map<String, String> getOutgoingTypeMapping(String tenantId) {
        return this.ipngMappingRepository.findById(tenantId)
                .map(IpngMappings::getInnerTypesToOutgoingIpngTypes)
                .orElse(Collections.emptyMap());
    }


    @Override
    public void addIncomingTypeMappings(String tenantId, @NotNull List<TypeAssociation> additionalAssociations) {
        IpngMappings tenantMapping = this.ipngMappingRepository.findById(tenantId).orElse(new IpngMappings(tenantId));
        Map<String, String> incomingTypeMapping = tenantMapping.getIncomingIpngTypesToInnerTypes();
        additionalAssociations.forEach(typeAssociation -> {
            incomingTypeMapping.put(typeAssociation.ipngType().getKey(), typeAssociation.tenantType().getId());
        });
        this.ipngMappingRepository.save(tenantMapping);
    }


    @Override
    public String getLocalSubtypeIdForIncomingIpngType(String tenantId, String ipngTypeId) {
        return this.ipngMappingRepository.findById(tenantId)
                .map(IpngMappings::getIncomingIpngTypesToInnerTypes)
                .map(map -> map.get(ipngTypeId))
                .orElse(null);
    }


    @Override
    public void deleteIncomingTypeAssociation(String tenantId, String ipngTypeId) {
        IpngMappings tenantMapping = this.ipngMappingRepository.findById(tenantId).orElse(new IpngMappings(tenantId));
        Map<String, String> incomingTypesMap = tenantMapping.getIncomingIpngTypesToInnerTypes();

        String mappedSubtypeId = incomingTypesMap.get(ipngTypeId);
        if (mappedSubtypeId == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("No mapping found for subtypeId %s", ipngTypeId));
        }

        incomingTypesMap.remove(ipngTypeId);
        this.ipngMappingRepository.save(tenantMapping);
    }


    @Override
    public Map<String, String> getIncomingTypeMapping(String tenantId) {
        return this.ipngMappingRepository.findById(tenantId)
                .map(IpngMappings::getIncomingIpngTypesToInnerTypes)
                .orElse(Collections.emptyMap());
    }


    @Override
    public void addMetadataMappings(String tenantId, List<MetadataAssociation> additionalAssociations) {
        IpngMappings tenantMapping = this.ipngMappingRepository.findById(tenantId).orElse(new IpngMappings(tenantId));
        Map<String, String> metadataMapping = tenantMapping.getInnerToIpngMetadata();
        additionalAssociations.forEach(metadataAssociation -> {
            metadataMapping.put(metadataAssociation.tenantMetadata().getKey(), metadataAssociation.ipngMetadata().getKey());
        });

        this.ipngMappingRepository.save(tenantMapping);
    }


    @Override
    public String getLocalMetadataKeyForIncomingIpngMetadata(String tenantId, String ipngMetadataKey) {
        Map<String, String> metadataMap = this.ipngMappingRepository.findById(tenantId)
                .map(IpngMappings::getInnerToIpngMetadata).orElse(Collections.emptyMap());

        return metadataMap.entrySet().stream()
                .filter(entry -> StringUtils.equals(entry.getValue(), ipngMetadataKey))
                .map(Map.Entry::getKey)
                .findFirst().orElse(null);
    }


    @Override
    public String getIpngMetadataKeyForOutgoingTenantMetadata(String tenantId, String tenantMetadataKey) {
        return this.ipngMappingRepository.findById(tenantId)
                .map(IpngMappings::getInnerToIpngMetadata)
                .map(map -> map.get(tenantMetadataKey))
                .orElse(null);
    }


    @Override
    public void deleteMetadataAssociation(String tenantId, String tenantMetadataKey) {
        IpngMappings tenantMapping = this.ipngMappingRepository.findById(tenantId).orElse(new IpngMappings(tenantId));
        Map<String, String> metadataMap = tenantMapping.getInnerToIpngMetadata();

        String mappedSubtypeId = metadataMap.get(tenantMetadataKey);
        if (mappedSubtypeId == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("No mapping found for tenantMetadataKey %s", tenantMetadataKey));
        }

        metadataMap.remove(tenantMetadataKey);
        this.ipngMappingRepository.save(tenantMapping);
    }


    @Override
    public Map<String, String> getMetadataMapping(String tenantId) {
        return this.ipngMappingRepository.findById(tenantId)
                .map(IpngMappings::getInnerToIpngMetadata)
                .orElse(Collections.emptyMap());
    }


    @Override
    public void logIpngMappings() {

    }
}
