/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.ipng;

import com.fasterxml.jackson.core.JsonProcessingException;
import coop.libriciel.ipcore.model.ipng.*;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;


public interface IpngServiceInterface {

    String BUSINESS_ID_FORMAT = "%s_%s_%s";
    String PROOF_ID_FORMAT = "%s_%s_%s";
    String BUSINESS_ID_PREFIX = "BusinessId";
    String PROOF_ID_PREFIX = "ProofId";

    String BEAN_NAME = "ipng";
    String PREFERENCES_PROVIDER_KEY = "services.ipng.provider";

    String notAnAlphanumCharRegex = "[^A-Za-z0-9]";

    default
    @NotNull String buildBusinessId(String uuid, String folderName) {
        String strippedFolderName = folderName.replaceAll(notAnAlphanumCharRegex, "");
        return String.format(BUSINESS_ID_FORMAT, BUSINESS_ID_PREFIX, uuid, strippedFolderName);
    }

    default
    @NotNull String buildProofId(String uuid, String folderName) {
        String strippedFolderName = folderName.replaceAll(notAnAlphanumCharRegex, "");
        return String.format(PROOF_ID_FORMAT, PROOF_ID_PREFIX, uuid, strippedFolderName);
    }


    @NotNull List<IpngEntity> listAllEntities();

    @NotNull IpngEntity getEntity(@NotNull String entityId);

    @NotNull Deskbox getDeskBox(@NotNull String entityId, @NotNull String id);

//    @NotNull List<Deskbox> getDeskboxProfileByTenant(@NotNull String tenantId);

    @NotNull IpngMetadataList getLatestMetadataList(String token);

    @NotNull void postProof(@NotNull IpngProof ipngProof, String token, Consumer<IpngProofResult> successCallback) throws JsonProcessingException;

    @NotNull IpngProofResult postProof(@NotNull IpngProof ipngProof, String token) throws JsonProcessingException;

    @NotNull IpngProofWrap getProof(String proofId);

    @NotNull IpngTypology getLatestTypology(String token);

    void handleRedisMessageMap(Map<String, String> msgContent);
}
