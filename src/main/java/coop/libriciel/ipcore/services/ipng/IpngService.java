/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.ipng;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import coop.libriciel.ipcore.business.workflow.WorkflowBusinessService;
import coop.libriciel.ipcore.model.content.CreateFolderRequest;
import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.filetransfer.FolderUnpackedContent;
import coop.libriciel.ipcore.model.ipng.*;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.database.SubtypeRepository;
import coop.libriciel.ipcore.services.database.TenantRepository;
import coop.libriciel.ipcore.services.filetransfer.FileTransferServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.ArbitraryAuthenticationUtils;
import coop.libriciel.ipcore.utils.IpInternalException;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.function.Consumer;

import static coop.libriciel.ipcore.model.workflow.Action.START;
import static coop.libriciel.ipcore.model.workflow.State.CURRENT;
import static coop.libriciel.ipcore.model.workflow.State.PENDING;
import static coop.libriciel.ipcore.services.content.ContentServiceInterface.FILE_TRANSFER_BUFFER_SIZE;
import static coop.libriciel.ipcore.services.workflow.IpWorkflowService.*;
import static coop.libriciel.ipcore.utils.RequestUtils.SUBSERVICE_REQUEST_LONG_TIMEOUT;
import static coop.libriciel.ipcore.utils.RequestUtils.SUBSERVICE_REQUEST_SHORT_TIMEOUT;
import static coop.libriciel.ipcore.utils.UserUtils.AUTOMATIC_TASK_USER_ID;
import static org.springframework.core.io.buffer.DataBufferUtils.readInputStream;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.security.config.Elements.HTTP;


@Log4j2
@Service(IpngServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = IpngServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = "ipng")
public class IpngService implements IpngServiceInterface {


    // TODO ipng should use separate queues (with qualified names) for each type of contract (directory, ontology, proof).
    public static final String IPNG_DIRECTORY_NOTIFICATION_QUEUE = "ipng-directory";
    public static final String IPNG_ONTOLOGY_NOTIFICATION_QUEUE = "ipng-ontology";
    public static final String IPNG_PROOF_NOTIFICATION_QUEUE = "ipng-proof";
    public static final String IPNG_ERROR_QUEUE = "ipng-error";


    private static final String IPNG_BASE_URL = "ipng/api";
    private static final String IPNG_ENTITIES_URL = IPNG_BASE_URL + "/directory/entities";
    private static final String IPNG_GET_ALL_ENTITIES_URL = IPNG_ENTITIES_URL;
    private static final String IPNG_PROOFS_URL = IPNG_BASE_URL + "/proofs";
    private static final String IPNG_SINGLE_ENTITY_URL = IPNG_BASE_URL + "/directory/entity";
    private static final String IPNG_DESKBOX_PROFILES_PATH_COMPONENT = "deskboxProfiles";
    //    private static final String IPNG_GET_DESKBOX_PROFILE_BY_TENANT_URL = IPNG_BASE_URL + "/directory/deskboxProfiles/tenant";
    private static final String IPNG_GET_LATEST_METADATA_URL = IPNG_BASE_URL + "/ontology/metadatas/latest";
    private static final String IPNG_POST_PROOF_URL = IPNG_BASE_URL + "/proofs";
    private static final String IPNG_GET_LATEST_TYPOLOGY_URL = IPNG_BASE_URL + "/ontology/typologies/latest";


    private static final String IPNG_MSG_KEY_NAME = "name";
    private static final String IPNG_MSG_KEY_PROOF_SENDER_ID = "subject.senderEntityId";
    private static final String IPNG_MSG_KEY_PROOF_RECEIVER_ID = "subject.receiverEntityId";
    private static final String IPNG_MSG_KEY_PROOF_ID = "subject.id";
    private static final String IPNG_MSG_TYPE_ADDPROOF = "AddProof";

    //    private static final String IPNG_MSG_KEY_ = "name";
    private final TenantRepository tenantRepository;
    private final SubtypeRepository subtypeRepository;
    private final WorkflowBusinessService workflowBusinessService;
    private final WorkflowServiceInterface workflowService;
    private final FileTransferServiceInterface filetransferService;
    private final ContentServiceInterface contentService;
    private final PendingIpngFolderRepository pendingIpngFolderRepository;
    private final IpngBindingsInterface ipngBindings;
    private final KeycloakBuilder keycloakBuilder;
    private final IpngServiceProperties properties;


    private final PlatformTransactionManager platformTransactionManager;


    // FIXME this should be persisted (or use another mecanism) to handle IPNG responses after restart
    private final Map<String, Consumer<IpngProofResult>> successCallbackMap = new HashMap<>();

    private @Value("${services.ipng.host}") String host;
    private @Value("${services.ipng.port}") Integer port;


    public IpngService(TenantRepository tenantRepository,
                       WorkflowBusinessService workflowBusinessService,
                       WorkflowServiceInterface workflowService,
                       ContentServiceInterface contentService,
                       PendingIpngFolderRepository pendingIpngFolderRepository,
                       IpngBindingsInterface ipngBindings,
                       SubtypeRepository subtypeRepository,
                       FileTransferServiceInterface filetransferService,
                       KeycloakBuilder keycloakBuilder,
                       IpngServiceProperties properties,
                       PlatformTransactionManager platformTransactionManager) {

        this.tenantRepository = tenantRepository;
        this.workflowBusinessService = workflowBusinessService;
        this.workflowService = workflowService;
        this.contentService = contentService;
        this.pendingIpngFolderRepository = pendingIpngFolderRepository;
        this.ipngBindings = ipngBindings;
        this.subtypeRepository = subtypeRepository;
        this.filetransferService = filetransferService;
        this.keycloakBuilder = keycloakBuilder;
        this.properties = properties;
        this.platformTransactionManager = platformTransactionManager;
    }


    @Override
    public @NotNull List<IpngEntity> listAllEntities() {
        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(IPNG_GET_ALL_ENTITIES_URL)
                .build().normalize().toUri();
        return WebClient.builder()
                .build()
                .get()
                .uri(requestUri)
                .accept(APPLICATION_JSON)
                .retrieve()
                .bodyToFlux(IpngEntity.class)
                .log()
                .collectList()
                .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT).orElse(Collections.emptyList());
    }


    @Override
    public @NotNull IpngEntity getEntity(@NotNull String entityId) {
        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(IPNG_ENTITIES_URL)
                .pathSegment(entityId)
                .build().normalize().toUri();
        return Objects.requireNonNull(WebClient.builder()
//                .filter(responseErrorFilter())
                .build()
                .get()
                .uri(requestUri)
                .accept(APPLICATION_JSON)
                .retrieve()
                .onStatus(NOT_FOUND::equals,
                        clientResponse -> {
                            log.debug("intercepted error in getEntity");
                            return Mono.error(new LocalizedStatusException(NOT_FOUND, "message.unknown_ipng_entity_id"));
                        })
                .bodyToMono(IpngEntity.class)
//                .defaultIfEmpty(new IpngEntity())
//                .filter(Objects::nonNull)
                .block(SUBSERVICE_REQUEST_SHORT_TIMEOUT));
    }


    @Override
    public @NotNull Deskbox getDeskBox(@NotNull String entityId, @NotNull String id) {
        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(IPNG_SINGLE_ENTITY_URL)
                .pathSegment(entityId)
                .pathSegment(IPNG_DESKBOX_PROFILES_PATH_COMPONENT)
                .pathSegment(id)
                .build().normalize().toUri();
        return Objects.requireNonNull(WebClient.builder()
                .build()
                .get()
                .uri(requestUri)
                .accept(APPLICATION_JSON)
                .retrieve()
                .bodyToMono(Deskbox.class)
                .block(SUBSERVICE_REQUEST_SHORT_TIMEOUT));
    }


    @Override
    public @NotNull IpngMetadataList getLatestMetadataList(String token) {
        final String usedToken = this.cleanAuthToken(token);

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(IPNG_GET_LATEST_METADATA_URL)
                .build().normalize().toUri();
        return Objects.requireNonNull(WebClient.builder()
                .build()
                .get()
                .uri(requestUri)
                .headers(h -> h.setBearerAuth(usedToken))
                .accept(APPLICATION_JSON)
                .retrieve()
                .bodyToMono(IpngMetadataList.class)
                .block(SUBSERVICE_REQUEST_LONG_TIMEOUT));
    }


    @Override
    public void postProof(@NotNull IpngProof ipngProof, String token, Consumer<IpngProofResult> successCallback) throws JsonProcessingException {
        if (successCallback != null) {
            this.successCallbackMap.put(ipngProof.getId(), successCallback);
        }
        this.postProof(ipngProof, token);
    }


    @Override
    public @NotNull IpngProofResult postProof(@NotNull IpngProof ipngProof, String token) throws JsonProcessingException {

        final String usedToken = this.cleanAuthToken(token);

        ObjectMapper objectMapper = new ObjectMapper();
        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(IPNG_POST_PROOF_URL)
                .build().normalize().toUri();
        return WebClient.builder()
                .build()
                .post()
                .uri(requestUri)
                .headers(h -> h.setBearerAuth(usedToken))
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .bodyValue(objectMapper.writeValueAsString(ipngProof))
                .retrieve()
                .bodyToMono(IpngProofResult.class)
                .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_ipng"));
    }


    @Override
    public @NotNull IpngProofWrap getProof(String proofId) {

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(IPNG_PROOFS_URL)
                .pathSegment(proofId)
                .build().normalize().toUri();


        IpngProofWrap wrappedProofData;
        try (Keycloak kcClient = this.keycloakClient()) {
            String token = kcClient.tokenManager().getAccessToken().getToken();
            // TODO handle null/empty/missing result
            wrappedProofData = WebClient.builder()
                    .build()
                    .get()
                    .uri(requestUri)
                    .header(
                            "Authorization",
                            "Bearer " + token
                    )
                    .accept(APPLICATION_JSON)
                    .retrieve()
                    .bodyToMono(IpngProofWrap.class)
                    .block(SUBSERVICE_REQUEST_SHORT_TIMEOUT);
        }
        if (wrappedProofData == null) {
            throw new IpInternalException("Could not retrieve ipng proof ");
        }
        return wrappedProofData;
    }


    @Override
    public @NotNull IpngTypology getLatestTypology(String token) {

        final String usedToken = this.cleanAuthToken(token);

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(IPNG_GET_LATEST_TYPOLOGY_URL)
                .build().normalize().toUri();
        return Objects.requireNonNull(WebClient.builder()
                .build()
                .get()
                .uri(requestUri)
                .headers(h -> h.setBearerAuth(usedToken))
                .accept(APPLICATION_JSON)
                .retrieve()
                .bodyToMono(IpngTypology.class)
                .block(SUBSERVICE_REQUEST_LONG_TIMEOUT));
    }


    private Keycloak keycloakClient() {
        // TODO maybe pass entityId, which maps to a single tenant, which could have credentials for a dedicated "ipngUser"
        return keycloakBuilder
                .build();
    }


    private IpngProofWrap handleReceivedProof(ReceivedProofNotifSubject proofNotif) throws IpInternalException {
        log.info("handleReceivedProof");

        IpngProofWrap wrappedProofData = this.getProof(proofNotif.getId());

        log.info("handleReceivedProof - got wrapped proof data : {}", wrappedProofData);

        IpngProof proofContent = wrappedProofData.getFolderExchange();

        String tenantId = ipngBindings.getMappedTenantIdForIpngEntity(wrappedProofData.getReceiverEntityId());

        // Should not be a status exception, and should be caught
        Tenant tenant = tenantRepository.findById(tenantId)
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_tenant_id"));


        if (proofContent.getProofType() == IpngProof.ProofType.RECEIVE) {
            log.info("The proof received is a receipt acknowledgement");
            Optional<PendingIpngFolder> pendingFolderOpt = findPendingIpngFolder(tenantId, proofContent);
            pendingFolderOpt.filter(p -> StringUtils.equals(p.getRemoteDeskboxId(), proofContent.getSenderDeskboxId()))
                    .ifPresentOrElse(pendingFolder -> {
                        String deskboxId = proofContent.getReceiverDeskboxId();
                        String deskId = this.ipngBindings.getMappedDeskIdForDeskbox(tenantId, deskboxId);
                        this.workflowService.ipngProofReceiptWasReceived(tenantId, deskId, pendingFolder, wrappedProofData);
                    }, () -> {
                        log.warn("received a receipt from IPNG, but no matching folder found for proofId, or the deskbox id does not match");
                        log.info("proof content: {}", proofContent);
                    });

            return null;
        }

        if (proofContent.getProofType() == IpngProof.ProofType.RESPONSE) {
            log.info("The proof received is a response");
            Optional<PendingIpngFolder> pendingFolderOpt = findPendingIpngFolder(tenantId, proofContent);

            pendingFolderOpt.filter(p -> StringUtils.equals(p.getRemoteDeskboxId(), proofContent.getSenderDeskboxId()))
                    .ifPresentOrElse(pendingFolder -> {
                        try {
                            updateFolderFromIpngResponse(tenantId, proofContent, pendingFolder, wrappedProofData);
                        } catch (IOException e) {
                            log.info("tenant id : {}, sender endity ID : {}, sender deskbox id : {}",
                                    tenantId,
                                    wrappedProofData.getSenderEntityId(),
                                    proofContent.getSenderDeskboxId());
                            log.debug("Error : ", e);
                            throw new IpInternalException("An error prevented loading a folder as response from ipng", e);
                        }
                    }, () -> {
                        log.warn("received a response from IPNG, but no matching folder found for proofId, or the deskbox id does not match");
                        log.info("proof content: {}", proofContent);
                    });
            return null;
        }

        new TransactionTemplate(platformTransactionManager).execute(new TransactionCallback<Void>() {
            @Override
            public Void doInTransaction(TransactionStatus transactionStatus) {
                try {
                    FolderData folderData = retrieveFolderData(wrappedProofData);

                    createNewFolderFromIPNG(proofContent, tenant, folderData);

                    sendReceipt(proofContent);
                } catch (IOException e) {
                    log.info("tenant id : {}, sender entity id : {}, sender deskbox id : {}",
                            tenantId,
                            wrappedProofData.getSenderEntityId(),
                            proofContent.getSenderDeskboxId());
                    log.debug("Error : ", e);
                    throw new IpInternalException("An error prevented loading a folder received through ipng", e);
                }
                return null;
            }
        });

        return wrappedProofData;

    }


    @NotNull
    private Optional<PendingIpngFolder> findPendingIpngFolder(String tenantId, IpngProof proofContent) {
        PendingIpngFolderKey pendingFolderKey = new PendingIpngFolderKey(proofContent.getBusinessId(), tenantId);
        Optional<PendingIpngFolder> pendingFolderOptional = this.pendingIpngFolderRepository.findById(pendingFolderKey);
        return pendingFolderOptional.filter(p -> p.getFolderId() != null);
    }


    private void updateFolderFromIpngResponse(String tenantId,
                                              IpngProof proofContent,
                                              PendingIpngFolder pendingFolder,
                                              IpngProofWrap wrappedProofData) throws IOException {
        String deskId;
        String deskboxId = proofContent.getReceiverDeskboxId();
        deskId = this.ipngBindings.getMappedDeskIdForDeskbox(tenantId, deskboxId);
        log.info("receivedIpngResponse - deskId : {}", deskId);

        Folder folder = workflowService.getFolder(pendingFolder.getFolderId(), tenantId, true);

        FolderData folderData = retrieveFolderData(wrappedProofData);
        if (!folderData.getDocList().isEmpty()) {
            List<Document> previousDocList = this.contentService.getDocumentList(folder.getContentId());

            folderData.getDocList().forEach(newDoc -> {
                Optional<Document> prevDocOpt = previousDocList.stream()
                        .filter(d -> StringUtils.equals(d.getName(), newDoc.getName()))
                        .findFirst();

                prevDocOpt.ifPresentOrElse(
                        prevDoc -> contentService.updateDocument(prevDoc.getId(), newDoc, false),
                        () -> contentService.createDocument(tenantId, folder, newDoc)
                );
            });
        }
        this.workflowService.receivedIpngResponse(tenantId, deskId, folder, pendingFolder, wrappedProofData);
    }


    private void sendReceipt(IpngProof srcProof) throws JsonProcessingException {
        log.debug("sendReceipt - proofContent : {}", srcProof);
        IpngProof receiptProof = srcProof.withProofType(IpngProof.ProofType.RECEIVE);
        String origSenderDbxId = receiptProof.getSenderDeskboxId();
        receiptProof.setSenderDeskboxId(receiptProof.getReceiverDeskboxId());
        receiptProof.setReceiverDeskboxId(origSenderDbxId);
        log.debug("sendReceipt - receiptProof : {}", receiptProof);

        String token = this.keycloakClient().tokenManager().getAccessToken().getToken();
        this.postProof(receiptProof, token);
    }


    private void createNewFolderFromIPNG(IpngProof proofContent, Tenant tenant, FolderData folderData) throws IpInternalException {
        log.debug("createNewFolderFromIPNG - receiver deskbox id : {}", proofContent.getReceiverDeskboxId());
        String tenantId = tenant.getId();
        String deskId = ipngBindings.getMappedDeskIdForDeskbox(tenantId, proofContent.getReceiverDeskboxId());
        if (deskId == null) {
            throw new IpInternalException(String.format(
                    "Error loading a folder from ipng : No desk mapped to deskboxId %s was found",
                    proofContent.getReceiverDeskboxId()
            ));
        }

        String subtypeId = this.ipngBindings.getLocalSubtypeIdForIncomingIpngType(tenantId, proofContent.getType().getKey());
        Subtype fullSubtype = this.subtypeRepository.findByIdAndTenant_Id(subtypeId, tenantId)
                .orElseThrow(() -> new IpInternalException("No matching subtype found for id defined in IPNG mapping : " + subtypeId));

        String typeId = fullSubtype.getParentType().getId();

        CreateFolderRequest draftFolderParams = new CreateFolderRequest();
        draftFolderParams.setTypeId(typeId);
        draftFolderParams.setSubtypeId(subtypeId);

        Folder folder = workflowBusinessService.createDraftFolderFromInternalTask(deskId,
                folderData.getName(),
                tenant,
                folderData.getDocList(),
                draftFolderParams
        );

        Map<String, String> metadatas = folder.getMetadata();
        metadatas.put(METADATA_KEY_IPNG_SOURCE_DESKBOX_ID, proofContent.getReceiverDeskboxId());
        metadatas.put(METADATA_KEY_IPNG_RETURN_DESKBOX_ID, proofContent.getSenderDeskboxId());
        metadatas.put(METADATA_KEY_IPNG_RETURN_BUSINESS_ID, proofContent.getBusinessId());
        metadatas.put(METADATA_KEY_IPNG_RETURN_TYPE_ID, proofContent.getType().getKey());

        String mergedMetadata = proofContent.getMetadatas().stream()
                .map(IpngMetadata::getKey)
                .reduce("", (result, elem) -> result + IPNG_METADATA_KEYS_SEPARATOR + elem);
        log.debug("preparing folder from ipng - merged metadata : {}", mergedMetadata);

        metadatas.put(METADATA_KEY_IPNG_RETURN_METADATA_IDS, mergedMetadata);

        Folder editRequest = new Folder();
        editRequest.setName(folderData.getName());
        editRequest.setType(fullSubtype.getParentType());
        editRequest.setSubtype(fullSubtype);
        editRequest.setMetadata(metadatas);

        workflowService.editFolder(tenantId, folder.getId(), editRequest);

        // Mandatory to retrieve the start tasks
        folder = workflowService.getFolder(folder.getId(), tenantId, true);
        folder.getStepList().stream()
                .filter(task -> task.getAction() == START)
                .filter(task -> task.getState() == PENDING)
                .findFirst()
                .ifPresent(task -> task.setState(CURRENT));

        try {
            // TODO use a dedicated username and role? This is only used down the road  for logging and stats AFAIK
            ArbitraryAuthenticationUtils.configureAuthentication(AUTOMATIC_TASK_USER_ID, "ADMIN");
            workflowBusinessService.doStartWorkflow(tenant.getId(), folder, deskId, null);
        } finally {
            ArbitraryAuthenticationUtils.clearAuthentication();
        }
    }


    @NotNull
    private String getBaseUrlOfSender(IpngProofWrap wrappedProofData) {
        String url = "http://iparapheur.dom.local";
        if (!this.properties.isLocalMode()) {
            Deskbox d = this.getDeskBox(wrappedProofData.getSenderEntityId(), wrappedProofData.getFolderExchange().getSenderDeskboxId());
            url = d.getUrlInstance();
        }

        return url;
    }


    @NotNull
    private FolderData retrieveFolderData(IpngProofWrap wrappedProofData) throws IOException {
        IpngProof proofContent = wrappedProofData.getFolderExchange();
        String retrieveUrl = filetransferService.buildRetrievalUrl(this.getBaseUrlOfSender(wrappedProofData), proofContent.getHash());
        FolderData folderData = new FolderData();
        FolderUnpackedContent folderContent = filetransferService.getFolderFilesFromUrl(retrieveUrl);
        log.info("got folder files, main list size : {}", folderContent.getMainDocs().size());

        if (folderContent.getDescriptionFile() == null) {
            log.error("Didn't find info file in zip, abort");
            throw new IOException("Malformed IPNG zip folder");
        }

        byte[] infoFileContent = Files.readAllBytes(folderContent.getDescriptionFile());

        String infoXml = new String(infoFileContent, StandardCharsets.UTF_8);
        int start1 = infoXml.indexOf("<folderName>");
        int start2 = infoXml.indexOf("</folderName>");
        String folderName = infoXml.substring(start1 + "<folderName>".length(), start2);
        if (StringUtils.isEmpty(folderName)) {
            log.error("Didn't find folder name in info file, using default 'IpngfolderName'");
            folderName = "IpngfolderName";
        }
        folderData.setName(folderName);

        List<DocumentBuffer> docList = new ArrayList<>();
        for (Path path : folderContent.getMainDocs()) {
            log.debug("loading file content for {}", path.getFileName());
            DocumentBuffer document = createDocumentBufferFromPath(path);
            document.setMainDocument(true);
            docList.add(document);
        }

        for (Path path : folderContent.getAnnexes()) {
            log.debug("loading file content for {}", path.getFileName());
            DocumentBuffer document = createDocumentBufferFromPath(path);
            document.setMainDocument(false);
            docList.add(document);
        }

        folderData.setDocList(docList);
        return folderData;
    }


    private DocumentBuffer createDocumentBufferFromPath(Path path) throws IOException {
        DocumentBuffer document = new DocumentBuffer();
        document.setName(path.getFileName().toString());
        document.setContentLength(Files.size(path));
        document.setContentFlux(readInputStream(() -> Files.newInputStream(path, StandardOpenOption.DELETE_ON_CLOSE),
                new DefaultDataBufferFactory(),
                FILE_TRANSFER_BUFFER_SIZE));
        return document;
    }


    @Override
    public void handleRedisMessageMap(Map<String, String> msgContent) {
        log.info("::handleRedisMessageMap");
        String nameValue = msgContent.get(IPNG_MSG_KEY_NAME);
        if (nameValue == null) {
            log.warn("::handleRedisMessageMap : missing name key in message map");
            return;
        }

        if (IPNG_MSG_TYPE_ADDPROOF.equals(nameValue)) {
            handleRedisAddProofMessage(msgContent);
        } else {
            log.warn("::handleRedisMessageMap : unhandled message name : {}", nameValue);
        }
    }


    private void handleRedisAddProofMessage(Map<String, String> msgContent) {
        log.info("::handleRedisAddProofMessage");
        if (msgContent.containsKey(IPNG_MSG_KEY_PROOF_SENDER_ID)) {
            log.info("::handleRedisAddProofMessage - handling sentProof notif ");
            SentProofNotifSubject proofNotif = new SentProofNotifSubject();
            proofNotif.setSenderEntityId(msgContent.get(IPNG_MSG_KEY_PROOF_SENDER_ID));
            proofNotif.setId(msgContent.get(IPNG_MSG_KEY_PROOF_ID));

            this.proofWasSent(proofNotif);
        }

        if (msgContent.containsKey(IPNG_MSG_KEY_PROOF_RECEIVER_ID)) {
            log.info("::handleRedisAddProofMessage - handling receivedProof notif ");
            ReceivedProofNotifSubject proofNotif = new ReceivedProofNotifSubject();
            proofNotif.setReceiverEntityId(msgContent.get(IPNG_MSG_KEY_PROOF_RECEIVER_ID));
            proofNotif.setId(msgContent.get(IPNG_MSG_KEY_PROOF_ID));

            this.handleReceivedProof(proofNotif);
        }

    }


    private void proofWasSent(SentProofNotifSubject proofNotif) {
        log.info("proofWasSent");

        IpngProofWrap proofData = getProof(proofNotif.getId());

        log.info("proofWasSent - got proof : {}", proofData);

        String tenantId = ipngBindings.getMappedTenantIdForIpngEntity(proofNotif.getSenderEntityId());

        // Should not be a status exception, and should be caught
        Tenant tenant = tenantRepository.findById(tenantId)
                .orElseThrow(() -> new IpInternalException(
                        String.format("Error : unknown tenant/entty for sent ipng proof : %s / %s",
                                tenantId, proofNotif.getSenderEntityId())
                ));

        IpngProof proofContent = proofData.getFolderExchange();
        String deskId;
        String deskboxId = proofContent.getReceiverDeskboxId();
        deskId = this.ipngBindings.getMappedDeskIdForDeskbox(tenantId, deskboxId);
        log.info("proofWasSent - deskId : {}", deskId);

        findPendingIpngFolder(tenantId, proofContent).ifPresentOrElse(pendingIpngFolder -> {
            this.workflowService.ipngProofWasSent(tenantId, deskId, pendingIpngFolder, proofData);
        }, () -> {
            log.warn("Received a 'proofwasSent' notif but no matching pending folder found.");
            log.debug("proofData : {}", proofData);
        });
    }


    private String cleanAuthToken(@NotNull String receivedToken) {
        if (receivedToken.equals("")) {
            //FIXME we should not do this here, this is an ugly hack relying on hardcoded credentials
            return this.keycloakClient().tokenManager().getAccessToken().getToken();
        }

        if (receivedToken.startsWith("Bearer ")) {
            receivedToken = receivedToken.substring(7);
        }
        return receivedToken;
    }


    ExchangeFilterFunction responseErrorFilter() {
        return ExchangeFilterFunction.ofResponseProcessor(clientResponse -> {
            if (clientResponse.statusCode().isError()) {

                return clientResponse.bodyToMono(String.class)
                        .map(errorDetails -> {
                            log.warn(errorDetails);
                            return errorDetails;
                        })
                        .flatMap(errorDetails -> Mono.error(new ResponseStatusException(clientResponse.statusCode(), errorDetails)));
            }
            return Mono.just(clientResponse);
        });
    }

}


