/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.auth;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;


@Data
@AllArgsConstructor
@ConfigurationProperties(prefix = "services.auth")
public class AuthServiceProperties {


    @Data
    @AllArgsConstructor
    public static class DataBase {

        private String host;
        private int port;
        private String name;
        private String login;
        private String password;

    }


    private String provider;
    private String host;
    private int port;
    private long ticketDelay;
    private String realm;
    private String username;
    private String password;
    private DataBase db;
    private int poolSize;


}
