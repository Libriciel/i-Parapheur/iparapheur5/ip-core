/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.auth;

import coop.libriciel.ipcore.model.auth.*;
import coop.libriciel.ipcore.model.auth.requests.UserDto;
import coop.libriciel.ipcore.model.auth.requests.UserRepresentation;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.PaginatedList;
import coop.libriciel.ipcore.utils.UserUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.support.CronExpression;

import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static coop.libriciel.ipcore.model.auth.User.USER_PREFERENCE_SINGLE_NOTIFICATIONS;
import static coop.libriciel.ipcore.model.auth.UserPrivilege.*;
import static coop.libriciel.ipcore.utils.TextUtils.*;
import static coop.libriciel.ipcore.utils.UserUtils.AUTOMATIC_TASK_USER_ID;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE;


public interface AuthServiceInterface {


    Logger log = LogManager.getLogger(AuthServiceInterface.class);

    String BEAN_NAME = "authService";
    String PREFERENCES_PROVIDER_KEY = "services.auth.provider";

    List<String> INTERNAL_USERNAMES = List.of("service-account-ipcore-api", "soapui", "rest-admin");


    // <editor-fold desc="Utils">


    default List<String> filterMatchingTenantIds(UserPrivilege privilege, @NotNull List<String> roleNames) {

        Pattern pattern = switch (privilege) {
            case TENANT_ADMIN -> TENANT_ID_ADMIN_NAME_PATTERN;
            case FUNCTIONAL_ADMIN -> TENANT_ID_FUNCTIONAL_ADMIN_NAME_PATTERN;
            default -> throw new RuntimeException("getRoleTenantId is not applicable to the privilege " + privilege);
        };

        return roleNames
                .stream()
                .map(pattern::matcher)
                .filter(Matcher::matches)
                .map(m -> m.group(MATCH_GROUP_TENANT_ID))
                .toList();
    }


    default void checkUserCrudPrivileges(@NotNull User currentUser, @NotNull User targetExistingUser, @Nullable UserDto targetRequest) {

        UserPrivilege targetRequestPrivilege = Optional.ofNullable(targetRequest).map(UserDto::getPrivilege).orElse(null);
        boolean isTargetRequestSuperAdmin = targetRequestPrivilege == SUPER_ADMIN;
        boolean isCurrentUserSuperAdmin = currentUser.getPrivilege() == SUPER_ADMIN;
        boolean isTargetUserSuperAdmin = targetExistingUser.getPrivilege() == SUPER_ADMIN;
        if (!isCurrentUserSuperAdmin && (isTargetUserSuperAdmin || isTargetRequestSuperAdmin)) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.cannot_touch_a_super_admin");
        }

        if (isInternalUser(targetExistingUser)) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.cannot_touch_an_internal_user");
        }

        if (targetRequest != null) {
            boolean isSelfEdit = StringUtils.equals(currentUser.getId(), targetExistingUser.getId());
            boolean isPrivilegeChanged = currentUser.getPrivilege() != targetRequest.getPrivilege();
            if (isSelfEdit && isPrivilegeChanged) {
                throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.cannot_promote_or_un_promote_yourself");
            }
        }
    }


    boolean isInternalUser(@NotNull User user);


    // </editor-fold desc="Utils">


    // <editor-fold desc="Tenants CRUDL">


    @NotNull String createTenant(@NotNull String tenantName);


    void deleteTenant(@NotNull String tenantId);


    // </editor-fold desc="Tenants CRUDL">


    // <editor-fold desc="Users CRUDL">


    @NotNull String createUser(@NotNull UserDto request, @Nullable String currentTenantId);


    @Nullable User findUserById(@NotNull String id);


    default void updateUser(@NotNull ModelMapper modelMapper,
                            @NotNull User originalUser,
                            @NotNull UserDto request,
                            @Nullable List<String> originalAdministeredTenantIds) {
        updateUser(modelMapper, originalUser, request, originalAdministeredTenantIds, null);
    }


    default void updateUser(@NotNull ModelMapper modelMapper,
                            @NotNull User originalUser,
                            @NotNull UserDto request,
                            @Nullable List<String> originalAdministeredTenantIds,
                            String specificTenantId) {

        User pendingRequestUser = modelMapper.map(request, User.class);
        pendingRequestUser.setId(originalUser.getId());

        // Permission update

        if (originalUser.getPrivilege() != request.getPrivilege()) {
            updateUserGlobalPrivileges(originalUser, request, specificTenantId);
        }

        if ((request.getPrivilege() == TENANT_ADMIN) && isNotEmpty(originalAdministeredTenantIds) && isNotEmpty(request.getAdministeredTenantIds())) {

            originalAdministeredTenantIds.stream()
                    .filter(origId -> request.getAdministeredTenantIds().stream().noneMatch(requestId -> StringUtils.equals(origId, requestId)))
                    .toList()
                    .forEach(idToRemove -> updateUserPrivileges(idToRemove, originalUser, NONE));

            request.getAdministeredTenantIds().stream()
                    .filter(origId -> originalAdministeredTenantIds.stream().noneMatch(requestId -> StringUtils.equals(origId, requestId)))
                    .toList()
                    .forEach(idToAdd -> updateUserPrivileges(idToAdd, originalUser, TENANT_ADMIN));
        }

        // Integrity check

        boolean firstNameChanged = hasAttributeChanged(originalUser, pendingRequestUser, User::getFirstName);
        boolean lastNameChanged = hasAttributeChanged(originalUser, pendingRequestUser, User::getLastName);
        boolean emailChanged = hasAttributeChanged(originalUser, pendingRequestUser, User::getEmail);

        boolean hasInnerFieldChanged = firstNameChanged || lastNameChanged || emailChanged;
        if (hasInnerFieldChanged && originalUser.getIsLdapSynchronized()) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.cannot_modify_LDAP_user");
        }

        // Update

        boolean complementaryFieldChanged = hasAttributeChanged(originalUser, pendingRequestUser, User::getComplementaryField);
        boolean hasAnyFieldChanged = hasInnerFieldChanged || complementaryFieldChanged;

        if (hasAnyFieldChanged) {
            updateUser(
                    originalUser.getId(),
                    getNewAttributeIfChanged(originalUser, pendingRequestUser, User::getFirstName),
                    getNewAttributeIfChanged(originalUser, pendingRequestUser, User::getLastName),
                    getNewAttributeIfChanged(originalUser, pendingRequestUser, User::getEmail),
                    getNewAttributeIfChanged(originalUser, pendingRequestUser, User::getComplementaryField)
            );
        }
    }


    void updateUser(@NotNull String userId,
                    @Nullable String firstName,
                    @Nullable String lastName,
                    @Nullable String email,
                    @Nullable String complementaryField);


    default void updateUserPreferences(@NotNull String userId,
                                       @NotNull User oldUserPreferences,
                                       @NotNull User newUserPreferences) {

        // Integrity checks

        String newCronExpression = newUserPreferences.getNotificationsCronFrequency();
        boolean isCronSet = StringUtils.isNotEmpty(newCronExpression);
        boolean isCronNotInternalValue = !StringUtils.equalsAny(newCronExpression, "none", USER_PREFERENCE_SINGLE_NOTIFICATIONS);
        boolean isCronNotValid = !CronExpression.isValidExpression(newCronExpression);
        log.debug("updateUser newCronExpression:{}", newCronExpression);
        if (isCronSet && isCronNotInternalValue && isCronNotValid) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.invalid_cron_expression");
        }

        // Update, if needed

        boolean hasNotificationOnConfidentialFoldersChanged =
                hasAttributeChangedBoolean(oldUserPreferences, newUserPreferences, User::isNotifiedOnConfidentialFolders);
        boolean hadNotificationOnFollowedFoldersChanged =
                hasAttributeChangedBoolean(oldUserPreferences, newUserPreferences, User::isNotifiedOnFollowedFolders);
        boolean hasNotificationOnLateFoldersChanged =
                hasAttributeChangedBoolean(oldUserPreferences, newUserPreferences, User::isNotifiedOnLateFolders);
        boolean redirectionMailChanged =
                hasAttributeChanged(oldUserPreferences, newUserPreferences, User::getNotificationsRedirectionMail);
        boolean notificationCronFrequencyChanged =
                hasAttributeChanged(oldUserPreferences, newUserPreferences, User::getNotificationsCronFrequency);

        boolean hasAnyFieldChanged = hasNotificationOnConfidentialFoldersChanged
                || hadNotificationOnFollowedFoldersChanged
                || hasNotificationOnLateFoldersChanged
                || redirectionMailChanged
                || notificationCronFrequencyChanged;

        if (hasAnyFieldChanged) {
            updateUserPreferences(
                    userId,
                    getNewAttributeIfChanged(oldUserPreferences, newUserPreferences, User::getNotificationsRedirectionMail),
                    getNewAttributeIfChangedBoolean(oldUserPreferences, newUserPreferences, User::isNotifiedOnConfidentialFolders),
                    getNewAttributeIfChangedBoolean(oldUserPreferences, newUserPreferences, User::isNotifiedOnFollowedFolders),
                    getNewAttributeIfChangedBoolean(oldUserPreferences, newUserPreferences, User::isNotifiedOnLateFolders),
                    getNewAttributeIfChanged(oldUserPreferences, newUserPreferences, User::getNotificationsCronFrequency)
            );
        }
    }


    void updateUserPreferences(@NotNull String userId,
                               @Nullable String notificationsRedirectionMail,
                               @Nullable String isNotifiedOnConfidentialFolders,
                               @Nullable String isNotifiedOnFollowedFolders,
                               @Nullable String isNotifiedOnLateFolders,
                               @Nullable String notificationsCronFrequency);


    void updateUserInternalMetadata(@NotNull String userId, @Nullable Integer contentGroupIndex, @Nullable String contentNodeId,
                                    @Nullable String signatureImageContentId);


    void updateUserGlobalPrivileges(@NotNull User user, @NotNull UserDto updatedUser, String specificTenantId);


    void resetUserPassword(@NotNull String userId, @NotNull String password);


    void deleteUser(@NotNull String userId);


    int countLoggedInUsers();


    @NotNull Set<String> listTenantsForUser(String userId);


    @NotNull Page<User> listUsers(@NotNull Pageable pageable, @Nullable String searchTerm);


    @NotNull List<User> getUsersByIds(@NotNull Set<String> userIds);


    // </editor-fold desc="Users CRUDL">


    /**
     * Checks the given users privileges,
     * and returns the appropriate list amongst them.
     *
     * @param userIds        a set of (existing) user Ids
     * @param privilege      the privilege to test
     * @param targetTenantId if set, narrow the test to this specific target
     * @return A new immutable {@link Set}
     */
    @NotNull Set<String> filterUsersWithPrivilege(@NotNull Set<String> userIds, @NotNull UserPrivilege privilege, @Nullable String targetTenantId);


    /**
     * Retrieving the super admins, directly from the Auth engine.
     *
     * @param maxResults a threshold, the poor man's pagination
     * @return an immutable and (probably) unordered {@link List}
     */
    @NotNull List<User> getSuperAdminsList(int maxResults);


    // <editor-fold desc="Tenant users CRUDL">


    void addUserToTenant(@NotNull User user, @NotNull String tenantId, @NotNull UserPrivilege privilege);


    void addDefaultUserOnTenant(Tenant tenant);


    @Nullable User findTenantUserById(@Nullable String tenantId, @NotNull String id);


    void updateUserPrivileges(@NotNull String tenantId, @NotNull User user, @NotNull UserPrivilege newPrivilege);


    void removeUserFromTenant(@NotNull String userId, @NotNull String tenantId);


    @NotNull Page<User> listTenantUsers(@NotNull String tenantId,
                                        @NotNull Pageable pageable,
                                        @Nullable String searchTerm);


    @NotNull PaginatedList<User> listTenantAdminUsers(@NotNull String tenantId, int page, int pageSize);


    // </editor-fold desc="Tenant users CRUDL">


    // <editor-fold desc="Desks CRUDL">


    @NotNull String createDesk(@NotNull String tenantId, @NotNull String name, @NotNull String shortName, @Nullable String description,
                               @Nullable String parentDeskId);


    /**
     * @param tenantId
     * @param deskId
     * @return
     * @deprecated Replaced by {@link #findDeskByIdNoException(String, String)}}
     */
    @Deprecated
    @Nullable Desk findDeskById(@Nullable String tenantId, @NotNull String deskId);


    @Nullable Desk findDeskByIdNoException(@Nullable String tenantId, @NotNull String deskId);


    void editDesk(@NotNull String tenantId, @NotNull Desk previousDeskVersion, @NotNull String name, @NotNull String shortName,
                  @Nullable String description, @Nullable String directParentId);


    void deleteDesk(@NotNull String deskId);


    @NotNull Page<Desk> listDesks(@NotNull String tenantKey, @NotNull Pageable pageable, @NotNull List<String> reverseIdList, boolean collapseAll);


    @NotNull Page<Desk> searchDesks(@NotNull String tenantId, @NotNull Pageable pageable, @NotNull List<String> reverseIdList,
                                    @NotNull String searchTerm, boolean collapseAl);


    // </editor-fold desc="Desks CRUDL">


    default @Nullable Desk findDeskByIdOrShortName(@NotNull String tenantId, @Nullable String deskIdOrShortName) {

        if (StringUtils.isEmpty(deskIdOrShortName)) {
            return null;
        }

        return Optional.of(deskIdOrShortName)
                // Easier one first: we check for an id match
                .map(i -> findDeskByIdNoException(tenantId, i))
                // Harder one then: Searching for a shortName match
                .or(() -> findDeskByShortName(tenantId, deskIdOrShortName)
                        .stream()
                        // We may have multiple results, if a desk short name overlaps another one.
                        // "desk_11" and "desk_12" can be returned on a "desk_1" request.
                        // We'll fetch the full Desks, and return the first exact shortName match.
                        .map(DeskRepresentation::getId)
                        .map(id -> findDeskByIdNoException(tenantId, id))
                        .filter(Objects::nonNull)
                        .filter(desk -> StringUtils.equals(desk.getShortName(), deskIdOrShortName))
                        .findFirst()
                )
                .orElse(null);
    }


    @NotNull Page<DeskRepresentation> findDeskByShortName(@NotNull String tenantId, @NotNull String shortName);


    @NotNull Map<String, String> getDeskNames(@NotNull Set<String> deskIds);


    // <editor-fold desc="Desk users CRUDL">


    void addUsersToDesk(@NotNull String deskId, @NotNull Collection<String> userIdList);


    @NotNull Page<DeskRepresentation> getDesksFromUser(@NotNull String userId, Pageable pageable, @Nullable String searchTerm);


    void removeUsersFromDesk(@NotNull String deskId, @NotNull Collection<String> userIds);


    @NotNull PaginatedList<User> listUsersFromDesk(String tenantId, @NotNull String deskId, int page, int pageSize);


    // </editor-fold desc="Desk users CRUDL">


    void refreshUsersDeskStatus(@NotNull List<User> userList, @NotNull String deskId);


    void populateDeskNamesAndTenantIds(@NotNull Map<String, Desk> deskMap);

    String getServiceAccountUserId();

    default void updateFolderReferencedUserNames(@Nullable List<? extends Folder> folderList) {

        if (CollectionUtils.isEmpty(folderList)) {
            return;
        }

        // Build referenced user list

        List<UserRepresentation> referencedUsers = folderList.stream()
                .map(Folder::getStepList)
                .flatMap(Collection::stream)
                .map(Task::getUser)
                .filter(Objects::nonNull)
                .filter(u -> StringUtils.isNotEmpty(u.getId()))
                .toList();

        // Actual update

        updateUserNames(referencedUsers);
    }


    default void updateTaskReferencedUserNames(@Nullable List<? extends Task> taskList) {

        if (CollectionUtils.isEmpty(taskList)) {
            return;
        }

        // Build referenced user list

        List<UserRepresentation> referencedUsers = taskList.stream()
                .map(Task::getUser)
                .filter(Objects::nonNull)
                .filter(u -> StringUtils.isNotEmpty(u.getId()))
                .toList();

        // Actual update

        updateUserNames(referencedUsers);
    }


    default void updateUserNames(@Nullable List<UserRepresentation> userList) {

        if (CollectionUtils.isEmpty(userList)) {
            return;
        }

        // Fetch informations

        Set<String> userIds = userList.stream()
                .map(UserRepresentation::getId)
                .filter(StringUtils::isNotEmpty)
                .filter(i -> !StringUtils.equals(i, AUTOMATIC_TASK_USER_ID))
                .collect(toSet());

        Map<String, User> referencedUsersMap = getUsersByIds(userIds).stream()
                .collect(toMap(User::getId, u -> u));

        User automaticUser = UserUtils.getAutomaticUser();
        referencedUsersMap.put(automaticUser.getId(), automaticUser);

        // Update in place

        userList.stream()
                .filter(u -> referencedUsersMap.containsKey(u.getId()))
                .forEach(u -> {
                    User fullUser = referencedUsersMap.get(u.getId());
                    u.setUserName(fullUser.getUserName());
                    u.setFirstName(fullUser.getFirstName());
                    u.setLastName(fullUser.getLastName());
                });
    }


    default void updateFolderReferencedDeskNames(@Nullable List<? extends Folder> folderList) {

        if (CollectionUtils.isEmpty(folderList)) {
            return;
        }

        // Build referenced desk list

        List<Task> referencedTaskList = folderList.stream()
                .map(Folder::getStepList)
                .filter(CollectionUtils::isNotEmpty)
                .flatMap(Collection::stream)
                .filter(Objects::nonNull)
                .toList();

        List<DeskRepresentation> referencedDeskList = Stream.of(
                        referencedTaskList.stream().map(Task::getDesks).flatMap(Collection::stream),
                        referencedTaskList.stream().map(Task::getNotifiedDesks).flatMap(Collection::stream),
                        referencedTaskList.stream().map(Task::getDelegatedByDesk),
                        folderList.stream().map(Folder::getOriginDesk),
                        folderList.stream().map(Folder::getFinalDesk)
                )
                .flatMap(Function.identity())
                .filter(Objects::nonNull)
                .toList();

        // Actual update

        updateDeskNames(referencedDeskList);
    }


    default void updateTaskReferencedDeskNames(@Nullable List<Task> taskList) {

        if (CollectionUtils.isEmpty(taskList)) {
            return;
        }

        // Build referenced desk list

        List<DeskRepresentation> deskList = taskList.stream()
                .map(Task::getDesks)
                .flatMap(Collection::stream)
                .toList();

        // Actual update

        updateDeskNames(deskList);
    }


    default void updateDeskNames(@Nullable List<? extends DeskRepresentation> deskList) {

        if (CollectionUtils.isEmpty(deskList)) {
            return;
        }

        // Fetch informations

        Set<String> referencedIds = deskList.stream()
                .filter(Objects::nonNull)
                .map(DeskRepresentation::getId)
                .filter(StringUtils::isNotEmpty)
                .collect(toSet());

        Map<String, String> deskNames = getDeskNames(referencedIds);

        // Update in place

        deskList.stream()
                .filter(Objects::nonNull)
                .filter(d -> StringUtils.isNotEmpty(d.getId()))
                .forEach(desk -> desk.setName(deskNames.get(desk.getId())));
    }


    @NotNull PasswordPolicies getPasswordPolicies();


}
