/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.auth;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import coop.libriciel.ipcore.model.auth.PasswordPolicies.KnownRegexes;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.auth.*;
import coop.libriciel.ipcore.model.auth.requests.UserDto;
import coop.libriciel.ipcore.model.auth.requests.UserSortBy;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.workflow.DelegationRule;
import coop.libriciel.ipcore.services.mail.NotificationServiceProperties;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.PaginatedList;
import coop.libriciel.ipcore.utils.RequestUtils;
import coop.libriciel.ipcore.utils.TextUtils;
import jakarta.annotation.PostConstruct;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringSubstitutor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jooq.*;
import org.jooq.impl.DSL;
import org.jooq.impl.DefaultDataType;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.*;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RealmRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.function.Function;
import java.util.function.LongSupplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static coop.libriciel.ipcore.model.auth.Desk.*;
import static coop.libriciel.ipcore.model.auth.PasswordPolicies.DEFAULT_PASSWORD_MINIMUM_LENGTH;
import static coop.libriciel.ipcore.model.auth.PasswordPolicies.KnownRegexes.ENTROPY_80;
import static coop.libriciel.ipcore.model.auth.User.*;
import static coop.libriciel.ipcore.model.auth.UserPrivilege.*;
import static coop.libriciel.ipcore.model.auth.requests.UserSortBy.USERNAME;
import static coop.libriciel.ipcore.services.database.PostgresService.TRUE_CONDITION;
import static coop.libriciel.ipcore.utils.ApiUtils.MAX_USERS_COUNT;
import static coop.libriciel.ipcore.utils.CollectionUtils.computeTotal;
import static coop.libriciel.ipcore.utils.PaginatedList.MAX_BIG_PAGE_SIZE;
import static coop.libriciel.ipcore.utils.TextUtils.*;
import static java.lang.Boolean.FALSE;
import static java.net.HttpURLConnection.HTTP_CONFLICT;
import static java.net.HttpURLConnection.HTTP_CREATED;
import static java.util.Arrays.asList;
import static java.util.Arrays.stream;
import static java.util.Collections.*;
import static java.util.UUID.randomUUID;
import static java.util.stream.Collectors.*;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.jooq.SQLDialect.POSTGRES;
import static org.jooq.conf.ParamType.INLINED;
import static org.jooq.impl.DSL.max;
import static org.jooq.impl.DSL.*;
import static org.jooq.impl.SQLDataType.CLOB;
import static org.jooq.util.postgres.PostgresDSL.arrayAppend;
import static org.keycloak.representations.idm.CredentialRepresentation.PASSWORD;
import static org.springframework.data.domain.Sort.Direction.ASC;
import static org.springframework.http.HttpStatus.*;


@Log4j2
@Service(AuthServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = AuthServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = "keycloak")
public class KeycloakService implements AuthServiceInterface {


    public static final String DESK_ROLE_PREFIX = "desk_";
    public static final String TENANT_PLACEHOLDER = "tenant";
    public static final String DESK_PLACEHOLDER = "desk";
    public static final String TARGET_DESK_PLACEHOLDER = "target_desk";
    public static final String TARGET_TYPE_PLACEHOLDER = "target_type";
    public static final String TARGET_SUBTYPE_PLACEHOLDER = "target_subtype";
    public static final String USER_PLACEHOLDER = "user";
    public static final String METADATA_PLACEHOLDER = "metadata";
    public static final String SUBTYPE_PLACEHOLDER = "subtype";
    public static final String START_DATE_PLACEHOLDER = "start_date";
    public static final String END_DATE_PLACEHOLDER = "end_date";
    public static final String SUPER_ADMIN_ROLE_NAME = "admin";

    public static final String TENANT_INTERNAL_NAME = String.format("tenant_${%s}", TENANT_PLACEHOLDER);
    public static final String TENANT_ADMIN_INTERNAL_NAME = String.format("tenant_${%s}_admin", TENANT_PLACEHOLDER);
    public static final String TENANT_FUNCTIONAL_ADMIN_INTERNAL_NAME = String.format("tenant_${%s}_functional_admin", TENANT_PLACEHOLDER);
    public static final String DESK_INTERNAL_NAME = String.format("tenant_${%s}_desk_${%s}", TENANT_PLACEHOLDER, DESK_PLACEHOLDER);
    public static final String SUBTYPE_INTERNAL_NAME = String.format("tenant_${%s}_subtype_${%s}", TENANT_PLACEHOLDER, SUBTYPE_PLACEHOLDER);
    public static final String METADATA_INTERNAL_NAME = String.format("tenant_${%s}_metadata_${%s}", TENANT_PLACEHOLDER, METADATA_PLACEHOLDER);

    public static final String DESK_ID_REGEX = "^" + ID_REGEX + "$";
    private static final String CLIENT_ID_ADMIN_CLI = "admin-cli";

    private static final String SQL_REALM_TABLE = "realm";
    private static final String SQL_REALM_ID = "id";
    private static final String SQL_REALM_NAME = "name";
    private static final String SQL_USER_TABLE = "user_entity";
    private static final String SQL_USER_IDENTITY_PROVIDER_TABLE = "federated_identity";
    private static final String SQL_USER_ID = "id";
    private static final String SQL_IDP_TABLE_USER_ID = "user_id";
    private static final String SQL_USER_USERNAME = "username";
    private static final String SQL_USER_EMAIL = "email";
    private static final String SQL_USER_FIRST_NAME = "first_name";
    private static final String SQL_USER_LAST_NAME = "last_name";
    private static final String SQL_ROLE_ID = "id";
    private static final String SQL_ROLE_REALM_ID = "realm_id";
    private static final String SQL_ROLE_FULL_NAME = "name";
    private static final String SQL_ROLE_TABLE = "keycloak_role";
    private static final String SQL_FEDERATION_LINK = "federation_link";
    private static final String SQL_SSO_IDENTITY_PROVIDER_FIELD = "identity_provider";
    private static final String SQL_ROLE_DESCRIPTION = "description";
    private static final String SQL_ROLE_DELEGATING_ID = "delegating_" + SQL_ROLE_ID;
    private static final String SQL_ROLE_PARENT_METADATA_DELEGATION = "parent_delegation_by_metadata";

    private static final String IPARAPHEUR_REALM = "i-Parapheur";


    private static HikariDataSource dataSource;
    private @Value("${services.auth.initial-admin}") String initialAdminUsername;
    private @Value("${keycloak.resource}") String client;
    private @Value("${springdoc.swagger-ui.oauth.clientId}") String keycloakWebClientId;


    ClientResource clientResource;
    RealmResource realmResource;
    UsersResource usersResourceClient;
    RolesResource rolesResourceClient;
    ScheduledExecutorService executorService;
    PasswordPolicies passwordPolicies;


    // <editor-fold desc="Beans">


    private final AuthServiceProperties authServiceProperties;
    private final Keycloak keycloak;
    private final ModelMapper modelMapper;
    private final NotificationServiceProperties notificationServiceProperties;


    @Autowired
    public KeycloakService(AuthServiceProperties properties,
                           Keycloak keycloak,
                           ModelMapper modelMapper,
                           NotificationServiceProperties notificationServiceProperties) {
        this.authServiceProperties = properties;
        this.keycloak = keycloak;
        this.modelMapper = modelMapper;
        this.notificationServiceProperties = notificationServiceProperties;
    }


    @PostConstruct
    public void setup() {

        String url = "jdbc:postgresql://%s:%d/%s".formatted(
                authServiceProperties.getDb().getHost(),
                authServiceProperties.getDb().getPort(),
                authServiceProperties.getDb().getName()
        );

        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(url);
        config.setUsername(authServiceProperties.getDb().getLogin());
        config.setPassword(authServiceProperties.getDb().getPassword());
        config.setMaxLifetime(5L * 60L * 1000L);
        dataSource = new HikariDataSource(config);

        executorService = Executors.newSingleThreadScheduledExecutor();

        generateClient();

        String passwordPoliciesString = realmResource.toRepresentation().getPasswordPolicy();
        passwordPolicies = KeycloakService.parsePasswordPolicies(passwordPoliciesString);

        checkAndAddMissingPasswordPolicies();
        updateSmtpSettings();
    }


    // </editor-fold desc="Beans">


    /**
     * On every start, we check the existing policies, adding the missing ones.
     * Some users already have some other rules (digits, uppercase, lowercase...), we don't want to touch that.
     * We're only adding (or increasing) rules.
     */
    private void checkAndAddMissingPasswordPolicies() {

        // For now, we make every request match one of the known ones. We want to unify the existing instances.
        // Every instance ≥ [5.0.29|5.1.7] will have the 80-entropy default one. Prior to that... We just aren't sure.
        // TODO: Once no more instance < [5.0.29|5.1.7] will exist, we MAY reverse the logic, and only update the known ones.
        //  That COULD allow some custom regexes. Maybe we don't want that, but it could be something wanted.
        //  But for now (early-2025), we cannot do better than normalize things.
        boolean doesRegexNeedUpdate = stream(KnownRegexes.values())
                .map(KnownRegexes::getRegex)
                .noneMatch(regex -> StringUtils.equals(regex, passwordPolicies.getRegexPattern()));

        // If any, we don't want to downgrade an user-defined upper threshold
        boolean doesMinLengthNeedUpdate = passwordPolicies.getMinLength() < DEFAULT_PASSWORD_MINIMUM_LENGTH;
        boolean doesMailEquivalenceNeedUpdate = !passwordPolicies.isNotEmail();
        boolean doesUsernameEquivalenceNeedUpdate = !passwordPolicies.isNotUsername();

        boolean doesSettingsChange = doesMinLengthNeedUpdate || doesMailEquivalenceNeedUpdate || doesUsernameEquivalenceNeedUpdate || doesRegexNeedUpdate;

        // Increase existing restrictions, if needed

        if (doesRegexNeedUpdate) {
            passwordPolicies.setRegexPattern(ENTROPY_80.getRegex());
        }

        if (doesMinLengthNeedUpdate) {
            passwordPolicies.setMinLength(DEFAULT_PASSWORD_MINIMUM_LENGTH);
        }

        if (doesMailEquivalenceNeedUpdate) {
            passwordPolicies.setNotEmail(true);
        }

        if (doesUsernameEquivalenceNeedUpdate) {
            passwordPolicies.setNotUsername(true);
        }

        // Actual save

        if (doesSettingsChange) {
            log.info("The password policy is outdated, and will be reset to the current version standards.");

            String serializedUpdatedPolicies = KeycloakService.serializePasswordPolicies(passwordPolicies);

            RealmRepresentation realmRepresentation = realmResource.toRepresentation();
            realmRepresentation.setPasswordPolicy(serializedUpdatedPolicies);

            keycloak.realm(authServiceProperties.getRealm()).update(realmRepresentation);
        }
    }


    /**
     * On every start, we refresh the existing SMTP settings, updating those if they have been changed.
     * The JSON-way only set it on startup, and never updates it.
     * <p>
     * Keycloak does not give the password back. Since we can't know if it was changed at all,
     * we have to reset everything on every startup.
     */
    private void updateSmtpSettings() {

        Map<String, String> smtpPropertiesMap = new HashMap<>();
        smtpPropertiesMap.put("host", notificationServiceProperties.getHost());
        smtpPropertiesMap.put("port", Objects.toString(notificationServiceProperties.getPort(), null));
        smtpPropertiesMap.put("user", notificationServiceProperties.getUsername());
        smtpPropertiesMap.put("password", notificationServiceProperties.getPassword());
        smtpPropertiesMap.put("auth", Objects.toString(notificationServiceProperties.getAuth(), FALSE.toString()));
        smtpPropertiesMap.put("from", notificationServiceProperties.getFrom());
        smtpPropertiesMap.put("fromDisplayName", "iparapheur");
        smtpPropertiesMap.put("ssl", Objects.toString(notificationServiceProperties.getSsl(), FALSE.toString()));
        smtpPropertiesMap.put("starttls", Objects.toString(notificationServiceProperties.getStartTls(), FALSE.toString()));

        RealmRepresentation realmRepresentation = realmResource.toRepresentation();
        realmRepresentation.setSmtpServer(smtpPropertiesMap);
        keycloak.realm(authServiceProperties.getRealm()).update(realmRepresentation);
    }


    /**
     * We usually have the Client ID only, and not the Client's Resource ID.
     * This is why we have to go for it here.
     * Yep, the loop is kinda ugly, but that's how they do it in their own tests.
     *
     * @return a properly fetched interface
     * @see <a href="https://github.com/keycloak/keycloak/blob/master/testsuite/integration-arquillian/tests/base/src/test/java/org/keycloak/testsuite/authz/RolePolicyTest.java>Keycloak's tests</a></a>
     */
    private void generateClient() {
        realmResource = keycloak.realm(authServiceProperties.getRealm());
        ClientsResource clients = realmResource.clients();
        clientResource = clients.findByClientId(client)
                .stream()
                .map(representation -> clients.get(representation.getId()))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Expected client [resource-server-test]"));

        usersResourceClient = realmResource.users();
        rolesResourceClient = realmResource.roles();
    }


    private @Nullable RoleRepresentation getRole(@NotNull String roleName) {
        RoleRepresentation roleRepresentation = null;
        try {
            roleRepresentation = rolesResourceClient.get(roleName).toRepresentation();
        } catch (NotFoundException e) {
            log.warn("Role not found:{}", roleName);
        }
        return roleRepresentation;
    }


    static @NotNull PasswordPolicies parsePasswordPolicies(String unparsedPasswordPolicy) {

        //noinspection RegExpUnnecessaryNonCapturingGroup
        Pattern parseKeycloakPoliciesRegex = Pattern
                .compile("^(?:"
                        + "(?:"
                        + "length\\((?<minLength>\\d+)\\)|"
                        + "maxLength\\((?<maxLength>\\d+)\\)|"
                        + "lowerCase\\((?<lowerCase>\\d+)\\)|"
                        + "upperCase\\((?<upperCase>\\d+)\\)|"
                        + "digits\\((?<digits>\\d+)\\)|"
                        + "specialChars\\((?<specialChars>\\d+)\\)|"
                        + "notUsername\\((?<notUsername>undefined)\\)|"
                        + "notEmail\\((?<notEmail>undefined)\\)|"
                        + "regexPattern\\((?<regexPattern>.*?)\\)(?=(?:\\sand\\s)|$)|"
                        + "hashAlgorithm\\((?<hashAlgorithm>.*?(?=.(?:\\sand\\s)|$))\\)|"
                        + "forceExpiredPasswordChange\\((?<forceExpiredPasswordChange>\\d+)\\)|"
                        + "passwordHistory\\((?<passwordHistory>\\d+)\\)|"
                        + "maxAuthAge\\((?<maxAuthAge>\\d+)\\)"
                        + ")"
                        + "(?:\\sand\\s|$)"
                        + ")*$");

        return Optional.ofNullable(unparsedPasswordPolicy)
                .filter(StringUtils::isNotEmpty)
                .map(parseKeycloakPoliciesRegex::matcher)
                .filter(Matcher::matches)
                .map(m -> {
                    PasswordPolicies passwordPolicies = new PasswordPolicies();

                    Optional.ofNullable(m.group("minLength"))
                            .filter(TextUtils::isInteger)
                            .map(Integer::parseInt)
                            .ifPresent(passwordPolicies::setMinLength);

                    Optional.ofNullable(m.group("maxLength"))
                            .filter(TextUtils::isInteger)
                            .map(Integer::parseInt)
                            .ifPresent(passwordPolicies::setMaxLength);

                    Optional.ofNullable(m.group("notUsername"))
                            .filter(s -> StringUtils.equals(s, "undefined"))
                            .ifPresent(s -> passwordPolicies.setNotUsername(true));

                    Optional.ofNullable(m.group("notEmail"))
                            .filter(s -> StringUtils.equals(s, "undefined"))
                            .ifPresent(s -> passwordPolicies.setNotEmail(true));

                    Optional.ofNullable(m.group("specialChars"))
                            .filter(TextUtils::isInteger)
                            .map(Integer::parseInt)
                            .ifPresent(passwordPolicies::setSpecialCharsMinCount);

                    Optional.ofNullable(m.group("upperCase"))
                            .filter(TextUtils::isInteger)
                            .map(Integer::parseInt)
                            .ifPresent(passwordPolicies::setUppercaseCharsMinCount);

                    Optional.ofNullable(m.group("lowerCase"))
                            .filter(TextUtils::isInteger)
                            .map(Integer::parseInt)
                            .ifPresent(passwordPolicies::setLowercaseCharsMinCount);

                    Optional.ofNullable(m.group("digits"))
                            .filter(TextUtils::isInteger)
                            .map(Integer::parseInt)
                            .ifPresent(passwordPolicies::setDigitsMinCount);

                    Optional.ofNullable(m.group("regexPattern"))
                            .filter(StringUtils::isNotEmpty)
                            .ifPresent(passwordPolicies::setRegexPattern);

                    Optional.ofNullable(m.group("hashAlgorithm"))
                            .filter(StringUtils::isNotEmpty)
                            .ifPresent(passwordPolicies::setHashAlgorithm);

                    Optional.ofNullable(m.group("forceExpiredPasswordChange"))
                            .filter(TextUtils::isInteger)
                            .map(Integer::parseInt)
                            .ifPresent(passwordPolicies::setForceExpiredPasswordChange);

                    Optional.ofNullable(m.group("passwordHistory"))
                            .filter(TextUtils::isInteger)
                            .map(Integer::parseInt)
                            .ifPresent(passwordPolicies::setPasswordHistory);

                    Optional.ofNullable(m.group("maxAuthAge"))
                            .filter(TextUtils::isInteger)
                            .map(Integer::parseInt)
                            .ifPresent(passwordPolicies::setMaxAuthAge);

                    return passwordPolicies;
                })
                .orElseGet(() -> {
                    log.error("Something went wrong parsing the Keycloak's password policies. Sending the default one.");
                    return new PasswordPolicies();
                });
    }


    static @NotNull String serializePasswordPolicies(@NotNull PasswordPolicies passwordPolicies) {
        List<String> rules = new ArrayList<>();

        if (passwordPolicies.getMinLength() > 0) {
            rules.add("length(%d)".formatted(passwordPolicies.getMinLength()));
        }
        if (passwordPolicies.getMaxLength() > 0) {
            rules.add("maxLength(%d)".formatted(passwordPolicies.getMaxLength()));
        }
        if (passwordPolicies.isNotUsername()) {
            rules.add("notUsername(undefined)");
        }
        if (passwordPolicies.isNotEmail()) {
            rules.add("notEmail(undefined)");
        }
        if (passwordPolicies.getSpecialCharsMinCount() > 0) {
            rules.add("specialChars(%d)".formatted(passwordPolicies.getSpecialCharsMinCount()));
        }
        if (passwordPolicies.getUppercaseCharsMinCount() > 0) {
            rules.add("upperCase(%d)".formatted(passwordPolicies.getUppercaseCharsMinCount()));
        }
        if (passwordPolicies.getLowercaseCharsMinCount() > 0) {
            rules.add("lowerCase(%d)".formatted(passwordPolicies.getLowercaseCharsMinCount()));
        }
        if (StringUtils.isNotEmpty(passwordPolicies.getRegexPattern())) {
            rules.add("regexPattern(%s)".formatted(passwordPolicies.getRegexPattern()));
        }
        if (passwordPolicies.getDigitsMinCount() > 0) {
            rules.add("digits(%d)".formatted(passwordPolicies.getDigitsMinCount()));
        }
        if (StringUtils.isNotEmpty(passwordPolicies.getHashAlgorithm())) {
            rules.add("hashAlgorithm(%s)".formatted(passwordPolicies.getHashAlgorithm()));
        }
        if (passwordPolicies.getForceExpiredPasswordChange() > 0) {
            rules.add("forceExpiredPasswordChange(%d)".formatted(passwordPolicies.getForceExpiredPasswordChange()));
        }
        if (passwordPolicies.getPasswordHistory() > 0) {
            rules.add("passwordHistory(%d)".formatted(passwordPolicies.getPasswordHistory()));
        }
        if (passwordPolicies.getMaxAuthAge() > 0) {
            rules.add("maxAuthAge(%d)".formatted(passwordPolicies.getMaxAuthAge()));
        }

        return StringUtils.join(rules, " and ");
    }


    @Override
    public boolean isInternalUser(@NotNull User user) {
        return StringUtils.equals(user.getUserName(), authServiceProperties.getUsername());
    }


    private @NotNull RoleRepresentation createRole(@NotNull Function<String, String> internalNameFunction,
                                                   @NotNull String roleName,
                                                   @Nullable Map<String, String> additionalAttributes) {

        String tempId = randomUUID().toString();

        // Creating desk first, to retrieve the Id
        // (We cannot force it on creation)

        RoleRepresentation roleRepresentation = new RoleRepresentation();
        roleRepresentation.setName(tempId);

        rolesResourceClient.create(roleRepresentation);

        // Changing name and creating the actual role.

        RoleRepresentation role = rolesResourceClient.get(tempId).toRepresentation();
        role.setName(internalNameFunction.apply(role.getId()));
        role.setDescription(roleName);

        Optional.ofNullable(additionalAttributes)
                .orElse(emptyMap())
                .entrySet()
                .stream()
                .filter(entry -> isNotEmpty(entry.getValue()))
                .forEach(entry -> role.singleAttribute(entry.getKey(), entry.getValue()));

        realmResource.rolesById().updateRole(role.getId(), role);
        return role;
    }


    private @NotNull PaginatedList<User> listUserFromRole(@NotNull String roleName, int page, int pageSize) {

        List<User> users = rolesResourceClient
                .get(roleName)
                .getUserMembers(page * pageSize, pageSize)
                .stream()
                .map(User::new)
                .toList();

        users.forEach(user -> user.setIsChecked(true));

        // Return result

        LongSupplier totalSupplier = () -> rolesResourceClient.get(roleName).getUserMembers().size();
        return new PaginatedList<>(users, page, pageSize, totalSupplier);
    }


    // <editor-fold desc="Tenants CRUDL">


    @Override
    public @NotNull String createTenant(@NotNull String tenantName) {

        // Two roles are created, for now : the root one and the admin one.
        // Retrieving the root tenant id and forcing it in the admin's name makes everything cleaner in the Keycloak admin console.

        RoleRepresentation tenantRole = createRole(
                id -> StringSubstitutor.replace(TENANT_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, id)),
                tenantName + " role",
                emptyMap()
        );

        RoleRepresentation functionalAdminRole = createRole(
                id -> StringSubstitutor.replace(TENANT_FUNCTIONAL_ADMIN_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, tenantRole.getId())),
                tenantName + " functional admin",
                emptyMap()
        );

        RoleRepresentation adminRole = createRole(
                id -> StringSubstitutor.replace(TENANT_ADMIN_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, tenantRole.getId())),
                tenantName + " admin",
                emptyMap()
        );

        log.trace("Admin composite role successfully created:{}", adminRole.getName());

        // Linking roles as composites.
        // The admin role should be the root in the composite-hierarchy. Admins have every rights.

        String internalAdminRoleName = StringSubstitutor.replace(TENANT_ADMIN_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, tenantRole.getId()));
        String internalFunctionalAdminRoleName = StringSubstitutor.replace(TENANT_FUNCTIONAL_ADMIN_INTERNAL_NAME,
                Map.of(TENANT_PLACEHOLDER, tenantRole.getId()));

        rolesResourceClient.get(internalAdminRoleName).addComposites(singletonList(functionalAdminRole));
        rolesResourceClient.get(internalFunctionalAdminRoleName).addComposites(singletonList(tenantRole));

        return tenantRole.getId();
    }


    @Override
    public void deleteTenant(@NotNull String tenantId) {

        log.debug("deleteTenant tenantId:{}", tenantId);

        String adminFunctionalTenantInternalName = StringSubstitutor.replace(TENANT_FUNCTIONAL_ADMIN_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, tenantId));
        String adminTenantInternalName = StringSubstitutor.replace(TENANT_ADMIN_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, tenantId));
        String tenantInternalName = StringSubstitutor.replace(TENANT_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, tenantId));

        log.debug("deleting {}", adminFunctionalTenantInternalName);
        if (getRole(adminFunctionalTenantInternalName) != null) {
            rolesResourceClient.deleteRole(adminFunctionalTenantInternalName);
        }

        log.debug("deleting {}", adminTenantInternalName);
        if (getRole(adminTenantInternalName) != null) {
            rolesResourceClient.deleteRole(adminTenantInternalName);
        }

        log.debug("deleting {}", tenantId);
        if (getRole(tenantInternalName) != null) {
            rolesResourceClient.deleteRole(tenantInternalName);
        }
    }


    // </editor-fold desc="Tenants CRUDL">


    // <editor-fold desc="User management">


    @Override
    public @NotNull String createUser(@NotNull UserDto request, @Nullable String currentTenantId) {

        if (INTERNAL_USERNAMES.contains(request.getUserName())) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.cannot_create_a_user_with_an_internal_reserved_username");
        }

        CredentialRepresentation credential = new CredentialRepresentation();
        credential.setType(PASSWORD);
        credential.setValue(request.getPassword());
        credential.setTemporary(false);

        UserRepresentation user = new UserRepresentation();
        if (request.isResetPasswordRequired()) {
            user.setRequiredActions(singletonList("UPDATE_PASSWORD"));
        }

        user.setUsername(request.getUserName());
        user.setFirstName(request.getFirstName());
        user.setLastName(request.getLastName());
        user.setEmail(request.getEmail());
        user.setCredentials(singletonList(credential));
        user.setEnabled(true);

        Map<String, List<String>> newAttributes = new HashMap<>();
        Optional.ofNullable(request.getComplementaryField()).ifPresent(s -> newAttributes.put(ATTRIBUTE_COMPLEMENTARY_FIELD, singletonList(s)));
        user.setAttributes(newAttributes);

        // Create

        try (Response result = usersResourceClient.create(user)) {

            switch (result.getStatus()) {
                case HTTP_CREATED -> log.debug("User successfully created in Keycloak");
                case HTTP_CONFLICT -> throw new LocalizedStatusException(
                        CONFLICT,
                        // If the current admin can't find the conflicting user,
                        // we'll set a more appropriate error message.
                        doesUserExistOnTenant(request.getUserName(), currentTenantId)
                        ? "message.duplicate_user_name"
                        : "message.duplicate_user_name_on_another_entity"
                );
                default -> {
                    log.error("Couldn't create user. errorCode:{} info:{}", result.getStatus(), result.getStatusInfo());

                    String errorBody = result.readEntity(String.class);
                    if (StringUtils.contains(errorBody, "Password policy not met")) {
                        throw new LocalizedStatusException(BAD_REQUEST, "message.password_policy_not_met");
                    }

                    HttpStatus status = HttpStatus.resolve(result.getStatus());
                    String reason = result.getStatusInfo().getReasonPhrase();
                    HttpStatus httpStatus = (status != null) ? status : INTERNAL_SERVER_ERROR;
                    throw new LocalizedStatusException(httpStatus, new Throwable(reason), "message.cannot_reach_auth_service");
                }
            }

            // Sending back result

            log.info("User successfully created userName:{}", request.getUserName());

            // We have to refresh the user representation to get the userId.
            // It is set on creation, but not returned on the create request.

            user = usersResourceClient
                    .search(user.getUsername(), true)
                    .stream().findFirst()
                    .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_auth_service"));

            return user.getId();
        }
    }


    @Override
    public void updateUser(@NotNull String userId,
                           @Nullable String firstName,
                           @Nullable String lastName,
                           @Nullable String email,
                           @Nullable String complementaryField) {

        UserResource user = usersResourceClient.get(userId);

        UserRepresentation userRepresentation = user.toRepresentation();
        Optional.ofNullable(firstName).filter(StringUtils::isNotEmpty).ifPresent(userRepresentation::setFirstName);
        Optional.ofNullable(lastName).filter(StringUtils::isNotEmpty).ifPresent(userRepresentation::setLastName);
        Optional.ofNullable(email).filter(StringUtils::isNotEmpty).ifPresent(userRepresentation::setEmail);

        Map<String, List<String>> newAttributes = Optional.ofNullable(userRepresentation.getAttributes()).orElse(new HashMap<>());

        Optional.ofNullable(complementaryField)
                .ifPresent(s -> newAttributes.put(ATTRIBUTE_COMPLEMENTARY_FIELD, singletonList(s)));

        userRepresentation.setAttributes(newAttributes);
        user.update(userRepresentation);
    }


    @Override
    public void updateUserPreferences(@Nullable String userId,
                                      @Nullable String notificationsRedirectionMail,
                                      @Nullable String isNotifiedOnConfidentialFolders,
                                      @Nullable String isNotifiedOnFollowedFolders,
                                      @Nullable String isNotifiedOnLateFolders,
                                      @Nullable String notificationsCronFrequency) {

        UserResource user = usersResourceClient.get(userId);

        UserRepresentation userRepresentation = user.toRepresentation();
        Map<String, List<String>> newAttributes = Optional.ofNullable(userRepresentation.getAttributes()).orElse(new HashMap<>());

        Optional.ofNullable(notificationsRedirectionMail)
                .ifPresent(s -> newAttributes.put(ATTRIBUTE_NOTIFICATIONS_REDIRECTION_MAIL, singletonList(s)));
        Optional.ofNullable(isNotifiedOnConfidentialFolders)
                .ifPresent(s -> newAttributes.put(ATTRIBUTE_IS_NOTIFIED_ON_CONFIDENTIAL_FOLDERS, singletonList(s)));
        Optional.ofNullable(isNotifiedOnFollowedFolders)
                .ifPresent(s -> newAttributes.put(ATTRIBUTE_IS_NOTIFIED_ON_FOLLOWED_FOLDERS, singletonList(s)));
        Optional.ofNullable(isNotifiedOnLateFolders)
                .ifPresent(s -> newAttributes.put(ATTRIBUTE_IS_NOTIFIED_ON_LATE_FOLDERS, singletonList(s)));
        Optional.ofNullable(notificationsCronFrequency)
                .ifPresent(s -> newAttributes.put(ATTRIBUTE_NOTIFICATIONS_CRON_FREQUENCY, singletonList(s)));

        log.debug("updateUserPreferences - newAttributes: {}", newAttributes);
        userRepresentation.setAttributes(newAttributes);
        user.update(userRepresentation);
    }


    @Override
    public void updateUserInternalMetadata(@NotNull String userId,
                                           @Nullable Integer contentGroupIndex,
                                           @Nullable String contentNodeId,
                                           @Nullable String signatureImageContentId) {

        UserResource user = usersResourceClient.get(userId);
        UserRepresentation userRepresentation = user.toRepresentation();

        Map<String, List<String>> newAttributes = Optional.ofNullable(userRepresentation.getAttributes()).orElse(new HashMap<>());

        Optional.ofNullable(contentNodeId)
                .ifPresent(s -> newAttributes.put(ATTRIBUTE_CONTENT_NODE_ID, singletonList(s)));

        Optional.ofNullable(signatureImageContentId)
                .ifPresent(s -> {
                    // If an non-null empty value was given, let's delete the attribute...
                    String newValue = Optional.of(signatureImageContentId).filter(StringUtils::isNotEmpty).orElse(null);
                    newAttributes.put(ATTRIBUTE_SIGNATURE_IMAGE_CONTENT_ID, singletonList(newValue));
                });

        Optional.ofNullable(contentGroupIndex)
                .map(String::valueOf)
                .ifPresent(s -> newAttributes.put(ATTRIBUTE_CONTENT_GROUP_INDEX, singletonList(s)));

        userRepresentation.setAttributes(newAttributes);
        user.update(userRepresentation);
    }


    @Override
    public void updateUserGlobalPrivileges(@NotNull User user, @NotNull UserDto modifiedUser, String specificTenantId) {

        log.info("updateUserGlobalPrivileges");
        RolesResource roles = rolesResourceClient;
        RoleScopeResource roleScopeResource = usersResourceClient.get(user.getId()).roles().realmLevel();
        List<RoleRepresentation> existingRolesRepresentation = roleScopeResource.listAll();
        List<String> existingRoleNames = existingRolesRepresentation.stream().map(RoleRepresentation::getName).toList();
        List<String> existingAdminTenantIds = filterMatchingTenantIds(TENANT_ADMIN, existingRoleNames).stream().toList();
        List<String> existingFunctionalAdminTenantIds = filterMatchingTenantIds(FUNCTIONAL_ADMIN, existingRoleNames).stream().toList();

        // Admin case

        RoleRepresentation superAdminRole = roles.get(SUPER_ADMIN_ROLE_NAME).toRepresentation();

        boolean shouldBeAdmin = modifiedUser.getPrivilege() == SUPER_ADMIN;
        boolean isAdmin = existingRolesRepresentation.stream().anyMatch(t -> StringUtils.equals(t.getId(), superAdminRole.getId()));

        if (shouldBeAdmin && !isAdmin) {
            roleScopeResource.add(singletonList(superAdminRole));
        } else if (isAdmin && !shouldBeAdmin) {
            roleScopeResource.remove(singletonList(superAdminRole));
        }

        boolean onSpecificTenant = !StringUtils.isEmpty(specificTenantId);

        // Tenant admin case

        boolean shouldBeTenantAdmin = modifiedUser.getPrivilege() == TENANT_ADMIN;
        boolean shouldBeAdminOfSomeTenant = shouldBeTenantAdmin || modifiedUser.getAdministeredTenantIds().size() > 0 && onSpecificTenant;

        log.debug("updateUserGlobalPrivileges - shouldBeAdmin : {}, shouldBeTenantAdmin : {}, specificTenantId : {}",
                shouldBeAdmin, shouldBeTenantAdmin, specificTenantId);

        Set<String> tenantToAddAdminRoleNames = modifiedUser.getAdministeredTenantIds()
                .stream()
                .map(i -> StringSubstitutor.replace(TENANT_ADMIN_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, i)))
                .collect(toSet());

        List<RoleRepresentation> administeredTenantsToRemove = existingAdminTenantIds.stream()
                .map(i -> StringSubstitutor.replace(TENANT_ADMIN_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, i)))
                .filter(roleName -> !shouldBeAdminOfSomeTenant || !tenantToAddAdminRoleNames.contains(roleName))
                .map(roles::get)
                .map(RoleResource::toRepresentation)
                .toList();

        if (shouldBeTenantAdmin) {

            List<RoleRepresentation> administeredTenantsToAdd = tenantToAddAdminRoleNames.stream()
                    .filter(n -> existingRolesRepresentation.stream().noneMatch(t -> StringUtils.equals(t.getName(), n)))
                    .map(roles::get)
                    .map(RoleResource::toRepresentation)
                    .toList();

            roleScopeResource.add(administeredTenantsToAdd);
        }

        if (CollectionUtils.isNotEmpty(administeredTenantsToRemove)) {
            roleScopeResource.remove(administeredTenantsToRemove);
        }

        // Functional admin case

        boolean shouldBeFunctionalAdmin = modifiedUser.getPrivilege() == FUNCTIONAL_ADMIN;
        if (shouldBeFunctionalAdmin && !onSpecificTenant) {
            throw new ResponseStatusException(NOT_ACCEPTABLE, "Cannot set functional admin role out of a specific tenant");
        }

        if (shouldBeFunctionalAdmin) {
            String targetRoleName = StringSubstitutor.replace(TENANT_FUNCTIONAL_ADMIN_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, specificTenantId));
            roleScopeResource.add(singletonList(roles.get(targetRoleName).toRepresentation()));
        } else {
            if (onSpecificTenant) {
                String targetRoleName = StringSubstitutor.replace(TENANT_FUNCTIONAL_ADMIN_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, specificTenantId));
                roleScopeResource.remove(singletonList(roles.get(targetRoleName).toRepresentation()));
            } else {
                //FIXME I don't yet how we can manage this case... Maybe it should just be forbidden?
                //else we remove all functional admin def... that's ugly, and does not take into account all desk-specific permissions
                log.warn("could not properly remove functional admin role from user");
            }
        }
    }


    @Override
    public void addDefaultUserOnTenant(Tenant tenant) {
        User defaultUser = Optional.ofNullable(findUserById(usersResourceClient.search(initialAdminUsername, true).get(0).getId()))
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message."));

        addUserToTenant(defaultUser, tenant.getId(), SUPER_ADMIN);
    }


    @Override
    public void updateUserPrivileges(@NotNull String tenantId, @NotNull User user, @NotNull UserPrivilege newPrivilege) {

        log.debug("updateUserPrivileges tenantId:{} privilege:{}", tenantId, newPrivilege);
        RoleScopeResource roleScopeResource = usersResourceClient.get(user.getId()).roles().realmLevel();

        // Cleanup before

        String tenantAdminName = StringSubstitutor.replace(TENANT_ADMIN_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, tenantId));
        String tenantFunctionalAdminName = StringSubstitutor.replace(TENANT_FUNCTIONAL_ADMIN_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, tenantId));
        String tenantName = StringSubstitutor.replace(TENANT_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, tenantId));

        RoleRepresentation tenantAdminRole = rolesResourceClient.get(tenantAdminName).toRepresentation();
        RoleRepresentation tenantFunctionalAdminRole = rolesResourceClient.get(tenantFunctionalAdminName).toRepresentation();
        RoleRepresentation tenantRole = rolesResourceClient.get(tenantName).toRepresentation();

        // Special case :
        //
        // Keycloak forbids role removal if it was set with the `hardcoded-ldap-role-mapper`.
        // That's what we use on a LDAP sync, to set the tenantRole, so we have to prevent it.
        //
        // The LDAP user is not really bound with the tenantRole : it is bound to a federation, itself is bound to the tenantRole.
        // That's why we can add any adminTenantRole to a LDAP user without removing it from the default one.
        roleScopeResource.remove(user.getIsLdapSynchronized()
                                 ? asList(tenantAdminRole, tenantFunctionalAdminRole)
                                 : asList(tenantAdminRole, tenantFunctionalAdminRole, tenantRole));

        // Actual add

        switch (newPrivilege) {
            case TENANT_ADMIN -> {
                roleScopeResource.add(singletonList(tenantAdminRole));
                roleScopeResource.add(singletonList(tenantRole));
            }
            case FUNCTIONAL_ADMIN -> {
                roleScopeResource.add(singletonList(tenantFunctionalAdminRole));
                roleScopeResource.add(singletonList(tenantRole));
            }
            default -> roleScopeResource.add(singletonList(tenantRole));
        }
    }


    @Override
    public void resetUserPassword(@NotNull String userId, @NotNull String password) {
        log.debug("Update password user:{}", userId);

        CredentialRepresentation cr = new CredentialRepresentation();
        cr.setType(PASSWORD);
        cr.setValue(password);

        try {
            usersResourceClient.get(userId).resetPassword(cr);
        } catch (BadRequestException e) {
            // Nothing is send back by the Keycloak engine here.
            // We can only assume that the password policy was not met on this specific exception.
            throw new LocalizedStatusException(BAD_REQUEST, "message.password_policy_not_met");
        }
    }


    @Override
    public int countLoggedInUsers() {
        return realmResource.getClientSessionStats()
                .stream()
                .filter(clientSessionStat -> StringUtils.equals(keycloakWebClientId, clientSessionStat.get("clientId")))
                .findFirst()
                .map(ipcoreWebStat -> {
                    String activeSessions = ipcoreWebStat.get("active");
                    return activeSessions != null ? Integer.parseInt(activeSessions) : 0;
                })
                .orElse(-1);
    }


    @Override
    public @NotNull Set<String> listTenantsForUser(String userId) {
        List<RoleRepresentation> userRoleList = usersResourceClient.get(userId).roles().realmLevel().listAll();

        return userRoleList.stream()
                .filter(roleRep -> TENANT_ID_NAME_PATTERN.matcher(roleRep.getName()).matches())
                .map(RoleRepresentation::getId)
                .collect(toSet());
    }


    @Override
    public @NotNull Page<User> listUsers(@NotNull Pageable pageable, @Nullable String searchTerm) {

        long page = RequestUtils.getPageNumber(pageable);
        long pageSize = RequestUtils.getPageSize(pageable);
        UserSortBy sortBy = RequestUtils.getFirstOderSort(pageable, UserSortBy.class, USERNAME);
        boolean asc = RequestUtils.isFirstOrderAsc(pageable);

        log.debug("listUsers sortBy:{} searchTerm:{} page:{} pageSize:{}", sortBy, searchTerm, page, pageSize);

        try (Connection connection = dataSource.getConnection()) {
            DSLContext dsl = DSL.using(connection, PSQL_RENDER_SETTINGS);

            List<User> users = dsl
                    .fetch(buildListUsersRequest(sortBy, asc, searchTerm, page, pageSize))
                    .stream()
                    .map(User::new)
                    .toList();

            // Sending back result

            LongSupplier totalSupplier = () -> dsl
                    .fetchOptional(buildListUsersTotalRequest(searchTerm))
                    .map(Record1::value1)
                    .orElse(-1);

            return new PageImpl<>(users, pageable, computeTotal(users, page, pageSize, totalSupplier));

        } catch (SQLException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_database", e);
        }
    }


    @Override
    public @NotNull Page<User> listTenantUsers(@NotNull String tenantId,
                                               @NotNull Pageable pageable,
                                               @Nullable String searchTerm) {

        UserSortBy sortBy = pageable.getSort().stream().findFirst()
                .map(Sort.Order::getProperty)
                .map(sortByName -> EnumUtils.getEnum(UserSortBy.class, sortByName))
                .orElseThrow(() -> new ResponseStatusException(BAD_REQUEST, "Invalid sort parameter"));

        boolean asc = pageable.getSort().stream().findFirst()
                .map(Sort.Order::getDirection)
                .map(Sort.Direction::isAscending)
                .orElse(true);

        long page = (pageable != Pageable.unpaged()) ? pageable.getPageNumber() : 0L;
        long pageSize = (pageable != Pageable.unpaged()) ? pageable.getPageSize() : MAX_USERS_COUNT;

        log.debug("listUsers tenantId:{} sortBy:{} searchTerm:{} page:{} pageSize:{}", tenantId, sortBy, searchTerm, page, pageSize);

        try (Connection connection = dataSource.getConnection()) {
            DSLContext dsl = DSL.using(connection, PSQL_RENDER_SETTINGS);

            List<User> users = dsl
                    .fetch(buildSearchUserWithinGroupRequest(tenantId, sortBy, asc, searchTerm, page, pageSize))
                    .stream()
                    .map(User::new)
                    .toList();

            // Sending back result

            LongSupplier totalSupplier = () -> dsl
                    .fetchOptional(buildSearchUserWithinGroupTotalRequest(tenantId, searchTerm))
                    .map(Record1::value1)
                    .orElse(-1);

            return new PageImpl<>(users, pageable, computeTotal(users, page, pageSize, totalSupplier));

        } catch (SQLException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_database", e);
        }
    }


    @Override
    public @NotNull PaginatedList<User> listTenantAdminUsers(@NotNull String tenantId, int page, int pageSize) {
        String tenantAdminName = StringSubstitutor.replace(TENANT_ADMIN_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, tenantId));
        log.debug("listAdminUsersFromTenant tenantName:{} page:{} pageSize:{}", tenantAdminName, page, pageSize);
        return listUserFromRole(tenantAdminName, page, pageSize);
    }


    @Override
    public @NotNull PaginatedList<User> listUsersFromDesk(String tenantId, @NotNull String deskId, int page, int pageSize) {
//        String roleName = realmResource.rolesById().getRole(id).getName();
        String roleName = StringSubstitutor.replace(DESK_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, tenantId, DESK_PLACEHOLDER, deskId));
        log.debug("Get users from desk roleName:{} page:{} pageSize:{}", roleName, page, pageSize);
        return listUserFromRole(roleName, page, pageSize);
    }


    @Override
    public void refreshUsersDeskStatus(@NotNull List<User> userList, @NotNull String deskId) {

        String roleName = realmResource.rolesById().getRole(deskId).getName();
        log.debug("refreshUsersDeskStatus get users from desk roleName:{}", roleName);

        // Unfortunately, we retrieve every user associated with the given desk here.
        // TODO : We can make it in one request, targeting only given users.
        Set<String> selectedUsersIds = rolesResourceClient
                .get(roleName).getRoleUserMembers().stream()
                .map(User::new).toList().stream()
                .map(User::getId)
                .collect(toSet());

        userList.forEach(user -> user.setIsChecked(selectedUsersIds.contains(user.getId())));
    }


    @Override
    public void addUserToTenant(@NotNull User user, @NotNull String tenantId, @NotNull UserPrivilege privilege) {
        log.debug("user:{} tenantId:{}", user.getUserName(), tenantId);
        updateUserPrivileges(tenantId, user, privilege);
    }


    @Override
    public void removeUserFromTenant(@NotNull String userId, @NotNull String tenantId) {

        RoleScopeResource userRoleScopeResource = usersResourceClient.get(userId).roles().realmLevel();
        RolesResource allRoles = rolesResourceClient;

        String tenantName = StringSubstitutor.replace(TENANT_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, tenantId));
        List<RoleRepresentation> allTenantRelatedRoles = allRoles.list(tenantName, true);

        userRoleScopeResource.remove(allTenantRelatedRoles);
    }


    @Override
    public void deleteUser(@NotNull String userId) {
        usersResourceClient.get(userId).remove();
    }


    @Override
    public @Nullable User findUserById(@NotNull String id) {
        return findTenantUserById(null, id);
    }


    @Override
    public @Nullable User findTenantUserById(@Nullable String tenantId, @NotNull String id) {

        // Integrity check

        // This is for BPMN patch-replacements.
        // Since we have no idea if the users entered a deskId or a deskName,
        // This simple check eases Keycloak for irrelevant calls.
        if (!id.matches(ID_REGEX)) {
            return null;
        }

        // Actual search

        UserResource userResource = usersResourceClient
                .get(id);

        // Computing internal fields level

        User result = new User(userResource.toRepresentation());
        List<RoleRepresentation> userRoles = userResource.roles().realmLevel().listAll();
        result.setRolesCount(userRoles.size());
        result.setPrivilege(getUserPrivilege(tenantId, userRoles));

        // Sending back result

        return result;
    }


    @Override
    public @NotNull List<User> getUsersByIds(@NotNull Set<String> userIds) {
        try (Connection connection = dataSource.getConnection()) {
            return DSL.using(connection, PSQL_RENDER_SETTINGS)
                    .fetch(buildListUsersByIds(userIds))
                    .stream()
                    .map(User::new)
                    .toList();
        } catch (SQLException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_database", e);
        }
    }


    @Override
    public @NotNull Set<String> filterUsersWithPrivilege(@NotNull Set<String> userIds, @NotNull UserPrivilege privilege, @Nullable String targetTenantId) {

        try (Connection connection = dataSource.getConnection()) {
            return DSL.using(connection, PSQL_RENDER_SETTINGS)
                    .fetch(buildFilterUsersWithPrivilege(userIds, privilege, targetTenantId))
                    .stream()
                    .map(r -> r.get(User.FIELD_ID, String.class))
                    .collect(toSet());
        } catch (SQLException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_database", e);
        }
    }


    @Override
    public @NotNull List<User> getSuperAdminsList(int maxResults) {
        return rolesResourceClient
                .get(SUPER_ADMIN_ROLE_NAME)
                .getRoleUserMembers(0, maxResults)
                .stream()
                .map(User::new)
                .toList();
    }


    // </editor-fold desc="User management">


    /**
     * Returns the higher role available for the given tenant.
     * If no tenant Id is given, will return the higher role in any of the user's tenant.
     *
     * @param tenantId  the tenantId to narrow
     * @param userRoles the Keycloak internal roles
     * @return
     */
    UserPrivilege getUserPrivilege(@Nullable String tenantId, @NotNull List<RoleRepresentation> userRoles) {

        Set<String> rolesSet = userRoles.stream().map(RoleRepresentation::getName).collect(toSet());

        // Global (easy) case

        if (rolesSet.contains(SUPER_ADMIN_ROLE_NAME)) {
            return SUPER_ADMIN;
        }

        // Tenant admin

        Pattern tenantAdminPattern = Optional.ofNullable(tenantId)
                .filter(StringUtils::isNotEmpty)
                .map(i -> StringSubstitutor.replace(TENANT_ADMIN_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, i)))
                .map(Pattern::compile)
                .orElse(TENANT_ID_ADMIN_NAME_PATTERN);

        if (rolesSet.stream()
                .map(tenantAdminPattern::matcher)
                .anyMatch(Matcher::matches)) {
            return TENANT_ADMIN;
        }

        // Functional admin

        Pattern tenantFunctionalAdminPattern = Optional.ofNullable(tenantId)
                .filter(StringUtils::isNotEmpty)
                .map(i -> StringSubstitutor.replace(TENANT_FUNCTIONAL_ADMIN_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, i)))
                .map(Pattern::compile)
                .orElse(TENANT_ID_FUNCTIONAL_ADMIN_NAME_PATTERN);

        if (rolesSet.stream()
                .map(tenantFunctionalAdminPattern::matcher)
                .anyMatch(Matcher::matches)) {
            return FUNCTIONAL_ADMIN;
        }

        // Default case

        return NONE;
    }


    // <editor-fold desc="Desk management">


    @Override
    public @NotNull String createDesk(@NotNull String tenantId, @NotNull String name, @NotNull String shortName, @Nullable String description,
                                      @Nullable String parentDeskId) {

        RoleRepresentation roleRepresentation = createRole(
                id -> StringSubstitutor.replace(DESK_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, tenantId, DESK_PLACEHOLDER, id)),
                name,
                Map.of(
                        ATTRIBUTE_SHORT_NAME, shortName,
                        ATTRIBUTE_DESCRIPTION, Optional.ofNullable(description).orElse(EMPTY),
                        ATTRIBUTE_PARENT_DESK_ID, Optional.ofNullable(parentDeskId).orElse(EMPTY)
                )
        );

        String deskId = roleRepresentation.getId();
        String userAccountClientId = clientResource.getServiceAccountUser().getId();
        addUsersToDesk(deskId, singletonList(userAccountClientId));

        return deskId;
    }


    @Override
    public void editDesk(@NotNull String tenantId, @NotNull Desk previousDeskVersion, @NotNull String name, @NotNull String shortName,
                         @Nullable String description, @Nullable String directParentId) {

        log.info("editDesk id:{} name:{} shortName:{} description:{} directParentId:{}",
                previousDeskVersion.getId(), name, shortName, description, directParentId);

        RoleRepresentation role = realmResource.rolesById().getRole(previousDeskVersion.getId());
        role.setDescription(name);
        role.singleAttribute(ATTRIBUTE_SHORT_NAME, shortName);
        role.singleAttribute(ATTRIBUTE_DESCRIPTION, description);

        String previousParentDeskId = Optional.ofNullable(previousDeskVersion.getParentDesk()).map(DeskRepresentation::getId).orElse(null);
        if (!StringUtils.equals(previousParentDeskId, directParentId)) {

            // Checking loops in desk hierarchy
            if (StringUtils.isNotEmpty(directParentId)) {
                Desk parentDesk;
                try (Connection connection = dataSource.getConnection()) {
                    parentDesk = DSL.using(connection, PSQL_RENDER_SETTINGS)
                            .fetch(buildListDesksSqlRequest(tenantId, 0, MAX_BIG_PAGE_SIZE, false, emptyList(), null, directParentId, true))
                            .stream()
                            .map(Desk::new)
                            .findFirst()
                            .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_parent_desk_id"));
                } catch (SQLException e) {
                    throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_database", e);
                }

                if (parentDesk.getParentIdsChain().contains(role.getId())) {
                    log.error("Loop prevented in desk hierarchy");
                    throw new LocalizedStatusException(CONFLICT, "message.a_loop_has_been_detected");
                }
            }

            // Updating metadata
            role.singleAttribute(ATTRIBUTE_PARENT_DESK_ID, RegExUtils.replaceFirst(directParentId, DESK_ROLE_PREFIX, EMPTY));
        }

        realmResource.rolesById().updateRole(previousDeskVersion.getId(), role);
    }


    @Override
    public void deleteDesk(@NotNull String deskId) {
        realmResource.rolesById().deleteRole(deskId);
    }


    @Override
    public @NotNull Page<Desk> listDesks(@NotNull String tenantId,
                                         @NotNull Pageable pageable,
                                         @NotNull List<String> reverseIdList,
                                         boolean collapseAll) {
        return this.searchDesks(tenantId, pageable, reverseIdList, null, collapseAll);
    }


    @Override
    public @NotNull Page<Desk> searchDesks(@NotNull String tenantId,
                                           @NotNull Pageable pageable,
                                           @NotNull List<String> reverseIdList,
                                           @Nullable String searchTerm,
                                           boolean collapseAll) {

        long page = (pageable != Pageable.unpaged()) ? pageable.getPageNumber() : 0L;
        long pageSize = (pageable != Pageable.unpaged()) ? pageable.getPageSize() : MAX_BIG_PAGE_SIZE;
        boolean asc = pageable.getSort().stream().findFirst().map(order -> order.getDirection() == ASC).orElse(true);

        try (Connection connection = dataSource.getConnection()) {
            DSLContext dsl = DSL.using(connection, PSQL_RENDER_SETTINGS);

            List<Desk> desks = dsl
                    .fetch(buildListDesksSqlRequest(tenantId, page, pageSize, collapseAll, reverseIdList, searchTerm, null, asc))
                    .stream()
                    .map(Desk::new)
                    .toList();

            log.debug("Search desks size:{}", desks.size());

            // Sending back result
            LongSupplier totalSupplier = () -> dsl
                    .fetchOptional(buildTotalDesksSqlRequest(tenantId, false, emptyList(), searchTerm))
                    .map(Record1::value1)
                    .orElse(-1);

            return new PageImpl<>(desks, pageable, computeTotal(desks, page, pageSize, totalSupplier));

        } catch (SQLException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_database", e);
        }
    }


    @Override
    public @NotNull Page<DeskRepresentation> findDeskByShortName(@NotNull String tenantId, @NotNull String shortName) {
        try (Connection connection = dataSource.getConnection()) {

            List<DeskRepresentation> result = DSL.using(connection, PSQL_RENDER_SETTINGS)
                    .fetch(buildListDesksByShortNameSqlRequest(tenantId, shortName))
                    .stream()
                    .map(r -> new DeskRepresentation(r.get(SQL_ID, String.class), r.get(SQL_NAME, String.class)))
                    .toList();

            return new PageImpl<>(result);

        } catch (SQLException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_database", e);
        }

    }


    @Override
    public @Nullable Desk findDeskById(@Nullable String tenantId, @NotNull String id) {

        // Integrity check

        // This is for BPMN patch-replacements.
        // Since we have no idea if the users entered a deskId or a deskName,
        // This simple check eases Keycloak for irrelevant calls.
        if (!id.matches(DESK_ID_REGEX)) {
            return null;
        }

        // Actual search
        RoleByIdResource roleListById = realmResource
                .rolesById();

        RoleRepresentation role = roleListById.getRole(id);

        List<Desk> delegatingDesks = roleListById
                .getRoleComposites(id)
                .stream()
                .map(Desk::new)
                .toList();

        // Building result

        Desk resultDesk = new Desk(role);
        DeskRepresentation directParent = role.getAttributes()
                .getOrDefault(ATTRIBUTE_PARENT_DESK_ID, emptyList())
                .stream()
                .filter(StringUtils::isNotEmpty)
                .findFirst()
                .map(i -> new DeskRepresentation(i, null))
                .orElse(null);

        updateDeskNames(singletonList(directParent));

        resultDesk.setParentDesk(directParent);
        resultDesk.setTenantId(getTenantIdForRoleName(id, role.getName()));
        resultDesk.setDelegatingDesks(delegatingDesks);

        return resultDesk;
    }


    @Override
    public @Nullable Desk findDeskByIdNoException(@Nullable String tenantId, @NotNull String id) {

        // Integrity check

        // This is for BPMN patch-replacements.
        // Since we have no idea if the users entered a deskId or a deskName,
        // This simple check eases Keycloak for irrelevant calls.
        if (!id.matches(DESK_ID_REGEX)) {
            return null;
        }

        // Actual search
        RoleRepresentation role;

        try {
            role = realmResource
                    .rolesById()
                    .getRole(id);
        } catch (Exception e) {
            return null;
        }

        List<Desk> delegatingDesks = realmResource
                .rolesById()
                .getRoleComposites(id)
                .stream()
                .map(Desk::new)
                .toList();

        // Building result

        Desk resultDesk = new Desk(role);
        DeskRepresentation directParent = role.getAttributes()
                .getOrDefault(ATTRIBUTE_PARENT_DESK_ID, emptyList())
                .stream()
                .filter(StringUtils::isNotEmpty)
                .findFirst()
                .map(i -> new DeskRepresentation(i, null))
                .orElse(null);

        updateDeskNames(singletonList(directParent));

        resultDesk.setParentDesk(directParent);
        resultDesk.setTenantId(getTenantIdForRoleName(id, role.getName()));
        resultDesk.setDelegatingDesks(delegatingDesks);

        return resultDesk;
    }

    // </editor-fold desc="Desk management">


    String getTenantIdForRoleName(@NotNull String id, @NotNull String roleName) {
        return Pattern
                .compile(StringSubstitutor.replace(DESK_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, "(.*?)", DESK_PLACEHOLDER, id)))
                .matcher(roleName)
                .results()
                .map(m -> m.group(1))
                .findFirst()
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.error_getting_the_desk_s_parent_tenant"));
    }


    @Override
    public void addUsersToDesk(@NotNull String deskId, @NotNull Collection<String> userIds) {
        log.debug("Adding userList size:{} to desk id:{}", userIds.size(), deskId);
        RoleRepresentation role = realmResource.rolesById().getRole(deskId);
        userIds.forEach(userId -> usersResourceClient.get(userId).roles().realmLevel().add(singletonList(role)));
    }


    @Override
    public @NotNull Page<DeskRepresentation> getDesksFromUser(@NotNull String userId, Pageable pageable, @Nullable String searchTerm) {

        log.debug("getDesksFromUser userId:{}", userId);
        HashMap<String, Desk> result = new HashMap<>();

        long page = RequestUtils.getPageNumber(pageable);
        long pageSize = RequestUtils.getPageSize(pageable);

        try (Connection connection = dataSource.getConnection()) {
            DSL.using(connection, PSQL_RENDER_SETTINGS)
                    .fetch(buildListDeskAndDelegates(userId, page, pageSize, searchTerm))
                    .stream()
                    .peek(r -> {
                        String id = r.get(SQL_ROLE_ID, String.class);
                        if (!result.containsKey(id)) {
                            String name = r.get(SQL_ROLE_DESCRIPTION, String.class);
                            String roleFullName = r.get(SQL_ROLE_FULL_NAME, String.class);
                            String tenantId = getTenantIdForRoleName(id, roleFullName);

                            List<DelegationRule> delegationRules = parseDelegatingRulesAttribute(r);
                            result.put(id, Desk.builder().id(id).name(name).tenantId(tenantId).delegationRules(delegationRules).build());
                        }
                    })
                    .forEach(r -> {
                        String parentId = r.get(SQL_ROLE_ID, String.class);
                        String id = r.get(SQL_ROLE_DELEGATING_ID, String.class);
                        if (isNotEmpty(id)) {
                            result.get(parentId).getDelegationRules().add(new DelegationRule(id, null, null, null));
                        }
                    });
        } catch (SQLException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_database", e);
        }

        List<DeskRepresentation> sortedDesks = result
                .values()
                .stream()
                .sorted(Comparator.comparing(Desk::getTenantId))
                .map(desk -> modelMapper.map(desk, DeskRepresentation.class))
                .collect(toList());

        // Sending back result

        log.info("getDesksFromUser resultSize:{}", result.size());

        // TODO : A request that just counts, and not retrieve everything
        Pattern deskRolePattern = Pattern.compile(StringSubstitutor.replace(DESK_INTERNAL_NAME,
                Map.of(TENANT_PLACEHOLDER, "(.*?)", DESK_PLACEHOLDER, "(.*?)")));

        LongSupplier totalSupplier = () -> usersResourceClient.get(userId).roles().getAll()
                .getRealmMappings().stream()
                .filter(role -> deskRolePattern.matcher(role.getName()).matches())
                .count();

        return new PageImpl<>(sortedDesks, pageable, computeTotal(sortedDesks, page, pageSize, totalSupplier));
    }


    @Override
    public void removeUsersFromDesk(@NotNull String deskId, @NotNull Collection<String> userIds) {
        log.debug("Removing userList size:{} to desk id:{}", userIds.size(), deskId);
        RoleRepresentation role = realmResource.rolesById().getRole(deskId);
        userIds.forEach(userId -> usersResourceClient.get(userId).roles().realmLevel().remove(singletonList(role)));
    }


    @Override
    public @NotNull Map<String, String> getDeskNames(@NotNull Set<String> deskIds) {

        if (CollectionUtils.isEmpty(deskIds)) {
            return emptyMap();
        }

        try (Connection connection = dataSource.getConnection()) {
            return DSL.using(connection, PSQL_RENDER_SETTINGS)
                    .fetch(buildGetDeskNames(deskIds))
                    .stream()
                    .collect(toMap(
                            r -> r.get(SQL_ROLE_ID, String.class),
                            r -> r.get(SQL_ROLE_DESCRIPTION, String.class)
                    ));
        } catch (SQLException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_database", e);
        }
    }


    @Override
    public void populateDeskNamesAndTenantIds(@NotNull Map<String, Desk> deskMap) {
        Pattern tenantIdRegex = Pattern.compile("^tenant_(\\w{8}-\\w{4}-\\w{4}-\\w{4}-\\w{12})_desk_.*$");
        Set<String> deskIds = deskMap.keySet();

        try (Connection connection = dataSource.getConnection()) {
            DSL.using(connection, PSQL_RENDER_SETTINGS)
                    .fetch(buildGetDeskNamesAndRoles(deskIds))
                    .forEach(r -> {
                        Desk d = deskMap.get(r.get(SQL_ROLE_ID, String.class));
                        d.setName(r.get(SQL_ROLE_DESCRIPTION, String.class));
                        Matcher m = tenantIdRegex.matcher(r.get(SQL_ROLE_FULL_NAME, String.class));
                        if (m.matches()) {
                            d.setTenantId(m.group(1));
                        } else {
                            log.error("Desk '{}' has an Id incorrectly built : '{}', could not retrieve tenantId", d.getName(), d.getId());
                        }
                    });
        } catch (SQLException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_database", e);
        }
    }


    @Override
    public @NotNull PasswordPolicies getPasswordPolicies() {
        return passwordPolicies;
    }


    public static String getAttribute(@Nullable Map<String, List<String>> map, String key) {

        if ((map == null) || map.isEmpty()) {
            return null;
        }

        List<String> valueList = map.get(key);
        if (CollectionUtils.isEmpty(valueList)) {
            return null;
        }

        return valueList.get(0);
    }


    @Override
    public String getServiceAccountUserId() {
        return clientResource.getServiceAccountUser().getId();
    }


    private static @NotNull List<DelegationRule> parseDelegatingRulesAttribute(@NotNull Record6<String, String, String, String, String, String> record) {
        return Optional.ofNullable(record.get(SQL_ROLE_PARENT_METADATA_DELEGATION, String.class))
                .filter(s -> !StringUtils.isEmpty(s))
                .map(s -> {
                    try {
                        return new ObjectMapper().readValue(s, new TypeReference<List<DelegationRule>>() {});
                    } catch (JsonProcessingException e) {
                        log.warn("Cannot parse delegations", e);
                        return new ArrayList<DelegationRule>();
                    }
                })
                .orElse(new ArrayList<>());
    }


    @NotNull
    Select<Record7<String, String, String, String, String, String, String>>
    buildListUsersRequest(@Nullable UserSortBy sortBy, boolean asc, @Nullable String searchTerm, long page, long pageSize) {

        String userEntityAlias = "ue";
        String userSsoTableAlias = "sso_table";
        Condition searchCondition = Optional.ofNullable(searchTerm)
                .filter(StringUtils::isNotEmpty)
                .map(s -> String.format("%%%s%%", s))
                .map(DSL::upper)
                .map(s -> or(
                        upper(field(userEntityAlias + "." + SQL_USER_USERNAME, String.class)).like(s),
                        upper(field(userEntityAlias + "." + SQL_USER_FIRST_NAME, String.class)).like(s),
                        upper(field(userEntityAlias + "." + SQL_USER_LAST_NAME, String.class)).like(s),
                        upper(field(userEntityAlias + "." + SQL_USER_EMAIL, String.class)).like(s)
                ))
                .orElse(val(1).eq(1));

        Field<String> sortByField = switch (Optional.ofNullable(sortBy).orElse(USERNAME)) {
            case ID -> field(SQL_USER_ID, String.class);
            case FIRST_NAME -> field(SQL_USER_FIRST_NAME, String.class);
            case LAST_NAME -> field(SQL_USER_LAST_NAME, String.class);
            case EMAIL -> field(SQL_USER_EMAIL, String.class);
            default -> field(SQL_USER_USERNAME, String.class);
        };

        return DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(
                        field(userEntityAlias + "." + SQL_USER_ID, String.class).as(User.FIELD_ID),
                        field(userEntityAlias + "." + SQL_USER_USERNAME, String.class).as(User.FIELD_USERNAME),
                        field(userEntityAlias + "." + SQL_USER_FIRST_NAME, String.class).as(User.FIELD_FIRST_NAME),
                        field(userEntityAlias + "." + SQL_USER_LAST_NAME, String.class).as(User.FIELD_LAST_NAME),
                        field(userEntityAlias + "." + SQL_USER_EMAIL, String.class).as(User.FIELD_EMAIL),
                        field(userEntityAlias + "." + SQL_FEDERATION_LINK, String.class).as(User.FIELD_FEDERATION),
                        field(userSsoTableAlias + "." + SQL_SSO_IDENTITY_PROVIDER_FIELD, String.class).as(FIELD_IDENTITY_PROVIDER)
                )
                .from(table(SQL_USER_TABLE).as(userEntityAlias))
                .leftOuterJoin(table(SQL_USER_IDENTITY_PROVIDER_TABLE).as(userSsoTableAlias))
                .on(field(userEntityAlias + "." + SQL_USER_ID, String.class)
                        .equal(field(userSsoTableAlias + "." + SQL_IDP_TABLE_USER_ID, String.class)))
                .where(and(
                        searchCondition,
                        field(userEntityAlias + "." + SQL_USER_USERNAME, String.class).notIn(INTERNAL_USERNAMES),
                        field(userEntityAlias + "." + SQL_ROLE_REALM_ID, String.class).equal(IPARAPHEUR_REALM)

                ))
                .orderBy(asc ? sortByField.asc() : sortByField.desc())
                .limit(pageSize)
                .offset(page * pageSize);
    }


    @NotNull
    Select<Record8<String, String, String, String, String, String, String, Integer>>
    buildSearchUserWithinGroupRequest(@NotNull String tenantId,
                                      @Nullable UserSortBy sortBy,
                                      boolean asc,
                                      @Nullable String searchTerm,
                                      long page,
                                      long pageSize) {

        String userEntityAlias = "ue";
        String userSsoTableAlias = "sso_table";
        String tenantNamePattern = StringSubstitutor.replace(TENANT_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, tenantId));
        String tenantAdminNamePattern = StringSubstitutor.replace(TENANT_ADMIN_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, tenantId));
        String tenantFunctionalAdminNamePattern = StringSubstitutor.replace(TENANT_FUNCTIONAL_ADMIN_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, tenantId));

        Condition searchCondition = Optional.ofNullable(searchTerm)
                .filter(StringUtils::isNotEmpty)
                .map(s -> String.format("%%%s%%", s))
                .map(DSL::upper)
                .map(s -> or(
                        upper(field(userEntityAlias + "." + SQL_USER_USERNAME, String.class)).like(s),
                        upper(field(userEntityAlias + "." + SQL_USER_FIRST_NAME, String.class)).like(s),
                        upper(field(userEntityAlias + "." + SQL_USER_LAST_NAME, String.class)).like(s),
                        upper(field(userEntityAlias + "." + SQL_USER_EMAIL, String.class)).like(s)
                ))
                .orElse(val(1).eq(1));

        Field<String> sortByField = switch (Optional.ofNullable(sortBy).orElse(USERNAME)) {
            case ID -> field(SQL_USER_ID, String.class);
            case FIRST_NAME -> field(SQL_USER_FIRST_NAME, String.class);
            case LAST_NAME -> field(SQL_USER_LAST_NAME, String.class);
            case EMAIL -> field(SQL_USER_EMAIL, String.class);
            default -> field(SQL_USER_USERNAME, String.class);
        };

        return DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(
                        field(userEntityAlias + "." + SQL_USER_ID, String.class).as(User.FIELD_ID),
                        field(userEntityAlias + "." + SQL_USER_USERNAME, String.class).as(User.FIELD_USERNAME),
                        field(userEntityAlias + "." + SQL_USER_FIRST_NAME, String.class).as(User.FIELD_FIRST_NAME),
                        field(userEntityAlias + "." + SQL_USER_LAST_NAME, String.class).as(User.FIELD_LAST_NAME),
                        field(userEntityAlias + "." + SQL_USER_EMAIL, String.class).as(User.FIELD_EMAIL),
                        field(userEntityAlias + "." + SQL_FEDERATION_LINK, String.class).as(User.FIELD_FEDERATION),
                        field(userSsoTableAlias + "." + SQL_SSO_IDENTITY_PROVIDER_FIELD, String.class).as(FIELD_IDENTITY_PROVIDER),
                        max(
                                when(or(field("kr.name", String.class).equal(tenantAdminNamePattern),
                                        field("cf.value", String.class).equal(tenantAdminNamePattern)), 3)
                                        .when(or(field("kr.name", String.class).equal(tenantFunctionalAdminNamePattern),
                                                field("cf.value", String.class).equal(tenantFunctionalAdminNamePattern)), 2)
                                        .when(or(field("kr.name", String.class).equal(tenantNamePattern),
                                                field("cf.value", String.class).equal(tenantNamePattern)), 1)
                                        .otherwise(0)
                        ).as(User.FIELD_PRIVILEGE_INDEX)
                )
                .from(table(SQL_USER_TABLE).as(userEntityAlias))
                .leftOuterJoin(table(SQL_USER_IDENTITY_PROVIDER_TABLE).as(userSsoTableAlias))
                .on(field(userEntityAlias + "." + SQL_USER_ID, String.class)
                        .equal(field(userSsoTableAlias + "." + SQL_IDP_TABLE_USER_ID, String.class)))

                .leftOuterJoin(table("user_role_mapping").as("urm")
                        .innerJoin(table("keycloak_role").as("kr"))
                        .on(
                                and(
                                        field("urm.role_id").equal(field("kr." + SQL_ROLE_ID)),
                                        or(
                                                field("kr.name", String.class).equal(tenantNamePattern),
                                                field("kr.name", String.class).equal(tenantAdminNamePattern),
                                                field("kr.name", String.class).equal(tenantFunctionalAdminNamePattern)
                                        )
                                )
                        ))
                .on(field("urm.user_id").equal(field(userEntityAlias + ".id")))

                .leftOuterJoin(table("component").as("c")
                        .innerJoin(table("component_config").as("cf"))
                        .on(
                                and(
                                        field("c.id").equal(field("cf.component_id")),
                                        field("cf.name").equal("role"),
                                        field("cf.value").equal(tenantNamePattern)
                                )
                        )
                )
                .on(field("c.parent_id").equal(field(userEntityAlias + ".federation_link")))

                .where(and(
                        searchCondition,
                        field(userEntityAlias + "." + SQL_USER_USERNAME, String.class).notIn(INTERNAL_USERNAMES),
                        field(userEntityAlias + "." + SQL_ROLE_REALM_ID, String.class).equal(IPARAPHEUR_REALM)
                ))

                .groupBy(
                        field(userEntityAlias + "." + SQL_USER_ID, String.class),
                        field(userEntityAlias + "." + SQL_USER_USERNAME, String.class),
                        field(userEntityAlias + "." + SQL_USER_FIRST_NAME, String.class),
                        field(userEntityAlias + "." + SQL_USER_LAST_NAME, String.class),
                        field(userEntityAlias + "." + SQL_USER_EMAIL, String.class),
                        field(userEntityAlias + "." + SQL_FEDERATION_LINK, String.class),
                        field(userSsoTableAlias + "." + SQL_SSO_IDENTITY_PROVIDER_FIELD, String.class)
                )
                .having(or(
                        val(tenantNamePattern, String.class).equal(any(arrayAgg(field("kr.name", String.class)))),
                        val(tenantNamePattern, String.class).equal(any(arrayAgg(field("cf.value", String.class)))),
                        val(tenantAdminNamePattern, String.class).equal(any(arrayAgg(field("kr.name", String.class)))),
                        val(tenantAdminNamePattern, String.class).equal(any(arrayAgg(field("cf.value", String.class)))),
                        val(tenantFunctionalAdminNamePattern, String.class).equal(any(arrayAgg(field("kr.name", String.class)))),
                        val(tenantFunctionalAdminNamePattern, String.class).equal(any(arrayAgg(field("cf.value", String.class))))
                ))
                .orderBy(asc ? sortByField.asc() : sortByField.desc())
                .limit(pageSize)
                .offset(page * pageSize);
    }


    @NotNull
    Select<Record1<Integer>>
    buildListUsersTotalRequest(@Nullable String searchTerm) {

        String userEntityAlias = "ue";
        Condition searchCondition = Optional.ofNullable(searchTerm)
                .filter(StringUtils::isNotEmpty)
                .map(s -> String.format("%%%s%%", s))
                .map(DSL::upper)
                .map(s -> or(
                        upper(field(userEntityAlias + "." + SQL_USER_USERNAME, String.class)).like(s),
                        upper(field(userEntityAlias + "." + SQL_USER_FIRST_NAME, String.class)).like(s),
                        upper(field(userEntityAlias + "." + SQL_USER_LAST_NAME, String.class)).like(s),
                        upper(field(userEntityAlias + "." + SQL_USER_EMAIL, String.class)).like(s)
                ))
                .orElse(val(1).eq(1));

        return DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .selectCount()
                .from(table(SQL_USER_TABLE).as(userEntityAlias))
                .where(searchCondition);
    }


    @NotNull
    Select<Record1<Integer>>
    buildSearchUserWithinGroupTotalRequest(@NotNull String tenantId, @Nullable String searchTerm) {

        String userEntityAlias = "ue";
        String tenantNamePattern = StringSubstitutor.replace(TENANT_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, tenantId));
        String tenantAdminNamePattern = StringSubstitutor.replace(TENANT_ADMIN_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, tenantId));
        String tenantFunctionalAdminNamePattern = StringSubstitutor.replace(TENANT_FUNCTIONAL_ADMIN_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, tenantId));

        Condition searchCondition = Optional.ofNullable(searchTerm)
                .filter(StringUtils::isNotEmpty)
                .map(s -> String.format("%%%s%%", s))
                .map(DSL::upper)
                .map(s -> or(
                        upper(field(userEntityAlias + "." + SQL_USER_USERNAME, String.class)).like(s),
                        upper(field(userEntityAlias + "." + SQL_USER_FIRST_NAME, String.class)).like(s),
                        upper(field(userEntityAlias + "." + SQL_USER_LAST_NAME, String.class)).like(s),
                        upper(field(userEntityAlias + "." + SQL_USER_EMAIL, String.class)).like(s)
                ))
                .orElse(val(1).eq(1));

        return DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(countDistinct(field(userEntityAlias + ".id")))

                .from(table(SQL_USER_TABLE).as(userEntityAlias))

                .leftOuterJoin(table("user_role_mapping").as("urm")
                        .innerJoin(table("keycloak_role").as("kr"))
                        .on(
                                and(
                                        field("urm.role_id").equal(field("kr." + SQL_ROLE_ID)),
                                        or(
                                                field("kr.name", String.class).equal(tenantNamePattern),
                                                field("kr.name", String.class).equal(tenantAdminNamePattern),
                                                field("kr.name", String.class).equal(tenantFunctionalAdminNamePattern)
                                        )
                                )
                        ))
                .on(field("urm.user_id").equal(field(userEntityAlias + ".id")))

                .leftOuterJoin(table("component").as("c")
                        .innerJoin(table("component_config").as("cf"))
                        .on(
                                and(
                                        field("c.id").equal(field("cf.component_id")),
                                        field("cf.name").equal("role"),
                                        field("cf.value").equal(tenantNamePattern)
                                )
                        )
                )
                .on(field("c.parent_id").equal(field(userEntityAlias + ".federation_link")))

                .where(
                        and(
                                searchCondition,
                                or(
                                        val(tenantNamePattern, String.class).equal(field("kr.name", String.class)),
                                        val(tenantNamePattern, String.class).equal(field("cf.value", String.class)),
                                        val(tenantAdminNamePattern, String.class).equal(field("kr.name", String.class)),
                                        val(tenantAdminNamePattern, String.class).equal(field("cf.value", String.class)),
                                        val(tenantFunctionalAdminNamePattern, String.class).equal(field("kr.name", String.class)),
                                        val(tenantFunctionalAdminNamePattern, String.class).equal(field("cf.value", String.class))
                                )
                        )
                );
    }


    @NotNull
    Select<Record6<String, String, String, String, String, String>>
    buildListDeskAndDelegates(@NotNull String userId, long page, long pageSize, @Nullable String searchTerm) {

        String deskNamePattern = StringSubstitutor.replace(DESK_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, "%", DESK_PLACEHOLDER, "%"));

        return DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(
                        field("keycloak_role_parent." + SQL_ROLE_ID, String.class).as(SQL_ROLE_ID),
                        field("keycloak_role_parent." + SQL_ROLE_DESCRIPTION, String.class).as(SQL_ROLE_DESCRIPTION),
                        field("keycloak_role_parent." + SQL_ROLE_FULL_NAME, String.class).as(SQL_ROLE_FULL_NAME),
                        field("keycloak_role_child." + SQL_ROLE_ID, String.class).as(SQL_ROLE_DELEGATING_ID),
                        field("role_attribute_parent.value", String.class).as(SQL_ROLE_PARENT_METADATA_DELEGATION),
                        DSL.substring(field("keycloak_role_parent." + SQL_ROLE_FULL_NAME, String.class), 7, 36).as("tenantId")
                )
                .from(table("user_role_mapping"))

                .leftJoin(table("composite_role"))
                .on(field("composite_role.composite").equal(field("user_role_mapping.role_id")))

                .innerJoin(table("keycloak_role").as("keycloak_role_parent"))
                .on(field("user_role_mapping.role_id").equal(field("keycloak_role_parent." + SQL_ROLE_ID)))

                .leftJoin(table("keycloak_role").as("keycloak_role_child"))
                .on(field("composite_role.child_role").equal(field("keycloak_role_child." + SQL_ROLE_ID)))

                .leftJoin(table("role_attribute").as("role_attribute_parent"))
                .on(field("keycloak_role_parent.id").equal(field("role_attribute_parent.role_id"))
                        .and(field("role_attribute_parent.name").equal(ATTRIBUTE_TYPOLOGY_DELEGATIONS)))

                .where(
                        and(
                                field("user_role_mapping.user_id", String.class).eq(userId),
                                field("keycloak_role_parent." + SQL_ROLE_REALM_ID, String.class).eq(IPARAPHEUR_REALM),
                                field("keycloak_role_parent.name", String.class).like(deskNamePattern),
                                isNotEmpty(searchTerm)
                                ? lower(field("keycloak_role_parent." + SQL_ROLE_DESCRIPTION, String.class))
                                        .like(lower(val("%" + searchTerm + "%")))
                                : val(1).equal(val(1))
                        )
                )
                .orderBy(field("tenantId"))
                .limit(pageSize)
                .offset(page * pageSize);
    }


    @NotNull
    Select<Record2<String, String>> buildGetDeskNames(@NotNull Collection<String> deskIds) {
        return DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(
                        field("kr." + SQL_ROLE_ID, String.class).as(SQL_ROLE_ID),
                        field("kr." + SQL_ROLE_DESCRIPTION, String.class).as(SQL_ROLE_DESCRIPTION)
                )
                .from(table(SQL_ROLE_TABLE).as("kr"))
                .innerJoin(table(SQL_REALM_TABLE).as("r"))
                .on(field("kr." + SQL_ROLE_REALM_ID, String.class).eq(field("r." + SQL_REALM_ID, String.class)))
                .where(and(
                        field("r." + SQL_REALM_NAME, String.class).equal(authServiceProperties.getRealm()),
                        field("kr." + SQL_ROLE_ID).in(deskIds)
                ));
    }


    @NotNull
    Select<Record3<String, String, String>> buildGetDeskNamesAndRoles(@NotNull Collection<String> deskIds) {
        return DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(
                        field(SQL_ROLE_ID, String.class),
                        field(SQL_ROLE_FULL_NAME, String.class),
                        field(SQL_ROLE_DESCRIPTION, String.class)
                )
                .from(table(SQL_ROLE_TABLE))
                .where(and(
                        field(SQL_ROLE_REALM_ID).equal(IPARAPHEUR_REALM),
                        field(SQL_ROLE_ID).in(deskIds)
                ));
    }


    @NotNull
    Select<Record7<String, String, String, String, String, String, String>> buildListUsersByIds(@NotNull Collection<String> userIds) {
        String userEntityAlias = "ue";
        String userSsoTableAlias = "sso_table";
        return DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(
                        field(userEntityAlias + "." + SQL_USER_ID, String.class).as(User.FIELD_ID),
                        field(userEntityAlias + "." + SQL_USER_USERNAME, String.class).as(User.FIELD_USERNAME),
                        field(userEntityAlias + "." + SQL_USER_FIRST_NAME, String.class).as(User.FIELD_FIRST_NAME),
                        field(userEntityAlias + "." + SQL_USER_LAST_NAME, String.class).as(User.FIELD_LAST_NAME),
                        field(userEntityAlias + "." + SQL_USER_EMAIL, String.class).as(User.FIELD_EMAIL),
                        field(userEntityAlias + "." + SQL_FEDERATION_LINK, String.class).as(User.FIELD_FEDERATION),
                        field(userSsoTableAlias + "." + SQL_SSO_IDENTITY_PROVIDER_FIELD, String.class).as(FIELD_IDENTITY_PROVIDER)
                )
                .from(table(SQL_USER_TABLE).as(userEntityAlias))
                .leftOuterJoin(table(SQL_USER_IDENTITY_PROVIDER_TABLE).as(userSsoTableAlias))
                .on(field(userEntityAlias + "." + SQL_USER_ID, String.class)
                        .equal(field(userSsoTableAlias + "." + SQL_IDP_TABLE_USER_ID, String.class)))
                .where(and(
                        field(userEntityAlias + "." + SQL_ROLE_REALM_ID, String.class).eq(IPARAPHEUR_REALM),
                        field(userEntityAlias + "." + SQL_USER_ID, String.class).in(userIds)
                ));
    }


    @NotNull
    Select<Record1<String>> buildFilterUsersWithPrivilege(@NotNull Collection<String> userIds,
                                                          @NotNull UserPrivilege privilege,
                                                          @Nullable String targetTenantId) {

        String targetTenantIdSql = Optional.ofNullable(targetTenantId)
                .filter(StringUtils::isNotEmpty)
                .orElse(SQL_LIKE_ANY_36_CHAR);

        String sqlRoleNameSql = switch (privilege) {
            case SUPER_ADMIN -> SUPER_ADMIN_ROLE_NAME;
            case TENANT_ADMIN -> StringSubstitutor.replace(TENANT_ADMIN_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, targetTenantIdSql));
            case FUNCTIONAL_ADMIN -> StringSubstitutor.replace(TENANT_FUNCTIONAL_ADMIN_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, targetTenantIdSql));
            default -> throw new RuntimeException("This request is not available for the privilege " + privilege);
        };

        Condition sqlRoleCondition = Optional
                .of(field("kr.name", String.class))
                .map(f -> StringUtils.isNotEmpty(targetTenantId)
                          ? f.eq(sqlRoleNameSql)
                          : f.like(sqlRoleNameSql)
                )
                .get();

        SelectConditionStep<Record1<String>> result = using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(field("urm.user_id", String.class).as(FIELD_ID))
                .from(table(SQL_ROLE_TABLE).as("kr"))
                .join(table("user_role_mapping").as("urm"))
                .on(field("kr.id", String.class).eq(field("urm.role_id", String.class)))
                .join(table("realm").as("r"))
                .on(and(
                        field("kr.realm_id", String.class).eq(field("r.id", String.class)),
                        field("r.name", String.class).eq(authServiceProperties.getRealm())
                ))
                .where(and(
                        sqlRoleCondition,
                        field("urm.user_id", String.class).in(userIds)
                ));

        log.trace("buildFilterUsersWithPrivilege request:\n{}", result.getSQL(INLINED));
        return result;
    }


    @NotNull
    Select<Record6<String, String, Integer, Integer, String[], String[]>>
    buildListDesksSqlRequest(@NotNull String tenantId, long page, long pageSize, boolean collapseAll,
                             @NotNull List<String> reversedIdList, @Nullable String searchTerm, @Nullable String idEquals, boolean asc) {

        String deskNamePattern = StringSubstitutor.replace(DESK_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, tenantId, DESK_PLACEHOLDER, "%"));
        DefaultDataType<String> psqlTextDataType = new DefaultDataType<>(POSTGRES, CLOB, "TEXT");

        return DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .withRecursive("rel_tree").as(
                        select(
                                field("kr." + SQL_ROLE_ID, String.class).as(SQL_ID),
                                val(0, Integer.class).as(SQL_LEVEL),
                                array(field("kr.description", String.class).concat(" :: ").concat(field("kr.id", String.class)))
                                        .cast(psqlTextDataType.getArrayDataType()).as(SQL_PARENT_NAMES_CHAIN),
                                array(field("kr." + SQL_ROLE_ID, String.class))
                                        .cast(psqlTextDataType.getArrayDataType()).as(SQL_PARENT_IDS_CHAIN),
                                field("kr.description").as(SQL_NAME)
                        )
                                .from(table("keycloak_role").as("kr"))

                                .leftJoin(table("role_attribute").as("ra"))
                                .on(field("kr." + SQL_ROLE_ID).equal(field("ra.role_id")))
                                .and(field("ra.name").equal(ATTRIBUTE_PARENT_DESK_ID))

                                .where(field("ra.value").isNull())
                                .and(field("kr.name").like(deskNamePattern))

                                .unionAll(
                                        select(
                                                field("ra.role_id", String.class).as(SQL_ID),
                                                field("p.level", Integer.class).plus(1).as(SQL_LEVEL),
                                                arrayAppend(
                                                        field("p." + SQL_PARENT_NAMES_CHAIN, String[].class),
                                                        field("kr.description", String.class)
                                                                .concat(" :: ")
                                                                .concat(field("ra.role_id", String.class)).cast(psqlTextDataType))
                                                        .as(SQL_PARENT_NAMES_CHAIN),
                                                arrayAppend(
                                                        field("p." + SQL_PARENT_IDS_CHAIN, String[].class),
                                                        field("ra.role_id", String.class).cast(psqlTextDataType))
                                                        .as(SQL_PARENT_IDS_CHAIN),
                                                field("kr.description").as(SQL_NAME)
                                        )
                                                .from(table("role_attribute").as("ra"))

                                                .join(table("rel_tree").as("p"))
                                                .on(field("p.id").equal(field("ra.value")))

                                                .join(table("keycloak_role").as("kr"))
                                                .on(field("kr.id").equal(field("ra.role_id")))

                                                .where(field("ra.name").equal(ATTRIBUTE_PARENT_DESK_ID))
                                                .and(collapseAll ? field("ra.value").in(reversedIdList) : field("ra.value").notIn(reversedIdList))
                                                .and(field("kr.name").like(deskNamePattern))
                                )
                )
                .select(
                        field("rt." + SQL_ID, String.class).as(SQL_ID),
                        field("rt." + SQL_NAME, String.class).as(SQL_NAME),
                        field("rt." + SQL_LEVEL, Integer.class).as(SQL_LEVEL),
                        count(field("ra.id")).as(SQL_DIRECT_CHILDREN_COUNT),
                        field("rt." + SQL_PARENT_NAMES_CHAIN, String[].class).as(SQL_PARENT_NAMES_CHAIN),
                        field("rt." + SQL_PARENT_IDS_CHAIN, String[].class).as(SQL_PARENT_IDS_CHAIN)
                )
                .from(table("rel_tree").as("rt"))

                .leftJoin(table("role_attribute").as("ra"))
                .on(field("ra.value").equal(field("rt." + SQL_ID)))
                .and(field("ra.name").equal(ATTRIBUTE_PARENT_DESK_ID))

                .leftJoin(table("role_attribute").as("ra_shortname"))
                .on(field("rt." + SQL_ID).equal(field("ra_shortname.role_id")))
                .and(field("ra_shortname.name").equal(ATTRIBUTE_SHORT_NAME))

                .where(TextUtils.getAsSqlLikeWrapped(searchTerm)
                        .map(wrappedSearch -> (Condition) or(
                                field(SQL_NAME, String.class).likeIgnoreCase(wrappedSearch),
                                field("ra_shortname.value").likeIgnoreCase(wrappedSearch)
                        ))
                        .orElse(TRUE_CONDITION))


                .and(isNotEmpty(idEquals) ? field("rt." + SQL_ID).equal(val(idEquals)) : val(1).equal(val(1)))

                .groupBy(field("rt." + SQL_ID), field(SQL_NAME), field(SQL_LEVEL), field(SQL_PARENT_NAMES_CHAIN), field(SQL_PARENT_IDS_CHAIN))
                .orderBy(asc ? field(SQL_PARENT_NAMES_CHAIN).asc() : field(SQL_PARENT_NAMES_CHAIN).desc())
                .limit(pageSize)
                .offset(page * pageSize);
    }


    @NotNull
    Select<Record1<Integer>>
    buildTotalDesksSqlRequest(@NotNull String tenantId, boolean collapseAll, @NotNull List<String> reversedIdList,
                              @Nullable String searchTerm) {

        String deskNamePattern = StringSubstitutor.replace(DESK_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, tenantId, DESK_PLACEHOLDER, "%"));

        return DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .withRecursive("rel_tree").as(
                        select(
                                field("kr.id", String.class).as(SQL_ID),
                                field("kr.description", String.class).as(SQL_NAME)
                        )
                                .from(table("keycloak_role").as("kr"))

                                .leftJoin(table("role_attribute").as("ra"))
                                .on(field("kr.id").equal(field("ra.role_id")))
                                .and(field("ra.name").equal(ATTRIBUTE_PARENT_DESK_ID))

                                .where(field("ra.value").isNull())
                                .and(field("kr.name").like(deskNamePattern))

                                .unionAll(
                                        select(
                                                field("ra.role_id", String.class).as(SQL_ID),
                                                field("kr.description", String.class).as(SQL_NAME)
                                        )
                                                .from(table("role_attribute").as("ra"))

                                                .join(table("rel_tree").as("p"))
                                                .on(field("p.id").equal(field("ra.value")))

                                                .join(table("keycloak_role").as("kr"))
                                                .on(field("kr.id").equal(field("ra.role_id")))

                                                .where(field("ra.name").equal(ATTRIBUTE_PARENT_DESK_ID))
                                                .and(collapseAll ? field("ra.value").in(reversedIdList) : field("ra.value").notIn(reversedIdList))
                                                .and(field("kr.name").like(deskNamePattern))
                                )
                )

                .selectCount()
                .from(table("rel_tree").as("rt"))

                .where(TextUtils.getAsSqlLikeWrapped(searchTerm)
                        .map(wrappedSearch -> (Condition) field(SQL_NAME, String.class).likeIgnoreCase(wrappedSearch))
                        .orElse(TRUE_CONDITION));
    }


    @NotNull
    Select<Record2<String, String>>
    buildListDesksByShortNameSqlRequest(@NotNull String tenantId, @Nullable String shortName) {

        String deskNamePattern = StringSubstitutor.replace(DESK_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, tenantId, DESK_PLACEHOLDER, "%"));

        return DSL.using(POSTGRES, PSQL_RENDER_SETTINGS)
                .select(
                        field("kr.id", String.class).as(SQL_ID),
                        field("kr.description", String.class).as(SQL_NAME)
                )
                .from(table("keycloak_role").as("kr"))

                .leftJoin(table("role_attribute").as("ra"))
                .on(field("kr.id", String.class).equal(field("ra.role_id", String.class)))
                .and(field("ra.name", String.class).equal(ATTRIBUTE_SHORT_NAME))

                .where(field("kr.name", String.class).like(deskNamePattern))
                .and(field("ra.value", String.class).equalIgnoreCase(shortName));
    }


    /**
     * Checks if the given username is tenant-related
     *
     * @param username the target user
     * @param tenantId the target tenant
     * @return
     */
    boolean doesUserExistOnTenant(@NotNull String username, @Nullable String tenantId) {

        if (StringUtils.isEmpty(tenantId)) {
            return false;
        }

        return usersResourceClient.searchByUsername(username, true).stream()
                // This filtrer prevents false positive on overlapped usernames.
                // Users named "a" and "ab" can both be returned on a "a" search.
                .filter(existingUser -> StringUtils.equals(username, existingUser.getUsername()))
                .map(existingUser -> listTenantsForUser(existingUser.getId()))
                .flatMap(Collection::stream)
                .anyMatch(id -> StringUtils.equals(id, tenantId));
    }


}
