/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.mail;

import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.crypto.SealCertificateRepresentation;
import coop.libriciel.ipcore.model.database.TenantRepresentation;
import coop.libriciel.ipcore.model.mail.MailContent;
import coop.libriciel.ipcore.model.mail.MailNotification;
import coop.libriciel.ipcore.model.mail.MailTarget;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.core.io.Resource;

import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static coop.libriciel.ipcore.model.auth.User.USER_PREFERENCE_NO_NOTIFICATIONS;
import static coop.libriciel.ipcore.model.auth.User.USER_PREFERENCE_SINGLE_NOTIFICATIONS;


public interface NotificationServiceInterface {

    long MAX_MAIL_ATTACHMENT_SIZE = 10 * 1024 * 1024;

    Logger log = LogManager.getLogger(NotificationServiceInterface.class);

    String BEAN_NAME = "notificationService";
    String PREFERENCES_PROVIDER_KEY = "services.mail.provider";

    static boolean isScheduledNotification(String notificationFrequency) {
        return StringUtils.isNotEmpty(notificationFrequency)
                && !StringUtils.equals(notificationFrequency, USER_PREFERENCE_SINGLE_NOTIFICATIONS)
                && !StringUtils.equals(notificationFrequency, USER_PREFERENCE_NO_NOTIFICATIONS);
    }

    /**
     * Flushing pending notifications, if needed.
     * If some are pending, and if the settings had changed accordingly.
     *
     * @param userId
     * @param oldUserCron
     * @param newUserCron
     */
    default void flushPendingNotifications(@NotNull String userId,
                                           @Nullable String oldUserCron,
                                           @Nullable String newUserCron) {

        log.debug("flushPendingNotifications userId:{}", userId);

        boolean wasDigestNotifications = isScheduledNotification(oldUserCron);
        boolean newSettingIsNotDigest = !isScheduledNotification(newUserCron);

        if (wasDigestNotifications && newSettingIsNotDigest) {
            computePendingMails(userId);
        }
    }


    void processNotification(@NotNull MailNotification notification, @Nullable User user);


    void computePendingMails(@NotNull String userId);


    void mailExpiredCertificates(@NotNull List<User> usersToNotify,
                                 @NotNull TenantRepresentation tenant,
                                 @NotNull List<SealCertificateRepresentation> certificates);


    void mailFolder(@NotNull List<MailTarget> targetList,
                    @NotNull Resource mailTemplate,
                    @NotNull List<MailContent> contentList,
                    @Nullable User sender,
                    @Nullable String object,
                    @Nullable String message,
                    @Nullable InputStream mergedDocsInputStream,
                    @Nullable String attachementFilename);


    void resetNotificationScheduler(String userId, String newNotificationFrequency);

    Map<String, String> getDesksToNotifyByDelegation(String tenantId, Set<String> directDesksToNotify);
}
