/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
package coop.libriciel.ipcore.services.mail;

import lombok.extern.log4j.Log4j2;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import static coop.libriciel.ipcore.utils.TextUtils.INTERNAL_PREFIX;


@Log4j2
@Component
@DisallowConcurrentExecution
@PersistJobDataAfterExecution
public class NotificationJob extends QuartzJobBean {


    public static final String TRIGGER_GROUP = INTERNAL_PREFIX + "notification_job_trigger_group";
    public static final String DATA_MAP_KEY_USER_ID = "user_id";

    // Dev Warning: Intelllij puts a warning here because of the autowired field, but it is **mandatory** for quartz...
    // Do NOT apply the proposed action
    @Autowired
    private NotificationServiceInterface notificationService;


    public NotificationJob() {

    }


    @Override
    protected void executeInternal(JobExecutionContext context) {

        JobDataMap jobDataMap = context.getMergedJobDataMap();
        String userId = jobDataMap.getString(DATA_MAP_KEY_USER_ID);

        log.info("Notification fired for user:{}", userId);
        notificationService.computePendingMails(userId);
    }


}
