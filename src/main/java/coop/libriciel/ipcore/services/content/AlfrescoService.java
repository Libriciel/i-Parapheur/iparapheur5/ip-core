/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.content;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import coop.libriciel.alfresco.core.ApiClient;
import coop.libriciel.alfresco.core.client.NodesApi;
import coop.libriciel.alfresco.core.model.*;
import coop.libriciel.iparapheur.customalfrescotengines.model.content.PdfVersion;
import coop.libriciel.ipcore.model.content.*;
import coop.libriciel.ipcore.model.content.alfresco.AlfrescoEntry;
import coop.libriciel.ipcore.model.content.alfresco.AlfrescoNode;
import coop.libriciel.ipcore.model.content.alfresco.AlfrescoTicketRequest;
import coop.libriciel.ipcore.model.crypto.PdfSignaturePosition;
import coop.libriciel.ipcore.model.database.TemplateType;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.pdfstamp.Layer;
import coop.libriciel.ipcore.model.pdfstamp.SignaturePlacement;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.utils.FileUtils;
import coop.libriciel.ipcore.utils.IpInternalException;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.RequestUtils;
import coop.libriciel.ipcore.utils.exceptions.ExternalException;
import jakarta.annotation.PostConstruct;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringSubstitutor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.http.InvalidMediaTypeException;
import org.springframework.http.MediaType;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientException;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Stream;

import static coop.libriciel.ipcore.model.content.alfresco.AlfrescoNode.*;
import static coop.libriciel.ipcore.model.content.alfresco.AlfrescoProperties.PROPERTY_TARGET_DOCUMENT_ID;
import static coop.libriciel.ipcore.model.workflow.Action.START;
import static coop.libriciel.ipcore.utils.CollectionUtils.*;
import static coop.libriciel.ipcore.utils.FolderUtils.MAX_SIGNATURE_PROOF_FILE_PER_FOLDER;
import static coop.libriciel.ipcore.utils.PaginatedList.MAX_PAGE_SIZE;
import static coop.libriciel.ipcore.utils.RequestUtils.*;
import static coop.libriciel.ipcore.utils.TextUtils.INTERNAL_PREFIX;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Calendar.*;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.Comparator.*;
import static java.util.Locale.ROOT;
import static java.util.stream.Collectors.toMap;
import static org.apache.commons.io.FilenameUtils.getExtension;
import static org.apache.commons.io.FilenameUtils.removeExtension;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.springframework.core.io.buffer.DataBufferUtils.readInputStream;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.MediaType.*;
import static org.springframework.security.config.Elements.HTTP;


@Log4j2
@Service(ContentServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = ContentServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = AlfrescoService.PROVIDER_NAME)
public class AlfrescoService implements ContentServiceInterface {


    public static final String PROVIDER_NAME = "alfresco";
    public static final String PREMIS_NODE_FILE_NAME = INTERNAL_PREFIX + "premis.xml";

    /**
     * These metadata are folder-specific.
     * They won't change, even on a version update.
     */
    private static final String PROPERTY_TASK_ID = "iparapheur:proofTargetTaskId";
    private static final String PROPERTY_INDEX = "iparapheur:index";
    private static final String PROPERTY_IS_MAIN_DOCUMENT = "iparapheur:isMainDocument";
    private static final String PROPERTY_IS_INTERNAL = "iparapheur:isInternal";
    private static final String PROPERTY_ERROR_MESSAGE = "iparapheur:errorMessage";
    private static final String PROPERTY_SOURCE_DOCUMENT_ID = "iparapheur:sourceDocumentId";
    private static final String PROPERTY_TARGET_TASK_ID = "iparapheur:targetTaskId";
    private static final String PROPERTY_DELETABLE = "iparapheur:deletable";
    private static final String PROPERTY_DETACHED_SIGNATURE = "iparapheur:extractedDetachedSignatureInformation";

    /**
     * These properties are automatically extracted by the Transform Extractor.
     * They should be extracted again on every document update (every signature...).
     * To force their re-extraction, they have to be removed.
     */
    private static final String PROPERTY_PAGES_PROPERTIES = "iparapheur:extractedPagesProperties";
    private static final String PROPERTY_PAGE_COUNT = "iparapheur:pageCount";
    private static final String PROPERTY_CHECKSUM_VALUE = "iparapheur:sha256Checksum";
    static final String PROPERTY_PDF_VERSION = "iparapheur:pdfVersion";
    static final String PROPERTY_SIGNATURES = "iparapheur:extractedDocumentSignatures";
    private static final String PROPERTY_EXTRACTED_SIGNATURE_TAGS = "iparapheur:extractedSignatureTags";
    private static final String PROPERTY_EXTRACTED_SEAL_TAGS = "iparapheur:extractedSealTags";
    private static final String PROPERTY_PASSWORD_PRESENCE = "iparapheur:passwordPresence";

    /**
     * Extracted properties that should be absolute.
     * On minor document modification (signature, comment...), these do not need another extraction.
     */
    private static final Set<String> DOCUMENT_EXTRACTED_FINAL_METADATA = Set.of(
            PROPERTY_PDF_VERSION, PROPERTY_PASSWORD_PRESENCE,
            PROPERTY_PAGES_PROPERTIES, PROPERTY_PAGE_COUNT,
            PROPERTY_EXTRACTED_SIGNATURE_TAGS, PROPERTY_EXTRACTED_SEAL_TAGS
    );

    /**
     * Extracted properties that should be reset on any document modification (signature, comment...).
     */
    private static final Set<String> DOCUMENT_EXTRACTED_VERSATILE_METADATA = Set.of(
            PROPERTY_CHECKSUM_VALUE, PROPERTY_SIGNATURES
    );

    /**
     * These properties are user-specific.
     * They should stay on a regular document update (every signature...).
     * But they have to be reset on every document swap (in a Draft step).
     */
    private static final String PROPERTY_ANNOTATIONS = "iparapheur:annotations";
    private static final String PROPERTY_SIGNATURE_TAGS = "iparapheur:signatureTags";
    private static final String PROPERTY_SEAL_TAGS = "iparapheur:sealTags";
    private static final Set<String> DOCUMENT_USER_METADATA = Set.of(
            PROPERTY_ANNOTATIONS, PROPERTY_SIGNATURE_TAGS, PROPERTY_SEAL_TAGS
    );

    private static final String AUTHORITY_GROUP_PLACEHOLDER = "authorityGroup";
    private static final String AUTHORITY_ID_PLACEHOLDER = "authorityId";
    private static final String YEAR_PLACEHOLDER = "year";
    private static final String MONTH_PLACEHOLDER = "month";
    private static final String DAY_PLACEHOLDER = "day";
    private static final String HOUR_PLACEHOLDER = "hour";
    private static final String MINUTE_PLACEHOLDER = "minute";
    private static final String USER_GROUP_PLACEHOLDER = "userGroup";
    private static final String USER_ID_PLACEHOLDER = "userId";
    private static final String DATA_PATH_AUTHORITIES_GROUP = String.format("authorities/${%s}", AUTHORITY_GROUP_PLACEHOLDER);
    private static final String DATA_PATH_TENANT = String.format("%s/${%s}", DATA_PATH_AUTHORITIES_GROUP, AUTHORITY_ID_PLACEHOLDER);
    private static final String FOLDER_DATA_PATH = String.format("%s/folders/${%s}/${%s}-${%s}/${%s}-${%s}",
            DATA_PATH_TENANT, YEAR_PLACEHOLDER, MONTH_PLACEHOLDER, DAY_PLACEHOLDER, HOUR_PLACEHOLDER, MINUTE_PLACEHOLDER);
    private static final String USER_DATA_PATH = String.format("user_data/${%s}/${%s}", USER_GROUP_PLACEHOLDER, USER_ID_PLACEHOLDER);
    private static final String TICKET_URL = "alfresco/api/-default-/public/authentication/versions/1/tickets";
    private static final String NODES_URL = "alfresco/api/-default-/public/alfresco/versions/1/nodes";
    private static final String ROOT_NODE = "-root-";
    private static final String NODE_CHILDREN = "children";
    private static final String NODE_CONTENT = "content";
    private static final String PROPERTIES_KEY = "properties";


    private static final Comparator<DetachedSignature> DETACHED_SIGNATURE_COMPARATOR = comparing(DetachedSignature::getTargetTaskId, nullsLast(naturalOrder()))
            .thenComparing(DetachedSignature::getName, nullsLast(naturalOrder()))
            .thenComparing(DetachedSignature::getId, nullsLast(naturalOrder()));

    static final Converter<ContentInfo, Long> CONTENT_INFO_TO_CONTENT_LENGTH_CONVERTER =
            context -> Optional.ofNullable(context.getSource())
                    .map(ContentInfo::getSizeInBytes)
                    .orElse(-1L);

    static final Converter<ContentInfo, MediaType> CONTENT_INFO_TO_MEDIA_TYPE_CONVERTER =
            context -> Optional.ofNullable(context.getSource())
                    .map(ContentInfo::getMimeType)
                    .filter(StringUtils::isNotEmpty)
                    .map(serialized -> {
                        try {
                            return MediaType.parseMediaType(serialized);
                        } catch (InvalidMediaTypeException e) {
                            return null;
                        }
                    })
                    .orElse(null);


    static final Converter<Map<String, Object>, Integer> PROPERTIES_TO_PAGE_COUNT_EXTRACTOR =
            context -> MapUtils.getInteger(context.getSource(), PROPERTY_PAGE_COUNT, null);


    static final Converter<Map<String, Object>, Float> PROPERTIES_TO_MEDIA_VERSION_EXTRACTOR =
            context -> Optional
                    .ofNullable(MapUtils.getString(context.getSource(), PROPERTY_PDF_VERSION, null))
                    // Parse the serialized String into the lib's Object
                    .map(PdfVersion::valueOf)
                    // Map the lib object to internal Object
                    .map(PdfVersion::getVersion)
                    .orElse(null);

    static final Converter<Map<String, Object>, Map<String, PageInfo>> PROPERTIES_TO_PAGE_INFO_MAP_EXTRACTOR =
            context -> Optional
                    .ofNullable(MapUtils.getString(context.getSource(), PROPERTY_PAGES_PROPERTIES, null))
                    .map(serializedString -> {
                        try {
                            return new ObjectMapper().readValue(serializedString, new TypeReference<Map<String, PageInfo>>() {});
                        } catch (JsonProcessingException e) {
                            log.warn("Cannot parse page properties", e);
                            return new HashMap<String, PageInfo>();
                        }
                    })
                    .orElse(emptyMap());


    private final ObjectMapper objectMapper;
    private final WebClient.Builder sharedWebClientBuilder;
    private final ContentServiceProperties properties;
    private final ModelMapper alfrescoModelMapper;
    private static final ModelMapper modelMapper = new ModelMapper();
    private String authHeader = null;
    private ApiClient apiClient;


    @Autowired
    public AlfrescoService(ContentServiceProperties properties,
                           ObjectMapper objectMapper,
                           WebClient.Builder sharedWebClientBuilder) {

        this.properties = properties;
        this.objectMapper = objectMapper;
        this.sharedWebClientBuilder = sharedWebClientBuilder;

        this.alfrescoModelMapper = generateAlfrescoModelMapper();
    }


    static Converter<Map<String, Object>, List<ValidatedSignatureInformation>> generatePropertiesToEmbeddedSignatureInfoExtractor(@NotNull ObjectMapper objectMapper) {
        return context -> Optional.ofNullable(context.getSource())
                .map(map -> MapUtils.getString(map, PROPERTY_SIGNATURES, null))
                .filter(StringUtils::isNotEmpty)
                // Parse the serialized String into the lib's Object
                .map(serializedSignatureInfo -> {
                    try {
                        return objectMapper.readValue(
                                serializedSignatureInfo,
                                new TypeReference<List<ValidatedSignatureInformation>>() {}
                        );
                    } catch (JsonProcessingException e) {
                        log.warn("Cannot parse the signature-info from Alfresco. Will use `null`, to mark indeterminate status.");
                        log.trace("Error: ", e);
                        return null;
                    }
                })
                // Map the lib object to internal Object
                .map(list -> list.stream()
                        .map(serializedObject -> modelMapper.map(serializedObject, ValidatedSignatureInformation.class))
                        .toList())
                .orElse(null);
    }


    static Converter<Map<String, Object>, List<SignaturePlacement>> generatePropertiesToAnnotationsExtractor(@NotNull ObjectMapper objectMapper) {
        return context -> Optional.ofNullable(context.getSource())
                .map(map -> MapUtils.getString(map, PROPERTY_ANNOTATIONS, null))
                .filter(StringUtils::isNotEmpty)
                // Parse the serialized String into the internal Object.
                // The object is already serialized by Core, and is not know by Alfresco-Transform-Extractor.
                // That's why we don't pass through any lib Object.
                .map(serializedSignatureInfo -> {
                    try {
                        return objectMapper.readValue(
                                serializedSignatureInfo,
                                new TypeReference<List<SignaturePlacement>>() {}
                        );
                    } catch (JsonProcessingException e) {
                        log.warn("Cannot parse the signature-positioning from Alfresco. Will use `null`, to set the default position.");
                        log.trace("Error: ", e);
                        return null;
                    }
                })
                .orElse(emptyList());
    }


    private static Converter<Map<String, Object>, Map<Integer, PdfSignaturePosition>> generateSignaturePositionsExtractor(@NotNull ObjectMapper objectMapper,
                                                                                                                          @NotNull String key,
                                                                                                                          @NotNull String fallbackKey) {
        return context -> Optional.ofNullable(context.getSource())
                .map(map -> MapUtils.getString(map, key, null))
                .filter(StringUtils::isNotEmpty)
                // Parse the internal list, if exists
                .map(serializedString -> {
                    try {
                        return objectMapper.readValue(
                                serializedString,
                                new TypeReference<Map<Integer, PdfSignaturePosition>>() {}
                        );
                    } catch (JsonProcessingException e) {
                        log.warn("Cannot parse the tag-position from Alfresco. Will use a null value, to mark indeterminate status.");
                        log.debug("Error: ", e);
                        return null;
                    }
                })
                // Parse the default list
                .orElseGet(() -> Optional.ofNullable(context.getSource())
                        .map(map -> MapUtils.getString(map, fallbackKey, null))
                        .filter(StringUtils::isNotEmpty)
                        .map(serializedString -> {
                            try {
                                return objectMapper.readValue(
                                        serializedString,
                                        new TypeReference<Map<Integer, coop.libriciel.iparapheur.customalfrescotengines.model.crypto.PdfSignaturePosition>>() {}
                                );
                            } catch (JsonProcessingException e) {
                                log.warn("Cannot parse the default tag-position from Alfresco. Will use a null value, to mark indeterminate status.");
                                log.debug("Error: ", e);
                                return null;
                            }
                        })
                        .map(map -> map.entrySet()
                                .stream()
                                .collect(toMap(
                                        Map.Entry::getKey,
                                        entry -> modelMapper.map(entry.getValue(), PdfSignaturePosition.class)
                                ))
                        )
                        // The null value marks an indeterminate status
                        .orElse(null)
                );
    }


    static @NotNull String computePathPlaceholders(@NotNull Tenant tenant) {
        return StringSubstitutor.replace(
                FOLDER_DATA_PATH,
                Map.of(
                        AUTHORITY_GROUP_PLACEHOLDER, String.valueOf(tenant.getIndex() / 1000),
                        AUTHORITY_ID_PLACEHOLDER, tenant.getId(),
                        YEAR_PLACEHOLDER, Integer.toString(Calendar.getInstance().get(YEAR)),
                        MONTH_PLACEHOLDER, Integer.toString(Calendar.getInstance().get(MONTH)),
                        DAY_PLACEHOLDER, Integer.toString(Calendar.getInstance().get(DAY_OF_MONTH)),
                        HOUR_PLACEHOLDER, Integer.toString(Calendar.getInstance().get(HOUR)),
                        MINUTE_PLACEHOLDER, Integer.toString(Calendar.getInstance().get(MINUTE))
                ));
    }


    ModelMapper generateAlfrescoModelMapper() {
        ModelMapper modelMapper = new ModelMapper();

        // Alfresco mappings
        // Some seems like duplicates, but the Alfresco lib generates multiples and non-related objects, so... multiples mappings.

        modelMapper.typeMap(Node.class, Document.class)
                .addMappings(m -> m.using(CONTENT_INFO_TO_CONTENT_LENGTH_CONVERTER)
                        .map(Node::getContent, Document::setContentLength))
                .addMappings(m -> m.using(CONTENT_INFO_TO_MEDIA_TYPE_CONVERTER)
                        .map(Node::getContent, Document::setMediaType))
                .addMappings(m -> m.using(PROPERTIES_TO_MEDIA_VERSION_EXTRACTOR)
                        .map(Node::getProperties, Document::setMediaVersion))
                .addMappings(m -> m.using(PROPERTIES_TO_PAGE_COUNT_EXTRACTOR)
                        .map(Node::getProperties, Document::setPageCount))
                .addMappings(m -> m.using(PROPERTIES_TO_PAGE_INFO_MAP_EXTRACTOR)
                        .map(Node::getProperties, Document::setPagesProperties))
                .addMappings(m -> m.using(createMapIntegerExtractor(PROPERTY_INDEX, null))
                        .map(Node::getProperties, Document::setIndex))
                .addMappings(m -> m.using(createMapBooleanExtractor(PROPERTY_DELETABLE))
                        .map(Node::getProperties, Document::setDeletable))
                .addMappings(m -> m.using(createMapStringExtractor(PROPERTY_CHECKSUM_VALUE))
                        .map(Node::getProperties, Document::setChecksumValue))
                .addMappings(m -> m.using(createMapBooleanExtractor(PROPERTY_IS_MAIN_DOCUMENT))
                        .map(Node::getProperties, Document::setMainDocument))
                .addMappings(m -> m.using(createMapBooleanExtractor(PROPERTY_PASSWORD_PRESENCE))
                        .map(Node::getProperties, Document::setPasswordProtected))
                .addMappings(m -> m.using(generatePropertiesToEmbeddedSignatureInfoExtractor(objectMapper))
                        .map(Node::getProperties, Document::setEmbeddedSignatureInfos))
                .addMappings(m -> m.using(generatePropertiesToAnnotationsExtractor(objectMapper))
                        .map(Node::getProperties, Document::setSignaturePlacementAnnotations))
                .addMappings(m -> m.using(generateSignaturePositionsExtractor(objectMapper, PROPERTY_SIGNATURE_TAGS, PROPERTY_EXTRACTED_SIGNATURE_TAGS))
                        .map(Node::getProperties, Document::setSignatureTags))
                .addMappings(m -> m.using(generateSignaturePositionsExtractor(objectMapper, PROPERTY_SEAL_TAGS, PROPERTY_EXTRACTED_SEAL_TAGS))
                        .map(Node::getProperties, Document::setSealTags));

        modelMapper.typeMap(NodeChildAssociation.class, Document.class)
                .addMappings(m -> m.using(CONTENT_INFO_TO_CONTENT_LENGTH_CONVERTER)
                        .map(NodeChildAssociation::getContent, Document::setContentLength))
                .addMappings(m -> m.using(CONTENT_INFO_TO_MEDIA_TYPE_CONVERTER)
                        .map(NodeChildAssociation::getContent, Document::setMediaType))
                .addMappings(m -> m.using(PROPERTIES_TO_MEDIA_VERSION_EXTRACTOR)
                        .map(NodeChildAssociation::getProperties, Document::setMediaVersion))
                .addMappings(m -> m.using(PROPERTIES_TO_PAGE_COUNT_EXTRACTOR)
                        .map(NodeChildAssociation::getProperties, Document::setPageCount))
                .addMappings(m -> m.using(PROPERTIES_TO_PAGE_INFO_MAP_EXTRACTOR)
                        .map(NodeChildAssociation::getProperties, Document::setPagesProperties))
                .addMappings(m -> m.using(createMapIntegerExtractor(PROPERTY_INDEX, null))
                        .map(NodeChildAssociation::getProperties, Document::setIndex))
                .addMappings(m -> m.using(createMapBooleanExtractor(PROPERTY_DELETABLE))
                        .map(NodeChildAssociation::getProperties, Document::setDeletable))
                .addMappings(m -> m.using(createMapStringExtractor(PROPERTY_CHECKSUM_VALUE))
                        .map(NodeChildAssociation::getProperties, Document::setChecksumValue))
                .addMappings(m -> m.using(createMapBooleanExtractor(PROPERTY_IS_MAIN_DOCUMENT))
                        .map(NodeChildAssociation::getProperties, Document::setMainDocument))
                .addMappings(m -> m.using(createMapBooleanExtractor(PROPERTY_PASSWORD_PRESENCE))
                        .map(NodeChildAssociation::getProperties, Document::setPasswordProtected))
                .addMappings(m -> m.using(generatePropertiesToEmbeddedSignatureInfoExtractor(objectMapper))
                        .map(NodeChildAssociation::getProperties, Document::setEmbeddedSignatureInfos))
                .addMappings(m -> m.using(generatePropertiesToAnnotationsExtractor(objectMapper))
                        .map(NodeChildAssociation::getProperties, Document::setSignaturePlacementAnnotations))
                .addMappings(m -> m.using(generateSignaturePositionsExtractor(objectMapper, PROPERTY_SIGNATURE_TAGS, PROPERTY_EXTRACTED_SIGNATURE_TAGS))
                        .map(NodeChildAssociation::getProperties, Document::setSignatureTags))
                .addMappings(m -> m.using(generateSignaturePositionsExtractor(objectMapper, PROPERTY_SEAL_TAGS, PROPERTY_EXTRACTED_SEAL_TAGS))
                        .map(NodeChildAssociation::getProperties, Document::setSealTags));

        modelMapper.typeMap(AlfrescoNode.class, Document.class)
                .addMappings(m -> m.using(CONTENT_INFO_TO_CONTENT_LENGTH_CONVERTER)
                        .map(AlfrescoNode::getContent, Document::setContentLength))
                .addMappings(m -> m.using(CONTENT_INFO_TO_MEDIA_TYPE_CONVERTER)
                        .map(AlfrescoNode::getContent, Document::setMediaType))
                .addMappings(m -> m.using(PROPERTIES_TO_MEDIA_VERSION_EXTRACTOR)
                        .map(AlfrescoNode::getProperties, Document::setMediaVersion))
                .addMappings(m -> m.using(PROPERTIES_TO_PAGE_COUNT_EXTRACTOR)
                        .map(AlfrescoNode::getProperties, Document::setPageCount))
                .addMappings(m -> m.using(PROPERTIES_TO_PAGE_INFO_MAP_EXTRACTOR)
                        .map(AlfrescoNode::getProperties, Document::setPagesProperties))
                .addMappings(m -> m.using(createMapIntegerExtractor(PROPERTY_INDEX, null))
                        .map(AlfrescoNode::getProperties, Document::setIndex))
                .addMappings(m -> m.using(createMapBooleanExtractor(PROPERTY_DELETABLE))
                        .map(AlfrescoNode::getProperties, Document::setDeletable))
                .addMappings(m -> m.using(createMapStringExtractor(PROPERTY_CHECKSUM_VALUE))
                        .map(AlfrescoNode::getProperties, Document::setChecksumValue))
                .addMappings(m -> m.using(createMapBooleanExtractor(PROPERTY_IS_MAIN_DOCUMENT))
                        .map(AlfrescoNode::getProperties, Document::setMainDocument))
                .addMappings(m -> m.using(createMapBooleanExtractor(PROPERTY_PASSWORD_PRESENCE))
                        .map(AlfrescoNode::getProperties, Document::setPasswordProtected))
                .addMappings(m -> m.using(generatePropertiesToEmbeddedSignatureInfoExtractor(objectMapper))
                        .map(AlfrescoNode::getProperties, Document::setEmbeddedSignatureInfos))
                .addMappings(m -> m.using(generatePropertiesToAnnotationsExtractor(objectMapper))
                        .map(AlfrescoNode::getProperties, Document::setSignaturePlacementAnnotations))
                .addMappings(m -> m.using(generateSignaturePositionsExtractor(objectMapper, PROPERTY_SIGNATURE_TAGS, PROPERTY_EXTRACTED_SIGNATURE_TAGS))
                        .map(AlfrescoNode::getProperties, Document::setSignatureTags))
                .addMappings(m -> m.using(generateSignaturePositionsExtractor(objectMapper, PROPERTY_SEAL_TAGS, PROPERTY_EXTRACTED_SEAL_TAGS))
                        .map(AlfrescoNode::getProperties, Document::setSealTags));

        return modelMapper;
    }


    @PostConstruct
    public void setup() {

        // Proper way...

        URI alfrescoUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP)
                .host(properties.getHost())
                .port(properties.getPort())
                .path("alfresco/api/-default-/public/alfresco/versions/1")
                .build().normalize().toUri();

        apiClient = new ApiClient();
        apiClient.setBasePath(alfrescoUri.toString());
        apiClient.setUsername(properties.getUsername());
        apiClient.setPassword(properties.getPassword());

        // Old way...

        try {
            generateHeaders();
        } catch (WebClientException | IpInternalException e) {
            log.warn("Alfresco is not ready yet...");
        }
    }


    // <editor-fold desc="Utils">


    /**
     * This is only used for the few remaining un-library-ised methods.
     * It should be deleted once the OpenApiGenerator bug will be fixed.
     * <p>
     * TODO: Maybe we could library-ized its inner method... ?
     */
    @Scheduled(fixedDelayString = "${services.content.ticket-delay}")
    protected void generateHeaders() {

        log.debug("Requesting Alfresco ticket ...");

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(TICKET_URL)
                .build().normalize().toUri();

        try {
            sharedWebClientBuilder.build()
                    .post().uri(requestUri)
                    .contentType(APPLICATION_JSON).accept(APPLICATION_JSON)
                    .bodyValue(new AlfrescoTicketRequest(properties.getUsername(), properties.getPassword()))
                    .retrieve()
                    // Result
                    .bodyToMono(new ParameterizedTypeReference<AlfrescoEntry<AlfrescoNode>>() {})
                    .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                    .map(AlfrescoEntry::getEntry)
                    .map(AlfrescoNode::getId)
                    .map(i -> Base64.getEncoder().encodeToString(i.getBytes()))
                    // https://docs.alfresco.com/6.1/concepts/dev-api-by-language-alf-rest-auth-with-repo.html
                    .ifPresent(b -> authHeader = b);
        } catch (IllegalStateException e) {
            // catching timeout and rethrow an exception with a friendlier message
            throw new IpInternalException(
                    "The content service (alfresco) did not respond to header request after 5 seconds, it is probably down or malfunctionning");
        }
    }


    private @NotNull String getAlfrescoHeader() {

        if (StringUtils.isNotEmpty(authHeader)) {
            return authHeader;
        }

        generateHeaders();
        return authHeader;
    }


    private @NotNull String createFolder(@NotNull NodeBodyCreate nodeBodyCreate) {
        return new NodesApi(apiClient)
                .createNode(ROOT_NODE, nodeBodyCreate, true, null, null, null, null)
                .doOnError(e -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_content_service");})
                .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT)
                .map(nodeEntry -> nodeEntry.getEntry().getId())
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_content_service"));
    }


    private @NotNull String createContent(@NotNull String contentId, @NotNull MultipartBodyBuilder builder) {

        // TODO : This could be library-ised, but we'll have to refactor the MultipartBodyBuilder into a NodeCreateRequest.
        //  A little too big for the moment, it will deserve a dedicated MR

        URI createContentRequestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(NODES_URL).pathSegment(contentId).pathSegment(NODE_CHILDREN)
                .build().normalize().toUri();

        return sharedWebClientBuilder.build()
                .post().uri(createContentRequestUri)
                .headers(headers -> headers.setBasicAuth(getAlfrescoHeader()))
                .contentType(MULTIPART_FORM_DATA).accept(APPLICATION_JSON)
                .body(BodyInserters.fromMultipartData(builder.build()))
                .exchangeToMono(clientResponse -> {
                    if (clientResponse.statusCode().isError()) {
                        return clientResponse.bodyToMono(String.class)
                                .doOnNext(errorResponse -> log.debug("createContent - raw error from Alfresco : {}", errorResponse))
                                .flatMap(errorResponse -> Mono.error(new IpInternalException(errorResponse)));
                    } else {
                        return clientResponse.bodyToMono(new ParameterizedTypeReference<AlfrescoEntry<AlfrescoNode>>() {});
                    }
                })
                .doOnError(e -> {
                    log.warn("Error creating content on alfresco : {}", e.getMessage());
                    log.debug("Error details : ", e);
                    throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_content_service");
                })
                .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_parse_content_service_response"))
                .getEntry()
                .getId();
    }


    private @NotNull String updateContent(@NotNull String nodeId,
                                          @NotNull DocumentBuffer documentBuffer,
                                          boolean isFullDocumentSwap) {

        // Update the internal metadata
        // We have to send an empty String to delete the Alfresco property.
        // And we have to delete the internal property to force the Alfresco Transform Extractor to re-extract the changed data.

        Map<String, String> parametersToRemoveMap = Stream.of(
                        DOCUMENT_EXTRACTED_VERSATILE_METADATA.stream(),
                        isFullDocumentSwap ? DOCUMENT_EXTRACTED_FINAL_METADATA.stream() : Stream.<String>empty(),
                        isFullDocumentSwap ? DOCUMENT_USER_METADATA.stream() : Stream.<String>empty()
                )
                .flatMap(Function.identity())
                .collect(toMap(key -> key, key -> EMPTY));

        NodeBodyUpdate nodeBodyUpdate = new NodeBodyUpdate();
        nodeBodyUpdate.setProperties(parametersToRemoveMap);

        new NodesApi(apiClient)
                .updateNode(nodeId, nodeBodyUpdate, emptyList(), emptyList())
                .doOnError(e -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_content_service");})
                .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_content_service"));

        // Actually update the content,
        // This will trigger the Transform Extractors

        String updatedName = isFullDocumentSwap ? documentBuffer.getName() : null;
        try (InputStream inputStream = RequestUtils.bufferToInputStream(documentBuffer)) {
            InputStreamResource resource = new InputStreamResource(inputStream);

            return new NodesApi(apiClient)
                    .updateNodeContent(nodeId, resource, true, null, updatedName, emptyList(), emptyList())
                    .doOnError(e -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_content_service");})
                    .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT)
                    .map(NodeEntry::getEntry)
                    .map(Node::getId)
                    .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_content_service"));

        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_content_service");
        }
    }


    private @NotNull Node getNode(@NotNull String nodeId) {
        return new NodesApi(apiClient)
                .getNode(nodeId, null, null, null)
                .doOnError(e -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_content_service");})
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .map(NodeEntry::getEntry)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_content_service"));
    }


    /**
     * This method cannot be library-ised yet.
     * The standard OpenApiGenerator downloads everything in a temp file, and returns the {@link File} object.
     * This is specific to the WebClient generation, it works well on an older RestTemplate lib.
     *
     * @param nodeId
     * @return
     * @see <a href="https://stackoverflow.com/questions/72142403/useabstractionforfiles-openapi-webclient-not-working">The StackOverflow associate issue</a>
     */
    private @NotNull DocumentBuffer getContent(@NotNull String nodeId) {

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(NODES_URL).pathSegment(nodeId).pathSegment(NODE_CONTENT)
                .queryParam("attachment", true)
                .build().normalize().toUri();

        DocumentBuffer result = new DocumentBuffer();

        Flux<DataBuffer> dataFlux = sharedWebClientBuilder.build()
                .get().uri(requestUri)
                .headers(headers -> headers.setBasicAuth(getAlfrescoHeader()))
                .accept(APPLICATION_OCTET_STREAM)
                .exchangeToFlux(response -> RequestUtils.clientResponseToDataBufferFlux(response, result, nodeId))
                .doOnError(e -> {
                    log.error("Error retrieving node content on alfresco : {}", e.getMessage());
                    throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_content_service");
                });

        result.setContentFlux(dataFlux);

        return result;
    }


    private void deleteContent(@NotNull String nodeId) {
        log.debug("AlfrescoService deleteContent:{}", nodeId);
        new NodesApi(apiClient)
                .deleteNode(nodeId, false)
                .onErrorResume(e -> {
                    if (e instanceof WebClientResponseException wcrException && wcrException.getStatusCode().value() == 404) {
                        log.warn("Delete content that does not exist, skipping...");
                        return Mono.empty();
                    }
                    log.debug("error detail : ", e);
                    log.warn("deleteContent - error while deleting a node");
                    throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_content_service");
                })
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT);
    }


    // </editor-fold desc="Utils">


    @Override
    public @NotNull String updateDocumentInternalProperties(@NotNull Document document) {

        NodeBodyUpdate updateRequest = new NodeBodyUpdate();
        try {
            updateRequest.setProperties(Map.of(
                    PROPERTY_DELETABLE, objectMapper.writeValueAsString(document.isDeletable()),
                    PROPERTY_ANNOTATIONS, objectMapper.writeValueAsString(document.getSignaturePlacementAnnotations()),
                    PROPERTY_SIGNATURE_TAGS, objectMapper.writeValueAsString(document.getSignatureTags()),
                    PROPERTY_SEAL_TAGS, objectMapper.writeValueAsString(document.getSealTags())
            ));
        } catch (JsonProcessingException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.error_writing_request", e);
        }

        return new NodesApi(apiClient)
                .updateNode(document.getId(), updateRequest, emptyList(), emptyList())
                .doOnError(e -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_content_service");})
                .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT)
                .map(NodeEntry::getEntry)
                .map(Node::getId)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_content_service"));
    }


    @Override
    public void removeFirstSignaturePlacementAnnotation(List<Document> mainDocumentList) {
        mainDocumentList
                .stream()
                .filter(doc -> !doc.getSignaturePlacementAnnotations().isEmpty())
                .forEach(doc -> {
                    doc.getSignaturePlacementAnnotations().sort(Comparator.comparingInt(SignaturePlacement::getSignatureNumber));
                    doc.getSignaturePlacementAnnotations().remove(0);
                    updateDocumentInternalProperties(doc);
                });
    }


    @Override
    public @NotNull String createTenant(@NotNull Tenant tenant) {

        String tenantPath = StringSubstitutor.replace(
                DATA_PATH_AUTHORITIES_GROUP,
                Map.of(AUTHORITY_GROUP_PLACEHOLDER, String.valueOf(tenant.getIndex() / 1000))
        );

        NodeBodyCreate request = new NodeBodyCreate();
        request.setName(tenant.getId());
        request.setNodeType(NODE_TYPE_NATIVE_FOLDER);
        request.setRelativePath(tenantPath);
        request.setProperties(emptyMap());

        String newNodeId = createFolder(request);
        log.debug("createTenant response create:{}", newNodeId);
        return newNodeId;
    }


    @Override
    public void deleteTenant(@NotNull Tenant tenant) {
        log.debug("deleteTenant id:{} contentId:{}", tenant.getId(), tenant.getContentId());
        deleteContent(tenant.getContentId());
    }


    @Override
    public @NotNull String createUserData(@NotNull String userId, int userDataGroupIndex) {

        String userDataPath = StringSubstitutor.replace(
                USER_DATA_PATH,
                Map.of(
                        USER_GROUP_PLACEHOLDER, String.valueOf(userDataGroupIndex / 1000),
                        USER_ID_PLACEHOLDER, userId
                ));

        NodeBodyCreate request = new NodeBodyCreate();
        request.setName(userId);
        request.setNodeType(NODE_TYPE_NATIVE_FOLDER);
        request.setRelativePath(userDataPath);
        request.setProperties(emptyMap());

        String newNodeId = createFolder(request);
        log.debug("createUserData response create:{}", newNodeId);
        return newNodeId;
    }


    @Override
    public void deleteUserData(@NotNull String contentId) {
        deleteContent(contentId);
    }


    // <editor-fold desc="Signature image CRUD">


    @Override
    public @NotNull String createUserSignatureImage(@NotNull String userDataContentId, @NotNull DocumentBuffer file) {

        log.debug("createSignatureImage userDataContentId:{} docLength:{}", userDataContentId, file.getContentLength());

        String fileName = StringUtils.firstNonEmpty(file.getName(), "(No name)");

        MultipartBodyBuilder builder = new MultipartBodyBuilder();
        builder.part("name", file.getName());
        builder.part("nodeType", NODE_TYPE_NATIVE_CONTENT);
        builder.part("autoRename", true);

        // For some reason, header seems mandatory for the asyncPart, and not for the other ones.
        // Cheers to this article : https://dev.to/shavz/sending-multipart-form-data-using-spring-webtestclient-2gb7
        builder.asyncPart("filedata", file.getContentFlux(), DataBuffer.class)
                .header(CONTENT_DISPOSITION, "form-data; name=filedata; filename=" + fileName);

        // Upload content

        return createContent(userDataContentId, builder);
    }


    @Override
    public @NotNull String createSealCertificateSignatureImage(@NotNull Tenant tenant, @NotNull String sealCertificateId, @NotNull DocumentBuffer file) {

        log.debug("createSealCertificateSignatureImage certificateId:{} docLength:{}", sealCertificateId, file.getContentLength());

        String fileName = StringUtils.firstNonEmpty(file.getName(), "(No name)");

        MultipartBodyBuilder builder = new MultipartBodyBuilder();
        builder.part("name", file.getName());
        builder.part("nodeType", NODE_TYPE_NATIVE_CONTENT);
        builder.part("relativePath", String.format("seal_certificate_data/%s/signatureImage", sealCertificateId));
        builder.part("autoRename", false);

        // For some reason, header seems mandatory for the asyncPart, and not for the other ones.
        // Cheers to this article : https://dev.to/shavz/sending-multipart-form-data-using-spring-webtestclient-2gb7
        builder.asyncPart("filedata", file.getContentFlux(), DataBuffer.class)
                .header(CONTENT_DISPOSITION, "form-data; name=filedata; filename=" + fileName);

        // Upload content

        return createContent(tenant.getContentId(), builder);
    }


    @Override
    public String updateSignatureImage(@NotNull String imageNodeId, @NotNull DocumentBuffer file) {
        log.debug("updateSignatureImage imageNodeId:{} docLength:{}", imageNodeId, file.getContentLength());
        String nodeId = updateContent(imageNodeId, file, true);
        log.debug("updateDocument done nodeId:{}", nodeId);
        return nodeId;
    }


    @Override
    public @NotNull DocumentBuffer getSignatureImage(@NotNull String signatureNodeId) {
        log.debug("getSignatureImage id:{}", signatureNodeId);
        DocumentBuffer result = getContent(signatureNodeId);
        log.debug("getSignatureImage id:{} length:{}", signatureNodeId, result.getContentLength());
        return result;
    }


    @Override
    public void deleteSignatureImage(@NotNull String signatureNodeId) {
        deleteContent(signatureNodeId);
    }


    // </editor-fold desc="Signature image CRUD">


    @Override
    public @NotNull String createFolder(@NotNull Tenant tenant) {

        NodeBodyCreate request = new NodeBodyCreate();
        request.setName("draft");
        request.setNodeType(NODE_TYPE_IP_FOLDER);
        request.setRelativePath(computePathPlaceholders(tenant));
        request.setProperties(emptyMap());

        log.debug("createFolder request:{}", request);
        String newNodeId = createFolder(request);
        log.debug("createFolder response create:{}", newNodeId);
        return newNodeId;
    }


    @Override
    public @NotNull String createDocument(@NotNull String tenantId, @NotNull Folder folder, @NotNull DocumentBuffer documentBuffer) {

        log.debug("createDocument folderContentId:{} docLength:{}", folder.getContentId(), documentBuffer.getContentLength());

        String fileName = firstNonNull(documentBuffer.getName(), "(No name)");

        MultipartBodyBuilder builder = new MultipartBodyBuilder();
        builder.part("name", fileName);
        builder.part("nodeType", NODE_TYPE_IP_DOCUMENT);
        builder.part("autoRename", true);
        builder.part(PROPERTY_INDEX, documentBuffer.getIndex());
        builder.part(PROPERTY_DELETABLE, true);
        builder.part(PROPERTY_IS_MAIN_DOCUMENT, documentBuffer.isMainDocument());

        // For some reason, header seems mandatory for the asyncPart, and not for the other ones.
        // Cheers to this article : https://dev.to/shavz/sending-multipart-form-data-using-spring-webtestclient-2gb7
        builder.asyncPart("filedata", documentBuffer.getContentFlux(), DataBuffer.class)
                .header(CONTENT_DISPOSITION, "form-data; name=filedata; filename=" + fileName);

        // Upload content

        String documentId = createContent(folder.getContentId(), builder);
        documentBuffer.setId(documentId);

        return documentId;
    }


    @Override
    public @NotNull String createDetachedSignature(@NotNull String folderContentId,
                                                   @NotNull Document targetDocument,
                                                   @NotNull DocumentBuffer documentBuffer,
                                                   @Nullable Task task) {
        log.debug(
                "createSignature folderContentId:{} taskId:{} taskIndex:{} docLength:{}",
                folderContentId,
                Optional.ofNullable(task).map(Task::getId).orElse(null),
                Optional.ofNullable(task).map(Task::getStepIndex).orElse(null),
                documentBuffer.getContentLength());

        String fileName = Optional
                .ofNullable(task)
                .map(t -> String.format(
                        "%s-%d-%s.%s",
                        removeExtension(targetDocument.getName()),
                        (t.getAction() == START) ? 0 : t.getStepIndex() + 1,
                        (t.getAction() == START) ? "signature_externe" : t.getUser().getFirstName() + " " + t.getUser().getLastName(),
                        FileUtils.isXml(documentBuffer.getMediaType()) ? "xml" : "p7s"
                ))
                .orElse(documentBuffer.getName());

        MultipartBodyBuilder builder = new MultipartBodyBuilder();
        builder.part("name", fileName);
        builder.part("nodeType", NODE_TYPE_IP_DETACHED_SIGNATURE);
        builder.part("autoRename", true);
        builder.part(PROPERTY_TARGET_DOCUMENT_ID, targetDocument.getId());

        Optional.ofNullable(task).map(t -> builder.part(PROPERTY_TARGET_TASK_ID, t.getId()));

        // For some reason, header seems mandatory for the asyncPart, and not for the other ones.
        // Cheers to this article : https://dev.to/shavz/sending-multipart-form-data-using-spring-webtestclient-2gb7
        builder.asyncPart("filedata", documentBuffer.getContentFlux(), DataBuffer.class)
                .header(CONTENT_DISPOSITION, "form-data; name=filedata; filename=" + fileName);

        // Upload content

        return createContent(folderContentId, builder);
    }


    @Override
    public @NotNull String createSignatureProof(@NotNull String folderContentId,
                                                @NotNull DocumentBuffer documentBuffer,
                                                @NotNull Boolean isInternal,
                                                @Nullable String errorMessage,
                                                @Nullable String taskId) {
        log.debug(
                "createSignatureProof folderContentId:{}, docLength:{}",
                folderContentId,
                documentBuffer.getContentLength()
        );

        String extension = ".pdf";
        if (documentBuffer.getMediaType() == APPLICATION_XML) {
            extension = ".xml";
        }

        String docName = documentBuffer.getName();
        log.debug("createSignatureProof - initial docname : {}", docName);
        String currentExt = getExtension(docName);
        if (StringUtils.isEmpty(currentExt)) {
            docName = docName + extension;
        }

        log.debug("createSignatureProof - final docName : {}", docName);

        MultipartBodyBuilder builder = new MultipartBodyBuilder();
        builder.part("name", docName);
        builder.part("nodeType", NODE_TYPE_IP_SIGNATURE_PROOF);
        builder.part("autoRename", true);
        if (taskId != null) {
            builder.part(PROPERTY_TASK_ID, taskId);
        }
        builder.part(PROPERTY_IS_MAIN_DOCUMENT, false);
        builder.part(PROPERTY_IS_INTERNAL, isInternal);
        if (errorMessage != null) {
            builder.part(PROPERTY_ERROR_MESSAGE, errorMessage);
        }

        builder.asyncPart("filedata", documentBuffer.getContentFlux(), DataBuffer.class)
                .header(CONTENT_DISPOSITION, "form-data; name=filedata; filename=" + documentBuffer.getName() + extension);

        return createContent(folderContentId, builder);
    }


    @Override
    public @NotNull String createPremisDocument(@NotNull Folder folder, @NotNull String premis) {
        log.debug("createPremisDocument folderContentId:{} docLength:{}", folder.getContentId(), premis.length());

        // delete any previously stored premis

        List<NodeChildAssociation> premisNodeList = getPremisNodesFromFolder(folder);
        premisNodeList.forEach(node -> {
            log.warn("Found a pre-existing premis node for folder {}, deleting it", folder.getName());
            deleteContent(node.getId());
        });

        // Create content

        MultipartBodyBuilder builder = new MultipartBodyBuilder();
        builder.part("name", PREMIS_NODE_FILE_NAME);
        builder.part("nodeType", NODE_TYPE_NATIVE_CONTENT);
        builder.part("autoRename", true);

        // For some reason, header seems mandatory for the asyncPart, and not for the other ones.
        // Cheers to this article : https://dev.to/shavz/sending-multipart-form-data-using-spring-webtestclient-2gb7
        builder.part("filedata", premis)
                .header(CONTENT_DISPOSITION, "form-data; name=filedata; filename=" + PREMIS_NODE_FILE_NAME);

        String premisNodeId = createContent(folder.getContentId(), builder);
        log.debug("createPremisDocument nodeId:{}", premisNodeId);
        return premisNodeId;
    }


    private @NotNull List<NodeChildAssociation> getPremisNodesFromFolder(@NotNull Folder folder) {
        return new NodesApi(apiClient)
                .listNodeChildren(
                        folder.getContentId(),
                        0,
                        MAX_PAGE_SIZE,
                        null,
                        String.format("(nodeType='%s')", NODE_TYPE_NATIVE_CONTENT),
                        List.of(PROPERTIES_KEY),
                        null,
                        false,
                        List.of("nodeType", "name", "id")
                )
                .doOnError(e -> {
                    log.error("getPremisNodesFromFolder - listNodeChildren error: {}", e.getMessage());
                    log.trace("getPremisNodesFromFolder - listNodeChildren error details:", e);
                    throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_content_service");
                })
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .map(NodeChildAssociationPaging::getList)
                .map(NodeChildAssociationPagingList::getEntries)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_content_service"))
                .stream()
                .map(NodeChildAssociationEntry::getEntry)
                .filter(nodeChildAssoc -> StringUtils.equals(nodeChildAssoc.getName(), PREMIS_NODE_FILE_NAME))
                .toList();
    }


    // <editor-fold desc="Document CRUDL">


    @Override
    public void populateFolderWithAllDocumentTypes(@NotNull Folder folder) {

        log.debug("populateFolderWithAllDocumentTypes - folder.getContentId() : {}", folder.getContentId());

        if (StringUtils.isEmpty(folder.getContentId())) {
            throw new RuntimeException("The content id should not be empty");
        }

        List<Document> documentList = Optional.of(getDocumentList(folder.getContentId()))
                .filter(CollectionUtils::isNotEmpty)
                .orElseThrow(() -> {
                    log.error("No document found for Folder: {}", folder.getName());
                    return new LocalizedStatusException(NOT_FOUND, "message.unknown_document_id");
                });

        folder.setDocumentList(documentList.stream()
                // Prune the subclasses additional parameters here
                .map(document -> returnRedactedResponse(alfrescoModelMapper, document, Document::new))
                .collect(toMutableList())
        );
        log.debug("getFolder documentList:{}", folder.getDocumentList());

        folder.setSignatureProofList(getSignatureProofList(folder.getContentId()));
        log.debug("getFolder signatureProofList:{}", folder.getSignatureProofList());
    }


    /**
     * TODO : the 3 requests in a single one ?
     *
     * @param folderContentId
     * @return
     */
    @Override
    public @NotNull List<Document> getDocumentList(@NotNull String folderContentId) {

        List<Document> documentList = new NodesApi(apiClient)
                .listNodeChildren(
                        folderContentId,
                        0,
                        MAX_PAGE_SIZE,
                        null,
                        String.format("(nodeType='%s')", NODE_TYPE_IP_DOCUMENT),
                        List.of(PROPERTIES_KEY),
                        null,
                        false,
                        List.of("nodeType", "name", "id", "content")
                )
                .doOnError(e -> {
                    log.error("listNodeChildren error: {}", e.getMessage());
                    log.trace("listNodeChildren error details:", e);
                    throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_content_service");
                })
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .map(NodeChildAssociationPaging::getList)
                .map(NodeChildAssociationPagingList::getEntries)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_content_service"))
                .stream()
                .map(NodeChildAssociationEntry::getEntry)
                .map(nodeChildAssoc -> {
                    log.debug("getDocumentList - browsing node :  {}", nodeChildAssoc);
                    Document document = alfrescoModelMapper.map(nodeChildAssoc, Document.class);
                    Map<String, String> pro = (HashMap<String, String>) nodeChildAssoc.getProperties();
                    if (pro.get(PROPERTY_SIGNATURES) != null) {
                        try {
                            List<ValidatedSignatureInformation> signatureInfos = Arrays.stream(objectMapper.readValue(pro.get(PROPERTY_SIGNATURES), ValidatedSignatureInformation[].class))
                                    .toList();
                            document.setEmbeddedSignatureInfos(signatureInfos);
                        } catch (JsonProcessingException e) {
                            log.error("getDocumentList error: {}", e.getMessage());
                            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_parse_signatue_properties");
                        }
                    }
                    return document;
                })
                .toList();

        List<DetachedSignature> signatureList = new NodesApi(apiClient)
                .listNodeChildren(
                        folderContentId,
                        0,
                        MAX_PAGE_SIZE,
                        null,
                        String.format("(nodeType='%s')", NODE_TYPE_IP_DETACHED_SIGNATURE),
                        List.of(PROPERTIES_KEY),
                        null,
                        false,
                        List.of("nodeType", "name", "id", "content")
                )
                .doOnError(e -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_content_service");})
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .map(NodeChildAssociationPaging::getList)
                .map(NodeChildAssociationPagingList::getEntries)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_content_service"))
                .stream()
                .map(NodeChildAssociationEntry::getEntry)
                .map(nodeChildAssociation -> {
                    try {
                        DetachedSignature detachedSignature = new DetachedSignature(nodeChildAssociation);
                        Map<String, String> pro = (HashMap<String, String>) nodeChildAssociation.getProperties();
                        if (pro.get(PROPERTY_DETACHED_SIGNATURE) != null) {
                            ValidatedSignatureInformation signatureInfo = objectMapper.readValue(pro.get(PROPERTY_DETACHED_SIGNATURE), ValidatedSignatureInformation.class);
                            detachedSignature.setSignatureInfo(signatureInfo);
                        }
                        return detachedSignature;
                    } catch (JsonProcessingException e) {
                        log.error("getDocumentList error: {}", e.getMessage());
                        throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_parse_signatue_properties");
                    }
                })
                .toList();

        List<NodeChildAssociation> visualPdfList = new NodesApi(apiClient)
                .listNodeChildren(
                        folderContentId,
                        0,
                        MAX_PAGE_SIZE,
                        null,
                        String.format("(nodeType='%s')", NODE_TYPE_IP_PDF_VISUAL),
                        List.of(PROPERTIES_KEY),
                        null,
                        false,
                        List.of("nodeType", "name", "id", "content")
                )
                .doOnError(e -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_content_service");})
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .map(NodeChildAssociationPaging::getList)
                .map(NodeChildAssociationPagingList::getEntries)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_content_service"))
                .stream()
                .map(NodeChildAssociationEntry::getEntry)
                .toList();

        // Merge results

        documentList = new ArrayList<>(documentList); // Mutable copy
        documentList.sort(comparingInt(Document::getIndex));

        documentList.forEach(d -> d.setDetachedSignatures(
                signatureList.stream()
                        .filter(n -> StringUtils.equals(n.getTargetDocumentId(), d.getId()))
                        .sorted(DETACHED_SIGNATURE_COMPARATOR)
                        .toList()
        ));

        documentList.forEach(d -> d.setPdfVisualId(
                visualPdfList.stream()
                        .filter(v -> {
                            //noinspection unchecked
                            HashMap<String, Object> visualPdfProperties = (HashMap<String, Object>) v.getProperties();
                            String sourceDocumentId = MapUtils.getString(visualPdfProperties, PROPERTY_SOURCE_DOCUMENT_ID, null);
                            return StringUtils.equals(sourceDocumentId, d.getId());
                        })
                        .map(NodeChildAssociation::getId)
                        .findFirst()
                        .orElse(null))
        );

        return documentList;
    }


    @Override
    public @NotNull SignatureProof getSignatureProof(@NotNull String documentId) throws IOException {
        Node node = getNode(documentId);
        SignatureProof signatureProof = alfrescoModelMapper.map(node, SignatureProof.class);
        String nodePropertiesString = objectMapper.writeValueAsString(node.getProperties());
        Map<String, Object> entryProperties = objectMapper.readValue(nodePropertiesString, new TypeReference<>() {});
        setSignatureProof(signatureProof, entryProperties, node.getContent());
        return signatureProof;
    }


    @Override
    public @NotNull List<SignatureProof> getSignatureProofList(@NotNull String folderContentId) {
        return new NodesApi(apiClient)
                .listNodeChildren(
                        folderContentId,
                        0,
                        MAX_SIGNATURE_PROOF_FILE_PER_FOLDER,
                        null,
                        String.format("(nodeType='%s')", NODE_TYPE_IP_SIGNATURE_PROOF),
                        List.of(PROPERTIES_KEY),
                        null,
                        false,
                        List.of("nodeType", "name", "id", "content")
                )
                .doOnError(e -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_content_service");})
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .map(NodeChildAssociationPaging::getList)
                .map(NodeChildAssociationPagingList::getEntries)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_content_service"))
                .stream()
                .map(NodeChildAssociationEntry::getEntry)
                .map(entry -> {
                    SignatureProof signatureProof = alfrescoModelMapper.map(entry, SignatureProof.class);
                    //noinspection unchecked
                    Map<String, Object> entryProperties = (Map<String, Object>) entry.getProperties();
                    setSignatureProof(signatureProof, entryProperties, entry.getContent());
                    return signatureProof;
                })
                .toList();
    }


    private void setSignatureProof(SignatureProof signatureProof, Map<String, Object> entryProperties, ContentInfo content) {
        signatureProof.setProofTargetTaskId(MapUtils.getString(entryProperties, PROPERTY_TASK_ID));
        signatureProof.setInternal(Objects.equals(MapUtils.getString(entryProperties, PROPERTY_IS_INTERNAL), "true"));
        signatureProof.setSignatureProof(true);
        String entryMediaType = Optional.ofNullable(content)
                .map(ContentInfo::getMimeType)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.missing_mediatype", signatureProof.getName()));
        signatureProof.setMediaType(MediaType.parseMediaType(entryMediaType));
        signatureProof.setErrorMessage(MapUtils.getString(entryProperties, PROPERTY_ERROR_MESSAGE));
    }


    @Override
    public @NotNull Document getDocumentInfo(@NotNull String documentId) {
        Node node = getNode(documentId);
        return alfrescoModelMapper.map(node, Document.class);
    }


    @Override
    public void updateDocument(@NotNull String documentId, @NotNull DocumentBuffer documentBuffer, boolean isFullDocumentSwap) {
        log.debug("updateDocument id:{} length:{}", documentId, documentBuffer.getContentLength());
        String nodeId = updateContent(documentId, documentBuffer, isFullDocumentSwap);
        log.debug("updateDocument done nodeId:{}", nodeId);
    }


    // </editor-fold desc="Document CRUDL">


    @Override
    public void updateEmbeddedSignatureInformation(@NotNull String nodeId,
                                                   @NotNull List<ValidatedSignatureInformation> signatureInfos) throws JsonProcessingException {

        Node node = getNode(nodeId);
        HashMap<String, Object> properties = (HashMap<String, Object>) node.getProperties();
        if (properties == null) {
            throw new ExternalException("updateSignatureInformation() node properties are null");
        }
        String ext = properties.get(PROPERTY_SIGNATURES).toString();
        JsonNode rootNode = objectMapper.readTree(ext);

        rootNode.forEach(infoNode -> signatureInfos.stream()
                .filter(signatureInfo -> infoNode.get("signatureDateTime").asLong() == signatureInfo.getSignatureDateTime())
                .forEach(signatureInfo -> {
                    ObjectNode objectInfo = (ObjectNode) infoNode;
                    try {
                        String signatureInfoJson = objectMapper.writeValueAsString(signatureInfo);
                        JsonNode signatureInfoNode = objectMapper.readTree(signatureInfoJson);

                        signatureInfoNode.fields().forEachRemaining(field -> objectInfo.set(field.getKey(), field.getValue()));
                    } catch (JsonProcessingException e) {
                        throw new ExternalException(e.getMessage());
                    }
                }));

        String updatedProperties = rootNode.toString();
        NodeBodyUpdate updateRequest = new NodeBodyUpdate();
        updateRequest.setProperties(Map.of(PROPERTY_SIGNATURES, updatedProperties));

        new NodesApi(apiClient)
                .updateNode(nodeId, updateRequest, emptyList(), emptyList())
                .doOnError(e -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_content_service");})
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_content_service"));
    }


    @Override
    public void updateDetachedSignatureInformation(@NotNull String detachedSignatureId,
                                                   @NotNull ValidatedSignatureInformation signatureInfo) throws JsonProcessingException {
        Node node = getNode(detachedSignatureId);
        HashMap<String, Object> properties = (HashMap<String, Object>) node.getProperties();
        if (properties == null) {
            throw new ExternalException("updateSignatureInformation() node properties are null");
        }

        properties.computeIfAbsent(PROPERTY_DETACHED_SIGNATURE, k -> new HashMap<>());

        String ext = properties.get(PROPERTY_DETACHED_SIGNATURE).toString();
        JsonNode rootNode;

        if (ext.isEmpty()) {
            rootNode = objectMapper.createObjectNode();
        } else {
            rootNode = objectMapper.readTree(ext);
        }

        ObjectNode objectInfo = (ObjectNode) rootNode;
        String signatureInfoJson = objectMapper.writeValueAsString(signatureInfo);
        JsonNode signatureInfoNode = objectMapper.readTree(signatureInfoJson);
        signatureInfoNode.fields().forEachRemaining(field -> objectInfo.set(field.getKey(), field.getValue()));

        String updatedProperties = rootNode.toString();
        NodeBodyUpdate updateRequest = new NodeBodyUpdate();
        updateRequest.setProperties(Map.of(PROPERTY_DETACHED_SIGNATURE, updatedProperties));

        new NodesApi(apiClient)
                .updateNode(detachedSignatureId, updateRequest, emptyList(), emptyList())
                .doOnError(e -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_content_service");})
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_content_service"));
    }


    @Override
    public @NotNull DocumentBuffer retrieveContent(@NotNull String contentId) {
        log.debug("retrieveContent id:{}", contentId);
        DocumentBuffer result = getContent(contentId);
        log.debug("retrieveContent id:{} length:{}", contentId, result.getContentLength());
        return result;
    }


    /**
     * This method cannot be library-ised yet.
     * The standard OpenApiGenerator downloads everything in a temp file, and returns the {@link File} object.
     * This is specific to the WebClient generation, it works well on an older RestTemplate lib.
     *
     * @param documentId
     * @return
     * @see <a href="https://stackoverflow.com/questions/72142403/useabstractionforfiles-openapi-webclient-not-working">The StackOverflow associate issue</a>
     */
    @Override
    public @NotNull InputStream retrievePipedDocument(@NotNull String documentId) {

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(NODES_URL).pathSegment(documentId).pathSegment(NODE_CONTENT)
                .queryParam("attachment", false)
                .build().normalize().toUri();

        Flux<DataBuffer> bodyDataFlux = sharedWebClientBuilder.build()
                .get().uri(requestUri)
                .headers(headers -> headers.setBasicAuth(getAlfrescoHeader()))
                .accept(APPLICATION_OCTET_STREAM)
                .exchangeToFlux(RequestUtils::clientResponseToRawDataBufferFlux);

        try {
            return RequestUtils.pipeFluxAsInputStream(bodyDataFlux);
        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_content_service");
        }
    }


    @Override
    public void deleteFolder(@NotNull Folder folder) {
        log.debug("deleteFolder id:{} contentId:{}", folder.getId(), folder.getContentId());

        Optional.ofNullable(folder.getContentId())
                .ifPresent(this::deleteContent);
    }


    @Override
    public void deleteDocument(@NotNull String documentId) {
        deleteContent(documentId);
    }


    @Override
    public @NotNull String createPdfVisual(@NotNull Folder folder, @NotNull String targetDocument, @NotNull DocumentBuffer documentBuffer) {

        log.debug("createPdfVisual folderContentId:{} docLength:{}", folder.getContentId(), documentBuffer.getContentLength());

        String fileName = firstNonNull(documentBuffer.getName(), "(No name)");

        MultipartBodyBuilder builder = new MultipartBodyBuilder();
        builder.part("name", fileName);
        builder.part("nodeType", NODE_TYPE_IP_PDF_VISUAL);
        builder.part("autoRename", true);
        builder.part(PROPERTY_SOURCE_DOCUMENT_ID, targetDocument);

        // For some reason, header seems mandatory for the asyncPart, and not for the other ones.
        // Cheers to this article : https://dev.to/shavz/sending-multipart-form-data-using-spring-webtestclient-2gb7
        builder.asyncPart("filedata", documentBuffer.getContentFlux(), DataBuffer.class)
                .header(CONTENT_DISPOSITION, "form-data; name=filedata; filename=" + fileName);

        // Upload content

        return createContent(folder.getContentId(), builder);
    }


    // <editor-fold desc="Templates CRUD">


    @Override
    public @NotNull String createCustomTemplate(@NotNull Tenant tenant, @NotNull TemplateType templateType, @NotNull String templateValue) {

        // This file name is not used, but Alfresco demands a real one, with a proper extension...
        String fileName = templateType.name().toLowerCase(ROOT) + ".ftl";

        MultipartBodyBuilder builder = new MultipartBodyBuilder();
        builder.part("name", fileName);
        builder.part("nodeType", NODE_TYPE_NATIVE_CONTENT);
        builder.part("relativePath", "templates");
        builder.part("autoRename", false);
        builder.part("filedata", templateValue).header(CONTENT_DISPOSITION, "form-data; name=filedata; filename=" + fileName);

        String newNodeId = createContent(tenant.getContentId(), builder);
        log.debug("createCustomTemplate response create:{}", newNodeId);
        return newNodeId;
    }


    @Override
    public void editCustomTemplate(@NotNull Tenant tenant, @NotNull TemplateType templateType, @NotNull String templateValue) {

        String nodeId = Optional.ofNullable(tenant.getCustomTemplates().get(templateType))
                .orElseThrow(() -> {
                    log.error("No custom template of type {} exist on tenant {}", templateType.name(), tenant.getName());
                    return new LocalizedStatusException(NOT_FOUND, "message.missing_template");
                });

        // Note, the DataBufferUtils closes the given stream
        ByteArrayInputStream bais = new ByteArrayInputStream(templateValue.getBytes(UTF_8));
        DocumentBuffer documentBuffer = new DocumentBuffer();
        documentBuffer.setName("%s.ftl".formatted(templateType));
        documentBuffer.setContentFlux(readInputStream(() -> bais, new DefaultDataBufferFactory(), FILE_TRANSFER_BUFFER_SIZE));

        updateContent(nodeId, documentBuffer, true);
    }


    @Override
    public @NotNull DocumentBuffer getCustomTemplate(@NotNull Tenant tenant, @NotNull TemplateType templateType) {

        String nodeId = Optional.ofNullable(tenant.getCustomTemplates().get(templateType))
                .orElseThrow(() -> {
                    log.error("No custom template of type {} exist on tenant {}", templateType.name(), tenant.getName());
                    return new LocalizedStatusException(NOT_FOUND, "message.missing_template");
                });

        return getContent(nodeId);
    }


    @Override
    public void deleteCustomTemplate(@NotNull Tenant tenant, @NotNull TemplateType templateType) {

        String nodeId = Optional.ofNullable(tenant.getCustomTemplates().get(templateType))
                .orElseThrow(() -> {
                    log.error("No custom template of type {} exist on tenant {}", templateType.name(), tenant.getName());
                    return new LocalizedStatusException(NOT_FOUND, "message.missing_template");
                });

        deleteContent(nodeId);
    }


    // </editor-fold desc="Templates CRUD">


    // <editor-fold desc="Layer CRUD">


    @Override
    public @NotNull String createLayerImage(@NotNull Tenant tenant, @NotNull Layer layer, @NotNull DocumentBuffer file) {

        log.debug("createLayerImage certificateId:{} docLength:{}", layer.getId(), file.getContentLength());

        String fileName = StringUtils.firstNonEmpty(file.getName(), "(No name)");

        MultipartBodyBuilder builder = new MultipartBodyBuilder();
        builder.part("name", file.getName());
        builder.part("nodeType", NODE_TYPE_NATIVE_CONTENT);
        builder.part("relativePath", String.format("layer_data/%s/", layer.getId()));
        builder.part("autoRename", true);

        // For some reason, header seems mandatory for the asyncPart, and not for the other ones.
        // Cheers to this article : https://dev.to/shavz/sending-multipart-form-data-using-spring-webtestclient-2gb7
        builder.asyncPart("filedata", file.getContentFlux(), DataBuffer.class)
                .header(CONTENT_DISPOSITION, "form-data; name=filedata; filename=" + fileName);

        // Upload content

        return createContent(tenant.getContentId(), builder);
    }


    @Override
    public void deleteLayerImage(@NotNull String layerImageId) {
        deleteContent(layerImageId);
    }


    // </editor-fold desc="Layer CRUD">


}
