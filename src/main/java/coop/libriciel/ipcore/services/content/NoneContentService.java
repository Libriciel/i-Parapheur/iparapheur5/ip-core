/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.content;

import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.content.SignatureProof;
import coop.libriciel.ipcore.model.content.ValidatedSignatureInformation;
import coop.libriciel.ipcore.model.database.TemplateType;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.pdfstamp.Layer;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.io.Serial;
import java.util.List;

import static coop.libriciel.ipcore.utils.TextUtils.NONE_SERVICE;
import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;


@Service(ContentServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = ContentServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = NONE_SERVICE)
public class NoneContentService implements ContentServiceInterface {


    private static class NoContentServiceError extends LocalizedStatusException {

        private static final @Serial long serialVersionUID = -3551445318972142139L;


        public NoContentServiceError() {
            super(SERVICE_UNAVAILABLE, "message.content_service_not_available");
        }

    }


    @Override
    public @NotNull String createTenant(@NotNull Tenant tenant) {
        throw new NoContentServiceError();
    }


    @Override
    public void deleteTenant(@NotNull Tenant tenant) {
        throw new NoContentServiceError();
    }


    @Override
    public @NotNull String createUserData(@NotNull String userId, int userDataGroupIndex) {
        throw new NoContentServiceError();
    }


    @Override
    public void deleteUserData(@NotNull String dataGroupNodeId) {
        throw new NoContentServiceError();
    }


    @Override
    public @NotNull String createUserSignatureImage(@NotNull String userDataContentId, @NotNull DocumentBuffer file) {
        throw new NoContentServiceError();
    }


    @Override
    public @NotNull String createSealCertificateSignatureImage(@NotNull Tenant tenant, @NotNull String sealCertificateId, @NotNull DocumentBuffer file) {
        throw new NoContentServiceError();
    }


    @Override
    public @NotNull DocumentBuffer getSignatureImage(@NotNull String signatureNodeId) {
        throw new NoContentServiceError();
    }


    @Override
    public String updateSignatureImage(@NotNull String imageNodeId, @NotNull DocumentBuffer file) {
        throw new NoContentServiceError();
    }


    @Override
    public void deleteSignatureImage(@NotNull String signatureNodeId) {
        throw new NoContentServiceError();
    }


    @Override
    public @NotNull String createFolder(@NotNull Tenant tenant) {
        throw new NoContentServiceError();
    }


    @Override
    public void deleteFolder(@NotNull Folder folder) {
        throw new NoContentServiceError();
    }


    @Override
    public @NotNull String createPremisDocument(@NotNull Folder folder, @NotNull String premis) {
        throw new NoContentServiceError();
    }


    @Override
    public void updateEmbeddedSignatureInformation(@NotNull String nodeId, @NotNull List<ValidatedSignatureInformation> signatureInfos) {
        throw new NoContentServiceError();
    }


    @Override
    public void updateDetachedSignatureInformation(@NotNull String detachedSignatureId, @NotNull ValidatedSignatureInformation signatureInfo) {
        throw new NoContentServiceError();
    }


    @Override
    public String updateDocumentInternalProperties(@NotNull Document document) {
        throw new NoContentServiceError();
    }


    @Override
    public void removeFirstSignaturePlacementAnnotation(List<Document> mainDocumentList) {
        throw new NoContentServiceError();
    }


    @Override
    public @NotNull String createDocument(@NotNull String tenantId, @NotNull Folder folder, @NotNull DocumentBuffer file) {
        throw new NoContentServiceError();
    }


    @Override
    public @NotNull List<SignatureProof> getSignatureProofList(@NotNull String folderContentId) {
        throw new NoContentServiceError();
    }


    @Override
    public @NotNull Document getDocumentInfo(@NotNull String documentId) {
        throw new NoContentServiceError();
    }


    @Override
    public @NotNull SignatureProof getSignatureProof(@NotNull String documentId) {
        throw new NoContentServiceError();
    }


    @Override
    public void updateDocument(@NotNull String documentId, @NotNull DocumentBuffer documentBuffer, boolean isFullDocumentSwap) {
        throw new NoContentServiceError();
    }


    @Override
    public void deleteDocument(@NotNull String documentId) {
        throw new NoContentServiceError();
    }


    @Override
    public void populateFolderWithAllDocumentTypes(@NotNull Folder folder) {
        throw new NoContentServiceError();
    }


    @Override
    public @NotNull List<Document> getDocumentList(@NotNull String folderContentId) {
        throw new NoContentServiceError();
    }


    @Override
    public @NotNull DocumentBuffer retrieveContent(@NotNull String documentId) {
        throw new NoContentServiceError();
    }


    @Override
    public @NotNull InputStream retrievePipedDocument(@NotNull String documentId) {
        throw new NoContentServiceError();
    }


    @Override
    public @NotNull String createDetachedSignature(@NotNull String folderContentId,
                                                   @NotNull Document targetDocument,
                                                   @NotNull DocumentBuffer file,
                                                   @Nullable Task task) {
        throw new NoContentServiceError();
    }


    @Override
    public @NotNull String createSignatureProof(@NotNull String folderContentId,
                                                @NotNull DocumentBuffer documentBuffer,
                                                @NotNull Boolean isInternal,
                                                @Nullable String errorMessage,
                                                @Nullable String taskId) {
        throw new NoContentServiceError();
    }


    @Override
    public @NotNull String createPdfVisual(@NotNull Folder folder, @NotNull String targetDocument, @NotNull DocumentBuffer documentBuffer) {
        throw new NoContentServiceError();
    }


    @Override
    public @NotNull String createCustomTemplate(@NotNull Tenant tenant, @NotNull TemplateType templateType, @NotNull String templateValue) {
        throw new NoContentServiceError();
    }


    @Override
    public void editCustomTemplate(@NotNull Tenant tenant, @NotNull TemplateType templateType, @NotNull String templateValue) {
        throw new NoContentServiceError();
    }


    @Override
    public @NotNull DocumentBuffer getCustomTemplate(@NotNull Tenant tenant, @NotNull TemplateType templateType) {
        throw new NoContentServiceError();
    }


    @Override
    public void deleteCustomTemplate(@NotNull Tenant tenant, @NotNull TemplateType templateType) {
        throw new NoContentServiceError();
    }


    @Override
    public @NotNull String createLayerImage(@NotNull Tenant tenant, @NotNull Layer layer, @NotNull DocumentBuffer file) {
        throw new NoContentServiceError();
    }


    @Override
    public void deleteLayerImage(@NotNull String layerImageId) {
        throw new NoContentServiceError();
    }


}
