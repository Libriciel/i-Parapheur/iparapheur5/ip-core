/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.websocket;

import coop.libriciel.ipcore.business.auth.DeskBusinessService;
import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.websockets.DeskCount;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import lombok.extern.log4j.Log4j2;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Log4j2
@Service
public class WebsocketService {

    private final AuthServiceInterface authService;
    private final SimpMessagingTemplate simpMessagingTemplate;
    private final DeskBusinessService deskBusinessService;


    public WebsocketService(AuthServiceInterface authService, SimpMessagingTemplate simpMessagingTemplate, DeskBusinessService deskBusinessService) {
        this.authService = authService;
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.deskBusinessService = deskBusinessService;
    }


    public void sendNewCountsForDesk(String tenantId, String deskId) {

        Desk desk = authService.findDeskByIdNoException(tenantId, deskId);

        if (desk == null) {
            return;
        }

        // mandatory to obtain consistent delegation count
        deskBusinessService.populateCountsAndDelegationNoAuth(tenantId, Collections.singletonMap(desk.getId(), desk), Collections.singletonList(desk));
        DeskCount deskCountNotification = deskBusinessService.getDeskCount(desk);

        simpMessagingTemplate.convertAndSend("/websocket/tenant/" + tenantId + "/desk/" + deskId + "/folder/watch", deskCountNotification);
    }
}
