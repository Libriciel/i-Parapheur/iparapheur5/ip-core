/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.workflow;

import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.TypologyEntity;
import coop.libriciel.ipcore.model.database.userPreferences.FolderFilter;
import coop.libriciel.ipcore.model.database.userPreferences.FolderFilterParam;
import coop.libriciel.ipcore.model.ipng.IpngProof;
import coop.libriciel.ipcore.model.ipng.IpngProofWrap;
import coop.libriciel.ipcore.model.ipng.PendingIpngFolder;
import coop.libriciel.ipcore.model.workflow.*;
import coop.libriciel.ipcore.model.workflow.Task.ExternalState;
import coop.libriciel.ipcore.model.workflow.ipWorkflow.IpWorkflowInstance;
import coop.libriciel.ipcore.model.workflow.ipWorkflow.IpWorkflowTask;
import coop.libriciel.ipcore.model.workflow.requests.FilteredRequest;
import coop.libriciel.ipcore.model.workflow.requests.IntResponse;
import coop.libriciel.ipcore.model.workflow.requests.SimpleTaskParams;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.ipng.PendingIpngFolderRepository;
import coop.libriciel.ipcore.utils.*;
import coop.libriciel.ipcore.utils.exceptions.LocalizedNotFoundStatusException;
import coop.libriciel.workflow.api.ApiClient;
import coop.libriciel.workflow.api.client.DefinitionApi;
import coop.libriciel.workflow.api.client.InstanceApi;
import coop.libriciel.workflow.api.client.TaskApi;
import coop.libriciel.workflow.api.model.Instance;
import coop.libriciel.workflow.api.model.PerformTaskRequest;
import jakarta.annotation.PostConstruct;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONStringer;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.net.URLEncoder;
import java.security.InvalidParameterException;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static coop.libriciel.ipcore.configuration.ModelMapperConfigurer.*;
import static coop.libriciel.ipcore.model.workflow.Action.*;
import static coop.libriciel.ipcore.model.workflow.FolderSortBy.*;
import static coop.libriciel.ipcore.model.workflow.State.*;
import static coop.libriciel.ipcore.model.workflow.WorkflowDefinition.*;
import static coop.libriciel.ipcore.utils.CollectionUtils.toMutableList;
import static coop.libriciel.ipcore.utils.RequestUtils.*;
import static coop.libriciel.ipcore.utils.TextUtils.INTERNAL_PREFIX;
import static coop.libriciel.ipcore.utils.TextUtils.ISO8601_DATE_TIME_FORMAT;
import static java.lang.Boolean.TRUE;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.time.ZoneId.systemDefault;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.Comparator.*;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import static org.modelmapper.config.Configuration.AccessLevel.PRIVATE;
import static org.modelmapper.convention.MatchingStrategies.STRICT;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.security.config.Elements.HTTP;


@Log4j2
@Service(WorkflowServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = WorkflowServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = "workflow")
public class IpWorkflowService implements WorkflowServiceInterface {


    public static final String METADATA_TRANSACTION_ID = INTERNAL_PREFIX + "transaction_id";
    public static final String METADATA_PASTELL_DOCUMENT_ID = INTERNAL_PREFIX + "pastell_document_id";
    public static final String METADATA_KEY_IPNG_RETURN_DESKBOX_ID = INTERNAL_PREFIX + "ipng_deskbox_profile_id";
    public static final String METADATA_KEY_IPNG_SOURCE_DESKBOX_ID = INTERNAL_PREFIX + "ipng_source_deskbox_id";

    public static final String METADATA_KEY_IPNG_RETURN_BUSINESS_ID = INTERNAL_PREFIX + "ipng_proof_id";
    public static final String METADATA_KEY_IPNG_RETURN_TYPE_ID = INTERNAL_PREFIX + "ipng_type_id";
    public static final String METADATA_KEY_IPNG_RETURN_METADATA_IDS = INTERNAL_PREFIX + "ipng_metadata_ids";
    public static final String IPNG_METADATA_KEYS_SEPARATOR = "|";
    public static final String METADATA_IPNG_BUSINESS_ID = INTERNAL_PREFIX + "ipng_business_id";
    public static final String METADATA_STATUS = INTERNAL_PREFIX + "status";
    public static final String METADATA_PUBLIC_ANNOTATION = INTERNAL_PREFIX + "public_annotation";
    public static final String METADATA_PRIVATE_ANNOTATION = INTERNAL_PREFIX + "private_annotation";
    public static final String METADATA_DRAFT_CREATION_DATE = INTERNAL_PREFIX + "creation_date";
    public static final String METADATA_TARGET_DELEGATE = "workflow_internal_delegate_candidate_groups";
    public static final String METADATA_PUBLIC_CERTIFICATE_BASE64 = INTERNAL_PREFIX + "public_certificate_base64";

    private static final String GROUP_URL = "workflow/group";
    private static final String ARCHIVE_URL = "workflow/archive";
    private static final String INSTANCE_URL = "workflow/instance";
    private static final String TASK_URL = "workflow/task";
    private static final String READ_TASKS = "readTasks";
    private static final String COUNT_PATH = "count";

    static final Converter<Date, OffsetDateTime> DATE_TO_OFFSETDATETIME_CONVERTER = context -> IpWorkflowService.toOffsetDateTime(context.getSource());
    static final Converter<OffsetDateTime, Date> OFFSETDATETIME_TO_DATE_CONVERTER = context -> IpWorkflowService.toDate(context.getSource());
    public static final String WORKFLOW_INTERNAL_VAR_PREFIX = "workflow_internal_";


    private ApiClient apiClient;
    private static ModelMapper ipWorkflowModelMapper;


    // <editor-fold desc="Beans">


    private final AuthServiceInterface authService;
    private final PendingIpngFolderRepository pendingIpngFolderRepository;
    private final WorkflowServiceProperties properties;


    @Autowired
    public IpWorkflowService(AuthServiceInterface authService,
                             PendingIpngFolderRepository pendingIpngFolderRepository,
                             WorkflowServiceProperties properties) {
        this.authService = authService;
        this.pendingIpngFolderRepository = pendingIpngFolderRepository;
        this.properties = properties;
    }


    // </editor-fold desc="Beans">


    @PostConstruct
    public void init() {

        ipWorkflowModelMapper = initializeInternalModelMapper();

        // Proper way...

        URI workflowUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP)
                .host(properties.getHost())
                .port(properties.getPort())
                .path("workflow")
                .build().normalize().toUri();

        apiClient = new ApiClient();
        apiClient.setBasePath(workflowUri.toString());
    }


    /**
     * We don't want to pollute the main ModelMapper with the lib-specific sub-objects.
     * So we confine every setting in here.
     */
    static ModelMapper initializeInternalModelMapper() {

        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration()
                .setFieldMatchingEnabled(true)
                .setFieldAccessLevel(PRIVATE)
                .setMatchingStrategy(STRICT);

        // Global enums

        mapper.addConverter((Converter<coop.libriciel.workflow.api.model.State, State>) context -> IpWorkflowService.toCoreValue(context.getSource()));
        mapper.addConverter((Converter<State, coop.libriciel.workflow.api.model.State>) context -> IpWorkflowService.toIpWorkflowValue(context.getSource()));

        mapper.addConverter((Converter<coop.libriciel.workflow.api.model.ExternalState, ExternalState>) c -> IpWorkflowService.toCoreValue(c.getSource()));
        mapper.addConverter((Converter<ExternalState, coop.libriciel.workflow.api.model.ExternalState>) c -> IpWorkflowService.toIpWorkflowValue(c.getSource()));

        mapper.addConverter((Converter<coop.libriciel.workflow.api.model.Action, Action>) context -> IpWorkflowService.toCoreValue(context.getSource()));
        mapper.addConverter((Converter<Action, coop.libriciel.workflow.api.model.Action>) context -> IpWorkflowService.toIpWorkflowValue(context.getSource()));

        mapper.addConverter((Converter<coop.libriciel.workflow.api.model.ParallelType, StepDefinitionParallelType>) c -> IpWorkflowService.toCoreValue(c.getSource()));
        mapper.addConverter((Converter<StepDefinitionParallelType, coop.libriciel.workflow.api.model.ParallelType>) c -> IpWorkflowService.toIpWorkflowValue(c.getSource()));

        // Specific objects

        initializeMapperForFolderFilters(mapper);
        initializeMapperForDefinitions(mapper);
        initializeMapperForInstances(mapper);
        initializeMapperForTasks(mapper);

        return mapper;
    }


    private static void initializeMapperForFolderFilters(@NotNull ModelMapper mapper) {

        mapper.typeMap(FolderFilter.class, FolderFilterParam.class)
                .addMappings(m -> m.using(TYPE_TO_ID_CONVERTER)
                        .map(FolderFilter::getType, FolderFilterParam::setTypeId))
                .addMappings(m -> m.using(SUBTYPE_TO_ID_CONVERTER)
                        .map(FolderFilter::getSubtype, FolderFilterParam::setSubtypeId));

        mapper.typeMap(FolderFilterParam.class, FolderFilter.class)
                .addMappings(m -> m.using(ID_TO_TYPE_CONVERTER)
                        .map(FolderFilterParam::getTypeId, FolderFilter::setType))
                .addMappings(m -> m.using(ID_TO_SUBTYPE_CONVERTER)
                        .map(FolderFilterParam::getSubtypeId, FolderFilter::setSubtype));
    }


    private static void initializeMapperForDefinitions(@NotNull ModelMapper mapper) {

        mapper.typeMap(StepDefinition.class, coop.libriciel.workflow.api.model.StepDefinition.class)
                .addMappings(m -> m.using(METADATA_TO_ID_NULLABLE_LIST_CONVERTER)
                        .map(StepDefinition::getMandatoryValidationMetadata,
                                coop.libriciel.workflow.api.model.StepDefinition::setMandatoryValidationMetadataIds))
                .addMappings(m -> m.using(METADATA_TO_ID_NULLABLE_LIST_CONVERTER)
                        .map(StepDefinition::getMandatoryRejectionMetadata, coop.libriciel.workflow.api.model.StepDefinition::setMandatoryRejectionMetadataIds))
                .addMappings(m -> m.using(DESK_REPRESENTATION_TO_ID_NULLABLE_LIST_CONVERTER)
                        .map(StepDefinition::getValidatingDesks, coop.libriciel.workflow.api.model.StepDefinition::setValidatingDeskIds))
                .addMappings(m -> m.using(DESK_REPRESENTATION_TO_ID_NULLABLE_LIST_CONVERTER)
                        .map(StepDefinition::getNotifiedDesks, coop.libriciel.workflow.api.model.StepDefinition::setNotifiedDeskIds));

        mapper.typeMap(coop.libriciel.workflow.api.model.StepDefinition.class, StepDefinition.class)
                .addMappings(m -> m.using(ID_TO_METADATA_NULLABLE_LIST_CONVERTER)
                        .map(coop.libriciel.workflow.api.model.StepDefinition::getMandatoryValidationMetadataIds,
                                StepDefinition::setMandatoryValidationMetadata))
                .addMappings(m -> m.using(ID_TO_METADATA_NULLABLE_LIST_CONVERTER)
                        .map(coop.libriciel.workflow.api.model.StepDefinition::getMandatoryRejectionMetadataIds, StepDefinition::setMandatoryRejectionMetadata))
                .addMappings(m -> m.using(ID_TO_DESK_REPRESENTATION_NULLABLE_LIST_CONVERTER)
                        .map(coop.libriciel.workflow.api.model.StepDefinition::getValidatingDeskIds, StepDefinition::setValidatingDesks))
                .addMappings(m -> m.using(ID_TO_DESK_REPRESENTATION_NULLABLE_LIST_CONVERTER)
                        .map(coop.libriciel.workflow.api.model.StepDefinition::getNotifiedDeskIds, StepDefinition::setNotifiedDesks));

        mapper.typeMap(WorkflowDefinition.class, coop.libriciel.workflow.api.model.WorkflowDefinition.class)
                .addMappings(m -> m.using(DESK_REPRESENTATION_TO_ID_CONVERTER)
                        .map(WorkflowDefinition::getFinalDesk, coop.libriciel.workflow.api.model.WorkflowDefinition::setFinalDeskId))
                .addMappings(m -> m.using(DESK_REPRESENTATION_TO_ID_NULLABLE_LIST_CONVERTER)
                        .map(WorkflowDefinition::getFinalNotifiedDesks, coop.libriciel.workflow.api.model.WorkflowDefinition::setFinalNotifiedDeskIds));
        mapper.typeMap(coop.libriciel.workflow.api.model.WorkflowDefinition.class, WorkflowDefinition.class)
                .addMappings(m -> m.using(ID_TO_DESK_REPRESENTATION_CONVERTER)
                        .map(coop.libriciel.workflow.api.model.WorkflowDefinition::getFinalDeskId, WorkflowDefinition::setFinalDesk))
                .addMappings(m -> m.using(ID_TO_DESK_REPRESENTATION_NULLABLE_LIST_CONVERTER)
                        .map(coop.libriciel.workflow.api.model.WorkflowDefinition::getFinalNotifiedDeskIds, WorkflowDefinition::setFinalNotifiedDesks));
    }


    private static void initializeMapperForInstances(@NotNull ModelMapper mapper) {

        mapper.typeMap(coop.libriciel.workflow.api.model.Instance.class, Folder.class)
                .addMappings(m -> m.map(coop.libriciel.workflow.api.model.Instance::getBusinessKey, Folder::setContentId))
                .addMappings(m -> m.map(coop.libriciel.workflow.api.model.Instance::getVariables, Folder::setMetadata))
                .addMappings(m -> m.map(coop.libriciel.workflow.api.model.Instance::getTaskList, Folder::setStepList))
                .addMappings(new PropertyMap<>() {
                    @Override
                    protected void configure() {
                        using(ctx -> {
                            var source = ((coop.libriciel.workflow.api.model.Instance) ctx.getSource());
                            if (source.getTaskList() == null) {
                                return UPCOMING;
                            }
                            return computeFolderStateFromWorkflowTaskList(source.getTaskList());
                        }).map(source, destination.getState());
                    }
                })
                .addMappings(m -> m.using(createMapStringExtractor(META_TYPE_ID))
                        .<String>map(coop.libriciel.workflow.api.model.Instance::getVariables, (f, id) -> f.getType().setId(id)))
                .addMappings(m -> m.using(createMapStringExtractor(META_SUBTYPE_ID))
                        .<String>map(coop.libriciel.workflow.api.model.Instance::getVariables, (f, id) -> f.getSubtype().setId(id)))
                .addMappings(m -> m.using(createMapStringExtractor(META_PREMIS_NODE_ID))
                        .map(coop.libriciel.workflow.api.model.Instance::getVariables, Folder::setPremisNodeId))
                .addMappings(m -> m.map(coop.libriciel.workflow.api.model.Instance::getCreationWorkflowKey, Folder::setCreationWorkflowDefinitionKey))
                .addMappings(m -> m.map(coop.libriciel.workflow.api.model.Instance::getValidationWorkflowKey, Folder::setValidationWorkflowDefinitionKey))
                .addMappings(m -> m.using(OFFSETDATETIME_TO_DATE_CONVERTER)
                        .map(coop.libriciel.workflow.api.model.Instance::getDueDate, Folder::setDueDate))
                .addMappings(m -> m.using(ID_TO_DESK_REPRESENTATION_CONVERTER)
                        .map(coop.libriciel.workflow.api.model.Instance::getOriginGroup, Folder::setOriginDesk))
                .addMappings(m -> m.using(ID_TO_DESK_REPRESENTATION_CONVERTER)
                        .map(coop.libriciel.workflow.api.model.Instance::getFinalGroup, Folder::setFinalDesk));

        mapper.typeMap(Folder.class, coop.libriciel.workflow.api.model.Instance.class)
                .addMappings(m -> m.map(Folder::getContentId, coop.libriciel.workflow.api.model.Instance::setBusinessKey))
                .addMappings(new PropertyMap<>() {
                    @Override
                    protected void configure() {
                        using(ctx -> {
                            Folder source = ((Folder) ctx.getSource());
                            Map<String, String> result = new HashMap<>();
                            Optional.ofNullable(source.getMetadata())
                                    .ifPresent(result::putAll);
                            Optional.ofNullable(source.getType())
                                    .map(TypologyEntity::getId)
                                    .ifPresent(id -> result.put(META_TYPE_ID, id));
                            Optional.ofNullable(source.getSubtype())
                                    .map(TypologyEntity::getId)
                                    .ifPresent(id -> result.put(META_SUBTYPE_ID, id));
                            Optional.ofNullable(source.getPremisNodeId())
                                    .ifPresent(id -> result.put(META_PREMIS_NODE_ID, id));
                            return result;
                        }).map(source, destination.getVariables());
                    }
                })
                .addMappings(m -> m.map(folder -> folder.getType().getTenant().getId(), coop.libriciel.workflow.api.model.Instance::setTenantId))
                .addMappings(m -> m.map(Folder::getCreationWorkflowDefinitionKey, coop.libriciel.workflow.api.model.Instance::setCreationWorkflowKey))
                .addMappings(m -> m.map(Folder::getValidationWorkflowDefinitionKey, coop.libriciel.workflow.api.model.Instance::setValidationWorkflowKey))
                .addMappings(m -> m.using(DATE_TO_OFFSETDATETIME_CONVERTER)
                        .map(Folder::getDueDate, coop.libriciel.workflow.api.model.Instance::setDueDate))
                .addMappings(m -> m.using(DESK_REPRESENTATION_TO_ID_CONVERTER)
                        .map(Folder::getOriginDesk, coop.libriciel.workflow.api.model.Instance::setOriginGroup))
                .addMappings(m -> m.using(DESK_REPRESENTATION_TO_ID_CONVERTER)
                        .map(Folder::getFinalDesk, coop.libriciel.workflow.api.model.Instance::setFinalGroup));
    }


    private static void initializeMapperForTasks(@NotNull ModelMapper mapper) {

        mapper.typeMap(Task.class, coop.libriciel.workflow.api.model.Task.class)
                .addMappings(new PropertyMap<>() {
                    @Override
                    protected void configure() {
                        using(ctx -> {
                            Task source = ((Task) ctx.getSource());
                            Map<String, String> result = new HashMap<>(source.getMetadata());
                            result.put(METADATA_PUBLIC_CERTIFICATE_BASE64, source.getPublicCertificateBase64());
                            result.put(METADATA_PUBLIC_ANNOTATION, source.getPublicAnnotation());
                            result.put(METADATA_PRIVATE_ANNOTATION, source.getPrivateAnnotation());
                            result.put(METADATA_TRANSACTION_ID, source.getExternalSignatureProcedureId());
                            Optional.ofNullable(source.getDraftCreationDate())
                                    .map(date -> TextUtils.serializeDate(date, ISO8601_DATE_TIME_FORMAT))
                                    .ifPresent(dateString -> result.put(METADATA_DRAFT_CREATION_DATE, dateString));
                            return result;
                        }).map(source, destination.getVariables());
                    }
                })
                .addMappings(m -> m.map(Task::getAction, coop.libriciel.workflow.api.model.Task::setExpectedAction))
                .addMappings(m -> m.using(DESK_REPRESENTATION_TO_ID_CONVERTER)
                        .map(Task::getDelegatedByDesk, coop.libriciel.workflow.api.model.Task::setDelegatedByGroupId))
                .addMappings(m -> m.using(USER_REP_TO_ID_CONVERTER)
                        .map(Task::getUser, coop.libriciel.workflow.api.model.Task::setAssignee))
                .addMappings(m -> m.using(DESK_REPRESENTATION_TO_ID_NULLABLE_LIST_CONVERTER)
                        .map(Task::getDesks, coop.libriciel.workflow.api.model.Task::setCandidateGroups))
                .addMappings(m -> m.using(DESK_REPRESENTATION_TO_ID_NULLABLE_LIST_CONVERTER)
                        .map(Task::getNotifiedDesks, coop.libriciel.workflow.api.model.Task::setNotifiedGroups))
                .addMappings(m -> m.using(DATE_TO_OFFSETDATETIME_CONVERTER)
                        .map(Task::getDate, coop.libriciel.workflow.api.model.Task::setDate))
                .addMappings(m -> m.using(DATE_TO_OFFSETDATETIME_CONVERTER)
                        .map(Task::getBeginDate, coop.libriciel.workflow.api.model.Task::setBeginDate));

        mapper.typeMap(coop.libriciel.workflow.api.model.Task.class, Task.class)
                .addMappings(m -> m.map(coop.libriciel.workflow.api.model.Task::getExpectedAction, Task::setAction))
                .addMappings(new PropertyMap<>() {
                    @Override
                    protected void configure() {
                        using(ctx -> {
                            var source = ((coop.libriciel.workflow.api.model.Task) ctx.getSource());
                            return computeState(source);
                        }).map(source, destination.getState());
                    }
                })
                .addMappings(m -> m.using(createMapStringExtractor(METADATA_PUBLIC_CERTIFICATE_BASE64))
                        .map(coop.libriciel.workflow.api.model.Task::getVariables, Task::setPublicCertificateBase64))
                .addMappings(m -> m.using(createMapStringExtractor(METADATA_PUBLIC_ANNOTATION))
                        .map(coop.libriciel.workflow.api.model.Task::getVariables, Task::setPublicAnnotation))
                .addMappings(m -> m.using(createMapStringExtractor(METADATA_PRIVATE_ANNOTATION))
                        .map(coop.libriciel.workflow.api.model.Task::getVariables, Task::setPrivateAnnotation))
                .addMappings(m -> m.using(createMapStringExtractor(METADATA_TRANSACTION_ID))
                        .map(coop.libriciel.workflow.api.model.Task::getVariables, Task::setExternalSignatureProcedureId))
                .addMappings(m -> m.using(createMapDateExtractor(METADATA_DRAFT_CREATION_DATE))
                        .map(coop.libriciel.workflow.api.model.Task::getVariables, Task::setDraftCreationDate))
                .addMappings(m -> m.map(coop.libriciel.workflow.api.model.Task::getVariables, Task::setMetadata))
                .addMappings(m -> m.using(ID_TO_DESK_REPRESENTATION_NULLABLE_LIST_CONVERTER)
                        .map(coop.libriciel.workflow.api.model.Task::getNotifiedGroups, Task::setNotifiedDesks))
                .addMappings(m -> m.using(ID_TO_DESK_REPRESENTATION_CONVERTER)
                        .map(coop.libriciel.workflow.api.model.Task::getDelegatedByGroupId, Task::setDelegatedByDesk))
                .addMappings(m -> m.using(ID_TO_USER_REP_CONVERTER)
                        .map(coop.libriciel.workflow.api.model.Task::getAssignee, Task::setUser))
                .addMappings(m -> m.using(ID_TO_DESK_REPRESENTATION_NULLABLE_LIST_CONVERTER)
                        .map(coop.libriciel.workflow.api.model.Task::getCandidateGroups, Task::setDesks))
                .addMappings(m -> m.using(OFFSETDATETIME_TO_DATE_CONVERTER)
                        .map(coop.libriciel.workflow.api.model.Task::getDate, Task::setDate))
                .addMappings(m -> m.using(OFFSETDATETIME_TO_DATE_CONVERTER)
                        .map(coop.libriciel.workflow.api.model.Task::getBeginDate, Task::setBeginDate))
                .addMappings(m -> m.map(coop.libriciel.workflow.api.model.Task::getInstanceName, Task::setInstanceName));
    }


    // <editor-fold desc="Utils">


    static Converter<Map<String, String>, String> createMapStringExtractor(@NotNull String mapKey) {
        return context -> MapUtils.getString(context.getSource(), mapKey);
    }


    static Converter<Map<String, String>, Date> createMapDateExtractor(@NotNull String mapKey) {
        return context -> Optional.ofNullable(MapUtils.getString(context.getSource(), mapKey))
                .map(t -> TextUtils.deserializeDate(t, ISO8601_DATE_TIME_FORMAT))
                .orElse(null);
    }


    static coop.libriciel.workflow.api.model.Action toIpWorkflowValue(@Nullable Action action) {

        if (action == null) {
            return null;
        }

        return switch (action) {
            case ARCHIVE -> coop.libriciel.workflow.api.model.Action.ARCHIVE;
            case ASK_SECOND_OPINION -> coop.libriciel.workflow.api.model.Action.ASK_SECOND_OPINION;
            case BYPASS -> coop.libriciel.workflow.api.model.Action.BYPASS;
            case CHAIN -> coop.libriciel.workflow.api.model.Action.CHAIN;
            case CREATE -> coop.libriciel.workflow.api.model.Action.CREATE;
            case DELETE -> coop.libriciel.workflow.api.model.Action.DELETE;
            case EXTERNAL_SIGNATURE -> coop.libriciel.workflow.api.model.Action.EXTERNAL_SIGNATURE;
            case IPNG -> coop.libriciel.workflow.api.model.Action.IPNG;
            case IPNG_RETURN -> coop.libriciel.workflow.api.model.Action.IPNG_RETURN;
            case PAPER_SIGNATURE -> coop.libriciel.workflow.api.model.Action.PAPER_SIGNATURE;
            case RECYCLE -> coop.libriciel.workflow.api.model.Action.RECYCLE;
            case READ -> coop.libriciel.workflow.api.model.Action.READ;
            case REJECT -> coop.libriciel.workflow.api.model.Action.REJECT;
            case SEAL -> coop.libriciel.workflow.api.model.Action.SEAL;
            case SECOND_OPINION -> coop.libriciel.workflow.api.model.Action.SECOND_OPINION;
            case SECURE_MAIL -> coop.libriciel.workflow.api.model.Action.SECURE_MAIL;
            case SIGNATURE -> coop.libriciel.workflow.api.model.Action.SIGNATURE;
            case START -> coop.libriciel.workflow.api.model.Action.START;
            case TRANSFER -> coop.libriciel.workflow.api.model.Action.TRANSFER;
            case UNDO -> coop.libriciel.workflow.api.model.Action.UNDO;
            case UNKNOWN -> coop.libriciel.workflow.api.model.Action.UNKNOWN;
            case VISA -> coop.libriciel.workflow.api.model.Action.VISA;
            default -> throw new IllegalStateException("Unexpected value: " + action);
        };
    }


    static Action toCoreValue(@Nullable coop.libriciel.workflow.api.model.Action action) {

        if (action == null) {
            return null;
        }

        return switch (action) {
            case ARCHIVE -> Action.ARCHIVE;
            case ASK_SECOND_OPINION -> Action.ASK_SECOND_OPINION;
            case BYPASS -> Action.BYPASS;
            case CHAIN -> Action.CHAIN;
            case CREATE -> Action.CREATE;
            case DELETE -> Action.DELETE;
            case EXTERNAL_SIGNATURE -> Action.EXTERNAL_SIGNATURE;
            case IPNG -> Action.IPNG;
            case IPNG_RETURN -> Action.IPNG_RETURN;
            case PAPER_SIGNATURE -> Action.PAPER_SIGNATURE;
            case RECYCLE -> Action.RECYCLE;
            case READ -> Action.READ;
            case REJECT -> Action.REJECT;
            case SEAL -> Action.SEAL;
            case SECOND_OPINION -> Action.SECOND_OPINION;
            case SECURE_MAIL -> Action.SECURE_MAIL;
            case SIGNATURE -> Action.SIGNATURE;
            case START -> Action.START;
            case TRANSFER -> Action.TRANSFER;
            case UNDO -> Action.UNDO;
            case VISA -> Action.VISA;
            default -> Action.UNKNOWN;
        };
    }


    static coop.libriciel.workflow.api.model.State toIpWorkflowValue(@Nullable State state) {

        if (state == null) {
            return null;
        }

        return switch (state) {
            case DOWNSTREAM -> coop.libriciel.workflow.api.model.State.DOWNSTREAM;
            case DRAFT -> coop.libriciel.workflow.api.model.State.DRAFT;
            case FINISHED -> coop.libriciel.workflow.api.model.State.FINISHED;
            case LATE -> coop.libriciel.workflow.api.model.State.LATE;
            case PENDING -> coop.libriciel.workflow.api.model.State.PENDING;
            case REJECTED -> coop.libriciel.workflow.api.model.State.REJECTED;
            case RETRIEVABLE -> coop.libriciel.workflow.api.model.State.RETRIEVABLE;
            case VALIDATED -> coop.libriciel.workflow.api.model.State.VALIDATED;
            default -> throw new IllegalStateException("Unexpected value: " + state);
        };
    }


    static State toCoreValue(@Nullable coop.libriciel.workflow.api.model.State state) {

        if (state == null) {
            return null;
        }

        return switch (state) {
            case DOWNSTREAM -> State.DOWNSTREAM;
            case DRAFT -> State.DRAFT;
            case FINISHED -> State.FINISHED;
            case LATE -> State.LATE;
            case PENDING -> State.PENDING;
            case REJECTED -> State.REJECTED;
            case RETRIEVABLE -> State.RETRIEVABLE;
            case VALIDATED -> State.VALIDATED;
        };
    }


    static coop.libriciel.workflow.api.model.ExternalState toIpWorkflowValue(@Nullable ExternalState state) {

        if (state == null) {
            return null;
        }

        return switch (state) {
            case ACTIVE -> coop.libriciel.workflow.api.model.ExternalState.ACTIVE;
            case ERROR -> coop.libriciel.workflow.api.model.ExternalState.ERROR;
            case EXPIRED -> coop.libriciel.workflow.api.model.ExternalState.EXPIRED;
            case FORM -> coop.libriciel.workflow.api.model.ExternalState.FORM;
            case REFUSED -> coop.libriciel.workflow.api.model.ExternalState.REFUSED;
            case SIGNED -> coop.libriciel.workflow.api.model.ExternalState.SIGNED;
            default -> throw new IllegalStateException("Unexpected value: " + state);
        };
    }


    static ExternalState toCoreValue(@Nullable coop.libriciel.workflow.api.model.ExternalState state) {

        if (state == null) {
            return null;
        }

        return switch (state) {
            case ACTIVE -> ExternalState.ACTIVE;
            case ERROR -> ExternalState.ERROR;
            case EXPIRED -> ExternalState.EXPIRED;
            case FORM -> ExternalState.FORM;
            case REFUSED -> ExternalState.REFUSED;
            case SIGNED -> ExternalState.SIGNED;
        };
    }


    static coop.libriciel.workflow.api.model.ParallelType toIpWorkflowValue(@Nullable StepDefinitionParallelType step) {

        if (step == null) {
            return null;
        }

        return switch (step) {
            case AND -> coop.libriciel.workflow.api.model.ParallelType.AND;
            case OR -> coop.libriciel.workflow.api.model.ParallelType.OR;
        };
    }


    static StepDefinitionParallelType toCoreValue(@Nullable coop.libriciel.workflow.api.model.ParallelType step) {

        if (step == null) {
            return null;
        }

        return switch (step) {
            case AND -> StepDefinitionParallelType.AND;
            case OR -> StepDefinitionParallelType.OR;
        };
    }


    static String toIpWorkflowValue(@Nullable FolderSortBy sortBy) {
        return switch (firstNonNull(sortBy, CREATION_DATE)) {
            case FOLDER_NAME -> "INSTANCE_NAME";
            case FOLDER_ID -> "INSTANCE_ID";
            case TYPE_ID -> META_TYPE_ID;
            case SUBTYPE_ID -> META_SUBTYPE_ID;
            case TYPE_NAME, SUBTYPE_NAME -> throw new InvalidParameterException("IpWorkflow can't sort by type/subtype name");
            case LATE_DATE -> LATE_DATE.toString();
            default -> CREATION_DATE.toString();
        };
    }


    static coop.libriciel.workflow.api.model.WorkflowDefinitionSortBy toIpWorkflowValue(WorkflowDefinitionSortBy enumValue) {
        return switch (enumValue) {
            case NAME -> coop.libriciel.workflow.api.model.WorkflowDefinitionSortBy.NAME;
            case KEY -> coop.libriciel.workflow.api.model.WorkflowDefinitionSortBy.KEY;
            case ID -> coop.libriciel.workflow.api.model.WorkflowDefinitionSortBy.ID;
        };
    }


    static Date toDate(OffsetDateTime offsetDateTime) {
        return Optional.ofNullable(offsetDateTime)
                .map(OffsetDateTime::toInstant)
                .map(Date::from)
                .orElse(null);
    }


    static OffsetDateTime toOffsetDateTime(Date date) {
        return Optional
                .ofNullable(date)
                .map(Date::getTime)
                .map(Instant::ofEpochMilli)
                .map(instant -> instant.atZone(systemDefault()))
                .map(ZonedDateTime::toOffsetDateTime)
                .orElse(null);
    }


    static State computeState(coop.libriciel.workflow.api.model.Task task) {

        // Simple cases

        if (task.isPending() == TRUE) {
            return PENDING;
        }

        if (task.getPerformedAction() == null) {
            return UPCOMING;
        }

        if (task.getExpectedAction() == toIpWorkflowValue(READ)) {
            return VALIDATED;
        }

        boolean isTheExpectedAction = task.getExpectedAction() == task.getPerformedAction();
        boolean isAnExpectedSignature = task.getExpectedAction() == toIpWorkflowValue(SIGNATURE);
        boolean isAPerformedPaperSignature = task.getPerformedAction() == toIpWorkflowValue(PAPER_SIGNATURE);
        boolean isAnAcceptableSecondaryAction = isAnExpectedSignature && isAPerformedPaperSignature;
        if (isTheExpectedAction || isAnAcceptableSecondaryAction) {
            return VALIDATED;
        }

        return switch (task.getPerformedAction()) {
            case TRANSFER -> TRANSFERRED;
            case ASK_SECOND_OPINION -> SECONDED;
            case BYPASS -> BYPASSED;
            case REJECT -> REJECTED;
            default -> toCoreValue(task.getState());
        };
    }


    static State computeFolderStateFromWorkflowTask(@NotNull coop.libriciel.workflow.api.model.Task task) {
        return FolderUtils.computeFolderStateFromActionAndDate(toCoreValue(task.getExpectedAction()), toDate(task.getDueDate()));
    }


    static State computeFolderStateFromWorkflowTaskList(@NotNull List<coop.libriciel.workflow.api.model.Task> tasks) {

        if (tasks.stream()
                .map(coop.libriciel.workflow.api.model.Task::getState)
                .anyMatch(state -> state == coop.libriciel.workflow.api.model.State.REJECTED)) {
            return REJECTED;
        }

        return tasks.stream()
                .filter(task -> task.getPerformedAction() == null || task.getPerformedAction() == toIpWorkflowValue(ARCHIVE))
                .filter(task -> task.getExpectedAction() != coop.libriciel.workflow.api.model.Action.READ)
                .filter(task -> FolderUtils.isActiveOrDraftOrFinished(toCoreValue(task.getState())))
                .findFirst()
                .map(IpWorkflowService::computeFolderStateFromWorkflowTask)
                .orElseGet(() -> {
                    log.warn("computeFolderStateFromWorkflowTaskList cannot determine the folder state from tasks:{}", tasks);
                    return PENDING;
                });
    }


    // </editor-fold desc="Utils">


    // <editor-fold desc="Workflow definition CRUDL">


//    @Override
//    public void createWorkflowDefinition(@NotNull String tenantId, @NotNull String workflowDefinition, @NotNull String workflowName) {
//
//        URI requestUri = UriComponentsBuilder.newInstance()
//                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
//                .path(DEFINITIONS_URL)
//                .queryParam("tenantId", tenantId)
//                .build().normalize().toUri();
//
//        JSONStringer stringer;
//        try {
//            stringer = new JSONStringer()
//                    .object()
//                    /**/.key("workflowDefinitionBase64").value(Base64.getEncoder().encodeToString(workflowDefinition.getBytes()))
//                    /**/.key("workflowName").value(workflowName + (workflowName.endsWith(".bpmn20.xml") ? EMPTY : ".bpmn20.xml"))
//                    /**/.key("isBpmnPatchNeeded").value(true)
//                    .endObject();
//        } catch (JSONException e) {
//            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_writing_request");
//        }
//
//        WorkflowDefinition result = WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
//                .post().uri(requestUri)
//                .contentType(APPLICATION_JSON).accept(APPLICATION_JSON)
//                .bodyValue(stringer.toString())
//                .retrieve()
//                // Result
//                .bodyToMono(WorkflowDefinition.class)
//                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
//                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service"));
//
//        log.info("Created definition response:" + result);
//    }


    @Override
    public void createWorkflowDefinition(@NotNull String tenantId, @NotNull WorkflowDefinition workflowDefinition) {

        substitutePlaceholdersToIndexedPlaceholders(workflowDefinition);
        workflowDefinition.setId(workflowDefinition.getKey());

        var libDefinition = ipWorkflowModelMapper.map(workflowDefinition, coop.libriciel.workflow.api.model.WorkflowDefinition.class);
        new DefinitionApi(apiClient)
                .createWorkflowDefinition(tenantId, libDefinition)
                .doOnError(error -> {
                    log.error("Error while creating workflow definition: {}, {}", workflowDefinition.getName(), error.getMessage());
                    throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, error, "message.cannot_reach_workflow_service");
                })
                .doOnSuccess(response -> log.info("Created definition tenantId:{} key:{}", tenantId, response.getKey()))
                .block(SUBSERVICE_REQUEST_LONG_TIMEOUT);
    }


    @Override
    public @Nullable WorkflowDefinition getWorkflowDefinitionById(@NotNull String tenantId, @NotNull String id) {

        WorkflowDefinition result = new DefinitionApi(apiClient)
                .getWorkflowDefinitionById(id, tenantId)
                .doOnError(error -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, error, "message.cannot_reach_workflow_service");})
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .map(requestResult -> ipWorkflowModelMapper.map(requestResult, WorkflowDefinition.class))
                .orElse(null);

        log.debug("getWorkflowDefinitionById {}", result);
        return result;
    }


    @Override
    public @NotNull Optional<WorkflowDefinition> getWorkflowDefinitionByKey(@NotNull String tenantId, @NotNull String key) {
        log.debug("getWorkflowDefinitionByKey - tenant: {}, key: {}", tenantId, key);

        Optional<WorkflowDefinition> result = new DefinitionApi(apiClient)
                .getWorkflowDefinitionByKey(key, tenantId)
                // We just ignore the 404 result, to return an empty object here.
                // Keeping any other error.
                .onErrorResume(error -> Optional.of(error)
                        .filter(WebClientResponseException.class::isInstance)
                        .map(WebClientResponseException.class::cast)
                        .map(WebClientResponseException::getStatusCode)
                        .filter(status -> status.equals(NOT_FOUND))
                        .map(status -> Mono.empty().cast(coop.libriciel.workflow.api.model.WorkflowDefinition.class))
                        .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, error, "message.cannot_reach_workflow_service"))
                )
                .blockOptional(SUBSERVICE_REQUEST_MEDIUM_TIMEOUT)
                .map(requestResult -> ipWorkflowModelMapper.map(requestResult, WorkflowDefinition.class));

        log.debug("getWorkflowDefinitionByKey result:{}", result);

        // FIXME : In any other non-404 error, we should re-throw an error instead of returning an empty object
        return result;
    }


    @Override
    public @NotNull Optional<WorkflowDefinition> getWorkflowDefinitionByKeyAndMapPlaceholders(@NotNull String tenantId,
                                                                                              @NotNull String key,
                                                                                              @Nullable String originDeskId) {
        Optional<WorkflowDefinition> result = getWorkflowDefinitionByKey(tenantId, key);

        if (result.isEmpty()) {
            return result;
        }
        // Computing placeholders, if possible
        if (StringUtils.isNotEmpty(originDeskId)) {
            substitutePlaceholdersToIndexedPlaceholders(result.get());
            Map<String, String> placeholdersMapping = computePlaceholderDesksConcreteValues(result.get(), originDeskId, emptyMap());
            result.get().getSteps().stream()
                    .flatMap(s -> s.getValidatingDesks().stream())
                    .forEach(d -> mapIndexedPlaceholdersToActualDesk(d, originDeskId, placeholdersMapping));

            mapIndexedPlaceholdersToActualDesk(result.get().getFinalDesk(), originDeskId, placeholdersMapping);
        }

        log.debug("getWorkflowDefinitionByKeyAndMapPlaceholders {}", result);
        return result;
    }


    @Override
    public void updateWorkflowDefinition(@NotNull String tenantId, @NotNull WorkflowDefinition workflowDefinition) {

        substitutePlaceholdersToIndexedPlaceholders(workflowDefinition);

        var libDefinition = ipWorkflowModelMapper.map(workflowDefinition, coop.libriciel.workflow.api.model.WorkflowDefinition.class);
        new DefinitionApi(apiClient)
                .updateWorkflowDefinition(workflowDefinition.getKey(), tenantId, libDefinition)
                .doOnError(error -> {
                    log.error("Error while updating workflow definition: {}, {}", workflowDefinition.getId(), error.getMessage());
                    throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, error, "message.cannot_reach_workflow_service");
                })
                .doOnSuccess(response -> log.info("Update definition tenantId:{} key:{}", tenantId, response.getKey()))
                .block(SUBSERVICE_REQUEST_LONG_TIMEOUT);
    }


    @Override
    public void deleteWorkflowDefinition(@NotNull String tenantId, @NotNull String workflowDefinitionKey) {
        new DefinitionApi(apiClient)
                .deleteWorkflowDefinition(tenantId, workflowDefinitionKey)
                .doOnError(error -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, error, "message.cannot_reach_workflow_service");})
                .doOnSuccess(response -> log.info("Deleted definition key:{}", workflowDefinitionKey))
                .block(SUBSERVICE_REQUEST_LONG_TIMEOUT);
    }


    @Override
    public @NotNull Page<WorkflowDefinitionRepresentation> listWorkflowDefinitions(@NotNull String tenantId,
                                                                                   @NotNull Pageable pageable,
                                                                                   @Nullable String searchTerm) {

        String urlEncodedSearchTerm = Optional.ofNullable(searchTerm)
                .filter(StringUtils::isNotEmpty)
                .map(value -> URLEncoder.encode(value, UTF_8))
                .orElse(null);

        log.debug("getWorkflowDefinitions - Url encoded searchTerm:{}", urlEncodedSearchTerm);

        long page = RequestUtils.getPageNumber(pageable);
        long pageSize = RequestUtils.getPageSize(pageable);

        WorkflowDefinitionSortBy sortBy = pageable.getSort().stream().findFirst()
                .map(Sort.Order::getProperty)
                .map(WorkflowDefinitionSortBy::valueOf)
                .orElse(WorkflowDefinitionSortBy.NAME);

        boolean asc = pageable.getSort().stream().findFirst()
                .map(Sort.Order::getDirection)
                .map(Sort.Direction::isAscending)
                .orElse(true);

        Page<WorkflowDefinitionRepresentation> result = new DefinitionApi(apiClient)
                .getWorkflowDefinitions((int) page, (int) pageSize, tenantId, toIpWorkflowValue(sortBy), asc, searchTerm)
                .doOnError(e -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_workflow_service");})
                .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT)
                .filter(requestResult -> requestResult.getData() != null)
                .filter(requestResult -> requestResult.getTotal() != null)
                .map(requestResult -> new PageImpl<>(
                        requestResult.getData().stream().map(s -> ipWorkflowModelMapper.map(s, WorkflowDefinitionRepresentation.class)).toList(),
                        pageable,
                        requestResult.getTotal()
                ))
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service"));

        log.debug("getWorkflowDefinitions {}", result);

        // Filtering internal workflows

        return result;
    }


    // </editor-fold desc="Workflow definition CRUDL">


    // <editor-fold desc="Folder CRUDL">


    @Override
    public @NotNull Folder getFolder(@NotNull String id, @NotNull String tenantId, boolean withHistory) {

        Folder folder = new InstanceApi(apiClient)
                .getInstance(id, withHistory, tenantId)
                .doOnError(error -> {
                    log.error("getFolder - error while calling workflow service");
                    log.debug("error detail: ", error);
                    if (error instanceof WebClientResponseException wcrException) {
                        if (wcrException.getStatusCode().value() == NOT_FOUND.value()) {
                            throw new LocalizedNotFoundStatusException(error, "message.unknown_folder_id");
                        }
                    }
                    throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, error, "message.cannot_reach_workflow_service");
                })
                .doOnSuccess(response -> log.debug("getFolder tenantId:{} key:{}", tenantId, response.getId()))
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .map(response -> ipWorkflowModelMapper.map(response, Folder.class))
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service"));

        log.debug("getFolder metadata {}", folder.getMetadata());

        folder.getStepList().stream()
                .filter(t -> t.getAction() != START)
                .flatMap(t -> t.getDesks().stream())
                .forEach(d -> mapIndexedPlaceholdersToActualDesk(d, folder.getOriginDesk().getId(), folder.getMetadata()));

        return folder;
    }


    @Override
    public @NotNull Page<Folder> listFoldersByState(@NotNull String role,
                                                    @NotNull State state,
                                                    @NotNull List<DelegationRule> delegationRules,
                                                    @Nullable FolderFilter folderFilter,
                                                    @NotNull Pageable pageable) {

        log.debug("listFoldersByState role:{} state:{}", role, state);
        Pageable ipWorkflowPageable = RequestUtils.convertSortedPageable(pageable, FolderSortBy.class, IpWorkflowService::toIpWorkflowValue);

        // The original CURRENT state use to concatenate CURRENT and DELEGATED folders.
        // Due to a specs re-planification, we don't want the DELEGATED folders anymore,
        // and we had to create a specific DELEGATED state, that does not match the CURRENT.
        // TODO : Find a proper way to request Workflow.
        //  The current state of the art is kinda weird anyway: we have some data in a path, and some in the body.
        //  Maybe we should manage everything through the body object, that would contain (or not) the current desk.
        String finalRole = (state == DELEGATED) ? "non-existing-id" : role;

        // Workflow is a permission-free zone, it does not know anything.
        // DELEGATED and PENDING are equal up there.
        State finalState = (state == DELEGATED) ? PENDING : state;

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(GROUP_URL).pathSegment(finalRole).pathSegment(finalState.toString())
                .queryParam("sortBy", ipWorkflowPageable.getSort().stream().map(Sort.Order::getProperty).findFirst().orElse(null))
                .queryParam("asc", isFirstOrderAsc(ipWorkflowPageable))
                .queryParam("page", ipWorkflowPageable.getPageNumber())
                .queryParam("pageSize", ipWorkflowPageable.getPageSize())
                .build().normalize().toUri();

        PaginatedList<IpWorkflowTask> result;

        FilteredRequest body = new FilteredRequest();
        if (state == DELEGATED) {
            body.setFilteringParameters(delegationRules
                    .stream()
                    // DELEGATED is actually a PENDING state in Workflow,
                    .map(d -> new FilteringParameterResultCount(d.getDelegatingGroup(), PENDING, d.getMetadataKey(), d.getMetadataValue(), null))
                    .toList());
        }

        if (Objects.nonNull(folderFilter)) {
            FolderFilterParam folderFilterParam = ipWorkflowModelMapper.map(folderFilter, FolderFilterParam.class);
            body.setFolderFilter(folderFilterParam);
        }

        WebClient webclient = RequestUtils.getWebClientWithBufferSize(64, true);

        result = webclient
                .post().uri(requestUri)
                .contentType(APPLICATION_JSON).accept(APPLICATION_JSON)
                .bodyValue(body)
                .retrieve()
                // Result
                .bodyToMono(new ParameterizedTypeReference<PaginatedList<IpWorkflowTask>>() {})
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service"));

        return new PageImpl<>(
                result.getData().stream()
                        .map(IpWorkflowInstance::new)
                        .map(Folder.class::cast)
                        .toList(),
                pageable,
                result.getTotal()
        );
    }


    @Override
    public @NotNull Map<DelegationRule, Integer> countFolders(@NotNull Set<String> roles, @NotNull List<DelegationRule> delegationRules) {
        log.trace("countFolders roles:{} delegationRules:{}", roles, delegationRules);

        // Building body

        List<FilteringParameterResultCount> groupCounts = new ArrayList<>();
        roles.stream()
                .map(r -> new FilteringParameterResultCount(r, null, null, null, null))
                .forEach(groupCounts::add);

        delegationRules.stream()
                .map(d -> new FilteringParameterResultCount(d.getDelegatingGroup(), PENDING, d.getMetadataKey(), d.getMetadataValue(), null))
                .forEach(groupCounts::add);

        FilteredRequest body = FilteredRequest
                .builder()
                .filteringParameters(groupCounts)
                .build();

        // Request

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(GROUP_URL)
                .build().normalize().toUri();

        List<FilteringParameterResultCount> requestResult = WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
                .post().uri(requestUri)
                .contentType(APPLICATION_JSON).accept(APPLICATION_JSON)
                .bodyValue(body)
                .retrieve()
                // Result
                .bodyToMono(new ParameterizedTypeReference<List<FilteringParameterResultCount>>() {})
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service"));

        Map<DelegationRule, Integer> resultMap = new HashMap<>();
        requestResult.forEach(f -> resultMap.put(
                new DelegationRule(f.getGroupId(), f.getState(), f.getFilterMetadataKey(), f.getFilterMetadataValue()),
                f.getCount())
        );

        log.trace("countFolders call result {}", resultMap);
        return resultMap;
    }


    @Override
    public @NotNull Integer countFolders(@NotNull String role, @NotNull State state) {

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(GROUP_URL).pathSegment(role).pathSegment(COUNT_PATH).path(state.toString())
                .build().normalize().toUri();

        log.debug("countFolders URI:{}", requestUri);

        IntResponse result = WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
                .get().uri(requestUri)
                .accept(APPLICATION_JSON)
                .retrieve()
                // Result
                .bodyToMono(IntResponse.class)
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service"));

        log.debug("countFolders role:{} state:{} result:{}", role, state, result);
        return result.getResult();
    }


    @Override
    public @NotNull PaginatedList<? extends Folder> listFoldersForDesks(@NotNull String tenantId,
                                                                        int page,
                                                                        int pageSize,
                                                                        FolderSortBy sortBy,
                                                                        @NotNull FolderFilter folderFilter,
                                                                        boolean asc,
                                                                        @NotNull List<String> deskIds,
                                                                        @Nullable Long emitBeforeTime,
                                                                        @Nullable Long stillSinceTime,
                                                                        @Nullable State state) {

        log.debug("listFoldersForDesks deskIds:{}", deskIds);
        String urlEncodedSearchTerm = Optional.ofNullable(folderFilter.getSearchData())
                .filter(StringUtils::isNotEmpty)
                .map(value -> URLEncoder.encode(value, UTF_8))
                .orElse(null);

        UriComponentsBuilder builder = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(INSTANCE_URL).pathSegment("search")
                .queryParam("tenantId", tenantId)
                .queryParam("page", page)
                .queryParam("pageSize", pageSize)
                .queryParam("sortBy", toIpWorkflowValue(sortBy))
                .queryParam("asc", asc);

        JSONStringer stringer;
        try {
            stringer = new JSONStringer()
                    .object()
                    /**/.key("searchTerm").value(urlEncodedSearchTerm)
                    /**/.key("emitBeforeTime").value(emitBeforeTime)
                    /**/.key("stillSinceTime").value(stillSinceTime)
                    /**/.key("state").value(Optional.ofNullable(state).map(State::name).orElse(null))
                    /**/.key("groupIds").array();
            for (String id : deskIds) {
                stringer.value(id);
            }
            stringer.endArray();

            if (folderFilter.getType() != null) {
                stringer.key("typeId").value(Optional.of(folderFilter.getType()).map(TypologyEntity::getId).orElse(null));
            }
            if (folderFilter.getSubtype() != null) {
                stringer.key("subtypeId").value(Optional.of(folderFilter.getSubtype()).map(TypologyEntity::getId).orElse(null));
            }
            if (folderFilter.getLegacyId() != null) {
                stringer.key("legacyId").value(folderFilter.getLegacyId());
            }

            stringer.endObject();

        } catch (JSONException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_writing_request");
        }


        URI requestUri = builder.build().normalize().toUri();

        WebClient webclient = RequestUtils.getWebClientWithBufferSize(64, true);

        PaginatedList<IpWorkflowTask> taskPaginatedList = webclient
                .post().uri(requestUri)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .bodyValue(stringer.toString())
                .retrieve()
                // Result
                .bodyToMono(new ParameterizedTypeReference<PaginatedList<IpWorkflowTask>>() {})
                .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service"));

        log.debug("listFoldersForDesks taskPaginatedList:{}", taskPaginatedList);

        // Re-wrap retrieved tasks into Instances.
        return new PaginatedList<>(
                taskPaginatedList.getData().stream().map(IpWorkflowInstance::new).toList(),
                page,
                pageSize,
                taskPaginatedList.getTotal()
        );
    }


    @Override
    public @NotNull Page<IpWorkflowInstance> listFolders(@NotNull String tenantId,
                                                         @NotNull Pageable pageable,
                                                         @Nullable FolderFilter folderFilter,
                                                         @Nullable String deskId,
                                                         @Nullable Long emitBeforeTime,
                                                         @Nullable Long stillSinceTime,
                                                         @Nullable State state) {

        String urlEncodedSearchTerm = Optional.ofNullable(folderFilter)
                .map(FolderFilter::getSearchData)
                .filter(StringUtils::isNotEmpty)
                .map(value -> URLEncoder.encode(value, UTF_8))
                .orElse(null);

        long page = RequestUtils.getPageNumber(pageable);
        long pageSize = RequestUtils.getPageSize(pageable);
        FolderSortBy sortBy = RequestUtils.getFirstOderSort(pageable, FolderSortBy.class, FOLDER_NAME);
        boolean asc = RequestUtils.isFirstOrderAsc(pageable);

        UriComponentsBuilder builder = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(INSTANCE_URL)
                .queryParam("tenantId", tenantId)
                .queryParam("page", page)
                .queryParam("pageSize", pageSize)
                .queryParam("sortBy", toIpWorkflowValue(sortBy))
                .queryParam("asc", asc)
                .queryParam("groupId", Optional.ofNullable(deskId).filter(StringUtils::isNotEmpty).orElse(null))
                .queryParam("searchTerm", urlEncodedSearchTerm)
                .queryParam("emitBeforeTime", emitBeforeTime)
                .queryParam("stillSinceTime", stillSinceTime)
                .queryParam("state", Optional.ofNullable(state).map(State::name).orElse(null));

        if (Objects.nonNull(folderFilter)) {
            builder.queryParam("typeId", Optional.ofNullable(folderFilter.getType()).map(TypologyEntity::getId).orElse(null))
                    .queryParam("subtypeId", Optional.ofNullable(folderFilter.getSubtype()).map(TypologyEntity::getId).orElse(null))
                    .queryParam("legacyId", folderFilter.getLegacyId());
        }

        URI requestUri = builder.build().normalize().toUri();

        WebClient webclient = RequestUtils.getWebClientWithBufferSize(64, true);

        PaginatedList<IpWorkflowTask> taskPaginatedList = webclient
                .get().uri(requestUri)
                .accept(APPLICATION_JSON)
                .retrieve()
                // Result
                .bodyToMono(new ParameterizedTypeReference<PaginatedList<IpWorkflowTask>>() {})
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service"));

        log.debug("listFolders taskPaginatedList:{}", taskPaginatedList);

        return new PageImpl<>(taskPaginatedList.getData(), pageable, taskPaginatedList.getTotal())
                // Re-wrap retrieved tasks into Instances.
                .map(IpWorkflowInstance::new);
    }


    @Override
    public @NotNull Folder createDraftWorkflow(@NotNull String tenantId, @NotNull Folder folder, Map<Integer, String> variableDesksIds) {

        if (folder.getMetadata().entrySet().stream().anyMatch(meta -> meta.getKey().startsWith(INTERNAL_PREFIX))) {
            log.info("createDraftWorkflow - a metadata uses internal prefix, abort");
            throw new LocalizedStatusException(BAD_REQUEST, "message.metadata_starting_with_p_are_reserved", INTERNAL_PREFIX);
        }

        WorkflowDefinition validationWorkflowDefinition = getWorkflowDefinitionByKey(tenantId, folder.getValidationWorkflowDefinitionKey())
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.unknown_workflow_definition_id"));

        Map<String, String> additionalMetadata = computePlaceholderDesksConcreteValues(
                validationWorkflowDefinition,
                folder.getOriginDesk().getId(),
                variableDesksIds
        );
        folder.getMetadata().putAll(additionalMetadata);
        folder.setFinalDesk(Optional.ofNullable(validationWorkflowDefinition.getFinalDesk()).orElse(folder.getOriginDesk()));
        mapIndexedPlaceholdersToActualDesk(folder.getFinalDesk(), folder.getOriginDesk().getId(), additionalMetadata);

        Instance request = ipWorkflowModelMapper.map(folder, Instance.class);

        Folder folderResult = new InstanceApi(apiClient)
                .createInstance(request)
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .map(result -> ipWorkflowModelMapper.map(result, Folder.class))
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service"));

        log.debug("createDraftWorkflow call result {}", folderResult);
        return folderResult;
    }


    // </editor-fold desc="Folder CRUDL">


    public void substitutePlaceholdersToIndexedPlaceholders(@NotNull WorkflowDefinition workflowDefinition) {

        // The real list, in streamed elements,
        // including an atomic-getter, to keep trace of the current step index.
        // We want to keep this streamed.

        AtomicInteger stepIdx = new AtomicInteger(-1);
        Stream<DeskRepresentation> concernedDesksStream = workflowDefinition.getSteps().stream()
                .peek(s -> stepIdx.getAndIncrement())
                .map(StepDefinition::getValidatingDesks)
                .flatMap(Collection::stream);

        concernedDesksStream = Stream.concat(
                concernedDesksStream,
                Stream.of(workflowDefinition.getFinalDesk())
                        .peek(s -> stepIdx.getAndIncrement())
        );

        // BOSS_OF/VARIABLE_DESKS are not permitted in the notified desks.
        // we can simply loop through those, and simply append these to the global stream...
        // It may seems useless, but we still want to evaluate a notified emitter.

        List<DeskRepresentation> notifiedDesks = workflowDefinition.getSteps().stream()
                .map(StepDefinition::getNotifiedDesks)
                .flatMap(Collection::stream)
                .toList();

        concernedDesksStream = Stream.concat(
                concernedDesksStream,
                notifiedDesks.stream()
        );

        concernedDesksStream = Stream.concat(
                concernedDesksStream,
                workflowDefinition.getFinalNotifiedDesks().stream()
        );

        // Loop through everything, and compute values.
        // This is where the index-counter will start to increase

        concernedDesksStream.forEach(entity -> {
            switch (entity.getId()) {
                case EMITTER_ID_PLACEHOLDER -> entity.setId(String.format("${%s}", META_EMITTER_ID));
                case VARIABLE_DESK_ID_PLACEHOLDER -> {
                    String varDeskMetadataKey = String.format(META_VARIABLE_DESK_DESKID_X_FORMAT, stepIdx.get());
                    entity.setId(String.format("${%s}", varDeskMetadataKey));
                }
                case BOSS_OF_ID_PLACEHOLDER -> {
                    String bossOfMetadataKey = String.format(META_BOSS_OF_DESKID_X_FORMAT, stepIdx.get());
                    entity.setId(String.format("${%s}", bossOfMetadataKey));
                }
            }
        });
    }


    public @NotNull Map<String, String> computePlaceholderDesksConcreteValues(@NotNull WorkflowDefinition validationWorkflowDefinition,
                                                                              @NotNull String originDeskId,
                                                                              @NotNull Map<Integer, String> variableDesksValues) {

        Map<String, String> result = new HashMap<>();
        result.put(META_EMITTER_ID, originDeskId);

        // We probably need an additional mechanic to handle the creation workflow,
        // As for now the metadata will be the same for both workflow
        // whereas the variable desks metadata may differ

//        if (folder.getCreationWorkflowDefinitionKey() != null) {
//            try {
//                WorkflowDefinition creationWorkflowDefinition = this.getWorkflowDefinitionByKey(tenantId, folder.getCreationWorkflowDefinitionKey());
//                Map<String, String> additionalMetadata = getMetadataForPlaceholders(
//                        folder,
//                        creationWorkflowDefinition,
//                        ...,
//                        ...
//                );
//                folder.getMetadata().putAll(additionalMetadata);
//            } catch (Exception e) {
//                log.error("Didn't find the creation workflow");
//            }
//        }

        AtomicInteger stepIdx = new AtomicInteger(-1);
        AtomicReference<String> previousValidatorDeskId = new AtomicReference<>(originDeskId);

        Consumer<String> processDeskIdFn = (String deskId) -> {

            if (META_EMITTER_PATTERN.matcher(deskId).matches()) {
                deskId = originDeskId;
            }

            Matcher bossDeskIdMatcher = META_BOSSOF_PATTERN.matcher(deskId);
            if (bossDeskIdMatcher.matches()) {
                Desk previousDesk = Optional.ofNullable(previousValidatorDeskId.get())
                        .map(i -> authService.findDeskById(null, i))
                        .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.unknown_desk_id"));

                deskId = Optional.ofNullable(previousDesk.getParentDesk())
                        .map(DeskRepresentation::getId)
                        .orElse(previousDesk.getId());

                String metadataKey = bossDeskIdMatcher.group(1);
                result.put(metadataKey, deskId);
            }

            Matcher variableDeskIdMatcher = META_VARIABLEDESK_PATTERN.matcher(deskId);
            if (variableDeskIdMatcher.matches()) {
                String idValue = variableDesksValues.get(stepIdx.get());
                if (idValue != null) {
                    // Watch out - this means that a 'BossOf' step cannot follow a 'VariableDesk' step,
                    // If this variable desk's value is not provided at creation (but eg in selection script)
                    deskId = idValue;
                    String metadataKey = variableDeskIdMatcher.group(1);
                    result.put(metadataKey, deskId);
                } else {
                    log.warn("No ID provided for variable desk n° {}", stepIdx.get());
                }
            }
            previousValidatorDeskId.set(deskId);
        };


        validationWorkflowDefinition.getSteps().stream()
                .peek(s -> stepIdx.getAndIncrement())
                .forEach(step -> step.getValidatingDesks().stream()
                        .map(DeskRepresentation::getId)
                        .forEach(processDeskIdFn));

        stepIdx.getAndIncrement();
        Optional.ofNullable(validationWorkflowDefinition.getFinalDesk())
                .map(DeskRepresentation::getId)
                .ifPresent(processDeskIdFn);

        return result;
    }


    public void mapIndexedPlaceholdersToActualDesk(@NotNull DeskRepresentation desk,
                                                   @NotNull String originDeskId,
                                                   @NotNull Map<String, String> placeholdersMapping) {

        log.debug("mapVariablePlaceholdersToActualDesk deskId : {}", desk.getId());
        Matcher matcher = META_EMITTER_PATTERN.matcher(desk.getId());
        if (matcher.matches()) {
            desk.setId(originDeskId);
            return;
        }

        matcher = META_BOSSOF_PATTERN.matcher(desk.getId());
        if (matcher.matches()) {
            String metaKey = matcher.group(1);
            String actualDeskId = placeholdersMapping.get(metaKey);
            if (actualDeskId == null) {
                log.error("Could not find ID for 'bossOf' placeholder : {}", desk.getId());
            }
            desk.setId(actualDeskId);
            return;
        }

        matcher = META_VARIABLEDESK_PATTERN.matcher(desk.getId());
        if (matcher.matches()) {
            String metaKey = matcher.group(1);
            String actualDeskId = placeholdersMapping.get(metaKey);
            if (actualDeskId == null) {
                log.error("Could not find ID for 'variableDesk' placeholder : {}", desk.getId());
                return;
            }
            desk.setId(actualDeskId);
        }
    }


    @Override
    public void editFolder(String tenantId, @NotNull String folderId, @NotNull Folder folder) {

        log.info("editFolder tenantId:{}, folderId:{}", tenantId, folderId);
        log.debug(" editFolder - new folder data: {}", folder);

        Instance request = ipWorkflowModelMapper.map(folder, Instance.class);
        request.getVariables().keySet().removeIf(key -> key.startsWith(WORKFLOW_INTERNAL_VAR_PREFIX));

        new InstanceApi(apiClient)
                .editInstance(folderId, request, tenantId)
                .doOnError(error -> {
                    log.error("editFolder - error while calling workflow service");
                    log.debug("error detail :  ", error);
                    throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, error, "message.cannot_reach_workflow_service");
                })
                .doOnSuccess(response -> log.info("editFolder - folder with id '{}' successfully edited", folderId))
                .block(SUBSERVICE_REQUEST_LONG_TIMEOUT);
    }


    @Override
    public @NotNull List<Task> getHistoricTasks(@NotNull String folderId) {

        List<Task> result = new TaskApi(apiClient)
                .getHistoryTasks(folderId)
                .doOnError(error -> {
                    log.error("Error while retrieving historic tasks on folderId:{}: {}", folderId, error.getMessage());
                    throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service");
                })
                .collectList()
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service"))
                .stream()
                .map(task -> ipWorkflowModelMapper.map(task, Task.class))
                .filter(t -> !((t.getAction() == UNDO) && (t.getUser() == null)))
                .filter(t -> !((t.getAction() == READ) && (t.getUser() == null)))
                // Filtering internal pending tasks
                // Sorting chronologically
                .sorted(comparing(Task::getDate, nullsLast(naturalOrder())))
                .collect(toMutableList());

        log.debug("getHistoricTasks result {}", result);
        return result;
    }


    @Override
    public @NotNull List<Task> getReadTasks(Folder folder) {

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(TASK_URL).pathSegment(folder.getId()).pathSegment(READ_TASKS)
                .build().normalize().toUri();

        // TODO in the long run, the cleaner solution would be to stream each task back from workflow, one at a time
        WebClient webClient = RequestUtils.getWebClientWithBufferSize(2, true);

        List<Task> result = webClient
                .get().uri(requestUri)
                .accept(APPLICATION_JSON)
                .retrieve()
                // Result
                .bodyToMono(new ParameterizedTypeReference<List<IpWorkflowTask>>() {})
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service"))
                .stream()
                .map(t -> (Task) t)
                // Sorting chronologically
                .sorted(comparing(Task::getDate, nullsLast(naturalOrder())))
                .toList();

        log.debug("getTask result {}", result);
        return result;
    }


    @Override
    public @NotNull Task getTask(@NotNull String taskId) {

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(TASK_URL).pathSegment(taskId)
                .build().normalize().toUri();

        Task result = WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
                .get().uri(requestUri)
                .accept(APPLICATION_JSON)
                .retrieve()
                // Result
                .bodyToMono(IpWorkflowTask.class)
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service"));

        log.debug("getTask result {}", result);
        return result;
    }


    @Override
    public void performTask(@NotNull Task task,
                            @NotNull Action action,
                            @NotNull String userId,
                            @NotNull Folder folder,
                            @Nullable String deskId,
                            @Nullable String transactionId,
                            @Nullable String pastellDocumentId,
                            @Nullable String ipngBusinessId,
                            @Nullable SimpleTaskParams simpleTaskParams,
                            @Nullable String targetDeskId,
                            @Nullable String publicCertBase64) {

        // In 2 cases, we want to re-write the current candidate group :
        // - If we're in a OR parallel task, and we want to remove unused ones
        // - If we're in a delegation, and we want to rewrite everything to get the delegating & delegate candidates

        boolean isOrParallelTask = task.getDesks().size() > 1;
        boolean isSomeDelegation = task.getDesks().stream()
                .map(DeskRepresentation::getId)
                .noneMatch(id -> StringUtils.equals(id, deskId));
        String randomOriginalCandidateDeskId = task.getDesks().stream()
                .findFirst()
                .map(DeskRepresentation::getId)
                .orElse(null);

        // Variable building

        Map<String, String> variablesMap = new HashMap<>();

        variablesMap.put(METADATA_TRANSACTION_ID, transactionId);
        variablesMap.put(METADATA_PASTELL_DOCUMENT_ID, pastellDocumentId);
        variablesMap.put(METADATA_IPNG_BUSINESS_ID, ipngBusinessId);
        variablesMap.put(METADATA_TARGET_DELEGATE, targetDeskId);
        variablesMap.put(METADATA_PUBLIC_CERTIFICATE_BASE64, publicCertBase64);

        Optional<SimpleTaskParams> optionalSimpleTaskParams = Optional.ofNullable(simpleTaskParams);
        String publicAnnotation = optionalSimpleTaskParams.map(SimpleTaskParams::getPublicAnnotation).orElse(null);
        String privateAnnotation = optionalSimpleTaskParams.map(SimpleTaskParams::getPrivateAnnotation).orElse(null);
        variablesMap.put(METADATA_PUBLIC_ANNOTATION, publicAnnotation);
        variablesMap.put(METADATA_PRIVATE_ANNOTATION, privateAnnotation);

        // Request body

        PerformTaskRequest request = new PerformTaskRequest();
        request.setAction(IpWorkflowService.toIpWorkflowValue(action));
        request.setUserId(userId);

        if ((isSomeDelegation || isOrParallelTask) && StringUtils.isNotEmpty(deskId)) {
            request.setGroupId(deskId);
        }

        if (isSomeDelegation && StringUtils.isNotEmpty(randomOriginalCandidateDeskId)) {
            request.setDelegatedByGroupId(randomOriginalCandidateDeskId);
        }

        request.setVariables(
                variablesMap.entrySet().stream()
                        .filter(e -> StringUtils.isNotEmpty(e.getKey()))
                        .filter(e -> StringUtils.isNotEmpty(e.getValue()))
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue))
        );

        // Additional logic

        if (Objects.nonNull(simpleTaskParams) && MapUtils.isNotEmpty(simpleTaskParams.getMetadata())) {

            folder.getMetadata().putAll(simpleTaskParams.getMetadata());

            Map<String, String> filteredMetadata = folder.getMetadata()
                    .entrySet()
                    .stream()
                    .filter(entry -> !entry.getKey().contains(WORKFLOW_INTERNAL_VAR_PREFIX))
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

            Map<String, String> metadata = new HashMap<>(filteredMetadata);
            folder.setMetadata(metadata);
            // FIXME This is probably wrong
            //  this.editFolder(tenantId, folder.getId(), folder);
            this.editFolder(null, folder.getId(), folder);
        }

        new TaskApi(apiClient)
                .performTask(task.getId(), request)
                .doOnError(error -> {
                    log.error("Error while performing task {}: {}", task.getId(), error.getMessage());
                    throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, error, "message.cannot_reach_workflow_service");
                })
                .doOnSuccess(unused -> log.debug("task id:{} action:{} performed successfully", task.getId(), action))
                .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT);
    }


    @Override
    public void deleteWorkflow(@NotNull String folderId) {
        deleteInstance(folderId, false);
    }


    // <editor-fold desc="Archives CRUDL">


    @Override
    public @NotNull PaginatedList<? extends Folder> getArchives(@NotNull String tenantId,
                                                                @NotNull Pageable pageable,
                                                                @Nullable Long stillSinceDate) {

        long page = RequestUtils.getPageNumber(pageable);
        long pageSize = RequestUtils.getPageSize(pageable);
        FolderSortBy sortBy = RequestUtils.getFirstOderSort(pageable, FolderSortBy.class, FOLDER_NAME);
        boolean asc = RequestUtils.isFirstOrderAsc(pageable);

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(ARCHIVE_URL)
                .queryParam("page", page)
                .queryParam("tenantId", tenantId)
                .queryParam("pageSize", pageSize)
                .queryParam("sortBy", toIpWorkflowValue(sortBy))
                .queryParam("asc", asc)
                .queryParam("stillSinceDate", stillSinceDate)
                .build().normalize().toUri();


        return RequestUtils.getWebClientWithBufferSize(64, true)
                .get().uri(requestUri)
                .accept(APPLICATION_JSON)
                .retrieve()
                // Result
                .bodyToMono(new ParameterizedTypeReference<PaginatedList<IpWorkflowInstance>>() {})
                // TODO this probably could be a short timeout, to be tested
                .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service"));
    }


    @Override
    public void deleteArchive(@NotNull String folderId) {
        deleteInstance(folderId, true);
    }


    // </editor-fold desc="Archives CRUDL">


    @Override
    public void deleteInstance(@NotNull String folderId, boolean archive) {

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(archive ? ARCHIVE_URL : INSTANCE_URL).pathSegment(folderId)
                .build().normalize().toUri();

        log.debug("deleteWorkflow folderId:{} url:{}", folderId, requestUri.toString());

        WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
                .delete().uri(requestUri)
                .exchange()
                .doOnError(e -> {
                    log.error("Error calling deleteInstance on workflow service : {}", e.getMessage());
                    log.debug("error detail : ", e);
                    throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_workflow_service");
                })
                .block(SUBSERVICE_REQUEST_LONG_TIMEOUT);
    }


    // <editor-fold desc="IPNG lifecycle">


    @Override
    public void setFolderWaitingForIpngResponse(@NotNull IpngProof sentProof, @NotNull String tenantId, @NotNull Folder folder) {
        log.info("Proof sent successfully into IPNG network : {}. Referring folder id : {}", sentProof, folder.getId());
        PendingIpngFolder pendingFolder = new PendingIpngFolder(sentProof.getBusinessId(),
                tenantId,
                folder.getId(),
                sentProof.getReceiverDeskboxId(),
                PendingIpngFolder.State.SENT);
        this.pendingIpngFolderRepository.save(pendingFolder);
    }


    @Override
    public void receivedIpngResponse(String tenantId, String deskId, Folder folder, PendingIpngFolder pendingFolder, IpngProofWrap proofData) {
        log.info("receivedIpngResponse");

        Map<String, String> metadataToAdd = Map.of(META_IPNG_RESPONSE_ID, proofData.getId());
        Folder editRequest = new Folder();
        editRequest.setMetadata(metadataToAdd);
        editFolder(tenantId, pendingFolder.getFolderId(), editRequest);

        Optional<Task> matchingTask = folder.getStepList().stream()
                .filter(t -> t.getAction() == IPNG || t.getAction() == IPNG_RETURN)
                .filter(t -> FolderUtils.isActive(t.getState()))
                .filter(t -> t.getExternalState() == ExternalState.ACTIVE)
                .findFirst();

        log.info("matching tasks : {}", matchingTask.orElse(null));

        if (matchingTask.isPresent()) {
            this.performTask(matchingTask.get(), IPNG, folder, null, null, null, null);
            this.pendingIpngFolderRepository.delete(pendingFolder);
        } else {
            log.error("received a response from IPNG, with a matching folder but no active IPNG task awaiting.");
        }
    }


    @Override
    public void ipngProofReceiptWasReceived(String tenantId, String deskId, PendingIpngFolder pendingFolder, IpngProofWrap proofData) {
        log.info("ipngProofReceiptWasReceived");

        Map<String, String> metadataToAdd = Map.of(META_IPNG_PROOF_RECEIPT_ID, proofData.getId());
        Folder editRequest = new Folder();
        editRequest.setMetadata(metadataToAdd);
        editFolder(tenantId, pendingFolder.getFolderId(), editRequest);

        Folder folder = getFolder(pendingFolder.getFolderId(), tenantId, true);

        Optional<Task> matchingTask = folder.getStepList().stream()
                .filter(t -> t.getAction() == IPNG || t.getAction() == IPNG_RETURN)
                .filter(t -> FolderUtils.isActive(t.getState()))
                // TODO once we've updated externalState mechanics on workflow side
//                .filter(t -> t.getExternalState() == Task.ExternalState.ACTIVE)
                .findFirst();

        if (matchingTask.isPresent()) {
            log.info("Ok, proof was indeed sent");
            pendingFolder.setState(PendingIpngFolder.State.RECEIVED);
            this.pendingIpngFolderRepository.save(pendingFolder);

            // TODO once we've updated externalState mechanics on workflow side
//            this.performTask(matchingTask.get(), IPNG, null, null, deskId);
        } else {
            log.error("received a receipt from IPNG, with a matching folder but no active IPNG task awaiting.");
        }
    }


    @Override
    public void ipngProofWasSent(String tenantId, String deskId, PendingIpngFolder pendingFolder, IpngProofWrap proofData) {
        log.info("ipngProofWasSent");

        Map<String, String> metadataToAdd = new HashMap<>();
        metadataToAdd.put(META_IPNG_PROOF_SENT_ID, proofData.getId());
        metadataToAdd.put(META_IPNG_RECIPIENT_ENTITY_ID, proofData.getReceiverEntityId());
        metadataToAdd.put(META_IPNG_RECIPIENT_DBX_ID, proofData.getFolderExchange().getReceiverDeskboxId());

        Folder editRequest = new Folder();
        editRequest.setMetadata(metadataToAdd);
        editFolder(tenantId, pendingFolder.getFolderId(), editRequest);

        Folder folder = getFolder(pendingFolder.getFolderId(), tenantId, true);

        Optional<Task> matchingTask = folder.getStepList().stream()
                .filter(t -> t.getAction() == IPNG || t.getAction() == IPNG_RETURN)
                .filter(t -> FolderUtils.isActive(t.getState()))
                // TODO once we've updated externalState mechanics on workflow side
//                .filter(t -> t.getExternalState() == Task.ExternalState.ACTIVE)
                .findFirst();

        if (matchingTask.isPresent()) {
            log.info("Ok, proof was indeed sent");
            pendingFolder.setState(PendingIpngFolder.State.IN_NETWORK);
            this.pendingIpngFolderRepository.save(pendingFolder);
            // TODO once we've updated externalState mechanics on workflow side
//            this.performTask(matchingTask.get(), IPNG, null, null, deskId);
        } else {
            log.error("received a proof-sent notification from IPNG, with a matching folder but no active IPNG task awaiting.");
        }

    }


    // </editor-fold desc="IPNG lifecycle">


    public Page<WorkflowDefinition> getWorkflowsUsingDeskId(String deskId, String tenantId) {
        Pageable pageable = PageRequest.of(0, 3);

        return new DefinitionApi(apiClient)
                .getWorkflowDefinitionsByGroupId(deskId, tenantId, pageable.getPageNumber(), pageable.getPageSize(), List.of())
                .doOnError(error -> {
                    log.error("Error while retrieving workflow definitions used by desk {}: {}", deskId, error.getMessage());
                    throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, error, "message.cannot_reach_workflow_service");
                })
                .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT)
                .filter(pageWorkflowDefinition -> pageWorkflowDefinition.getContent() != null)
                .filter(pageWorkflowDefinition -> pageWorkflowDefinition.getTotalElements() != null)
                .map(pageWorkflowDefinition -> new PageImpl<>(
                        pageWorkflowDefinition.getContent().stream().map(workflow -> ipWorkflowModelMapper.map(workflow, WorkflowDefinition.class)).toList(),
                        pageable,
                        pageWorkflowDefinition.getTotalElements()))
                .orElseThrow(() -> {
                    log.error("Unexpected error while retrieving workflow definitions used by desk {}", deskId);
                    return new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service");
                });
    }


    public Page<Folder> getInstancesUsingOldWorkflowsByDeskId(String tenantId, String deskId) {
        Pageable pageable = PageRequest.of(0, 5);

        return new InstanceApi(apiClient)
                .getInstancesUsingOldWorkflowDefinitionsByGroupId(tenantId, deskId, pageable.getPageNumber(), pageable.getPageSize(), emptyList())
                .doOnError(error -> {
                    log.error("Error while retrieving folders referencing old workflow definitions used by desk {} : {}", deskId, error.getMessage());
                    throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, error, "message.cannot_reach_workflow_service");
                })
                .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT)
                .filter(pageInstance -> pageInstance.getContent() != null)
                .filter(pageInstance -> pageInstance.getTotalElements() != null)
                .map(pageInstance -> new PageImpl<>(
                        pageInstance.getContent().stream().map(instance -> ipWorkflowModelMapper.map(instance, Folder.class)).toList(),
                        pageable,
                        pageInstance.getTotalElements())
                )
                .orElseThrow(() -> {
                    log.error("Unexpected error while retrieving folders referencing old workflow definitions used by desk  {}", deskId);
                    return new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service");
                });
    }


    public Page<WorkflowDefinition> getWorkflowsUsingMetadataId(String metadataId, String tenantId) {
        Pageable pageable = PageRequest.of(0, 3);

        return new DefinitionApi(apiClient)
                .getWorkflowDefinitionsByMetadata(metadataId, tenantId, pageable.getPageNumber(), pageable.getPageSize(), List.of())
                .doOnError(error -> {
                    log.error("Error while retrieving workflow definitions using metadata {} : {}", metadataId, error.getMessage());
                    throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, error, "message.cannot_reach_workflow_service");
                })
                .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT)
                .filter(pageWorkflowDefinition -> pageWorkflowDefinition.getContent() != null)
                .filter(pageWorkflowDefinition -> pageWorkflowDefinition.getTotalElements() != null)
                .map(pageWorkflowDefinition -> new PageImpl<>(
                        pageWorkflowDefinition.getContent().stream().map(workflow -> ipWorkflowModelMapper.map(workflow, WorkflowDefinition.class)).toList(),
                        pageable,
                        pageWorkflowDefinition.getTotalElements()))
                .orElseThrow(() -> {
                    log.error("Unexpected error while retrieving workflow definitions using metadata {}", metadataId);
                    return new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service");
                });
    }


    public Page<Folder> getInstancesUsingOldWorkflowsByMetadataId(String metadataId, String tenantId) {
        Pageable pageable = PageRequest.of(0, 3);

        return new InstanceApi(apiClient)
                .getInstancesUsingOldWorkflowDefinitionsByMetadataId(tenantId, metadataId, pageable.getPageNumber(), pageable.getPageSize(), emptyList())
                .doOnError(error -> {
                    log.error("Error while retrieving folders referencing old workflow definitions using metadata {} : {}", metadataId, error.getMessage());
                    throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, error, "message.cannot_reach_workflow_service");
                })
                .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT)
                .filter(pageInstance -> pageInstance.getContent() != null)
                .filter(pageInstance -> pageInstance.getTotalElements() != null)
                .map(pageInstance -> new PageImpl<>(
                        pageInstance.getContent().stream().map(instance -> ipWorkflowModelMapper.map(instance, Folder.class)).toList(),
                        pageable,
                        pageInstance.getTotalElements()
                ))
                .orElseThrow(() -> {
                    log.error("Unexpected error while retrieving folders referencing old workflow definitions using metadata {}", metadataId);
                    return new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service");
                });

    }


    public Page<Task> getInstancesByMetadataValue(String metadataKey, String metadataValue, String tenantId) {
        Pageable pageable = PageRequest.of(0, 3);

        return new InstanceApi(apiClient)
                .getInstancesByMetadataValue(tenantId, metadataKey, metadataValue, pageable.getPageNumber(), pageable.getPageSize(), null)
                .doOnError(error -> {
                    log.error("Error while retrieving folders with metadata key -> value {} -> {} : {}", metadataKey, metadataValue, error.getMessage());
                    throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, error, "message.cannot_reach_workflow_service");
                })
                .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT)
                .filter(pageTask -> pageTask.getContent() != null)
                .filter(pageTask -> pageTask.getTotalElements() != null)
                .map(pageTask -> new PageImpl<>(
                        pageTask.getContent().stream().map(task -> ipWorkflowModelMapper.map(task, Task.class)).toList(),
                        pageable,
                        pageTask.getTotalElements()
                ))
                .orElseThrow(() -> {
                    log.error("Unexpected error while retrieving folders with metadata key -> value {} -> {}", metadataKey, metadataValue);
                    return new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service");
                });
    }


    @Override
    public Page<Task> getInstancesByTypology(String tenantId, String typeId, String subtypeId, Pageable pageable) {

        return new InstanceApi(apiClient)
                .getInstancesByTypology(tenantId, typeId, subtypeId, pageable.getPageNumber(), pageable.getPageSize(), null)
                .doOnError(error -> {
                    log.error("Error while retrieving folders by typology -> typeId: {}, subtypeId: {} : {}", typeId, subtypeId, error.getMessage());
                    throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, error, "message.cannot_reach_workflow_service");
                })
                .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT)
                .filter(pageTask -> pageTask.getData() != null)
                .filter(pageTask -> pageTask.getTotal() != null)
                .map(pageTask -> new PageImpl<>(
                        pageTask.getData().stream().map(task -> ipWorkflowModelMapper.map(task, Task.class)).toList(),
                        pageable,
                        pageTask.getTotal()
                ))
                .orElseThrow(() -> {
                    log.error("Unexpected error while retrieving folders by typology -> typeId: {}, subtypeId: {}", typeId, subtypeId);
                    return new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_workflow_service");
                });
    }

}
