/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.workflow;

import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.userPreferences.FolderFilter;
import coop.libriciel.ipcore.model.ipng.IpngProof;
import coop.libriciel.ipcore.model.ipng.IpngProofWrap;
import coop.libriciel.ipcore.model.ipng.PendingIpngFolder;
import coop.libriciel.ipcore.model.workflow.*;
import coop.libriciel.ipcore.model.workflow.requests.SimpleTaskParams;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.PaginatedList;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.io.Serial;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static coop.libriciel.ipcore.utils.TextUtils.NONE_SERVICE;
import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;


@Service(WorkflowServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = WorkflowServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = NONE_SERVICE)
public class NoneWorkflowService implements WorkflowServiceInterface {


    private static class NoWorkflowServiceError extends LocalizedStatusException {

        private static final @Serial long serialVersionUID = -8937873196479612969L;


        public NoWorkflowServiceError() {
            super(SERVICE_UNAVAILABLE, "message.workflow_service_not_available");
        }

    }


    @Override
    public void createWorkflowDefinition(@NotNull String tenantId, @NotNull WorkflowDefinition workflowDefinition) {
        throw new NoWorkflowServiceError();
    }


    @Override
    public @Nullable WorkflowDefinition getWorkflowDefinitionById(@NotNull String tenantId, @NotNull String id) {
        throw new NoWorkflowServiceError();
    }


    @Override
    public @NotNull Optional<WorkflowDefinition> getWorkflowDefinitionByKey(@NotNull String tenantId,
                                                                            @NotNull String key) {
        throw new NoWorkflowServiceError();
    }


    @Override
    public @NotNull Optional<WorkflowDefinition> getWorkflowDefinitionByKeyAndMapPlaceholders(@NotNull String tenantId,
                                                                                              @NotNull String key,
                                                                                              @Nullable String originDeskId) {
        throw new NoWorkflowServiceError();
    }


    @Override
    public void updateWorkflowDefinition(@NotNull String tenantId, @NotNull WorkflowDefinition workflowDefinition) {
        throw new NoWorkflowServiceError();
    }


    @Override
    public void deleteWorkflowDefinition(@NotNull String tenantId, @NotNull String workflowDefinitionKey) {
        throw new NoWorkflowServiceError();
    }


    @Override
    public @NotNull Page<WorkflowDefinitionRepresentation> listWorkflowDefinitions(@NotNull String tenantId,
                                                                                   @NotNull Pageable pageable,
                                                                                   @Nullable String searchTerm) {
        throw new NoWorkflowServiceError();
    }


    @Override
    public @NotNull PaginatedList<? extends Folder> listFoldersForDesks(@NotNull String tenantId,
                                                                        int page,
                                                                        int pageSize,
                                                                        FolderSortBy sortBy,
                                                                        @Nullable FolderFilter folderFilter,
                                                                        boolean asc,
                                                                        @NotNull List<String> deskIds,
                                                                        @Nullable Long emitBeforeTime,
                                                                        @Nullable Long stillSinceTime,
                                                                        @Nullable State state) {
        throw new NoWorkflowServiceError();
    }


    @Override
    public @NotNull Page<? extends Folder> listFolders(@NotNull String tenantId,
                                                       @NotNull Pageable pageable,
                                                       @Nullable FolderFilter folderFilter,
                                                       @Nullable String deskId,
                                                       @Nullable Long emitBeforeTime,
                                                       @Nullable Long stillSinceTime,
                                                       @Nullable State state) {
        throw new NoWorkflowServiceError();
    }


    @Override
    public @NotNull Page<Folder> listFoldersByState(@NotNull String role,
                                                    @NotNull State state,
                                                    @NotNull List<DelegationRule> delegationsRules,
                                                    @Nullable FolderFilter folderFilter,
                                                    @NotNull Pageable pageable) {
        throw new NoWorkflowServiceError();
    }


    @Override
    public @NotNull Map<DelegationRule, Integer> countFolders(@NotNull Set<String> roles, @NotNull List<DelegationRule> delegationRules) {
        throw new NoWorkflowServiceError();
    }


    @Override
    public @NotNull Integer countFolders(@NotNull String role, @NotNull State state) {
        throw new NoWorkflowServiceError();
    }


    @Override
    public @NotNull Folder createDraftWorkflow(@NotNull String tenantId, @NotNull Folder folder, Map<Integer, String> variableDesksIds) {
        throw new NoWorkflowServiceError();
    }


    @Override
    public @NotNull Folder getFolder(@NotNull String id, @NotNull String tenantId, boolean withHistory) {
        throw new NoWorkflowServiceError();
    }


    @Override
    public void editFolder(String tenantId, @NotNull String folderId, @NotNull Folder request) {
        throw new NoWorkflowServiceError();
    }


    @Override
    public @NotNull List<Task> getHistoricTasks(@NotNull String folderId) {
        throw new NoWorkflowServiceError();
    }


    @Override
    public @NotNull List<Task> getReadTasks(Folder folder) {
        throw new NoWorkflowServiceError();
    }


    @Override
    public @NotNull Task getTask(@NotNull String taskId) {
        throw new NoWorkflowServiceError();
    }


    @Override
    public void performTask(@NotNull Task task,
                            @NotNull Action action,
                            @NotNull String userId,
                            @NotNull Folder folder,
                            @Nullable String deskId,
                            @Nullable String transactionId,
                            @Nullable String pastellDocumentId,
                            @Nullable String ipngBusinessId,
                            @Nullable SimpleTaskParams simpleTaskParams,
                            @Nullable String targetDeskId,
                            @Nullable String certBase64) {
        throw new NoWorkflowServiceError();
    }


    @Override
    public void deleteWorkflow(@NotNull String folderId) {
        throw new NoWorkflowServiceError();
    }


    @Override
    public void deleteArchive(@NotNull String folderId) {
        throw new NoWorkflowServiceError();
    }


    @Override
    public void deleteInstance(@NotNull String folderId, boolean archive) { throw new NoWorkflowServiceError(); }


    @Override
    public void setFolderWaitingForIpngResponse(@NotNull IpngProof sentProof, @NotNull String tenantId, @NotNull Folder folder) {
        throw new NoWorkflowServiceError();
    }


    @Override
    public @NotNull PaginatedList<? extends Folder> getArchives(@NotNull String tenantId,
                                                                @NotNull Pageable pageable,
                                                                @Nullable Long stillSinceDate) {
        throw new NoWorkflowServiceError();
    }


    @Override
    public void receivedIpngResponse(String tenantId, String deskId, Folder folder, PendingIpngFolder pendingFolder, IpngProofWrap proofData) {
        throw new NoWorkflowServiceError();
    }


    @Override
    public void ipngProofReceiptWasReceived(String tenantId, String deskId, PendingIpngFolder pendingFolder, IpngProofWrap proofData) {
        throw new NoWorkflowServiceError();
    }


    @Override
    public void ipngProofWasSent(String tenantId, String deskId, PendingIpngFolder pendingFolder, IpngProofWrap proofData) {
        throw new NoWorkflowServiceError();
    }


    @Override
    public Map<String, String> computePlaceholderDesksConcreteValues(@NotNull WorkflowDefinition definition,
                                                                     @NotNull String originDeskId,
                                                                     @NotNull Map<Integer, String> variableDesksIdsMap) {
        throw new NoWorkflowServiceError();
    }


    @Override
    public void mapIndexedPlaceholdersToActualDesk(@NotNull DeskRepresentation finalDesk,
                                                   @NotNull String originDeskId,
                                                   @NotNull Map<String, String> additionalMetadata) {
        throw new NoWorkflowServiceError();
    }


    @Override
    public void substitutePlaceholdersToIndexedPlaceholders(@NotNull WorkflowDefinition definition) {
        throw new NoWorkflowServiceError();
    }


    @Override
    public Page<WorkflowDefinition> getWorkflowsUsingDeskId(String tenantId, String deskId) {throw new NoWorkflowServiceError();}


    public Page<Folder> getInstancesUsingOldWorkflowsByDeskId(String tenantId, String deskId) {throw new NoWorkflowServiceError();}


    @Override
    public Page<WorkflowDefinition> getWorkflowsUsingMetadataId(String metadataId, String tenantId) {
        return null;
    }


    @Override
    public Page<Folder> getInstancesUsingOldWorkflowsByMetadataId(String metadataId, String tenantId) {
        return null;
    }


    @Override
    public Page<Task> getInstancesByMetadataValue(String metadataKey, String metadataValue, String tenantId) {
        return null;
    }


    @Override
    public Page<Task> getInstancesByTypology(String tenantId, String typeId, String subtypeId, Pageable pageable) {
        return null;
    }
}
