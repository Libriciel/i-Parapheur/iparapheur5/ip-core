/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.services.workflow;

import coop.libriciel.ipcore.model.workflow.LegacyIdRecord;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface LegacyIdRepository extends CrudRepository<LegacyIdRecord, String>, PagingAndSortingRepository<LegacyIdRecord, String> {

    @NotNull
    @Override
    Optional<LegacyIdRecord> findById(@NotNull String s);

    @Override
    <S extends LegacyIdRecord> @NotNull S save(@NotNull S legacyIdRecord);

}
