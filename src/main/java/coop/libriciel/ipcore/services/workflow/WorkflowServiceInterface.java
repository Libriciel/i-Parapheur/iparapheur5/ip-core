/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.workflow;

import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.database.userPreferences.FolderFilter;
import coop.libriciel.ipcore.model.externalsignature.ExternalSignatureParams;
import coop.libriciel.ipcore.model.ipng.IpngProof;
import coop.libriciel.ipcore.model.ipng.IpngProofWrap;
import coop.libriciel.ipcore.model.ipng.PendingIpngFolder;
import coop.libriciel.ipcore.model.securemail.MailParams;
import coop.libriciel.ipcore.model.workflow.*;
import coop.libriciel.ipcore.model.workflow.requests.SimpleTaskParams;
import coop.libriciel.ipcore.utils.KeycloakSecurityUtils;
import coop.libriciel.ipcore.utils.PaginatedList;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.*;
import java.util.regex.Pattern;

import static coop.libriciel.ipcore.model.workflow.Action.*;
import static coop.libriciel.ipcore.utils.TextUtils.INTERNAL_PREFIX;
import static coop.libriciel.ipcore.utils.UserUtils.AUTOMATIC_TASK_USER_ID;


public interface WorkflowServiceInterface {


    String BEAN_NAME = "workflowService";
    String PREFERENCES_PROVIDER_KEY = "services.workflow.provider";

    String META_CONTENT_ID = INTERNAL_PREFIX + "content_id";
    String META_TYPE_ID = INTERNAL_PREFIX + "type_id";
    String META_SUBTYPE_ID = INTERNAL_PREFIX + "subtype_id";
    String META_PREMIS_NODE_ID = INTERNAL_PREFIX + "premis_node_id";

    String META_IPNG_RECIPIENT_DBX_ID = INTERNAL_PREFIX + "ipng_recipient_dbx_id";
    String META_IPNG_RECIPIENT_ENTITY_ID = INTERNAL_PREFIX + "ipng_recipient_entity_id";
    String META_IPNG_PROOF_SENT_ID = INTERNAL_PREFIX + "ipng_proof_sent_id";
    String META_IPNG_PROOF_RECEIPT_ID = INTERNAL_PREFIX + "ipng_proof_receipt_id";
    String META_IPNG_RESPONSE_ID = INTERNAL_PREFIX + "ipng_response_id";

    // To use formatted with the task index
    String META_BOSS_OF_DESKID_X_FORMAT = INTERNAL_PREFIX + "validation_boss_of_desk_id_%d";
    Pattern META_BOSSOF_PATTERN = Pattern.compile("^\\$\\{(" + INTERNAL_PREFIX + "validation_boss_of_desk_id_\\d+)\\}$");

    String META_VARIABLE_DESK_DESKID_X_FORMAT = INTERNAL_PREFIX + "validation_variable_desk_id_%d";
    Pattern META_VARIABLEDESK_PATTERN = Pattern.compile("^\\$\\{(" + INTERNAL_PREFIX + "validation_variable_desk_id_\\d+)\\}$");

    String META_EMITTER_ID = INTERNAL_PREFIX + "emitter_id";
    Pattern META_EMITTER_PATTERN = Pattern.compile("^\\$\\{" + META_EMITTER_ID + "\\}$");


    /**
     * For some reason, the v4 behaviour is a read state, that is reset at every step.
     * We may want to change that in the future, so we'll leave an explicit method name.
     *
     * @param currentIndex A workflowIndex/stepIndex pair
     * @param currentUser
     * @param readTasks
     * @return
     */
    default boolean hasAlreadyBeenReadAtIndex(@NotNull Pair<Long, Long> currentIndex, @NotNull User currentUser, @NotNull List<Task> readTasks) {
        return readTasks.stream()
                .filter(t -> Objects.equals(t.getWorkflowIndex(), currentIndex.getLeft()))
                .filter(t -> Objects.equals(t.getStepIndex(), currentIndex.getRight()))
                .filter(t -> t.getUser() != null)
                .anyMatch(t -> StringUtils.equals(currentUser.getId(), t.getUser().getId()));
    }


    // <editor-fold desc="Workflow definition CRUDL">


    // void createWorkflowDefinition(@NotNull String tenantId, @NotNull String workflowDefinition, @NotNull String workflowName);


    void createWorkflowDefinition(@NotNull String tenantId, @NotNull WorkflowDefinition workflowDefinition);


    @Nullable WorkflowDefinition getWorkflowDefinitionById(@NotNull String tenantId, @NotNull String id);


    /**
     * Retrieving every {@link StepDefinition} for the given {@link WorkflowDefinition}'s key.
     *
     * @param tenantId (self-explanatory)
     * @param key      The workflow definition key
     * @return A {@link WorkflowDefinition} object, wrapping every {@link StepDefinition}.
     */
    @NotNull Optional<WorkflowDefinition> getWorkflowDefinitionByKey(@NotNull String tenantId, @NotNull String key);


    /**
     * Retrieving every {@link StepDefinition} for the given {@link WorkflowDefinition}'s key, and compute variable desks values if originDeskId is provided
     * Typical use case :
     * - On instance draft creation, we'll want a glimpse of the workflow, with the actual values.
     *
     * @param tenantId
     * @param key
     * @param originDeskId The Id of the desk that would start the workflow
     * @return A {@link WorkflowDefinition} object, wrapping every {@link StepDefinition}.
     */
    @NotNull Optional<WorkflowDefinition> getWorkflowDefinitionByKeyAndMapPlaceholders(@NotNull String tenantId,
                                                                                       @NotNull String key,
                                                                                       @Nullable String originDeskId);

    void updateWorkflowDefinition(@NotNull String tenantId, @NotNull WorkflowDefinition workflowDefinition);


    void deleteWorkflowDefinition(@NotNull String tenantId, @NotNull String workflowDefinitionKey);


    @NotNull Page<WorkflowDefinitionRepresentation> listWorkflowDefinitions(@NotNull String tenantId, @NotNull Pageable pageable, @Nullable String searchTerm);


    // </editor-fold desc="Workflow definition CRUDL">


    @NotNull PaginatedList<? extends Folder> listFoldersForDesks(@NotNull String tenantId,
                                                                 int page,
                                                                 int pageSize,
                                                                 FolderSortBy sortBy,
                                                                 @Nullable FolderFilter folderFilter,
                                                                 boolean asc,
                                                                 @NotNull List<String> deskIds,
                                                                 @Nullable Long emitBeforeTime,
                                                                 @Nullable Long stillSinceTime,
                                                                 @Nullable State state);


    @NotNull Page<? extends Folder> listFolders(@NotNull String tenantId,
                                                @NotNull Pageable pageable,
                                                @Nullable FolderFilter folderFilter,
                                                @Nullable String deskId,
                                                @Nullable Long emitBeforeTime,
                                                @Nullable Long stillSinceTime,
                                                @Nullable State state);


    @NotNull Page<Folder> listFoldersByState(@NotNull String role,
                                             @NotNull State state,
                                             @NotNull List<DelegationRule> delegationsRules,
                                             @Nullable FolderFilter folderFilter,
                                             @NotNull Pageable pageable);


    @NotNull Map<DelegationRule, Integer> countFolders(@NotNull Set<String> roles, @NotNull List<DelegationRule> delegationRules);


    @NotNull Integer countFolders(@NotNull String role, @NotNull State state);


    // </editor-fold desc="Lists">


    @NotNull Folder createDraftWorkflow(@NotNull String tenantId, @NotNull Folder folder, Map<Integer, String> variableDesksIds);


    @NotNull Folder getFolder(@NotNull String id, @NotNull String tenantId, boolean withHistory);


    void editFolder(String tenantId, @NotNull String folderId, @NotNull Folder folderParams);


    @NotNull List<Task> getHistoricTasks(@NotNull String folderId);


    @NotNull List<Task> getReadTasks(Folder folder);


    @NotNull Task getTask(@NotNull String taskId);


    default void performTask(@NotNull Action action,
                             @NotNull Task task,
                             @Nullable String deskId,
                             @NotNull Folder folder,
                             @Nullable SimpleTaskParams simpleTaskParams) {

        String userId = KeycloakSecurityUtils.getCurrentSessionUserId();
        performTask(
                task,
                action,
                userId,
                folder,
                deskId,
                null,
                null,
                null,
                simpleTaskParams,
                null,
                null
        );
    }


    default void performExternalSignature(@NotNull Task task,
                                          @NotNull User user,
                                          @NotNull Folder folder,
                                          @Nullable ExternalSignatureParams externalSignatureParams,
                                          @Nullable String transactionId) {
        performTask(
                task,
                EXTERNAL_SIGNATURE,
                user.getId(),
                folder,
                null,
                transactionId,
                null,
                null,
                externalSignatureParams,
                null,
                null
        );
    }


    default void performIpng(@NotNull Task task,
                             @NotNull Folder folder,
                             @Nullable String businessId,
                             String userId,
                             @Nullable SimpleTaskParams simpleTaskParams) {
        performTask(
                task,
                IPNG,
                userId,
                folder,
                null,
                null,
                null,
                businessId,
                simpleTaskParams,
                null,
                null
        );
    }


    default void performSecureMail(@NotNull Task task,
                                   @NotNull User user,
                                   @NotNull Folder folder,
                                   @Nullable MailParams mailParams,
                                   @NotNull String pastellDocumentId) {
        performTask(
                task,
                SECURE_MAIL,
                user.getId(),
                folder,
                null,
                null,
                pastellDocumentId,
                null,
                mailParams,
                null,
                null
        );
    }


    default void performTask(@NotNull Task task,
                             @NotNull Action action,
                             @NotNull Folder folder,
                             @Nullable String deskId,
                             @Nullable SimpleTaskParams simpleTaskParams,
                             @Nullable String targetDesk,
                             @Nullable String publicCertBase64) {

        String userId = Optional.ofNullable(KeycloakSecurityUtils.getCurrentSessionUserIdNoException())
                .orElse(AUTOMATIC_TASK_USER_ID);

        performTask(
                task,
                action,
                userId,
                folder,
                deskId,
                null,
                null,
                null,
                simpleTaskParams,
                targetDesk,
                publicCertBase64
        );
    }


    void performTask(@NotNull Task task,
                     @NotNull Action action,
                     @NotNull String userId,
                     @NotNull Folder folder,
                     @Nullable String deskId,
                     @Nullable String transactionId,
                     @Nullable String pastellDocumentId,
                     @Nullable String ipngBusinessId,
                     @Nullable SimpleTaskParams simpleTaskParams,
                     @Nullable String targetDeskId,
                     @Nullable String certBase64);


    void deleteWorkflow(@NotNull String folderId);


    // <editor-fold desc="Archives CRUDL">


    void deleteArchive(@NotNull String folderId);


    void deleteInstance(@NotNull String folderId, boolean archive);

    void setFolderWaitingForIpngResponse(@NotNull IpngProof sentProof, @NotNull String tenantId, @NotNull Folder folder);


    @NotNull PaginatedList<? extends Folder> getArchives(@NotNull String tenantId, @NotNull Pageable pageable, @Nullable Long stillSinceDate);


    // </editor-fold desc="Archives CRUDL">


    void receivedIpngResponse(String tenantId, String deskId, Folder folder, PendingIpngFolder pendingFolder, IpngProofWrap proofData);


    void ipngProofReceiptWasReceived(String tenantId, String deskId, PendingIpngFolder pendingFolder, IpngProofWrap proofData);


    void ipngProofWasSent(String tenantId, String deskId, PendingIpngFolder pendingFolder, IpngProofWrap proofData);


    Map<String, String> computePlaceholderDesksConcreteValues(@NotNull WorkflowDefinition definition,
                                                              @NotNull String originDeskId,
                                                              @NotNull Map<Integer, String> variableDesksIdsMap);


    void mapIndexedPlaceholdersToActualDesk(@NotNull DeskRepresentation finalDesk,
                                            @NotNull String originDeskId,
                                            @NotNull Map<String, String> additionalMetadata);


    void substitutePlaceholdersToIndexedPlaceholders(@NotNull WorkflowDefinition definition);


    Page<WorkflowDefinition> getWorkflowsUsingDeskId(String tenantId, String deskId) throws Exception;


    Page<Folder> getInstancesUsingOldWorkflowsByDeskId(String tenantId, String deskId);


    Page<WorkflowDefinition> getWorkflowsUsingMetadataId(String metadataId, String tenantId);


    Page<Folder> getInstancesUsingOldWorkflowsByMetadataId(String metadataId, String tenantId);


    Page<Task> getInstancesByMetadataValue(String metadataKey, String metadataValue, String tenantId);


    Page<Task> getInstancesByTypology(String tenantId, String typeId, String subtypeId, Pageable pageable);


}
