/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.securemail;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.securemail.*;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.database.SubtypeRepository;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.FolderUtils;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.UserUtils;
import coop.libriciel.ipcore.utils.exceptions.LocalizedNotFoundStatusException;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;

import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static coop.libriciel.ipcore.model.workflow.Action.SECURE_MAIL;
import static coop.libriciel.ipcore.services.workflow.IpWorkflowService.METADATA_PASTELL_DOCUMENT_ID;
import static coop.libriciel.ipcore.utils.RequestUtils.SUBSERVICE_REQUEST_LONG_TIMEOUT;
import static coop.libriciel.ipcore.utils.RequestUtils.SUBSERVICE_REQUEST_SHORT_TIMEOUT;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA;
import static org.springframework.security.config.Elements.HTTP;


@Log4j2
@Service(SecureMailServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = SecureMailServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = "pastell-connector")
public class SecureMailService implements SecureMailServiceInterface, MessageListener {


    private static final String PASTELL_CONNECTOR_BASE_URL = "pastell-connector/api/server";
    private static final String PASTELL_CONNECTOR_TEST_SERVER_URL = PASTELL_CONNECTOR_BASE_URL + "/test";
    private static final String PASTELL_CONNECTOR_SERVER_ENTITIES_URL = PASTELL_CONNECTOR_BASE_URL + "/entities";
    private static final String PASTELL_CONNECTOR_DOCUMENT_URL = "document";


    // <editor-fold desc="Beans">


    private final ContentServiceInterface contentService;
    private final SubtypeRepository subtypeRepository;
    private final WorkflowServiceInterface workflowService;
    private final SecureMailServiceProperties properties;

    private final ObjectMapper objectMapper;


    @Autowired
    public SecureMailService(ContentServiceInterface contentService,
                             SubtypeRepository subtypeRepository,
                             WorkflowServiceInterface workflowService,
                             SecureMailServiceProperties properties) {
        this.contentService = contentService;
        this.subtypeRepository = subtypeRepository;
        this.workflowService = workflowService;
        this.properties = properties;
        this.objectMapper = new ObjectMapper();
    }


    // </editor-fold desc="Beans">


    @Override
    public @NotNull SecureMailServer createServer(@NotNull SecureMailServer secureMailServer) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(PASTELL_CONNECTOR_BASE_URL)
                .build().normalize().toUri();
        return WebClient.builder()
                .build()
                .post()
                .uri(requestUri)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .bodyValue(objectMapper.writeValueAsString(secureMailServer))
                .retrieve()
                .bodyToMono(SecureMailServer.class)
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_pastell_connector"));
    }


    @Override
    public boolean testServer(@NotNull SecureMailServer secureMailServer) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(PASTELL_CONNECTOR_TEST_SERVER_URL)
                .build().normalize().toUri();
        return WebClient.builder()
                .build()
                .post()
                .uri(requestUri)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .bodyValue(objectMapper.writeValueAsString(secureMailServer))
                .retrieve()
                .bodyToMono(boolean.class)
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_pastell_connector"));
    }


    @Override
    public @NotNull List<SecureMailServer> findAllServers(@NotNull String tenantId) {
        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(PASTELL_CONNECTOR_BASE_URL)
                .queryParam("tenantId", tenantId)
                .build().normalize().toUri();
        return Arrays.asList(Objects.requireNonNull(WebClient.builder()
                .build()
                .get()
                .uri(requestUri)
                .retrieve()
                .bodyToMono(SecureMailServer[].class)
                .block(SUBSERVICE_REQUEST_SHORT_TIMEOUT)));
    }


    @Override
    public SecureMailServer findServer(@NotNull String tenantId, @NotNull Long id) {
        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(PASTELL_CONNECTOR_BASE_URL)
                .pathSegment(id.toString())
                .queryParam("tenantId", tenantId)
                .build().normalize().toUri();
        return Objects.requireNonNull(WebClient.builder()
                .build()
                .get()
                .uri(requestUri)
                .retrieve()
                .bodyToMono(SecureMailServer.class)
                .block(SUBSERVICE_REQUEST_SHORT_TIMEOUT));
    }


    @Override
    public void deleteServer(@NotNull String tenantId, @NotNull Long id) {
        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(PASTELL_CONNECTOR_BASE_URL)
                .pathSegment(id.toString())
                .queryParam("tenantId", tenantId)
                .build().normalize().toUri();
        Objects.requireNonNull(WebClient.builder()
                .build()
                .delete()
                .uri(requestUri)
                .exchange()
                .block(SUBSERVICE_REQUEST_SHORT_TIMEOUT));
    }


    @Override
    public @NotNull SecureMailServer updateServerObject(@NotNull String tenantId, @NotNull SecureMailServer secureMailServer) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(PASTELL_CONNECTOR_BASE_URL)
                .pathSegment(secureMailServer.getId().toString())
                .queryParam("tenantId", tenantId)
                .build().normalize().toUri();
        return WebClient.builder()
                .build()
                .post()
                .uri(requestUri)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .bodyValue(objectMapper.writeValueAsString(secureMailServer))
                .retrieve()
                .bodyToMono(SecureMailServer.class)
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_pastell_connector"));
    }


    @Override
    public @NotNull SecureMailServer updateServer(@NotNull String tenantId,
                                                  @NotNull Long id,
                                                  String url,
                                                  String login,
                                                  String password,
                                                  String sslPublicKey,
                                                  int entity,
                                                  String type) {
        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(PASTELL_CONNECTOR_BASE_URL)
                .pathSegment(id.toString())
                .queryParam("tenantId", tenantId)
                .queryParam("url", url)
                .queryParam("login", login)
                .queryParam("password", password)
                .queryParam("sslPublicKey", sslPublicKey)
                .queryParam("entity", entity)
                .queryParam("type", type)
                .build().normalize().toUri();
        return WebClient.builder()
                .build()
                .patch()
                .uri(requestUri)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .retrieve()
                .bodyToMono(SecureMailServer.class)
                .blockOptional(SUBSERVICE_REQUEST_SHORT_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_pastell_connector"));
    }


    @Override
    public @NotNull SecureMailDocument createAndSendSecureMail(@NotNull Tenant tenant,
                                                               @NotNull Folder folder,
                                                               @NotNull MailParams mailParams,
                                                               @NotNull List<Document> documents,
                                                               Flux<DataBuffer> docketFlux) {

        Subtype subtype = subtypeRepository.findById(folder.getSubtype().getId())
                .orElseThrow(() -> new LocalizedStatusException(BAD_REQUEST, "message.unknown_subtype_id"));

        SecureMailServer secureMailServer =
                Optional.ofNullable(this.findServer(tenant.getId(), subtype.getSecureMailServerId()))
                        .orElseThrow(() -> new LocalizedStatusException(BAD_REQUEST, "message.unknown_mailsec_config_id"));

        MultipartBodyBuilder multipartBodyBuilder = new MultipartBodyBuilder();
        multipartBodyBuilder.part("login", secureMailServer.getLogin());
        for (String to : mailParams.getTo()) {
            multipartBodyBuilder.part("to", to);
        }
        for (String cc : mailParams.getCc()) {
            multipartBodyBuilder.part("cc", cc);
        }
        for (String bcc : mailParams.getBcc()) {
            multipartBodyBuilder.part("bcc", bcc);
        }
        multipartBodyBuilder.part("object", mailParams.getObject());
        multipartBodyBuilder.part("message", mailParams.getMessage());
        multipartBodyBuilder.part("payload", folder.getId());

        for (Document document : documents) {
            DocumentBuffer documentBuffer = this.contentService.retrieveContent(document.getId());
            multipartBodyBuilder.asyncPart("files", documentBuffer.getContentFlux(), DataBuffer.class)
                    .header(CONTENT_DISPOSITION, "form-data; name=files; filename=" + document.getName());
        }

        if (mailParams.isIncludeDocket()) {
            multipartBodyBuilder.asyncPart("files", docketFlux, DataBuffer.class)
                    .header(CONTENT_DISPOSITION, "form-data; name=files; filename=iParapheur_impression_dossier.pdf");
        }


        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(PASTELL_CONNECTOR_BASE_URL)
                .pathSegment(subtype.getSecureMailServerId().toString())
                .pathSegment(PASTELL_CONNECTOR_DOCUMENT_URL)
                .build().normalize().toUri();
        return WebClient.builder()
                .build()
                .post()
                .uri(requestUri)
                .contentType(MULTIPART_FORM_DATA)
                .accept(APPLICATION_JSON)
                .bodyValue(multipartBodyBuilder.build())
                .retrieve()
                .bodyToMono(SecureMailDocument.class)
                .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_pastell_connector"));
    }


    private void deleteDocument(@NotNull String secureMailServerId, @NotNull String id) {
        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(PASTELL_CONNECTOR_BASE_URL)
                .pathSegment(secureMailServerId)
                .pathSegment(PASTELL_CONNECTOR_DOCUMENT_URL)
                .pathSegment(id)
                .build().normalize().toUri();
        Objects.requireNonNull(WebClient.builder()
                .build()
                .delete()
                .uri(requestUri)
                .exchange()
                .block(SUBSERVICE_REQUEST_SHORT_TIMEOUT));
    }


    @Override
    public SecureMailDocument findDocument(@NotNull String secureMailServerId, @NotNull String id) {
        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(PASTELL_CONNECTOR_BASE_URL)
                .pathSegment(secureMailServerId)
                .pathSegment(PASTELL_CONNECTOR_DOCUMENT_URL)
                .pathSegment(id)
                .build().normalize().toUri();
        return WebClient.builder()
                .build()
                .get()
                .uri(requestUri)
                .retrieve()
                .bodyToMono(SecureMailDocument.class)
                .block(SUBSERVICE_REQUEST_SHORT_TIMEOUT);
    }


    @Override
    public List<SecureMailEntity> findAllPastellEntities(@NotNull SecureMailServer secureMailServer) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(PASTELL_CONNECTOR_SERVER_ENTITIES_URL)
                .build().normalize().toUri();
        return WebClient.builder()
                .build()
                .post()
                .uri(requestUri)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .bodyValue(objectMapper.writeValueAsString(secureMailServer))
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<SecureMailEntity>>() {})
                // TODO this probably could be a short timeout, to be tested
                .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_pastell_connector"));
    }


    @Override
    public void rejectExternalProcedure(@NotNull Folder folder, @NotNull Task task) {
        Subtype subtype = this.subtypeRepository.findById(folder.getSubtype().getId())
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.unknown_subtype_id"));

        if (task.getAction().equals(SECURE_MAIL)
                && Objects.nonNull(subtype.getSecureMailServerId())
                && task.getMetadata().containsKey(METADATA_PASTELL_DOCUMENT_ID)) {

            this.deleteDocument(
                    subtype.getSecureMailServerId().toString(),
                    task.getMetadata().get(METADATA_PASTELL_DOCUMENT_ID)
            );
        }
    }


    @Override
    public void onMessage(Message message, byte[] pattern) {
        DocumentMap documentMap = null;
        try {
            documentMap = objectMapper.readValue(message.toString(), DocumentMap.class);
            final DocumentMap finalDocumentMap = documentMap;
            Folder folder = workflowService.getFolder(documentMap.getPayload(), documentMap.getTenantId(), true);
            Task task = folder.getStepList().stream()
                    .filter(t -> FolderUtils.isActive(t.getState()))
                    .filter(t -> t.getMetadata().containsKey(METADATA_PASTELL_DOCUMENT_ID))
                    .filter(t -> t.getMetadata().get(METADATA_PASTELL_DOCUMENT_ID).equals(finalDocumentMap.getDocumentId()))
                    .findFirst()
                    .orElse(null);

            if (task != null) {
                User automaticUser = UserUtils.getAutomaticUser();
                workflowService.performSecureMail(task, automaticUser, folder, null, documentMap.getDocumentId());
            }

        } catch (JsonProcessingException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_parse_redis_message");
        } catch (LocalizedNotFoundStatusException e) {
            log.error("A securemail-related message came back but the associated folder with id '{}' could not be found.",
                    documentMap != null ? documentMap.getPayload() : "<error parsing redis message>");

            String pastellConnectorDocId = documentMap.getDocumentId();
            long pastellConnectorServerId = documentMap.getServerId();

            this.deleteDocument(Long.toString(pastellConnectorServerId), pastellConnectorDocId);
        }
    }


}
