/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.securemail;

import com.fasterxml.jackson.core.JsonProcessingException;
import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.securemail.MailParams;
import coop.libriciel.ipcore.model.securemail.SecureMailDocument;
import coop.libriciel.ipcore.model.securemail.SecureMailEntity;
import coop.libriciel.ipcore.model.securemail.SecureMailServer;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import org.jetbrains.annotations.NotNull;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.core.io.buffer.DataBuffer;
import reactor.core.publisher.Flux;

import java.util.List;


public interface SecureMailServiceInterface {


    String BEAN_NAME = "secureMailService";
    String PREFERENCES_PROVIDER_KEY = "services.secure-mail.provider";


    @NotNull SecureMailServer createServer(@NotNull SecureMailServer secureMailServer) throws JsonProcessingException;

    boolean testServer(@NotNull SecureMailServer secureMailServer) throws JsonProcessingException;

    @NotNull List<SecureMailServer> findAllServers(@NotNull String tenantId);

    SecureMailServer findServer(@NotNull String tenantId, @NotNull Long id);

    void deleteServer(@NotNull String tenantId, @NotNull Long id);

    @NotNull SecureMailServer updateServerObject(@NotNull String tenantId, @NotNull SecureMailServer secureMailServer) throws JsonProcessingException;

    @NotNull SecureMailServer updateServer(@NotNull String tenantId,
                                           @NotNull Long id,
                                           String url,
                                           String login,
                                           String password,
                                           String sslPublicKey,
                                           int entity,
                                           String type) throws JSONException;

    SecureMailDocument findDocument(@NotNull String secureMailServerId, @NotNull String id);

    List<SecureMailEntity> findAllPastellEntities(@NotNull SecureMailServer secureMailServer) throws JsonProcessingException;

    void rejectExternalProcedure(@NotNull Folder folder, @NotNull Task task);


    @NotNull SecureMailDocument createAndSendSecureMail(@NotNull Tenant tenant,
                                                        @NotNull Folder folder,
                                                        @NotNull MailParams mailParams,
                                                        @NotNull List<Document> documents,
                                                        Flux<DataBuffer> docketFlux);

}
