/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.securemail;

import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.securemail.MailParams;
import coop.libriciel.ipcore.model.securemail.SecureMailDocument;
import coop.libriciel.ipcore.model.securemail.SecureMailEntity;
import coop.libriciel.ipcore.model.securemail.SecureMailServer;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import org.jetbrains.annotations.NotNull;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.io.Serial;
import java.util.List;

import static coop.libriciel.ipcore.utils.TextUtils.NONE_SERVICE;
import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;


@Service(SecureMailServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = SecureMailServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = NONE_SERVICE)
public class NoneSecureMailService implements SecureMailServiceInterface {


    private static class NoSecureMailServiceError extends LocalizedStatusException {

        private static final @Serial long serialVersionUID = -8100420598078023683L;


        public NoSecureMailServiceError() {
            super(SERVICE_UNAVAILABLE, "message.secure_mail_service_not_available");
        }

    }


    @Override
    public @NotNull SecureMailServer createServer(@NotNull SecureMailServer secureMailServer) {
        throw new NoSecureMailServiceError();
    }


    @Override
    public boolean testServer(@NotNull SecureMailServer secureMailServer) {
        throw new NoSecureMailServiceError();
    }


    @Override
    public @NotNull List<SecureMailServer> findAllServers(@NotNull String tenantId) {
        throw new NoSecureMailServiceError();
    }


    @Override
    public SecureMailServer findServer(@NotNull String tenantId, @NotNull Long id) {
        throw new NoSecureMailServiceError();
    }


    @Override
    public void deleteServer(@NotNull String tenantId, @NotNull Long id) {
        throw new NoSecureMailServiceError();
    }


    @Override
    public @NotNull SecureMailServer updateServerObject(@NotNull String tenantId, @NotNull SecureMailServer secureMailServer) {
        throw new NoSecureMailServiceError();
    }


    @Override
    public @NotNull SecureMailServer updateServer(@NotNull String tenantId,
                                                  @NotNull Long id,
                                                  String url,
                                                  String login,
                                                  String password,
                                                  String sslPublicKey,
                                                  int entity,
                                                  String type) {
        throw new NoSecureMailServiceError();
    }


    @Override
    public SecureMailDocument findDocument(@NotNull String secureMailServerId, @NotNull String id) {
        throw new NoSecureMailServiceError();
    }


    @Override
    public List<SecureMailEntity> findAllPastellEntities(@NotNull SecureMailServer secureMailServer) {
        throw new NoSecureMailServiceError();
    }


    @Override
    public void rejectExternalProcedure(@NotNull Folder folder, @NotNull Task task) {
        throw new NoSecureMailServiceError();
    }


    @Override
    public @NotNull SecureMailDocument createAndSendSecureMail(@NotNull Tenant tenant,
                                                               @NotNull Folder folder,
                                                               @NotNull MailParams mailParams,
                                                               @NotNull List<Document> documents,
                                                               Flux<DataBuffer> docketFlux) {
        throw new NoSecureMailServiceError();
    }


}
