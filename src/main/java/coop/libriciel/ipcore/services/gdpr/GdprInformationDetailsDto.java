/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.gdpr;

import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

import static coop.libriciel.ipcore.utils.ApiUtils.*;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ConfigurationProperties(prefix = "gdpr")
public class GdprInformationDetailsDto {


    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GdprEntity {

        // TODO : Validation for subclasses is not being taken into account.
        @Size(max = GDPR_FIELD_MAX_LENGTH, message = GDPR_NAME_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
        private String name;
        @Size(max = GDPR_FIELD_MAX_LENGTH, message = GDPR_ADDRESS_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
        private String address;
        @Size(max = GDPR_FIELD_MAX_LENGTH, message = GDPR_SIRET_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
        private String siret;

    }


    @Getter
    @Setter
    @NoArgsConstructor
    public static class GdprDeclaringEntity extends GdprEntity {

        @Getter
        @Setter
        @NoArgsConstructor
        public static class GdprDeclaringEntityDpo {

            @Size(max = GDPR_FIELD_MAX_LENGTH, message = GDPR_DPO_NAME_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
            private String name;
            @Size(max = GDPR_FIELD_MAX_LENGTH, message = GDPR_DPO_MAIL_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
            private String mail;

        }


        @Getter
        @Setter
        @NoArgsConstructor
        public static class GdprDeclaringEntityResponsible {

            @Size(max = GDPR_FIELD_MAX_LENGTH, message = GDPR_RESPONSIBLE_NAME_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
            private String name;
            @Size(max = GDPR_FIELD_MAX_LENGTH, message = GDPR_RESPONSIBLE_TITLE_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
            private String title;

        }


        @Size(max = GDPR_FIELD_MAX_LENGTH, message = GDPR_ENTITY_APECODE_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
        private String apeCode;
        @Size(max = GDPR_FIELD_MAX_LENGTH, message = GDPR_ENTITY_PHONE_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
        private String phoneNumber;
        @Size(max = GDPR_FIELD_MAX_LENGTH, message = GDPR_ENTITY_MAIL_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
        private String mail;
        private GdprDeclaringEntityDpo dpo = new GdprDeclaringEntityDpo();
        private GdprDeclaringEntityResponsible responsible = new GdprDeclaringEntityResponsible();

    }


    @Getter
    @Setter
    @NoArgsConstructor
    public static class GdprApplication {


        @Getter
        @Setter
        @NoArgsConstructor
        @AllArgsConstructor
        public static class GdprDataElement {

            private String name;
            private List<String> elements;

        }


        @Getter
        @Setter
        @NoArgsConstructor
        @AllArgsConstructor
        public static class GdprCookie extends GdprDataElement {

            private String description;


            public GdprCookie(String name, List<String> elements, String description) {
                super(name, elements);
                this.description = description;
            }

        }


        @Getter
        @Setter
        @NoArgsConstructor
        @AllArgsConstructor
        public static class GdprDataSet {

            private String name;
            private List<String> mandatoryElements;
            private List<String> optionalElements;

        }


        private String name = "iparapheur";
        private String cookieSessionDuration = "180 minutes";
        private List<String> mandatoryCookies = List.of("Cookie de localisation");
        private List<String> preservedDataAfterDeletion = List.of(
                "Historique des transactions",
                "Commentaires");
        private List<GdprCookie> optionalCookies = List.of(
                new GdprCookie("Cookies de personnalisation de l'application",
                        List.of("un cookie de personnalisation"),
                        "Permet de conserver vos préférences."));
        private boolean noCookies = false;
        private GdprEntity editor = new GdprEntity(
                "Libriciel Scop",
                "140 Rue Aglaonice de Thessalie, 34170 Castelnau-le-Lez",
                "491 011 698 00025");
        private boolean noDataProcessed = false;
        private boolean noDataCollected = false;
        private List<GdprDataElement> dataProcesses = List.of(
                new GdprDataElement("Affichage de vos données personnelles",
                        List.of("Liste des utilisateurs : votre identifiant de connexion, apparait dans la liste.")
                ));
        private List<GdprDataSet> collectedDataSet = List.of(
                new GdprDataSet(
                        "Création d'un utilisateur",
                        List.of("Civilité de l'utilisateur", "Nom de l'utilisateur"),
                        List.of("Numéro de téléphone")
                ));
    }


    private GdprDeclaringEntity declaringEntity = new GdprDeclaringEntity();
    @Size(max = GDPR_FIELD_MAX_LENGTH, message = GDPR_HOSTING_COMMENT_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
    private String hostingEntityComments;
    private GdprEntity hostingEntity = new GdprEntity();
    private GdprEntity maintenanceEntity = new GdprEntity();
    private GdprApplication application = new GdprApplication();


}
