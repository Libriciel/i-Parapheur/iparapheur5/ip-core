/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
package coop.libriciel.ipcore.services.secret;

import coop.libriciel.ipcore.model.crypto.SealCertificate;
import coop.libriciel.ipcore.model.crypto.SealCertificateRepresentation;
import coop.libriciel.ipcore.model.database.SealCertificateSortBy;
import coop.libriciel.ipcore.model.signatureValidation.ValidationServiceConfiguration;
import coop.libriciel.ipcore.services.signatureValidation.SignatureValidationProperties;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.RequestUtils;
import coop.libriciel.ipcore.utils.TextUtils;
import io.github.jopenlibs.vault.Vault;
import io.github.jopenlibs.vault.VaultConfig;
import io.github.jopenlibs.vault.VaultException;
import io.github.jopenlibs.vault.api.sys.Sys;
import io.github.jopenlibs.vault.response.LogicalResponse;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.text.StringSubstitutor;
import org.apache.http.client.utils.URIBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.net.URISyntaxException;
import java.util.*;
import java.util.function.Function;

import static coop.libriciel.ipcore.model.crypto.SealCertificate.*;
import static coop.libriciel.ipcore.utils.PaginatedList.MAX_PAGE_SIZE;
import static coop.libriciel.ipcore.utils.TextUtils.ISO8601_DATE_TIME_FORMAT;
import static java.util.Comparator.*;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;


@Log4j2
@Service(SecretServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = SecretServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = "vault")
public class VaultService implements SecretServiceInterface {

    private static final String TENANT_ID_PLACEHOLDER = "tenantId";
    private static final String SEAL_CERTIFICATE_ID_PLACEHOLDER = "sealCertificateId";

    private static final String TENANT_PATH = String.format("secret/tenant/${%s}", TENANT_ID_PLACEHOLDER);
    private static final String SEAL_CERTIFICATE_LIST_PATH = String.format("%s/sealCertificate", TENANT_PATH);
    private static final String SEAL_CERTIFICATE_PATH = String.format("%s/${%s}", SEAL_CERTIFICATE_LIST_PATH, SEAL_CERTIFICATE_ID_PLACEHOLDER);
    public static final String CLIENT_ID = "client_id";
    public static final String CLIENT_SECRET = "client_secret";
    public static final String VALIDATION_SERVICE_CONFIGURATION_PATH = "secret/validation-service/";

    private final SignatureValidationProperties signatureValidationProperties;
    private Vault vault;
    private VaultConfig vaultConfig;


    // <editor-fold desc="LifeCycle">


    private final SecretServiceProperties properties;


    @Autowired
    public VaultService(SecretServiceProperties properties,
                        SignatureValidationProperties signatureValidationProperties) {
        this.properties = properties;
        this.signatureValidationProperties = signatureValidationProperties;
    }


    @PostConstruct
    public void init() {
        final VaultConfig config;
        try {

            // TODO : See if we can generate a token with the key :
            // - That'll save a few steps on install.
            // - That'll save a parameter
            // - Having a 35min token, renewed every 30 min with a Delayed method would be cleaner.

            vaultConfig = new VaultConfig()
                    .address(new URIBuilder().setScheme(properties.getScheme()).setHost(properties.getHost()).setPort(properties.getPort()).build().toString())
                    .token(properties.getToken())
                    .engineVersion(2)
                    .build();

            vault = Vault.create(vaultConfig);

            new Sys(vaultConfig).seal().unseal(properties.getUnsealKey());

        } catch (VaultException | URISyntaxException e) {
            throw new RuntimeException("Cannot init Vault connection", e);
        }
    }


    @PreDestroy
    public void close() {
        try {
            new Sys(vaultConfig).seal().seal();
        } catch (VaultException e) {
            log.warn("Vault was not properly sealed on exit", e);
        }
    }


    // </editor-fold desc="LifeCycle">


    // <editor-fold desc="Seal certificate CRUDL">


    @Override
    public void storeSealCertificate(@NotNull String tenantId, @NotNull SealCertificate sealCertificate) {

        String sealCertPath = StringSubstitutor
                .replace(SEAL_CERTIFICATE_PATH, Map.of(TENANT_ID_PLACEHOLDER, tenantId, SEAL_CERTIFICATE_ID_PLACEHOLDER, sealCertificate.getId()));

        log.debug("store path:{}", sealCertPath);

        Map<String, Object> secrets = new HashMap<>();
        secrets.put(FIELD_NAME, sealCertificate.getName());
        secrets.put(FIELD_SIGNATURE_IMAGE_CONTENT_ID, sealCertificate.getSignatureImageContentId());
        secrets.put(FIELD_PUBLIC_CERTIFICATE, sealCertificate.getPublicCertificateBase64());
        secrets.put(FIELD_PRIVATE_KEY, sealCertificate.getPrivateKeyBase64());
        secrets.put(FIELD_EXPIRATION_DATE, TextUtils.serializeDate(sealCertificate.getExpirationDate(), ISO8601_DATE_TIME_FORMAT));

        try {
            vault.logical().write(sealCertPath, secrets);
        } catch (VaultException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_vault_service");
        }
    }


    @Override
    public @Nullable SealCertificate getSealCertificate(@NotNull String tenantId, @NotNull String sealCertificateId) {

        String sealCertificatePath = StringSubstitutor
                .replace(SEAL_CERTIFICATE_PATH, Map.of(TENANT_ID_PLACEHOLDER, tenantId, SEAL_CERTIFICATE_ID_PLACEHOLDER, sealCertificateId));

        log.debug("getSealCertificate path:{}", sealCertificatePath);

        LogicalResponse response;
        try {
            response = vault.logical().read(sealCertificatePath);
        } catch (VaultException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_vault_service");
        }

        return new SealCertificate(sealCertificateId, response.getData());
    }


    @Override
    public void deleteSealCertificate(@NotNull String tenantId, @NotNull String stampCertificateId) {

        String stampCertPath = StringSubstitutor
                .replace(SEAL_CERTIFICATE_PATH, Map.of(TENANT_ID_PLACEHOLDER, tenantId, SEAL_CERTIFICATE_ID_PLACEHOLDER, stampCertificateId));

        try {
            vault.logical().delete(stampCertPath);
        } catch (VaultException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_vault_service");
        }
    }


    /**
     * Vault doesn't support paginated requests,
     * but we still want it, to lighten HTTP requests.
     * <p>
     * So we pull everything internally, then we paginate.
     * This is kinda acceptable, there is a very low {@link SecretServiceInterface#MAX_SEAL_CERTIFICATES_PER_TENANT} allowed,
     * and this is an advanced menu. Not used that much.
     * <p>
     * Maybe another Vault-like service will comes out, with a pagination. Maybe Vault will finally implement it some day.
     * That's why we do it here, and not in the parent controller.
     *
     * @param tenantId to compute the Vault inner path
     * @param pageable Standard SpringBoot pagination
     * @return A properly paginated list, with redacted private informations.
     */
    @Override
    public @NotNull Page<SealCertificateRepresentation> getSealCertificateList(@NotNull String tenantId, @NotNull Pageable pageable) {

        String sealCertListFolder = StringSubstitutor.replace(SEAL_CERTIFICATE_LIST_PATH, Map.of(TENANT_ID_PLACEHOLDER, tenantId));
        log.debug("getSealCertificateList path:{}", sealCertListFolder);

        List<String> vaultNodeList;
        try {
            vaultNodeList = vault.logical().list(sealCertListFolder).getListData();
        } catch (VaultException e) {
            log.error("Error getting seal certificate list : {}", e.getMessage());
            log.debug("Error details : ", e);
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_vault_service");
        }

        String sortBy = pageable.getSort().stream().findFirst()
                .map(Sort.Order::getProperty)
                .orElse(SealCertificateSortBy.NAME.getFieldName());

        boolean asc = pageable.getSort().stream().findFirst()
                .map(Sort.Order::getDirection)
                .map(Sort.Direction::isAscending)
                .orElse(true);

        Comparator<SealCertificate> comparator = switch (sortBy) {
            case FIELD_ID -> comparing(SealCertificate::getId, asc ? nullsLast(naturalOrder()) : nullsFirst(reverseOrder()));
            case FIELD_EXPIRATION_DATE -> comparing(SealCertificate::getExpirationDate, asc ? nullsLast(naturalOrder()) : nullsFirst(reverseOrder()));
            default -> comparing(
                    (Function<SealCertificate, String>) sealCertificate -> Optional.ofNullable(sealCertificate.getName())
                            .map(name -> name.toLowerCase(Locale.ROOT))
                            .orElse(null),
                    asc ? nullsLast(naturalOrder()) : nullsFirst(reverseOrder())
            );
        };

        long skip = (pageable != Pageable.unpaged()) ? pageable.getOffset() : 0L;
        long limit = RequestUtils.getPageSize(pageable);

        List<SealCertificate> sealCertList = vaultNodeList.stream()
                .map(n -> getSealCertificate(tenantId, n))                          // Retrieving the real certificate for each node
                .filter(Objects::nonNull)
                .sorted(comparator)                                                 // Manual sort
                .skip(skip)                                                         // Manual pagination
                .limit(limit)
                .toList();

        return new PageImpl<>(sealCertList, pageable, vaultNodeList.size())
                .map(SealCertificateRepresentation::new);
    }


    // </editor-fold desc="Seal certificate CRUDL">


    @Override
    public void deleteTenant(@NotNull String tenantId) {

        String tenantPath = StringSubstitutor.replace(TENANT_PATH, Map.of(TENANT_ID_PLACEHOLDER, tenantId));

        try {
            vault.logical().delete(tenantPath);
        } catch (VaultException e) {
            if (e.getHttpStatusCode() == NOT_FOUND.value()) {
                log.debug("No tenant secret found");
            } else {
                log.error("Error deleting tenant's secret: {}", e.getMessage());
                log.debug("Error stacktrace: ", e);
                throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_vault_service");
            }
        }
    }


    @Override
    public void storeValidationServiceConfiguration(@NotNull String clientId, @NotNull String clientSecret) {
        Map<String, Object> secrets = new HashMap<>();
        secrets.put(CLIENT_ID, clientId);
        secrets.put(CLIENT_SECRET, clientSecret);

        try {
            LogicalResponse response = vault.logical().write(VALIDATION_SERVICE_CONFIGURATION_PATH + signatureValidationProperties.getProvider(), secrets);
            log.debug(response.getRestResponse().toString());
            log.debug(response);
        } catch (VaultException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_vault_service");
        }
    }


    @Override
    public @Nullable ValidationServiceConfiguration getValidationServiceConfiguration() {
        try {
            Map<String, String> response = vault.logical()
                    .read(VALIDATION_SERVICE_CONFIGURATION_PATH + signatureValidationProperties.getProvider())
                    .getData();
            if (response.isEmpty()) {
                return null;
            }
            return new ValidationServiceConfiguration(response.get(CLIENT_ID), response.get(CLIENT_SECRET));
        } catch (VaultException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_vault_service");
        }
    }


    @Override
    public void deleteValidationServiceConfiguration() {
        try {
            vault.logical().delete(VALIDATION_SERVICE_CONFIGURATION_PATH + signatureValidationProperties.getProvider());
        } catch (VaultException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_vault_service");
        }
    }

}
