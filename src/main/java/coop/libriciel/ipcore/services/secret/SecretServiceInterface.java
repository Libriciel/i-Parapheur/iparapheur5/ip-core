/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.secret;

import coop.libriciel.ipcore.model.signatureValidation.ValidationServiceConfiguration;
import coop.libriciel.ipcore.model.crypto.SealCertificate;
import coop.libriciel.ipcore.model.crypto.SealCertificateRepresentation;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface SecretServiceInterface {

    int MAX_SEAL_CERTIFICATES_PER_TENANT = 50;

    String BEAN_NAME = "secretService";
    String PREFERENCES_PROVIDER_KEY = "services.secret.provider";


    // <editor-fold desc="Seal certificate CRUDL">


    void storeSealCertificate(@NotNull String tenantId, @NotNull SealCertificate sealCertificate);


    @Nullable SealCertificate getSealCertificate(@NotNull String tenantId, @NotNull String sealCertificateId);


    void deleteSealCertificate(@NotNull String tenantId, @NotNull String sealCertificateId);


    @NotNull Page<SealCertificateRepresentation> getSealCertificateList(@NotNull String tenantId, @NotNull Pageable pageable);


    // </editor-fold desc="Seal certificate CRUDL">


    void deleteTenant(@NotNull String tenantId);


    void storeValidationServiceConfiguration(@NotNull String clientId, @NotNull String clientSecret);


    @Nullable ValidationServiceConfiguration getValidationServiceConfiguration();


    void deleteValidationServiceConfiguration();
}
