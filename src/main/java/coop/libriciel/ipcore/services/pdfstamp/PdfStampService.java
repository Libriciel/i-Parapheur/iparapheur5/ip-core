/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.pdfstamp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.pdfstamp.Stamp;
import coop.libriciel.ipcore.utils.ForcedExceptionsConnector;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.RequestUtils;
import coop.libriciel.pdf.stamp.api.model.*;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;

import static coop.libriciel.ipcore.model.pdfstamp.StampType.IMAGE;
import static coop.libriciel.ipcore.model.pdfstamp.StampType.METADATA;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.MediaType.*;
import static org.springframework.security.config.Elements.HTTP;


@Log4j2
@Service(PdfStampServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = PdfStampServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = "pdfstamp")
public class PdfStampService implements PdfStampServiceInterface {


    private static final String API_V3_PATH = "pdf-stamp/v3/";
    private static final String STAMP_ADD_URL = API_V3_PATH + "stamp/add";
    private static final String TEXT_ADD_URL = API_V3_PATH + "comment/add";
    private static final String TEXT_DELETE_URL = API_V3_PATH + "comment/delete";


    // <editor-fold desc="Beans">


    private final ObjectMapper objectMapper;
    private final PdfStampServiceProperties properties;


    @Autowired
    public PdfStampService(ObjectMapper objectMapper,
                           PdfStampServiceProperties properties) {
        this.objectMapper = objectMapper;
        this.properties = properties;
    }


    // </editor-fold desc="Beans">


    private static coop.libriciel.pdf.stamp.api.model.Stamp convertToLibraryStamp(Stamp stamp) {
        coop.libriciel.pdf.stamp.api.model.Stamp result = new coop.libriciel.pdf.stamp.api.model.Stamp();

        Position position = new Position();
        position.setX(stamp.getX());
        position.setY(stamp.getY());
        position.setOrigin(PositionOrigin.BOTTOM_LEFT);

        Row row = new Row();
        if (stamp.getType() == IMAGE) {
            Logo logo = new Logo();
            logo.setImageRef(stamp.getId());
            row.setLogo(logo);
        } else {
            row.setValue(stamp.getType() == METADATA ? stamp.getComputedValue() : stamp.getValue());
            row.setColor(stamp.getTextColor().toString().toLowerCase(Locale.getDefault()));
        }

        result.setPage(stamp.getPage());
        result.setFontSize(stamp.getFontSize());
        result.setPosition(position);
        result.setRows(List.of(row));
        result.setOpacity(1f);

        return result;
    }


    /**
     * TODO : Use the webClient lib,
     *  For now, it kinda don't work well with streams.
     *
     * @param documentBuffer the source document
     * @param stampList
     * @return the patched document
     */
    @Override
    public @NotNull DocumentBuffer createStamp(@NotNull DocumentBuffer documentBuffer,
                                               @NotNull List<Stamp> stampList,
                                               @NotNull Function<String, DocumentBuffer> imageSupplier) {

        List<coop.libriciel.pdf.stamp.api.model.Stamp> pdfStampAnnotationList = stampList.stream()
                .map(PdfStampService::convertToLibraryStamp)
                .toList();

        StampRequest request = new StampRequest();
        request.setStampList(pdfStampAnnotationList);

        MultipartBodyBuilder builder = new MultipartBodyBuilder();
        builder.part("stampRequest", request, APPLICATION_JSON);

        builder.asyncPart("pdfSource", documentBuffer.getContentFlux(), DataBuffer.class)
                .header(CONTENT_TYPE, APPLICATION_PDF_VALUE)
                .header(CONTENT_DISPOSITION, "form-data; name=pdfSource; filename=" + documentBuffer.getName());

        stampList.stream()
                .filter(stamp -> stamp.getType() == IMAGE)
                .forEach(stamp -> builder
                        .asyncPart("images", imageSupplier.apply(stamp.getId()).getContentFlux(), DataBuffer.class)
                        .header(CONTENT_TYPE, APPLICATION_PDF_VALUE)
                        .header(CONTENT_DISPOSITION, "form-data; name=images; filename=" + stamp.getId())
                );

        // Upload content

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort()).path(STAMP_ADD_URL)
                .build().normalize().toUri();

        DocumentBuffer result = new DocumentBuffer();
        result.setContentFlux(WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
                        .post().uri(requestUri)
                        .contentType(MULTIPART_FORM_DATA).accept(APPLICATION_OCTET_STREAM)
                        .body(BodyInserters.fromMultipartData(builder.build()))
                        .exchangeToFlux(response -> RequestUtils.clientResponseToDataBufferFlux(response, result, documentBuffer.getName()))
                        // Result
                        .doOnError(e -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_pdf_stamp_service");})
//                .doOnSuccess(cr -> result.setMediaType(cr.headers().contentType().orElse(APPLICATION_OCTET_STREAM)))
//                .doOnSuccess(cr -> result.setContentLength(cr.headers().contentLength().orElse(-1L)))
                // TODO : Parse fileName in "content-disposition": "attachment; filename=\"dummy-34.pdf\"; filename*=UTF-8''dummy-34.pdf",
//                .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT)
//                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_parse_pdf_stamp_service_response"))
//                .bodyToFlux(DataBuffer.class)
        );

        return result;
    }


    /**
     * TODO : Use the webClient lib,
     *  For now, it kinda don't work well with streams.
     *
     * @param documentBuffer the source document
     * @param comment
     * @return the patched document
     */
    @Override
    public @NotNull DocumentBuffer addComment(@NotNull DocumentBuffer documentBuffer, @NotNull Comment comment) {

        String serializedComment;
        try {
            serializedComment = objectMapper.writeValueAsString(comment);
        } catch (JsonProcessingException e) {
            log.error("addComment - An error occurred while serializing comment data: {}", e.getMessage());
            log.debug("error details: ", e);
            throw new RuntimeException(e);
        }

        MultipartBodyBuilder builder = new MultipartBodyBuilder();
        builder.part("comment", serializedComment, APPLICATION_JSON);
        builder.asyncPart("pdfSource", documentBuffer.getContentFlux(), DataBuffer.class)
                .header(CONTENT_DISPOSITION, "form-data; name=pdfSource; filename=" + documentBuffer.getName());

        // Upload content

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort()).path(TEXT_ADD_URL)
                .build().normalize().toUri();

        DocumentBuffer result = new DocumentBuffer();
        result.setContentFlux(WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
                .post().uri(requestUri)
                .contentType(MULTIPART_FORM_DATA).accept(APPLICATION_PDF)
                .body(BodyInserters.fromMultipartData(builder.build()))
                .exchangeToFlux(response -> RequestUtils.clientResponseToDataBufferFlux(response, result, documentBuffer.getName()))
                // Result
                .doOnError(e -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_pdf_stamp_service");})
        );

        return result;
    }


    /**
     * TODO : Use the webClient lib,
     *  For now, it kinda don't work well with streams.
     *
     * @param documentBuffer the source document
     * @param commentId
     * @return the patched document
     */
    @Override
    public @NotNull DocumentBuffer deleteComment(@NotNull DocumentBuffer documentBuffer, @NotNull String commentId) {

        MultipartBodyBuilder builder = new MultipartBodyBuilder();
        // For some reason, header seems mandatory for the asyncPart, and not for the other ones.
        // Cheers to this article : https://dev.to/shavz/sending-multipart-form-data-using-spring-webtestclient-2gb7
        builder.asyncPart("pdfSource", documentBuffer.getContentFlux(), DataBuffer.class)
                .header(CONTENT_DISPOSITION, "form-data; name=pdfSource; filename=" + documentBuffer.getName());

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(TEXT_DELETE_URL).pathSegment(commentId)
                .build().normalize().toUri();

        DocumentBuffer result = new DocumentBuffer();

        result.setContentFlux(WebClient.builder().clientConnector(new ForcedExceptionsConnector()).build()
                .post().uri(requestUri)
                .contentType(MULTIPART_FORM_DATA).accept(APPLICATION_PDF)
                .body(BodyInserters.fromMultipartData(builder.build()))
                .exchangeToFlux(rep -> RequestUtils.clientResponseToDataBufferFlux(rep, result, documentBuffer.getName()))
                // Result
                .doOnError(e -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_pdf_stamp_service");})
        );

        return result;
    }


}
