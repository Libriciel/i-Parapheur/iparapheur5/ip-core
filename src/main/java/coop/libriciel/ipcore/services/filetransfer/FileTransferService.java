/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.filetransfer;

import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.filetransfer.FolderUnpackedContent;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.IOUtils;
import org.bouncycastle.util.encoders.Hex;
import org.jetbrains.annotations.NotNull;
import org.jooq.tools.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.DigestOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import static coop.libriciel.ipcore.utils.RequestUtils.SUBSERVICE_REQUEST_LONG_TIMEOUT;
import static java.nio.file.StandardOpenOption.CREATE;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.MediaType.ALL;
import static org.springframework.security.config.Elements.HTTP;


@Log4j2
@Service(FileTransferServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = FileTransferServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = "transfer")
public class FileTransferService implements FileTransferServiceInterface {

    private static final String FOLDER_INFO_NAME_PLACEHOLDER = "%%FOLDER_NAME%%";
    private static final String FOLDER_INFO_CONTENT_TEMPLATE = "<folderName>%s</folderName>".formatted(FOLDER_INFO_NAME_PLACEHOLDER);

    private static final String TEMP_WORKDIR_PREFIX = "ipv5_ipng_filetransfer";
    private static final String TEMP_ZIP_FILENAME = "downloadedFolder.zip";
    private static final int FILE_TRANSFER_BUFFER_SIZE = 16384;

    private static final String FILE_TRANSFER_EXPOSED_PATH = "/filetransfer";
    private static final String FILE_TRANSFER_SERVICE_BASE_RETRIEVAL_PATH = "/get/1";

    private final String FOLDER_ZIPPED_STRUCTURE_MAIN_DIR = "mainDocs";
    private final String FOLDER_ZIPPED_STRUCTURE_ANNEXES_DIR = "annexes";


    private record PathWithHash(Path filePath, byte[] fileHash) {}


    // <editor-fold desc="Beans">


    private final ContentServiceInterface contentService;
    private final FileTransferProperties properties;


    @Autowired
    public FileTransferService(ContentServiceInterface contentService, FileTransferProperties properties) {
        this.contentService = contentService;
        this.properties = properties;
    }


    // </editor-fold desc="Beans">


    @Override
    public @NotNull String buildRetrievalUrl(String baseUrl, String fileName) {
        return UriComponentsBuilder.fromUriString(baseUrl)
                .path(FILE_TRANSFER_EXPOSED_PATH)
                .path(FILE_TRANSFER_SERVICE_BASE_RETRIEVAL_PATH)
                .pathSegment(fileName)
                .toUriString();
    }


    @Override
    public String postFolder(@NotNull Folder folder, String targetName) throws NoSuchAlgorithmException, IOException {

        log.info("::postFolder - targetName");

        PathWithHash zipAndhash = this.packFolder(folder);
        log.info("::postFolder - zipAndHash : {}", zipAndhash);
        InputStream fInputStream = Files.newInputStream(zipAndhash.filePath);

        byte[] hash = zipAndhash.fileHash;
        String sha256hex = new String(Hex.encode(hash));

        log.info("::postFolder - generatedHash : {}", sha256hex);

        targetName = targetName + "---" + sha256hex;
        targetName = URLEncoder.encode(targetName, StandardCharsets.UTF_8);

        log.info("::postFolder - final targetName : {}", targetName);

        this.sendFileFromStream(fInputStream, targetName);
        return targetName;

    }


    @Override
    public FolderUnpackedContent getFolderFilesFromUrl(@NotNull String url) throws IOException {
        Path zippedFodlerPath = retrieveStreamedZipFromUrl(url);
        return unpackFolderFiles(zippedFodlerPath);
    }


    FolderUnpackedContent unpackFolderFiles(Path zippedFolderPath) throws IOException {
        log.info("unpackFolderFiles");

        Path baseWorkDir = zippedFolderPath.getParent();
        Path extractionDir = Files.createDirectory(baseWorkDir.resolve("extracted"));
        FolderUnpackedContent result = new FolderUnpackedContent();

        try (InputStream fis = Files.newInputStream(zippedFolderPath)) { //, StandardOpenOption.DELETE_ON_CLOSE

            ZipInputStream zis = new ZipInputStream(fis);
            ZipEntry zipEntry = zis.getNextEntry();

            for (; zipEntry != null; zipEntry = zis.getNextEntry()) {
                if (zipEntry.isDirectory()) {
                    log.debug("unpackFolderFiles - unzipping a directory : {}", zipEntry.getName());
                    Path targetPath = extractionDir.resolve(zipEntry.getName());
                    Files.createDirectory(targetPath);

                } else {
                    log.debug("unpackFolderFiles - unzipping file : {}", zipEntry.getName());
                    Path relPath = Path.of(zipEntry.getName());
                    Path targetPath = extractionDir.resolve(relPath);
                    try (OutputStream targetOutputStream = Files.newOutputStream(targetPath, CREATE)) {
                        IOUtils.copyLarge(zis, targetOutputStream);
                    }

                    String rootDirName = relPath.getParent() != null ? relPath.getParent().toString() : EMPTY;
                    switch (rootDirName) {
                        case EMPTY -> {
                            if (StringUtils.equals(zipEntry.getName(), FOLDER_INFO_FILE_NAME)) {
                                result.setDescriptionFile(targetPath);
                            }
                        }
                        case FOLDER_ZIPPED_STRUCTURE_MAIN_DIR -> result.getMainDocs().add(targetPath);
                        case FOLDER_ZIPPED_STRUCTURE_ANNEXES_DIR -> result.getAnnexes().add(targetPath);
                        default -> log.warn("unpackFolderFiles - undefined state reached, entry : {}", zipEntry.getName());
                    }
                }
            }
            zis.closeEntry();
            zis.close();
        }
        log.info("unzipping - result : {}", result);

        return result;
    }


    Path retrieveStreamedZipFromUrl(@NotNull String url) throws IOException {
        URI requestUri = UriComponentsBuilder.fromUriString(url)
                .build().normalize().toUri();

        Flux<DataBuffer> fileDataFlux = WebClient.builder()
                .filters(exchangeFilterFunctions -> {
                    exchangeFilterFunctions.add(logResponse());
                })
                .build()
                .get()
                .uri(requestUri)
                .accept(ALL)
                .retrieve()
                .bodyToFlux(DataBuffer.class);

        Path tempDirWithPrefix = this.createTempDir(TEMP_WORKDIR_PREFIX + "_incoming");
        Path targetFile = Files.createFile(tempDirWithPrefix.resolve(TEMP_ZIP_FILENAME));

        try (OutputStream outputStream = Files.newOutputStream(targetFile)) {
//            DataBufferUtils.write(fileDataFlux, outputStream)
////                    .share()
//                    .subscribe(DataBufferUtils.releaseConsumer());

            DataBufferUtils.write(fileDataFlux, outputStream)
                    .map(DataBufferUtils::release)
                    .then().block(SUBSERVICE_REQUEST_LONG_TIMEOUT);
        }

        return targetFile;
    }


    ExchangeFilterFunction logResponse() {
        return ExchangeFilterFunction.ofResponseProcessor(clientResponse -> {

            StringBuilder sb = new StringBuilder("Request: \n");
            //append clientRequest method and url
            clientResponse
                    .headers().asHttpHeaders()
                    .forEach((name, values) -> {
                        sb.append("header " + name + " : ");
                        values.forEach(value -> sb.append(value + ","));
                    });
            log.info(sb.toString());

            return Mono.just(clientResponse);
        });
    }


    void sendFileFromStream(InputStream fis, String targetName) {

        Flux<DataBuffer> dataFlux = DataBufferUtils.readInputStream(() -> fis, new DefaultDataBufferFactory(), FILE_TRANSFER_BUFFER_SIZE);

        URI requestUri = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(properties.getHost()).port(properties.getPort())
                .path(targetName)
                .build().normalize().toUri();

        String result = WebClient.builder()
                .filters(exchangeFilterFunctions -> exchangeFilterFunctions.add(logResponse()))
                .build()
                .put()
                .uri(requestUri)
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .accept(ALL)
                .body(BodyInserters.fromProducer(dataFlux, DataBuffer.class))
                .retrieve()
                .bodyToMono(String.class)
                .blockOptional(SUBSERVICE_REQUEST_LONG_TIMEOUT)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_filetransfer"));
        log.info("::sendFile - result = {}", result);
    }


    private PathWithHash packFolder(Folder folder) throws IOException, NoSuchAlgorithmException {

        List<Document> docList = this.contentService.getDocumentList(folder.getContentId());
        if (docList.size() == 0) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, String.format("No document attached to folder %s", folder.getName()));
        }


        Path workDir = this.createTempDir(TEMP_WORKDIR_PREFIX);
        Path zipFile = Files.createTempFile(workDir, "", ".zip");

        MessageDigest digester = MessageDigest.getInstance("SHA-256");
        try (OutputStream fileOutputStream = Files.newOutputStream(zipFile)) {
            DigestOutputStream dos = new DigestOutputStream(fileOutputStream, digester);
            ZipOutputStream zipOut = new ZipOutputStream(fileOutputStream);

            Path mainDocsBasePath = Path.of(FOLDER_ZIPPED_STRUCTURE_MAIN_DIR);
            Path annexesBasePath = Path.of(FOLDER_ZIPPED_STRUCTURE_ANNEXES_DIR);
            ZipEntry zipEntry = new ZipEntry(FOLDER_ZIPPED_STRUCTURE_MAIN_DIR + "/");
            zipOut.putNextEntry(zipEntry);
            zipOut.closeEntry();

            zipEntry = new ZipEntry(FOLDER_ZIPPED_STRUCTURE_ANNEXES_DIR + "/");
            zipOut.putNextEntry(zipEntry);
            zipOut.closeEntry();


            for (Document doc : docList) {
                InputStream istream = this.contentService.retrievePipedDocument(doc.getId());
                Path entryRelativePath = doc.isMainDocument() ? mainDocsBasePath.resolve(doc.getName()) : annexesBasePath.resolve(doc.getName());
                ZipEntry entry = new ZipEntry(entryRelativePath.toString());
                zipOut.putNextEntry(entry);

                IOUtils.copyLarge(istream, zipOut);

                istream.close();
            }


            String folderInfoContent = FOLDER_INFO_CONTENT_TEMPLATE.replace(FOLDER_INFO_NAME_PLACEHOLDER, folder.getName());
            InputStream istream = new ByteArrayInputStream(folderInfoContent.getBytes(StandardCharsets.UTF_8));
            zipEntry = new ZipEntry(FOLDER_INFO_FILE_NAME);
            zipOut.putNextEntry(zipEntry);

            IOUtils.copyLarge(istream, zipOut);

            istream.close();
            zipOut.close();
            dos.close();
        }

        return new PathWithHash(zipFile, digester.digest());
    }


    private Path createTempDir(String prefix) throws IOException {
        Path dir = Files.createTempDirectory(prefix);
//        dir.toFile().deleteOnExit();
        return dir;
    }

}
