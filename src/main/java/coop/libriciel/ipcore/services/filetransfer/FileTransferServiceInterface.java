/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.filetransfer;

import coop.libriciel.ipcore.model.filetransfer.FolderUnpackedContent;
import coop.libriciel.ipcore.model.workflow.Folder;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import static coop.libriciel.ipcore.utils.TextUtils.INTERNAL_PREFIX;


public interface FileTransferServiceInterface {

    String BEAN_NAME = "filetransfer";
    String PREFERENCES_PROVIDER_KEY = "services.file-transfer.provider";
    String FOLDER_INFO_FILE_NAME = INTERNAL_PREFIX + "folderInfoContent.xml";

    @NotNull String buildRetrievalUrl(String baseUrl, String fileName);

    /**
     * Export a folder to an archive format (premis?)
     * and share it with the filetransfer service.
     *  @param folder the folder to share
     * @param targetName
     * @return
     */
    String postFolder(@NotNull Folder folder, String targetName) throws NoSuchAlgorithmException, IOException;
    FolderUnpackedContent getFolderFilesFromUrl(@NotNull String url) throws IOException;
}
