/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.utils;

import coop.libriciel.ipcore.model.content.DocumentBuffer;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.modelmapper.ModelMapper;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.data.domain.*;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.*;
import java.time.Duration;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static coop.libriciel.ipcore.utils.FileUtils.parseFileNameFromHeaders;
import static coop.libriciel.ipcore.utils.PaginatedList.MAX_PAGE_SIZE;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Calendar.*;
import static org.springframework.core.io.buffer.DataBufferUtils.releaseConsumer;
import static org.springframework.data.domain.Sort.Direction.ASC;
import static org.springframework.data.domain.Sort.Direction.DESC;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM;
import static reactor.core.scheduler.Schedulers.boundedElastic;


@Log4j2
public class RequestUtils {

    public static final int DEFAULT_PIPE_SIZE = 8192;

    public static final String PREAUTHORIZE_IS_AUTHENTICATED = "isAuthenticated()";
    public static final String PREAUTHORIZE_TENANT_MEMBER_OR_MORE =
            "hasAnyRole('admin', 'tenant_' + #tenantId + '_admin', 'tenant_' + #tenantId + '_functional_admin', 'tenant_' + #tenantId)";
    public static final String PREAUTHORIZE_TENANT_ADMIN_OR_MORE =
            "hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')";
    public static final String PREAUTHORIZE_TENANT_FUNCTIONAL_ADMIN_OR_MORE =
            "hasAnyRole('admin', 'tenant_' + #tenantId + '_admin', 'tenant_' + #tenantId + '_functional_admin')";
    public static final String PREAUTHORIZE_SUPER_ADMIN = "hasRole('admin')";

    public static final Duration SUBSERVICE_REQUEST_SHORT_TIMEOUT = Duration.ofSeconds(5);
    public static final Duration SUBSERVICE_REQUEST_MEDIUM_TIMEOUT = Duration.ofSeconds(20);
    public static final Duration SUBSERVICE_REQUEST_LONG_TIMEOUT = Duration.ofSeconds(60);
    public static final Duration HTTP_CONNECTOR_RESPONSE_TIMEOUT = Duration.ofSeconds(60);

    public static final long WEBCLIENT_CLOSE_DELAY_SEC = 10;

    private static final HashMap<Integer, WebClient.Builder> webClientBuilderMap = new HashMap<>();


    private RequestUtils() {
        throw new IllegalStateException("Utility class");
    }


    /**
     * JPA database request asks for a column name.
     * Yet, we don't want those names to be listed anywhere publicly, in some kind of enum.
     * <p>
     * We want a standard enum, that we can control.
     * The inner value shall be translated.
     *
     * @param pageable        the pageable source
     * @param enumClass       the target enum definition
     * @param sortByConverter the method that transform the type into the inner column
     * @param <E>             a target SortBy enum
     * @return the same Pageable object, with inner (hidden) Sort values
     */
    public static <E extends Enum<E>> Pageable convertSortedPageable(@NotNull Pageable pageable,
                                                                     @NotNull Class<E> enumClass,
                                                                     @NotNull Function<E, String> sortByConverter) {

        if (pageable == Pageable.unpaged()) {
            return pageable;
        }

        Sort patchedSort = Sort.by(pageable.getSort().stream()
                .map(order -> new Sort.Order(
                        order.getDirection(),
                        // Here is the actual translation
                        Optional.ofNullable(EnumUtils.getEnum(enumClass, order.getProperty()))
                                .map(sortByConverter)
                                .filter(StringUtils::isNotEmpty)
                                .orElseThrow(() -> new LocalizedStatusException(BAD_REQUEST, "message.unknown_sort_by_enum_value_s", order.getProperty())),
                        order.getNullHandling()
                ))
                .toList()
        );

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), patchedSort);
    }


    public static boolean isFirstOrderAsc(@NotNull Pageable pageable) {

        if (pageable == Pageable.unpaged()) {
            return true;
        }

        return pageable.getSort().stream()
                .findFirst()
                .map(Sort.Order::isAscending)
                .orElse(true);
    }


    public static <E extends Enum<E>> E getFirstOderSort(@NotNull Pageable pageable,
                                                         @NotNull Class<E> enumClass,
                                                         E defaultValue) {

        if (pageable == Pageable.unpaged()) {
            return defaultValue;
        }

        return pageable.getSort().stream()
                .map(Sort.Order::getProperty)
                .map(prop -> EnumUtils.getEnum(enumClass, prop))
                .findFirst()
                .orElse(defaultValue);
    }


    public static long getPageNumber(@NotNull Pageable pageable) {
        return (pageable != Pageable.unpaged())
               ? pageable.getPageNumber()
               : 0L;
    }


    public static int getPageSize(@NotNull Pageable pageable) {
        return (pageable != Pageable.unpaged())
               ? pageable.getPageSize()
               : MAX_PAGE_SIZE;
    }


    public static void writeFluxToOutputStream(@NotNull Flux<DataBuffer> inFlux, @NotNull OutputStream targetBaos) throws IOException {
        DataBufferUtils.write(inFlux, targetBaos)
                .map(DataBufferUtils::release)
                .then()
                .block(SUBSERVICE_REQUEST_LONG_TIMEOUT);

        targetBaos.flush();
    }


    public static String writeBufferToString(@NotNull DocumentBuffer documentBuffer) {

        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            writeFluxToOutputStream(documentBuffer.getContentFlux(), baos);
            return baos.toString(UTF_8);
        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_reading_file");
        }
    }


    public static @NotNull InputStream bufferToInputStream(@NotNull DocumentBuffer documentBuffer) throws IOException {
        log.debug("bufferToInputStream");
        PipedOutputStream osPipe = new PipedOutputStream();
        // default size for IS is 1024, whereas experiment show that for data-buffer flux it is 8192
        PipedInputStream isPipe = new PipedInputStream(osPipe, DEFAULT_PIPE_SIZE);

        DataBufferUtils.write(documentBuffer.getContentFlux(), osPipe)
                .timeout(SUBSERVICE_REQUEST_LONG_TIMEOUT.minusSeconds(5))
                .subscribeOn(boundedElastic())
                .doOnTerminate(() -> {
                    try {
                        osPipe.close();
                    } catch (IOException e) {
                        log.error("bufferToInputStream - Error closing input stream", e);
                    }
                })
                .subscribe(releaseConsumer());
        return isPipe;
    }


    public static void writeBufferToResponse(@NotNull DocumentBuffer documentBuffer, @NotNull HttpServletResponse response) {

        log.debug("writeBufferToResponse");
        try (OutputStream responseOutput = response.getOutputStream()) {

            Flux<DataBuffer> targetFlux = documentBuffer.getContentFlux().switchOnFirst((signal, flux) -> {
                // if no value is present it means an error or empty result was returned
                if (signal.hasValue()) {
                    String mimeType = documentBuffer.getMediaType() != null ? documentBuffer.getMediaType().toString() : "application/octet-stream";
                    String filename = documentBuffer.getName() != null ? documentBuffer.getName() : "document.pdf";
                    response.setContentType(mimeType);
                    response.setContentLengthLong(documentBuffer.getContentLength());
                    response.setHeader(CONTENT_DISPOSITION, "attachment;filename=%s".formatted(filename));
                }
                return flux;
            });

            DataBufferUtils.write(targetFlux, responseOutput)
                    .map(DataBufferUtils::release)
                    .then()
                    .block(SUBSERVICE_REQUEST_LONG_TIMEOUT);

            log.debug(
                    "writeBufferToResponse fileName:{} mediaType:{} length:{}",
                    documentBuffer.getName(),
                    documentBuffer.getMediaType(),
                    documentBuffer.getContentLength()
            );

            response.flushBuffer();

        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_reading_file");
        }
    }


    public static @NotNull InputStream pipeFluxAsInputStream(@NotNull Flux<DataBuffer> inFlux) throws IOException {
        log.debug("pipeFluxAsInputStream");
        PipedOutputStream osPipe = new PipedOutputStream();
        PipedInputStream isPipe = new PipedInputStream(osPipe, DEFAULT_PIPE_SIZE);

        DataBufferUtils.write(inFlux, osPipe)
                .timeout(SUBSERVICE_REQUEST_LONG_TIMEOUT.minusSeconds(5))
                .subscribeOn(boundedElastic())
                .doOnTerminate(() -> {
                    try {
                        osPipe.close();
                    } catch (IOException e) {
                        log.error("pipeAsInputStream - Error closing input stream", e);
                    }
                })
                .subscribe(releaseConsumer());

        return isPipe;
    }


    @SafeVarargs
    public static <T, U extends Comparable<U>> boolean hasChangesBetween(T o1, T o2, Function<T, U>... getters) {
        return Stream.of(getters).anyMatch(g -> g.apply(o1) != g.apply(o2));
    }


    private static WebClient.Builder webClientBuilderForSize(Integer bufferSizeKey) {
        WebClient.Builder builder = webClientBuilderMap.get(bufferSizeKey);
        if (builder == null) {
            ExchangeStrategies exchangeStrategies = ExchangeStrategies.builder()
                    .codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(1024 * 1024 * bufferSizeKey)).build();

            builder = WebClient.builder().exchangeStrategies(exchangeStrategies);
            webClientBuilderMap.put(bufferSizeKey, builder);
        }

        return builder;
    }


    @NotNull
    public static WebClient getWebClientWithBufferSize(int bufferSizeInMo, boolean withForcedExceptionConnector) {
        WebClient.Builder builder = webClientBuilderForSize(bufferSizeInMo);

        if (withForcedExceptionConnector) {
            builder.clientConnector(new ForcedExceptionsConnector());
        }

        return builder.build();
    }


    @NotNull
    public static Flux<DataBuffer> clientResponseToDataBufferFlux(ClientResponse response, DocumentBuffer result, String defaultDocName) {
        if (response.statusCode() == OK) {
            log.debug("clientResponseToDataBufferFlux - converting to flux");
            result.setMediaType(response.headers().contentType().orElse(APPLICATION_OCTET_STREAM));
            result.setName(parseFileNameFromHeaders(response.headers(), defaultDocName));
            result.setContentLength(response.headers().contentLength().orElse(-1L));
            log.debug("clientResponseToDataBufferFlux - converting to flux - result.getContentLength() : {}", result.getContentLength());
            return response.bodyToFlux(DataBuffer.class);
        } else {
            return response.createException().flatMapMany(Mono::error);
        }
    }


    @NotNull
    public static Flux<DataBuffer> clientResponseToRawDataBufferFlux(ClientResponse response) {
        if (response.statusCode() == OK) {
            log.debug("clientResponseToRawDataBufferFlux - converting to flux");
            return response.bodyToFlux(DataBuffer.class);
        } else {
            return response.createException().flatMapMany(Mono::error);
        }
    }


    /**
     * The standard {@link ModelMapper#map(Object, Class<T>)} does not really create an appropriate object when it is returned in an HTTP response.
     * Even if the target is from the appropriate class, the JSON response contains every field from the source one.
     * <p>
     * Creating the target object, and map the source object into it, properly removes the unwanted fields on the JSON serialization.
     * Yet, it is not a one-liner, so... This.
     * <p>
     * For some reason, this issue is only present on a single-object response.
     * Mapped lists are properly mapped.
     *
     * @param modelMapper
     * @param source
     * @param targetConstructor
     * @return A non-null object of type T
     * @return
     */
    public static <T> T returnRedactedResponse(@NotNull ModelMapper modelMapper, @NotNull Object source, @NotNull Supplier<T> targetConstructor) {
        T redactedResult = targetConstructor.get();
        modelMapper.map(source, redactedResult);
        return redactedResult;
    }


    /**
     * "Before or equal" any date should catch everything in the given day.
     * Since we usually send a timeless (=midnight) date, we have to add 23h 59m 59s to it, to get a proper threshold.
     *
     * @param date
     * @return the same date, at the end of the day
     */
    public static @NotNull Date toEndOfDay(@NotNull Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(HOUR_OF_DAY, 23);
        calendar.add(MINUTE, 59);
        calendar.add(SECOND, 59);
        return calendar.getTime();
    }


    public static <T> @NotNull PaginatedList<T> toPaginatedList(@NotNull Page<T> page) {
        return new PaginatedList<>(page.getContent(), page.getNumber(), page.getSize(), page.getTotalElements());
    }


    public static <T> @NotNull Page<T> toPage(@NotNull PaginatedList<T> page, @NotNull Object sortBy, boolean ascending) {
        Sort sort = Sort.by(ascending ? ASC : DESC, String.valueOf(sortBy));
        Pageable pageable = PageRequest.of((int) page.getPage(), (int) page.getPageSize(), sort);
        return new PageImpl<>(page.getData(), pageable, page.getTotal());
    }


}
