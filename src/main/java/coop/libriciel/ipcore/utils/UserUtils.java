/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.utils;

import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.auth.requests.UserRepresentation;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.modelmapper.ModelMapper;

import static coop.libriciel.ipcore.utils.TextUtils.INTERNAL_PREFIX;
import static org.apache.commons.lang3.StringUtils.EMPTY;


public class UserUtils {


    public static final String AUTOMATIC_TASK_USER_ID = INTERNAL_PREFIX + "automatic_task";
    public static final String AUTOMATIC_TASK_USER_NAME = "iparapheur";


    private UserUtils() {
        throw new IllegalStateException("Utility class");
    }


    public static @NotNull User getAutomaticUser() {

        User automaticUser = new User();
        automaticUser.setId(AUTOMATIC_TASK_USER_ID);
        automaticUser.setUserName(AUTOMATIC_TASK_USER_NAME);
        automaticUser.setFirstName(EMPTY);
        automaticUser.setLastName(EMPTY);

        return automaticUser;
    }


    /**
     * Pretty print. By default, the first name and the last name.
     * Else, we want the single first name, or the single last name, or any other field until the id...
     *
     * @param user
     * @return a non-null String, may be empty on default case
     */
    public static @NotNull String prettyPrint(@Nullable UserRepresentation user) {

        if (user == null) {
            return EMPTY;
        }

        return StringUtils.isNoneEmpty(user.getFirstName(), user.getLastName())
               ? "%s %s".formatted(user.getFirstName(), user.getLastName())
               : TextUtils.firstNotEmpty(user.getLastName(), user.getFirstName(), user.getUserName(), user.getId(), EMPTY);
    }


    /**
     * Pretty print. By default, the first name and the last name.
     * Else, we want the single first name, or the single last name, or any other field until the id...
     *
     * @param user
     * @return a non-null String, may be empty on default case
     */
    public static @NotNull String prettyPrint(@Nullable User user) {

        if (user == null) {
            return EMPTY;
        }

        return prettyPrint(new ModelMapper().map(user, UserRepresentation.class));
    }


}
