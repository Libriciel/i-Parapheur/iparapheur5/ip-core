/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.utils;

import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.requests.UserRepresentation;
import coop.libriciel.ipcore.model.content.DetachedSignature;
import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.workflow.Action;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import gov.loc.premis.v3.File;
import gov.loc.premis.v3.*;
import jakarta.xml.bind.*;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.xml.security.Init;
import org.apache.xml.security.c14n.Canonicalizer;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.server.ResponseStatusException;

import javax.xml.namespace.QName;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.util.*;
import java.util.function.Function;

import static coop.libriciel.ipcore.model.crypto.SignatureFormat.PKCS7;
import static coop.libriciel.ipcore.model.crypto.SignatureFormat.XADES_DETACHED;
import static coop.libriciel.ipcore.model.workflow.Action.*;
import static coop.libriciel.ipcore.utils.CollectionUtils.distinctByProperty;
import static coop.libriciel.ipcore.utils.TextUtils.*;
import static jakarta.xml.bind.Marshaller.JAXB_ENCODING;
import static jakarta.xml.bind.Marshaller.JAXB_SCHEMA_LOCATION;
import static java.nio.charset.StandardCharsets.UTF_8;
import static javax.xml.XMLConstants.*;
import static javax.xml.transform.OutputKeys.*;
import static org.apache.commons.codec.digest.MessageDigestAlgorithms.SHA_256;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.xml.security.c14n.Canonicalizer.ALGO_ID_C14N_WITH_COMMENTS;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.MediaType.APPLICATION_XML;


@Log4j2
public class XmlUtils {


    public static final String OUTPUTKEYS_INDENT_AMOUNT = "{http://xml.apache.org/xslt}indent-amount";

    public static final String PREMIS_NAMESPACE_URL = "http://www.loc.gov/premis/v3";
    public static final String FOLDER_ID_LABEL = "folderId";
    public static final String DOCUMENT_ID_LABEL = "documentId";

    public static final String SIGNIFICANT_PROPERTY_KEY = "significantPropertiesType";
    public static final String SIGNIFICANT_PROPERTY_VALUE = "significantPropertiesValue";
    public static final String SIGNATURE_INFORMATIONS_KEY = "signatureInformation";
    public static final String SIGNATURE_METHOD_KEY = "signatureMethod";
    public static final String SIGNATURE_VALUE_KEY = "signatureValue";

    public static final String SIGNIFICANT_PROPERTY_TYPE_KEY = RESERVED_PREFIX + "type";
    public static final String SIGNIFICANT_PROPERTY_SUBTYPE_KEY = RESERVED_PREFIX + "subtype";
    public static final String SIGNIFICANT_PROPERTY_DUE_DATE = RESERVED_PREFIX + "dueDate";
    public static final String SIGNIFICANT_PROPERTY_MAIN_DOCUMENT_KEY = RESERVED_PREFIX + "mainDocument";

    private static final QName QNAME_SIGNIFICANT_PROPERTY_KEY = new QName(PREMIS_NAMESPACE_URL, SIGNIFICANT_PROPERTY_KEY);
    private static final QName QNAME_SIGNIFICANT_PROPERTY_VALUE = new QName(PREMIS_NAMESPACE_URL, SIGNIFICANT_PROPERTY_VALUE);
    private static final QName QNAME_EVENT_OUTCOME_DETAIL_NOTE = new QName(PREMIS_NAMESPACE_URL, "eventOutcomeDetailNote");
    private static final QName QNAME_EVENT_OUTCOME_DETAIL = new QName(PREMIS_NAMESPACE_URL, "eventOutcomeDetail");
    private static final QName QNAME_FORMAT_DESIGNATION = new QName(PREMIS_NAMESPACE_URL, "formatDesignation");
    private static final QName QNAME_SIGNATURE = new QName(PREMIS_NAMESPACE_URL, "signature");

    private static final String PREMIS_XSI_LOCATION = "http://www.loc.gov/premis/v3 http://www.loc.gov/standards/premis/v3/premis.xsd";


    static {
        Init.init();
    }


    private XmlUtils() {
        throw new IllegalStateException("Utility class");
    }


    public static @NotNull String prettyPrint(@NotNull String xml) throws TransformerException {

        TransformerFactory factory = TransformerFactory.newInstance();
        factory.setAttribute(ACCESS_EXTERNAL_DTD, EMPTY);
        factory.setAttribute(ACCESS_EXTERNAL_STYLESHEET, EMPTY);

        Transformer transformer = factory.newTransformer();
        transformer.setOutputProperty(METHOD, "xml");
        transformer.setOutputProperty(ENCODING, UTF_8.name());
        transformer.setOutputProperty(INDENT, "yes");
        transformer.setOutputProperty(DOCTYPE_PUBLIC, "yes");
        transformer.setOutputProperty(OUTPUTKEYS_INDENT_AMOUNT, "2");

        StreamResult result = new StreamResult(new StringWriter());
        transformer.transform(new StreamSource(new ByteArrayInputStream(xml.getBytes(UTF_8))), result);

        return result.getWriter().toString();
    }


    /**
     * The PREMIS file is created and stored on every archive action.
     * Since we will lose every workflow information, this file will not be re-created anymore.
     * <p>
     * We had some issue with some old versions, returning some non-valid PREMIS file.
     * We have to fix it.
     * <p>
     * But we shall not use this to "improve" the stored PREMIS !
     * These stored files are only accessible through a deprecated entrypoint, and we should not help anyone not playing the game.
     * <p>
     * We'll only fix the ERRORS in here.
     *
     * @return The patched PREMIS file
     * @throws TransformerException
     */
    public static @NotNull String patchTrashBinPremis(ClassLoader loader, InputStream premisInputStream) throws TransformerException, IOException {

        SAXTransformerFactory transformerFactory = (SAXTransformerFactory) TransformerFactory.newInstance();
        transformerFactory.setAttribute(ACCESS_EXTERNAL_DTD, EMPTY);
        transformerFactory.setAttribute(ACCESS_EXTERNAL_STYLESHEET, EMPTY);
        transformerFactory.setFeature(FEATURE_SECURE_PROCESSING, true);

        TransformerHandler transformerHandler01 = transformerFactory
                .newTransformerHandler(new StreamSource(loader.getResourceAsStream("premis/trash-bin-patches/01-preservation-level.xslt")));

        // Building the chain of transformers, and feeding the inputStream
        String patched;
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {

            transformerHandler01.setResult(new StreamResult(baos));

            transformerFactory.newTransformer().transform(new StreamSource(premisInputStream), new SAXResult(transformerHandler01));

            // There are some differences between OracleJDK and OpenJDK results.
            // Canonicalization makes the results unit-tests friendly.
            patched = XmlUtils.canonicalize(baos.toByteArray());
        }

        // For some reason, the header is lost during the XSLT transform.
        // Transformer options does not seems to work, but it's pretty easy to fix anyway...
        patched = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + patched;

        return patched;
    }


    public static @NotNull String canonicalize(byte[] xmlBytes) throws TransformerException {

        String result;
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            Canonicalizer canonicalizer = Canonicalizer.getInstance(ALGO_ID_C14N_WITH_COMMENTS);
            canonicalizer.canonicalize(xmlBytes, baos, false);
            result = baos.toString(UTF_8);
        } catch (XMLSecurityException | IOException e) {
            throw new TransformerException(e.getLocalizedMessage());
        }

        return result;
    }


    public static @Nullable String getSignificantPropertyValue(@NotNull List<SignificantPropertiesComplexType> significantPropertiesComplexTypeList,
                                                               @NotNull String significantPropertyKey) {

        return significantPropertiesComplexTypeList.stream()
                .map(SignificantPropertiesComplexType::getContent)
                .filter(properties -> properties.stream()
                        .map(property -> extractSignificantPropertyKey(properties))
                        .anyMatch(value -> StringUtils.equals(value, significantPropertyKey))
                )
                .findFirst()
                .map(XmlUtils::extractSignificantPropertyValue)
                .orElse(null);
    }


    public static boolean getSignificantBooleanPropertyValue(@NotNull List<SignificantPropertiesComplexType> significantPropertiesComplexTypeList,
                                                             @NotNull String significantPropertyKey) {
        return Optional
                .ofNullable(getSignificantPropertyValue(significantPropertiesComplexTypeList, significantPropertyKey))
                .map(Boolean::parseBoolean)
                .orElse(false);
    }


    @SuppressWarnings("unchecked")
    public static @Nullable String extractSignificantPropertyKey(List<JAXBElement<?>> properties) {
        return properties.stream()
                .filter(property -> StringUtils.equals(property.getName().getLocalPart(), SIGNIFICANT_PROPERTY_KEY))
                .findFirst()
                .map(property -> (JAXBElement<StringPlusAuthority>) property)
                .map(JAXBElement::getValue)
                .map(StringPlusAuthority::getValue)
                .orElse(null);
    }


    @SuppressWarnings("unchecked")
    public static @Nullable String extractSignificantPropertyValue(List<JAXBElement<?>> properties) {
        return properties.stream()
                .filter(property -> StringUtils.equals(property.getName().getLocalPart(), SIGNIFICANT_PROPERTY_VALUE))
                .findFirst()
                .map(property -> (JAXBElement<String>) property)
                .map(JAXBElement::getValue)
                .orElse(null);
    }


    private static ObjectIdentifierComplexType createObjectIdentifier(@NotNull String key, @NotNull String value) {
        ObjectIdentifierComplexType objectIdentifier = new ObjectIdentifierComplexType();
        objectIdentifier.setObjectIdentifierType(createStringPlusAuthority(key));
        objectIdentifier.setObjectIdentifierValue(value);
        return objectIdentifier;
    }


    private static SignificantPropertiesComplexType createSignificantProperty(@NotNull String key, @NotNull String value) {
        SignificantPropertiesComplexType significantProperties = new SignificantPropertiesComplexType();
        significantProperties.getContent().addAll(List.of(
                createJaxbElement(QNAME_SIGNIFICANT_PROPERTY_KEY, createStringPlusAuthority(key)),
                createJaxbElement(QNAME_SIGNIFICANT_PROPERTY_VALUE, value)
        ));
        return significantProperties;
    }


    @SuppressWarnings(SONAR_CODE_BLOCKS)
    public static @NotNull String folderToPremisXml(@NotNull Folder folder,
                                                    @NotNull Function<String, InputStream> documentSupplier) {

        ResourceBundle messages = ResourceBundle.getBundle(MESSAGE_BUNDLE, Locale.getDefault());

        PremisComplexType premis = new PremisComplexType();
        premis.setVersion("3.0");

        IntellectualEntity intellectualEntity = new IntellectualEntity();
        {
            OriginalNameComplexType originalName = new OriginalNameComplexType();
            originalName.setValue(folder.getName());
            intellectualEntity.setOriginalName(originalName);

            intellectualEntity.getObjectIdentifier()
                    .add(createObjectIdentifier(FOLDER_ID_LABEL, folder.getId()));

            intellectualEntity.getSignificantProperties()
                    .add(createSignificantProperty(SIGNIFICANT_PROPERTY_TYPE_KEY, folder.getType().getName()));

            intellectualEntity.getSignificantProperties()
                    .add(createSignificantProperty(SIGNIFICANT_PROPERTY_SUBTYPE_KEY, folder.getSubtype().getName()));

            if (folder.getDueDate() != null) {
                String serializedDate = TextUtils.serializeDate(folder.getDueDate(), ISO8601_DATE_FORMAT);
                intellectualEntity.getSignificantProperties()
                        .add(createSignificantProperty(SIGNIFICANT_PROPERTY_DUE_DATE, serializedDate));
            }

            folder.getMetadata().entrySet().stream()
                    .filter(entry -> !StringUtils.startsWith(entry.getKey(), INTERNAL_PREFIX))
                    .filter(entry -> StringUtils.isNotEmpty(entry.getValue()))
                    .map(entry -> createSignificantProperty(entry.getKey(), entry.getValue()))
                    .forEach(significantProperty -> intellectualEntity.getSignificantProperties().add(significantProperty));
        }

        premis.getObject().add(intellectualEntity);

        for (Document document : folder.getDocumentList()) {
            File file = new File();
            {
                OriginalNameComplexType originalName = new OriginalNameComplexType();
                originalName.setValue(document.getName());
                file.setOriginalName(originalName);

                file.getObjectIdentifier()
                        .add(createObjectIdentifier(DOCUMENT_ID_LABEL, document.getId()));

                file.getSignificantProperties()
                        .add(createSignificantProperty(SIGNIFICANT_PROPERTY_MAIN_DOCUMENT_KEY, String.valueOf(document.isMainDocument())));

                ObjectCharacteristicsComplexType objectCharacteristicsComplexType = new ObjectCharacteristicsComplexType();
                {
                    FixityComplexType fixityComplexType = new FixityComplexType();
                    {
                        fixityComplexType.setMessageDigestAlgorithm(createStringPlusAuthority(SHA_256));
                        fixityComplexType.setMessageDigest(
                                Optional.ofNullable(document.getChecksumValue())
                                        .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "Missing digest value"))
                        );
                    }
                    objectCharacteristicsComplexType.getFixity().add(fixityComplexType);

                    objectCharacteristicsComplexType.setSize(document.getContentLength());

                    FormatComplexType formatComplexType = new FormatComplexType();
                    {
                        FormatDesignationComplexType formatDesignationComplexType = new FormatDesignationComplexType();
                        formatDesignationComplexType.setFormatName(createStringPlusAuthority(String.valueOf(document.getMediaType())));
                        formatDesignationComplexType.setFormatVersion(String.valueOf(document.getMediaVersion()));
                        formatComplexType.getContent().add(createJaxbElement(QNAME_FORMAT_DESIGNATION, formatDesignationComplexType));
                    }
                    objectCharacteristicsComplexType.getFormat().add(formatComplexType);
                }
                file.getObjectCharacteristics().add(objectCharacteristicsComplexType);

                for (DetachedSignature detachedSignature : document.getDetachedSignatures()) {
                    SignatureInformationComplexType signatureInformationComplexType = new SignatureInformationComplexType();
                    {
                        SignatureComplexType signatureComplexType = new SignatureComplexType();
                        {
                            signatureComplexType.setSignatureEncoding(createStringPlusAuthority(UTF_8.toString()));
                            signatureComplexType.setSignatureMethod(createStringPlusAuthority(
                                    (detachedSignature.getMediaType() == APPLICATION_XML ? XADES_DETACHED : PKCS7).getPremisMethod()
                            ));

                            signatureComplexType.setSignatureValue(
                                    Optional.ofNullable(documentSupplier.apply(detachedSignature.getId()))
                                            .map(documentInputStream -> {
                                                try (InputStream is = documentInputStream) {
                                                    return new String(is.readAllBytes(), UTF_8);
                                                } catch (IOException e) {
                                                    log.warn("Something went wrong while reading the signature file", e);
                                                    return null;
                                                }
                                            })
                                            .filter(StringUtils::isNotEmpty)
                                            .orElseThrow(() -> new ResponseStatusException(INTERNAL_SERVER_ERROR, "Missing detached signature"))
                            );

                            signatureComplexType.setSignatureValidationRules(createStringPlusAuthority(
                                    (detachedSignature.getMediaType() == APPLICATION_XML ? XADES_DETACHED : PKCS7).getPremisValidationRule()
                            ));
                        }
                        signatureInformationComplexType.getContent().add(createJaxbElement(QNAME_SIGNATURE, signatureComplexType));
                    }
                    file.getSignatureInformation().add(signatureInformationComplexType);
                }
            }
            premis.getObject().add(file);
        }

        List<Task> cleanedTask = folder.getStepList().stream()
                .filter(task -> {
                    // Pending READ and UNDO actions are irrelevant
                    boolean isAnIrrelevantTask = (task.getAction() == READ) || (task.getAction() == UNDO);
                    boolean isPending = task.getPerformedAction() == null;
                    return !(isAnIrrelevantTask && isPending);
                })
                .peek(task -> {
                    // FIXME: for some reason, the past READ tasks have a performedAction as the main action.
                    //  This is an ugly fix
                    boolean isRead = task.getAction() == READ;
                    boolean isPerformed = task.getPerformedAction() != null;
                    //noinspection ConstantValue
                    if (isRead && isPerformed) {
                        task.setPerformedAction(READ);
                    }
                })
                .toList();

        for (Task task : cleanedTask) {
            EventComplexType eventComplexType = new EventComplexType();
            {
                EventIdentifierComplexType eventIdentifierComplexType = new EventIdentifierComplexType();
                {
                    eventIdentifierComplexType.setEventIdentifierType(createStringPlusAuthority("taskId"));
                    eventIdentifierComplexType.setEventIdentifierValue(firstNonNull(task.getId(), EMPTY));
                }
                eventComplexType.setEventIdentifier(eventIdentifierComplexType);

                Action action = firstNonNull(task.getPerformedAction(), task.getAction(), UNKNOWN);
                String serializedDate = Optional.ofNullable(task.getDate())
                        .map(date -> TextUtils.serializeDate(date, ISO8601_DATE_TIME_FORMAT))
                        .orElse(EMPTY);

                eventComplexType.setEventType(createStringPlusAuthority(action.toString()));
                eventComplexType.setEventDateTime(serializedDate);

                // TODO : Find a way to get the original task's desk, and print the delegation
                // if (!task.getCandidateDesk().contains(task.getPublicAnnotation())) {
                //     w.writeStartElement("eventDetail");
                //     w.writeStartElement("eventDetailInformation");
                //     w.writeCharacters(String.format("Par suppléance de : %s", task.getDesk().getName());
                //     w.writeEndElement();
                //     w.writeEndElement();
                // }

                if (StringUtils.isNotEmpty(task.getPublicAnnotation())) {
                    EventOutcomeInformationComplexType eventOutcomeInformationComplexType = new EventOutcomeInformationComplexType();
                    EventOutcomeDetailComplexType eventOutcomeDetailComplexType = new EventOutcomeDetailComplexType();
                    eventOutcomeDetailComplexType.getContent().add(createJaxbElement(QNAME_EVENT_OUTCOME_DETAIL_NOTE, task.getPublicAnnotation()));
                    eventOutcomeInformationComplexType.getContent().add(createJaxbElement(QNAME_EVENT_OUTCOME_DETAIL, eventOutcomeDetailComplexType));
                    eventComplexType.getEventOutcomeInformation().add(eventOutcomeInformationComplexType);
                }

                LinkingAgentIdentifierComplexType linkingAgentIdentifierComplexType = new LinkingAgentIdentifierComplexType();
                {
                    String userId = Optional.ofNullable(task.getUser())
                            .map(UserRepresentation::getId)
                            .orElse(EMPTY);

                    linkingAgentIdentifierComplexType.setLinkingAgentIdentifierType(createStringPlusAuthority("userId"));
                    linkingAgentIdentifierComplexType.setLinkingAgentIdentifierValue(userId);

                    List<StringPlusAuthority> deskStringPlusAuthorityList = task.getDesks().stream()
                            .map(desk -> Optional.of(desk)
                                    .map(DeskRepresentation::getName)
                                    .orElse(messages.getString("message.deleted_desk"))
                            )
                            .map(XmlUtils::createStringPlusAuthority)
                            .toList();
                    linkingAgentIdentifierComplexType.getLinkingAgentRole().addAll(deskStringPlusAuthorityList);
                }
                eventComplexType.getLinkingAgentIdentifier().add(linkingAgentIdentifierComplexType);
            }
            premis.getEvent().add(eventComplexType);
        }

        List<UserRepresentation> userSet = cleanedTask.stream()
                .map(Task::getUser)
                .filter(Objects::nonNull)
                .filter(u -> u.getId() != null)
                .filter(distinctByProperty(UserRepresentation::getId))
                .toList();

        for (UserRepresentation user : userSet) {

            AgentComplexType agentComplexType = new AgentComplexType();
            {
                agentComplexType.getAgentName().add(createStringPlusAuthority(UserUtils.prettyPrint(user)));

                AgentIdentifierComplexType agentIdentifierComplexType = new AgentIdentifierComplexType();
                {
                    agentIdentifierComplexType.setAgentIdentifierType(createStringPlusAuthority("userId"));
                    agentIdentifierComplexType.setAgentIdentifierValue(user.getId());
                }
                agentComplexType.getAgentIdentifier().add(agentIdentifierComplexType);
            }
            premis.getAgent().add(agentComplexType);
        }

        try (StringWriter writer = new StringWriter()) {

            JAXBElement<PremisComplexType> premisXml = new ObjectFactory().createPremis(premis);
            JAXBContext jaxbContext = JAXBContext.newInstance(PremisComplexType.class);

            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(JAXB_ENCODING, UTF_8.toString());
            marshaller.setProperty(JAXB_SCHEMA_LOCATION, PREMIS_XSI_LOCATION);
            marshaller.marshal(premisXml, writer);

            return writer.toString();

        } catch (IOException | JAXBException e) {
            log.error("Error initializing the XML serializer. This should never happen...", e);
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e.getLocalizedMessage());
        }
    }


    public static @NotNull PremisComplexType parsePremisString(@NotNull InputStream folderPremisInputStream) {
        PremisComplexType result;

        try (InputStream inputStream = folderPremisInputStream) {

            JAXBContext jaxbContext = JAXBContext.newInstance(PremisComplexType.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            //noinspection unchecked
            JAXBElement<PremisComplexType> premisComplexTypeElement = (JAXBElement<PremisComplexType>) unmarshaller.unmarshal(inputStream);
            result = premisComplexTypeElement.getValue();

        } catch (JAXBException | IOException ex) {
            log.debug("Error on PREMIS parse: {}", ex.getMessage(), ex);
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.error_reading_premis_file");
        }

        return result;
    }


    private static StringPlusAuthority createStringPlusAuthority(String value) {
        StringPlusAuthority stringPlusAuthority = new StringPlusAuthority();
        stringPlusAuthority.setValue(value);
        return stringPlusAuthority;
    }


    @SuppressWarnings("unchecked")
    private static <T> JAXBElement<T> createJaxbElement(QName qName, T value) {
        return new JAXBElement<>(qName, (Class<T>) value.getClass(), value);
    }


}
