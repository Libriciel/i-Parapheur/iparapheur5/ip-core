/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.utils;

import coop.libriciel.ipcore.model.content.DocumentBuffer;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.io.IOUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.http.ContentDisposition;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.ClientResponse;
import reactor.core.publisher.Flux;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static coop.libriciel.ipcore.services.content.ContentServiceInterface.FILE_TRANSFER_BUFFER_SIZE;
import static org.apache.commons.io.FilenameUtils.getExtension;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.MediaType.*;
import static org.springframework.util.ResourceUtils.CLASSPATH_URL_PREFIX;
import static org.springframework.util.ResourceUtils.FILE_URL_PREFIX;


@Log4j2
public class FileUtils {


    public static final String TEMPLATE_PATH = CLASSPATH_URL_PREFIX + "templates/";
    public static final String MAIL_TEMPLATE_PATH = TEMPLATE_PATH + "mail/";
    public static final String SIGNATURE_TEMPLATE_PATH = TEMPLATE_PATH + "signature/";
    public static final MediaType MEDIA_TYPE_ZIP = new MediaType("application", "zip");

    /**
     * Sometimes, we cannot use the more appropriate {@link ContentDisposition#parse}.
     * Alfresco itself does not return an RFC 5987 compliant header when the file has parenthesis in its name.
     * We go for the manual regex.
     */
    private static final Pattern ATTACHMENT_FILENAME_PATTERN = Pattern.compile("\\sfilename=\"(?<filename>.*?)\";\\s");


    private FileUtils() {
        throw new IllegalStateException("Utility class");
    }


    /**
     * Send back the most appropriate overridable resource in this order :
     * - The current file path (<code>/</code> in a Docker image)
     * - The classpath (the <code>src/main/resource</code> folder, before compilation)
     * - A {@link RuntimeException}, if nothing matches.
     * <p>
     * Weird that SpringBoot does not have such a feature for any resource.
     * It natively works for `application.properties|yml` only.
     *
     * @param resourceLoader Should be autowired
     * @param resourceName   If it includes a prefixed path, the overridden file should be prefixed too
     * @return an existing {@link Resource} file
     */
    public static @NotNull Resource getOverridableResource(@NotNull ResourceLoader resourceLoader, @NotNull String resourceName) {
        Resource result = resourceLoader.getResource(FILE_URL_PREFIX + resourceName);

        if (result.exists()) {
            log.debug("Overridden '{}' file found", resourceName);
            return result;
        }

        return Optional.of(resourceLoader.getResource(CLASSPATH_URL_PREFIX + resourceName))
                .filter(Resource::exists)
                .orElseThrow(() -> new RuntimeException("The resource " + resourceName + " should have a default value in the resource folder."));
    }


    public static @Nullable String parseFileNameFromHeaders(@NotNull ClientResponse.Headers headers, @Nullable String defaultValue) {
        Optional<String> resultOpt = headers.header(CONTENT_DISPOSITION)
                .stream()
                .map(FileUtils::parseFileNameFromAttachment)
                .filter(StringUtils::isNotEmpty)
                .findFirst();

        if (resultOpt.isEmpty()) {
            log.debug("parseFileNameFromHeaders - could not find filename in response headers, using default value : {}", defaultValue);
        }

        return resultOpt.orElse(defaultValue);
    }


    public static @Nullable String parseFileNameFromAttachment(@Nullable String attachmentHeader) {
        return Optional.ofNullable(attachmentHeader)
                .map(ATTACHMENT_FILENAME_PATTERN::matcher)
                .filter(Matcher::find)
                .map(m -> m.group("filename"))
                .orElse(null);
    }


    /**
     * Append an enumeration on existing name files.
     *
     * @param filename
     * @param alreadyExistingNames the pre-existing file names
     * @return something like "filename (2).pdf"
     */
    public static @NotNull String enumerateOnDuplicates(@NotNull String filename, @NotNull Set<String> alreadyExistingNames) {

        if (!alreadyExistingNames.contains(filename)) {
            return filename;
        }

        String baseName = FilenameUtils.getBaseName(filename);
        String extension = FilenameUtils.getExtension(filename);

        return IntStream.range(2, alreadyExistingNames.size() + 2)
                .mapToObj(index -> "%s (%d).%s".formatted(baseName, index, extension))
                .filter(name -> !alreadyExistingNames.contains(name))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Something went wrong while computing the fileNames file names"));
    }


    /**
     * Clean up special chars and too long names.
     * The extension is not expected, we're working on the regular file name.
     *
     * @param filename
     * @return
     */
    public static @NotNull String sanitizeFileName(@NotNull String filename) {

        String result = filename
                // Chrome
                .replaceAll(",", "_")
                // Linux/Unix
                .replaceAll("/", "_")
                // MacOS
                .replaceAll(":", "-")
                // Windows
                .replaceAll("<", "_")
                .replaceAll(">", "_")
                .replaceAll("\\\\", "_")
                .replaceAll("\\|", "_")
                .replaceAll("\\?", "_")
                .replaceAll("\\*", "_");

        // Windows don't handle very well names over 255 chars. We want to remove 10 more to the 255-limit:
        // - 5 chars for the ".docx" extension,
        // - 5 chars for a potential "_(99)" counter, if we download the same file multiple times.
        // - if we have to truncate, 3 more chars for the ellipsis/dots.
        if (result.length() > 245) {
            result = "%s...".formatted(StringUtils.substring(result, 0, 242));
        }

        return result;
    }


    public static boolean isXml(@Nullable MediaType mediaType) {
        return (mediaType != null) && Stream
                .of(APPLICATION_XML, APPLICATION_XHTML_XML, TEXT_XML, TEXT_HTML)
                .anyMatch(s -> s.equalsTypeAndSubtype(mediaType));
    }


    /**
     * Creates a tempFile from the given InputStream.
     *
     * @param inputStream that will be closed before the end of the method call
     * @return A file that will be deleted on application exit
     */
    public static @NotNull File createTempFile(@NotNull InputStream inputStream, @NotNull String prefix, @NotNull String suffix) throws IOException {

        File tempDocumentFile = File.createTempFile(prefix, suffix);
        tempDocumentFile.deleteOnExit();

        try (FileOutputStream tempDocumentInputStream = new FileOutputStream(tempDocumentFile);
             InputStream tempDocumentStream = inputStream) {

            IOUtils.copy(tempDocumentStream, tempDocumentInputStream);

        }

        return tempDocumentFile;
    }


    /**
     * Creates a tempFile from the given InputStream.
     * A wrapper around the {@link FileUtils#createTempFile}, with a try/catch.
     *
     * @param inputStream that will be closed before the end of the method call
     * @return A file that will be deleted on application exit
     */
    public static @NotNull File safeCreateTempFile(@NotNull InputStream inputStream, @NotNull String prefix, @NotNull String suffix) {
        try {
            return FileUtils.createTempFile(inputStream, prefix, suffix);
        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.error_generating_temporary_file", e);
        }
    }


    public static DocumentBuffer getDocumentBufferFromFile(File document, String documentName, boolean isSignatureProof) {
        Flux<DataBuffer> dataBuffer = DataBufferUtils.read(document.toPath(), new DefaultDataBufferFactory(), FILE_TRANSFER_BUFFER_SIZE);
        DocumentBuffer documentBuffer = new DocumentBuffer();
        documentBuffer.setContentFlux(dataBuffer);
        documentBuffer.setName(documentName);
        documentBuffer.setSignatureProof(isSignatureProof);

        MediaType mediaType = getMediaTypeFromFileName(documentName);
        documentBuffer.setMediaType(mediaType);

        return documentBuffer;
    }


    public static @Nullable MediaType getMediaTypeFromFileName(@NotNull String fileName) {
        String extension = getExtension(fileName);
        log.debug("getMediaTypeFromFileName extension:{}", extension);
        return switch (extension) {
            case "pdf" -> APPLICATION_PDF;
            case "xml" -> APPLICATION_XML;
            case "doc" -> new MediaType("application", "msword");
            case "xls" -> new MediaType("application", "vnd.ms-excel");
            case "ppt" -> new MediaType("application", "vnd.ms-powerpoint");
            case "docx" -> new MediaType("application", "vnd.openxmlformats-officedocument.wordprocessingml.document");
            case "xlsx" -> new MediaType("application", "vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            case "pptx" -> new MediaType("application", "vnd.openxmlformats-officedocument.presentationml.presentation");
            case "odt" -> new MediaType("application", "vnd.oasis.opendocument.text");
            case "ods" -> new MediaType("application", "vnd.oasis.opendocument.spreadsheet");
            case "odp" -> new MediaType("application", "vnd.oasis.opendocument.presentation");
            default -> {
                log.warn("getMediaTypeFromFileName unknown extension: {}", extension);
                yield null;
            }
        };
    }


}
