/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.utils;

import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;

import java.util.function.Supplier;


@Log4j2
public class ApiUtils {


    public static final int MAX_DESKS_COUNT = 250000;
    public static final int MAX_OWNERS_PER_DESKS_COUNT = 250;
    public static final int MAX_USERS_COUNT = 50000;
    public static final int MAX_WORKFLOWS_COUNT = 50000;
    public static final int MAX_TYPES_COUNT = 50000;
    public static final int MAX_SUBTYPES_COUNT = 50000;
    public static final int MAX_FOLDERS_COUNT = 1000000;
    public static final int MAX_METADATAS_COUNT = 50000;
    public static final int MAX_LAYERS_COUNT = 50000;

    public static final int MAX_DOCUMENT_SIZE = 200 * 1024 * 1024;


    private ApiUtils() {
        throw new IllegalStateException("Utility class");
    }


    /**
     * This is only used on error declaration.
     * TODO: Find the SpringBoot inner error message model instead.
     */
    @Data
    public static class ErrorResponse {

        private String timestamp;
        private int status;
        private String error;
        private String message;
        private String path;

    }


    public static final String CODE_200 = "200";
    public static final String CODE_201 = "201";
    public static final String CODE_204 = "204";
    public static final String CODE_400 = "400";
    public static final String CODE_401 = "401";
    public static final String CODE_403 = "403";
    public static final String CODE_404 = "404";
    public static final String CODE_406 = "406";
    public static final String CODE_407 = "407";
    public static final String CODE_409 = "409";
    public static final String CODE_500 = "500";
    public static final String CODE_507 = "507";

    /**
     * Validated annotations support translated templates wrapped in curly braces : @Min{value=5, message="{message.xxx}"}
     * Those messages will be properly translated and returned by the SpringBoot engine. Nothing more to do.
     * <p>
     * Yet, wrapping those directly will mark translations as unused.
     * Splitting the braces wrapper and the real "message.xxx", computing those on the fly, will solve everything.
     */
    public static final String JAVAX_MESSAGE_PREFIX = "{";
    public static final String JAVAX_MESSAGE_SUFFIX = "}";

    public static final int NAME_MIN_LENGTH = 2;
    public static final int NAME_MAX_LENGTH = 255;
    public static final String NAME_SIZE_ERROR_MESSAGE = "message.name_field_should_be_between_2_and_255_chars";
    public static final String NAME_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE = JAVAX_MESSAGE_PREFIX + NAME_SIZE_ERROR_MESSAGE + JAVAX_MESSAGE_SUFFIX;

    public static final String NAME_CONTAINS_FORBIDDEN_CHARACTER_ERROR_MESSAGE = "message.name_contains_invalid_char";
    public static final String NAME_CONTAINS_FORBIDDEN_CHARACTER_JAVAX_VALIDATION_ERROR_MESSAGE = JAVAX_MESSAGE_PREFIX
            + NAME_CONTAINS_FORBIDDEN_CHARACTER_ERROR_MESSAGE
            + JAVAX_MESSAGE_SUFFIX;

    public static final String KEY_CONTAINS_FORBIDDEN_CHARACTER_ERROR_MESSAGE = "message.key_contains_invalid_char_or_starts_with_a_number";
    public static final String KEY_CONTAINS_FORBIDDEN_CHARACTER_JAVAX_VALIDATION_ERROR_MESSAGE = JAVAX_MESSAGE_PREFIX
            + KEY_CONTAINS_FORBIDDEN_CHARACTER_ERROR_MESSAGE
            + JAVAX_MESSAGE_SUFFIX;

    public static final int KEY_MIN_LENGTH = 1;
    public static final int KEY_MAX_LENGTH = 128;
    public static final String KEY_SIZE_ERROR_MESSAGE = "message.key_field_should_be_between_1_and_128_chars";
    public static final String KEY_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE = JAVAX_MESSAGE_PREFIX + KEY_SIZE_ERROR_MESSAGE + JAVAX_MESSAGE_SUFFIX;

    public static final int DESCRIPTION_MIN_LENGTH = 3;
    public static final int DESCRIPTION_MAX_LENGTH = 255;
    public static final String DESCRIPTION_SIZE_ERROR_MESSAGE = "message.description_field_should_be_between_3_and_255_chars";
    public static final String DESCRIPTION_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE = JAVAX_MESSAGE_PREFIX + DESCRIPTION_SIZE_ERROR_MESSAGE + JAVAX_MESSAGE_SUFFIX;

    public static final int ANNOTATION_MAX_LENGTH = 512;
    public static final String ANNOTATION_SIZE_ERROR_MESSAGE = "message.annotation_should_be_under_512_chars";
    public static final String ANNOTATION_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE = JAVAX_MESSAGE_PREFIX + ANNOTATION_SIZE_ERROR_MESSAGE + JAVAX_MESSAGE_SUFFIX;

    public static final int GDPR_FIELD_MAX_LENGTH = 255;
    public static final String GDPR_NAME_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE = JAVAX_MESSAGE_PREFIX +  "message.error_gdpr_name_field_size" + JAVAX_MESSAGE_SUFFIX;
    public static final String GDPR_ADDRESS_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE = JAVAX_MESSAGE_PREFIX + "message.error_gdpr_address_field_size" + JAVAX_MESSAGE_SUFFIX;
    public static final String GDPR_SIRET_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE = JAVAX_MESSAGE_PREFIX + "message.error_gdpr_siret_field_size" + JAVAX_MESSAGE_SUFFIX;
    public static final String GDPR_ENTITY_APECODE_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE = JAVAX_MESSAGE_PREFIX + "message.error_gdpr_entity_apecode_field_size" + JAVAX_MESSAGE_SUFFIX;
    public static final String GDPR_ENTITY_PHONE_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE = JAVAX_MESSAGE_PREFIX + "message.error_gdpr_entity_phone_field_size" + JAVAX_MESSAGE_SUFFIX;
    public static final String GDPR_ENTITY_MAIL_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE = JAVAX_MESSAGE_PREFIX + "message.error_gdpr_entity_mail_field_size" + JAVAX_MESSAGE_SUFFIX;
    public static final String GDPR_RESPONSIBLE_NAME_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE = JAVAX_MESSAGE_PREFIX + "message.error_gdpr_responsible_name_field_size" + JAVAX_MESSAGE_SUFFIX;
    public static final String GDPR_RESPONSIBLE_TITLE_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE = JAVAX_MESSAGE_PREFIX + "message.error_gdpr_responsible_title_field_size" + JAVAX_MESSAGE_SUFFIX;
    public static final String GDPR_DPO_NAME_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE = JAVAX_MESSAGE_PREFIX + "message.error_gdpr_dpo_name_field_size" + JAVAX_MESSAGE_SUFFIX;
    public static final String GDPR_DPO_MAIL_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE = JAVAX_MESSAGE_PREFIX + "message.error_gdpr_dpo_mail_field_size" + JAVAX_MESSAGE_SUFFIX;
    public static final String GDPR_HOSTING_COMMENT_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE = JAVAX_MESSAGE_PREFIX + "message.error_gdpr_hosting_comment_field_size" + JAVAX_MESSAGE_SUFFIX;


    /**
     * Some methods cannot manage some DTO fields.
     * Those fields are usually ignored, but if someone tries to set one of these, it won't work.
     * We want to assert that some parameters are not set, to avoid misconceptions, and print a warning.
     *
     * @param supplier       the method that should return null
     * @param warningMessage the logged message
     */
    public static void assertNullity(@NotNull Supplier<Object> supplier, @NotNull String warningMessage) {
        if (supplier.get() != null) {
            log.warn(warningMessage);
        }
    }


}
