/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.utils.connector;

import lombok.extern.log4j.Log4j2;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import reactor.netty.http.client.HttpClient;
import reactor.netty.resources.ConnectionProvider;

import static coop.libriciel.ipcore.utils.RequestUtils.HTTP_CONNECTOR_RESPONSE_TIMEOUT;


@Log4j2
public class IpDefaultConnector extends ReactorClientHttpConnector {


    public IpDefaultConnector() {
        super(HttpClient.create().followRedirect(true).responseTimeout(HTTP_CONNECTOR_RESPONSE_TIMEOUT));
    }


    public IpDefaultConnector(boolean followRedirect) {
        super(HttpClient.create().followRedirect(followRedirect).responseTimeout(HTTP_CONNECTOR_RESPONSE_TIMEOUT));
    }


    public IpDefaultConnector(ConnectionProvider connectionPool) {
        super(HttpClient.create(connectionPool).followRedirect(true).responseTimeout(HTTP_CONNECTOR_RESPONSE_TIMEOUT));
    }


    public IpDefaultConnector(boolean followRedirect, ConnectionProvider connectionPool) {
        super(HttpClient.create(connectionPool).followRedirect(followRedirect).responseTimeout(HTTP_CONNECTOR_RESPONSE_TIMEOUT));
    }


}
