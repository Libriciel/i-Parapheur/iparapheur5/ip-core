/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.utils;

import org.jetbrains.annotations.NotNull;


public class WorkflowUtils {


    public static final String WORKFLOW_KEY_REGEXP = "^[^\\d][a-z\\d_.-]*$";
    private static final String WORKFLOW_KEY_FORBIDDEN_CHARS_REGEXP = "[^a-z\\d_.-]";


    private WorkflowUtils() {
        throw new IllegalStateException("Utility class");
    }


    /**
     * The v4 scripts were used with the name, containing spaces, upper-cases, and other v5 forbidden chars.
     * We need a special treatment here, to ease v4 to v5 migrations.
     *
     * @param workflowName
     * @return
     */
    public static @NotNull String nameToKey(@NotNull String workflowName) {

        // Forcing the lowercase and underscores
        String key = workflowName
                .toLowerCase()
                .replaceAll(WORKFLOW_KEY_FORBIDDEN_CHARS_REGEXP, "_");

        // Force an underscore if there is a number as first char, for the same reason
        key = key.replaceAll("^(\\d)", "_$1");

        return key;
    }


}
