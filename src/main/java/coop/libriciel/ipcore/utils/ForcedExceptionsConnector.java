/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
package coop.libriciel.ipcore.utils;

import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.reactive.ClientHttpRequest;
import org.springframework.http.client.reactive.ClientHttpResponse;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;
import reactor.netty.resources.ConnectionProvider;

import java.net.URI;
import java.util.function.Function;

import static coop.libriciel.ipcore.utils.RequestUtils.HTTP_CONNECTOR_RESPONSE_TIMEOUT;
import static org.springframework.http.HttpStatus.BAD_REQUEST;


/**
 * The default HttpClient does not fail on 400+ status codes.
 * <p>
 * In here, we just override the connect method, to force an error on such codes.
 * It smells like a hack, but it is pretty straightforward, and it give us a cleaner error management.
 */
@Log4j2
public class ForcedExceptionsConnector extends ReactorClientHttpConnector {


    public ForcedExceptionsConnector() {
        super(HttpClient.create().followRedirect(true).responseTimeout(HTTP_CONNECTOR_RESPONSE_TIMEOUT));
    }


    public ForcedExceptionsConnector(boolean followRedirect) {
        super(HttpClient.create().followRedirect(followRedirect).responseTimeout(HTTP_CONNECTOR_RESPONSE_TIMEOUT));
    }


    public ForcedExceptionsConnector(ConnectionProvider connectionPool) {
        super(HttpClient.create(connectionPool).followRedirect(true).responseTimeout(HTTP_CONNECTOR_RESPONSE_TIMEOUT));
    }


    public ForcedExceptionsConnector(boolean followRedirect, ConnectionProvider connectionPool) {
        super(HttpClient.create(connectionPool).followRedirect(followRedirect).responseTimeout(HTTP_CONNECTOR_RESPONSE_TIMEOUT));
    }


    @Override
    public @NotNull Mono<ClientHttpResponse> connect(@NotNull HttpMethod method,
                                                     @NotNull URI uri,
                                                     @NotNull Function<? super ClientHttpRequest, Mono<Void>> requestCallback) {

        Mono<ClientHttpResponse> response = super.connect(method, uri, requestCallback);
        return response.flatMap(r -> {
            if (r.getStatusCode().value() >= BAD_REQUEST.value()) {
                return Mono.error(new LocalizedStatusException(r.getStatusCode(), "message.error_requesting_u", uri));
            } else {
                return Mono.just(r);
            }
        });
    }


}
