/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.utils;

import coop.libriciel.ipcore.model.crypto.PdfSignaturePosition;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.TextPosition;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static coop.libriciel.ipcore.model.pdfstamp.Origin.CENTER;


/**
 * This is a naive search based on the official PdfBox's PrintTextLocations sample.
 * Pretty much every line has been modified, though, but it was a straightforward work around it.
 * We may retrieve and work around the same sample on a breaking lib update.
 */
@Log4j2
public class PdfTextLocationStripper extends PDFTextStripper {


    private static final String DEFAULT_SIGNATURE_TAG = "#signature#";
    private static final String DEFAULT_SEAL_TAG = "#cachet#";
    private static final String COUNT_TAG = "%n";
    private static final String COUNT_TAG_INDEX = "index";
    private static final String COUNT_TAG_REGEX = "(?<" + COUNT_TAG_INDEX + ">\\d+)?";


    private final Pattern signatureTagRegex;
    private final boolean signatureTagRegexHasCountPlaceholder;
    private final Pattern sealTagRegex;
    private final boolean sealTagRegexHasCountPlaceholder;

    private final @Getter Map<Integer, PdfSignaturePosition> signatureTagsFound = new HashMap<>();
    private final @Getter Map<Integer, PdfSignaturePosition> sealTagsFound = new HashMap<>();


    /**
     * Instantiate a new PDFTextStripper object.
     *
     * @throws IOException If there is an error loading the properties.
     */
    public PdfTextLocationStripper(@Nullable String signatureTag, @Nullable String sealTag) throws IOException {

        signatureTagRegexHasCountPlaceholder = Optional.ofNullable(signatureTag)
                .filter(StringUtils::isNotEmpty)
                .map(t -> StringUtils.contains(t, COUNT_TAG))
                .orElse(false);
        sealTagRegexHasCountPlaceholder = Optional.ofNullable(sealTag)
                .filter(StringUtils::isNotEmpty)
                .map(t -> StringUtils.contains(t, COUNT_TAG))
                .orElse(false);

        String currentSignatureTagRegex = Optional.ofNullable(signatureTag)
                .filter(StringUtils::isNotEmpty)
                .map(t -> t.replace(COUNT_TAG, COUNT_TAG_REGEX))
                .orElse(DEFAULT_SIGNATURE_TAG);
        String currentSealTagRegex = Optional.ofNullable(sealTag)
                .filter(StringUtils::isNotEmpty)
                .map(t -> t.replace(COUNT_TAG, COUNT_TAG_REGEX))
                .orElse(DEFAULT_SEAL_TAG);

        signatureTagRegex = Pattern.compile(currentSignatureTagRegex);
        sealTagRegex = Pattern.compile(currentSealTagRegex);
    }


    /**
     * This will print the documents data.
     *
     * @throws IOException If there is an error parsing the document.
     */
    public void search(PDDocument document) throws IOException {

        setSortByPosition(true);
        setStartPage(0);
        setEndPage(document.getNumberOfPages());

        Writer dummy = new OutputStreamWriter(new ByteArrayOutputStream());
        writeText(document, dummy);
    }


    /**
     * Override the default functionality of PDFTextStripper.
     */
    @Override
    protected void writeString(String string, List<TextPosition> textPositions) {
        search(string, textPositions, signatureTagRegexHasCountPlaceholder, signatureTagRegex, signatureTagsFound);
        search(string, textPositions, sealTagRegexHasCountPlaceholder, sealTagRegex, sealTagsFound);
    }


    /**
     * Signature and Seal tag search have exactly the same behaviour.
     * <p>
     * Yet, we don't want to open the PDF and stream it twice,
     * that's why we perform twice the same kind of search.
     *
     * @param text                     The full text to search
     * @param textPositions            The full text's positions, split by letters
     * @param regexHasCountPlaceholder Pre-computed variable, to avoid unnecessary searches.
     * @param tagRegex                 Pre-computed variable, the regex to execute
     * @param resultMap                The appropriate result wrapper, to be filled
     */
    private void search(@NotNull String text, @NotNull List<TextPosition> textPositions, boolean regexHasCountPlaceholder,
                        @NotNull Pattern tagRegex, @NotNull Map<Integer, PdfSignaturePosition> resultMap) {

        // Checking string candidate

        if (!resultMap.isEmpty() && !regexHasCountPlaceholder) {
            return;
        }

        Matcher matcher = tagRegex.matcher(text);
        if (!matcher.find()) {
            return;
        }

        log.trace("PdfTextLocationStripper string size:{}", text.length());
        log.trace("PdfTextLocationStripper textPositions size:{}", textPositions.size());

        // Computing location

        if (textPositions.size() < matcher.end()) {
            return;
        }

        TextPosition start = textPositions.get(matcher.start());
        TextPosition end = textPositions.get(matcher.end() - 1);

        PdfSignaturePosition foundResult = new PdfSignaturePosition(
                (end.getX() + start.getX()) / 2.0f,
                (end.getY() + start.getY()) / 2.0f,
                super.getCurrentPageNo(),
                CENTER,
                null
        );

        // Crypto is bottom-left based, we have to convert the Y axis
        foundResult.setY(end.getPageHeight() - foundResult.getY());

        // Storing the result.
        // Yet, only the first result counts.

        int placeholderIndex = Optional.of(matcher)
                .filter(t -> regexHasCountPlaceholder)
                .map(m -> m.group(COUNT_TAG_INDEX))
                .map(NumberUtils::createInteger)
                .orElse(0);

        if (resultMap.containsKey(placeholderIndex)) {
            return;
        }

        log.debug("PdfTextLocationStripper index:{} foundResult:{}", placeholderIndex, foundResult);
        resultMap.put(placeholderIndex, foundResult);
    }


}
