/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.utils;

import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringSubstitutor;
import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;

import static coop.libriciel.ipcore.services.auth.KeycloakService.*;
import static coop.libriciel.ipcore.utils.CollectionUtils.toMutableList;
import static coop.libriciel.ipcore.utils.TextUtils.*;
import static java.util.stream.Collectors.toSet;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;


@Log4j2
public class KeycloakSecurityUtils {

    public static final String ROLE_PREFIX_FOR_SPRING_AUTH = "ROLE_";


    public static String getCurrentSessionUserId() throws ResponseStatusException {
        Authentication token = SecurityContextHolder.getContext().getAuthentication();
        if (token == null) {
            throw new ResponseStatusException(UNAUTHORIZED, "Aucun utilisateur connecté");
        }
        return token.getName();
    }


    public static String getCurrentSessionUserIdNoException() {
        return Optional.ofNullable(SecurityContextHolder.getContext())
                .map(SecurityContext::getAuthentication)
                .map(Principal::getName)
                .orElse(null);
    }


    public static List<String> getCurrentUserRoles() {
        log.debug("getCurrentUserRoles");
        Authentication token = SecurityContextHolder.getContext().getAuthentication();
        if (token == null) {
            throw new ResponseStatusException(UNAUTHORIZED, "No authenticated user");
        }
        return token.getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .map(roleName -> roleName.substring(ROLE_PREFIX_FOR_SPRING_AUTH.length()))
                .toList();
    }


    public static String getCurrentUserRawTokenString() {
        Authentication token = SecurityContextHolder.getContext().getAuthentication();
        if (token == null) {
            throw new ResponseStatusException(UNAUTHORIZED, "No authenticated user");
        }

        if (token.getPrincipal() == null || !(token.getPrincipal() instanceof Jwt jwtToken)) {
            throw new ResponseStatusException(UNAUTHORIZED, "Invalid authentication token, could not parse it into a Spring Jwt");
        }

        return jwtToken.getTokenValue();
    }


    public static @NotNull Set<String> getCurrentUserTenantIds() {
        List<String> roles = getCurrentUserRoles();
        log.trace("Token roles:{}", roles);

        Set<String> result = roles.stream()
                .map(role -> Optional.of(TENANT_ID_NAME_PATTERN.matcher(role))
                        .filter(Matcher::matches)
                        .or(() -> Optional.of(TENANT_ID_ADMIN_NAME_PATTERN.matcher(role)))
                        .filter(Matcher::matches)
                        .or(() -> Optional.of(TENANT_ID_FUNCTIONAL_ADMIN_NAME_PATTERN.matcher(role)))
                        .filter(Matcher::matches)
                )
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(m -> m.group(MATCH_GROUP_TENANT_ID))
                .collect(toSet());

        log.debug("getCurrentUserTenantIds result:{}", result);
        return result;
    }


    public static @NotNull List<String> getCurrentUserDeskIds() {
        return getCurrentUserDesks().stream()
                .map(DeskRepresentation::getId)
                .collect(toMutableList());
    }


    public static @NotNull List<DeskRepresentation> getCurrentUserDesks() {
        List<String> roles = getCurrentUserRoles();
        log.trace("Token roles:{}", roles);

        List<DeskRepresentation> result = roles.stream()
                .map(DESK_INTERNAL_NAME_PATTERN::matcher)
                .filter(Matcher::matches)
                .map(m -> new DeskRepresentation(m.group(MATCH_GROUP_DESK_ID), null, m.group(MATCH_GROUP_TENANT_ID), null))
                .collect(toMutableList());

        log.debug("getCurrentUserDesks result:{}", result);
        return result;
    }


    public static boolean isSuperAdmin() {
        List<String> roles = getCurrentUserRoles();

        log.trace("Token roles:{}", roles);
        return roles.contains(SUPER_ADMIN_ROLE_NAME);
    }


    public static boolean isFunctionalAdmin(String tenantId) {
        List<String> roles = getCurrentUserRoles();

        log.trace("Token roles:{}", roles);
        return roles.contains(StringSubstitutor.replace(TENANT_FUNCTIONAL_ADMIN_INTERNAL_NAME, Map.of(TENANT_PLACEHOLDER, tenantId)));
    }


    public static boolean currentUserIsTenantAdmin(@NotNull String tenantId) {
        Map<String, String> tenantIdMapping = Map.of(TENANT_PLACEHOLDER, tenantId);
        String tenantAdminRoleName = StringSubstitutor.replace(TENANT_ADMIN_INTERNAL_NAME, tenantIdMapping);

        return getCurrentUserRoles()
                .stream()
                .anyMatch(roleName -> StringUtils.equals(tenantAdminRoleName, roleName));
    }


    public static boolean currentUserIsTenantOrSuperAdmin(@NotNull String tenantId) {
        Map<String, String> tenantIdMapping = Map.of(TENANT_PLACEHOLDER, tenantId);
        String tenantAdminRoleName = StringSubstitutor.replace(TENANT_ADMIN_INTERNAL_NAME, tenantIdMapping);
        
        return getCurrentUserRoles()
                .stream()
                .anyMatch(roleName -> StringUtils.equals(tenantAdminRoleName, roleName) || StringUtils.equals(SUPER_ADMIN_ROLE_NAME, roleName));
    }


}
