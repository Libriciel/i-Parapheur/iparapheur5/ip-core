/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.utils;

import java.beans.PropertyEditorSupport;


/**
 * Credits: https://stackoverflow.com/a/32354774/9122113
 */
public class EnumLowercaseConverter<E extends Enum<E>> extends PropertyEditorSupport {

    private Class<E> enumClass;


    public EnumLowercaseConverter(Class<E> enumClass) {
        this.enumClass = enumClass;
    }


    @Override
    public void setAsText(final String text) throws IllegalArgumentException {
        setValue(E.valueOf(enumClass, text.toUpperCase()));
    }

}
