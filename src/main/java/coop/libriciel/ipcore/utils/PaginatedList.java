/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
package coop.libriciel.ipcore.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.function.Function;
import java.util.function.LongSupplier;

import static java.util.Arrays.stream;
import static java.util.Collections.EMPTY_LIST;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class PaginatedList<T> {

    public static final String API_DOC_PAGE = "Result page, starting at 0";
    public static final String API_DOC_PAGE_DEFAULT = "0";
    public static final String API_DOC_PAGE_SIZE = "Result chunk size\n Maximum authorized value is 1000\n(Note that every retrieved chunk's data may be roughly 0.5ko)";
    public static final String API_DOC_PAGE_SIZE_DEFAULT = "50";
    public static final String API_DOC_SORT_BY = "Sorting parameter";
    public static final String API_DOC_ASC = "Results sorted in ascending/descending order";
    public static final String API_DOC_ASC_DEFAULT = "true";

    public static final int MAX_PAGE_SIZE = 1000;
    public static final int MAX_BIG_PAGE_SIZE = 10 * 1000;


    private List<T> data;
    private long page;
    private long pageSize;
    private long total;


    @SuppressWarnings("unchecked")
    public static <T> PaginatedList<T> emptyPaginatedList() {
        return new PaginatedList<T>((List<T>) EMPTY_LIST, 0, 0, 0);
    }


    /**
     * Constructor with a lazy total supplier, that will be called only if necessary.
     *
     * @param data          The final list, should not be modified afterwards
     * @param page          Starting at 0
     * @param pageSize      Page chunk size
     * @param totalSupplier The lazy evaluator
     */
    public PaginatedList(@NotNull List<T> data, long page, long pageSize, @NotNull LongSupplier totalSupplier) {
        this.data = data;
        this.page = page;
        this.pageSize = pageSize;
        this.total = CollectionUtils.computeTotal(data, page, pageSize, totalSupplier);
    }


    public PaginatedList(@NotNull Page<T> result) {
        this.data = result.toList();
        this.page = result.getPageable().getPageNumber();
        this.pageSize = result.getPageable().getPageSize();
        this.total = result.getTotalElements();
    }


    public <U> PaginatedList(@NotNull PaginatedList<U> paginatedList, @NotNull Function<U, T> dataMapper) {
        this.page = paginatedList.getPage();
        this.pageSize = paginatedList.getPageSize();
        this.total = paginatedList.getTotal();
        this.data = paginatedList.getData()
                .stream()
                .map(dataMapper)
                .toList();
    }


    @SafeVarargs
    public static <T> PaginatedList<T> of(T... elements) {
        List<T> data = stream(elements).toList();
        return new PaginatedList<>(data, 0, data.size(), data::size);
    }


}
