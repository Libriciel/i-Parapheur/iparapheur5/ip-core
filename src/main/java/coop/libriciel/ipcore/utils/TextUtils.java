/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.utils;

import coop.libriciel.ipcore.model.database.MetadataType;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jooq.conf.Settings;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static coop.libriciel.ipcore.controller.admin.AdminMetadataController.MAX_RESTRICTED_VALUES_COUNT;
import static coop.libriciel.ipcore.controller.admin.AdminMetadataController.MAX_SERIALIZED_VALUE_LENGTH;
import static coop.libriciel.ipcore.model.externalsignature.ExternalSignatureParams.METADATA_EXTERNAL_SIGNATURE_TO_DOCKET_LABEL;
import static java.time.ZoneOffset.UTC;
import static java.time.format.FormatStyle.MEDIUM;
import static java.util.function.Function.identity;
import static java.util.regex.Pattern.CASE_INSENSITIVE;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.jooq.conf.RenderKeywordCase.UPPER;
import static org.jooq.conf.RenderNameCase.AS_IS;
import static org.jooq.conf.RenderQuotedNames.EXPLICIT_DEFAULT_UNQUOTED;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INSUFFICIENT_STORAGE;


@Log4j2
public class TextUtils {


    /**
     * List of forbidden chars:
     * - \r
     * - \n
     * - \b
     * - \u00A0 // non-breaking space
     */
    public static final String NO_FORBIDDEN_CHARACTERS_REGEXP = "^[^\\r\\n\b\u00A0]*$";

    /**
     * Matches any linebreak ( \\R -> \n, \r or more exotic forms), plus non breaking space and backspace.
     */
    public static final String MATCHES_FORBIDDEN_CHARACTERS_REGEXP = "\\R|[\b\u00A0]";
    private static final Pattern containsForbiddenCharPattern = Pattern.compile(MATCHES_FORBIDDEN_CHARACTERS_REGEXP);

    public static final String NONE_SERVICE = "none";

    public static final String MESSAGE_BUNDLE = "messages";
    public static final String INTERNAL_PREFIX = "i_Parapheur_internal_";
    public static final String INTERNAL_COMPUTED_PREFIX = INTERNAL_PREFIX + "computed_";
    public static final String RESERVED_PREFIX = "i_Parapheur_reserved_";
    public static final String ISO8601_DATE_FORMAT = "yyyy-MM-dd";
    public static final String ISO8601_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    public static final String SQL_LIKE_ANY_36_CHAR = "____________________________________";

    public static final String ID_REGEX = "[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}";
    public static final Pattern IS_UUID_PATTERN = Pattern.compile("^%s$".formatted(ID_REGEX));

    public static final String DATE_REGEX = "\\d{4}-\\d{2}-\\d{2}";

    public static final String MATCH_GROUP_LINK = "link";
    public static final String MATCH_GROUP_DESK_ID = "deskId";
    public static final String MATCH_GROUP_TENANT_ID = "tenantId";
    public static final String MATCH_GROUP_TYPE_ID = "typeId";
    public static final String MATCH_GROUP_SUBTYPE_ID = "subtypeId";
    public static final String MATCH_GROUP_USER_ID = "userId";
    public static final String MATCH_GROUP_DATE_START = "start";
    public static final String MATCH_GROUP_DATE_END = "end";

    /**
     * This custom link naming convention allows us to recognize links in our template's arguments.
     * Templates are already HTML, so we don't want to allow any direct use of HTML links.
     * We don't want to follow a regular markdown-like language either, or users will expect more of it...
     * And the wrapping should be specific, to avoid collisions, so... JavaDoc-style will do.
     */
    public static final String TEMPLATE_LINK_REGEX = "\\{@link (?<%s>.*?)}".formatted(MATCH_GROUP_LINK);
    public static final String TEMPLATE_LINK_REPLACEMENT = "<a href='${%s}'>${%s}</a>".formatted(MATCH_GROUP_LINK, MATCH_GROUP_LINK);

    public static final String DESK_INTERNAL_NAME_REGEX = "tenant_(?<%s>%s)_desk_(?<%s>%s)"
            .formatted(MATCH_GROUP_TENANT_ID, ID_REGEX, MATCH_GROUP_DESK_ID, ID_REGEX);
    public static final String TENANT_ID_NAME_REGEX = "tenant_(?<%s>%s)"
            .formatted(MATCH_GROUP_TENANT_ID, ID_REGEX);
    public static final String TENANT_ID_ADMIN_NAME_REGEX = "tenant_(?<%s>%s)_admin"
            .formatted(MATCH_GROUP_TENANT_ID, ID_REGEX);
    public static final String TENANT_ID_FUNCTIONAL_ADMIN_NAME_REGEX = "tenant_(?<%s>%s)_functional_admin"
            .formatted(MATCH_GROUP_TENANT_ID, ID_REGEX);
    public static final String DESK_OWNER_INTERNAL_NAME_REGEX = "%s_owner"
            .formatted(DESK_INTERNAL_NAME_REGEX);
    public static final String USER_POLICY_INTERNAL_NAME_REGEX = "user_(?<%s>%s)"
            .formatted(MATCH_GROUP_USER_ID, ID_REGEX);
    public static final String DESK_ASSOCIATION_INTERNAL_NAME_REGEX = "%s_association"
            .formatted(DESK_INTERNAL_NAME_REGEX);
    public static final String TIME_PERMISSION_INTERNAL_NAME_REGEX = "time_(?<%s>%s|null)_to_(?<%s>%s|null)"
            .formatted(MATCH_GROUP_DATE_START, DATE_REGEX, MATCH_GROUP_DATE_END, DATE_REGEX);
    public static final String PARTIAL_TYPE_SCOPE_INTERNAL_REGEX = "urn:ipcore:scopes:delegation:tenant:(?<%s>.*?):type:(?<%s>.*)"
            .formatted(MATCH_GROUP_TENANT_ID, MATCH_GROUP_TYPE_ID);
    public static final String PARTIAL_SUBTYPE_SCOPE_INTERNAL_REGEX = "urn:ipcore:scopes:delegation:tenant:(?<%s>.*?):subtype:(?<%s>.*)"
            .formatted(MATCH_GROUP_TENANT_ID, MATCH_GROUP_SUBTYPE_ID);
    public static final String DELEGATION_CALENDAR_POLICY_INTERNAL_NAME_REGEX = "tenant_(?<%s>%s)_desk_.*?_owner_.*?_target_calendar"
            .formatted(MATCH_GROUP_TENANT_ID, ID_REGEX);
    public static final String PARTIAL_TYPE_DELEGATION_POLICY_INTERNAL_NAME_REGEX = "tenant_(?<%s>%s)_desk_.*?_owner_.*?_type_(?<%s>%s)(?:_target_calendar)?"
            .formatted(MATCH_GROUP_TENANT_ID, ID_REGEX, MATCH_GROUP_TYPE_ID, ID_REGEX);
    public static final String PARTIAL_SUBTYPE_DELEGATION_POLICY_INTERNAL_REGEX = "tenant_(?<%s>%s)_desk_.*?_owner_.*?_subtype_(?<%s>%s)(?:_target_calendar)?"
            .formatted(MATCH_GROUP_TENANT_ID, ID_REGEX, MATCH_GROUP_SUBTYPE_ID, ID_REGEX);

    public static final Pattern TEMPLATE_LINK_PATTERN = Pattern.compile(TEMPLATE_LINK_REGEX);
    public static final Pattern DESK_INTERNAL_NAME_PATTERN = Pattern.compile(DESK_INTERNAL_NAME_REGEX);
    public static final Pattern TENANT_ID_NAME_PATTERN = Pattern.compile(TENANT_ID_NAME_REGEX);
    public static final Pattern TENANT_ID_ADMIN_NAME_PATTERN = Pattern.compile(TENANT_ID_ADMIN_NAME_REGEX);
    public static final Pattern TENANT_ID_FUNCTIONAL_ADMIN_NAME_PATTERN = Pattern.compile(TENANT_ID_FUNCTIONAL_ADMIN_NAME_REGEX);
    public static final Pattern DESK_OWNER_INTERNAL_NAME_PATTERN = Pattern.compile(DESK_OWNER_INTERNAL_NAME_REGEX);
    public static final Pattern USER_POLICY_INTERNAL_NAME_PATTERN = Pattern.compile(USER_POLICY_INTERNAL_NAME_REGEX);
    public static final Pattern DESK_ASSOCIATION_INTERNAL_NAME_PATTERN = Pattern.compile(DESK_ASSOCIATION_INTERNAL_NAME_REGEX);
    public static final Pattern TIME_PERMISSION_INTERNAL_NAME_PATTERN = Pattern.compile(TIME_PERMISSION_INTERNAL_NAME_REGEX);
    public static final Pattern PARTIAL_TYPE_SCOPE_INTERNAL_NAME_PATTERN = Pattern.compile(PARTIAL_TYPE_SCOPE_INTERNAL_REGEX);
    public static final Pattern PARTIAL_SUBTYPE_SCOPE_INTERNAL_NAME_PATTERN = Pattern.compile(PARTIAL_SUBTYPE_SCOPE_INTERNAL_REGEX);
    public static final Pattern DELEGATION_CALENDAR_POLICY_INTERNAL_NAME_PATTERN = Pattern.compile(DELEGATION_CALENDAR_POLICY_INTERNAL_NAME_REGEX);
    public static final Pattern PARTIAL_TYPE_DELEGATION_POLICY_INTERNAL_NAME_PATTERN = Pattern.compile(PARTIAL_TYPE_DELEGATION_POLICY_INTERNAL_NAME_REGEX);
    public static final Pattern PARTIAL_SUBTYPE_DELEGATION_POLICY_INTERNAL_NAME_PATTERN = Pattern.compile(PARTIAL_SUBTYPE_DELEGATION_POLICY_INTERNAL_REGEX);

    public static final int UUID_STRING_SIZE = 36;

    public static final String SONAR_CODE_BLOCKS = "squid:S1199";

    private static final Pattern KEY_PATTERN = Pattern.compile("[\\w_-]+");
    private static final Pattern INTEGER_PATTERN = Pattern.compile("-?\\d+");
    private static final Pattern URL_RFC3986_PATTERN = Pattern.compile(
            "^[a-z]*://(?:\\S+(?::\\S*)?@)?(?:(?!10(?:\\.\\d{1,3}){3})(?!127(?:\\.\\d{1,3}){3})(?!169\\.254(?:\\.\\d{1,3}){2})("
                    + "?!192\\.168(?:\\.\\d{1,3}){2})(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})(?:[1-9]\\d?|1\\d\\d|2[01]\\d|"
                    + "22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4])|(?:[a-z\\u00a1-\\"
                    + "uffff\\d]+-?)*[a-z\\u00a1-\\uffff\\d]+(?:\\.(?:[a-z\\u00a1-\\uffff\\d]+-?)*[a-z\\u00a1-\\uffff\\d]+)*\\.[a-z\\"
                    + "u00a1-\\uffff]{2,})(?::\\d{2,5})?(?:/\\S*)?$", CASE_INSENSITIVE);

    public static Settings PSQL_RENDER_SETTINGS = new Settings()
            .withRenderQuotedNames(EXPLICIT_DEFAULT_UNQUOTED)
            .withRenderKeywordCase(UPPER)
            .withRenderFormatted(true)
            .withRenderNameCase(AS_IS);


    /**
     * This seems useless,
     * but this class is actually instantiated and fed to the FreeTemplate models
     */
    @SuppressWarnings("java:S1118")
    public TextUtils() {
        // Nothing to do here
    }


    public static @NotNull Optional<String> getAsSqlLikeWrapped(@Nullable String searchTerm) {
        return Optional.ofNullable(searchTerm)
                .filter(StringUtils::isNotEmpty)
                .map(search -> String.format("%%%s%%", search));
    }


    public static String firstNotEmpty(String... strings) {
        for (String string : strings) {
            if (StringUtils.isNotEmpty(string)) {
                return string;
            }
        }
        return ObjectUtils.firstNonNull(strings);
    }


    public static @Nullable Date deserializeDate(@Nullable String string, @NotNull String format) {
        return deserializeDate(string, new SimpleDateFormat(format));
    }


    public static @Nullable Date deserializeDate(@Nullable String string, @NotNull SimpleDateFormat simpleDateFormat) {

        if (isEmpty(string)) {
            return null;
        }

        try {
            return simpleDateFormat.parse(string);
        } catch (ParseException e) {
            log.warn("Cannot deserialize date: {}. Skipping...", string);
            log.debug("Not a valid date format.", e);
            return null;
        }
    }


    public static @NotNull String serializeDate(@NotNull Date date, @NotNull String format) {
        return new SimpleDateFormat(format).format(date);
    }


    public static @NotNull String prettyPrintDateTime(@NotNull Date date) {
        return DateTimeFormatter
                .ofLocalizedDateTime(MEDIUM, MEDIUM)
                .format(date.toInstant().atOffset(UTC).toLocalDateTime());
    }


    public static boolean isValidKey(@Nullable String url) {
        return Optional.ofNullable(url)
                .map(KEY_PATTERN::matcher)
                .map(Matcher::matches)
                .orElse(false);
    }


    public static boolean isUrlRfc3986Valid(@Nullable String url) {
        return Optional.ofNullable(url)
                .map(URL_RFC3986_PATTERN::matcher)
                .map(Matcher::matches)
                .orElse(false);
    }


    public static boolean isInteger(@Nullable String number) {
        return Optional.ofNullable(number)
                .map(INTEGER_PATTERN::matcher)
                .map(Matcher::matches)
                .orElse(false);
    }


    public static <T> boolean hasAttributeChanged(@NotNull T oldObject, @NotNull T newObject, @NotNull Function<? super T, String> mapper) {

        String oldValue = mapper.apply(oldObject);
        String newValue = mapper.apply(newObject);

        return !StringUtils.equals(oldValue, newValue);
    }


    public static <T> boolean hasAttributeChangedBoolean(@NotNull T oldObject, @NotNull T newObject, @NotNull Function<? super T, Boolean> mapper) {

        boolean oldValue = mapper.apply(oldObject);
        boolean newValue = mapper.apply(newObject);

        return oldValue != newValue;
    }


    public static <T> @Nullable String getNewAttributeIfChanged(@NotNull T oldObject, @NotNull T newObject, @NotNull Function<? super T, String> mapper) {

        String oldValue = mapper.apply(oldObject);
        String newValue = mapper.apply(newObject);

        return StringUtils.equals(oldValue, newValue) ? null : newValue;
    }


    public static <T> @Nullable String getNewAttributeIfChangedBoolean(@NotNull T oldObject,
                                                                       @NotNull T newObject,
                                                                       @NotNull Function<? super T, Boolean> mapper) {

        boolean oldValue = mapper.apply(oldObject);
        boolean newValue = mapper.apply(newObject);

        return oldValue == newValue ? null : BooleanUtils.toStringTrueFalse(newValue);
    }


    public static @Nullable String escapeMessageFormatSpecialChars(@Nullable String message) {

        if (message == null) {
            return null;
        }

        return message.replaceAll("'", "''");
    }


    public static void checkValues(@NotNull MetadataType type, @Nullable List<String> values) {

        // Default checks

        if (CollectionUtils.isEmpty(values)) {
            return;
        }

        if (values.stream().anyMatch(Objects::isNull)) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.metadata_incorrect_null_value");
        }

        if (values.stream().anyMatch(StringUtils::isEmpty)) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.metadata_incorrect_empty_value");
        }

        if (CollectionUtils.size(values) >= MAX_RESTRICTED_VALUES_COUNT) {
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE, "message.restricted_values_count_reach_the_limit_of_m", MAX_RESTRICTED_VALUES_COUNT);
        }

        if (values.stream()
                .collect(groupingBy(identity(), counting()))
                .values()
                .stream()
                .anyMatch(n -> n > 1)) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.metadata_restricted_values_cannot_contain_duplicates");
        }

        if (values.stream().anyMatch(v -> v.length() > MAX_SERIALIZED_VALUE_LENGTH)) {
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE,
                    "message.metadata_restricted_values_are_limited_to_n_chars", MAX_RESTRICTED_VALUES_COUNT);
        }

        // Type-specific checks

        switch (type) {
            case DATE -> values.stream()
                    .filter(v -> deserializeDate(v, ISO8601_DATE_FORMAT) == null)
                    .findFirst()
                    .ifPresent(v -> {throw new LocalizedStatusException(BAD_REQUEST, "message.metadata_incorrect_s_value_for_iso8601_date", v);});
            case INTEGER -> values.stream()
                    .filter(v -> !isInteger(v))
                    .findFirst()
                    .ifPresent(v -> {throw new LocalizedStatusException(BAD_REQUEST, "message.metadata_incorrect_s_value_for_integer", v);});
            case FLOAT -> values.stream()
                    .filter(v -> !NumberUtils.isParsable(v))
                    .findFirst()
                    .ifPresent(v -> {throw new LocalizedStatusException(BAD_REQUEST, "message.metadata_incorrect_s_value_for_float", v);});
            case BOOLEAN -> throw new LocalizedStatusException(BAD_REQUEST, "message.metadata_restricted_values_are_not_applicables_to_boolean_type");
            case URL -> values.stream()
                    .filter(v -> !isUrlRfc3986Valid(v))
                    .findFirst()
                    .ifPresent(v -> {throw new LocalizedStatusException(BAD_REQUEST, "message.metadata_incorrect_s_value_for_rfc3986_url", v);});
        }
    }


    public static @NotNull String sanitizeTemplateMessage(@Nullable String message) {

        if (StringUtils.isEmpty(message)) {
            return EMPTY;
        }

        String result = StringEscapeUtils.escapeHtml4(message);
        result = result.replaceAll("\n", "<br>");
        result = TEMPLATE_LINK_PATTERN.matcher(result).replaceAll(TEMPLATE_LINK_REPLACEMENT);

        return result;
    }


    public static @NotNull String sanitizeMetadataKey(@NotNull ResourceBundle bundle, @NotNull String key) {

        if (METADATA_EXTERNAL_SIGNATURE_TO_DOCKET_LABEL.containsKey(key)) {
            String bundleKey = METADATA_EXTERNAL_SIGNATURE_TO_DOCKET_LABEL.get(key);
            return bundle.getString(bundleKey);
        }

        return key;
    }


    /**
     * Parse whatever floating number, that use a comma or a dot as a decimal separator.
     * <p>
     * Note : Using the native NumberFormat methods seems to be a bad idea:
     * There is a tricky ambiguity between decimal and grouping separators, between most locales.
     *
     * @param string with no grouping char, except spaces
     * @return a parsed double
     * @throws NumberFormatException if nothing works
     */
    public static @NotNull Double parseLocalizedDouble(@Nullable String string) throws NumberFormatException {

        if (StringUtils.isEmpty(string)) {
            throw new NumberFormatException("For input string: \"%s\"".formatted(string));
        }

        string = string.replaceAll(" ", "");
        string = string.replaceAll(",", ".");

        return Double.valueOf(string);
    }


    public static boolean containsForbiddenChar(String name) {
        Pattern noForbiddenCharPattern = Pattern.compile(NO_FORBIDDEN_CHARACTERS_REGEXP);
        Matcher m = noForbiddenCharPattern.matcher(name);
        return !m.matches();
    }


    public static boolean isUuid(@Nullable String string) {
       return Optional.ofNullable(string)
                .map(IS_UUID_PATTERN::matcher)
                .map(Matcher::matches)
                .orElse(false);
    }


    /**
     * Substitute any forbidden char (linebreaks and such) found in the passed String by a space character
     *
     * @return A new String not containing any forbidden character
     */
    public static String substituteForbiddenChar(String name) {
        Matcher m = containsForbiddenCharPattern.matcher(name);
        return m.replaceAll(" ");
    }


}
