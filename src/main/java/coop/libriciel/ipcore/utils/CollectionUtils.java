/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.utils;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.MapUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.modelmapper.Converter;
import org.springframework.data.domain.*;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.LongSupplier;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Collections.emptyList;


@Log4j2
public class CollectionUtils {


    public static <T> @Nullable T popValue(@Nullable Map<String, T> map, @NotNull String key) {

        if (map == null) {
            return null;
        }

        T result = map.get(key);
        map.remove(key);
        return result;
    }


    public static <T> void safeAddValue(@NotNull Map<String, List<T>> map, @NotNull String key, @NotNull T value) {
        map.computeIfAbsent(key, k -> new ArrayList<>());
        map.get(key).add(value);
    }


    public static @NotNull Map<String, String> parseMetadata(@Nullable String[][] data) {

        Map<String, String> metadata = new HashMap<>();

        if (data == null) {
            return metadata;
        }

        Arrays.stream(data)
                .filter(entry -> entry.length == 2)
                .forEach(entry -> metadata.put(entry[0], entry[1]));

        return metadata;
    }


    public static <T> @Nullable T optValue(@NotNull org.jooq.Record record, @NotNull String key, Class<T> clazz) {
        return Optional.of(record)
                .filter(r -> r.field(key) != null)
                .map(r -> r.get(key, clazz))
                .orElse(null);
    }


    public static <T> Predicate<T> distinctByProperty(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }


    /**
     * Managing computable cases, avoiding unnecessary evaluations
     *
     * @param data          The final list, should not be modified afterwards
     * @param page          Starting at 0
     * @param pageSize      Page chunk size
     * @param totalSupplier The lazy evaluator
     * @return
     */
    public static long computeTotal(@NotNull List<?> data, long page, long pageSize, @NotNull LongSupplier totalSupplier) {

        // No data, nothing to compute
        if (data.isEmpty() && (page == 0)) {
            return 0;
        }

        // If the current page is not half-set, we can compute the total
        if (!data.isEmpty() && (data.size() != pageSize)) {
            return page * pageSize + data.size();
        }

        // Default case, we need to compute the total
        return totalSupplier.getAsLong();
    }


    /**
     * The same than the regular {@link Collection#stream}, with a null-safe behaviour.
     *
     * @param collection a nullable source
     * @param <T>
     * @return A (maybe empty) stream
     */
    public static <T> Stream<T> stream(@Nullable Collection<T> collection) {
        return Optional.ofNullable(collection)
                .orElse(emptyList())
                .stream();
    }


    public static <T> Collector<T, ?, ArrayList<T>> toMutableList() {
        return Collectors.toCollection(ArrayList::new);
    }


    public static <T> Collector<T, ?, HashSet<T>> toMutableSet() {
        return Collectors.toCollection(HashSet::new);
    }


    public static Converter<Map<String, Object>, Integer> createMapIntegerExtractor(@NotNull String propertyKey, @Nullable Integer defaultValue) {
        return context -> MapUtils.getInteger(context.getSource(), propertyKey, defaultValue);
    }


    public static Converter<Map<String, Object>, Boolean> createMapBooleanExtractor(@NotNull String propertyKey) {
        return context -> MapUtils.getBoolean(context.getSource(), propertyKey);
    }


    public static Converter<Map<String, String>, String> createMapStringExtractor(@NotNull String propertyKey) {
        return context -> MapUtils.getString(context.getSource(), propertyKey);
    }


    /**
     * Checks if the given array contains a sequence, without leaps.
     *
     * @param numbers may be unordered, the null entries will be ignored.
     * @return true on sequential (and on empty lists)
     */
    public static boolean isSequential(@NotNull Collection<Long> numbers) {

        if (org.apache.commons.collections4.CollectionUtils.isEmpty(numbers)) {
            return true;
        }

        List<Long> sortedList = numbers.stream()
                .filter(Objects::nonNull)
                .distinct()
                .sorted()
                .toList();

        int size = sortedList.size();
        long firstValue = sortedList.get(0);
        long lastValue = sortedList.get(size - 1);
        long range = lastValue - firstValue;
        return range == (size - 1);
    }


    /**
     * A method that call batches, and returns a stream of elements.
     * The "n+1 page" retrieval will be called once every elements of the "n page" had been consumed by the Stream.
     *
     * @param paginatedRequestSupplier a regular content supplier
     * @param pageSize the batch size
     * @return a regular, non-parallel, ordered Stream
     * @param <T>
     */
    public static <T> Stream<T> streamWithPaginatedRequest(@NotNull Function<Pageable, Page<T>> paginatedRequestSupplier, int pageSize) {
        return Stream.iterate(
                        paginatedRequestSupplier.apply(PageRequest.of(0, pageSize)),
                        Slice::hasContent,
                        page -> page.hasNext()
                                ? paginatedRequestSupplier.apply(page.nextPageable())
                                : new PageImpl<T>(emptyList(), page.nextPageable(), page.getTotalElements())
                )
                .map(Slice::getContent)
                .flatMap(Collection::stream);
    }


}
