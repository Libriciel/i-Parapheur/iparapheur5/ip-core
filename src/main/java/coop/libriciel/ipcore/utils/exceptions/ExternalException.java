/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.utils.exceptions;

import java.io.Serial;


/**
 * Exception used to retry error from outer services.
 */
public class ExternalException extends RuntimeException {


    public static final int RETRY_ATTEMPTS = 10;
    public static final long RETRY_DELAY = 3600000L;


    @Serial private static final long serialVersionUID = 6303670141339060549L;


    public ExternalException(String message) {
        super(message);
    }


}
