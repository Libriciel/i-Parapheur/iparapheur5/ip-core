/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.utils;

import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.crypto.CertificateInformations;
import coop.libriciel.ipcore.model.crypto.PdfSignaturePosition;
import coop.libriciel.ipcore.model.crypto.SealCertificate;
import coop.libriciel.ipcore.model.crypto.SignatureFormat;
import coop.libriciel.ipcore.model.database.TemplateType;
import coop.libriciel.ipcore.model.database.Type;
import coop.libriciel.ipcore.model.database.userPreferences.UserPreferences;
import coop.libriciel.ipcore.model.externalsignature.SignRequestMember;
import coop.libriciel.ipcore.model.pdfstamp.SignaturePlacement;
import coop.libriciel.ipcore.model.workflow.Action;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.services.redis.RedisMessageListenerService;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.x500.AttributeTypeAndValue;
import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.IETFUtils;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static coop.libriciel.ipcore.model.crypto.SignatureFormat.PES_V2;
import static coop.libriciel.ipcore.model.database.TemplateType.SEAL_MEDIUM;
import static coop.libriciel.ipcore.model.database.TemplateType.SIGNATURE_MEDIUM;
import static coop.libriciel.ipcore.model.externalsignature.ExternalSignatureParams.*;
import static coop.libriciel.ipcore.model.externalsignature.ExternalSignatureParams.METADATA_EXTERNAL_SIGNATURE_KEY_LASTNAME;
import static coop.libriciel.ipcore.model.pdfstamp.Origin.BOTTOM_LEFT;
import static coop.libriciel.ipcore.model.pdfstamp.Origin.CENTER;
import static coop.libriciel.ipcore.model.workflow.Action.*;
import static coop.libriciel.ipcore.model.workflow.State.BYPASSED;
import static coop.libriciel.ipcore.model.workflow.State.VALIDATED;
import static coop.libriciel.ipcore.utils.TextUtils.INTERNAL_PREFIX;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.List.of;
import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.bouncycastle.asn1.x500.style.BCStyle.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE;


@Log4j2
public class CryptoUtils {

    @SuppressWarnings("SpellCheckingInspection")
    public static final String EXAMPLE_PUBLIC_CERTIFICATE_BASE64 =
            """
            MIIG+jCCBOKgAwIBAgIUStb/+TKiIUj2TDZgq+uVGtUbZMkwDQYJKoZIhvcNAQELBQAwga8xCzAJBgNVBAYTAkZSMRAwDgYDVQQIDAdIZXJhdWx0MR0wGwYDVQQKDBRBc3NvY2lhdGl\
            vbiBBRFVMTEFDVDEkMCIGA1UECwwbQUNfQURVTExBQ1RfVXRpbGlzYXRldXJzX0czMSQwIgYDVQQDDBtBQ19BRFVMTEFDVF9VdGlsaXNhdGV1cnNfRzMxIzAhBgkqhkiG9w0BCQEWFH\
            N5c3RlbWVAYWR1bGxhY3Qub3JnMB4XDTIxMDcxNjEwMDQ1N1oXDTIzMDcxNjEwMDQ1N1owgZUxCzAJBgNVBAYTAkZSMRAwDgYDVQQIDAdIZXJhdWx0MRQwEgYDVQQHDAtNb250cGVsb\
            GllcjESMBAGA1UECgwJTGlicmljaWVsMRMwEQYDVQQLDApQcm9kdWN0aW9uMRIwEAYDVQQDDAlUZXN0X1RpbG8xITAfBgkqhkiG9w0BCQEWEnRpbG8udGVzdEBkb20udGVzdDCCASIw\
            DQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBANWdP3N5URDGUCf4qdx1krdDVHfs/KHN/7mGa/1C+pT1x5i8qX0ZF/DYI/zcSwMFhqJkr/G0sqLELOPgVjXgXKE/9YcsppwGcq4PwVI\
            Lr8LfiblmufREBJQaNKbUS+2XbMfFt+gYSa3twrKigkq1n4wo7I0S43nhioZPwBBfxdJUTWr4/Uc6VaOasWDrKqkznOtHUE5sD2edHqrapOfrIOSa1ccqdhgybPZQwCanA+Lk848Re/\
            KA5gxcx+X/3/8Qqww2wew27JdpIib2MJ3Ixq8ZlqYDaAmTpDhEiiWklJnEgNUlAZOQKz2Qmhs8QqAO+46R2JVf8rGSzkgCg6UK4ucCAwEAAaOCAiQwggIgMAkGA1UdEwQCMAAwQgYJY\
            IZIAYb4QgENBDUWM0NlcnRpZmljYXQgZ2VuZXJlIHBhciBsJ0FDX0FEVUxMQUNUX1VUSUxJU0FURVVSU19HMzAdBgNVHQ4EFgQU2sGKHc7e9oKl+i0yPq/88XEeW3cwgfUGA1UdIwSB\
            7TCB6oAUmbt/AgKYRZQErGAkE9Mq+9qjX0ChgbukgbgwgbUxCzAJBgNVBAYTAkZSMRAwDgYDVQQIDAdIZXJhdWx0MRQwEgYDVQQHDAtNb250cGVsbGllcjEdMBsGA1UECgwUQXNzb2N\
            pYXRpb24gQURVTExBQ1QxHDAaBgNVBAsME0FDX0FEVUxMQUNUX1JPT1RfRzMxHDAaBgNVBAMME0FDX0FEVUxMQUNUX1JPT1RfRzMxIzAhBgkqhkiG9w0BCQEWFHN5c3RlbWVAYWR1bG\
            xhY3Qub3JnghRvKXgHN8D4NbAw8kcWCR1fEjtQajAdBgNVHREEFjAUgRJ0aWxvLnRlc3RAZG9tLnRlc3QwHwYDVR0SBBgwFoEUc3lzdGVtZUBhZHVsbGFjdC5vcmcwOgYJYIZIAYb4Q\
            gEEBC0WK2h0dHA6Ly9jcmwuYWR1bGxhY3Qub3JnL0NBX1VTRVJTX0NSTF9nMy5wZW0wPAYDVR0fBDUwMzAxoC+gLYYraHR0cDovL2NybC5hZHVsbGFjdC5vcmcvQUNfVVNFUlNfQ1JM\
            X2czLnBlbTANBgkqhkiG9w0BAQsFAAOCAgEAcLKmNmkqXRMzTrMqLPB8BpGMWM2kNPA8OjIQyLhFD+WUXYJp+/4VfHPzJrv/cfeAC/9WPy9G1UV70PMIT67s6wsLdHCegxLOd5NUwJ6\
            89cBTtcCLvU6GUNubq+MDRHBTPzxprMPa1LzA4giPLIZZmVFslBYWtUKDgipn938+nS7X5buvVS1Razs2xwgsgRMuffGTbrtmp1XrKEnjhHOWzjTdSp43alK1s2i/7YUNkIUawZbPWK\
            QAYKxqlm5siEick5T5LhgIx+2BnrlWfrs0GPxTkplbolLFtMZucxAkazyaq29gLR6JtvJZCVIxWj2vXkWo9up7rW1d0w9pvuLO0zxmbYQZUmXmLn4lFZTYzF3esIxhRvKRKjmkpyOT/\
            7gXi6lrs/qVWg5MzGHOdg4uhKtnsYh6aD3PVjcxxPnltR4r27RQbj6Hu944BarXEH7veKFuvpjCUnUZuCqqptUA/1p99OYuhXuFwR7/jxHRzedlIPDJTmsDYv14bL4OApi0nlooldCZ\
            bprmw8+T0Z/n5qdu9neqeDvyp7WhRX+uQuIjyW5IP6RqHusuCOe3xJLD1XSDrZWhiF/GqvhEfogil3gx3U1RLfNeShiNdc3ZRnWHj+eC0UbOB8OVCkMMm56VrKjwY9vZWwl3k89AqZc\
            N0PXUEpXOExpS3XMW01wn1+U=\
            """;

    public static final String SIGNATURE_PLACEHOLDER_USER_NAME = INTERNAL_PREFIX + "user_name";
    public static final String SIGNATURE_PLACEHOLDER_DESK_NAME = INTERNAL_PREFIX + "desk_name";
    public static final String SIGNATURE_PLACEHOLDER_DESK_SHORT_NAME = INTERNAL_PREFIX + "desk_short_name";
    public static final String SIGNATURE_PLACEHOLDER_TENANT_NAME = INTERNAL_PREFIX + "tenant_name";
    public static final String SIGNATURE_PLACEHOLDER_COUNTRY_NAME = INTERNAL_PREFIX + "country";
    public static final String SIGNATURE_PLACEHOLDER_CITY_NAME = INTERNAL_PREFIX + "city";
    public static final String SIGNATURE_PLACEHOLDER_ZIPCODE_NAME = INTERNAL_PREFIX + "zipcode";
    public static final String SIGNATURE_PLACEHOLDER_SIGNATURE_DATE = INTERNAL_PREFIX + "signature_date";
    public static final String SIGNATURE_PLACEHOLDER_SIGNATURE_TIME = INTERNAL_PREFIX + "signature_time";
    public static final String SIGNATURE_PLACEHOLDER_DELEGATION_FROM = INTERNAL_PREFIX + "delegation_from";
    public static final String SIGNATURE_PLACEHOLDER_CUSTOM_SIGNATURE_FIELD = INTERNAL_PREFIX + "custom_signature_field";
    public static final String SIGNATURE_PLACEHOLDER_SIGNATURE_IMAGE_BASE64 = INTERNAL_PREFIX + "signature_image_base64";

    private static final String CERTIFICATE_TYPE_X509 = "X.509";

    /**
     * Every regexp have the same structure here: ^(?<key>i_Parapheur_reserved_ext_sig_xxx)_?(?<tag_index>\d*)$
     * - ^                                        : The start of the string
     * - (?<key>i_Parapheur_reserved_ext_sig_xxx) : then, a named capturing group containing the metadata key
     * - _?                                       : then, an optional underscore
     * - (?<index>\d*)                            : then, a named capturing group of any number of digits (may be 0)
     * - $                                        : then, the end of the string
     * This should capture every tag, numbered or not, with or without an underscore.
     * Weird case: it can capture a non-numbered metadata suffixed with a single underscore, but why the hell not.
     */
    private static final String METADATA_EXTERNAL_SIGNATURE_KEY_INDEX_GROUP = "index";
    private static final String METADATA_EXTERNAL_SIGNATURE_KEY_KEY_GROUP = "key";
    private static final String METADATA_EXTERNAL_SIGNATURE_KEY_REGEXP = "(?<%s>%%s)_?(?<%s>\\d*)"
            .formatted(METADATA_EXTERNAL_SIGNATURE_KEY_KEY_GROUP, METADATA_EXTERNAL_SIGNATURE_KEY_INDEX_GROUP);

    private static final String METADATA_EXTERNAL_SIGNATURE_KEY_MAIL_REGEXP = METADATA_EXTERNAL_SIGNATURE_KEY_REGEXP.formatted(
            METADATA_EXTERNAL_SIGNATURE_KEY_MAIL);
    private static final String METADATA_EXTERNAL_SIGNATURE_KEY_PHONE_REGEXP = METADATA_EXTERNAL_SIGNATURE_KEY_REGEXP.formatted(
            METADATA_EXTERNAL_SIGNATURE_KEY_PHONE);
    private static final String METADATA_EXTERNAL_SIGNATURE_KEY_FIRSTNAME_REGEXP = METADATA_EXTERNAL_SIGNATURE_KEY_REGEXP.formatted(
            METADATA_EXTERNAL_SIGNATURE_KEY_FIRSTNAME);
    private static final String METADATA_EXTERNAL_SIGNATURE_KEY_LASTNAME_REGEXP = METADATA_EXTERNAL_SIGNATURE_KEY_REGEXP.formatted(
            METADATA_EXTERNAL_SIGNATURE_KEY_LASTNAME);

    private static final Pattern METADATA_EXTERNAL_SIGNATURE_KEY_MAIL_PATTERN = Pattern.compile(METADATA_EXTERNAL_SIGNATURE_KEY_MAIL_REGEXP);
    private static final Pattern METADATA_EXTERNAL_SIGNATURE_KEY_PHONE_PATTERN = Pattern.compile(METADATA_EXTERNAL_SIGNATURE_KEY_PHONE_REGEXP);
    private static final Pattern METADATA_EXTERNAL_SIGNATURE_KEY_FIRSTNAME_PATTERN = Pattern.compile(METADATA_EXTERNAL_SIGNATURE_KEY_FIRSTNAME_REGEXP);
    private static final Pattern METADATA_EXTERNAL_SIGNATURE_KEY_LASTNAME_PATTERN = Pattern.compile(METADATA_EXTERNAL_SIGNATURE_KEY_LASTNAME_REGEXP);


    /**
     * This seems useless,
     * but this class is actually instantiated and fed to the FreeTemplate models
     */
    @SuppressWarnings("java:S1118")
    public CryptoUtils() {
        // Nothing to do here
    }


    public static @NotNull PrivateKey getPrivateKey(@NotNull SealCertificate sealCertificate) {
        try {
            byte[] privateKeyBytes = Base64.getDecoder().decode(sealCertificate.getPrivateKeyBase64());
            KeyFactory kf = KeyFactory.getInstance("RSA");
            return kf.generatePrivate(new PKCS8EncodedKeySpec(privateKeyBytes));
        } catch (GeneralSecurityException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.could_not_read_certificate_content");
        }
    }


    public static @Nullable String sha256Sign(byte[] bytesToSign, @NotNull PrivateKey privateKey) {

        try {
            Signature privateSignature = Signature.getInstance("SHA256withRSA");
            privateSignature.initSign(privateKey);
            privateSignature.update(bytesToSign);

            byte[] signature = privateSignature.sign();
            return Base64.getEncoder().encodeToString(signature);

        } catch (GeneralSecurityException e) {
            log.error("Signature failed", e);
            return null;
        }
    }


    private static @Nullable PdfSignaturePosition getHashtagPosition(@NotNull Folder folder,
                                                                     @NotNull Document document,
                                                                     @NotNull Action action,
                                                                     @NotNull TemplateType defaultTemplateType) {
        List<Action> signatureTagsActions = of(SIGNATURE, EXTERNAL_SIGNATURE);

        final List<Action> countedActions = signatureTagsActions.contains(action)
                                            ? signatureTagsActions
                                            : of(action);

        long index = folder.getStepList().stream()
                .filter(s -> countedActions.contains(s.getAction()))
                .filter(s -> s.getState() == VALIDATED || s.getState() == BYPASSED)
                .count() + 1;

        log.debug("actionIndex : {}", action);

        Map<Integer, PdfSignaturePosition> hashtagMap = ofNullable(
                switch (action) {
                    case SIGNATURE, EXTERNAL_SIGNATURE -> document.getSignatureTags();
                    case SEAL -> document.getSealTags();
                    default -> emptyMap();
                })
                .map(m -> (Map<Integer, PdfSignaturePosition>) m)
                .orElse(emptyMap());

        PdfSignaturePosition result = ofNullable(hashtagMap.get((int) index))
                .orElseGet(() -> hashtagMap.get(0));

        ofNullable(result).ifPresent(r -> {
            r.setOrigin(CENTER);
            r.setTemplateType(defaultTemplateType);
        });

        log.debug("getHashtagPosition action:{} index:{} result:{}", action, index, result);
        return result;
    }


    public static PdfSignaturePosition getPosition(@NotNull Folder folder,
                                                   @NotNull Document document,
                                                   @NotNull UserPreferences userPreferences,
                                                   @NotNull Action action) {
        TemplateType defaultTemplateType = action == SEAL
                                           ? ofNullable(userPreferences.getDefaultSealTemplate()).orElse(SEAL_MEDIUM)
                                           : ofNullable(userPreferences.getDefaultSignatureTemplate()).orElse(SIGNATURE_MEDIUM);

        // Annotations
        // TODO : have a seal-specific-one, they should not be shared
        Optional<PdfSignaturePosition> documentAnnotationPosition = Optional.of(document)
                .map(Document::getSignaturePlacementAnnotations)
                .orElse(emptyList())
                .stream()
                .min(Comparator.comparingInt(SignaturePlacement::getSignatureNumber))
                .map(p -> new PdfSignaturePosition(
                        p.getX(),
                        p.getY(),
                        p.getPage(),
                        Optional.ofNullable(p.getRectangleOrigin()).orElse(BOTTOM_LEFT),
                        Optional.ofNullable(p.getTemplateType()).orElse(defaultTemplateType)
                ));

        // HashTag

        Optional<PdfSignaturePosition> documentHashtagPosition = ofNullable(getHashtagPosition(folder, document, action, defaultTemplateType));

        // Type
        // TODO : have a seal-specific-one, they should not be shared
        Optional<PdfSignaturePosition> typeDefinedPosition = ofNullable(folder.getType())
                .map(Type::getSignaturePosition)
                .map(p -> new PdfSignaturePosition(
                                p.getX(),
                                p.getY(),
                                p.getPage(),
                                BOTTOM_LEFT,
                                ofNullable(p.getTemplateType()).orElse(defaultTemplateType)
                        )
                );

        log.debug("PdfSignaturePosition...");
        log.debug("    documentAnnotationPosition : {}", documentAnnotationPosition);
        log.debug("       documentHashtagPosition : {}", documentHashtagPosition);
        log.debug("           typeDefinedPosition : {}", typeDefinedPosition);

        return documentAnnotationPosition
                .or(() -> documentHashtagPosition)
                .or(() -> typeDefinedPosition)
                .orElse(new PdfSignaturePosition(
                        50,
                        50,
                        0,
                        BOTTOM_LEFT,
                        defaultTemplateType
                ));
    }


    public static void checkIfDetachedSignatureAllowed(@NotNull SignatureFormat signatureFormat, boolean hasSomeDetachedSignature) {
        if (signatureFormat.isEnvelopedSignature() && hasSomeDetachedSignature) {
            log.info("checkIfDetachedSignatureAllowed - Is envleopped signature && has detached signature, abort");
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.detached_signature_are_incompatible_with_enveloped_signature_types");
        }
    }


    public static void checkSignatureFormat(@NotNull SignatureFormat signatureFormat,
                                            @NotNull List<Document> documentList,
                                            @Nullable List<MultipartFile> detachedSignatures) {
        boolean hasSomeDetachedSignature = documentList.stream()
                .filter(Document::isMainDocument)
                .map(Document::getDetachedSignatures)
                .mapToLong(CollectionUtils::size)
                .sum() > 1
                || CollectionUtils.isNotEmpty(detachedSignatures);

        checkIfDetachedSignatureAllowed(signatureFormat, hasSomeDetachedSignature);

        // Integrity checks

        boolean isEnvelopedXadesSignature = (signatureFormat == PES_V2);
        boolean isSingleFile = documentList.size() <= 1;
        boolean areAllXmlFile = documentList.stream()
                .filter(Document::isMainDocument)
                .map(Document::getMediaType)
                .allMatch(FileUtils::isXml);
        if (isEnvelopedXadesSignature && !(isSingleFile && areAllXmlFile)) {
            log.info("checkSignatureFormat - Signature type is enveloped XAdES, and doc is not a single XML, abort");
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.enveloped_xades_signature_should_have_a_single_xml_document");
        }
    }


    public static @Nullable JcaX509CertificateHolder parseX509Certificate(@NotNull String publicCertificateBase64) {

        byte[] publicCertificateByteArray = Base64.getDecoder().decode(publicCertificateBase64);
        try (ByteArrayInputStream pubKeyInputStream = new ByteArrayInputStream(publicCertificateByteArray)) {

            CertificateFactory certFactory = CertificateFactory.getInstance(CERTIFICATE_TYPE_X509, new BouncyCastleProvider());
            X509Certificate x509Certificate = (X509Certificate) certFactory.generateCertificate(pubKeyInputStream);
            return new JcaX509CertificateHolder(x509Certificate);

        } catch (IllegalArgumentException | CertificateException | IOException e) {
            log.error("Cannot read public certificate pem", e);
            log.trace("Unreadable PEM:%s".formatted(publicCertificateBase64));
            return null;
        }
    }


    public static @NotNull String getAsn1StringValue(@NotNull X500Name x500name, ASN1ObjectIdentifier asn1ObjectIdentifier) {
        return Optional.of(x500name.getRDNs(asn1ObjectIdentifier))
                .map(List::of)
                .map(Collection::stream)
                .flatMap(Stream::findFirst)
                .map(RDN::getFirst)
                .map(AttributeTypeAndValue::getValue)
                .map(IETFUtils::valueToString)
                .orElse(EMPTY);
    }


    /**
     * May be used in mail templates.
     * TODO : delete this in 5.2.x.
     *
     * @param x500name a parsed X500Name value
     * @return
     * @deprecated use {@link #parseInnerX500Values(X500Name)}.getOrganization() instead
     */
    @Deprecated(since = "5.0.17", forRemoval = true)
    public static @NotNull String getOrganization(@NotNull X500Name x500name) {
        return getAsn1StringValue(x500name, O);
    }


    /**
     * May be used in mail templates.
     * TODO : delete this in 5.2.x.
     *
     * @param x500name a parsed X500Name value
     * @return
     * @deprecated use {@link #parseInnerX500Values(X500Name)}.getCommonName() instead
     */
    @Deprecated(since = "5.0.17", forRemoval = true)
    public static @NotNull String getName(@NotNull X500Name x500name) {
        return getAsn1StringValue(x500name, CN);
    }


    /**
     * May be used in mail templates.
     * TODO : delete this in 5.2.x.
     *
     * @param x500name a parsed X500Name value
     * @return
     * @deprecated use {@link #parseInnerX500Values(X500Name)}.getTitle() instead
     */
    @Deprecated(since = "5.0.17", forRemoval = true)
    public static @NotNull String getTitle(@NotNull X500Name x500name) {
        return getAsn1StringValue(x500name, T);
    }


    /**
     * Parse the X500Name and return the values in a X500Informations object.
     * Used to simplify the signature template generation.
     * <p>
     * We use empty strings here, since it is WAY easier to handle in FreeTemplates than nullable values.
     *
     * @param x500Name
     * @return
     */
    public static @NotNull CertificateInformations parseInnerX500Values(@NotNull X500Name x500Name) {
        CertificateInformations x500Informations = new CertificateInformations();
        x500Informations.setTitle(CryptoUtils.getAsn1StringValue(x500Name, T));
        x500Informations.setCommonName(CryptoUtils.getAsn1StringValue(x500Name, CN));
        x500Informations.setEmailAddress(CryptoUtils.getAsn1StringValue(x500Name, E));
        x500Informations.setOrganizationalUnit(CryptoUtils.getAsn1StringValue(x500Name, OU));
        x500Informations.setOrganization(CryptoUtils.getAsn1StringValue(x500Name, O));
        x500Informations.setLocality(CryptoUtils.getAsn1StringValue(x500Name, L));
        x500Informations.setStateOrProvince(CryptoUtils.getAsn1StringValue(x500Name, ST));
        x500Informations.setCountry(CryptoUtils.getAsn1StringValue(x500Name, C));
        return x500Informations;
    }


    /**
     * Parse the external signature info metadata into a more convenient Index-Object map.
     *
     * @param metadataMap properly set
     * @return a Long/Object map
     */
    public static Map<Long, SignRequestMember> getExternalSignatureMetadataMap(@NotNull Map<String, String> metadataMap) {

        Map<Long, SignRequestMember> result = new HashMap<>();

        metadataMap.entrySet().stream()
                .filter(entry -> StringUtils.isNotEmpty(entry.getValue()))
                .forEach(entry -> Stream
                        .of(
                                METADATA_EXTERNAL_SIGNATURE_KEY_FIRSTNAME_PATTERN,
                                METADATA_EXTERNAL_SIGNATURE_KEY_LASTNAME_PATTERN,
                                METADATA_EXTERNAL_SIGNATURE_KEY_MAIL_PATTERN,
                                METADATA_EXTERNAL_SIGNATURE_KEY_PHONE_PATTERN
                        )
                        .map(pattern -> pattern.matcher(entry.getKey()))
                        .filter(Matcher::matches)
                        .forEach(matcher -> {
                            Long index = Optional.of(matcher.group(METADATA_EXTERNAL_SIGNATURE_KEY_INDEX_GROUP))
                                    .map(catchIndex -> {
                                        if (StringUtils.isEmpty(catchIndex)) {
                                            return 0L;
                                        } else if (NumberUtils.isParsable(catchIndex)) {
                                            return Long.parseLong(catchIndex);
                                        } else {
                                            return null;
                                        }
                                    })
                                    .orElse(-1L);

                            result.computeIfAbsent(index, k -> new SignRequestMember());
                            SignRequestMember signRequestMember = result.get(index);

                            switch (matcher.group(METADATA_EXTERNAL_SIGNATURE_KEY_KEY_GROUP)) {
                                case METADATA_EXTERNAL_SIGNATURE_KEY_MAIL -> signRequestMember.setEmail(entry.getValue());
                                case METADATA_EXTERNAL_SIGNATURE_KEY_PHONE -> signRequestMember.setPhone(entry.getValue());
                                case METADATA_EXTERNAL_SIGNATURE_KEY_FIRSTNAME -> signRequestMember.setFirstname(entry.getValue());
                                case METADATA_EXTERNAL_SIGNATURE_KEY_LASTNAME -> signRequestMember.setLastname(entry.getValue());
                            }
                        })
                );

        return result;
    }


    public static boolean isConfigurationComplete(@Nullable SignRequestMember signRequestMember) {
        return Optional
                .ofNullable(signRequestMember)
                .map(member -> StringUtils.isNoneEmpty(
                        member.getFirstname(),
                        member.getLastname(),
                        member.getEmail(),
                        member.getPhone()
                ))
                .orElse(false);
    }


}
