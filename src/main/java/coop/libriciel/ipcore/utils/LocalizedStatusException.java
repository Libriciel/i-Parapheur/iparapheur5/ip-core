/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.utils;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.PropertyKey;
import java.io.Serial;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

import static coop.libriciel.ipcore.utils.ApiUtils.JAVAX_MESSAGE_PREFIX;
import static coop.libriciel.ipcore.utils.ApiUtils.JAVAX_MESSAGE_SUFFIX;
import static coop.libriciel.ipcore.utils.TextUtils.MESSAGE_BUNDLE;


public class LocalizedStatusException extends ResponseStatusException {


    @Serial private static final long serialVersionUID = 1L;


    public static String getMessageForLocale(@PropertyKey String messageKey, @NotNull Locale locale, @Nullable Object... stringArgs) {

        if (StringUtils.isEmpty(messageKey)) {
            return null;
        }

        // Validated annotations support translated templates wrapped in curly braces : @Min{value=5, message="{message.xxx}"}
        // Those messages will be properly translated and returned by the SpringBoot engine. Nothing more to do.
        // Yet, on manual validations, (using the ValidatorFactory...) errors will return the "wrapped" message.
        // We we want to get rid of these braces to get the proper error message.
        if (StringUtils.startsWith(messageKey, JAVAX_MESSAGE_PREFIX) && StringUtils.endsWith(messageKey, JAVAX_MESSAGE_SUFFIX)) {
            messageKey = new StringBuilder(messageKey)
                    .deleteCharAt(messageKey.length() - 1)
                    .deleteCharAt(0)
                    .toString();
        }

        String message = ResourceBundle
                .getBundle(MESSAGE_BUNDLE, locale)
                .getString(messageKey);

        message = TextUtils.escapeMessageFormatSpecialChars(message);
        return MessageFormat.format(message, stringArgs);
    }


    public LocalizedStatusException(HttpStatusCode status, @PropertyKey String messageKey, @Nullable Object... stringArgs) {
        super(status, getMessageForLocale(messageKey, Locale.getDefault(), stringArgs));
    }


    public LocalizedStatusException(HttpStatus status, Throwable cause, @PropertyKey String messageKey, @Nullable Object... stringArgs) {
        super(status, getMessageForLocale(messageKey, Locale.getDefault(), stringArgs), cause);
    }


}
