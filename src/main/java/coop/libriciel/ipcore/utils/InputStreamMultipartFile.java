/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.utils;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;


@Log4j2
public record InputStreamMultipartFile(@Getter @NotNull String name,
                                       @Getter @NotNull InputStream inputStream,
                                       @Nullable MediaType mediaType) implements MultipartFile {


    @Override
    public String getOriginalFilename() {
        return name;
    }


    @Override
    public String getContentType() {
        return mediaType != null ? mediaType.toString() : null;
    }


    @Override
    public boolean isEmpty() {
        try {
            return inputStream.available() < 0;
        } catch (IOException e) {
            throw new ResponseStatusException(INTERNAL_SERVER_ERROR, "Error reading InputStreamMultipartFile content", e);
        }
    }


    @Override
    public long getSize() {
        try {
            return inputStream.available();
        } catch (IOException e) {
            throw new ResponseStatusException(INTERNAL_SERVER_ERROR, "Error reading InputStreamMultipartFile content", e);
        }
    }


    @Override
    public byte @NotNull [] getBytes() throws IOException {
        try {
            return inputStream.readAllBytes();
        } catch (IOException e) {
            throw new ResponseStatusException(INTERNAL_SERVER_ERROR, "Error reading InputStreamMultipartFile content", e);
        }
    }


    @Override
    public void transferTo(@NotNull File dest) throws IllegalStateException {
        throw new UnsupportedOperationException();
    }


}
