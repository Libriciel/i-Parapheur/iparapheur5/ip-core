/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.utils;

import coop.libriciel.ipcore.model.workflow.Action;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.State;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.model.workflow.Task.ExternalState;
import coop.libriciel.ipcore.model.workflow.ipWorkflow.IpWorkflowTask;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static coop.libriciel.ipcore.model.workflow.Action.EXTERNAL_SIGNATURE;
import static coop.libriciel.ipcore.model.workflow.State.*;
import static coop.libriciel.ipcore.model.workflow.Task.ExternalState.*;
import static java.util.Collections.emptyList;
import static java.util.Comparator.comparing;
import static java.util.Comparator.naturalOrder;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;


@Log4j2
public class FolderUtils {

    public static final int MAX_SIGNATURE_PROOF_FILE_PER_FOLDER = 30;

    public static final int CREATION_WORKFLOW_INDEX = 1;
    public static final int VALIDATION_WORKFLOW_SMALLEST_INDEX = 2;


    private FolderUtils() {
        throw new IllegalStateException("Utility class");
    }


    public static ExternalState mapTaskStatus(String state) {
        return switch (state) {
            case "ACTIVE" -> ACTIVE;
            case "SIGNED" -> SIGNED;
            case "REFUSED" -> REFUSED;
            case "EXPIRED" -> EXPIRED;
            case "FORM" -> FORM;
            case "ERROR" -> ERROR;
            case "modification" -> IN_REDACTION;
            case "supression" -> DELETED;
            case "renvoi" -> SENT_AGAIN;
            case "reception-partielle" -> RECEIVED_PARTIALLY;
            case "non-recu" -> NOT_RECEIVED;
            case "SENT", "envoi" -> SENT;
            case "RECEIVED", "reception" -> RECEIVED;
            case "CREATED", "creation" -> CREATED;
            default -> throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.state_not_found");
        };
    }


    public static boolean isActive(@Nullable State state) {

        if (state == null) {
            return false;
        }

        return Set.of(CURRENT, PENDING).contains(state);
    }


    public static boolean isActiveOrDraftOrFinished(@Nullable State state) {

        if (state == null) {
            return false;
        }

        return Set.of(DRAFT, CURRENT, PENDING, FINISHED).contains(state);
    }


    public static State computeFolderStateFromTaskList(@NotNull List<IpWorkflowTask> tasks) {

        if (tasks.stream().map(IpWorkflowTask::getState).anyMatch(state -> state == REJECTED)) {
            return REJECTED;
        }

        return tasks.stream()
                .filter(task -> task.getPerformedAction() == null)
                .filter(task -> task.getAction() != Action.READ)
                .filter(task -> isActiveOrDraftOrFinished(task.getState()))
                .findFirst()
                .map(FolderUtils::computeFolderStateFromTask)
                .orElseGet(() -> {
                    log.warn("computeFolderStateFromTaskList cannot determine the folder state from tasks:{}", tasks);
                    return PENDING;
                });
    }


    public static State computeFolderStateFromTask(@NotNull IpWorkflowTask task) {
        return computeFolderStateFromActionAndDate(task.getAction(), task.getFolderDueDate());
    }


    public static State computeFolderStateFromActionAndDate(@Nullable Action action, @Nullable Date dueDate) {

        if (action == null) {
            return null;
        }

        return switch (action) {
            case START -> DRAFT;
            case DELETE -> REJECTED;
            case ARCHIVE -> FINISHED;
            default -> Optional.ofNullable(dueDate)
                    .filter(date -> date.compareTo(new Date()) <= 0)
                    .map(d -> LATE)
                    .orElse(PENDING);
        };
    }


    /**
     * Workflow index is :
     * - 0 on draft/start
     * - 1 in the creation workflow
     * - 2 in the validation workflow
     * - n for any chained validation workflow
     * - MAX_INT at the end
     *
     * @param folder with a {@link Folder#getStepList} properly available
     * @return the evaluation
     */
    public static boolean hasBeenStarted(@NotNull Folder folder) {

        if (CollectionUtils.isEmpty(folder.getStepList())) {
            throw new RuntimeException("hasBeenStarted should be called with an initialized folder's step list");
        }

        return folder.getStepList().stream()
                .filter(task -> isActive(task.getState()))
                .map(Task::getWorkflowIndex)
                .anyMatch(workflowIndex -> workflowIndex > CREATION_WORKFLOW_INDEX);
    }


    public static @Nullable Task getLastPerformedTask(@Nullable Folder folder, @NotNull Set<Action> actionsFilter) {
        return Optional.ofNullable(folder)
                .map(Folder::getStepList)
                .orElse(emptyList())
                .stream()
                .filter(t -> actionsFilter.contains(t.getAction()))
                .filter(t -> t.getState() == VALIDATED)
                .filter(t -> t.getDate() != null)
                .max(comparing(Task::getDate, naturalOrder()))
                .orElse(null);
    }


    /**
     * Get the current ext-sig index, starting at 1.
     * Note: The 0-value is the default one, applicable for any index.
     *
     * @param taskList already initialized
     * @return the index
     */
    public static long getCurrentExternalSignatureIndex(@NotNull List<Task> taskList) {
        return taskList.stream()
                .filter(step -> step.getAction() == EXTERNAL_SIGNATURE)
                .filter(step -> step.getPerformedAction() == EXTERNAL_SIGNATURE)
                .count() + 1;
    }


    /**
     * A simple action-type step count.
     * Some transfers and/or second opinion may pollute a simple count,
     * this method takes count of the definition only.
     *
     * @param taskList already initialized
     * @return the count
     */
    public static long countStepsWithAction(@NotNull List<Task> taskList, @NotNull Action action) {
        return taskList.stream()
                .filter(step -> step.getAction() == action)
                .filter(step -> step.getPerformedAction() == action || step.getPerformedAction() == null)
                .count();
    }


}
