/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
package coop.libriciel.ipcore.model.externalsignature;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import coop.libriciel.external.signature.connector.api.model.FileWrapper;
import coop.libriciel.ipcore.model.crypto.PdfSignaturePosition;
import coop.libriciel.ipcore.model.workflow.requests.SimpleTaskParams;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static coop.libriciel.ipcore.utils.TextUtils.RESERVED_PREFIX;
import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;


@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode(callSuper = true)
public class ExternalSignatureParams extends SimpleTaskParams {


    private static final String EXTERNAL_SIGNATURE_METADATA_KEY = RESERVED_PREFIX + "ext_sig_";

    public static final String METADATA_EXTERNAL_SIGNATURE_KEY_MAIL = EXTERNAL_SIGNATURE_METADATA_KEY + "mail";
    public static final String METADATA_EXTERNAL_SIGNATURE_KEY_PHONE = EXTERNAL_SIGNATURE_METADATA_KEY + "phone";
    public static final String METADATA_EXTERNAL_SIGNATURE_KEY_FIRSTNAME = EXTERNAL_SIGNATURE_METADATA_KEY + "firstname";
    public static final String METADATA_EXTERNAL_SIGNATURE_KEY_LASTNAME = EXTERNAL_SIGNATURE_METADATA_KEY + "lastname";

    public static Map<String, String> METADATA_EXTERNAL_SIGNATURE_TO_DOCKET_LABEL = Map.of(
            METADATA_EXTERNAL_SIGNATURE_KEY_MAIL, "message.ext_sig_mail_label",
            METADATA_EXTERNAL_SIGNATURE_KEY_PHONE, "message.ext_sig_phone_number_label",
            METADATA_EXTERNAL_SIGNATURE_KEY_FIRSTNAME, "message.ext_sig_first_name_label",
            METADATA_EXTERNAL_SIGNATURE_KEY_LASTNAME, "message.ext_sig_last_name_label"
    );


    private String name;
    private String folderId;
    private Collection<SignRequestMember> members = new ArrayList<>();
    private Map<String, PdfSignaturePosition> documentIdsToSignaturePlacementMapping;

    @Schema(accessMode = READ_ONLY)
    private List<FileWrapper> fileWrappers;

}
