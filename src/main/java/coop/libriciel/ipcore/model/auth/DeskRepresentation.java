/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.auth;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import coop.libriciel.ipcore.model.workflow.DelegationRule;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

import java.util.ArrayList;
import java.util.List;

import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.TextUtils.NO_FORBIDDEN_CHARACTERS_REGEXP;


/**
 * Simple object for minimal {@link Desk} representation
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeskRepresentation {


    public static final String API_DOC_ID_VALUE = "Desk id";
    public static final String DEFAULT_SORT_NAME = "NAME";
    public static final String API_PATH = "deskId";


    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    protected String id;

    @Size(min = NAME_MIN_LENGTH, max = NAME_MAX_LENGTH, message = NAME_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
    @NotNull(message = NAME_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
    @Pattern(regexp = NO_FORBIDDEN_CHARACTERS_REGEXP, message = NAME_CONTAINS_FORBIDDEN_CHARACTER_JAVAX_VALIDATION_ERROR_MESSAGE)
    protected String name;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    protected String tenantId;

    @JsonIgnore
    protected List<DelegationRule> delegationRules = new ArrayList<>();


    public DeskRepresentation(String id) {
        this.id = id;
    }


    public DeskRepresentation(String id, String name) {
        this(id);
        this.name = name;
    }


    public DeskRepresentation(String id, String name, String tenantId) {
        this(id, name);
        this.tenantId = tenantId;
    }


}
