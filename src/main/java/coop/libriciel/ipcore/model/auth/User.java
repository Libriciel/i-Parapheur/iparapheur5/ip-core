/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.auth;

import com.fasterxml.jackson.annotation.JsonIgnore;
import coop.libriciel.ipcore.model.database.TenantRepresentation;
import jakarta.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.jetbrains.annotations.NotNull;
import org.jooq.Record7;
import org.jooq.Record8;
import org.keycloak.representations.idm.UserRepresentation;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static coop.libriciel.ipcore.model.auth.UserPrivilege.*;
import static coop.libriciel.ipcore.utils.TextUtils.INTERNAL_PREFIX;
import static java.util.Collections.emptyList;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;
import static org.apache.commons.lang3.StringUtils.EMPTY;


@Data
@Log4j2
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {


    public static final String FIELD_ID = "id";
    public static final String FIELD_USERNAME = "userName";
    public static final String FIELD_FIRST_NAME = "firstName";
    public static final String FIELD_LAST_NAME = "lastName";
    public static final String FIELD_EMAIL = "email";
    public static final String FIELD_PRIVILEGE = "privilege";
    public static final String FIELD_PRIVILEGE_INDEX = "privilegeIndex";
    public static final String FIELD_FEDERATION = "federation";
    public static final String FIELD_IDENTITY_PROVIDER = "identityProvider";


    /**
     * The {@link #notificationsCronFrequency} attribute can take those values :
     * - {@link #USER_PREFERENCE_SINGLE_NOTIFICATIONS} : Single mail for every notification
     * - `0 30 * * *` : A grouped mail, everytime the cron triggers.
     * - `null`, or 'none' any non-cron String : Notification disabled
     * <p>
     * No need to store multiple values, this removes any ambiguity.
     */
    public static final String USER_PREFERENCE_NO_NOTIFICATIONS = "none";
    public static final String USER_PREFERENCE_SINGLE_NOTIFICATIONS = "single_notifications";
    public static final String ATTRIBUTE_NOTIFICATIONS_CRON_FREQUENCY = INTERNAL_PREFIX + "notifications_cron_frequency";
    public static final String ATTRIBUTE_NOTIFICATIONS_REDIRECTION_MAIL = INTERNAL_PREFIX + "notifications_redirection_mail";
    public static final String ATTRIBUTE_COMPLEMENTARY_FIELD = INTERNAL_PREFIX + "complementary_field";
    public static final String ATTRIBUTE_CONTENT_GROUP_INDEX = INTERNAL_PREFIX + "content_group_index";
    public static final String ATTRIBUTE_CONTENT_NODE_ID = INTERNAL_PREFIX + "content_node_id";
    public static final String ATTRIBUTE_SIGNATURE_IMAGE_CONTENT_ID = INTERNAL_PREFIX + "signature_image_content_id";
    public static final String ATTRIBUTE_IS_NOTIFIED_ON_CONFIDENTIAL_FOLDERS = INTERNAL_PREFIX + "is_notified_on_confidential_folders";
    public static final String ATTRIBUTE_IS_NOTIFIED_ON_FOLLOWED_FOLDERS = INTERNAL_PREFIX + "is_notified_on_followed_folders";
    public static final String ATTRIBUTE_IS_NOTIFIED_ON_LATE_FOLDERS = INTERNAL_PREFIX + "is_notified_on_late_folders";


    private String id;
    private String userName;
    private String email;
    private String firstName;
    private String lastName;
    private String notificationsRedirectionMail;
    private boolean isNotifiedOnConfidentialFolders;
    private boolean isNotifiedOnFollowedFolders;
    private boolean isNotifiedOnLateFolders;
    private String notificationsCronFrequency;

    private String complementaryField;

    private @JsonIgnore Integer contentGroupIndex;
    private @JsonIgnore String contentNodeId;
    private String signatureImageContentId;

    private UserPrivilege privilege;
    private @Builder.Default @Transient List<TenantRepresentation> administeredTenants = new ArrayList<>();
    private @Builder.Default @Transient List<DeskRepresentation> administeredDesks = new ArrayList<>();
    private @Builder.Default @Transient List<DeskRepresentation> supervisedDesks = new ArrayList<>();

    private Boolean isChecked;
    private Boolean isLocked;
    private Boolean isLdapSynchronized;
    private Boolean isFromIdentityProvider;
    private Integer rolesCount;


    public User(@NotNull UserRepresentation ur) {

        this.id = ur.getId();
        this.userName = ur.getUsername();

        // Keycloak returns "null" on empty fields. It messes up the "not-nullable" API.
        // Poorly set LDAP can make empty values here, everything's easier with empty Strings.
        this.firstName = defaultIfNull(ur.getFirstName(), EMPTY);
        this.lastName = defaultIfNull(ur.getLastName(), EMPTY);
        this.email = defaultIfNull(ur.getEmail(), EMPTY);

        this.rolesCount = Optional.ofNullable(ur.getRealmRoles())
                .map(List::size).orElse(0);

        this.notificationsCronFrequency = Optional.ofNullable(ur.getAttributes())
                .map(a -> a.get(ATTRIBUTE_NOTIFICATIONS_CRON_FREQUENCY)).orElse(emptyList())
                .stream()
                .filter(StringUtils::isNotEmpty)
                .findFirst().orElse(null);

        this.isNotifiedOnConfidentialFolders = Boolean.TRUE.equals(Optional.ofNullable(ur.getAttributes())
                .map(a -> a.get(ATTRIBUTE_IS_NOTIFIED_ON_CONFIDENTIAL_FOLDERS)).orElse(emptyList())
                .stream()
                .map(Boolean::parseBoolean)
                .findFirst().orElse(null));

        this.isNotifiedOnFollowedFolders = Boolean.TRUE.equals(Optional.ofNullable(ur.getAttributes())
                .map(a -> a.get(ATTRIBUTE_IS_NOTIFIED_ON_FOLLOWED_FOLDERS)).orElse(emptyList())
                .stream()
                .map(Boolean::parseBoolean)
                .findFirst().orElse(null));

        this.isNotifiedOnLateFolders = Boolean.TRUE.equals(Optional.ofNullable(ur.getAttributes())
                .map(a -> a.get(ATTRIBUTE_IS_NOTIFIED_ON_LATE_FOLDERS)).orElse(emptyList())
                .stream()
                .map(Boolean::parseBoolean)
                .findFirst().orElse(null));

        this.notificationsRedirectionMail = Optional.ofNullable(ur.getAttributes())
                .map(a -> a.get(ATTRIBUTE_NOTIFICATIONS_REDIRECTION_MAIL)).orElse(emptyList())
                .stream()
                .filter(StringUtils::isNotEmpty)
                .findFirst().orElse(null);

        this.complementaryField = Optional.ofNullable(ur.getAttributes())
                .map(a -> a.get(ATTRIBUTE_COMPLEMENTARY_FIELD)).orElse(emptyList())
                .stream()
                .filter(StringUtils::isNotEmpty)
                .findFirst().orElse(null);

        this.contentGroupIndex = Optional.ofNullable(ur.getAttributes())
                .map(a -> a.get(ATTRIBUTE_CONTENT_GROUP_INDEX)).orElse(emptyList())
                .stream()
                .filter(NumberUtils::isParsable)
                .map(Integer::parseInt)
                .findFirst().orElse(null);

        this.contentNodeId = Optional.ofNullable(ur.getAttributes())
                .map(a -> a.get(ATTRIBUTE_CONTENT_NODE_ID)).orElse(emptyList())
                .stream()
                .filter(StringUtils::isNotEmpty)
                .findFirst().orElse(null);

        this.signatureImageContentId = Optional.ofNullable(ur.getAttributes())
                .map(a -> a.get(ATTRIBUTE_SIGNATURE_IMAGE_CONTENT_ID)).orElse(emptyList())
                .stream()
                .filter(StringUtils::isNotEmpty)
                .findFirst().orElse(null);

        this.isLdapSynchronized = Optional.ofNullable(ur.getFederationLink())
                .map(StringUtils::isNotEmpty)
                .orElse(false);
    }


    public User(@NotNull String id) {
        this.id = id;
    }


    public User(@NotNull Record7<String, String, String, String, String, String, String> record) {
        this(record.get(FIELD_ID, String.class));
        this.userName = record.get(FIELD_USERNAME, String.class);

        // Keycloak returns "null" on empty fields. It messes up the "not-nullable" API.
        // Poorly set LDAP can make empty values here, everything's easier with empty Strings.
        this.firstName = defaultIfNull(record.get(FIELD_FIRST_NAME, String.class), EMPTY);
        this.lastName = defaultIfNull(record.get(FIELD_LAST_NAME, String.class), EMPTY);
        this.email = defaultIfNull(record.get(FIELD_EMAIL, String.class), EMPTY);

        this.isLdapSynchronized = StringUtils.isNotEmpty(record.get(FIELD_FEDERATION, String.class));
        this.isFromIdentityProvider = StringUtils.isNotEmpty(record.get(FIELD_IDENTITY_PROVIDER, String.class));

    }


    public User(@NotNull Record8<String, String, String, String, String, String, String, Integer> record) {
        this(record.get(FIELD_ID, String.class));
        this.userName = record.get(FIELD_USERNAME, String.class);

        // Keycloak returns "null" on empty fields. It messes up the "not-nullable" API.
        // Poorly set LDAP can make empty values here, everything's easier with empty Strings.
        this.firstName = defaultIfNull(record.get(FIELD_FIRST_NAME, String.class), EMPTY);
        this.lastName = defaultIfNull(record.get(FIELD_LAST_NAME, String.class), EMPTY);
        this.email = defaultIfNull(record.get(FIELD_EMAIL, String.class), EMPTY);

        this.isLdapSynchronized = StringUtils.isNotEmpty(record.get(FIELD_FEDERATION, String.class));
        this.isFromIdentityProvider = StringUtils.isNotEmpty(record.get(FIELD_IDENTITY_PROVIDER, String.class));
        switch (record.get(FIELD_PRIVILEGE_INDEX, Integer.class)) {
            case 3 -> this.privilege = TENANT_ADMIN;
            case 2 -> this.privilege = FUNCTIONAL_ADMIN;
            default -> this.privilege = NONE;
        }
    }


}
