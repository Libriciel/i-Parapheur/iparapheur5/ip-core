/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.auth;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class PasswordPolicies {


    @Getter
    @AllArgsConstructor
    public enum KnownRegexes {

        /**
         * Some users don't want any Regex, and prefers weaker.
         * This is the wildcard regex. Any char, zero or more time.
         */
        NO_RESTRICTION(".*"),

        /**
         * Every line is an "or", here's the bigger one : `(?<lowercaseuppercasedigitsspecial>(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=[^a-zA-Z0-9\n]).{12,})`
         * - `(?<lowercaseuppercasedigitsspecial>...)` : The main group, named to ease the code review
         * - `(?=.*[a-z])`                             : A positive-lookahead to check if there is any lowercase characters
         * - `(?=.*[A-Z])`                             : A positive-lookahead to check if there is any uppercase characters
         * - `(?=.*[0-9])`                             : A positive-lookahead to check if there is any digits characters
         * - `(?=[^a-zA-Z0-9\n])`                      : A positive-lookahead to check if there is any special characters
         * - `.{12,}`                                  : 12 characters, at least (their content has already been tested previously)
         * <p>
         * Every other line is a truncated variation of this one.
         * There is commonly 32 special chars on a keyboard, but come common AZERTY chars (`²°*¤ù£$€`) does not count towards.
         * Accentuated chars are commonly listed as special chars.
         * <p>
         * The CNIL defines it as 37 characters, so... We just assume a 37-chars "alphabet" in here anyway.
         */
        ENTROPY_80(
                """
                ^
                (?<lowercaseuppercasedigitsspecial>(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^a-zA-Z0-9]).{12,})
                |(?<lowercaseuppercasedigits>(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{14,})
                |(?<lowercaseuppercasespecial>(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9]).{13,})
                |(?<lowercasedigitsspecial>(?=.*[a-z])(?=.*[0-9])(?=.*[^a-zA-Z0-9]).{13,})
                |(?<uppercasedigitsspecial>(?=.*[A-Z])(?=.*[0-9])(?=.*[^a-zA-Z0-9]).{13,})
                |(?<lowercaseuppercase>(?=.*[a-z])(?=.*[A-Z]).{14,})
                |(?<lowercasedigits>(?=.*[a-z])(?=.*[0-9]).{16,})
                |(?<lowercasespecial>(?=.*[a-z])(?=.*[^a-zA-Z0-9]).{14,})
                |(?<uppercasedigits>(?=.*[A-Z])(?=.*[0-9]).{16,})
                |(?<uppercasespecial>(?=.*[A-Z])(?=.*[^a-zA-Z0-9]).{14,})
                |(?<digitsspecial>(?=.*[0-9])(?=.*[^a-zA-Z0-9]).{15,})
                |(?<lowercase>[a-z]{17,})
                |(?<uppercase>[A-Z]{17,})
                |(?<digits>[0-9]{24,})
                |(?<special>[^a-zA-Z0-9]{16,})
                $
                """
                        // Shrink to ease the parameter import
                        .replaceAll("<.*?>", ":")
                        .replaceAll("\n", "")
        );


        private final String regex;


    }


    /**
     * The minimal value to match the regex above.
     * Having this as an additional rule is not that useless; it adds a comprehensible error message.
     */
    public static final int DEFAULT_PASSWORD_MINIMUM_LENGTH = 12;


    private int minLength = 0;
    private int maxLength = 0;

    private boolean notUsername = false;
    private boolean notEmail = false;

    private int specialCharsMinCount = 0;
    private int uppercaseCharsMinCount = 0;
    private int lowercaseCharsMinCount = 0;
    private int digitsMinCount = 0;

    private String regexPattern = null;

    /**
     * The following rules are internal.
     * They should be parsed, but not send to web
     */

    private @JsonIgnore String hashAlgorithm = null;
    private @JsonIgnore int passwordHistory = 0;
    private @JsonIgnore int maxAuthAge = 0;
    private @JsonIgnore int forceExpiredPasswordChange = 0;

}
