/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.auth.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import coop.libriciel.ipcore.model.auth.UserPrivilege;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.NoArgsConstructor;

import static coop.libriciel.ipcore.model.auth.requests.UserRepresentation.*;
import static coop.libriciel.ipcore.utils.ApiUtils.JAVAX_MESSAGE_PREFIX;
import static coop.libriciel.ipcore.utils.ApiUtils.JAVAX_MESSAGE_SUFFIX;


/**
 * This class should inherit the {@link UserRepresentation}, but OpenApi manages the inheritance poorly.
 * We want to deprecate some fields from the representation model, and mode them into this class.
 * TODO: Once the 5.4.0 is published, the 5.2 would be out of support.
 *  We should then remove the deprecated fields in the representation, and reset the inheritance, cleaning the id/userName... fields here.
 */
@Data
@NoArgsConstructor
public class ListableUser {


    static final String EMAIL_VALIDATION_ERROR_MESSAGE = "message.email_is_invalid";
    static final String EMAIL_JAVAX_VALIDATION_ERROR_MESSAGE = JAVAX_MESSAGE_PREFIX + EMAIL_VALIDATION_ERROR_MESSAGE + JAVAX_MESSAGE_SUFFIX;

    static final String PRIVILEGE_MISSING_ERROR_MESSAGE = "message.privilege_is_missing";
    static final String PRIVILEGE_JAVAX_VALIDATION_ERROR_MESSAGE = JAVAX_MESSAGE_PREFIX + PRIVILEGE_MISSING_ERROR_MESSAGE + JAVAX_MESSAGE_SUFFIX;


    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private String id;

    @Size(min = USERNAME_MIN_LENGTH, max = USERNAME_MAX_LENGTH, message = USERNAME_JAVAX_VALIDATION_ERROR_MESSAGE)
    @NotNull(message = USERNAME_JAVAX_VALIDATION_ERROR_MESSAGE)
    private String userName;

    @Size(max = FIRST_NAME_MAX_LENGTH, message = FIRST_NAME_JAVAX_VALIDATION_ERROR_MESSAGE)
    @NotNull(message = FIRST_NAME_JAVAX_VALIDATION_ERROR_MESSAGE)
    private String firstName;

    @Size(max = LAST_NAME_MAX_LENGTH, message = LAST_NAME_JAVAX_VALIDATION_ERROR_MESSAGE)
    @NotNull(message = LAST_NAME_JAVAX_VALIDATION_ERROR_MESSAGE)
    private String lastName;

    @Email(message = EMAIL_JAVAX_VALIDATION_ERROR_MESSAGE)
    @NotNull(message = EMAIL_JAVAX_VALIDATION_ERROR_MESSAGE)
    private String email;

    @NotNull(message = PRIVILEGE_JAVAX_VALIDATION_ERROR_MESSAGE)
    private UserPrivilege privilege;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private Boolean isLdapSynchronized;


}
