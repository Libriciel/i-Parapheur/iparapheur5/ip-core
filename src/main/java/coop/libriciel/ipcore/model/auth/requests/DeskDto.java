/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.auth.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.MetadataRepresentation;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.util.List;

import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static io.swagger.v3.oas.annotations.media.Schema.AccessMode;


@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class DeskDto extends DeskRepresentation {


    // <editor-fold desc="Error messages">


    private static final String OWNER_LIST_NOT_NULL_ERROR_MESSAGE = "message.owner_list_should_not_be_null";
    private static final String OWNER_LIST_NOT_NULL_JAVAX_VALIDATION_ERROR_MESSAGE =
            JAVAX_MESSAGE_PREFIX + OWNER_LIST_NOT_NULL_ERROR_MESSAGE + JAVAX_MESSAGE_SUFFIX;

    private static final String OWNER_LIST_MAX_SIZE_ERROR_MESSAGE = "message.owner_list_size_should_be_under_256";
    private static final String OWNER_LIST_MAX_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE =
            JAVAX_MESSAGE_PREFIX + OWNER_LIST_MAX_SIZE_ERROR_MESSAGE + JAVAX_MESSAGE_SUFFIX;

    private static final int MAX_ASSOCIATED_DESKS_PER_DESKS_COUNT = 250;

    private static final String ASSOCIATED_DESKS_LIST_NOT_NULL_ERROR_MESSAGE = "message.associated_desks_list_should_not_be_null";
    private static final String ASSOCIATED_DESKS_LIST_NOT_NULL_JAVAX_VALIDATION_ERROR_MESSAGE =
            JAVAX_MESSAGE_PREFIX + ASSOCIATED_DESKS_LIST_NOT_NULL_ERROR_MESSAGE + JAVAX_MESSAGE_SUFFIX;

    private static final String ASSOCIATED_DESKS_MAX_SIZE_ERROR_MESSAGE = "message.associated_desks_should_be_under_256";
    private static final String ASSOCIATED_DESKS_MAX_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE =
            JAVAX_MESSAGE_PREFIX + ASSOCIATED_DESKS_MAX_SIZE_ERROR_MESSAGE + JAVAX_MESSAGE_SUFFIX;

    private static final int MAX_FILTERABLE_METADATA_PER_DESKS_COUNT = 250;

    private static final String FILTERABLE_METADATA_LIST_NOT_NULL_ERROR_MESSAGE = "message.filterable_metadata_list_should_not_be_null";
    private static final String FILTERABLE_METADATA_LIST_NOT_NULL_JAVAX_VALIDATION_ERROR_MESSAGE =
            JAVAX_MESSAGE_PREFIX + FILTERABLE_METADATA_LIST_NOT_NULL_ERROR_MESSAGE + JAVAX_MESSAGE_SUFFIX;

    private static final String FILTERABLE_METADATA_MAX_SIZE_ERROR_MESSAGE = "message.filterable_metadata_should_be_under_256";
    private static final String FILTERABLE_METADATA_MAX_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE =
            JAVAX_MESSAGE_PREFIX + FILTERABLE_METADATA_MAX_SIZE_ERROR_MESSAGE + JAVAX_MESSAGE_SUFFIX;

    private static final int MAX_SUPERVISOR_PER_DESKS_COUNT = 50;

    private static final String SUPERVISOR_LIST_NOT_NULL_ERROR_MESSAGE = "message.supervisor_list_should_not_be_null";
    private static final String SUPERVISOR_LIST_NOT_NULL_JAVAX_VALIDATION_ERROR_MESSAGE =
            JAVAX_MESSAGE_PREFIX + OWNER_LIST_NOT_NULL_ERROR_MESSAGE + JAVAX_MESSAGE_SUFFIX;

    private static final String SUPERVISOR_LIST_MAX_SIZE_ERROR_MESSAGE = "message.supervisor_should_be_under_50";
    private static final String SUPERVISOR_LIST_MAX_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE =
            JAVAX_MESSAGE_PREFIX + SUPERVISOR_LIST_MAX_SIZE_ERROR_MESSAGE + JAVAX_MESSAGE_SUFFIX;

    private static final int MAX_DELEGATION_MANAGER_PER_DESKS_COUNT = 50;

    private static final String DELEGATION_MANAGER_LIST_NOT_NULL_ERROR_MESSAGE = "message.delegation_manager_list_should_not_be_null";
    private static final String DELEGATION_MANAGER_LIST_NOT_NULL_JAVAX_VALIDATION_ERROR_MESSAGE =
            JAVAX_MESSAGE_PREFIX + DELEGATION_MANAGER_LIST_NOT_NULL_ERROR_MESSAGE + JAVAX_MESSAGE_SUFFIX;

    private static final String DELEGATION_MANAGER_LIST_MAX_SIZE_ERROR_MESSAGE = "message.delegation_manager_should_be_under_50";
    private static final String DELEGATION_MANAGER_LIST_MAX_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE =
            JAVAX_MESSAGE_PREFIX + DELEGATION_MANAGER_LIST_MAX_SIZE_ERROR_MESSAGE + JAVAX_MESSAGE_SUFFIX;


    // </editor-fold desc="Error messages">


    @Size(min = NAME_MIN_LENGTH, max = NAME_MAX_LENGTH, message = NAME_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
    @NotNull(message = NAME_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
    private String shortName;

    private String description;

    private boolean folderCreationAllowed = true;
    private boolean actionAllowed = true;
    private boolean archivingAllowed = true;
    private boolean chainAllowed = true;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY)
    private String parentDeskId;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private DeskRepresentation parentDesk;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY)
    @Size(max = MAX_OWNERS_PER_DESKS_COUNT, message = OWNER_LIST_MAX_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
    @NotNull(message = OWNER_LIST_NOT_NULL_JAVAX_VALIDATION_ERROR_MESSAGE)
    private List<String> ownerIds;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private List<UserRepresentation> owners;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY)
    @Size(max = MAX_ASSOCIATED_DESKS_PER_DESKS_COUNT, message = ASSOCIATED_DESKS_MAX_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
    @NotNull(message = ASSOCIATED_DESKS_LIST_NOT_NULL_JAVAX_VALIDATION_ERROR_MESSAGE)
    private List<String> associatedDeskIds;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private List<DeskRepresentation> associatedDesks;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY)
    @Size(max = MAX_FILTERABLE_METADATA_PER_DESKS_COUNT, message = FILTERABLE_METADATA_MAX_SIZE_ERROR_MESSAGE)
    @NotNull(message = FILTERABLE_METADATA_LIST_NOT_NULL_ERROR_MESSAGE)
    private List<String> filterableMetadataIds;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private List<MetadataRepresentation> filterableMetadata;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY)
    @Size(max = MAX_SUPERVISOR_PER_DESKS_COUNT, message = SUPERVISOR_LIST_MAX_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
    @NotNull(message = SUPERVISOR_LIST_NOT_NULL_JAVAX_VALIDATION_ERROR_MESSAGE)
    private List<String> supervisorIds;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private List<UserRepresentation> supervisors;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY)
    @Size(max = MAX_DELEGATION_MANAGER_PER_DESKS_COUNT, message = DELEGATION_MANAGER_LIST_MAX_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
    @NotNull(message = DELEGATION_MANAGER_LIST_NOT_NULL_JAVAX_VALIDATION_ERROR_MESSAGE)
    private List<String> delegationManagerIds;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private List<UserRepresentation> delegationManagers;

}
