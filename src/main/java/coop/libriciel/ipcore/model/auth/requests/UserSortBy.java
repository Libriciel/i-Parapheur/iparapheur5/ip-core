/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.auth.requests;

import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;
import io.swagger.v3.oas.annotations.media.Schema;


@Schema(enumAsRef = true)
public enum UserSortBy {


    @JsonEnumDefaultValue
    USERNAME,
    ID,
    FIRST_NAME,
    LAST_NAME,
    EMAIL;


    public static class Constants {

        public static final String LAST_NAME_VALUE = "LAST_NAME";
        public static final String API_DOC_SORT_BY_VALUES = "Sorting properties are restricted to the UserSortBy enum values";


        private Constants() {
            throw new IllegalStateException("Utility class");
        }

    }


}
