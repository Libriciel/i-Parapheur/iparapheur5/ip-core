/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.auth;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import coop.libriciel.ipcore.model.workflow.DelegationRule;
import coop.libriciel.ipcore.services.auth.KeycloakService;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jooq.Record2;
import org.jooq.Record6;
import org.keycloak.representations.idm.RoleRepresentation;

import jakarta.persistence.Transient;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static coop.libriciel.ipcore.utils.TextUtils.INTERNAL_PREFIX;


@Data
@Log4j2
@NoArgsConstructor
@AllArgsConstructor
public class Desk {

    public static final String API_FOLDER_FILTER = "Folder filter";
    public static final String ATTRIBUTE_SHORT_NAME = INTERNAL_PREFIX + "short_name";
    public static final String ATTRIBUTE_DESCRIPTION = INTERNAL_PREFIX + "description";
    public static final String ATTRIBUTE_PARENT_DESK_ID = INTERNAL_PREFIX + "parent_desk_id";
    public static final String ATTRIBUTE_TYPOLOGY_DELEGATIONS = INTERNAL_PREFIX + "typology_delegations";

    public static final String SQL_LEVEL = "level";
    public static final String SQL_DIRECT_CHILDREN_COUNT = "direct_children_count";
    public static final String SQL_PARENT_NAMES_CHAIN = "path_info_names";
    public static final String SQL_PARENT_IDS_CHAIN = "path_info_ids";

    public static final String SQL_ID = "id";
    public static final String SQL_NAME = "description";

    protected String id;
    protected String name;
    private String shortName;
    private String description;
    private String tenantId;
    private Integer level;
    private Integer directChildrenCount;
    private DeskRepresentation parentDesk;
    private List<String> parentIdsChain;
    private List<String> parentNamesChain;
    private @Transient boolean folderCreationAllowed = true;
    private @Transient boolean actionAllowed = true;
    private @Transient boolean archivingAllowed = true;
    private @Transient boolean chainAllowed = true;
    protected int draftFoldersCount = -1;
    protected int rejectedFoldersCount = -1;
    protected int pendingFoldersCount = -1;
    protected int finishedFoldersCount = -1;
    protected int lateFoldersCount = -1;
    protected @Transient List<User> owners = null;
    protected @Transient List<String> filterableMetadataIds = null;
    protected @Transient List<String> supervisorIds = null;
    protected @Transient List<DelegationRule> delegationRules = new ArrayList<>();
    protected @Transient List<Desk> delegatingDesks = new ArrayList<>();
    protected @Transient List<String> associatedDeskIds = new ArrayList<>();
    protected @Transient List<String> delegationManagerIds = null;


    public Desk(@NotNull String id) {
        this.id = id;
    }


    public Desk(@NotNull String id, @Nullable String name) {
        this.id = id;
        this.name = name;
    }


    public Desk(@NotNull DeskRepresentation representation) {
        this(representation.getId(), representation.getName());
    }


    public Desk(@NotNull Record2<String, String> record) {
        this(record.get(SQL_ID, String.class), record.get(SQL_NAME, String.class));
        this.description = null;
        this.level = -1;
        this.directChildrenCount = null;
        this.parentIdsChain = null;
    }


    public Desk(@NotNull Record6<String, String, Integer, Integer, String[], String[]> record) {
        this(record.get(SQL_ID, String.class), record.get(SQL_NAME, String.class));
        this.description = null;
        this.level = record.get(SQL_LEVEL, Integer.class);
        this.directChildrenCount = record.get(SQL_DIRECT_CHILDREN_COUNT, Integer.class);
        this.parentNamesChain = Arrays.asList(record.get(SQL_PARENT_NAMES_CHAIN, String[].class));
        this.parentIdsChain = Arrays.asList(record.get(SQL_PARENT_IDS_CHAIN, String[].class));
    }


    public Desk(@NotNull RoleRepresentation rr) {
        this(rr.getId(), rr.getDescription());
        this.shortName = KeycloakService.getAttribute(rr.getAttributes(), ATTRIBUTE_SHORT_NAME);
        this.description = KeycloakService.getAttribute(rr.getAttributes(), ATTRIBUTE_DESCRIPTION);
        this.level = null;
        this.parentNamesChain = null;
        this.parentIdsChain = null;

        Optional.ofNullable(KeycloakService.getAttribute(rr.getAttributes(), ATTRIBUTE_TYPOLOGY_DELEGATIONS))
                .ifPresent(s -> {
                    try {
                        delegationRules = new ObjectMapper().readValue(s, new TypeReference<>() {});
                    } catch (JsonProcessingException e) {
                        log.warn("Error parsing Desk metadata delegation attribute", e);
                    }
                });
    }


    @Builder
    public Desk(@NotNull String id, String name, String shortName, String description, String tenantId, int level, int directChildrenCount,
                DeskRepresentation directParent, List<Desk> delegatingDesks, List<String> parentIdsChain, List<String> parentNamesChain,
                List<DelegationRule> delegationRules) {
        this(id, name);
        this.shortName = shortName;
        this.tenantId = tenantId;
        this.description = description;
        this.level = level;
        this.directChildrenCount = directChildrenCount;
        this.parentDesk = directParent;
        this.delegatingDesks = delegatingDesks;
        this.parentIdsChain = parentIdsChain;
        this.parentNamesChain = parentNamesChain;
        this.delegationRules = Optional.ofNullable(delegationRules).orElse(new ArrayList<>());
    }


}
