/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.auth.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import coop.libriciel.ipcore.model.auth.UserPrivilege;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

import static coop.libriciel.ipcore.utils.ApiUtils.JAVAX_MESSAGE_PREFIX;
import static coop.libriciel.ipcore.utils.ApiUtils.JAVAX_MESSAGE_SUFFIX;


@Data
public class UserRepresentation {


    public static final String API_PATH = "userId";
    public static final String API_DOC_ID_VALUE = "User id";

    /**
     * Keycloak actually accepts every field up to 255 chars,
     * but on the username, we want a margin for the tenant name.
     * ... And for every other field, we want a regular size anyway.
     */

    static final int USERNAME_MIN_LENGTH = 1;
    static final int USERNAME_MAX_LENGTH = 128;
    static final String USERNAME_SIZE_ERROR_MESSAGE = "message.username_should_be_between_2_and_128_chars";
    static final String USERNAME_JAVAX_VALIDATION_ERROR_MESSAGE = JAVAX_MESSAGE_PREFIX + USERNAME_SIZE_ERROR_MESSAGE + JAVAX_MESSAGE_SUFFIX;

    static final int FIRST_NAME_MAX_LENGTH = 128;
    static final String FIRST_NAME_SIZE_ERROR_MESSAGE = "message.first_name_should_be_under_128_chars";
    static final String FIRST_NAME_JAVAX_VALIDATION_ERROR_MESSAGE = JAVAX_MESSAGE_PREFIX + FIRST_NAME_SIZE_ERROR_MESSAGE + JAVAX_MESSAGE_SUFFIX;

    static final int LAST_NAME_MAX_LENGTH = 128;
    static final String LAST_NAME_SIZE_ERROR_MESSAGE = "message.last_name_should_be_under_128_chars";
    static final String LAST_NAME_JAVAX_VALIDATION_ERROR_MESSAGE = JAVAX_MESSAGE_PREFIX + LAST_NAME_SIZE_ERROR_MESSAGE + JAVAX_MESSAGE_SUFFIX;


    public UserRepresentation() {}


    public UserRepresentation(String id) {
        this.id = id;
    }


    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private String id;

    @Size(min = USERNAME_MIN_LENGTH, max = USERNAME_MAX_LENGTH, message = USERNAME_JAVAX_VALIDATION_ERROR_MESSAGE)
    @NotNull(message = USERNAME_JAVAX_VALIDATION_ERROR_MESSAGE)
    private String userName;

    @Size(max = FIRST_NAME_MAX_LENGTH, message = FIRST_NAME_JAVAX_VALIDATION_ERROR_MESSAGE)
    @NotNull(message = FIRST_NAME_JAVAX_VALIDATION_ERROR_MESSAGE)
    private String firstName;

    @Size(max = LAST_NAME_MAX_LENGTH, message = LAST_NAME_JAVAX_VALIDATION_ERROR_MESSAGE)
    @NotNull(message = LAST_NAME_JAVAX_VALIDATION_ERROR_MESSAGE)
    private String lastName;

    @Deprecated(forRemoval = true, since = "5.2")
    private String email;

    @Deprecated(forRemoval = true, since = "5.2")
    private UserPrivilege privilege;

    @Deprecated(forRemoval = true, since = "5.2")
    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY, deprecated = true)
    private Boolean isLdapSynchronized;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private Boolean isFromIdentityProvider;


}
