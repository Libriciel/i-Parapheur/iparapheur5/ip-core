/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.auth.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.TenantRepresentation;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import lombok.Data;
import lombok.EqualsAndHashCode;

import jakarta.validation.constraints.Size;

import java.util.List;

import static coop.libriciel.ipcore.utils.ApiUtils.JAVAX_MESSAGE_PREFIX;
import static coop.libriciel.ipcore.utils.ApiUtils.JAVAX_MESSAGE_SUFFIX;


@Data
@EqualsAndHashCode(callSuper = true)
public class UserDto extends ListableUser {


    private static final int COMPLEMENTARY_FIELD_MAX_LENGTH = 255;
    private static final String COMPLEMENTARY_FIELD_SIZE_ERROR_MESSAGE = "message.complementary_field_should_be_between_0_and_255_chars";
    private static final String COMPLEMENTARY_FIELD_JAVAX_VALIDATION_ERROR_MESSAGE =
            JAVAX_MESSAGE_PREFIX + COMPLEMENTARY_FIELD_SIZE_ERROR_MESSAGE + JAVAX_MESSAGE_SUFFIX;


    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private List<TenantRepresentation> associatedTenants;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY)
    private List<String> associatedTenantIds;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private List<TenantRepresentation> administeredTenants;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY)
    private List<String> administeredTenantIds;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY, nullable = true)
    private List<DeskRepresentation> administeredDesks;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY, nullable = true)
    private List<String> administeredDeskIds;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY, nullable = true)
    private List<DeskRepresentation> associatedDesks;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY, nullable = true)
    private List<String> associatedDeskIds;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY, nullable = true)
    private List<DeskRepresentation> supervisedDesks;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY, nullable = true)
    private List<String> supervisedDeskIds;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY, nullable = true)
    private List<DeskRepresentation> delegationManagedDesks;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY, nullable = true)
    private List<String> delegationManagedDeskIds;

    @Size(max = COMPLEMENTARY_FIELD_MAX_LENGTH, message = COMPLEMENTARY_FIELD_JAVAX_VALIDATION_ERROR_MESSAGE)
    private String complementaryField;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY)
    private boolean resetPasswordRequired;

    @Deprecated
    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY)
    private String password;


}
