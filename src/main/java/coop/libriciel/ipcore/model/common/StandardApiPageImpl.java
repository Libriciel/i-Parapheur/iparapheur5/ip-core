/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.common;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PagedModel;
import org.springframework.util.Assert;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class StandardApiPageImpl<T> implements  Serializable {

    @Serial private static final long serialVersionUID = 867755909294341234L;

    private final List<T> content = new ArrayList<>();

    private boolean empty;
    private boolean first;
    private boolean last;

    private int number;
    private int numberOfElements;
    private int size;
    private long totalElements;
    private int totalPages;

    public record StandardApiSort (boolean empty, boolean sorted, boolean unsorted) implements Serializable {}
    private StandardApiSort sort;


    @Data
    @NoArgsConstructor
    public static class StandardApiPageable implements Serializable {

        @Serial private static final long serialVersionUID = -1337237589829734290L;
        private long offset;
        private long pageNumber;
        private long pageSize;
        private boolean paged = true;
        private boolean unpaged = false;
        private StandardApiSort sort = new StandardApiSort(false, true, false);

        StandardApiPageable(Pageable pageable) {
            this.offset = pageable.getOffset();
            this.pageNumber = pageable.getPageNumber();
            this.pageSize = pageable.getPageSize();
            this.paged = pageable.isPaged();
            this.unpaged = pageable.isUnpaged();
            Sort s = pageable.getSort();
            this.sort = new StandardApiSort(s.isEmpty(), s.isSorted(), s.isUnsorted());
        }
    }

    private StandardApiPageable pageable;


    public StandardApiPageImpl(List<T> content, Pageable pageable, long total) {

        Assert.notNull(content, "Content must not be null");
        Assert.notNull(pageable, "Pageable must not be null");

        this.content.addAll(content);
        this.pageable = new StandardApiPageable(pageable);
        this.sort = this.pageable.getSort();
        this.empty = content.isEmpty();
        this.first =  pageable.getPageNumber() == 0;
        this.number = pageable.getPageNumber();
        this.size = pageable.getPageSize();
        this.numberOfElements = content.size();

        this.totalElements = pageable.toOptional().filter(it -> !content.isEmpty())//
                .filter(it -> it.getOffset() + it.getPageSize() > total)//
                .map(it -> it.getOffset() + content.size())//
                .orElse(total);

        this.totalPages = (int)(totalElements / pageable.getPageSize()) + (totalElements % pageable.getPageSize() != 0 ? 1 : 0);
        this.last = pageable.getPageNumber() >= (totalPages - 1) || totalElements == 0;
    }

    public StandardApiPageImpl(List<T> content, Pageable pageable) {
        this(content, pageable, content.size());
    }


    public StandardApiPageImpl(PagedModel<T> inputPageModel, Pageable pageable) {
        Assert.notNull(inputPageModel, "PagedModel must not be null");
        Assert.notNull(pageable, "Pageable must not be null");
        Assert.notNull(inputPageModel.getMetadata(), "PagedModel's page must not be null");

        this.pageable = new StandardApiPageable(pageable);
        this.sort = this.pageable.getSort();
        this.content.addAll(inputPageModel.getContent());

        PagedModel.PageMetadata pageInfo = inputPageModel.getMetadata();
        this.number = (int)pageInfo.number();
        this.size = (int)pageInfo.size();
        this.totalElements = pageInfo.totalElements();
        this.totalPages = (int)pageInfo.totalPages();

        this.numberOfElements = content.size();
        this.empty = content.isEmpty();
        this.first =  pageable.getPageNumber() == 0;
        this.last = pageable.getPageNumber() == totalPages - 1;

    }

    public StandardApiPageImpl(Page<T> inputPage) {
        Assert.isTrue(inputPage instanceof PageImpl, "Page must be an instance of PageImpl");
        PageImpl<T> inputPageImpl = (PageImpl<T>) inputPage;

        Assert.notNull(inputPageImpl, "PageImpl must not be null");
        Assert.notNull(inputPageImpl.getPageable(), "PageImpl's pageable must not be null");

        this.pageable = new StandardApiPageable(inputPageImpl.getPageable());
        this.sort = this.pageable.getSort();
        this.content.addAll(inputPageImpl.getContent());


        this.number = inputPageImpl.getNumber();
        this.size = inputPageImpl.getSize();
        this.totalElements = inputPageImpl.getTotalElements();
        this.totalPages = inputPageImpl.getTotalPages();

        this.numberOfElements = inputPageImpl.getNumberOfElements();
        this.empty = inputPageImpl.isEmpty();
        this.first =  inputPageImpl.isFirst();
        this.last = inputPageImpl.isLast();
    }

}
