/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.content;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import coop.libriciel.ipcore.model.crypto.PdfSignaturePosition;
import coop.libriciel.ipcore.model.pdfstamp.SignaturePlacement;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;


@Data
@Log4j2
@Builder
@AllArgsConstructor
// FIXME : This is the real inner object, temporarily named DTO to clean the API.
//  We shall create a real inner object, with the same parameters, and split the logic.
@Schema(name = "DocumentDto")
@JsonIgnoreProperties("mainDocument")
public class Document {

    public static final String API_DOC_ID_VALUE = "Document Id";


    private String id;
    private String name;
    private int index;
    private int pageCount;
    private long contentLength;
    private MediaType mediaType;
    private @JsonIgnore Float mediaVersion;
    private Map<String, PageInfo> pagesProperties;
    private @JsonProperty("deletable") boolean deletable = true;
    private @JsonProperty("isMainDocument") boolean isMainDocument;
    private @JsonIgnore Boolean passwordProtected;
    private boolean isSignatureProof = false;
    private String pdfVisualId;
    private @JsonIgnore String checksumValue;
    private @Builder.Default List<SignaturePlacement> signaturePlacementAnnotations = new ArrayList<>();
    private @Builder.Default Map<Integer, PdfSignaturePosition> signatureTags = new HashMap<>();
    private @Builder.Default Map<Integer, PdfSignaturePosition> sealTags = new HashMap<>();
    private @Builder.Default List<DetachedSignature> detachedSignatures = new ArrayList<>();
    private @Builder.Default List<ValidatedSignatureInformation> embeddedSignatureInfos = null;


    public Document() {
        initializeInnerCollections();
    }


    public Document(@NotNull MultipartFile multipartFile, boolean isMainDocument, int index) {

        initializeInnerCollections();

        this.name = multipartFile.getOriginalFilename();
        this.index = index;
        this.isMainDocument = isMainDocument;
        this.contentLength = multipartFile.getSize();

        this.mediaType = Optional.ofNullable(multipartFile.getContentType())
                .map(MediaType::parseMediaType)
                .orElse(null); // TODO : A fallback guessed with the file extension ?
    }


    public Document(@NotNull MediatypeFileResource fileInputStream, boolean isMainDocument, int index) {

        this.name = fileInputStream.getName();
        this.index = index;
        this.isMainDocument = isMainDocument;
        this.contentLength = fileInputStream.getSize();

        this.mediaType = fileInputStream.getMediaType();
    }


    // FIXME: This should be deleted!
    //  An empty list has a different meaning than a null one.
    //  We rely on that difference everywhere else in the code, thus we should not initialize empty lists.
    private void initializeInnerCollections() {
        signaturePlacementAnnotations = new ArrayList<>();
        signatureTags = new HashMap<>();
        sealTags = new HashMap<>();
        detachedSignatures = new ArrayList<>();
    }
}
