/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.content.alfresco;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;


@Data
@JsonIgnoreProperties("isMainDocument")
public class AlfrescoProperties {


    public static final String PROPERTY_TITLE = "cm:title";
    public static final String PROPERTY_AUTHOR = "cm:author";
    public static final String PROPERTY_INDEX = "iparapheur:index";
    public static final String PROPERTY_IS_MAIN_DOCUMENT = "iparapheur:isMainDocument";
    public static final String PROPERTY_TARGET_DOCUMENT_ID = "iparapheur:targetDocumentId";
    public static final String PROPERTY_TARGET_FOLDER_ID = "iparapheur:targetFolderId";
    public static final String PROPERTY_SOURCE_DOCUMENT_ID = "iparapheur:sourceDocumentId";
    public static final String PROPERTY_TARGET_TASK_ID = "iparapheur:targetTaskId";
    public static final String PROPERTY_CHECKSUM_ALGORITHM = "iparapheur:checksumAlgorithm";
    public static final String PROPERTY_CHECKSUM_VALUE = "iparapheur:sha256Checksum";
    public static final String PROPERTY_MEDIA_VERSION = "iparapheur:mediaVersion";
    public static final String PROPERTY_ANNOTATIONS = "iparapheur:annotations";
    public static final String PROPERTY_DELETABLE = "iparapheur:deletable";
    public static final String PROPERTY_SIGNATURE_TAGS = "iparapheur:signatureTags";
    public static final String PROPERTY_SEAL_TAGS = "iparapheur:sealTags";
    public static final String PROPERTY_PROCEDURE_ID = "iparapheur:procedureId";

    private @JsonProperty(PROPERTY_INDEX) Integer index;
    private @JsonProperty(PROPERTY_TITLE) String title;
    private @JsonProperty(PROPERTY_AUTHOR) String author;
    private @JsonProperty(PROPERTY_IS_MAIN_DOCUMENT) Boolean isMainDocument;
    private @JsonProperty(PROPERTY_TARGET_DOCUMENT_ID) String targetDocumentId;
    private @JsonProperty(PROPERTY_TARGET_TASK_ID) String targetTaskId;
    private @JsonProperty(PROPERTY_CHECKSUM_ALGORITHM) String checksumAlgorithm;
    private @JsonProperty(PROPERTY_CHECKSUM_VALUE) String checksumValue;
    private @JsonProperty(PROPERTY_MEDIA_VERSION) String mediaVersion;
    private @JsonProperty(PROPERTY_DELETABLE) Boolean deletable;
    private @JsonProperty(PROPERTY_ANNOTATIONS) String annotations;
    private @JsonProperty(PROPERTY_SIGNATURE_TAGS) String signatureTags;
    private @JsonProperty(PROPERTY_SEAL_TAGS) String sealTags;
    private @JsonProperty(PROPERTY_SOURCE_DOCUMENT_ID) String sourceDocumentId;


}
