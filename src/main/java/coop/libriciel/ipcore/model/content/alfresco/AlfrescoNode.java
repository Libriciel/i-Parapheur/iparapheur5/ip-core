/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.content.alfresco;

import coop.libriciel.alfresco.core.model.ContentInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AlfrescoNode {

    public static final String NODE_TYPE_NATIVE_FOLDER = "cm:folder";
    public static final String NODE_TYPE_NATIVE_CONTENT = "cm:content";
    public static final String NODE_TYPE_IP_FOLDER = "iparapheur:folder";
    public static final String NODE_TYPE_IP_DOCUMENT = "iparapheur:document";
    public static final String NODE_TYPE_IP_DETACHED_SIGNATURE = "iparapheur:detachedSignature";
    public static final String NODE_TYPE_IP_SIGNATURE_PROOF = "iparapheur:signatureProof";
    public static final String NODE_TYPE_IP_PDF_VISUAL = "iparapheur:pdfVisual";


    private String id;
    private String name;
    private ContentInfo content;
    private Map<String, Object> properties;

}
