/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.content;

import lombok.Getter;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;

import java.io.InputStream;

@Getter
public class MediatypeFileResource extends InputStreamResource {

    private final MediaType mediaType;
    private final int size;
    private final String name;


    public MediatypeFileResource(InputStream inputStream, String description, MediaType mediaType, int size, String name) {
        super(inputStream, description);
        this.mediaType = mediaType;
        this.size = size;
        this.name = name;
    }
}
