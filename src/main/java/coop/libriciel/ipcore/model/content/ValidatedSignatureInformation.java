/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.content;

import coop.libriciel.iparapheur.customalfrescotengines.model.crypto.PdfSignaturePosition;
import coop.libriciel.iparapheur.customalfrescotengines.model.crypto.SignatureInformation;
import lombok.*;


@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ValidatedSignatureInformation extends SignatureInformation {

    private String eidasLevel;
    private String error;


    public ValidatedSignatureInformation(Long signatureDateTime,
                                         PdfSignaturePosition position,
                                         String principalIssuer,
                                         String principalSubjectIssuer,
                                         Boolean isSignatureValid,
                                         Long certificateBeginDate,
                                         Long certificateEndDate,
                                         String eidasLevel,
                                         String error) {
        super(signatureDateTime, position, principalIssuer, principalSubjectIssuer, isSignatureValid, certificateBeginDate, certificateEndDate);
        this.eidasLevel = eidasLevel;
        this.error = error;
    }

}
