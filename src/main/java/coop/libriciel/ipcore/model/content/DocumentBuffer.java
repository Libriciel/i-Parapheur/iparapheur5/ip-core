/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.content;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.web.multipart.MultipartFile;
import reactor.core.publisher.Flux;


@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class DocumentBuffer extends Document {


    private Flux<DataBuffer> contentFlux;


    public DocumentBuffer(@NotNull Document document) {
        super(
                document.getId(), document.getName(), document.getIndex(), document.getPageCount(), document.getContentLength(),
                document.getMediaType(), document.getMediaVersion(), document.getPagesProperties(), document.isDeletable(),
                document.isMainDocument(), document.getPasswordProtected(), document.isSignatureProof(), document.getPdfVisualId(), document.getChecksumValue(),
                document.getSignaturePlacementAnnotations(), document.getSignatureTags(), document.getSealTags(),
                document.getDetachedSignatures(), document.getEmbeddedSignatureInfos()
        );
    }


    public DocumentBuffer(@NotNull MultipartFile file, boolean isMainDocument, int index) {
        super(file, isMainDocument, index);
    }


    public DocumentBuffer(@NotNull MediatypeFileResource file, boolean isMainDocument, int index) {
        super(file, isMainDocument, index);
    }


}
