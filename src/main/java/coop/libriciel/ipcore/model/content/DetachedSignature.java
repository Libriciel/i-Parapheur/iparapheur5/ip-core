/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.content;

import coop.libriciel.alfresco.core.model.NodeChildAssociation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.collections4.MapUtils;
import org.springframework.http.MediaType;

import java.util.Map;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DetachedSignature {

    
    private static final String PROPERTY_TARGET_DOCUMENT_ID = "iparapheur:targetDocumentId";


    private String id;
    private String name;
    private long contentLength;
    private MediaType mediaType;
    private String targetDocumentId;
    private String targetTaskId;
    private ValidatedSignatureInformation signatureInfo;


    public DetachedSignature(NodeChildAssociation alfrescoNode) {
        id = alfrescoNode.getId();
        name = alfrescoNode.getName();
        contentLength = alfrescoNode.getContent().getSizeInBytes();
        mediaType = MediaType.parseMediaType(alfrescoNode.getContent().getMimeType());

        //noinspection unchecked
        Map<String, Object> properties = (Map<String, Object>) alfrescoNode.getProperties();
        targetDocumentId = MapUtils.getString(properties, PROPERTY_TARGET_DOCUMENT_ID);
        signatureInfo = new ValidatedSignatureInformation();
    }


}
