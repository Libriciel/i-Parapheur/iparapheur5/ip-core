/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.pdfstamp;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.TextUtils.NO_FORBIDDEN_CHARACTERS_REGEXP;
import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;


@Data
@NoArgsConstructor
public class LayerRepresentation {


    @Schema(accessMode = READ_ONLY)
    private String id;

    @Size(min = NAME_MIN_LENGTH, max = NAME_MAX_LENGTH, message = NAME_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
    @jakarta.validation.constraints.NotNull(message = NAME_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
    @Pattern(regexp = NO_FORBIDDEN_CHARACTERS_REGEXP, message = NAME_CONTAINS_FORBIDDEN_CHARACTER_JAVAX_VALIDATION_ERROR_MESSAGE)
    private String name;


    /**
     * This constructor seems unused,
     * but it is actually needed by the JPA engine to convert an entity to a representation
     *
     * @param layer the source entity to copy
     */
    @Builder
    public LayerRepresentation(@NotNull Layer layer) {
        this.id = layer.getId();
        this.name = layer.getName();
    }


}
