/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.pdfstamp;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import jakarta.persistence.Column;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;

import static coop.libriciel.ipcore.model.pdfstamp.Origin.TOP_RIGHT;
import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;


@Getter
@Setter
@MappedSuperclass
public abstract class Annotation {

    public static final String COLUMN_ID = "id";

    protected static final String COLUMN_PAGE = "page";
    protected static final String COLUMN_X = "x";
    protected static final String COLUMN_Y = "y";
    protected static final String COLUMN_WIDTH = "width";
    protected static final String COLUMN_HEIGHT = "height";
    protected static final String COLUMN_PAGE_ROTATION = "page_rotation";
    protected static final String COLUMN_PAGE_WIDTH = "page_width";
    protected static final String COLUMN_PAGE_HEIGHT = "page_height";
    protected static final String COLUMN_PAGE_MARGIN_X = "page_margin_x";
    protected static final String COLUMN_PAGE_MARGIN_Y = "page_margin_y";
    protected static final String COLUMN_RECTANGLE_ORIGIN = "rectangle_origin";


    @Schema(accessMode = READ_ONLY)
    @Column(name = COLUMN_ID)
    protected @Id String id;

    @Schema(example = "1")
    @Column(name = COLUMN_PAGE)
    protected int page = -1;

    @Schema(example = "15")
    @Column(name = COLUMN_WIDTH)
    protected int width;

    @Schema(example = "15")
    @Column(name = COLUMN_HEIGHT)
    protected int height;

    @Column(name = COLUMN_X)
    protected int x;

    @Column(name = COLUMN_Y)
    protected int y;

    @Schema(example = "0")
    @Column(name = COLUMN_PAGE_ROTATION)
    protected int pageRotation = 0;

    @Schema(example = "276")
    @Column(name = COLUMN_PAGE_WIDTH)
    protected Integer pageWidth;

    @Schema(example = "308")
    @Column(name = COLUMN_PAGE_HEIGHT)
    protected Integer pageHeight;

    @Schema(example = "TOP_RIGHT")
    @Column(name = COLUMN_RECTANGLE_ORIGIN)
    protected Origin rectangleOrigin = TOP_RIGHT;


}
