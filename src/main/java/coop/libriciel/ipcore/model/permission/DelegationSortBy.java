/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.permission;

import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.requests.SubtypeRepresentation;
import coop.libriciel.ipcore.model.database.requests.TypeRepresentation;
import io.swagger.v3.oas.annotations.media.Schema;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;
import java.util.Optional;
import java.util.function.Function;

import static java.util.Comparator.*;


@Schema(enumAsRef = true)
public enum DelegationSortBy {


    @JsonEnumDefaultValue
    SUBSTITUTE_DESK,
    START,
    END,
    TYPOLOGY;


    public static class Constants {

        public static final String SUBSTITUTE_DESK_VALUE = "SUBSTITUTE_DESK";
        public static final String API_DOC_SORT_BY_VALUES = "Sorting properties are restricted to the DelegationSortBy enum values";


        private Constants() {
            throw new IllegalStateException("Utility class");
        }

    }


    public static Comparator<DelegationDto> generateDelegationDtoComparator(@NotNull DelegationSortBy sortBy, boolean asc) {
        return switch (sortBy) {
            case SUBSTITUTE_DESK -> comparing(
                    (Function<DelegationDto, String>) dto -> Optional.ofNullable(dto.getDelegatingDesk()).map(DeskRepresentation::getName).orElse(null),
                    asc ? nullsLast(naturalOrder()) : nullsFirst(reverseOrder())
            );
            case START -> comparing(DelegationDto::getStart, asc ? nullsLast(naturalOrder()) : nullsFirst(reverseOrder()));
            case END -> comparing(DelegationDto::getEnd, asc ? nullsLast(naturalOrder()) : nullsFirst(reverseOrder()));
            case TYPOLOGY -> comparing(
                    (Function<DelegationDto, String>) dto -> Optional.ofNullable(dto.getType()).map(TypeRepresentation::getName).orElse(null),
                    asc ? nullsLast(naturalOrder()) : nullsFirst(reverseOrder())
            ).thenComparing(
                    (Function<DelegationDto, String>) dto -> Optional.ofNullable(dto.getSubtype()).map(SubtypeRepresentation::getName).orElse(null),
                    asc ? nullsLast(naturalOrder()) : nullsFirst(reverseOrder())
            );
        };
    }


}
