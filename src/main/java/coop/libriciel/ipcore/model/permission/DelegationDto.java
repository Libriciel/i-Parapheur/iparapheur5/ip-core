/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.permission;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.requests.SubtypeRepresentation;
import coop.libriciel.ipcore.model.database.requests.TypeRepresentation;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DelegationDto {


    public static final String API_DOC_ID_VALUE = "Delegation Id";
    public static final String API_PATH = "delegationId";
    public static final String API_DOC = "target substitute desk, optional type/subtype; and schedule containing Dates and actions to perform.";


    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private String id;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY)
    private String substituteDeskId;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private DeskRepresentation substituteDesk;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY)
    private String delegatingDeskId;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private DeskRepresentation delegatingDesk;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY, nullable = true)
    private String typeId;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY, nullable = true)
    private TypeRepresentation type;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY, nullable = true)
    private String subtypeId;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY, nullable = true)
    private SubtypeRepresentation subtype;

    @Schema(nullable = true)
    private Date start;

    @Schema(nullable = true)
    private Date end;


}
