/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.mail;

import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.securemail.MailParams;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

import static java.util.Collections.singletonList;
import static org.apache.commons.lang3.StringUtils.firstNonEmpty;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MailTarget {


    private String replyTo;
    private List<String> to;
    private List<String> cc;
    private List<String> bcc;


    public MailTarget(@NotNull MailParams mailParams) {
        this.replyTo = mailParams.getFrom();
        this.to = mailParams.getTo();
        this.cc = mailParams.getCc();
        this.bcc = mailParams.getBcc();
    }


    // <editor-fold desc="Utils">


    public static List<MailTarget> fromUserList(@NotNull List<User> usersToNotify, @Nullable String previousUserMail) {
        return usersToNotify.stream()
                .map(u -> firstNonEmpty(u.getNotificationsRedirectionMail(), u.getEmail()))
                .map(m -> {
                    MailTarget target = new MailTarget();
                    target.setReplyTo(previousUserMail);
                    target.setTo(singletonList(m));
                    return target;
                })
                .toList();
    }


    // </editor-fold desc="Utils">


}
