/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.mail;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import coop.libriciel.ipcore.model.workflow.Action;
import coop.libriciel.ipcore.model.workflow.NotificationType;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;


/**
 * This object is usually stored in Redis.
 * We don't want to store any data here that could be changed between the trigger and the actual send.
 */
@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class MailNotification {

    private Action taskAction;
    private NotificationType notificationType;
    private String taskState;
    private String tenantId;
    private String instanceId;
    private String instanceName;
    private String taskId;
    private String taskUserId;
    private String taskUserFirstName;
    private String taskUserLastName;

    private Set<String> deskIdsToNotify = new HashSet<>();
    private Set<String> additionalDeskIdsToNotify = new HashSet<>();

    private String previousGroup;
    private String previousAction;

    private boolean asFollower = false;
    private boolean byDelegation = false;
    private String delegatingDeskId;


    public MailNotification(MailNotification other) {
        this.taskAction = other.taskAction;
        this.notificationType = other.notificationType;
        this.taskState = other.taskState;
        this.tenantId = other.tenantId;
        this.instanceId = other.instanceId;
        this.instanceName = other.instanceName;
        this.taskId = other.taskId;
        this.previousGroup = other.previousGroup;
        this.previousAction = other.previousAction;
        this.asFollower = other.asFollower;
        this.byDelegation = other.byDelegation;
        this.delegatingDeskId = other.delegatingDeskId;
        this.taskUserId = other.taskUserId;
        this.taskUserFirstName = other.taskUserFirstName;
        this.taskUserLastName = other.taskUserLastName;

        this.deskIdsToNotify.addAll(other.deskIdsToNotify);
        this.additionalDeskIdsToNotify.addAll(other.additionalDeskIdsToNotify);
    }

}
