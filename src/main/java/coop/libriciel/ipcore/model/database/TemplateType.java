/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.database;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;


@Schema(enumAsRef = true)
public enum TemplateType {

    // <editor-fold desc="Mail templates">

    MAIL_NOTIFICATION_SINGLE,
    MAIL_NOTIFICATION_DIGEST,
    MAIL_ACTION_SEND,

    // </editor-fold desc="Mail templates">

    // <editor-fold desc="Signature Templates">

    SIGNATURE_SMALL,
    SIGNATURE_MEDIUM,
    SIGNATURE_LARGE,
    SIGNATURE_ALTERNATE_1,
    SIGNATURE_ALTERNATE_2,
    SEAL_AUTOMATIC,
    SEAL_MEDIUM,
    SEAL_LARGE,
    SEAL_ALTERNATE,

    // </editor-fold desc="Signature Templates">

    DOCKET;


    public boolean isSignatureTemplate() {
        return List.of(SIGNATURE_SMALL, SIGNATURE_MEDIUM, SIGNATURE_LARGE, SIGNATURE_ALTERNATE_1, SIGNATURE_ALTERNATE_2).contains(this);
    }

}

