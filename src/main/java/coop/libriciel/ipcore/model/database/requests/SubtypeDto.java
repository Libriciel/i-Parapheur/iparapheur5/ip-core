/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.database.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.crypto.SealCertificateRepresentation;
import coop.libriciel.ipcore.model.database.ExternalSignatureConfigRepresentation;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import jakarta.validation.constraints.Size;

import java.util.List;

import static coop.libriciel.ipcore.utils.ApiUtils.*;


@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class SubtypeDto extends SubtypeRepresentation {


    public static final int WORKFLOW_SELECTION_SCRIPT_DEFAULT_MAX_SIZE = 8192;
    public static final int WORKFLOW_SELECTION_SCRIPT_FULL_MAX_SIZE = 65535;


    @Size(min = DESCRIPTION_MIN_LENGTH, max = DESCRIPTION_MAX_LENGTH, message = DESCRIPTION_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
    private String description;

    @Schema(nullable = true)
    private String creationWorkflowId;
    private String validationWorkflowId;

    @Schema(nullable = true)
    @Size(max = WORKFLOW_SELECTION_SCRIPT_FULL_MAX_SIZE, message = JAVAX_MESSAGE_PREFIX + "message.invalid_subtype_data_script_too_big" + JAVAX_MESSAGE_SUFFIX)
    private String workflowSelectionScript;

    private boolean isReadingMandatory = false;
    private boolean isDigitalSignatureMandatory = true;
    private boolean isAnnexeIncluded = false;
    private boolean isMultiDocuments = false;
    private boolean annotationsAllowed = false;
    private boolean externalSignatureAutomatic = false;
    private boolean isSealAutomatic = false;

    @Schema(nullable = true)
    private Long secureMailServerId;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY, nullable = true)
    private String sealCertificateId = null;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY, nullable = true)
    private SealCertificateRepresentation sealCertificate = null;

    private List<SubtypeMetadataDto> subtypeMetadataList;

    private List<SubtypeLayerDto> subtypeLayers;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY, nullable = true)
    private String externalSignatureConfigId;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY, nullable = true)
    private ExternalSignatureConfigRepresentation externalSignatureConfig;


    // <editor-fold desc="Non persistent data">


    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY, nullable = true)
    private List<String> creationPermittedDeskIds;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY, nullable = true)
    private List<DeskRepresentation> creationPermittedDesks;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY, nullable = true)
    private List<String> filterableByDeskIds;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY, nullable = true)
    private List<DeskRepresentation> filterableByDesks;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private int maxMainDocuments;


    // </editor-fold desc="Non persistent data">

}
