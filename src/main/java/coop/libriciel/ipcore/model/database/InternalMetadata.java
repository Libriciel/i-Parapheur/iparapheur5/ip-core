/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.database;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.Nullable;

import static coop.libriciel.ipcore.model.database.InternalMetadata.Constants.*;
import static coop.libriciel.ipcore.model.database.MetadataType.DATE;
import static coop.libriciel.ipcore.model.database.MetadataType.TEXT;
import static coop.libriciel.ipcore.utils.TextUtils.INTERNAL_COMPUTED_PREFIX;
import static java.util.Arrays.stream;


@Getter
@AllArgsConstructor
@Schema(enumAsRef = true)
public enum InternalMetadata {


    FOLDER_NAME(FOLDER_NAME_KEY, TEXT),
    FOLDER_TYPE(FOLDER_TYPE_KEY, TEXT),
    FOLDER_SUBTYPE(FOLDER_SUBTYPE_KEY, TEXT),
    LAST_VISA_DESK(LAST_VISA_DESK_KEY, TEXT),
    LAST_VISA_USER(LAST_VISA_USER_KEY, TEXT),
    LAST_VISA_DATE(LAST_VISA_DATE_KEY, DATE),
    LAST_SIGNATURE_DESK(LAST_SIGNATURE_DESK_KEY, TEXT),
    LAST_SIGNATURE_USER(LAST_SIGNATURE_USER_KEY, TEXT),
    LAST_SIGNATURE_DATE(LAST_SIGNATURE_DATE_KEY, DATE),
    LAST_SIGNATURE_DELEGATED_BY(LAST_SIGNATURE_DELEGATED_BY_KEY, TEXT),
    LAST_SIGNATURE_HASH(LAST_SIGNATURE_HASH_KEY, TEXT);


    public static class Constants {

        public static final String FOLDER_NAME_KEY = INTERNAL_COMPUTED_PREFIX + "folder_name";
        public static final String FOLDER_TYPE_KEY = INTERNAL_COMPUTED_PREFIX + "folder_type";
        public static final String FOLDER_SUBTYPE_KEY = INTERNAL_COMPUTED_PREFIX + "folder_subtype";
        public static final String LAST_VISA_DESK_KEY = INTERNAL_COMPUTED_PREFIX + "last_visa_desk";
        public static final String LAST_VISA_USER_KEY = INTERNAL_COMPUTED_PREFIX + "last_visa_user";
        public static final String LAST_VISA_DATE_KEY = INTERNAL_COMPUTED_PREFIX + "last_visa_date";
        public static final String LAST_SIGNATURE_DESK_KEY = INTERNAL_COMPUTED_PREFIX + "last_signature_desk";
        public static final String LAST_SIGNATURE_USER_KEY = INTERNAL_COMPUTED_PREFIX + "last_signature_user";
        public static final String LAST_SIGNATURE_DATE_KEY = INTERNAL_COMPUTED_PREFIX + "last_signature_date";
        public static final String LAST_SIGNATURE_DELEGATED_BY_KEY = INTERNAL_COMPUTED_PREFIX + "last_signature_delegated_by";
        public static final String LAST_SIGNATURE_HASH_KEY = INTERNAL_COMPUTED_PREFIX + "last_signature_hash";


        private Constants() {
            throw new IllegalStateException("Utility class");
        }

    }


    private final String key;
    private final MetadataType type;


    public static @Nullable InternalMetadata fromKey(@Nullable String key) {
        return stream(InternalMetadata.values())
                .filter(internalMetadata -> StringUtils.equals(internalMetadata.getKey(), key))
                .findFirst()
                .orElse(null);
    }


}
