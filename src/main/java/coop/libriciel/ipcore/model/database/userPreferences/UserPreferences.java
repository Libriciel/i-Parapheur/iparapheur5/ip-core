/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.database.userPreferences;

import coop.libriciel.ipcore.model.database.TemplateType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.hibernate.annotations.Fetch;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static coop.libriciel.ipcore.model.database.TemplateType.SEAL_MEDIUM;
import static coop.libriciel.ipcore.model.database.TemplateType.SIGNATURE_MEDIUM;
import static coop.libriciel.ipcore.model.database.userPreferences.FolderFilter.COLUMN_ID;
import static coop.libriciel.ipcore.utils.TextUtils.UUID_STRING_SIZE;
import static jakarta.persistence.CascadeType.ALL;
import static jakarta.persistence.CascadeType.REMOVE;
import static jakarta.persistence.EnumType.STRING;
import static jakarta.persistence.FetchType.EAGER;
import static org.hibernate.annotations.FetchMode.SUBSELECT;


@Getter
@Setter
@Entity
@ToString
@RequiredArgsConstructor
@Table(name = UserPreferences.TABLE_NAME)
public class UserPreferences {


    public static final String TABLE_NAME = "user_preferences";
    public static final String COLUMN_USER_ID = "user_id";
    public static final String COLUMN_SHOW_DELEGATIONS = "show_delegations";
    public static final String COLUMN_SHOW_ADMIN_IDS = "show_admin_ids";
    public static final String COLUMN_TASK_VIEW_DEFAULT_PAGE_SIZE = "task_view_default_page_size";
    public static final String COLUMN_TASK_VIEW_DEFAULT_SORT_BY = "task_view_default_sort_by";
    public static final String COLUMN_TASK_VIEW_DEFAULT_ASC = "task_view_default_asc";
    public static final String COLUMN_ARCHIVE_VIEW_DEFAULT_PAGE_SIZE = "archive_view_default_page_size";
    public static final String TABLE_TASK_VIEW_COLUMNS_NAME = TABLE_NAME + "_task_view_columns";
    public static final String TABLE_ARCHIVES_COLUMNS_NAME = TABLE_NAME + "_archive_view_columns";
    public static final String TABLE_FAVORITE_DESKS = TABLE_NAME + "_favorite_desk_ids";
    public static final String TABLE_FOLDER_VIEW_BLOCKS = TABLE_NAME + "_folder_view_blocks";
    public static final String COLUMN_DEFAULT_SIGNATURE_TEMPLATE = TABLE_NAME + "_default_signature_template";
    public static final String COLUMN_DEFAULT_SEAL_TEMPLATE = TABLE_NAME + "_default_seal_template";
    public static final String COLUMN_FOLDER_VIEW_ORGANIZATION = TABLE_NAME + "_folder_view_organization";
    public static final String COLUMN_TASK_VIEW_ORGANIZATION = TABLE_NAME + "_task_view_organization";
    public static final String COLUMN_ARCHIVE_VIEW_ORGANIZATION = TABLE_NAME + "_archive_view_organization";


    @Id
    @Column(name = COLUMN_USER_ID, length = UUID_STRING_SIZE)
    private String userId;

    @Column(name = COLUMN_SHOW_DELEGATIONS)
    private Boolean showDelegations = true;

    @Column(name = COLUMN_TASK_VIEW_DEFAULT_PAGE_SIZE)
    private Integer taskViewDefaultPageSize = 10;

    @Column(name = COLUMN_ARCHIVE_VIEW_DEFAULT_PAGE_SIZE)
    private Integer archiveViewDefaultPageSize = 10;

    @Column(name = COLUMN_SHOW_ADMIN_IDS)
    private Boolean showAdminIds = false;

    @Column(name = COLUMN_DEFAULT_SIGNATURE_TEMPLATE)
    private TemplateType defaultSignatureTemplate = SIGNATURE_MEDIUM;

    @Column(name = COLUMN_DEFAULT_SEAL_TEMPLATE)
    private TemplateType defaultSealTemplate = SEAL_MEDIUM;

    @OneToOne
    @JoinColumn(name = COLUMN_ID, foreignKey = @ForeignKey(name = "fk_" + COLUMN_ID))
    private FolderFilter currentFilter = new FolderFilter();

    @OneToMany(fetch = EAGER, mappedBy = "userPreferences", cascade = REMOVE, orphanRemoval = true)
    private List<TableLayout> tableLayoutList;

    @ElementCollection(fetch = EAGER, targetClass = String.class)
    @Fetch(value = SUBSELECT)
    @CollectionTable(
            name = TABLE_FAVORITE_DESKS,
            joinColumns = @JoinColumn(name = COLUMN_USER_ID),
            foreignKey = @ForeignKey(name = "fk_" + COLUMN_USER_ID)
    )
    private List<String> favoriteDeskIdList = new ArrayList<>();

    @Deprecated
    @Enumerated(STRING)
    @Column(name = COLUMN_TASK_VIEW_DEFAULT_SORT_BY)
    private TaskViewColumn taskViewDefaultSortBy = TaskViewColumn.FOLDER_NAME;

    @Deprecated
    @Column(name = COLUMN_TASK_VIEW_DEFAULT_ASC)
    private Boolean taskViewDefaultAsc = true;

    @Deprecated
    @ElementCollection(fetch = EAGER, targetClass = FolderViewBlock.class)
    @Fetch(value = SUBSELECT)
    @Enumerated(STRING)
    @CollectionTable(
            name = TABLE_FOLDER_VIEW_BLOCKS,
            joinColumns = @JoinColumn(name = COLUMN_USER_ID),
            foreignKey = @ForeignKey(name = "fk_" + COLUMN_USER_ID)
    )
    private List<FolderViewBlock> folderViewBlockList;

    @Deprecated
    @ElementCollection(fetch = EAGER, targetClass = TaskViewColumn.class)
    @Fetch(value = SUBSELECT)
    @Enumerated(STRING)
    @CollectionTable(
            name = TABLE_TASK_VIEW_COLUMNS_NAME,
            joinColumns = @JoinColumn(name = COLUMN_USER_ID),
            foreignKey = @ForeignKey(name = "fk_" + COLUMN_USER_ID)
    )
    private List<TaskViewColumn> taskViewColumnList;

    @Deprecated
    @ElementCollection(fetch = EAGER, targetClass = ArchiveViewColumn.class)
    @Fetch(value = SUBSELECT)
    @Enumerated(STRING)
    @CollectionTable(
            name = TABLE_ARCHIVES_COLUMNS_NAME,
            joinColumns = @JoinColumn(name = COLUMN_USER_ID),
            foreignKey = @ForeignKey(name = "fk_" + COLUMN_USER_ID)
    )
    private List<ArchiveViewColumn> archiveViewColumnList;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null
                || Hibernate.getClass(this) != Hibernate.getClass(o)
                || this.getClass() != o.getClass()) return false;
        UserPreferences that = (UserPreferences) o;
        return userId != null && Objects.equals(userId, that.userId);
    }


    @Override
    public int hashCode() {
        return getClass().hashCode();
    }


}
