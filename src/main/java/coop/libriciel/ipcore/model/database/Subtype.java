/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
package coop.libriciel.ipcore.model.database;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jooq.Record7;
import org.springframework.beans.factory.annotation.Value;

import java.util.List;

import static jakarta.persistence.CascadeType.REMOVE;
import static jakarta.persistence.FetchType.EAGER;
import static jakarta.persistence.FetchType.LAZY;


@Setter
@Getter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(
        name = Subtype.TABLE_NAME,
        uniqueConstraints = @UniqueConstraint(
                name = TypologyEntity.COLUMN_TENANT_ID + "_" + Subtype.COLUMN_PARENT + "_" + TypologyEntity.COLUMN_NAME + "_unique",
                columnNames = {TypologyEntity.COLUMN_TENANT_ID, Subtype.COLUMN_PARENT, TypologyEntity.COLUMN_NAME}
        )
)
public class Subtype extends TypologyEntity {

    private static final int WORKFLOW_SELECTION_SCRIPT_MAX_FIELD_SIZE = 65535;

    public static final String TABLE_NAME = "subtype";
    public static final String API_PATH = "subtypeId";
    public static final String API_DOC_ID_VALUE = "Subtype id";
    public static final String API_DOC_NAME_VALUE = "Subtype name";
    public static final String COLUMN_PARENT = "parent_id";
    public static final String COLUMN_CREATION_WORKFLOW_ID = "creation_workflow_id";
    public static final String COLUMN_WORKFLOW_SELECTION_SCRIPT = "workflow_selection_script";
    public static final String COLUMN_VALIDATION_WORKFLOW_ID = "validation_workflow_id";
    public static final String COLUMN_SECURE_MAIL_SERVER_ID = "secure_mail_server_id";
    public static final String COLUMN_ANNOTATIONS_ALLOWED = "annotations_allowed";
    public static final String COLUMN_EXTERNAL_SIGNATURE_CONFIG_ID = "external_signature_config_id";
    public static final String COLUMN_EXTERNAL_SIGNATURE_AUTOMATIC = "external_signature_automatic";
    public static final String COLUMN_IS_DIGITAL_SIGNATURE_MANDATORY = "is_digital_signature_mandatory";
    public static final String COLUMN_IS_READING_MANDATORY = "is_reading_mandatory";
    public static final String COLUMN_IS_ANNEXE_INCLUDED = "is_annexe_included";
    public static final String COLUMN_IS_MULTI_DOCUMENTS = "is_multi_documents";
    public static final String COLUMN_IS_SEAL_AUTOMATIC = "is_seal_automatic";
    public static final String COLUMN_SEAL_CERTIFICATE_ID = "seal_certificate_id";


    @Column(name = COLUMN_CREATION_WORKFLOW_ID)
    private String creationWorkflowId;

    @Column(name = COLUMN_VALIDATION_WORKFLOW_ID)
    private String validationWorkflowId;

    @Column(name = COLUMN_WORKFLOW_SELECTION_SCRIPT, length = WORKFLOW_SELECTION_SCRIPT_MAX_FIELD_SIZE, columnDefinition = "text")
    private String workflowSelectionScript;

    @Column(name = COLUMN_IS_READING_MANDATORY)
    private boolean isReadingMandatory = false;

    @Column(name = COLUMN_IS_DIGITAL_SIGNATURE_MANDATORY)
    private boolean isDigitalSignatureMandatory = true;

    @Column(name = COLUMN_IS_ANNEXE_INCLUDED)
    private boolean isAnnexeIncluded = false;

    @Column(name = COLUMN_IS_MULTI_DOCUMENTS)
    private boolean isMultiDocuments = false;

    @Column(name = COLUMN_ANNOTATIONS_ALLOWED)
    private boolean annotationsAllowed = false;

    @Column(name = COLUMN_EXTERNAL_SIGNATURE_AUTOMATIC)
    private boolean externalSignatureAutomatic = false;

    @Column(name = COLUMN_IS_SEAL_AUTOMATIC)
    private boolean isSealAutomatic = false;

    @Column(name = COLUMN_SECURE_MAIL_SERVER_ID)
    private Long secureMailServerId;

    @Column(name = COLUMN_SEAL_CERTIFICATE_ID)
    private String sealCertificateId = null;

    @ToString.Exclude
    @ManyToOne(fetch = EAGER)
    @JoinColumn(name = COLUMN_EXTERNAL_SIGNATURE_CONFIG_ID, foreignKey = @ForeignKey(name = "fk_" + COLUMN_EXTERNAL_SIGNATURE_CONFIG_ID))
    private ExternalSignatureConfig externalSignatureConfig;

    @ToString.Exclude
    @OneToMany(fetch = LAZY, mappedBy = "subtype", cascade = REMOVE, orphanRemoval = true)
    private List<SubtypeMetadata> subtypeMetadataList;

    @ToString.Exclude
    @OneToMany(fetch = LAZY, mappedBy = "subtype", cascade = REMOVE, orphanRemoval = true)
    private List<SubtypeLayer> subtypeLayers;

    @JsonIgnore
    @ToString.Exclude
    @ManyToOne(fetch = EAGER)
    @JoinColumn(name = COLUMN_PARENT, foreignKey = @ForeignKey(name = "fk_" + COLUMN_PARENT))
    private Type parentType;

    private @Transient List<String> creationPermittedDeskIds;
    private @Transient List<String> filterableByDeskIds;


    @Builder
    public Subtype(@NotNull String id, @Nullable String name, @Nullable String description, Tenant tenant, @Nullable Type parentType,
                   @Nullable String creationWorkflowId, @Nullable String validationWorkflowId, @Nullable ExternalSignatureConfig externalSignatureConfig,
                   String sealCertificateId, boolean isDigitalSignatureMandatory, boolean isReadingMandatory, boolean isMultiDocuments,
                   boolean annotationsAllowed) {

        super(id, name, tenant, description);

        this.parentType = parentType;
        this.creationWorkflowId = creationWorkflowId;
        this.validationWorkflowId = validationWorkflowId;
        this.isDigitalSignatureMandatory = isDigitalSignatureMandatory;
        this.isReadingMandatory = isReadingMandatory;
        this.isMultiDocuments = isMultiDocuments;
        this.externalSignatureConfig = externalSignatureConfig;
        this.sealCertificateId = sealCertificateId;
        this.annotationsAllowed = annotationsAllowed;
    }


    public Subtype(@NotNull String id) {
        this.id = id;
    }


    public Subtype(@NotNull Record7<String, String, String, String, String, String, Boolean> record) {

        super(
                record.get(COLUMN_ID, String.class),
                record.get(COLUMN_NAME, String.class),
                Tenant.builder().id(record.get(COLUMN_TENANT_ID, String.class)).build(),
                record.get(COLUMN_DESCRIPTION, String.class)
        );

        this.creationWorkflowId = record.get(COLUMN_CREATION_WORKFLOW_ID, String.class);
        this.validationWorkflowId = record.get(COLUMN_VALIDATION_WORKFLOW_ID, String.class);
        this.isMultiDocuments = record.get(COLUMN_IS_MULTI_DOCUMENTS, Boolean.class);
    }


}
