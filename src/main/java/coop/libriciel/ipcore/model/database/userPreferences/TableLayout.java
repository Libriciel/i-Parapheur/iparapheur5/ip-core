/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.database.userPreferences;

import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

import static coop.libriciel.ipcore.model.database.userPreferences.TableLayout.*;
import static jakarta.persistence.EnumType.STRING;
import static jakarta.persistence.FetchType.EAGER;


@Getter
@Setter
@Entity
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(
        name = TableLayout.TABLE_NAME,
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {COLUMN_USER_PREFERENCES, COLUMN_TABLE_NAME, COLUMN_DESK_ID})
        }
)
public class TableLayout {


    public static final String TABLE_NAME = "table_layout";
    public static final String COLUMN_TARGET_ID = TABLE_NAME + "_target_id";
    public static final String COLUMN_TABLE_NAME = TABLE_NAME + "_table_name";
    public static final String COLUMN_DEFAULT_ASC = TABLE_NAME + "_default_asc";
    public static final String COLUMN_DEFAULT_SORT_BY = TABLE_NAME + "_default_sort_by";
    public static final String COLUMN_DEFAULT_PAGE_SIZE = TABLE_NAME + "_default_page_size";
    public static final String COLUMN_COLUMN_LIST = TABLE_NAME + "_column_list";
    public static final String TABLE_LAYOUT_COLUMN_LIST = "table_layout_column_list";
    public static final String COLUMN_USER_PREFERENCES = "user_id";
    public static final String COLUMN_DESK_ID = "desk_id";
    public static final String FOREIGN_KEY_USER_PREFERENCES = "fk_" + COLUMN_USER_PREFERENCES;

    @Id
    private String id;

    @ToString.Exclude
    @ManyToOne(fetch = EAGER)
    @JoinColumn(name = COLUMN_USER_PREFERENCES, foreignKey = @ForeignKey(name = FOREIGN_KEY_USER_PREFERENCES), updatable = false)
    private UserPreferences userPreferences;

    @Enumerated(STRING)
    @Column(name = COLUMN_TABLE_NAME, nullable = false)
    TableName tableName;

    @Column(name = COLUMN_DESK_ID, updatable = false)
    private String deskId;

    @Column(name = COLUMN_DEFAULT_ASC)
    private boolean defaultAsc;

    @Column(name = COLUMN_DEFAULT_SORT_BY)
    private String defaultSortBy;

    @Column(name = COLUMN_DEFAULT_PAGE_SIZE)
    private Integer defaultPageSize;

    @ElementCollection(fetch = EAGER, targetClass = String.class)
    @Column(name = COLUMN_COLUMN_LIST)
    @CollectionTable(
            name = TABLE_LAYOUT_COLUMN_LIST,
            joinColumns = @JoinColumn(name = COLUMN_TARGET_ID),
            foreignKey = @ForeignKey(name = "fk_" + COLUMN_TARGET_ID)
    )
    @Builder.Default
    private List<String> columnList = new ArrayList<>();


}
