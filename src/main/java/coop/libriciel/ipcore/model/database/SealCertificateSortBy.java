/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.database;


import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;
import coop.libriciel.ipcore.model.crypto.SealCertificate;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
@Schema(enumAsRef = true)
public enum SealCertificateSortBy {


    @JsonEnumDefaultValue
    NAME(SealCertificate.FIELD_NAME),
    ID(SealCertificate.FIELD_ID),
    EXPIRATION_DATE(SealCertificate.FIELD_EXPIRATION_DATE);


    private final String fieldName;


    public static class Constants {

        public static final String NAME_VALUE = "NAME";
        public static final String API_DOC_SORT_BY_VALUES = "Sorting properties are restricted to the SealCertificateSortBy enum values";


        private Constants() {
            throw new IllegalStateException("Utility class");
        }

    }


}
