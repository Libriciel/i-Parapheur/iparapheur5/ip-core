/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.database.userPreferences;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.TemplateType;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@NoArgsConstructor
public class UserPreferencesDto {


    // <editor-fold desc="Database-stored preferences">


    private boolean showDelegations;
    private boolean showAdminIds;
    private int archiveViewDefaultPageSize;
    private int taskViewDefaultPageSize;
    private boolean taskViewDefaultAsc;
    private TaskViewColumn taskViewDefaultSortBy;
    private TemplateType defaultSignatureTemplate;
    private TemplateType defaultSealTemplate;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private FolderFilterDto currentFilter;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY)
    private String currentFilterId;

    private List<FolderViewBlock> folderViewBlockList;
    private List<TaskViewColumn> taskViewColumnList;
    private List<ArchiveViewColumn> archiveViewColumnList;

    private List<TableLayoutDto> tableLayoutList;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private List<DeskRepresentation> favoriteDesks;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY)
    private List<String> favoriteDeskIds;


    // </editor-fold desc="Database-stored preferences">


    // <editor-fold desc="Keycloak-stored preferences">


    @Deprecated
    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY)
    private String password;

    private String notificationsRedirectionMail;
    private boolean isNotifiedOnConfidentialFolders;
    private boolean isNotifiedOnFollowedFolders;
    private boolean isNotifiedOnLateFolders;
    private String notificationsCronFrequency;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private String signatureImageContentId;


    // </editor-fold desc="Keycloak-stored preferences">


}
