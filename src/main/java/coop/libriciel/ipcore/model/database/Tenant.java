/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.database;

import com.fasterxml.jackson.annotation.JsonIgnore;
import coop.libriciel.ipcore.model.pdfstamp.Layer;
import jakarta.persistence.*;
import lombok.*;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.TextUtils.UUID_STRING_SIZE;
import static jakarta.persistence.CascadeType.REMOVE;
import static jakarta.persistence.FetchType.EAGER;
import static jakarta.persistence.FetchType.LAZY;


@Getter
@Setter
@Entity
@ToString
@NoArgsConstructor
@Table(name = Tenant.TABLE_NAME)
public class Tenant {


    public static final String API_DOC_ID_VALUE = "Tenant id";
    public static final String API_PATH = "tenantId";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_ZIP_CODE = "zip_code";
    public static final String COLUMN_DESK_LIMIT = "desk_limit";
    public static final String COLUMN_USER_LIMIT = "user_limit";
    public static final String COLUMN_WORKFLOW_LIMIT = "workflow_limit";
    public static final String COLUMN_TYPE_LIMIT = "type_limit";
    public static final String COLUMN_SUBTYPE_LIMIT = "subtype_limit";
    public static final String COLUMN_FOLDER_LIMIT = "folder_limit";
    public static final String COLUMN_METADATA_LIMIT = "metadata_limit";
    public static final String COLUMN_LAYER_LIMIT = "layer_limit";

    public static final String TABLE_NAME = "tenant";
    public static final String COLUMN_INDEX = "index";
    public static final String COLUMN_CONTENT_ID = "content_id";
    public static final String COLUMN_STATS_ID = "stats_id";

    public static final String TABLE_CUSTOM_TEMPLATES_NAME = TABLE_NAME + "_custom_templates";

    public static final String COLUMN_TENANT_ID = "tenant_id";


    @Column(name = COLUMN_ID, length = UUID_STRING_SIZE)
    protected @Id String id;

    @SuppressWarnings("DefaultAnnotationParam")
    @Column(name = COLUMN_NAME, length = NAME_MAX_LENGTH)
    protected String name;

    @Column(name = COLUMN_ZIP_CODE)
    protected String zipCode;

    @Column(name = COLUMN_DESK_LIMIT)
    protected Integer deskLimit;

    @Column(name = COLUMN_USER_LIMIT)
    protected Integer userLimit;

    @Column(name = COLUMN_WORKFLOW_LIMIT)
    protected Integer workflowLimit;

    @Column(name = COLUMN_TYPE_LIMIT)
    protected Integer typeLimit;

    @Column(name = COLUMN_SUBTYPE_LIMIT)
    protected Integer subtypeLimit;

    @Column(name = COLUMN_FOLDER_LIMIT)
    protected Integer folderLimit;

    @Column(name = COLUMN_METADATA_LIMIT)
    protected Integer metadataLimit;

    @Column(name = COLUMN_LAYER_LIMIT)
    protected Integer layerLimit;

    @Column(name = COLUMN_INDEX)
    private Long index = null;

    @Column(name = COLUMN_CONTENT_ID, length = UUID_STRING_SIZE)
    private String contentId = null;

    @Column(name = COLUMN_STATS_ID)
    private String statsId = null;

    /**
     * JPA Buddy doesn't support this kind of external connexion yet.
     * We'll have to wait for it, and manage sql diffs manually until then.
     *
     * @see <a href="https://youtrack.haulmont.com/issue/JPAB-806">JPA Buddy's tracked bug</a>
     */
    @ToString.Exclude
    @ElementCollection(fetch = EAGER)
    @CollectionTable(
            name = TABLE_CUSTOM_TEMPLATES_NAME,
            joinColumns = @JoinColumn(name = COLUMN_TENANT_ID),
            foreignKey = @ForeignKey(name = "fk_" + COLUMN_TENANT_ID)
    )
    private Map<TemplateType, String> customTemplates = new HashMap<>();

    /**
     * The following OneToMany fields are not columns in the Tenant table.
     * The relation is set in the child Table, with their {@link TypologyEntity#COLUMN_TENANT_ID} columns or equivalents.
     * The child-to-parent relationship is forced with the {@link OneToMany#mappedBy} annotation parameter.
     * <p>
     * Yet, the link has to be made to allow the cascade deletion.
     * That's why those lists are here, and in {@link FetchType#LAZY} mode.
     */

    @JsonIgnore
    @ToString.Exclude
    @OneToMany(fetch = LAZY, mappedBy = "tenant", cascade = REMOVE, orphanRemoval = true)
    private List<Type> typeList = new ArrayList<>();

    @JsonIgnore
    @ToString.Exclude
    @OneToMany(fetch = LAZY, mappedBy = "tenant", cascade = REMOVE, orphanRemoval = true)
    private List<ExternalSignatureConfig> externalSignatureConfigList = new ArrayList<>();

    @JsonIgnore
    @ToString.Exclude
    @OneToMany(fetch = LAZY, mappedBy = "tenant", cascade = REMOVE, orphanRemoval = true)
    private List<Metadata> metadataList = new ArrayList<>();

    @JsonIgnore
    @ToString.Exclude
    @OneToMany(fetch = LAZY, mappedBy = "tenant", cascade = REMOVE, orphanRemoval = true)
    private List<Layer> layerList = new ArrayList<>();


    @Builder
    public Tenant(@Nullable String id,
                  @Nullable String name,
                  @Nullable String zipCode,
                  @Nullable Integer deskLimit,
                  @Nullable Integer userLimit,
                  @Nullable Integer workflowLimit,
                  @Nullable Integer typeLimit,
                  @Nullable Integer subtypeLimit,
                  @Nullable Integer folderLimit,
                  @Nullable Integer metadataLimit,
                  @Nullable Integer layerLimit,
                  @Nullable Long index,
                  @Nullable String contentId,
                  @Nullable String statsId) {
        this.id = id;
        this.name = name;
        this.zipCode = zipCode;
        this.deskLimit = deskLimit;
        this.workflowLimit = workflowLimit;
        this.typeLimit = typeLimit;
        this.subtypeLimit = subtypeLimit;
        this.folderLimit = folderLimit;
        this.metadataLimit = metadataLimit;
        this.layerLimit = layerLimit;
        this.index = index;
        this.contentId = contentId;
        this.statsId = statsId;
    }


}
