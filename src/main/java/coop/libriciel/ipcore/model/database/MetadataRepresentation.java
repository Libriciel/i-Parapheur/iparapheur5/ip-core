/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.database;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.TextUtils.NO_FORBIDDEN_CHARACTERS_REGEXP;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class MetadataRepresentation {


    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private String id;

    @Schema(example = "Example metadata")
    @Size(min = NAME_MIN_LENGTH, max = NAME_MAX_LENGTH, message = NAME_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
    @NotNull(message = NAME_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
    @Pattern(regexp = NO_FORBIDDEN_CHARACTERS_REGEXP, message = NAME_CONTAINS_FORBIDDEN_CHARACTER_JAVAX_VALIDATION_ERROR_MESSAGE)
    private String name;

    @Schema(example = "example_metadata")
    @Size(min = KEY_MIN_LENGTH, max = KEY_MAX_LENGTH, message = KEY_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
    @NotNull(message = KEY_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
    // TODO add a specific regexp check for keys
    private String key;

    @Schema(nullable = true)
    private Integer index;

    private MetadataType type;


    /**
     * This constructor seems unused,
     * but it is actually needed by the JPA engine to convert an entity to a representation
     *
     * @param metadata self-explanatory
     */
    @SuppressWarnings("unused")
    public MetadataRepresentation(Metadata metadata) {
        this.id = metadata.getId();
        this.name = metadata.getName();
        this.key = metadata.getKey();
        this.index = metadata.getIndex();
        this.type = metadata.getType();
    }


    /**
     * This constructor seems unused,
     * but it is actually needed by the JPA engine to convert an entity to a representation
     *
     * @param id   self-explanatory
     * @param name self-explanatory
     */
    @SuppressWarnings("unused")
    public MetadataRepresentation(String id, String name) {
        this.id = id;
        this.name = name;
    }


}
