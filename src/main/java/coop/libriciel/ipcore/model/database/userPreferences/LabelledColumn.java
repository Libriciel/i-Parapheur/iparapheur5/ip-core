/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.database.userPreferences;


import com.fasterxml.jackson.annotation.JsonProperty;
import coop.libriciel.ipcore.model.database.MetadataRepresentation;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.annotation.Nullable;
import java.util.UUID;

import static coop.libriciel.ipcore.model.database.userPreferences.LabelledColumnType.CLASS_ATTRIBUTE;
import static coop.libriciel.ipcore.model.database.userPreferences.LabelledColumnType.METADATA;


@Data
public class LabelledColumn {

    protected String id;
    protected LabelledColumnType type;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    protected @Nullable MetadataRepresentation metadata;


    public LabelledColumn(String id) {
        this.id = id;
        try {
            UUID.fromString(this.id);
            this.type = METADATA;
        } catch (IllegalArgumentException ex) {
            this.type = CLASS_ATTRIBUTE;
        }
    }
}
