/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.database.serverInfo;

import jakarta.persistence.*;
import lombok.*;

import java.util.Set;


@Setter
@Getter
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = ServerInfo.TABLE_NAME)
public class ServerInfo {


    public static final String TABLE_NAME = "server_info";
    public static final String COLUMN_KEY = "key";
    public static final String COLUMN_VALUE = "value";


    public enum Key {

        ENTITY_NAME,
        ENTITY_ADDRESS,
        ENTITY_SIRET,
        ENTITY_APE_CODE,
        ENTITY_PHONE,
        ENTITY_MAIL,
        RESPONSIBLE_NAME,
        RESPONSIBLE_TITLE,
        DPO_NAME,
        DPO_MAIL,
        HOSTING_NAME,
        HOSTING_ADDRESS,
        HOSTING_SIRET,
        HOSTING_COMMENT,
        MAINTENANCE_NAME,
        MAINTENANCE_ADDRESS,
        MAINTENANCE_SIRET;


        public static class Constants {

            public static final Set<Key> GDPR_KEYS = Set.of(
                    ServerInfo.Key.ENTITY_NAME,
                    ServerInfo.Key.ENTITY_ADDRESS,
                    ServerInfo.Key.ENTITY_SIRET,
                    ServerInfo.Key.ENTITY_APE_CODE,
                    ServerInfo.Key.ENTITY_PHONE,
                    ServerInfo.Key.ENTITY_MAIL,
                    ServerInfo.Key.RESPONSIBLE_NAME,
                    ServerInfo.Key.RESPONSIBLE_TITLE,
                    ServerInfo.Key.DPO_NAME,
                    ServerInfo.Key.DPO_MAIL,
                    ServerInfo.Key.HOSTING_NAME,
                    ServerInfo.Key.HOSTING_ADDRESS,
                    ServerInfo.Key.HOSTING_SIRET,
                    ServerInfo.Key.HOSTING_COMMENT,
                    ServerInfo.Key.MAINTENANCE_NAME,
                    ServerInfo.Key.MAINTENANCE_ADDRESS,
                    ServerInfo.Key.MAINTENANCE_SIRET
            );


            private Constants() {
                throw new IllegalStateException("Utility class");
            }

        }

    }


    @Column(name = COLUMN_KEY)
    @Enumerated(EnumType.STRING)
    private @Id Key key;

    @Column(name = COLUMN_VALUE)
    private String value;


}
