/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
package coop.libriciel.ipcore.model.database;

import coop.libriciel.ipcore.model.crypto.PdfSignaturePosition;
import coop.libriciel.ipcore.model.crypto.SignatureFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;

import static jakarta.persistence.CascadeType.REMOVE;
import static jakarta.persistence.FetchType.EAGER;


@Getter
@Setter
@Entity
@ToString
@NoArgsConstructor
@Table(
        name = Type.TABLE_NAME,
        uniqueConstraints = @UniqueConstraint(
                name = TypologyEntity.COLUMN_TENANT_ID + "_" + TypologyEntity.COLUMN_NAME + "_unique",
                columnNames = {TypologyEntity.COLUMN_TENANT_ID, TypologyEntity.COLUMN_NAME}
        )
)
public class Type extends TypologyEntity {


    public static final String TABLE_NAME = "type";
    public static final String API_PATH = "typeId";
    public static final String API_DOC_ID_VALUE = "Type id";
    public static final String API_DOC_PARENT_NAME_VALUE = "Parent name";


    @Schema(enumAsRef = true)
    public enum SignatureProtocol {HELIOS, ACTES, NONE}


    @ToString.Exclude
    @OneToMany(fetch = EAGER, mappedBy = "parentType", cascade = REMOVE, orphanRemoval = true)
    private List<Subtype> subtypes;

    private SignatureFormat signatureFormat;
    private SignatureProtocol protocol;
    private boolean signatureVisible = true;

    @Convert(converter = PdfSignaturePositionConverter.class)
    private PdfSignaturePosition signaturePosition;

    private String signatureLocation;
    private String signatureZipCode;


    @Builder
    public Type(Tenant tenant,
                @NotNull String id,
                @Nullable String name,
                @Nullable String description,
                @Nullable List<Subtype> subtypes,
                SignatureFormat signatureFormat,
                @Nullable PdfSignaturePosition signaturePosition,
                @Nullable String signatureLocation,
                @Nullable String signatureZipCode,
                @Nullable Type.SignatureProtocol protocol,
                @Nullable Boolean isSignatureVisible) {

        super(id, name, tenant, description);

        this.subtypes = subtypes;
        this.signatureFormat = signatureFormat;
        this.signaturePosition = signaturePosition;
        this.signatureLocation = signatureLocation;
        this.signatureZipCode = signatureZipCode;
        this.protocol = protocol;

        Optional.ofNullable(isSignatureVisible).ifPresent(b -> this.signatureVisible = b);
    }


    public Type(@NotNull String id) {
        this.id = id;
    }


}
