/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.database.userPreferences;

import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Type;
import coop.libriciel.ipcore.model.workflow.State;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.Fetch;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static coop.libriciel.ipcore.utils.TextUtils.UUID_STRING_SIZE;
import static jakarta.persistence.EnumType.STRING;
import static jakarta.persistence.FetchType.EAGER;
import static org.hibernate.annotations.FetchMode.SUBSELECT;


@Getter
@Setter
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = FolderFilter.TABLE_NAME)
public class FolderFilter {

    public static final String TABLE_NAME = "folder_filter";
    public static final String TABLE_METADATA_FILTERS_NAME = TABLE_NAME + "_metadata_filters";

    public static final String API_DOC_FILTER_NAME = "Filter name parameter";
    public static final String API_DOC_TYPE_NAME = "Type name parameter";
    public static final String API_DOC_SUBTYPE_NAME = "Subtype name parameter";
    public static final String API_DOC_FROM = "From parameter";
    public static final String API_DOC_TO = "To parameter";
    public static final String API_DOC_SEARCH_DATA = "Search data parameter";
    public static final String API_DOC_LEGACY_ID = "Legacy bridge ID";
    public static final String API_DOC_STATE = "State of the folders to retrieve";
    public static final String API_DOC_DESK_ID = "If pressent, only folder on this desk will be returned";
    public static final String API_DOC_METADATA_FILTERS = "Metadata filters";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_USER_ID = "user_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_TYPE_ID = "type_id";
    public static final String COLUMN_SUBTYPE_ID = "subtype_id";
    public static final String COLUMN_CREATED_AFTER = "created_after";
    public static final String COLUMN_CREATED_BEFORE = "created_before";
    public static final String COLUMN_SEARCH_DATA = "search_data";
    public static final String COLUMN_LEGACY_ID = "legacy_id";
    public static final String COLUMN_FOLDER_FILTER_ID = "folder_filter_id";
    public static final String COLUMN_STATE = "state";


    @Column(name = COLUMN_ID, length = UUID_STRING_SIZE)
    private @Id String id;

    @Column(name = COLUMN_USER_ID, length = UUID_STRING_SIZE)
    private String userId;

    @Column(name = COLUMN_NAME)
    private String filterName;

    @ManyToOne(fetch = EAGER)
    @JoinColumn(name = COLUMN_TYPE_ID, foreignKey = @ForeignKey(name = "fk_" + COLUMN_TYPE_ID))
    private Type type;

    @ManyToOne(fetch = EAGER)
    @JoinColumn(name = COLUMN_SUBTYPE_ID, foreignKey = @ForeignKey(name = "fk_" + COLUMN_SUBTYPE_ID))
    private Subtype subtype;

    @Column(name = COLUMN_CREATED_AFTER)
    private Date createdAfter;

    @Column(name = COLUMN_CREATED_BEFORE)
    private Date createdBefore;

    @Column(name = COLUMN_SEARCH_DATA)
    private String searchData;

    @Column(name = COLUMN_LEGACY_ID)
    private String legacyId;

    @Enumerated(STRING)
    @Column(name = COLUMN_STATE)
    private State state;

    @ElementCollection(fetch = EAGER)
    @Fetch(value = SUBSELECT)
    @CollectionTable(
            name = TABLE_METADATA_FILTERS_NAME,
            joinColumns = @JoinColumn(name = COLUMN_FOLDER_FILTER_ID),
            foreignKey = @ForeignKey(name = "fk_" + COLUMN_FOLDER_FILTER_ID)
    )
    private Map<String, String> metadataFilters = new HashMap<>();


    public FolderFilter(Type type, Subtype subtype, String searchData, String legacyId) {
        this.type = type;
        this.subtype = subtype;
        this.searchData = searchData;
        this.legacyId = legacyId;
    }

}
