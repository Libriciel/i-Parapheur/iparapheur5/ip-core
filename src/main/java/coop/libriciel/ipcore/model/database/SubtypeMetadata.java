/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.database;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serial;
import java.io.Serializable;
import java.util.Comparator;
import java.util.Optional;
import java.util.function.Function;

import static coop.libriciel.ipcore.utils.TextUtils.UUID_STRING_SIZE;
import static jakarta.persistence.FetchType.EAGER;
import static java.util.Comparator.*;


@Getter
@Setter
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = SubtypeMetadata.TABLE_NAME)
public class SubtypeMetadata {

    public static final String TABLE_NAME = "subtype_metadata";

    public static final String COLUMN_SUBTYPE = "subtype_id";
    public static final String COLUMN_METADATA = "metadata_id";
    public static final String COLUMN_DEFAULT_VALUE = "default_value";
    public static final String COLUMN_MANDATORY = "mandatory";
    public static final String COLUMN_EDITABLE = "editable";

    /**
     * Sort the objects in the common way: Lower index, then name, then key, then id.
     * Null-safe everywhere.
     */
    public static final Comparator<SubtypeMetadata> SUBTYPE_METADATA_COMPARATOR = comparing((Function<SubtypeMetadata, Integer>)
            /* comparing */sm -> Optional.ofNullable(sm).map(SubtypeMetadata::getMetadata).map(Metadata::getIndex).orElse(null), nullsLast(naturalOrder()))
            .thenComparing(sm -> Optional.ofNullable(sm).map(SubtypeMetadata::getMetadata).map(Metadata::getName).orElse(null), nullsLast(naturalOrder()))
            .thenComparing(sm -> Optional.ofNullable(sm).map(SubtypeMetadata::getMetadata).map(Metadata::getKey).orElse(null), nullsLast(naturalOrder()))
            .thenComparing(sm -> Optional.ofNullable(sm).map(SubtypeMetadata::getMetadata).map(Metadata::getId).orElse(null), nullsLast(naturalOrder()));


    @Data
    @Embeddable
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CompositeId implements Serializable {


        @Serial
        private static final long serialVersionUID = -6008779124596137680L;

        @JsonIgnore
        @Column(name = COLUMN_SUBTYPE, length = UUID_STRING_SIZE, nullable = false, updatable = false)
        private String subtypeId;

        @Column(name = COLUMN_METADATA, length = UUID_STRING_SIZE, nullable = false, updatable = false)
        private String metadataId;


    }


    @EmbeddedId
    private CompositeId id;

    @JsonIgnore
    @ToString.Exclude
    @ManyToOne(fetch = EAGER)
    @JoinColumn(name = COLUMN_SUBTYPE, foreignKey = @ForeignKey(name = "fk_" + COLUMN_SUBTYPE), insertable = false, updatable = false)
    private Subtype subtype;

    @ToString.Exclude
    @ManyToOne(fetch = EAGER)
    @JoinColumn(name = COLUMN_METADATA, foreignKey = @ForeignKey(name = "fk_" + COLUMN_METADATA), insertable = false, updatable = false)
    private Metadata metadata;

    @Column(name = COLUMN_DEFAULT_VALUE)
    private String defaultValue;

    @Column(name = COLUMN_MANDATORY)
    private boolean mandatory = false;

    @Column(name = COLUMN_EDITABLE)
    private boolean editable = false;


    public SubtypeMetadata(@NotNull Subtype subtype, @NotNull Metadata metadata, @Nullable String defaultValue, boolean mandatory, boolean editable) {
        this(new CompositeId(subtype.getId(), metadata.getId()), subtype, metadata, defaultValue, mandatory, editable);
    }


    public SubtypeMetadata(@NotNull String metadataId) {
        this.metadata = new Metadata(metadataId);
    }

}
