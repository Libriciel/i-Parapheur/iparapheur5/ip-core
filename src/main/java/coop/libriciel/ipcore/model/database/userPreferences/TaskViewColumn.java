/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.database.userPreferences;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;


@Schema(enumAsRef = true)
public enum TaskViewColumn {


    CREATION_DATE,
    CURRENT_DESK,
    FOLDER_ID,
    FOLDER_NAME,
    LIMIT_DATE,
    ORIGIN_DESK,
    ORIGIN_USER,
    STATE,
    SUBTYPE,
    TASK_ID,
    TYPE;


    public static class Constants {

        public static final List<TaskViewColumn> DEFAULT_ORDER =
                List.of(FOLDER_NAME, STATE, TYPE, SUBTYPE, CURRENT_DESK, LIMIT_DATE, CREATION_DATE, ORIGIN_DESK);


        private Constants() {
            throw new IllegalStateException("Utility class");
        }

    }


}
