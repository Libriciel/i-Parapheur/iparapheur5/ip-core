/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.database.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import coop.libriciel.ipcore.model.database.TemplateType;
import coop.libriciel.ipcore.model.database.TenantRepresentation;
import coop.libriciel.ipcore.model.templates.TemplateInfo;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import jakarta.annotation.Nullable;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Positive;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Map;

import static coop.libriciel.ipcore.utils.ApiUtils.*;


@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class TenantDto extends TenantRepresentation {

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    Map<TemplateType, TemplateInfo> templateInfoMap;

    @Nullable
    protected String zipCode;

    @Max(MAX_DESKS_COUNT)
    @Positive
    @Nullable
    protected Integer deskLimit;

    @Max(MAX_USERS_COUNT)
    @Positive
    @Nullable
    protected Integer userLimit;

    @Max(MAX_WORKFLOWS_COUNT)
    @Positive
    @Nullable
    protected Integer workflowLimit;

    @Max(MAX_TYPES_COUNT)
    @Positive
    @Nullable
    protected Integer typeLimit;

    @Max(MAX_SUBTYPES_COUNT)
    @Positive
    @Nullable
    protected Integer subtypeLimit;

    @Max(MAX_FOLDERS_COUNT)
    @Positive
    @Nullable
    protected Integer folderLimit;

    @Max(MAX_METADATAS_COUNT)
    @Nullable
    protected Integer metadataLimit;

    @Max(MAX_LAYERS_COUNT)
    @Nullable
    protected Integer layerLimit;


    public TenantDto(String name) {
        super(name);
    }

    public TenantDto(String name,
                     @Nullable String zipCode,
                     @Nullable Integer deskLimit,
                     @Nullable Integer userLimit,
                     @Nullable Integer workflowLimit,
                     @Nullable Integer typeLimit,
                     @Nullable Integer subtypeLimit,
                     @Nullable Integer folderLimit,
                     @Nullable Integer metadataLimit,
                     @Nullable Integer layerLimit) {
        super(name);
        this.zipCode = zipCode;
        this.deskLimit = deskLimit;
        this.userLimit = userLimit;
        this.workflowLimit = workflowLimit;
        this.typeLimit = typeLimit;
        this.subtypeLimit = subtypeLimit;
        this.folderLimit = folderLimit;
        this.metadataLimit = metadataLimit;
        this.layerLimit = layerLimit;
    }


}
