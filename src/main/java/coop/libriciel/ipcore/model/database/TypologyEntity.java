/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.database;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.*;
import org.jooq.Record7;

import static coop.libriciel.ipcore.utils.ApiUtils.DESCRIPTION_MAX_LENGTH;
import static coop.libriciel.ipcore.utils.ApiUtils.NAME_MAX_LENGTH;
import static coop.libriciel.ipcore.utils.TextUtils.UUID_STRING_SIZE;
import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;
import static jakarta.persistence.FetchType.EAGER;
import static jakarta.persistence.InheritanceType.TABLE_PER_CLASS;


@Getter
@Setter
@ToString
@MappedSuperclass
@NoArgsConstructor
@AllArgsConstructor
@SqlResultSetMapping(
        name = TypologyEntity.SQL_MAPPING_NAME,
        classes = {
                @ConstructorResult(targetClass = TypologyEntity.class, columns = {
                        @ColumnResult(name = TypologyEntity.COLUMN_ID, type = String.class),
                        @ColumnResult(name = TypologyEntity.COLUMN_NAME, type = String.class),
                        @ColumnResult(name = TypologyEntity.COLUMN_DESCRIPTION, type = String.class)
                })
        }
)
public class TypologyEntity {

    public static final String SQL_MAPPING_NAME = "TypologyEntityMapping";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_TENANT_ID = "tenant_id";
    public static final String COLUMN_DESCRIPTION = "description";


    @Column(name = COLUMN_ID, length = UUID_STRING_SIZE)
    protected @Id String id;

    @SuppressWarnings("DefaultAnnotationParam")
    @Column(name = COLUMN_NAME, length = NAME_MAX_LENGTH)
    private String name;

    @JsonIgnore
    @ToString.Exclude
    @Schema(accessMode = READ_ONLY)
    @ManyToOne(fetch = EAGER)
    @JoinColumn(name = COLUMN_TENANT_ID, foreignKey = @ForeignKey(name = "fk_" + COLUMN_TENANT_ID))
    private Tenant tenant;

    @SuppressWarnings("DefaultAnnotationParam")
    @Column(name = COLUMN_DESCRIPTION, length = DESCRIPTION_MAX_LENGTH)
    private String description;


    public TypologyEntity(Record7<String, String, String, String, String, String, Integer> record) {
        this.id = record.get(COLUMN_ID, String.class);
        this.name = record.get(COLUMN_NAME, String.class);
        this.description = record.get(COLUMN_DESCRIPTION, String.class);
    }


}
