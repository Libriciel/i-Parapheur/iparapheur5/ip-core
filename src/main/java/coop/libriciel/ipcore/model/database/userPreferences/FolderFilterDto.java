/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.database.userPreferences;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import coop.libriciel.ipcore.model.database.TypologyRepresentation;
import coop.libriciel.ipcore.model.workflow.State;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import lombok.Data;
import lombok.NoArgsConstructor;

import jakarta.validation.constraints.Size;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static coop.libriciel.ipcore.utils.ApiUtils.*;


@Data
@NoArgsConstructor
public class FolderFilterDto {


    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private String id;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private String userId;

    @Size(min = NAME_MIN_LENGTH, max = NAME_MAX_LENGTH, message = NAME_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
    private String filterName;

    // Type and subtype

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY)
    private String typeId;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private TypologyRepresentation type;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY)
    private String subtypeId;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private TypologyRepresentation subtype;

    // "from" and "to" (meaning "created after" and "created before", respectively)

    @JsonProperty("from")
    private Date createdAfter;

    @JsonProperty("to")
    private Date createdBefore;

    private String searchData;

    @Deprecated
    private String legacyId;

    private State state;

    private Map<String, String> metadataFilters = new HashMap<>();


}
