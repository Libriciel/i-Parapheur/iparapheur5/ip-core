/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.database;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;


@Getter
@Schema(enumAsRef = true)
public enum ExternalSignatureProvider {


    YOUSIGN_V2,
    YOUSIGN_V3,
    UNIVERSIGN,
    DOCAGE,
    OODRIVE_SIGN;


    @Converter(autoApply = true)
    public static class ExternalSignatureServiceNameConverter implements AttributeConverter<ExternalSignatureProvider, String> {


        @Override
        public String convertToDatabaseColumn(ExternalSignatureProvider category) {
            if (category == null) {
                return null;
            }
            return category.toString();
        }


        @Override
        public ExternalSignatureProvider convertToEntityAttribute(String name) {
            if (name == null) return null;

            return switch (name.toLowerCase()) {
                case "yousign", "yousign_v2" -> YOUSIGN_V2;
                case "yousign_v3" -> YOUSIGN_V3;
                case "universign" -> UNIVERSIGN;
                case "docage" -> DOCAGE;
                case "oodrive_sign" -> OODRIVE_SIGN;
                default -> null;
            };
        }
    }


    public boolean equalsOneOf(ExternalSignatureProvider... candidates) {

        for (ExternalSignatureProvider candidate : candidates) {
            if (this == candidate) {
                return true;
            }
        }

        return false;
    }


}
