/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.database;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jooq.Record7;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.TextUtils.NO_FORBIDDEN_CHARACTERS_REGEXP;
import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypologyRepresentation {

    /**
     * Utility parameters, to ease UI pagination.
     * These are not in the database, they are only computed at runtime.
     * Thus, they can't be referenced in the native {@link TypologyEntity} class.
     */
    public static final String COLUMN_PARENT_ID = "parent_id";
    public static final String COLUMN_CHILDREN_COUNT = "children_count";


    @Schema(accessMode = READ_ONLY)
    private String id;

    @Size(min = NAME_MIN_LENGTH, max = NAME_MAX_LENGTH, message = NAME_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
    @NotNull(message = NAME_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
    @Pattern(regexp = NO_FORBIDDEN_CHARACTERS_REGEXP, message = NAME_CONTAINS_FORBIDDEN_CHARACTER_JAVAX_VALIDATION_ERROR_MESSAGE)
    private String name;

    @Schema(accessMode = READ_ONLY)
    private String parentId;

    @Schema(accessMode = READ_ONLY)
    private Integer childrenCount;


    public TypologyRepresentation(Record7<String, String, String, String, String, String, Integer> record) {
        this.id = record.get(TypologyEntity.COLUMN_ID, String.class);
        this.name = record.get(TypologyEntity.COLUMN_NAME, String.class);
        this.parentId = record.get(TypologyRepresentation.COLUMN_PARENT_ID, String.class);
        this.childrenCount = record.get(TypologyRepresentation.COLUMN_CHILDREN_COUNT, Integer.class);
    }


}
