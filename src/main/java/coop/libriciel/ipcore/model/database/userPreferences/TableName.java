/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.database.userPreferences;


import coop.libriciel.ipcore.model.workflow.Action;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;


// TODO remove @getter and @AllArgsConstructor and check if the enum is still in the angular Library
@Getter
@AllArgsConstructor
@Schema(enumAsRef = true)
public enum TableName {


    FOLDER_MAIN_LAYOUT,
    TASK_LIST,
    ADMIN_FOLDER_LIST,
    ARCHIVE_LIST;


    public boolean equalsOneOf(TableName... candidates) {

        for (TableName candidate : candidates) {
            if (this == candidate) {
                return true;
            }
        }

        return false;
    }
}
