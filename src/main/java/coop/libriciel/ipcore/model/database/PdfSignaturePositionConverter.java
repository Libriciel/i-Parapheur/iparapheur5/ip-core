/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.database;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import coop.libriciel.ipcore.model.crypto.PdfSignaturePosition;
import lombok.extern.log4j.Log4j2;
import org.jooq.tools.StringUtils;
import org.springframework.web.server.ResponseStatusException;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;


@Log4j2
@Converter
public class PdfSignaturePositionConverter implements AttributeConverter<PdfSignaturePosition, String> {


    @Override
    public String convertToDatabaseColumn(PdfSignaturePosition attribute) {

        if (attribute == null) {
            return null;
        }

        try {
            return new ObjectMapper().writeValueAsString(attribute);
        } catch (JsonProcessingException exception) {
            throw new ResponseStatusException(
                    INTERNAL_SERVER_ERROR,
                    "PdfSignaturePositionConverter cannot write PdfSignaturePosition. This should not happen.",
                    exception
            );
        }
    }


    @Override
    public PdfSignaturePosition convertToEntityAttribute(String dbData) {

        if (StringUtils.isEmpty(dbData)) {
            return null;
        }

        try {
            return new ObjectMapper().readValue(dbData, PdfSignaturePosition.class);
        } catch (JsonProcessingException exception) {
            throw new ResponseStatusException(
                    INTERNAL_SERVER_ERROR,
                    "PdfSignaturePositionConverter cannot read PdfSignaturePosition. The model may have changed in a non-retro-compatible way",
                    exception
            );
        }
    }


}
