/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.database.requests;

import com.fasterxml.jackson.annotation.JsonIgnore;
import coop.libriciel.ipcore.model.database.MetadataDto;
import coop.libriciel.ipcore.model.database.SubtypeMetadata;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;

import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;
import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.WRITE_ONLY;


@Data
@NoArgsConstructor
public class SubtypeMetadataDto {

    /**
     * It shall not be given, in any request, but we still want this parameter internally.
     * The {@link SubtypeMetadata.CompositeId} object conversion will be way easier with it.
     */
    @JsonIgnore
    private String subtypeId;

    @Schema(accessMode = WRITE_ONLY)
    private String metadataId;

    @Schema(accessMode = READ_ONLY)
    private MetadataDto metadata;

    @Schema(nullable = true)
    private String defaultValue;

    private boolean mandatory = false;
    private boolean editable = false;


    @JsonIgnore
    public SubtypeMetadata.CompositeId getCompositeId() {
        return new SubtypeMetadata.CompositeId(subtypeId, metadataId);
    }


}
