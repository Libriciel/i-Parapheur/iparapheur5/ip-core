/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.database;

import com.fasterxml.jackson.annotation.JsonIgnore;
import coop.libriciel.ipcore.model.pdfstamp.Layer;
import jakarta.persistence.*;
import lombok.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serial;
import java.io.Serializable;

import static coop.libriciel.ipcore.utils.TextUtils.UUID_STRING_SIZE;
import static jakarta.persistence.FetchType.EAGER;


@Getter
@Setter
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = SubtypeLayer.TABLE_NAME)
public class SubtypeLayer {

    public static final String TABLE_NAME = "subtype_layer";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_SUBTYPE = "subtype_id";
    public static final String COLUMN_LAYER = "layer_id";
    public static final String COLUMN_ASSOCIATION = "association";


    @Data
    @Embeddable
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CompositeId implements Serializable {


        @Serial
        private static final long serialVersionUID = 5245643820050304849L;

        @JsonIgnore
        @Column(name = COLUMN_SUBTYPE, length = UUID_STRING_SIZE, nullable = false, updatable = false)
        private String subtypeId;

        @Column(name = COLUMN_LAYER, length = UUID_STRING_SIZE, nullable = false, updatable = false)
        private String layerId;


    }


    @EmbeddedId
    private CompositeId id;

    @JsonIgnore
    @ToString.Exclude
    @ManyToOne(fetch = EAGER)
    @JoinColumn(name = COLUMN_SUBTYPE, foreignKey = @ForeignKey(name = "fk_" + COLUMN_SUBTYPE), insertable = false, updatable = false)
    private Subtype subtype;

    @JsonIgnore
    @ToString.Exclude
    @ManyToOne(fetch = EAGER)
    @JoinColumn(name = COLUMN_LAYER, foreignKey = @ForeignKey(name = "fk_" + COLUMN_LAYER), insertable = false, updatable = false)
    private Layer layer;

    @Column(name = COLUMN_ASSOCIATION)
    private SubtypeLayerAssociation association;


    public SubtypeLayer(@NotNull Subtype subtype, @NotNull Layer layer, @Nullable SubtypeLayerAssociation association) {
        this(new CompositeId(subtype.getId(), layer.getId()), subtype, layer, association);
    }


    public SubtypeLayer(@NotNull String layerId) {
        this.layer = new Layer(layerId);
    }
}
