/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
package coop.libriciel.ipcore.model.database;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import static coop.libriciel.ipcore.utils.TextUtils.UUID_STRING_SIZE;
import static jakarta.persistence.CascadeType.REMOVE;
import static jakarta.persistence.EnumType.STRING;
import static jakarta.persistence.FetchType.EAGER;
import static jakarta.persistence.FetchType.LAZY;


@Getter
@Setter
@Entity
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(
        name = Metadata.TABLE_NAME,
        uniqueConstraints = {
                @UniqueConstraint(
                        name = Metadata.COLUMN_TENANT_ID + "_" + Metadata.COLUMN_KEY + "_unique",
                        columnNames = {Metadata.COLUMN_TENANT_ID, Metadata.COLUMN_KEY}
                ),
                @UniqueConstraint(
                        name = Metadata.COLUMN_TENANT_ID + "_" + Metadata.COLUMN_NAME + "_unique",
                        columnNames = {Metadata.COLUMN_TENANT_ID, Metadata.COLUMN_NAME}
                ),
        }
)
public class Metadata {

    public static final String API_DOC_ID_VALUE = "Metadata id";
    public static final String API_PATH = "metadataId";
    public static final String TABLE_NAME = "metadata";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_KEY = "key";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_INDEX = "index";
    public static final String COLUMN_TENANT_ID = "tenant_id";

    public static final String TABLE_RESTRICTED_VALUES_NAME = TABLE_NAME + "_restricted_values";
    public static final String COLUMN_METADATA_ID = "metadata_id";
    public static final String COLUMN_RESTRICTED_VALUES = "restricted_values";


    @Column(name = COLUMN_ID, length = UUID_STRING_SIZE)
    private @Id String id;

    @Column(name = COLUMN_KEY)
    private String key;

    @Column(name = COLUMN_NAME)
    private String name;

    @Column(name = COLUMN_INDEX)
    private Integer index;

    @Enumerated(STRING)
    @Column(name = COLUMN_TYPE)
    private MetadataType type;

    @ElementCollection(fetch = EAGER, targetClass = String.class)
    @Column(name = COLUMN_RESTRICTED_VALUES)
    @CollectionTable(
            name = TABLE_RESTRICTED_VALUES_NAME,
            joinColumns = @JoinColumn(name = COLUMN_METADATA_ID),
            foreignKey = @ForeignKey(name = "fk_" + COLUMN_METADATA_ID)
    )
    @Builder.Default
    private List<String> restrictedValues = new ArrayList<>();

    @JsonIgnore
    @ToString.Exclude
    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = COLUMN_TENANT_ID, foreignKey = @ForeignKey(name = "fk_" + COLUMN_TENANT_ID))
    private Tenant tenant;

    @JsonIgnore
    @ToString.Exclude
    @OneToMany(fetch = LAZY, mappedBy = "metadata", cascade = REMOVE, orphanRemoval = true)
    private List<SubtypeMetadata> subtypeMetadataList;


    public Metadata(@NotNull String id) {
        this.id = id;
    }


}
