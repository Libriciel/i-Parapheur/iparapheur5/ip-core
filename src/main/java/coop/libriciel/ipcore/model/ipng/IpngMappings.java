/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.ipng;

import lombok.*;
import org.hibernate.Hibernate;

import jakarta.persistence.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
@Table(name = IpngMappings.TABLE_NAME)
public class IpngMappings {


    public static final String TABLE_NAME = "ipng_mappings";

    public static final String COLUMN_TENANT_ID = "tenant_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_VALUE = "value";

    public static final String TABLE_IPNG_DESK_TO_DESKBOX = "ipng_desk_to_deskbox";
    public static final String TABLE_IPNG_INNER_TO_OUTGOING_TYPES = "ipng_inner_to_outgoing_types";
    public static final String TABLE_IPNG_INCOMING_TO_INNER_TYPES = "ipng_incoming_to_inner_types";
    public static final String TABLE_INNER_TO_IPNG_METADATA = "inner_to_ipng_metadata";


    private @Id String tenantId;

    @ElementCollection(fetch = FetchType.EAGER)
    @MapKeyColumn(name = COLUMN_NAME)
    @Column(name = COLUMN_VALUE)
    @CollectionTable(
            name = TABLE_IPNG_DESK_TO_DESKBOX,
            joinColumns = @JoinColumn(name = COLUMN_TENANT_ID),
            foreignKey = @ForeignKey(name = "fk_" + COLUMN_TENANT_ID)
    )
    Map<String, String> deskToDeskbox = new HashMap<>(); // maps from attribute name to value

    @ElementCollection(fetch = FetchType.EAGER)
    @MapKeyColumn(name = COLUMN_NAME)
    @Column(name = COLUMN_VALUE)
    @CollectionTable(
            name = TABLE_IPNG_INNER_TO_OUTGOING_TYPES,
            joinColumns = @JoinColumn(name = COLUMN_TENANT_ID),
            foreignKey = @ForeignKey(name = "fk_" + COLUMN_TENANT_ID)
    )
    Map<String, String> innerTypesToOutgoingIpngTypes = new HashMap<>(); // maps from attribute name to value

    @ElementCollection(fetch = FetchType.EAGER)
    @MapKeyColumn(name = COLUMN_NAME)
    @Column(name = COLUMN_VALUE)
    @CollectionTable(
            name = TABLE_IPNG_INCOMING_TO_INNER_TYPES,
            joinColumns = @JoinColumn(name = COLUMN_TENANT_ID),
            foreignKey = @ForeignKey(name = "fk_" + COLUMN_TENANT_ID)
    )
    Map<String, String> incomingIpngTypesToInnerTypes = new HashMap<>(); // maps from attribute name to value

    @ElementCollection(fetch = FetchType.EAGER)
    @MapKeyColumn(name = COLUMN_NAME)
    @Column(name = COLUMN_VALUE)
    @CollectionTable(
            name = TABLE_INNER_TO_IPNG_METADATA,
            joinColumns = @JoinColumn(name = COLUMN_TENANT_ID),
            foreignKey = @ForeignKey(name = "fk_" + COLUMN_TENANT_ID)
    )
    Map<String, String> innerToIpngMetadata = new HashMap<>();


    public IpngMappings(String tenantId) {
        this.tenantId = tenantId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        IpngMappings that = (IpngMappings) o;
        return tenantId != null && Objects.equals(tenantId, that.tenantId);
    }


    @Override
    public int hashCode() {
        return getClass().hashCode();
    }


}
