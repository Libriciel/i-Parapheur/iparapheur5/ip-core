/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.ipng;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;

import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@With
@Data
public class IpngProof {
    private String hash;
    private String id;
    private String businessId;
    private List<IpngMetadata> metadatas;
    private ProofType proofType = ProofType.SEND;
    private String receiverDeskboxId;
    private String senderDeskboxId;
    private IpngType type;

    public enum ProofType {
        SEND, RECEIVE, RESPONSE
    }
}
