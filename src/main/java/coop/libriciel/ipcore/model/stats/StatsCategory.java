/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.stats;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.annotation.PropertyKey;


@Getter
@AllArgsConstructor
public enum StatsCategory {

    WORKFLOW("message.workflow"),
    DESK("message.desk"),
    USER("message.user"),
    FOLDER("message.folder"),
    TENANT("message.tenant"),
    TYPE("message.type"),
    SUBTYPE("message.subtype"),
    METADATA("message.metadata"),
    LAYER("message.layer"),
    SEAL_CERTIFICATE("message.seal_certificate"),
    EXTERNAL_SIGNATURE_CONFIG("message.external_signature_config"),
    TEMPLATE("message.template");


    private final @PropertyKey String messageKey;


}
