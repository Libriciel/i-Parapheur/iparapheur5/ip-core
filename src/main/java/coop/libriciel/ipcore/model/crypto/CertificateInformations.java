/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.crypto;

import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * A wrapper class for parsed X500 data
 */
@Data
@NoArgsConstructor
public class CertificateInformations {

    private String title;
    private String emailAddress;
    private String commonName;
    private String organizationalUnit;
    private String organization;
    private String locality;
    private String stateOrProvince;
    private String country;

}
