/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.crypto;

import coop.libriciel.ipcore.utils.LocalizedStatusException;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.MediaType;

import static coop.libriciel.ipcore.model.crypto.SignatureFormat.Constants.*;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.MediaType.*;


@Getter
@AllArgsConstructor
@Schema(enumAsRef = true)
public enum SignatureFormat {


    PKCS7(APPLICATION_OCTET_STREAM, CADES_PREMIS_VALUE, CADES_PREMIS_VALIDATION_RULE_VALUE),
    PADES(APPLICATION_PDF, null, null),
    PES_V2(APPLICATION_XML, null, null),
    XADES_DETACHED(APPLICATION_XML, XADES_PREMIS_VALUE, XADES_PREMIS_VALIDATION_RULE_VALUE),
    AUTO(null, null, null);


    private final MediaType mediaType;
    private final String premisMethod;
    private final String premisValidationRule;


    public static class Constants {


        public static final String CADES_PREMIS_VALUE = "CAdES_BASELINE_B";
        public static final String XADES_PREMIS_VALUE = "XAdES_BASELINE_B";
        public static final String CADES_PREMIS_VALIDATION_RULE_VALUE = "CAdES";
        public static final String XADES_PREMIS_VALIDATION_RULE_VALUE = "XAdES-1.3.2";


        private Constants() {
            throw new IllegalStateException("Utility class");
        }


    }


    public boolean isEnvelopedSignature() {
        return switch (this) {
            case PKCS7, XADES_DETACHED -> false;
            case PADES, PES_V2 -> true;
            default -> throw new RuntimeException("The signature format should have been evaluated before the isEnvelopedSignature call");
        };
    }


    public static SignatureFormat fromPremisValue(@NotNull String premisValue) {
        return switch (premisValue) {
            case XADES_PREMIS_VALUE -> XADES_DETACHED;
            case CADES_PREMIS_VALUE -> PKCS7;
            default -> throw new LocalizedStatusException(BAD_REQUEST, "message.invalid_detached_signature_format");
        };
    }

}
