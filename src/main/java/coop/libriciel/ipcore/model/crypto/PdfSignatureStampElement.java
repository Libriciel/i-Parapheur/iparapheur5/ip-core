/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.crypto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.PositiveOrZero;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class PdfSignatureStampElement {


    public enum Type {
        TEXT,
        IMAGE
    }


    public enum Font {

        TIMES_ROMAN,
        TIMES_BOLD,
        TIMES_ITALIC,
        TIMES_BOLD_ITALIC,

        HELVETICA,
        HELVETICA_BOLD,
        HELVETICA_OBLIQUE,
        HELVETICA_BOLD_OBLIQUE,

        COURIER,
        COURIER_BOLD,
        COURIER_OBLIQUE,
        COURIER_BOLD_OBLIQUE;

    }


    private Type type;
    private String value;

    private Font font;

    @PositiveOrZero(message = "{message.font_size_should_be_positive}")
    private Integer fontSize;

    private String colorCode;

    @PositiveOrZero(message = "{message.x_coordinate_should_be_positive}")
    private Float x;

    @PositiveOrZero(message = "{message.y_coordinate_should_be_positive}")
    private Float y;

    @Positive(message = "{message.width_should_be_positive}")
    private Float width;

    @Positive(message = "{message.height_should_be_positive}")
    private Float height;

}
