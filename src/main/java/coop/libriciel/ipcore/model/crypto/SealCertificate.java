/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.crypto;

import coop.libriciel.ipcore.utils.TextUtils;
import lombok.*;

import java.util.Map;
import java.util.Optional;


@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SealCertificate extends SealCertificateRepresentation {


    public static final String FIELD_ID = "id";
    public static final String FIELD_NAME = "name";
    public static final String FIELD_EXPIRATION_DATE = "expirationDate";
    public static final String FIELD_SIGNATURE_IMAGE_CONTENT_ID = "signatureImageContentId";
    public static final String FIELD_PUBLIC_CERTIFICATE = "publicCertificateBase64";
    public static final String FIELD_PRIVATE_KEY = "privateKeyBase64";


    private String publicCertificateBase64;
    private String privateKeyBase64;
    private String signatureImageContentId;


    public SealCertificate(String id, Map<String, String> map) {
        super(
                id,
                map.get(FIELD_NAME),
                Optional.ofNullable(map.get(FIELD_EXPIRATION_DATE))
                        .map(s -> TextUtils.deserializeDate(s, TextUtils.ISO8601_DATE_TIME_FORMAT))
                        .orElse(null),
                -1
        );

        this.publicCertificateBase64 = map.get(FIELD_PUBLIC_CERTIFICATE);
        this.privateKeyBase64 = map.get(FIELD_PRIVATE_KEY);
        this.signatureImageContentId = map.get(FIELD_SIGNATURE_IMAGE_CONTENT_ID);
    }


}
