/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.crypto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import lombok.*;


@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SealCertificateDto extends SealCertificateRepresentation {

    public static final String API_DOC_ID = "Seal certificate id";
    public static final String API_DOC_SEAL = "Seal certificate";
    public static final String API_DOC_CERTIFICATE = "Certificate file";
    public static final String API_DOC_IMAGE = "Image file";

    public static final String API_ID_PATH = "sealCertificateId";
    public static final String API_PART_SEAL = "sealCertificate";
    public static final String API_PART_CERTIFICATE = "certificateFile";
    public static final String API_PART_IMAGE = "imageFile";


    @Schema(nullable = true)
    private String signatureImageContentId;


    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY, nullable = true)
    private String password;


    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY, nullable = true)
    private CertificateInformations issuer;


    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY, nullable = true)
    private CertificateInformations subject;


}
