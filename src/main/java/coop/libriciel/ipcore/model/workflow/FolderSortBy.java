/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.workflow;

import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;
import io.swagger.v3.oas.annotations.media.Schema;


@Schema(enumAsRef = true)
public enum FolderSortBy {

    ACTION_TYPE,
    @JsonEnumDefaultValue CREATION_DATE,
    CURRENT_DESK_ID,
    CURRENT_DESK_NAME,
    EMITTER_DESK_ID,
    EMITTER_DESK_NAME,
    EMITTER_USER_ID,
    EMITTER_USER_NAME,
    END_DATE,
    FOLDER_ID,
    LEGACY_ID,
    FOLDER_NAME,
    IS_READ,
    LATE_DATE,
    START_DATE,
    STILL_SINCE_DATE,
    SUBTYPE_ID,
    SUBTYPE_NAME,
    TASK_ID,
    TYPE_ID,
    TYPE_NAME,
    VALIDATION_START_DATE;


    public static class Constants {

        public static final String ACTION_TYPE_VALUE = "ACTION_TYPE";
        public static final String CREATION_DATE_VALUE = "CREATION_DATE";
        public static final String START_DATE_VALUE = "START_DATE";
        public static final String CURRENT_DESK_ID_VALUE = "CURRENT_DESK_ID";
        public static final String CURRENT_DESK_NAME_VALUE = "CURRENT_DESK_NAME";

        // TODO remove this one when the correct value is handled on the preferences side
        public static final String CURRENT_DESK_VALUE = "CURRENT_DESK";

        public static final String EMITTER_DESK_ID_VALUE = "EMITTER_DESK_ID";
        public static final String EMITTER_DESK_NAME_VALUE = "EMITTER_DESK_NAME";
        public static final String EMITTER_USER_ID_VALUE = "EMITTER_USER_ID";
        public static final String EMITTER_USER_NAME_VALUE = "EMITTER_USER_NAME";
        public static final String END_DATE_VALUE = "END_DATE";
        public static final String FOLDER_ID_VALUE = "FOLDER_ID";
        public static final String FOLDER_NAME_VALUE = "FOLDER_NAME";
        public static final String IS_READ_VALUE = "IS_READ";
        public static final String LATE_DATE_VALUE = "LATE_DATE";
        public static final String STILL_SINCE_DATE_VALUE = "STILL_SINCE_DATE";
        public static final String SUBTYPE_ID_VALUE = "SUBTYPE_ID";
        public static final String SUBTYPE_NAME_VALUE = "SUBTYPE_NAME";
        public static final String TASK_ID_VALUE = "TASK_ID";
        public static final String TYPE_ID_VALUE = "TYPE_ID";
        public static final String TYPE_NAME_VALUE = "TYPE_NAME";
        public static final String VALIDATION_START_DATE_VALUE = "VALIDATION_START_DATE";
        public static final String READ_TASK_CURRENT_DESK_ID_VALUE = "READ_TASK_CURRENT_DESK_ID";

        public static final String API_DOC_SORT_BY_VALUES = "Sorting properties are restricted to the FolderSortBy enum values";


        private Constants() {
            throw new IllegalStateException("Utility class");
        }

    }


}
