/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.workflow;

import com.fasterxml.jackson.annotation.JsonProperty;
import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.content.SignatureProof;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;


@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class FolderDto extends FolderListableDto {


    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Schema(accessMode = Schema.AccessMode.WRITE_ONLY)
    protected String typeId;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Schema(accessMode = Schema.AccessMode.WRITE_ONLY)
    protected String subtypeId;

    @Schema(accessMode = READ_ONLY)
    protected List<Task> stepList = new ArrayList<>();

    @Schema(accessMode = READ_ONLY)
    protected List<Document> documentList = new ArrayList<>();

    @Schema(accessMode = READ_ONLY)
    protected List<SignatureProof> signatureProofList = new ArrayList<>();

    @Schema(accessMode = READ_ONLY)
    protected Set<String> readByUserIds = new HashSet<>();


}
