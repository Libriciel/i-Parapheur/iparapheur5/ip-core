/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.workflow;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.function.UnaryOperator;

import static coop.libriciel.ipcore.utils.TextUtils.RESERVED_PREFIX;


@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class WorkflowDefinition {


    public static final String API_ID_DOC_VALUE = "Workflow definition id";
    public static final String API_ID_PATH = "workflowDefinitionId";
    public static final String API_KEY_DOC_VALUE = "Workflow definition key";
    public static final String API_KEY_PATH = "workflowDefinitionKey";

    /**
     * The original, and still internal values.
     */
    public static final String EMITTER_ID_PLACEHOLDER = "##EMITTER##";
    public static final String VARIABLE_DESK_ID_PLACEHOLDER = "##VARIABLE_DESK##";
    public static final String BOSS_OF_ID_PLACEHOLDER = "##BOSS_OF##";

    public static final List<String> GENERIC_VALIDATORS_IDS_PLACEHOLDERS = List.of(
            EMITTER_ID_PLACEHOLDER,
            VARIABLE_DESK_ID_PLACEHOLDER,
            BOSS_OF_ID_PLACEHOLDER
    );

    /**
     * The standardized values, documented in the API.
     * Yet, they are just in the API, translated on-the-fly into the internal ones.
     * We use the other ones internally.
     * <p>
     * TODO:
     *  - Patch the existing values in Workflow, to these standardized ones.
     *  - Replace the internal ones in Web.
     */
    public static final String RESERVED_EMITTER_ID_PLACEHOLDER = RESERVED_PREFIX + "emitter";
    public static final String RESERVED_VARIABLE_DESK_ID_PLACEHOLDER = RESERVED_PREFIX + "variable_desk";
    public static final String RESERVED_BOSS_OF_ID_PLACEHOLDER = RESERVED_PREFIX + "boss_of";

    public static final List<String> RESERVED_GENERIC_VALIDATORS_IDS_PLACEHOLDERS = List.of(
            RESERVED_EMITTER_ID_PLACEHOLDER,
            RESERVED_VARIABLE_DESK_ID_PLACEHOLDER,
            RESERVED_BOSS_OF_ID_PLACEHOLDER
    );


    public static final UnaryOperator<String> GENERIC_VALIDATORS_INTERNAL_TO_PUBLIC_IDS_CONVERTER =
            value -> (value == null)
                     ? null
                     : switch (value) {
                         case EMITTER_ID_PLACEHOLDER -> RESERVED_EMITTER_ID_PLACEHOLDER;
                         case VARIABLE_DESK_ID_PLACEHOLDER -> RESERVED_VARIABLE_DESK_ID_PLACEHOLDER;
                         case BOSS_OF_ID_PLACEHOLDER -> RESERVED_BOSS_OF_ID_PLACEHOLDER;
                         default -> value;
                     };

    public static final UnaryOperator<String> GENERIC_VALIDATORS_PUBLIC_TO_INTERNAL_IDS_CONVERTER =
            value -> (value == null)
                     ? null
                     : switch (value) {
                         case RESERVED_EMITTER_ID_PLACEHOLDER -> EMITTER_ID_PLACEHOLDER;
                         case RESERVED_VARIABLE_DESK_ID_PLACEHOLDER -> VARIABLE_DESK_ID_PLACEHOLDER;
                         case RESERVED_BOSS_OF_ID_PLACEHOLDER -> BOSS_OF_ID_PLACEHOLDER;
                         default -> value;
                     };


    private String id;
    private String key;
    private String name;
    private Integer version = 0;
    private Boolean isSuspended = false;
    private long usageCount;
    private List<StepDefinition> steps;

    private DeskRepresentation finalDesk = new DeskRepresentation(EMITTER_ID_PLACEHOLDER);
    private List<DeskRepresentation> finalNotifiedDesks;


    public WorkflowDefinition(@NotNull String id,
                              @NotNull String key,
                              @NotNull String name,
                              @Nullable List<StepDefinition> steps,
                              @Nullable DeskRepresentation finalDesk,
                              boolean isSuspended,
                              @Nullable Integer version,
                              long usageCount) {
        this.id = id;
        this.key = key;
        this.name = name;
        this.version = version;
        this.isSuspended = isSuspended;
        this.usageCount = usageCount;
        this.steps = steps;
        this.finalDesk = finalDesk;
    }


}
