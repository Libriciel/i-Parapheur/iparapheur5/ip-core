/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.workflow.ipWorkflow;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.requests.UserRepresentation;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Type;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.utils.FolderUtils;
import coop.libriciel.ipcore.utils.TextUtils;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static coop.libriciel.ipcore.services.workflow.IpWorkflowService.METADATA_DRAFT_CREATION_DATE;
import static coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface.*;
import static coop.libriciel.ipcore.utils.CollectionUtils.popValue;
import static coop.libriciel.ipcore.utils.TextUtils.ISO8601_DATE_TIME_FORMAT;
import static coop.libriciel.ipcore.utils.TextUtils.firstNotEmpty;
import static java.util.Collections.singletonList;


@Log4j2
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class IpWorkflowInstance extends Folder {

    private static final String API_ID = "id";
    private static final String API_START_TIME = "draftCreationDate";
    private static final String API_VARIABLES = "variables";
    private static final String API_BUSINESS_KEY = "businessKey";
    private static final String API_ORIGIN_GROUP = "originGroup";
    private static final String API_FINAL_GROUP = "finalGroup";
    private static final String API_TASK_LIST = "taskList";


    @JsonProperty(API_ID)
    private void parseId(String id) {this.id = id;}


    @JsonProperty(API_START_TIME)
    private void parseCreationDate(String draftCreationDate) {this.draftCreationDate = TextUtils.deserializeDate(draftCreationDate, ISO8601_DATE_TIME_FORMAT);}


    @JsonProperty(API_BUSINESS_KEY)
    private void parseContentId(String contentId) {this.contentId = contentId;}


    @JsonProperty(API_ORIGIN_GROUP)
    private void parseOriginDeskId(String originDeskId) {this.originDesk = new DeskRepresentation(originDeskId, null);}


    @JsonProperty(API_FINAL_GROUP)
    private void parseFinalDeskId(String finalDeskId) {this.finalDesk = new DeskRepresentation(finalDeskId, null);}


    @JsonProperty(API_TASK_LIST)
    private void parseStepList(List<IpWorkflowTask> taskList) {
        this.stepList = new ArrayList<>(taskList);
        this.state = FolderUtils.computeFolderStateFromTaskList(taskList);
    }


    @JsonProperty(API_VARIABLES)
    public void parseNestedVariables(Map<String, String> variables) {
        draftCreationDate = TextUtils.deserializeDate(popValue(variables, METADATA_DRAFT_CREATION_DATE), ISO8601_DATE_TIME_FORMAT);
        contentId = firstNotEmpty(contentId, variables.get(META_CONTENT_ID));
        type = Optional.ofNullable(popValue(variables, META_TYPE_ID)).map(id -> Type.builder().id(id).build()).orElse(null);
        subtype = Optional.ofNullable(popValue(variables, META_SUBTYPE_ID)).map(id -> Subtype.builder().id(id).build()).orElse(null);
        premisNodeId = popValue(variables, META_PREMIS_NODE_ID);
        metadata = variables;
    }


    public IpWorkflowInstance(@NotNull IpWorkflowTask task) {
        super();

        this.legacyId = task.getLegacyId();
        this.visibility = task.getVisibility();
        this.id = task.getFolderId();
        this.name = task.getFolderName();
        this.dueDate = task.getFolderDueDate();

        parseNestedVariables(task.getMetadata());
        this.draftCreationDate = task.getDraftCreationDate();
        this.metadata = task.getMetadata();
        this.type = task.getFolderTypeId() != null ? Type.builder().id(task.getFolderTypeId()).build() : null;
        this.subtype = task.getFolderSubtypeId() != null ? Subtype.builder().id(task.getFolderSubtypeId()).build() : null;
        this.stepList = singletonList(task);
        this.readByUserIds = task.getReadByUserIds();
        this.isReadByCurrentUser = task.isReadByCurrentUser();
        this.originDesk = new DeskRepresentation(task.getFolderOriginDeskId());
        this.originUser = new UserRepresentation(task.getFolderOriginUserId());
        this.finalDesk = new DeskRepresentation(task.getFolderFinalDeskId());
        this.state = FolderUtils.computeFolderStateFromTask(task);
    }


}
