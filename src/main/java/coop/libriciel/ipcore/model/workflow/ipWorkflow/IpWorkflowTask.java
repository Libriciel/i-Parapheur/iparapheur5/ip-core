/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
package coop.libriciel.ipcore.model.workflow.ipWorkflow;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.requests.UserRepresentation;
import coop.libriciel.ipcore.model.workflow.Action;
import coop.libriciel.ipcore.model.workflow.FolderVisibility;
import coop.libriciel.ipcore.model.workflow.State;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.services.database.PostgresService;
import coop.libriciel.ipcore.utils.CollectionUtils;
import coop.libriciel.ipcore.utils.TextUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jooq.Record;

import java.util.*;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.WRITE_ONLY;
import static coop.libriciel.ipcore.model.workflow.Action.*;
import static coop.libriciel.ipcore.model.workflow.FolderSortBy.Constants.*;
import static coop.libriciel.ipcore.model.workflow.State.*;
import static coop.libriciel.ipcore.services.database.PostgresService.*;
import static coop.libriciel.ipcore.services.workflow.IpWorkflowService.*;
import static coop.libriciel.ipcore.utils.CollectionUtils.*;
import static coop.libriciel.ipcore.utils.TextUtils.*;
import static java.lang.Boolean.TRUE;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toMap;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.firstNonEmpty;


@Data
@Log4j2
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class IpWorkflowTask extends Task {

    // Flowable API JSON variables

    private static final String API_ID = "id";
    private static final String API_INSTANCE_ID = "instanceId";
    private static final String API_START_TIME = "draftCreationDate";
    private static final String API_INSTANCE_NAME = "instanceName";
    private static final String API_INSTANCE_LEGACY_ID = "legacyId";
    private static final String API_INSTANCE_VISIBILITY = "visibility";
    private static final String API_CREATE_TIME = "createTime";
    private static final String API_END_TIME = "endTime";
    private static final String API_DUE_DATE = "dueDate";
    private static final String API_STATE = "state";
    private static final String API_EXPECTED_ACTION = "expectedAction";
    private static final String API_PERFORMED_ACTION = "performedAction";
    private static final String API_PENDING = "pending";
    private static final String API_VARIABLES = "variables";
    private static final String API_CANDIDATE_GROUPS = "candidateGroups";
    private static final String API_NOTIFIED_GROUPS = "notifiedGroups";
    private static final String API_ASSIGNEE = "assignee";
    private static final String API_DELEGATED_BY_GROUP_ID = "delegatedByGroupId";

    public static final String API_VAR_KEY_PUBLIC_ANNOTATION = INTERNAL_PREFIX + "public_annotation";
    public static final String API_VAR_KEY_PRIVATE_ANNOTATION = INTERNAL_PREFIX + "private_annotation";
    public static final String API_VAR_KEY_CURRENT_CANDIDATE_GROUPS = "current_candidate_groups";


    private @JsonIgnore Boolean isPending = null;
    private @JsonIgnore String folderName;
    private @JsonIgnore String legacyId;
    private @JsonIgnore FolderVisibility visibility;
    private @JsonIgnore String folderId;
    private @JsonIgnore String folderTypeId;
    private @JsonIgnore String folderSubtypeId;
    private @JsonIgnore String folderOriginUserId;

    @JsonProperty(access = WRITE_ONLY)
    private @JsonAlias("originGroup") String folderOriginDeskId;

    @JsonProperty(access = WRITE_ONLY)
    private @JsonAlias("finalGroup") String folderFinalDeskId;

    private @JsonIgnore Date folderDueDate;
    private @JsonIgnore String expectedActionCurrentDeskId;

    private Action performedAction = null;
    private String externalSignatureProcedureId = null;


    @JsonProperty(API_ID)
    private void parseId(String id) {this.id = id;}


    @JsonProperty(API_INSTANCE_NAME)
    private void parseFolderName(String folderName) {this.folderName = folderName;}


    @JsonProperty(API_INSTANCE_LEGACY_ID)
    private void parseLegacyId(String legacyId) {this.legacyId = legacyId;}


    @JsonProperty(API_INSTANCE_VISIBILITY)
    private void parseVisibility(FolderVisibility visibility) {this.visibility = visibility;}


    @JsonProperty(API_INSTANCE_ID)
    private void parseFolderId(String folderId) {this.folderId = folderId;}


    @JsonProperty(API_START_TIME)
    @JsonAlias(API_CREATE_TIME)
    private void parseBeginDate(Date beginDate) {this.beginDate = beginDate;}


    @JsonProperty(API_END_TIME)
    private void parseEndDate(Date date) {this.date = date;}


    @JsonProperty(API_DUE_DATE)
    private void parseDueDate(Date date) {this.folderDueDate = date;}


    @JsonProperty(API_STATE)
    private void parseState(State state) {
        this.state = ObjectUtils.firstNonNull(state, this.state);
        computeState();
    }


    @JsonProperty(API_EXPECTED_ACTION)
    private void parseCurrentAction(Action action) {
        this.action = action;
        computeState();
    }


    @JsonProperty(API_PERFORMED_ACTION)
    private void parsePerformedAction(Action action) {
        this.performedAction = action;
        computeState();
    }


    @JsonProperty(API_PENDING)
    private void parsePending(boolean pending) {
        this.isPending = pending;
        computeState();
    }


    @JsonProperty(API_CANDIDATE_GROUPS)
    private void parseCurrentCandidateGroups(List<String> candidateGroupsId) {
        this.desks = candidateGroupsId
                .stream()
                .map(DeskRepresentation::new)
                .toList();
    }


    @JsonProperty(API_NOTIFIED_GROUPS)
    private void parseNotifiedGroups(List<String> notifiedGroupsId) {
        this.notifiedDesks = Optional.ofNullable(notifiedGroupsId)
                .orElse(emptyList())
                .stream()
                .map(DeskRepresentation::new)
                .toList();
    }


    @JsonProperty(API_ASSIGNEE)
    private void parseAssignee(String assigneeId) {
        this.user = Optional.ofNullable(assigneeId)
                .map(UserRepresentation::new)
                .orElse(null);
    }


    @JsonProperty(API_DELEGATED_BY_GROUP_ID)
    private void parseDelegatedByGroupId(String groupId) {
        this.delegatedByDesk = Optional.ofNullable(groupId)
                .map(DeskRepresentation::new)
                .orElse(null);
    }


    @JsonProperty(API_VARIABLES)
    public void parseNestedVariables(Map<String, String> variables) {
        publicAnnotation = popValue(variables, METADATA_PUBLIC_ANNOTATION);
        privateAnnotation = popValue(variables, METADATA_PRIVATE_ANNOTATION);

        // The 5.2+ columned request already hava the date from the SQL request.
        // The 5.1- has its own logic, and fetch it from a metadata.
        // TODO: remove this metadata, and check the retro-compatibility with old folders.
        draftCreationDate = ObjectUtils.firstNonNull(
                draftCreationDate,
                TextUtils.deserializeDate(popValue(variables, METADATA_DRAFT_CREATION_DATE), ISO8601_DATE_TIME_FORMAT)
        );

        folderTypeId = popValue(variables, META_TYPE_ID);
        folderSubtypeId = popValue(variables, META_SUBTYPE_ID);
        publicCertificateBase64 = popValue(variables, METADATA_PUBLIC_CERTIFICATE_BASE64);
        externalSignatureProcedureId = popValue(variables, METADATA_TRANSACTION_ID);
        metadata = variables;
    }


    void computeState() {

        // Simple cases

        if (isPending == TRUE) {
            this.state = PENDING;
            return;
        }

        if (ObjectUtils.anyNull(action, performedAction)) {
            // We'll compute it later, once every parameter will be set
            return;
        }

        if (action == READ) {
            this.state = VALIDATED;
            return;
        }

        if ((action == performedAction) || (action == SIGNATURE && performedAction == PAPER_SIGNATURE)) {
            this.state = VALIDATED;
        }

        this.state = switch (performedAction) {
            case TRANSFER -> TRANSFERRED;
            case ASK_SECOND_OPINION -> SECONDED;
            case BYPASS -> BYPASSED;
            case REJECT -> REJECTED;
            default -> this.state;
        };
    }


    public IpWorkflowTask(@NotNull Record record) {

        id = record.get(TASK_ID_VALUE, String.class);
        folderDueDate = record.get(LATE_DATE_VALUE, Date.class);
        draftCreationDate = record.get(CREATION_DATE_VALUE, Date.class);
        beginDate = record.get(START_DATE_VALUE, Date.class);
        folderId = record.get(FOLDER_ID_VALUE, String.class);
        folderName = record.get(FOLDER_NAME_VALUE, String.class);

        action = Optional.ofNullable(optValue(record, SQL_TASK_NAME, String.class))
                .map(Action::fromString)
                .orElse(VISA);

        desks = Optional.ofNullable(firstNonEmpty(
                        optValue(record, READ_TASK_CURRENT_DESK_ID_VALUE, String.class),
                        optValue(record, CURRENT_DESK_ID_VALUE, String.class)
                ))
                .filter(StringUtils::isNotEmpty)
                .map(id -> new DeskRepresentation(id, null))
                .map(Collections::singletonList)
                .orElse(emptyList());

        folderOriginDeskId = optValue(record, EMITTER_DESK_ID_VALUE, String.class);
        folderOriginUserId = optValue(record, EMITTER_USER_ID_VALUE, String.class);

        expectedActionCurrentDeskId = optValue(record, READ_TASK_CURRENT_DESK_ID_VALUE, String.class);

        metadata = Optional.ofNullable(optValue(record, SQL_VARIABLES, String[][].class))
                .map(CollectionUtils::parseMetadata)
                .orElse(new HashMap<>());

        parseNestedVariables(metadata);

        isReadByCurrentUser = Optional.ofNullable(optValue(record, IS_READ_VALUE, Boolean.class))
                .orElse(false);

        Optional.ofNullable(optValue(record, TYPE_ID_VALUE, String.class))
                .ifPresent(id -> folderTypeId = id);

        Optional.ofNullable(optValue(record, SUBTYPE_ID_VALUE, String.class))
                .ifPresent(id -> folderSubtypeId = id);

        metadata.putAll(
                record.intoMap().entrySet().stream()
                        .filter(entry -> StringUtils.isNotEmpty(entry.getKey()))
                        .filter(entry -> entry.getKey().startsWith(PostgresService.buildSqlMetadataName(EMPTY)))
                        .collect(toMap(
                                entry -> entry.getKey()
                                        // TODO: Set a cleaner way to do this
                                        .replaceAll(PostgresService.buildSqlMetadataName(EMPTY), EMPTY)
                                        .replaceAll("_", "-"),
                                entry -> Optional.ofNullable(entry.getValue())
                                        .map(Object::toString)
                                        .orElse(EMPTY)
                        ))
        );
    }


}
