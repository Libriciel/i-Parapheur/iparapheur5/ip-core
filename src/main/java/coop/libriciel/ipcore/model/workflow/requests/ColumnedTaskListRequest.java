/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.workflow.requests;

import coop.libriciel.ipcore.model.workflow.State;

import java.util.List;
import java.util.Map;


public record ColumnedTaskListRequest(

        String searchTerm,
        String typeId,
        String subtypeId,
        String legacyId,

        Long createdAfter,
        Long createdBefore,
        Long stillSinceTime,

        List<String> fromDeskIds,
        List<String> includeMetadata,
        Map<String, List<String>> metadataValueFilterMap,
        State state

) {}
