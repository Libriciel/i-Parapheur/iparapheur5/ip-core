/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.workflow.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import jakarta.validation.constraints.Size;

import java.util.Map;

import static coop.libriciel.ipcore.utils.ApiUtils.ANNOTATION_MAX_LENGTH;
import static coop.libriciel.ipcore.utils.ApiUtils.ANNOTATION_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE;


@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class SimpleTaskParams {

    public static final String API_NAME = "body";
    public static final String API_VALUE =
            """
            The public annotation is visible to any users, and will be stored in task history.
            It is mandatory for any reject, and should be longer than 3 characters.

            The private annotation will only be sent to the workflow's next user.
            It won't be archived anywhere.
            """;


    @Size(max = ANNOTATION_MAX_LENGTH, message = ANNOTATION_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
    protected String publicAnnotation;

    @Size(max = ANNOTATION_MAX_LENGTH, message = ANNOTATION_SIZE_JAVAX_VALIDATION_ERROR_MESSAGE)
    protected String privateAnnotation;

    @Schema(nullable = true)
    protected Map<String, String> metadata;

}
