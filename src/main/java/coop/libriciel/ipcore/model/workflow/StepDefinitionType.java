/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.workflow;

import io.swagger.v3.oas.annotations.media.Schema;
import org.jetbrains.annotations.NotNull;


@Schema(enumAsRef = true)
public enum StepDefinitionType {


    ARCHIVE,
    EXTERNAL_SIGNATURE,
    IPNG,
    SEAL,
    SECURE_MAIL,
    SIGNATURE,
    VISA;


    public @NotNull Action toAction() {
        return switch(this) {
            case ARCHIVE -> Action.ARCHIVE;
            case EXTERNAL_SIGNATURE -> Action.EXTERNAL_SIGNATURE;
            case IPNG -> Action.IPNG;
            case SEAL -> Action.SEAL;
            case SECURE_MAIL -> Action.SECURE_MAIL;
            case SIGNATURE -> Action.SIGNATURE;
            case VISA -> Action.VISA;
        };
    }


}
