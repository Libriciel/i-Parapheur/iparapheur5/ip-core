/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.workflow;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.MetadataDto;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class StepDefinitionDto {


    private StepDefinitionType type;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY)
    private List<String> validatingDeskIds;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private List<DeskRepresentation> validatingDesks;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY)
    private List<String> notifiedDeskIds;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private List<DeskRepresentation> notifiedDesks;

    /**
     * Full Validation/Rejection Metadata objects are used here.
     * We prefer the real DTO over the Representation ones.
     * These lists won't be that long, and already having the restricted values will prevent many requests.
     */

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY)
    private List<String> mandatoryValidationMetadataIds;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private List<MetadataDto> mandatoryValidationMetadata;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Schema(accessMode = AccessMode.WRITE_ONLY)
    private List<String> mandatoryRejectionMetadataIds;

    @JsonProperty(access = Access.READ_ONLY)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private List<MetadataDto> mandatoryRejectionMetadata;

    private StepDefinitionParallelType parallelType;


}
