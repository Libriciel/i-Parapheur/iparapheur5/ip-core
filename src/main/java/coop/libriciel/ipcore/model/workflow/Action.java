/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.workflow;

import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;

import javax.annotation.PropertyKey;
import java.util.Set;


@Log4j2
@Getter
@AllArgsConstructor
@Schema(enumAsRef = true)
public enum Action {


    @JsonEnumDefaultValue
    VISA("message.action_visa", "message.action_past_visa"),
    SEAL("message.action_seal", "message.action_past_seal"),
    SIGNATURE("message.action_signature", "message.action_past_signature"),
    PAPER_SIGNATURE("message.action_paper_signature", "message.action_past_paper_signature"),
    REJECT("message.action_reject", "message.action_past_reject"),
    EXTERNAL_SIGNATURE("message.action_external_signature", "message.action_past_external_signature"),
    SECURE_MAIL("message.action_secure_mail", "message.action_past_secure_mail"),
    IPNG("message.action_ipng", "message.action_past_ipng"),
    IPNG_RETURN("message.action_ipng_return", "message.action_past_ipng_return"),
    UNDO("message.action_undo", "message.action_past_undo"),
    TRANSFER("message.action_transfer", "message.action_past_transfer"),
    SECOND_OPINION("message.action_second_opinion", "message.action_past_second_opinion"),
    ASK_SECOND_OPINION("message.action_ask_second_opinion", "message.action_past_ask_second_opinion"),
    CREATE("message.action_create", "message.action_past_create"),
    START("message.action_start", "message.action_past_start"),
    ARCHIVE("message.action_archive", "message.action_past_archive"),
    CHAIN("message.action_chain", "message.action_past_chain"),
    RECYCLE("message.action_recycle", "message.action_past_recycle"),
    DELETE("message.action_delete", "message.action_past_delete"),
    BYPASS("message.action_bypass", "message.action_past_bypass"),
    UPDATE("message.action_edit", "message.action_past_edit"),
    READ("message.action_read", "message.action_past_read"),
    UNKNOWN("message.action_unknown", "message.action_unknown");


    private final @PropertyKey String messageKey;
    private final @PropertyKey String pastMessageKey;


    // <editor-fold desc="Utils">


    public boolean equalsOneOf(Action... candidates) {

        for (Action candidate : candidates) {
            if (this == candidate) {
                return true;
            }
        }

        return false;
    }


    public static @NotNull Action fromString(@NotNull String string) {
        return switch (string) {
            case "main_archive", "i_Parapheur_internal_archive" -> ARCHIVE;
            case "main_delete" -> DELETE;
            case "external_signature_fillingform", "external_signature_waiting" -> EXTERNAL_SIGNATURE;
            case "main_seal", "i_Parapheur_internal_seal" -> SEAL;
            case "main_second_opinion" -> SECOND_OPINION;
            case "main_signature", "i_Parapheur_internal_signature" -> SIGNATURE;
            case "main_start", "i_Parapheur_internal_start" -> START;
            case "main_visa", "i_Parapheur_internal_visa" -> VISA;
            case "undo" -> UNDO;
            default -> {
                log.warn("Unknown action string:{}", string);
                yield VISA;
            }
        };
    }


    public static boolean isExternal(@NotNull Action action) {
        return Set.of(EXTERNAL_SIGNATURE, IPNG, SECURE_MAIL)
                .contains(action);
    }


    public static boolean isSecondary(@NotNull Action action) {
        return Set.of(UNDO, READ)
                .contains(action);
    }


    // </editor-fold desc="Utils">


}
