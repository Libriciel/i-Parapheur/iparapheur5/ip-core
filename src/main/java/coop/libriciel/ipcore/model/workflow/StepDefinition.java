/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
package coop.libriciel.ipcore.model.workflow;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.Metadata;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;

import static coop.libriciel.ipcore.model.workflow.StepDefinitionParallelType.OR;
import static java.util.Collections.emptyList;


@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class StepDefinition {


    private Action type;
    private StepDefinitionParallelType parallelType = OR;
    private List<DeskRepresentation> validatingDesks;
    private List<DeskRepresentation> notifiedDesks;
    private List<Metadata> mandatoryValidationMetadata;
    private List<Metadata> mandatoryRejectionMetadata;


    @JsonProperty("validators")
    public void parseValidators(@Nullable List<String> validatorsIds) {
        validatingDesks = Optional.ofNullable(validatorsIds)
                .orElse(emptyList())
                .stream()
                .map(id -> new DeskRepresentation(id, null))
                .toList();
    }


    @JsonProperty("notifiedGroups")
    public void parseNotifiedDesks(@Nullable List<String> notifiedDeskIds) {
        notifiedDesks = Optional.ofNullable(notifiedDeskIds)
                .orElse(emptyList())
                .stream()
                .map(id -> new DeskRepresentation(id, null))
                .toList();
    }


    @JsonProperty("mandatoryValidationMetadataIds")
    public void parseMandatoryValidationMetadata(@Nullable List<String> mandatoryValidationMetadataIds) {
        mandatoryValidationMetadata = Optional.ofNullable(mandatoryValidationMetadataIds)
                .orElse(emptyList())
                .stream()
                .map(id -> Metadata.builder().id(id).build())
                .toList();
    }


    @JsonProperty("mandatoryRejectionMetadataIds")
    public void parseMandatoryRejectionMetadata(@Nullable List<String> mandatoryRejectionMetadataIds) {
        mandatoryRejectionMetadata = Optional.ofNullable(mandatoryRejectionMetadataIds)
                .orElse(emptyList())
                .stream()
                .map(id -> Metadata.builder().id(id).build())
                .toList();
    }


}
