/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller.admin;

import com.fasterxml.jackson.core.JsonProcessingException;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.securemail.SecureMailEntity;
import coop.libriciel.ipcore.model.securemail.SecureMailServer;
import coop.libriciel.ipcore.services.resolvers.SecureMailServerResolver.SecureMailServerResolved;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.services.securemail.SecureMailServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.PaginatedList;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static coop.libriciel.ipcore.IpCoreApplication.API_V1;
import static coop.libriciel.ipcore.IpCoreApplication.HIDE_UNSTABLE_API;
import static coop.libriciel.ipcore.utils.PaginatedList.*;
import static org.springframework.http.HttpStatus.*;


@Log4j2
@RestController
@PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
public class AdminSecureMailController {


    private static final int MAX_SECURE_MAIL_SERVER_PER_TENANT_COUNT = 50;

    private final SecureMailServiceInterface secureMailService;


    @Autowired
    public AdminSecureMailController(SecureMailServiceInterface secureMailService) {
        this.secureMailService = secureMailService;
    }


    @PostMapping(API_V1 + "/admin/tenant/{tenantId}/secureMail/server/test")
    @Operation(summary = "Test server configuration", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given tenantId does not exist"),
    })
    public boolean testServer(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                              @PathVariable(name = Tenant.API_PATH) String tenantId,
                              @TenantResolved Tenant tenant,
                              @RequestBody SecureMailServer secureMailServer) throws JsonProcessingException {

        return this.secureMailService.testServer(secureMailServer);
    }


    @PostMapping(API_V1 + "/admin/tenant/{tenantId}/secureMail/server")
    @Operation(summary = "Create server", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given tenantId does not exist"),
            @ApiResponse(responseCode = "409", description = "This type already exists"),
            @ApiResponse(responseCode = "507", description = "The external config max limit (" + MAX_SECURE_MAIL_SERVER_PER_TENANT_COUNT + ") has been reached.\n" +
                    "Consider deleting unused ones.")
    })
    public SecureMailServer createSecureMailServer(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                   @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                   @TenantResolved Tenant tenant,
                                                   @RequestBody SecureMailServer secureMailServer) throws JsonProcessingException {

        return this.secureMailService.createServer(secureMailServer);
    }


    @GetMapping(API_V1 + "/admin/tenant/{tenantId}/secureMail/server")
    @Operation(summary = "Get servers", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given tenantId does not exist")
    })
    public PaginatedList<SecureMailServer> getSecureMailServers(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                                @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                                @TenantResolved Tenant tenant,
                                                                @Parameter(description = API_DOC_PAGE)
                                                                @RequestParam(required = false, defaultValue = API_DOC_PAGE_DEFAULT) int page,
                                                                @Parameter(description = API_DOC_PAGE_SIZE)
                                                                @RequestParam(required = false, defaultValue = API_DOC_PAGE_SIZE_DEFAULT) int pageSize) {

        PaginatedList<SecureMailServer> paginatedList = new PaginatedList<>();
        paginatedList.setData(this.secureMailService.findAllServers(tenant.getId()));
        paginatedList.setPageSize(pageSize);
        paginatedList.setPage(page);
        return paginatedList;
    }


    @GetMapping(API_V1 + "/admin/tenant/{tenantId}/secureMail/server/{serverId}")
    @Operation(summary = "Get server", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given tenantId does not exist")
    })
    public SecureMailServer getSecureMailServerById(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                    @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                    @TenantResolved Tenant tenant,
                                                    @PathVariable(name = SecureMailServer.API_PATH) String serverId,
                                                    @SecureMailServerResolved SecureMailServer secureMailServer) {

        return secureMailServer;
    }


    @PostMapping(API_V1 + "/admin/tenant/{tenantId}/secureMail/server/{serverId}")
    @Operation(summary = "edit server", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given tenantId does not exist")
    })
    public SecureMailServer editSecureMailServer(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                 @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                 @TenantResolved Tenant tenant,
                                                 @PathVariable(name = SecureMailServer.API_PATH) String serverId,
                                                 @SecureMailServerResolved SecureMailServer secureMailServerRetrieved,
                                                 @RequestBody SecureMailServer secureMailServer) throws JsonProcessingException {

        return this.secureMailService.updateServerObject(tenant.getId(), secureMailServer);
    }


    @PatchMapping(API_V1 + "/admin/tenant/{tenantId}/secureMail/server/{serverId}")
    @Operation(summary = "edit server object", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given tenantId does not exist")
    })
    public SecureMailServer editSecureMailServerObject(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                       @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                       @TenantResolved Tenant tenant,
                                                       @PathVariable(name = SecureMailServer.API_PATH) String serverId,
                                                       @SecureMailServerResolved SecureMailServer secureMailServer,
                                                       Long id,
                                                       String url,
                                                       String login,
                                                       String password,
                                                       String sslPublicKey,
                                                       int entity,
                                                       String type) {

        try {
            return this.secureMailService.updateServer(tenant.getId(), id, url, login, password, sslPublicKey, entity, type);
        } catch (JSONException e) {
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE, "message.json_parsing_exception");
        }
    }


    @DeleteMapping(API_V1 + "/admin/tenant/{tenantId}/secureMail/server/{serverId}")
    @Operation(summary = "Delete server", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "NO_CONTENT"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given tenantId does not exist")
    })
    public void deleteSecureMailServer(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                       @PathVariable(name = Tenant.API_PATH) String tenantId,
                                       @TenantResolved Tenant tenant,
                                       @PathVariable(name = SecureMailServer.API_PATH) String serverId,
                                       @SecureMailServerResolved SecureMailServer secureMailServer) {

        this.secureMailService.deleteServer(tenant.getId(), Long.valueOf(serverId));
    }


    @PostMapping(API_V1 + "/admin/tenant/{tenantId}/secureMail/server/entities")
    @Operation(summary = "Get server", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given tenantId does not exist")
    })
    public List<SecureMailEntity> findAllPastellEntities(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                         @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                         @TenantResolved Tenant tenant,
                                                         @RequestBody SecureMailServer secureMailServer) throws JsonProcessingException {

        return this.secureMailService.findAllPastellEntities(secureMailServer);
    }


}
