/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller.admin;

import coop.libriciel.ipcore.model.database.*;
import coop.libriciel.ipcore.services.database.ExternalSignatureConfigRepository;
import coop.libriciel.ipcore.services.externalsignature.ExternalSignatureInterface;
import coop.libriciel.ipcore.services.resolvers.ExternalSignatureConfigResolver.ExternalSignatureConfigResolved;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;

import static coop.libriciel.ipcore.IpCoreApplication.API_INTERNAL;
import static coop.libriciel.ipcore.IpCoreApplication.API_V1;
import static coop.libriciel.ipcore.model.stats.StatsCategory.EXTERNAL_SIGNATURE_CONFIG;
import static coop.libriciel.ipcore.model.workflow.Action.*;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.RequestUtils.PREAUTHORIZE_TENANT_ADMIN_OR_MORE;
import static org.springframework.http.HttpStatus.*;


@Log4j2
@RestController
@Tag(name = "admin-external-signature", description = "Reserved operations on External Signature Configurations")
public class AdminExternalSignatureController {


    static final int MAX_EXTERNAL_SIGNATURE_CONFIG_PER_TENANT_COUNT = 50;


    // <editor-fold desc="Beans">


    private final ExternalSignatureConfigRepository externalSignatureConfigRepository;
    private final ExternalSignatureInterface externalSignatureService;
    private final ModelMapper modelMapper;
    private final StatsServiceInterface statsService;


    @Autowired
    public AdminExternalSignatureController(ExternalSignatureConfigRepository externalSignatureConfigRepository,
                                            ExternalSignatureInterface externalSignatureService,
                                            ModelMapper modelMapper,
                                            StatsServiceInterface statsService) {
        this.externalSignatureConfigRepository = externalSignatureConfigRepository;
        this.externalSignatureService = externalSignatureService;
        this.modelMapper = modelMapper;
        this.statsService = statsService;
    }


    // </editor-fold desc="Beans">


    // <editor-fold desc="External signature config CRUDL">


    @PostMapping(API_V1 + "/admin/tenant/{tenantId}/externalSignature/config")
    @Operation(summary = "Create an external signature config")
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_201),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_409, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_507, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public ExternalSignatureConfigDto createExternalSignatureConfig(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                                    @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                                    @TenantResolved Tenant tenant,
                                                                    @RequestBody ExternalSignatureConfigDto request) {

        log.info("createExternalSignatureConfig tenantId:{}", tenantId);

        // Integrity check

        long existingConfigCount = externalSignatureConfigRepository.countAllByTenantId(tenant.getId());
        if (existingConfigCount >= MAX_EXTERNAL_SIGNATURE_CONFIG_PER_TENANT_COUNT) {
            throw new LocalizedStatusException(
                    INSUFFICIENT_STORAGE,
                    "message.already_n_external_signature_configurations_maximum_reached",
                    MAX_EXTERNAL_SIGNATURE_CONFIG_PER_TENANT_COUNT
            );
        }

        ExternalSignatureConfig newExternalSignatureConfig = modelMapper.map(request, ExternalSignatureConfig.class);
        newExternalSignatureConfig.setId(UUID.randomUUID().toString());
        newExternalSignatureConfig.setTenant(tenant);

        // Actual register

        ExternalSignatureConfig createdExternalSignatureConfig = externalSignatureConfigRepository.save(newExternalSignatureConfig);
        statsService.registerAdminAction(tenant, EXTERNAL_SIGNATURE_CONFIG, CREATE, createdExternalSignatureConfig.getId());
        return modelMapper.map(createdExternalSignatureConfig, ExternalSignatureConfigDto.class);
    }


    @GetMapping(API_V1 + "/admin/tenant/{tenantId}/externalSignature/config/{configId}")
    @Operation(summary = "Get a full external signature config description")
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public ExternalSignatureConfigDto getExternalSignatureConfigById(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                                     @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                                     @TenantResolved Tenant tenant,
                                                                     @PathVariable String configId,
                                                                     @ExternalSignatureConfigResolved ExternalSignatureConfig externalSignatureConfig) {

        log.debug("getExternalSignatureConfigById configId:{}", externalSignatureConfig.getId());

        return modelMapper.map(externalSignatureConfig, ExternalSignatureConfigDto.class);
    }


    @PutMapping(API_V1 + "/admin/tenant/{tenantId}/externalSignature/config/{configId}")
    @Operation(summary = "Update an external signature config")
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public ExternalSignatureConfigDto editExternalSignatureConfig(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                                  @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                                  @TenantResolved Tenant tenant,
                                                                  @PathVariable String configId,
                                                                  @RequestBody ExternalSignatureConfigDto request,
                                                                  @ExternalSignatureConfigResolved ExternalSignatureConfig currentExternalSignatureConfig) {

        log.info("editExternalSignatureConfig id:{}", configId);

        ExternalSignatureConfig updatedExternalSignatureConfig = modelMapper.map(request, ExternalSignatureConfig.class);

        updatedExternalSignatureConfig.setId(currentExternalSignatureConfig.getId());
        updatedExternalSignatureConfig.setTransactionIds(currentExternalSignatureConfig.getTransactionIds());
        updatedExternalSignatureConfig.setTenant(tenant);

        // We allow edits without touching the WRITE_ONLY params.

        if (StringUtils.isEmpty(request.getPassword())) {
            request.setPassword(currentExternalSignatureConfig.getPassword());
        }

        if (StringUtils.isEmpty(request.getToken())) {
            request.setToken(currentExternalSignatureConfig.getToken());
        }

        // Actual save

        ExternalSignatureConfig config = externalSignatureConfigRepository.save(updatedExternalSignatureConfig);
        statsService.registerAdminAction(currentExternalSignatureConfig.getTenant(), EXTERNAL_SIGNATURE_CONFIG, UPDATE, config.getId());

        return modelMapper.map(updatedExternalSignatureConfig, ExternalSignatureConfigDto.class);
    }


    @DeleteMapping(API_V1 + "/admin/tenant/{tenantId}/externalSignature/config/{configId}")
    @Operation(summary = "Delete an external signature config")
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_204),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void deleteExternalSignatureConfig(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                              @PathVariable(name = Tenant.API_PATH) String tenantId,
                                              @TenantResolved Tenant tenant,
                                              @PathVariable(name = ExternalSignatureConfig.API_PATH) String configId,
                                              @ExternalSignatureConfigResolved ExternalSignatureConfig externalSignatureConfig) {
        log.info("deleteExternalSignatureConfig id:{}", externalSignatureConfig.getId());

        if (CollectionUtils.isNotEmpty(externalSignatureConfig.getSubtypes())) {
            throw new LocalizedStatusException(
                    BAD_REQUEST,
                    "message.cannot_delete_an_external_signature_configuration_referenced_by_subtypes",
                    externalSignatureConfig.getSubtypes().size()
            );
        }

        if (CollectionUtils.isNotEmpty(externalSignatureConfig.getTransactionIds())) {
            throw new LocalizedStatusException(
                    BAD_REQUEST,
                    "message.cannot_delete_an_external_signature_configuration_referenced_by_folders",
                    externalSignatureConfig.getTransactionIds().size()
            );
        }

        externalSignatureConfigRepository.deleteById(externalSignatureConfig.getId());
        statsService.registerAdminAction(externalSignatureConfig.getTenant(), EXTERNAL_SIGNATURE_CONFIG, DELETE, externalSignatureConfig.getId());
    }


    @GetMapping(API_V1 + "/admin/tenant/{tenantId}/externalSignature/config")
    @Operation(summary = "List external signature configs", description = ExternalSignatureConfigSortBy.Constants.API_DOC_SORT_BY_VALUES)
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<ExternalSignatureConfigRepresentation> listExternalSignatureConfigs(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                                                    @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                                                    @TenantResolved Tenant tenant,
                                                                                    @ParameterObject Pageable pageable,
                                                                                    @RequestParam(required = false) String searchTerm) {

        log.debug("getExternalSignatureConfigs page:{} pageSize:{}", pageable.getPageNumber(), pageable.getPageSize());

        Page<ExternalSignatureConfig> requestResult = Optional.ofNullable(searchTerm)
                .filter(StringUtils::isNotEmpty)
                .map(s -> "%" + s + "%")
                .map(w -> externalSignatureConfigRepository.findAllWithSearchTermAndTenant(tenantId, w, pageable))
                .orElseGet(() -> externalSignatureConfigRepository.findAllByTenantId(tenantId, pageable));

        log.debug("listExternalSignatureConfigs result:{}", requestResult.getSize());

        return requestResult.map(externalSignatureConfig -> modelMapper.map(externalSignatureConfig, ExternalSignatureConfigRepresentation.class));
    }


    // </editor-fold desc="External signature config CRUDL">


    @PostMapping(API_INTERNAL + "/admin/tenant/{tenantId}/externalSignature/config/test")
    @Operation(summary = "Test an external signature config")
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_409, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_507, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void testExternalSignatureConfig(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                            @PathVariable(name = Tenant.API_PATH) String tenantId,
                                            @TenantResolved Tenant tenant,
                                            @RequestBody ExternalSignatureConfigDto request) {

        log.info("testExternalSignatureConfig tenantId:{}", tenantId);

        ExternalSignatureConfig pendingConfig = modelMapper.map(request, ExternalSignatureConfig.class);
        externalSignatureService.testService(pendingConfig);
    }


}
