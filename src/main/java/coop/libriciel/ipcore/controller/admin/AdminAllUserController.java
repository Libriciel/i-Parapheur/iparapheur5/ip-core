/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller.admin;

import coop.libriciel.ipcore.business.auth.UserBusinessService;
import coop.libriciel.ipcore.business.database.TenantBusinessService;
import coop.libriciel.ipcore.model.auth.requests.ListableUser;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.auth.requests.UserDto;
import coop.libriciel.ipcore.model.auth.requests.UserRepresentation;
import coop.libriciel.ipcore.model.auth.requests.UserSortBy;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.database.TenantRepresentation;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.database.DatabaseServiceInterface;
import coop.libriciel.ipcore.services.database.UserPreferencesRepository;
import coop.libriciel.ipcore.services.resolvers.CurrentUserResolver.CurrentUserResolved;
import coop.libriciel.ipcore.services.resolvers.UserResolver.UserResolved;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.utils.ApiUtils;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.RequestUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static coop.libriciel.ipcore.IpCoreApplication.*;
import static coop.libriciel.ipcore.model.auth.UserPrivilege.*;
import static coop.libriciel.ipcore.model.auth.requests.UserSortBy.Constants.LAST_NAME_VALUE;
import static coop.libriciel.ipcore.model.stats.StatsCategory.USER;
import static coop.libriciel.ipcore.model.workflow.Action.*;
import static coop.libriciel.ipcore.services.auth.AuthServiceInterface.INTERNAL_USERNAMES;
import static coop.libriciel.ipcore.services.content.ContentServiceInterface.FILE_TRANSFER_BUFFER_SIZE;
import static coop.libriciel.ipcore.services.resolvers.UserResolver.UserResolved.Scope.GLOBAL;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.RequestUtils.PREAUTHORIZE_SUPER_ADMIN;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.tomcat.util.http.fileupload.FileUploadBase.MULTIPART_FORM_DATA;
import static org.springframework.core.io.buffer.DataBufferUtils.readInputStream;
import static org.springframework.data.domain.Pageable.unpaged;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM_VALUE;


@Log4j2
@RestController
@PreAuthorize(PREAUTHORIZE_SUPER_ADMIN)
@Tag(name = "admin-all-users", description = "Reserved operations on users")
public class AdminAllUserController {


    private final AuthServiceInterface authService;
    private final ContentServiceInterface contentService;
    private final DatabaseServiceInterface dbService;
    private final ModelMapper modelMapper;
    private final StatsServiceInterface statsService;
    private final TenantBusinessService tenantBusinessService;
    private final UserBusinessService userBusinessService;
    private final UserPreferencesRepository userPreferencesRepository;


    // <editor-fold desc="Beans">


    @Autowired
    public AdminAllUserController(AuthServiceInterface authService,
                                  ContentServiceInterface contentService,
                                  DatabaseServiceInterface dbService,
                                  ModelMapper modelMapper,
                                  StatsServiceInterface statsService,
                                  TenantBusinessService tenantBusinessService,
                                  UserBusinessService userBusinessService,
                                  UserPreferencesRepository userPreferencesRepository) {
        this.authService = authService;
        this.contentService = contentService;
        this.dbService = dbService;
        this.modelMapper = modelMapper;
        this.statsService = statsService;
        this.tenantBusinessService = tenantBusinessService;
        this.userBusinessService = userBusinessService;
        this.userPreferencesRepository = userPreferencesRepository;
    }


    // </editor-fold desc="Beans">


    // TODO: move this to an internal controller, we do not want to support it
    @GetMapping(API_V1 + "/admin/user/count")
    @Operation(summary = "Count logged in users")
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public int countLoggedInUsers() {
        return authService.countLoggedInUsers();
    }


    // <editor-fold desc="User CRUDL">


    @PostMapping(API_PROVISIONING_V1 + "/admin/user")
    @Operation(summary = "Create a new super admin user")
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_201),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_409, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_507, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public UserDto createSuperAdminAsSuperAdmin(@Parameter(name = "body", description = "Every field is mandatory")
                                                @RequestBody @Valid UserDto request,
                                                @CurrentUserResolved User currentUser) {

        log.info("createSuperAdminUser request:{}", request);
        String userId = authService.createUser(request, null);

        // Integrity check

        ApiUtils.assertNullity(request::getAssociatedDeskIds, "Associated desks cannot be modified outside tenant context. Skipping.");
        ApiUtils.assertNullity(request::getAdministeredDeskIds, "Administered desks cannot be modified outside tenant context. Skipping.");
        ApiUtils.assertNullity(request::getSupervisedDeskIds, "Supervised desks cannot be modified outside tenant context. Skipping.");
        ApiUtils.assertNullity(request::getDelegationManagedDeskIds, "Delegation managed desks cannot be modified outside tenant context. Skipping.");

        // Actual update

        User user = authService.getUsersByIds(Set.of(userId)).get(0);
        request.setPrivilege(SUPER_ADMIN);
        request.setAdministeredTenantIds(Collections.emptyList());
        request.setAdministeredTenants(Collections.emptyList());
        authService.updateUserGlobalPrivileges(user, request, null);

        statsService.registerAdminAction(null, USER, CREATE, userId);

        // Update missing values and return

        UserDto result = modelMapper.map(user, UserDto.class);
        tenantBusinessService.updateInnerTenantValues(request);
        return result;
    }


    @GetMapping(API_PROVISIONING_V1 + "/admin/user/{userId}")
    @Operation(summary = "Get a full user representation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public UserDto getUserAsSuperAdmin(@Parameter(description = UserRepresentation.API_DOC_ID_VALUE)
                                       @PathVariable(name = UserRepresentation.API_PATH) String userId,
                                       @UserResolved(scope = GLOBAL) User user) {

        log.debug("getUserAsSuperAdmin id:{}", userId);

        UserDto result = modelMapper.map(user, UserDto.class);

        // Fetch missing values

        userBusinessService.updateGlobalInnerValues(result);
        tenantBusinessService.updateInnerTenantValues(result);

        return result;
    }


    @PutMapping(API_PROVISIONING_V1 + "/admin/user/{userId}")
    @Operation(summary = "Edit a user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public UserDto updateUserAsSuperAdmin(@Parameter(description = UserRepresentation.API_DOC_ID_VALUE)
                                          @PathVariable(name = UserRepresentation.API_PATH) String userId,
                                          @UserResolved(scope = GLOBAL) User existingUser,
                                          @RequestBody UserDto request,
                                          @CurrentUserResolved User currentUser) {

        log.info("updateUser userId:{}", userId);
        log.debug("updateUser request:{}", request);

        // Integrity check

        ApiUtils.assertNullity(request::getAssociatedDeskIds, "Associated desks cannot be modified outside tenant context. Skipping.");
        ApiUtils.assertNullity(request::getAdministeredDeskIds, "Administered desks cannot be modified outside tenant context. Skipping.");
        ApiUtils.assertNullity(request::getSupervisedDeskIds, "Supervised desks cannot be modified outside tenant context. Skipping.");
        ApiUtils.assertNullity(request::getDelegationManagedDeskIds, "Delegation managed desks cannot be modified outside tenant context. Skipping.");

        authService.checkUserCrudPrivileges(currentUser, existingUser, request);

        // Registering

        Page<TenantRepresentation> administeredTenants = dbService
                .listTenantsForUser(existingUser.getId(), unpaged(), null, false, true, false);

        List<String> administeredTenantIds = administeredTenants.getContent().stream()
                .map(TenantRepresentation::getId)
                .toList();

        userBusinessService.computeGlobalLevelDifferencesAndUpdateUserRolesAndPermissions(existingUser, request);
        authService.updateUser(modelMapper, existingUser, request, administeredTenantIds);
        statsService.registerAdminAction(null, USER, UPDATE, userId);

        // Update missing values and return

        tenantBusinessService.updateInnerTenantValues(request);
        return request;
    }


    @DeleteMapping(API_PROVISIONING_V1 + "/admin/user/{userId}")
    @Operation(summary = "Delete a user")
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_204),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void deleteUserAsSuperAdmin(@Parameter(description = UserRepresentation.API_DOC_ID_VALUE)
                                       @PathVariable(name = UserRepresentation.API_PATH) String userId,
                                       @UserResolved(scope = GLOBAL) User targetUser,
                                       @CurrentUserResolved User currentUser) {

        log.info("deleteUser userId:{}", userId);

        // Integrity check

        if (StringUtils.equals(targetUser.getId(), currentUser.getId())) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.cannot_delete_yourself");
        }

        boolean isTargetSuperAdmin = targetUser.getPrivilege() == SUPER_ADMIN;
        boolean hasOnlyOneSuperAdmin = authService.getSuperAdminsList(2).size() <= 1;
        if (isTargetSuperAdmin && hasOnlyOneSuperAdmin) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.cannot_delete_the_last_admin");
        }

        if (INTERNAL_USERNAMES.contains(targetUser.getUserName())) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.cannot_touch_an_internal_user");
        }

        // Actual delete

        log.debug("deleteUser userId:{} contentNodeId:{}", userId, targetUser.getContentNodeId());
        authService.deleteUser(targetUser.getId());
        userPreferencesRepository.deleteByUserId(userId);

        Optional.ofNullable(targetUser.getContentNodeId())
                .ifPresent(contentService::deleteUserData);

        statsService.registerAdminAction(null, USER, DELETE, userId);
    }


    @GetMapping(API_PROVISIONING_V1 + "/admin/user")
    @Operation(summary = "List all users on the instance", description = UserSortBy.Constants.API_DOC_SORT_BY_VALUES)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<ListableUser> listUsersAsSuperAdmin(@PageableDefault(sort = LAST_NAME_VALUE)
                                                    @ParameterObject Pageable pageable,
                                                    @Parameter(description = "Searching for a specific term within `username`, `firstName`, `lastName` or `email` fields.")
                                                    @RequestParam(required = false) String searchTerm) {

        log.debug("listAllUsers pageable:{}", pageable);
        Page<User> result = authService.listUsers(pageable, searchTerm);
        log.debug("List users result:{}", result.getSize());

        // Update internal values

        userBusinessService.updatePrivileges(result.getContent(), null);

        // Sending back result

        return result.map(user -> modelMapper.map(user, ListableUser.class));
    }


    // </editor-fold desc="User CRUDL">


    // <editor-fold desc="Signature image CRUD">


    @PostMapping(value = API_PROVISIONING_V1 + "/admin/user/{userId}/signature-image", consumes = MULTIPART_FORM_DATA)
    @Operation(summary = "Create user's signature image")
    @PreAuthorize(PREAUTHORIZE_SUPER_ADMIN)
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_201),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_409, content = @Content(schema = @Schema(implementation = ErrorResponse.class)))
    })
    public void setSignatureImageAsSuperAdmin(@Parameter(description = UserRepresentation.API_DOC_ID_VALUE)
                                              @PathVariable(name = UserRepresentation.API_PATH) String userId,
                                              @UserResolved(scope = GLOBAL) User user,
                                              @Parameter(description = "File")
                                              @RequestPart MultipartFile file) {

        log.info("setSignatureImage userId:{} imageSize:{} imageName:{}", userId, file.getSize(), file.getName());

        if (StringUtils.isNotEmpty(user.getSignatureImageContentId())) {
            throw new LocalizedStatusException(CONFLICT, "message.user_already_have_a_signature_image_set");
        }

        // Init User content data if needed

        if (StringUtils.isEmpty(user.getContentNodeId())) {

            int contentGroupIndex = Optional
                    .ofNullable(user.getContentGroupIndex())
                    .orElseGet(dbService::getUserDataGroupIndexAvailable);

            String userDataContentNodeId = contentService.createUserData(user.getId(), contentGroupIndex);
            authService.updateUserInternalMetadata(userId, contentGroupIndex, userDataContentNodeId, null);

            user.setContentGroupIndex(contentGroupIndex);
            user.setContentNodeId(userDataContentNodeId);
        }

        // Updating data

        String imageSignatureNodeId;
        try (InputStream body = file.getInputStream()) {
            DocumentBuffer imageBuffer = new DocumentBuffer();
            imageBuffer.setName(file.getOriginalFilename());
            imageBuffer.setContentFlux(readInputStream(() -> body, new DefaultDataBufferFactory(), FILE_TRANSFER_BUFFER_SIZE));
            imageSignatureNodeId = contentService.createUserSignatureImage(user.getContentNodeId(), imageBuffer);
        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_reading_file");
        }

        authService.updateUserInternalMetadata(userId, null, null, imageSignatureNodeId);
        statsService.registerAdminAction(null, USER, UPDATE, user.getId());
    }


    @GetMapping(API_PROVISIONING_V1 + "/admin/user/{userId}/signature-image")
    @Operation(summary = "Get user's signature image")
    @PreAuthorize(PREAUTHORIZE_SUPER_ADMIN)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200, content = @Content(mediaType = APPLICATION_OCTET_STREAM_VALUE)),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void getSignatureImageAsSuperAdmin(@Parameter(description = UserRepresentation.API_DOC_ID_VALUE)
                                              @PathVariable(name = UserRepresentation.API_PATH) String userId,
                                              @UserResolved(scope = GLOBAL) User user,
                                              HttpServletResponse response) {

        log.info("getSignatureImage userId:{}", userId);

        // Integrity check

        if (StringUtils.isEmpty(user.getSignatureImageContentId())) {
            throw new LocalizedStatusException(NOT_FOUND, "message.no_signature_for_the_given_user_id");
        }

        // Image fetch

        DocumentBuffer documentBuffer = contentService.getSignatureImage(user.getSignatureImageContentId());
        log.debug("getSignatureImage retrieve from Alfresco buffer imageContentId:{} buffer:{}", user.getSignatureImageContentId(), documentBuffer);

        // Build Response

        RequestUtils.writeBufferToResponse(documentBuffer, response);
    }


    @PutMapping(value = API_PROVISIONING_V1 + "/admin/user/{userId}/signature-image", consumes = MULTIPART_FORM_DATA)
    @Operation(summary = "Replace user's signature image")
    @PreAuthorize(PREAUTHORIZE_SUPER_ADMIN)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class)))
    })
    public void updateSignatureImageAsSuperAdmin(@Parameter(description = UserRepresentation.API_DOC_ID_VALUE)
                                                 @PathVariable(name = UserRepresentation.API_PATH) String userId,
                                                 @UserResolved(scope = GLOBAL) User user,
                                                 @Parameter(description = "File")
                                                 @RequestPart MultipartFile file) {

        log.info("updateSignatureImage userId:{} imageSize:{} imageName:{}", userId, file.getSize(), file.getName());

        // Integrity check

        String imageNodeId = Optional.ofNullable(user.getSignatureImageContentId())
                .filter(StringUtils::isNotEmpty)
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.no_signature_for_the_given_user_id"));

        // Updating data

        try (InputStream body = file.getInputStream()) {
            DocumentBuffer imageBuffer = new DocumentBuffer();
            imageBuffer.setName(file.getOriginalFilename());
            imageBuffer.setContentFlux(readInputStream(() -> body, new DefaultDataBufferFactory(), FILE_TRANSFER_BUFFER_SIZE));
            contentService.updateSignatureImage(imageNodeId, imageBuffer);
        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_reading_file");
        }

        statsService.registerAdminAction(null, USER, UPDATE, user.getId());
    }


    @DeleteMapping(API_PROVISIONING_V1 + "/admin/user/{userId}/signature-image")
    @Operation(summary = "Delete user's signature image")
    @PreAuthorize(PREAUTHORIZE_SUPER_ADMIN)
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_204),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class)))
    })
    public void deleteSignatureImageAsSuperAdmin(@Parameter(description = UserRepresentation.API_DOC_ID_VALUE)
                                                 @PathVariable(name = UserRepresentation.API_PATH) String userId,
                                                 @UserResolved(scope = GLOBAL) User user) {

        log.info("deleteSignatureImage userId:{}", userId);

        // Integrity check

        String imageNodeId = Optional.ofNullable(user.getSignatureImageContentId())
                .filter(StringUtils::isNotEmpty)
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.no_signature_for_the_given_user_id"));

        // Actual delete

        contentService.deleteSignatureImage(imageNodeId);
        authService.updateUserInternalMetadata(userId, null, null, EMPTY);
        statsService.registerAdminAction(null, USER, UPDATE, userId);
    }


    // </editor-fold desc="Signature image CRUD">


}
