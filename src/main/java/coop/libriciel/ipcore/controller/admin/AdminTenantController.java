/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller.admin;

import coop.libriciel.ipcore.business.database.TenantBusinessService;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.database.TenantRepresentation;
import coop.libriciel.ipcore.model.database.TenantSortBy;
import coop.libriciel.ipcore.model.database.requests.TenantDto;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.database.DatabaseServiceInterface;
import coop.libriciel.ipcore.services.database.SubtypeRepository;
import coop.libriciel.ipcore.services.database.TenantRepository;
import coop.libriciel.ipcore.services.database.TypeRepository;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.services.secret.SecretServiceInterface;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.PostConstruct;
import jakarta.validation.Valid;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

import static coop.libriciel.ipcore.IpCoreApplication.*;
import static coop.libriciel.ipcore.model.auth.requests.UserSortBy.Constants.LAST_NAME_VALUE;
import static coop.libriciel.ipcore.model.database.TenantSortBy.Constants.NAME_VALUE;
import static coop.libriciel.ipcore.model.workflow.FolderSortBy.FOLDER_ID;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.RequestUtils.PREAUTHORIZE_SUPER_ADMIN;
import static coop.libriciel.ipcore.utils.RequestUtils.convertSortedPageable;
import static coop.libriciel.ipcore.utils.TextUtils.MESSAGE_BUNDLE;
import static coop.libriciel.ipcore.utils.TextUtils.NONE_SERVICE;
import static java.util.Collections.emptyList;
import static org.springframework.data.domain.Pageable.unpaged;
import static org.springframework.data.domain.Sort.Direction.ASC;
import static org.springframework.http.HttpStatus.*;


@Log4j2
@RestController
@PreAuthorize(PREAUTHORIZE_SUPER_ADMIN)
@Tag(name = "admin-tenant", description = "Reserved operations on tenants")
public class AdminTenantController {


    public static final int MAX_TENANTS_COUNT = 10000;


    @Value("${" + AuthServiceInterface.PREFERENCES_PROVIDER_KEY + "}")
    private String authProviderName;


    // <editor-fold desc="Beans">


    private final AuthServiceInterface authService;
    private final ContentServiceInterface contentService;
    private final DatabaseServiceInterface databaseService;
    private final ModelMapper modelMapper;
    private final SecretServiceInterface secretService;
    private final StatsServiceInterface statsService;
    private final SubtypeRepository subtypeRepository;
    private final TenantBusinessService tenantBusinessService;
    private final TenantRepository tenantRepository;
    private final TypeRepository typeRepository;
    private final WorkflowServiceInterface workflowService;


    @Autowired
    public AdminTenantController(AuthServiceInterface authService,
                                 ContentServiceInterface contentService,
                                 DatabaseServiceInterface databaseService,
                                 ModelMapper modelMapper,
                                 SecretServiceInterface secretService,
                                 StatsServiceInterface statsService,
                                 SubtypeRepository subtypeRepository,
                                 TenantBusinessService tenantBusinessService,
                                 TenantRepository tenantRepository,
                                 TypeRepository typeRepository,
                                 WorkflowServiceInterface workflowService) {
        this.authService = authService;
        this.contentService = contentService;
        this.databaseService = databaseService;
        this.modelMapper = modelMapper;
        this.secretService = secretService;
        this.statsService = statsService;
        this.subtypeRepository = subtypeRepository;
        this.tenantBusinessService = tenantBusinessService;
        this.tenantRepository = tenantRepository;
        this.typeRepository = typeRepository;
        this.workflowService = workflowService;
    }


    @PostConstruct
    public void init() {

        // Default case

        boolean isInBlankMode = StringUtils.equals(authProviderName, NONE_SERVICE);
        boolean alreadyHasTenants = tenantRepository.count() > 0;
        if (isInBlankMode || alreadyHasTenants) {
            return;
        }

        // Init

        log.info("Building the default tenant...");
        ResourceBundle messageResourceBundle = ResourceBundle.getBundle(MESSAGE_BUNDLE, Locale.getDefault());
        String defaultTenantName = messageResourceBundle.getString("message.initial_tenant");

        TenantDto defaultTenantDto = createTenant(new TenantDto(defaultTenantName));

        tenantRepository.findById(defaultTenantDto.getId())
                .ifPresentOrElse(
                        authService::addDefaultUserOnTenant,
                        () -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.");}
                );
    }


    // </editor-fold desc="Beans">


    // <editor-fold desc="Tenant CRUDL">


    @PostMapping(API_PROVISIONING_V1 + "/admin/tenant")
    @Operation(summary = "Create a new tenant")
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_201),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_507, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public TenantDto createTenant(@Parameter(description = "body", required = true)
                                  @RequestBody @Valid TenantDto tenantRequest) {

        log.info("createTenant name:{}", tenantRequest.getName());

        // Integrity check

        if (tenantRepository.count() >= MAX_TENANTS_COUNT) {
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE, "message.already_n_tenants_maximum_reached", MAX_TENANTS_COUNT);
        }

        // Actual registering

        Tenant newEntity = modelMapper.map(tenantRequest, Tenant.class);
        newEntity.setId(authService.createTenant(tenantRequest.getName()));
        newEntity.setIndex(databaseService.getTenantIndexAvailable());
        newEntity.setContentId(contentService.createTenant(newEntity));

        Tenant savedTenant = tenantRepository.save(newEntity);

        return modelMapper.map(savedTenant, TenantDto.class);
    }


    @GetMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}")
    @Operation(summary = "Get a full tenant description")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public TenantDto getTenant(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                               @PathVariable(name = Tenant.API_PATH) String tenantId,
                               @TenantResolved Tenant tenant) {

        log.debug("getTenant id:{}", tenant.getId());

        return modelMapper.map(tenant, TenantDto.class);
    }


    @PutMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}")
    @Operation(summary = "Edit a tenant")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public TenantDto updateTenant(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                  @PathVariable(name = Tenant.API_PATH) String tenantId,
                                  @TenantResolved Tenant existingTenant,
                                  @Parameter(description = "body", required = true)
                                  @RequestBody @Valid TenantDto request) {

        log.info("updateTenant existingTenant:{}", existingTenant);

        Optional.ofNullable(request.getName())
                .filter(StringUtils::isNotEmpty)
                .ifPresent(existingTenant::setName);

        tenantBusinessService.updateExistingTenant(existingTenant, request);

        Tenant savedTenant = tenantRepository.save(existingTenant);
        return modelMapper.map(savedTenant, TenantDto.class);
    }


    @DeleteMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}")
    @Operation(summary = "Delete a tenant")
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_204),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void deleteTenant(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                             @PathVariable(name = Tenant.API_PATH) String tenantId,
                             @TenantResolved Tenant tenant) {

        this.deleteTenantIntegrityChecks(tenant);

        // Actual delete

        secretService.deleteTenant(tenant.getId());
        contentService.deleteTenant(tenant);
        authService.deleteTenant(tenant.getId());
        tenantRepository.delete(tenant);

        log.info("Deleted tenant id:{}", tenant.getId());
    }


    @GetMapping(API_PROVISIONING_V1 + "/admin/tenant")
    @Operation(summary = "List tenants", description = TenantSortBy.Constants.API_DOC_SORT_BY_VALUES)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<TenantRepresentation> listTenants(@PageableDefault(sort = NAME_VALUE)
                                                  @ParameterObject Pageable pageable,
                                                  @Parameter(description = "Search for a specific tenant name")
                                                  @RequestParam(required = false) String searchTerm) {

        log.debug("listTenants page:{} pageSize:{} searchTerm:{}", pageable.getPageNumber(), pageable.getPageSize(), searchTerm);

        Pageable innerPageable = convertSortedPageable(pageable, TenantSortBy.class, TenantSortBy::getColumnName);
        Page<Tenant> requestResult = Optional.ofNullable(searchTerm)
                .filter(StringUtils::isNotEmpty)
                .map(s -> "%" + s + "%")
                .map(w -> tenantRepository.findAllWithSearchTerm(w, innerPageable))
                .orElseGet(() -> tenantRepository.findDistinctBy(innerPageable));

        log.debug("listTenants result:{}", requestResult.getSize());
        return requestResult.map(tenant -> modelMapper.map(tenant, TenantRepresentation.class));
    }


    // </editor-fold desc="Tenant CRUDL">


    private void deleteTenantIntegrityChecks(Tenant tenant) {

        if (workflowService.getArchives(tenant.getId(), unpaged(), null).getTotal() > 0) {
            throw new LocalizedStatusException(CONFLICT, "message.cannot_delete_a_tenant_with_archives");
        }

        long totalFolderCount = workflowService
                .listFolders(tenant.getId(), PageRequest.of(0, 1), null, null, null, null, null)
                .getTotalElements();

        if (totalFolderCount > 0) {
            throw new LocalizedStatusException(CONFLICT, "message.cannot_delete_a_tenant_with_folders");
        }

        if (subtypeRepository.findAllByTenant_Id(tenant.getId(), PageRequest.of(0, 1)).getTotalElements() > 0) {
            throw new LocalizedStatusException(CONFLICT, "message.cannot_delete_a_tenant_with_subtypes");
        }

        if (typeRepository.findAllByTenantId(tenant.getId(), PageRequest.of(0, 1)).getTotalElements() > 0) {
            throw new LocalizedStatusException(CONFLICT, "message.cannot_delete_a_tenant_with_types");
        }

        if (secretService.getSealCertificateList(tenant.getId(), PageRequest.of(0, 1)).getTotalElements() > 0) {
            throw new LocalizedStatusException(CONFLICT, "message.cannot_delete_a_tenant_with_seal_certificates");
        }

        if (workflowService.listWorkflowDefinitions(tenant.getId(), PageRequest.of(0, 1), null).getTotalElements() > 0) {
            throw new LocalizedStatusException(CONFLICT, "message.cannot_delete_a_tenant_with_workflows");
        }

        if (authService.listDesks(tenant.getId(), Pageable.ofSize(1), emptyList(), false).getTotalElements() > 0) {
            throw new LocalizedStatusException(CONFLICT, "message.cannot_delete_a_tenant_with_desks");
        }

        if (authService.listTenantUsers(tenant.getId(), PageRequest.of(0, 1, ASC, LAST_NAME_VALUE), null).getTotalElements() > 1) {
            throw new LocalizedStatusException(CONFLICT, "message.cannot_delete_a_tenant_with_users");
        }
    }


}
