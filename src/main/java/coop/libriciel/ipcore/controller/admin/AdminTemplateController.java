/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller.admin;

import coop.libriciel.ipcore.business.template.TemplateBusinessService;
import coop.libriciel.ipcore.business.workflow.FolderBusinessService;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.auth.requests.UserRepresentation;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.TemplateType;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.database.Type;
import coop.libriciel.ipcore.model.database.requests.MailTemplateTestRequest;
import coop.libriciel.ipcore.model.database.requests.PdfTemplateTestRequest;
import coop.libriciel.ipcore.model.mail.MailContent;
import coop.libriciel.ipcore.model.mail.MailTarget;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.crypto.CryptoServiceInterface;
import coop.libriciel.ipcore.services.database.TenantRepository;
import coop.libriciel.ipcore.services.mail.NotificationServiceInterface;
import coop.libriciel.ipcore.services.resolvers.CurrentUserResolver;
import coop.libriciel.ipcore.services.resolvers.CurrentUserResolver.CurrentUserResolved;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.RequestUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import jakarta.servlet.http.HttpServletResponse;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static coop.libriciel.ipcore.IpCoreApplication.API_INTERNAL;
import static coop.libriciel.ipcore.IpCoreApplication.API_PROVISIONING_V1;
import static coop.libriciel.ipcore.configuration.CacheConfig.TEMPLATES_CACHE_KEY;
import static coop.libriciel.ipcore.configuration.CacheConfig.TEMPLATES_DIMENSIONS_CACHE_KEY_GENERATOR_KEY;
import static coop.libriciel.ipcore.model.crypto.SignatureFormat.PADES;
import static coop.libriciel.ipcore.model.database.Type.SignatureProtocol.ACTES;
import static coop.libriciel.ipcore.model.stats.StatsCategory.TEMPLATE;
import static coop.libriciel.ipcore.model.workflow.Action.*;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.CryptoUtils.EXAMPLE_PUBLIC_CERTIFICATE_BASE64;
import static coop.libriciel.ipcore.utils.RequestUtils.PREAUTHORIZE_IS_AUTHENTICATED;
import static coop.libriciel.ipcore.utils.RequestUtils.PREAUTHORIZE_TENANT_ADMIN_OR_MORE;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.singletonList;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_PDF_VALUE;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;


@Log4j2
@RestController
@Tag(name = "admin-template", description = "Reserved operations on templates")
public class AdminTemplateController {


    // <editor-fold desc="Beans">


    private final ContentServiceInterface contentService;
    private final CryptoServiceInterface cryptoService;
    private final FolderBusinessService folderBusinessService;
    private final NotificationServiceInterface notificationService;
    private final StatsServiceInterface statsService;
    private final TemplateBusinessService templateBusinessService;
    private final TenantRepository tenantRepository;


    @Autowired
    public AdminTemplateController(ContentServiceInterface contentService, CryptoServiceInterface cryptoService,
                                   FolderBusinessService folderBusinessService,
                                   NotificationServiceInterface notificationService,
                                   StatsServiceInterface statsService,
                                   TemplateBusinessService templateBusinessService,
                                   TenantRepository tenantRepository) {
        this.contentService = contentService;
        this.cryptoService = cryptoService;
        this.folderBusinessService = folderBusinessService;
        this.notificationService = notificationService;
        this.statsService = statsService;
        this.templateBusinessService = templateBusinessService;
        this.tenantRepository = tenantRepository;
    }


    // </editor-fold desc="Beans">


    // <editor-fold desc="Custom template CRUD">


    @GetMapping(API_PROVISIONING_V1 + "/tenant/{tenantId}/templates/{templateType}")
    @Operation(summary = "Get a custom template")
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200, content = @Content(mediaType = TEXT_PLAIN_VALUE)),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void getCustomTemplate(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                  @PathVariable(name = Tenant.API_PATH) String tenantId,
                                  @TenantResolved Tenant tenant,
                                  @PathVariable TemplateType templateType,
                                  HttpServletResponse response) {

        log.debug("getCustomTemplate {}", templateType);

        DocumentBuffer templateBuffer = templateBusinessService.getCustomTemplate(tenant, templateType);
        if (templateBuffer == null) {
            log.warn("There is no custom template for this template type: {}", templateType);
            throw new LocalizedStatusException(NOT_FOUND, "message.missing_template");
        }

        response.setHeader(CONTENT_TYPE, TEXT_PLAIN_VALUE);
        RequestUtils.writeBufferToResponse(templateBuffer, response);
    }


    @PostMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/templates/{templateType}")
    @Operation(summary = "Create a custom template")
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    @CacheEvict(value = TEMPLATES_CACHE_KEY, keyGenerator = TEMPLATES_DIMENSIONS_CACHE_KEY_GENERATOR_KEY)
    public void createCustomTemplate(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                     @PathVariable(name = Tenant.API_PATH) String tenantId,
                                     @TenantResolved Tenant tenant,
                                     @PathVariable TemplateType templateType,
                                     @Parameter(description = "Template", content = @Content(mediaType = TEXT_PLAIN_VALUE))
                                     @RequestBody String template) {

        log.info("createCustomTemplate tenantId:{} template:{}", tenant.getId(), template.length());

        if (tenant.getCustomTemplates().containsKey(templateType)) {
            throw new LocalizedStatusException(CONFLICT, "message.tenant_already_have_this_template_set");
        }

        log.info("createCustomTemplate contentId:{}", tenant.getContentId());

        String nodeId = contentService.createCustomTemplate(tenant, templateType, template);
        tenant.getCustomTemplates().put(templateType, nodeId);
        tenantRepository.save(tenant);

        statsService.registerAdminAction(tenant, TEMPLATE, CREATE, templateType.name());
    }


    @PutMapping(value = API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/templates/{templateType}")
    @Operation(summary = "Update a custom template")
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200, content = @Content(mediaType = APPLICATION_PDF_VALUE)),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    @CacheEvict(value = TEMPLATES_CACHE_KEY, keyGenerator = TEMPLATES_DIMENSIONS_CACHE_KEY_GENERATOR_KEY)
    public void updateCustomTemplate(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                     @PathVariable(name = Tenant.API_PATH) String tenantId,
                                     @TenantResolved Tenant tenant,
                                     @PathVariable TemplateType templateType,
                                     @Parameter(description = "Template", content = @Content(mediaType = TEXT_PLAIN_VALUE))
                                     @RequestBody String template) {

        log.info("updateCustomTemplate tenantId:{} template:{}", tenant.getId(), template.length());

        if (!tenant.getCustomTemplates().containsKey(templateType)) {
            log.error("No custom template of type {} exist on tenant {}", templateType.name(), tenant.getName());
            throw new LocalizedStatusException(NOT_FOUND, "message.missing_template");
        }

        contentService.editCustomTemplate(tenant, templateType, template);

        statsService.registerAdminAction(tenant, TEMPLATE, UPDATE, templateType.name());
    }


    @DeleteMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/templates/{templateType}")
    @Operation(summary = "Delete a custom template")
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    @CacheEvict(value = TEMPLATES_CACHE_KEY, keyGenerator = TEMPLATES_DIMENSIONS_CACHE_KEY_GENERATOR_KEY)
    public void deleteCustomTemplate(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                     @PathVariable(name = Tenant.API_PATH) String tenantId,
                                     @TenantResolved Tenant tenant,
                                     @PathVariable TemplateType templateType) {

        log.info("deleteCustomTemplate tenantId:{} template:{}", tenant.getId(), templateType);

        if (!tenant.getCustomTemplates().containsKey(templateType)) {
            log.error("No custom template of type {} exist on tenant {}", templateType.name(), tenant.getName());
            throw new LocalizedStatusException(NOT_FOUND, "message.missing_template");
        }

        contentService.deleteCustomTemplate(tenant, templateType);
        tenant.getCustomTemplates().remove(templateType);
        tenantRepository.save(tenant);

        statsService.registerAdminAction(tenant, TEMPLATE, DELETE, templateType.name());
    }


    // </editor-fold desc="Template CRUD">


    /**
     * No need to be admin of anything here, since it is a version-based open-source resource.
     * We can't set a tenant-based preauthorize, since we don't pass any tenant id.
     * Yet, we want a minimal authentication, to avoid any potential information leak.
     *
     * @param templateType
     * @param response
     */
    @GetMapping(API_PROVISIONING_V1 + "/templates/{templateType}")
    @Operation(summary = "Get the server default template")
    @PreAuthorize(PREAUTHORIZE_IS_AUTHENTICATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200, content = @Content(mediaType = TEXT_PLAIN_VALUE)),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void getDefaultTemplate(@PathVariable TemplateType templateType, HttpServletResponse response) {

        log.info("getDefaultTemplate {}", templateType);
        Resource defaultResource = templateBusinessService.getDefaultTemplateResource(templateType);
        response.setHeader(CONTENT_TYPE, TEXT_PLAIN_VALUE);
        try (InputStream is = defaultResource.getInputStream(); OutputStream os = response.getOutputStream()) {
            IOUtils.copy(is, os);
        } catch (IOException e) {
            log.error("Error while reading default template resource : {}", templateType);
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.error_reading_file");
        }
    }


    @PostMapping(API_INTERNAL + "/admin/tenant/{tenantId}/test-mail-template")
    @Operation(summary = "Test the given mail template")
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void testMailTemplate(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                 @PathVariable(name = Tenant.API_PATH) String tenantId,
                                 @Parameter(description = "body", required = true)
                                 @RequestBody MailTemplateTestRequest templateTestRequest) {

        log.info("testMailTemplate mail:{} templateSize:{}", templateTestRequest.getMail(), templateTestRequest.getTemplate().length());

        // Dummy values
        // TODO : externalize in resources

        String dummyMessage = "Message";

        User dummyUser = User.builder()
                .id("user_id").userName("username_01")
                .firstName("Prénom").lastName("Nom")
                .build();

        DeskRepresentation dummyDesk = new DeskRepresentation("desk_01", "Bureau 01");

        MailTarget dummyMailTarget = MailTarget.builder()
                .to(singletonList(templateTestRequest.getMail()))
                .build();

        Task dummyTask = Task.builder()
                .action(SIGNATURE)
                .date(new Date())
                .desks(singletonList(dummyDesk))
                .build();

        MailContent dummyMailContent = MailContent.builder()
                .tenant(Tenant.builder().id("tenant_id").name("Tenant 01").build())
                .folder(Folder.builder().id("folder_id").name("Dossier 01").build())
                .desk(dummyDesk)
                .task(dummyTask)
                .build();

        // Sending mail

        Resource tempResource = new ByteArrayResource(templateTestRequest.getTemplate().getBytes(), "file");
        notificationService.mailFolder(
                singletonList(dummyMailTarget),
                tempResource,
                singletonList(dummyMailContent),
                dummyUser,
                "Objet du mail",
                dummyMessage,
                null,
                null
        );
    }


    @PostMapping(API_INTERNAL + "/admin/tenant/{tenantId}/test-docket-pdf-template")
    @Operation(summary = "Test the given docket PDF template")
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200, content = @Content(mediaType = APPLICATION_PDF_VALUE)),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_500, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void testDocketPdfTemplate(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                      @PathVariable(name = Tenant.API_PATH) String tenantId,
                                      @Parameter(description = "body", required = true)
                                      @RequestBody MailTemplateTestRequest templateTestRequest,
                                      HttpServletResponse response) {

        log.info("testPdfTemplate mail:{} templateSize:{}", templateTestRequest.getMail(), templateTestRequest.getTemplate().length());

        // Dummy values
        // TODO : externalize in resources

        UserRepresentation dummyUser = new UserRepresentation("user_id");
        dummyUser.setUserName("username_01");
        dummyUser.setFirstName("Prénom");
        dummyUser.setLastName("Nom");

        DeskRepresentation dummyDesk = new DeskRepresentation("desk_id", "Bureau 01");

        Folder dummyFolder = new Folder();
        dummyFolder.setId("folder_id");
        dummyFolder.setName("Dossier 01");
        dummyFolder.setMetadata(Map.of(
                "Métadonnée 01", "Valeur 01",
                "Métadonnée 02", "Valeur 02"
        ));
        dummyFolder.setType(Type.builder()
                .id("type01").name("Type 01").description("Type 01 description")
                .isSignatureVisible(true).signatureFormat(PADES).protocol(ACTES)
                .signatureLocation("Montpellier").signatureZipCode("34000")
                .build());
        dummyFolder.setSubtype(Subtype.builder()
                .id("subtype01").name("Sous-type 01").description("Sous-type 01 description")
                .build());
        dummyFolder.setStepList(List.of(
                Task.builder()
                        .action(START).date(new Date(1609502400000L))
                        .desks(List.of(dummyDesk)).user(dummyUser)
                        .workflowIndex(2L)
                        .build(),
                Task.builder()
                        .action(VISA).date(new Date(1643803200000L))
                        .desks(List.of(dummyDesk)).user(dummyUser)
                        .workflowIndex(2L)
                        .build(),
                Task.builder()
                        .action(SIGNATURE).date(new Date(1677844800000L))
                        .publicCertificateBase64(EXAMPLE_PUBLIC_CERTIFICATE_BASE64)
                        .desks(List.of(dummyDesk)).user(dummyUser)
                        .workflowIndex(2L)
                        .build(),
                Task.builder()
                        .action(ARCHIVE)
                        .desks(List.of(dummyDesk)).user(dummyUser)
                        .workflowIndex(2L)
                        .build()
        ));

        // Generate actual PDF and send back value

        try (ByteArrayInputStream templateInputStream = new ByteArrayInputStream(templateTestRequest.getTemplate().getBytes(UTF_8));
             ByteArrayInputStream pdfInputStream = folderBusinessService.generateDocketPdfInputStream(dummyFolder, templateInputStream);
             OutputStream responseOutputStream = response.getOutputStream()) {

            response.setHeader(CONTENT_TYPE, APPLICATION_PDF_VALUE);
            response.setHeader(CONTENT_DISPOSITION, "attachment;filename=Bordereau.pdf");
            response.setContentLengthLong(pdfInputStream.available());
            IOUtils.copyLarge(pdfInputStream, responseOutputStream);

        } catch (IOException exception) {
            log.error("Error while creating docket : {}", exception.getMessage());
            log.debug("Error details : ", exception);
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, exception, "message.error_creating_docket");
        }
    }


    @PostMapping(API_INTERNAL + "/admin/tenant/{tenantId}/test-signature-pdf-template")
    @Operation(summary = "Get the server default signature template")
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200, content = @Content(mediaType = APPLICATION_PDF_VALUE)),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_500, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void testSignaturePdfTemplate(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                         @PathVariable(name = Tenant.API_PATH) String tenantId,
                                         @TenantResolved Tenant tenant,
                                         @RequestBody PdfTemplateTestRequest templateTestRequest,
                                         @CurrentUserResolved User currentUser,
                                         HttpServletResponse response) {
        String signatureImageBase64 = Optional.ofNullable(currentUser.getSignatureImageContentId())
                .filter(StringUtils::isNotEmpty)
                .map(contentService::retrieveContentAsBase64)
                .orElse(cryptoService.getDefaultPdfSignatureBase64());

        Resource testTemplateResource = new ByteArrayResource(templateTestRequest.getTemplate().getBytes(UTF_8));
        DocumentBuffer signedDocumentBuffer = templateBusinessService.fakeSeal(testTemplateResource, signatureImageBase64, null);
        RequestUtils.writeBufferToResponse(signedDocumentBuffer, response);
    }


}
