/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller.admin;

import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.crypto.SealCertificate;
import coop.libriciel.ipcore.model.crypto.SealCertificateDto;
import coop.libriciel.ipcore.model.crypto.SealCertificateRepresentation;
import coop.libriciel.ipcore.model.database.IdCount;
import coop.libriciel.ipcore.model.database.SealCertificateSortBy;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.database.requests.SubtypeRepresentation;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.database.SubtypeRepository;
import coop.libriciel.ipcore.services.resolvers.SealCertificateResolver.SealCertificateResolved;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.services.secret.SecretServiceInterface;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.RequestUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.modelmapper.ModelMapper;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;

import static coop.libriciel.ipcore.IpCoreApplication.API_INTERNAL;
import static coop.libriciel.ipcore.IpCoreApplication.API_PROVISIONING_V1;
import static coop.libriciel.ipcore.model.stats.StatsCategory.SEAL_CERTIFICATE;
import static coop.libriciel.ipcore.model.workflow.Action.*;
import static coop.libriciel.ipcore.services.content.ContentServiceInterface.FILE_TRANSFER_BUFFER_SIZE;
import static coop.libriciel.ipcore.services.secret.SecretServiceInterface.MAX_SEAL_CERTIFICATES_PER_TENANT;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.RequestUtils.PREAUTHORIZE_TENANT_ADMIN_OR_MORE;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.tomcat.util.http.fileupload.FileUploadBase.MULTIPART_FORM_DATA;
import static org.springframework.core.io.buffer.DataBufferUtils.readInputStream;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM_VALUE;


@Log4j2
@RestController
@Tag(name = "admin-seal-certificate", description = "Reserved operations on seal certificates")
public class AdminSealCertificateController {


    // <editor-fold desc="Beans">


    private final ContentServiceInterface contentService;
    private final SecretServiceInterface secretService;
    private final StatsServiceInterface statsService;
    private final SubtypeRepository subtypeRepository;
    private final ModelMapper modelMapper;


    @Autowired
    public AdminSealCertificateController(ContentServiceInterface contentService,
                                          SecretServiceInterface secretService,
                                          StatsServiceInterface statsService,
                                          SubtypeRepository subtypeRepository,
                                          ModelMapper modelMapper) {
        this.contentService = contentService;
        this.secretService = secretService;
        this.statsService = statsService;
        this.subtypeRepository = subtypeRepository;
        this.modelMapper = modelMapper;
    }


    // </editor-fold desc="Beans">


    // <editor-fold desc="Seal certificate CRUDL deprecated">


    @PostMapping(value = API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/sealCertificate", consumes = MULTIPART_FORM_DATA)
    @Operation(
            summary = "DEPRECATED: Use the \"seal-certificate\" instead.\nCreate a seal certificate",
            deprecated = true
    )
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_201),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_507, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    @Deprecated(forRemoval = true, since = "5.1.8")
    public SealCertificateDto createSealCertificateDeprecated(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                              @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                              @Parameter(hidden = true)
                                                              @TenantResolved Tenant tenant,
                                                              @Parameter(description = SealCertificateDto.API_DOC_SEAL, required = true)
                                                              @RequestPart(name = SealCertificateDto.API_PART_SEAL) @Valid SealCertificateDto sealCertificateRequest,
                                                              @Parameter(description = SealCertificateDto.API_DOC_CERTIFICATE, required = true)
                                                              @RequestPart(name = SealCertificateDto.API_PART_CERTIFICATE) MultipartFile certificateFile,
                                                              @Parameter(description = SealCertificateDto.API_DOC_IMAGE)
                                                              @RequestPart(name = SealCertificateDto.API_PART_IMAGE, required = false) MultipartFile imageFile) {
        log.warn("Deprecated method called: createLegacySealCertificate");
        return createSealCertificate(tenantId, tenant, sealCertificateRequest, certificateFile, imageFile);
    }


    @GetMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/sealCertificate/{sealCertificateId}")
    @Operation(
            summary = "DEPRECATED: Use the \"seal-certificate\" instead.\nGet a seal certificate with every information set",
            deprecated = true
    )
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    @Deprecated(forRemoval = true, since = "5.1.8")
    public SealCertificateDto getSealCertificateDeprecated(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                           @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                           @TenantResolved Tenant tenant,
                                                           @Parameter(description = SealCertificateDto.API_DOC_ID)
                                                           @PathVariable(name = SealCertificateDto.API_ID_PATH) String sealCertificateId,
                                                           @SealCertificateResolved SealCertificate sealCertificate) {
        log.warn("Deprecated method called: getSealCertificateDeprecated");
        return getSealCertificate(tenantId, tenant, sealCertificateId, sealCertificate);
    }


    @PutMapping(value = API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/sealCertificate/{sealCertificateId}", consumes = MULTIPART_FORM_DATA)
    @Operation(
            summary = "DEPRECATED: Use the \"seal-certificate\" instead.\nEdit a seal certificate",
            deprecated = true
    )
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    @Deprecated(forRemoval = true, since = "5.1.8")
    public SealCertificateDto updateSealCertificateDeprecated(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                              @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                              @TenantResolved Tenant tenant,
                                                              @Parameter(description = SealCertificateDto.API_DOC_ID)
                                                              @PathVariable(name = SealCertificateDto.API_ID_PATH) String sealCertificateId,
                                                              @SealCertificateResolved SealCertificate existingSealCertificate,
                                                              @Parameter(description = SealCertificateDto.API_DOC_SEAL, required = true)
                                                              @RequestPart(name = SealCertificateDto.API_PART_SEAL) @Valid SealCertificateDto updatedSealCertificateRequest,
                                                              @Parameter(description = SealCertificateDto.API_DOC_CERTIFICATE)
                                                              @RequestPart(name = SealCertificateDto.API_PART_CERTIFICATE, required = false) MultipartFile certificateFile,
                                                              @Parameter(description = SealCertificateDto.API_DOC_IMAGE)
                                                              @RequestPart(name = SealCertificateDto.API_PART_IMAGE, required = false) MultipartFile imageFile) {
        log.warn("Deprecated method called: updateSealCertificateDeprecated");
        return updateSealCertificate(tenantId, tenant, sealCertificateId, existingSealCertificate, updatedSealCertificateRequest, imageFile, certificateFile);
    }


    @DeleteMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/sealCertificate/{sealCertificateId}")
    @Operation(
            summary = "DEPRECATED: Use the \"seal-certificate\" instead.\nDelete a stored seal certificate",
            deprecated = true
    )
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_204),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_406, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    @Deprecated(forRemoval = true, since = "5.1.8")
    public void deleteSealCertificateDeprecated(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                @TenantResolved Tenant tenant,
                                                @Parameter(description = SealCertificateDto.API_DOC_ID)
                                                @PathVariable(name = SealCertificateDto.API_ID_PATH) String sealCertificateId,
                                                @SealCertificateResolved SealCertificate sealCertificate) {
        log.warn("Deprecated method called: deleteSealCertificate");
        deleteSealCertificate(tenantId, tenant, sealCertificateId, sealCertificate);
    }


    @GetMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/sealCertificate")
    @Operation(
            summary = "DEPRECATED: Use the \"seal-certificate\" instead.\nList seal certificates",
            deprecated = true,
            description= SealCertificateSortBy.Constants.API_DOC_SORT_BY_VALUES
    )
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    @Deprecated(forRemoval = true, since = "5.1.8")
    public Page<SealCertificateRepresentation> listSealCertificateDeprecated(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                                             @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                                             @TenantResolved Tenant tenant,
                                                                             @PageableDefault(sort = SealCertificateSortBy.Constants.NAME_VALUE)
                                                                             @ParameterObject Pageable pageable) {
        log.warn("Deprecated method called: listSealCertificateDeprecated");
        return listSealCertificate(tenantId, tenant, pageable);
    }


    // </editor-fold desc="Seal certificate CRUDL deprecated">


    @PostMapping(value = API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/seal-certificate", consumes = MULTIPART_FORM_DATA)
    @Operation(summary = "Create a seal certificate")
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_201),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_507, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public SealCertificateDto createSealCertificate(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                    @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                    @Parameter(hidden = true)
                                                    @TenantResolved Tenant tenant,
                                                    @Parameter(description = SealCertificateDto.API_DOC_SEAL, required = true)
                                                    @RequestPart(name = SealCertificateDto.API_PART_SEAL) @Valid SealCertificateDto sealCertificateRequest,
                                                    @Parameter(description = SealCertificateDto.API_DOC_CERTIFICATE, required = true)
                                                    @RequestPart(name = SealCertificateDto.API_PART_CERTIFICATE) MultipartFile certificateFile,
                                                    @Parameter(description = SealCertificateDto.API_DOC_IMAGE)
                                                    @RequestPart(name = SealCertificateDto.API_PART_IMAGE, required = false) MultipartFile imageFile) {

        log.info("createSealCertificate");

        // Integrity check

        Page<SealCertificateRepresentation> existing = secretService.getSealCertificateList(tenant.getId(), PageRequest.of(0, 1));
        if (existing.getTotalElements() >= MAX_SEAL_CERTIFICATES_PER_TENANT) {
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE, "message.already_n_seal_certificates_maximum_reached", MAX_SEAL_CERTIFICATES_PER_TENANT);
        }

        SealCertificate newSealCertificate = modelMapper.map(sealCertificateRequest, SealCertificate.class);
        newSealCertificate.setId(UUID.randomUUID().toString());

        SealCertificate parsedSealCertificate = parseSealCertificateFromFile(certificateFile, sealCertificateRequest.getPassword());
        newSealCertificate.setPublicCertificateBase64(parsedSealCertificate.getPublicCertificateBase64());
        newSealCertificate.setPrivateKeyBase64(parsedSealCertificate.getPrivateKeyBase64());
        newSealCertificate.setExpirationDate(parsedSealCertificate.getExpirationDate());

        // Actual registering

        if (imageFile != null) {
            String imageContentId = setSealSignatureImage(tenant, newSealCertificate, imageFile);
            newSealCertificate.setSignatureImageContentId(imageContentId);
        }

        secretService.storeSealCertificate(tenant.getId(), newSealCertificate);
        statsService.registerAdminAction(tenant, SEAL_CERTIFICATE, CREATE, newSealCertificate.getId());

        return modelMapper.map(newSealCertificate, SealCertificateDto.class);
    }


    @GetMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/seal-certificate/{sealCertificateId}")
    @Operation(summary = "Get a seal certificate with every information set")
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public SealCertificateDto getSealCertificate(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                 @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                 @TenantResolved Tenant tenant,
                                                 @Parameter(description = SealCertificateDto.API_DOC_ID)
                                                 @PathVariable(name = SealCertificateDto.API_ID_PATH) String sealCertificateId,
                                                 @SealCertificateResolved SealCertificate sealCertificate) {

        log.debug("getSealCertificate tenantId:{} sealCertificateId:{}", tenant.getId(), sealCertificate.getId());

        // Computing transient values

        long usageCount = subtypeRepository
                .fetchCountBySealCertificateIdIn(Set.of(sealCertificateId))
                .stream()
                .findAny()
                .map(IdCount::getCount)
                .orElse(0L);

        sealCertificate.setUsageCount(usageCount);

        return modelMapper.map(sealCertificate, SealCertificateDto.class);
    }


    @PutMapping(value = API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/seal-certificate/{sealCertificateId}", consumes = MULTIPART_FORM_DATA)
    @Operation(summary = "Edit a seal certificate")
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public SealCertificateDto updateSealCertificate(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                    @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                    @TenantResolved Tenant tenant,
                                                    @Parameter(description = SealCertificateDto.API_DOC_ID)
                                                    @PathVariable(name = SealCertificateDto.API_ID_PATH) String sealCertificateId,
                                                    @SealCertificateResolved SealCertificate existingSealCertificate,
                                                    @Parameter(description = SealCertificateDto.API_DOC_SEAL, required = true)
                                                    @RequestPart(name = SealCertificateDto.API_PART_SEAL) @Valid SealCertificateDto updatedSealCertificateRequest,
                                                    @Parameter(description = SealCertificateDto.API_DOC_CERTIFICATE)
                                                    @RequestPart(name = SealCertificateDto.API_PART_CERTIFICATE, required = false) MultipartFile certificateFile,
                                                    @Parameter(description = SealCertificateDto.API_DOC_IMAGE)
                                                    @RequestPart(name = SealCertificateDto.API_PART_IMAGE, required = false) MultipartFile imageFile) {

        log.info("updateSealCertificate id:{} tenant:{}", existingSealCertificate.getId(), tenant.getId());

        // Integrity check

        boolean alreadyHasAnImage = StringUtils.isNotEmpty(existingSealCertificate.getSignatureImageContentId());
        boolean updatedReferencesAnImage = StringUtils.isNotEmpty(updatedSealCertificateRequest.getSignatureImageContentId());
        boolean areReferencedImagesTheSame = StringUtils
                .equals(existingSealCertificate.getSignatureImageContentId(), updatedSealCertificateRequest.getSignatureImageContentId());
        if (alreadyHasAnImage && updatedReferencesAnImage && !areReferencedImagesTheSame) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.cannot_change_a_seal_certificate_referenced_image");
        }

        SealCertificate updatedSealCertificate = modelMapper.map(existingSealCertificate, SealCertificate.class);
        updatedSealCertificate.setName(updatedSealCertificateRequest.getName());

        if (certificateFile != null) {
            SealCertificate newSealCertificate = parseSealCertificateFromFile(certificateFile, updatedSealCertificateRequest.getPassword());
            updatedSealCertificate.setPublicCertificateBase64(newSealCertificate.getPublicCertificateBase64());
            updatedSealCertificate.setPrivateKeyBase64(newSealCertificate.getPrivateKeyBase64());
            updatedSealCertificate.setExpirationDate(newSealCertificate.getExpirationDate());
        }

        // Image management

        boolean shouldNotHaveAnImage = StringUtils.isEmpty(updatedSealCertificateRequest.getSignatureImageContentId());
        boolean hasAnImageToStore = imageFile != null;

        if (alreadyHasAnImage && shouldNotHaveAnImage && !hasAnImageToStore) {
            try {
                contentService.deleteSignatureImage(existingSealCertificate.getSignatureImageContentId());
            } catch (LocalizedStatusException exception) {
                if (exception.getStatusCode() != NOT_FOUND) {
                    log.warn("The signature image has already been deleted for the seal certificate:{}, skipping...", existingSealCertificate.getId());
                } else {
                    throw exception;
                }
            }
            updatedSealCertificate.setSignatureImageContentId(null);
        }

        if (hasAnImageToStore) {
            String imageContentId = setSealSignatureImage(tenant, updatedSealCertificate, imageFile);
            updatedSealCertificate.setSignatureImageContentId(imageContentId);
        }

        // Actual certificate storing

        secretService.storeSealCertificate(tenant.getId(), updatedSealCertificate);
        statsService.registerAdminAction(tenant, SEAL_CERTIFICATE, UPDATE, existingSealCertificate.getId());

        return modelMapper.map(updatedSealCertificate, SealCertificateDto.class);
    }


    @DeleteMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/seal-certificate/{sealCertificateId}")
    @Operation(summary = "Delete a stored seal certificate")
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_204),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_406, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void deleteSealCertificate(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                      @PathVariable(name = Tenant.API_PATH) String tenantId,
                                      @TenantResolved Tenant tenant,
                                      @Parameter(description = SealCertificateDto.API_DOC_ID)
                                      @PathVariable(name = SealCertificateDto.API_ID_PATH) String sealCertificateId,
                                      @SealCertificateResolved SealCertificate sealCertificate) {

        log.info("deleteSealCertificate metadata tenantId:{} sealCertificateId:{}", tenant.getId(), sealCertificate.getId());

        // Integrity check

        Page<Subtype> associatedSubtypePage = subtypeRepository.findByTenant_IdAndSealCertificateId(tenantId, sealCertificateId, PageRequest.of(0,3));
        long associatedSubtypeNb = associatedSubtypePage.getTotalElements();
        if (associatedSubtypeNb > 0) {
            log.error("deleteSealCertificate error: the seal certificate is associated to {} subtype(s)", associatedSubtypeNb);
            throw new LocalizedStatusException(NOT_ACCEPTABLE,
                    "message.cannot_delete_seal_certificate_linked_to_subtypes",
                    associatedSubtypeNb,
                    associatedSubtypePage.getContent().stream().map(Subtype::getName).toList());
        }

        // Actual delete

        secretService.deleteSealCertificate(tenant.getId(), sealCertificateId);
        statsService.registerAdminAction(tenant, SEAL_CERTIFICATE, DELETE, sealCertificateId);
    }


    @GetMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/seal-certificate")
    @Operation(summary = "List seal certificates", description = SealCertificateSortBy.Constants.API_DOC_SORT_BY_VALUES)
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<SealCertificateRepresentation> listSealCertificate(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                                   @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                                   @TenantResolved Tenant tenant,
                                                                   @PageableDefault(sort = SealCertificateSortBy.Constants.NAME_VALUE)
                                                                   @ParameterObject Pageable pageable) {

        log.debug("listSealCertificate page:{} pageSize:{}", pageable.getPageNumber(), pageable.getPageSize());

        Pageable innerPageable = RequestUtils.convertSortedPageable(pageable, SealCertificateSortBy.class, SealCertificateSortBy::getFieldName);
        Page<SealCertificateRepresentation> result = secretService.getSealCertificateList(tenant.getId(), innerPageable);

        // Computing transient values

        Set<String> certIds = result.getContent().stream().map(SealCertificateRepresentation::getId).collect(toSet());
        Map<String, Long> idCounts = subtypeRepository.fetchCountBySealCertificateIdIn(certIds)
                .stream()
                .collect(toMap(IdCount::getId, IdCount::getCount));

        result.getContent().forEach(s -> s.setUsageCount(idCounts.getOrDefault(s.getId(), 0L)));

        // Sending back result

        return result;
    }


    // </editor-fold desc="Seal certificate CRUDL">


    @GetMapping(API_INTERNAL + "/admin/tenant/{tenantId}/sealCertificate/{sealCertificateId}/signatureImage")
    @Operation(summary = "Get seal's signature image")
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200, content = @Content(mediaType = APPLICATION_OCTET_STREAM_VALUE)),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void getSealSignatureImage(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                      @PathVariable(name = Tenant.API_PATH) String tenantId,
                                      @Parameter(hidden = true)
                                      @TenantResolved Tenant tenant,
                                      @Parameter(description = SealCertificateDto.API_DOC_ID)
                                      @PathVariable(name = SealCertificateDto.API_ID_PATH) String sealCertificateId,
                                      @Parameter(hidden = true)
                                      @SealCertificateResolved SealCertificate sealCertificate,
                                      HttpServletResponse response) {

        // Integrity check

        if (StringUtils.isEmpty(sealCertificate.getSignatureImageContentId())) {
            throw new LocalizedStatusException(NOT_FOUND, "message.no_signature_for_the_given_seal_certificate_id");
        }

        // Image fetch

        DocumentBuffer documentBuffer = contentService.getSignatureImage(sealCertificate.getSignatureImageContentId());
        log.debug("getSealSignatureImage imageContentId:{} buffer:{}", sealCertificate.getSignatureImageContentId(), documentBuffer);
        RequestUtils.writeBufferToResponse(documentBuffer, response);
    }


    @GetMapping(API_INTERNAL + "/admin/tenant/{tenantId}/sealCertificate/{sealCertificateId}/subtypeUsage")
    @Operation(summary = "List subtypes using the given certificate")
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<SubtypeRepresentation> listSubtypeUsage(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                        @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                        @Parameter(description = SealCertificateDto.API_DOC_ID)
                                                        @PathVariable(name = SealCertificateDto.API_ID_PATH) String sealCertificateId,
                                                        @SealCertificateResolved SealCertificate sealCertificate,
                                                        @ParameterObject Pageable pageable) {

        log.debug("listSealCertificate page:{} pageSize:{}", pageable.getPageNumber(), pageable.getPageSize());

        return subtypeRepository.findByTenant_IdAndSealCertificateId(tenantId, sealCertificate.getId(), pageable)
                .map(subtype -> modelMapper.map(subtype, SubtypeRepresentation.class));

    }


    private SealCertificate parseSealCertificateFromFile(MultipartFile certificateFile, String password) {
        SealCertificate sealCertificate = new SealCertificate();

        try (InputStream p12InputStream = certificateFile.getInputStream()) {

            KeyStore ks = KeyStore.getInstance("PKCS12");
            ks.load(p12InputStream, firstNonNull(password, EMPTY).toCharArray());

            Enumeration<String> aliasEnum = ks.aliases();

            PrivateKey privateKey = null;
            X509Certificate certificate = null;

            while (aliasEnum.hasMoreElements()) {
                String keyName = aliasEnum.nextElement();
                privateKey = (PrivateKey) ks.getKey(keyName, password.toCharArray());
                certificate = (X509Certificate) ks.getCertificate(keyName);
            }

            if (privateKey == null) {
                throw new CertificateException("Could not read private key from given file");
            }
            if (certificate == null) {
                throw new CertificateException("Could not read certificate from given file");
            }

            JcaX509CertificateHolder certificateHolder = new JcaX509CertificateHolder(certificate);

            sealCertificate.setExpirationDate(certificateHolder.getNotAfter());
            sealCertificate.setPublicCertificateBase64(Base64.getEncoder().encodeToString(certificate.getEncoded()));
            sealCertificate.setPrivateKeyBase64(Base64.getEncoder().encodeToString(privateKey.getEncoded()));

        } catch (UnrecoverableKeyException e) {
            // PrivateKey password error
            throw new LocalizedStatusException(BAD_REQUEST, "message.certificate_bad_password");
        } catch (IOException | CertificateException | NoSuchAlgorithmException | KeyStoreException e) {
            // KeyStore password error. It throws an IOException.
            // that we cannot differentiate from an InputStream one. We have to parse the message...
            if (StringUtils.contains(e.getMessage(), "password was incorrect")) {
                throw new LocalizedStatusException(BAD_REQUEST, "message.certificate_bad_password");
            }
            throw new LocalizedStatusException(BAD_REQUEST, "message.could_not_read_certificate_content");
        }

        sealCertificate = Optional.of(sealCertificate)
                .filter(c -> StringUtils.isNotEmpty(c.getPublicCertificateBase64()))
                .filter(c -> StringUtils.isNotEmpty(c.getPrivateKeyBase64()))
                .filter(c -> Objects.nonNull(c.getExpirationDate()))
                .orElseThrow(() -> new LocalizedStatusException(BAD_REQUEST, "message.could_not_read_certificate_content"));

        return sealCertificate;
    }


    private String setSealSignatureImage(Tenant tenant, SealCertificate sealCertificate, MultipartFile file) {

        // Updating data

        String imageSignatureNodeId;
        try (InputStream body = file.getInputStream()) {

            DocumentBuffer imageBuffer = new DocumentBuffer();
            imageBuffer.setName(file.getOriginalFilename());
            imageBuffer.setContentFlux(readInputStream(() -> body, new DefaultDataBufferFactory(), FILE_TRANSFER_BUFFER_SIZE));

            imageSignatureNodeId = Optional.of(sealCertificate)
                    .map(SealCertificate::getSignatureImageContentId)
                    .filter(StringUtils::isNotEmpty)
                    .map(contentId -> contentService.updateSignatureImage(contentId, imageBuffer))
                    .orElseGet(() -> contentService.createSealCertificateSignatureImage(tenant, sealCertificate.getId(), imageBuffer));

        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_reading_file");
        }

        sealCertificate.setSignatureImageContentId(imageSignatureNodeId);
        secretService.storeSealCertificate(tenant.getId(), sealCertificate);

        return imageSignatureNodeId;
    }


}
