/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller.admin;

import coop.libriciel.ipcore.business.workflow.WorkflowBusinessService;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.workflow.*;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.database.SubtypeRepository;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.services.resolvers.WorkflowDefinitionResolver.WorkflowDefinitionResolved;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.RequestUtils;
import coop.libriciel.ipcore.utils.XmlUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.modelmapper.ModelMapper;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.IntStream;

import static coop.libriciel.ipcore.IpCoreApplication.API_INTERNAL;
import static coop.libriciel.ipcore.IpCoreApplication.API_PROVISIONING_V1;
import static coop.libriciel.ipcore.model.stats.StatsCategory.WORKFLOW;
import static coop.libriciel.ipcore.model.workflow.Action.*;
import static coop.libriciel.ipcore.model.workflow.WorkflowDefinition.*;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.RequestUtils.PREAUTHORIZE_TENANT_ADMIN_OR_MORE;
import static coop.libriciel.ipcore.utils.XmlUtils.OUTPUTKEYS_INDENT_AMOUNT;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.stream.Collectors.toSet;
import static javax.xml.XMLConstants.*;
import static javax.xml.transform.OutputKeys.*;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.springframework.data.domain.Pageable.unpaged;
import static org.springframework.http.HttpStatus.*;


@Log4j2
@RestController
@Tag(name = "admin-workflow-definition", description = "Reserved operations on workflow definitions")
public class AdminWorkflowDefinitionController {


    private static final String EVALUATED_DESK_IDS_DESCRIPTION = EMPTY
            + "Some desk placeholders can be evaluated on every Folder creation.  \n"
            + "These placeholders can be passed as a regular id, using one of the reserved values:\n"
            + "- Emitter: `" + RESERVED_EMITTER_ID_PLACEHOLDER + "`  \n"
            + "- Boss of: `" + RESERVED_BOSS_OF_ID_PLACEHOLDER + "`  \n"
            + "- Variable desk: `" + RESERVED_VARIABLE_DESK_ID_PLACEHOLDER + "`";
    public static final int MAX_DEFINITION_PAGE_SIZE = 200;


    // <editor-fold desc="Beans">


    private final AuthServiceInterface authService;
    private final ModelMapper modelMapper;
    private final StatsServiceInterface statsService;
    private final SubtypeRepository subtypeRepository;
    private final WorkflowBusinessService workflowBusinessService;
    private final WorkflowServiceInterface workflowService;


    @Autowired
    public AdminWorkflowDefinitionController(AuthServiceInterface authService,
                                             ModelMapper modelMapper,
                                             StatsServiceInterface statsService,
                                             SubtypeRepository subtypeRepository,
                                             WorkflowServiceInterface workflowService,
                                             WorkflowBusinessService workflowBusinessService) {
        this.authService = authService;
        this.modelMapper = modelMapper;
        this.statsService = statsService;
        this.subtypeRepository = subtypeRepository;
        this.workflowService = workflowService;
        this.workflowBusinessService = workflowBusinessService;
    }


    // </editor-fold desc="Beans">


    // <editor-fold desc="Utils">


    static @NotNull String patchBpmn(ClassLoader loader, InputStream bpmnInputStream) throws TransformerException, IOException {

        SAXTransformerFactory stf = (SAXTransformerFactory) TransformerFactory.newInstance();
        stf.setAttribute(ACCESS_EXTERNAL_DTD, EMPTY);
        stf.setAttribute(ACCESS_EXTERNAL_STYLESHEET, EMPTY);
        stf.setFeature(FEATURE_SECURE_PROCESSING, true);

        TransformerHandler th1 = stf.newTransformerHandler(new StreamSource(loader.getResourceAsStream("bpmn/patches/add_flowable_xmlns.xslt")));
        TransformerHandler th2 = stf.newTransformerHandler(new StreamSource(loader.getResourceAsStream("bpmn/patches/set_process_executable.xslt")));
        TransformerHandler th3 = stf.newTransformerHandler(new StreamSource(loader.getResourceAsStream("bpmn/patches/callActivity_attributes.xslt")));
        TransformerHandler th4 = stf.newTransformerHandler(new StreamSource(loader.getResourceAsStream("bpmn/patches/callActivity_candidates.xslt")));
        TransformerHandler th5 = stf.newTransformerHandler(new StreamSource(loader.getResourceAsStream("bpmn/patches/callActivity_undo.xslt")));
        TransformerHandler th6 = stf.newTransformerHandler(new StreamSource(loader.getResourceAsStream("bpmn/patches/one_liner.xslt")));

        // There are some differences between OracleJDK and OpenJDK results.
        // Removing new lines (with the one_liner.xslt, and re-indenting it makes the results unit-tests friendly.

        TransformerHandler th7 = stf.newTransformerHandler();
        th7.getTransformer().setOutputProperty(METHOD, "xml");
        th7.getTransformer().setOutputProperty(ENCODING, UTF_8.name());
        th7.getTransformer().setOutputProperty(INDENT, "yes");
        th7.getTransformer().setOutputProperty(OUTPUTKEYS_INDENT_AMOUNT, "2");

        // Building the chain of transformers, and feeding the inputStream

        String canonicalized;
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {

            th1.setResult(new SAXResult(th2));
            th2.setResult(new SAXResult(th3));
            th3.setResult(new SAXResult(th4));
            th4.setResult(new SAXResult(th5));
            th5.setResult(new SAXResult(th6));
            th6.setResult(new SAXResult(th7));
            th7.setResult(new StreamResult(baos));

            stf.newTransformer().transform(new StreamSource(bpmnInputStream), new SAXResult(th1));

            // There are some differences between OracleJDK and OpenJDK results.
            // Canonicalization makes the results unit-tests friendly.
            canonicalized = XmlUtils.canonicalize(baos.toByteArray());
        }

        // Deleting line feeds, from https://bpmn.io/
        canonicalized = StringUtils.replace(canonicalized, "&#xA;\"", "\"");

        return canonicalized;
    }


    static boolean isParallelStep(@Nullable StepDefinitionDto stepDefinitionDto) {
        return Optional.ofNullable(stepDefinitionDto)
                .filter(s -> CollectionUtils.isNotEmpty(s.getValidatingDeskIds()))
                .filter(s -> s.getValidatingDeskIds().size() > 1)
                .isPresent();
    }


    static void checkDefinitionIntegrity(WorkflowDefinitionDto definitionDto) {

        // Nullity checks

        if (definitionDto.getSteps() == null) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.steps_cannot_be_null");
        }

        if (definitionDto.getSteps().stream().anyMatch(Objects::isNull)) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.steps_cannot_be_null");
        }

        if (definitionDto.getSteps().stream().map(StepDefinitionDto::getValidatingDeskIds).anyMatch(Objects::isNull)) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.validator_ids_cannot_be_null");
        }

        if (definitionDto.getSteps().stream().flatMap(s -> s.getValidatingDeskIds().stream()).anyMatch(Objects::isNull)) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.validator_ids_cannot_be_null");
        }

        // Actions check

        int stepListSize = definitionDto.getSteps().size();

        IntStream.range(0, stepListSize - 1)
                .mapToObj(i -> definitionDto.getSteps().get(i))
                .map(StepDefinitionDto::getType)
                .filter(type -> type == StepDefinitionType.ARCHIVE)
                .findFirst()
                .ifPresent(s -> {throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.the_archive_type_is_only_allowed_in_the_last_step", s);});

        // Last step checks

        StepDefinitionDto lastStepDefinition = definitionDto.getSteps().get(stepListSize - 1);

        if (lastStepDefinition.getType() != StepDefinitionType.ARCHIVE) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.the_last_step_should_be_an_archive_type");
        }

        if (CollectionUtils.size(lastStepDefinition.getValidatingDeskIds()) != 1) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.the_last_step_should_have_exactly_one_validator");
        }

        boolean lastStepHasMandatoryValidationMetadata = CollectionUtils.isNotEmpty(lastStepDefinition.getMandatoryValidationMetadataIds());
        boolean lastStepHasMandatoryRejectionMetadata = CollectionUtils.isNotEmpty(lastStepDefinition.getMandatoryRejectionMetadataIds());
        if (lastStepHasMandatoryValidationMetadata || lastStepHasMandatoryRejectionMetadata) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.the_last_step_cannot_have_a_mandatory_metadata");
        }

        // Disabling variable desks in parallel steps (for now)

        definitionDto.getSteps().stream()
                .filter(AdminWorkflowDefinitionController::isParallelStep)
                .map(StepDefinitionDto::getValidatingDeskIds)
                .filter(s -> RESERVED_GENERIC_VALIDATORS_IDS_PLACEHOLDERS.stream().anyMatch(s::contains))
                .findFirst()
                .ifPresent(s -> {throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.parallel_steps_cannot_contain_variable_desks");});

        // Disabling external actions in parallel steps

        if (definitionDto.getSteps().stream()
                .filter(s -> Action.isExternal(s.getType().toAction()))
                .anyMatch(AdminWorkflowDefinitionController::isParallelStep)) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.external_actions_cannot_be_parallel");
        }

        // Cannot have a BOSS OF step after a parallel one

        IntStream.range(1, definitionDto.getSteps().size())
                .filter(i -> definitionDto.getSteps().get(i).getValidatingDeskIds().stream()
                        .anyMatch(id -> StringUtils.equals(RESERVED_BOSS_OF_ID_PLACEHOLDER, id))
                )
                .filter(i -> isParallelStep(definitionDto.getSteps().get(i - 1)))
                .findFirst()
                .ifPresent(s -> {throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.boss_of_generic_step_cannot_be_after_a_parallel_one");});
    }


    void verifyEachTargetDeskExistence(WorkflowDefinition definition) {

        Set<DeskRepresentation> targetDesks = new HashSet<>();
        targetDesks.add(definition.getFinalDesk());
        targetDesks.addAll(definition.getFinalNotifiedDesks());
        targetDesks.addAll(definition.getSteps().stream().flatMap(s -> s.getValidatingDesks().stream()).collect(toSet()));
        targetDesks.addAll(definition.getSteps().stream().flatMap(s -> s.getNotifiedDesks().stream()).collect(toSet()));

        Set<String> targetDeskIds = targetDesks.stream()
                .map(DeskRepresentation::getId)
                .filter(deskId -> !GENERIC_VALIDATORS_IDS_PLACEHOLDERS.contains(deskId))
                .filter(deskId -> !RESERVED_GENERIC_VALIDATORS_IDS_PLACEHOLDERS.contains(deskId))
                .collect(toSet());

        Map<String, String> existingDeskNames = authService.getDeskNames(targetDeskIds);

        targetDeskIds.stream()
                .filter(i -> existingDeskNames.get(i) == null)
                .findFirst()
                .ifPresent(i -> {throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.s_is_not_a_known_desk_id_or_short_name", i);});

        // Since we have those, we can update them...

        targetDesks.forEach(deskRepresentation -> deskRepresentation.setName(existingDeskNames.get(deskRepresentation.getId())));
    }


    // </editor-fold desc="Utils">


    // <editor-fold desc="WorkflowDefinition CRUDL">


//    @PostMapping(API_V1 + "/admin/tenant/{tenantId}")
//    @Operation(summary = "Create a workflow definition")
//    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
//    @ResponseStatus(CREATED)
//    @ApiResponses(value = {
//            @ApiResponse(responseCode = "201", description = "Created"),
//            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
//            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)"),
//            @ApiResponse(responseCode = "404", description = "(At least) one of the desk called in the BPMN doesn't exist")
//    })
//    public void createWorkflowDefinition(@RequestParam MultipartFile bpmnXmlFile,
//                                         @RequestParam(required = false) String workflowName) {
//
//        // Patch the given XML to Flowable parameters
//
//        String bpmn;
//        try {
//            bpmn = IOUtils.toString(bpmnXmlFile.getInputStream(), UTF_8);
//            log.debug("BPMN definition :\n{}", bpmn);
//        } catch (IOException e) {
//            log.error("Error on WorkflowDefinition creation", e);
//            return;
//        }
//
//        // Retrieving desk names and IDs
//
//        Set<String> desks = new HashSet<>();
//        Matcher m = Pattern
//                .compile("<.*?callActivity.*?name=\"(?:visa|signature) (.*?)\"")
//                .matcher(bpmn);
//
//        while (m.find()) {
//            desks.add(m.group(1));
//        }
//
//        // Mapping to desks Ids from Keycloak
//
//        log.info("BPMN desks to patch:" + desks);
//
//        for (String desk : desks) {
//
//            String deskId = Optional.ofNullable(authService.findDeskById(desk))
//                    .or(() -> Optional.ofNullable(authService.findDeskByName(desk)))
//                    .orElseThrow(() -> new ResponseStatusException(NOT_FOUND, String.format("The desk named '%s' is neither a deskId nor a deskName", desk)))
//                    .getId();
//
//            bpmn = bpmn.replaceAll(
//                    "(<.*?callActivity.*?name=\")(visa|signature) " + desk + "\"",
//                    "$1$2 " + deskId + "\""
//            );
//
//            log.debug("Replacing {} with {} done", desk, deskId);
//        }
//
//        log.info("BPMN after patch :\n" + bpmn);
//
//        // Upload to Flowable
//
//        String workflowDefinitionName = ObjectUtils.firstNonNull(workflowName, bpmnXmlFile.getOriginalFilename(), bpmnXmlFile.getName());
//        workflowService.createWorkflowDefinition(bpmn, workflowDefinitionName);
//        statsService.registerAdminAction(WORKFLOW, CREATE, workflowDefinitionName);
//    }


    @PostMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/workflow-definition")
    @Operation(
            summary = "Create a workflow definition",
            description = EVALUATED_DESK_IDS_DESCRIPTION
    )
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_201),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_409, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public WorkflowDefinitionDto createWorkflowDefinition(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                          @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                          @TenantResolved Tenant tenant,
                                                          @RequestBody @Valid WorkflowDefinitionDto workflowDefinitionDto) {

        log.info("createWorkflowDefinition key:{} name:{}", workflowDefinitionDto.getKey(), workflowDefinitionDto.getName());

        // Integrity check

        checkDefinitionIntegrity(workflowDefinitionDto);

        WorkflowDefinition workflowDefinition = workflowBusinessService.convertToInternal(workflowDefinitionDto);

        Page<WorkflowDefinitionRepresentation> workflowList = workflowService.listWorkflowDefinitions(tenant.getId(), Pageable.ofSize(1), null);

        long workflowsCount = workflowList.getTotalElements();
        int limit = tenant.getWorkflowLimit() != null ? tenant.getWorkflowLimit() : MAX_WORKFLOWS_COUNT;
        if (workflowsCount >= limit) {
            log.error("workflow definitions limit exceeded, abort workflow definition creation");
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE,
                    "message.already_n_workflow_maximum_reached",
                    workflowsCount);
        }

        verifyEachTargetDeskExistence(workflowDefinition);

        // TODO implement this check directly in Workflow service
        long totalDefinitions = workflowList.getTotalElements();
        long currentNbElem = 0;
        for (int pageIdx = 0;
             currentNbElem < totalDefinitions && pageIdx < workflowList.getTotalPages() - 1;
             pageIdx++, currentNbElem += workflowList.getNumberOfElements()) {

            workflowList = workflowService.listWorkflowDefinitions(tenant.getId(),
                    Pageable.ofSize(MAX_DEFINITION_PAGE_SIZE).withPage(pageIdx),
                    workflowDefinitionDto.getName());

            if (workflowList.getContent().stream()
                    .map(WorkflowDefinitionRepresentation::getName)
                    .anyMatch(existingName -> StringUtils.equalsIgnoreCase(existingName, workflowDefinitionDto.getName()))) {
                log.error("A workflow definitions already exists with this name, abort workflow definition creation");
                throw new LocalizedStatusException(CONFLICT, "message.this_workflow_name_already_exists");
            }
        }

        if (workflowService.getWorkflowDefinitionByKey(tenant.getId(), workflowDefinition.getKey()).isPresent()) {
            log.error("A workflow definitions already exists with this key, abort workflow definition creation");
            throw new LocalizedStatusException(CONFLICT, "message.this_workflow_key_already_exists");
        }

        // Actual save

        workflowService.createWorkflowDefinition(tenant.getId(), workflowDefinition);
        statsService.registerAdminAction(tenant, WORKFLOW, CREATE, workflowDefinition.getKey());

        workflowBusinessService.populateDesksAndValidators(workflowDefinition);
        return workflowBusinessService.convertToDto(workflowDefinition);
    }


    @GetMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/workflow-definition/{workflowDefinitionKey}")
    @Operation(summary = "Get a workflow definition with every information set")
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public WorkflowDefinitionDto getWorkflowDefinition(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                       @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                       @TenantResolved Tenant tenant,
                                                       @Parameter(description = WorkflowDefinition.API_KEY_DOC_VALUE)
                                                       @PathVariable(name = WorkflowDefinition.API_KEY_PATH) String workflowDefinitionKey,
                                                       @WorkflowDefinitionResolved WorkflowDefinition workflowDefinition) {
        log.debug("getWorkflowDefinition tenantId:{} key:{}", tenantId, workflowDefinitionKey);
        workflowBusinessService.populateDesksAndValidators(workflowDefinition);
        return workflowBusinessService.convertToDto(workflowDefinition);
    }


    @PutMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/workflow-definition/{workflowDefinitionKey}")
    @Operation(
            summary = "Edit a workflow definition",
            description = EVALUATED_DESK_IDS_DESCRIPTION
    )
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_406, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_409, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public WorkflowDefinitionDto updateWorkflowDefinition(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                          @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                          @TenantResolved Tenant tenant,
                                                          @Parameter(description = WorkflowDefinition.API_KEY_DOC_VALUE)
                                                          @PathVariable(name = WorkflowDefinition.API_KEY_PATH) String workflowDefinitionKey,
                                                          @WorkflowDefinitionResolved WorkflowDefinition existingWorkflowDefinition,
                                                          @RequestBody @Valid WorkflowDefinitionDto definition) {

        log.info("updateWorkflowDefinition key:{} stepsCount:{}", workflowDefinitionKey, definition.getSteps().size());

        // Integrity check

        if (!StringUtils.equals(workflowDefinitionKey, definition.getKey())) {
            log.error("The workflow definition key cannot be changed, abort workflow definition edition");
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.workflow_keys_are_immutable");
        }

        workflowService.getWorkflowDefinitionByKey(tenant.getId(), workflowDefinitionKey)
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_workflow_definition_key"));

        if (!StringUtils.equals(existingWorkflowDefinition.getName(), definition.getName())) {
            log.info("An update request is trying to rename the workflow name:{} into:{}", existingWorkflowDefinition.getName(), definition.getName());
            if (workflowService.listWorkflowDefinitions(tenant.getId(), unpaged(), definition.getName()).getContent().stream()
                    .map(WorkflowDefinitionRepresentation::getName)
                    .anyMatch(existingName -> StringUtils.equalsIgnoreCase(existingName, definition.getName()))) {
                log.error("A workflow definitions already exists with this name, abort workflow definition edition");
                throw new LocalizedStatusException(CONFLICT, "message.this_workflow_name_already_exists");
            }
        }

        checkDefinitionIntegrity(definition);
        WorkflowDefinition updatedWorkflowDefinition = workflowBusinessService.convertToInternal(definition);
        verifyEachTargetDeskExistence(updatedWorkflowDefinition);

        // Actual save

        workflowService.updateWorkflowDefinition(tenant.getId(), updatedWorkflowDefinition);
        statsService.registerAdminAction(tenant, WORKFLOW, UPDATE, updatedWorkflowDefinition.getKey());

        workflowBusinessService.populateDesksAndValidators(updatedWorkflowDefinition);
        return workflowBusinessService.convertToDto(updatedWorkflowDefinition);
    }


    /**
     * @deprecated since 5.1.0, and will be removed in 5.2.0.
     */
    @DeleteMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/workflowDefinition/{workflowDefinitionKey}")
    @Operation(
            summary = "Delete a workflow definition",
            description = "This endpoint is deprecated since the 5.1.0, and will be removed in 5.2.0.  \n"
                    + "Please use `DELETE /admin/tenant/{tenantId}/workflow-definition` instead.",
            deprecated = true
    )
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_204),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_409, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    @Deprecated(since = "5.1", forRemoval = true)
    public void deleteWorkflowDefinitionLegacy(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                               @PathVariable(name = Tenant.API_PATH) String tenantId,
                                               @TenantResolved Tenant tenant,
                                               @Parameter(description = WorkflowDefinition.API_KEY_DOC_VALUE)
                                               @PathVariable(name = WorkflowDefinition.API_KEY_PATH) String workflowDefinitionKey,
                                               @WorkflowDefinitionResolved WorkflowDefinition workflowDefinition) {
        log.warn("Deprecated endpoint called : deleteWorkflowDefinition. It will be removed in 5.2.");
        deleteWorkflowDefinition(tenantId, tenant, workflowDefinitionKey, workflowDefinition);
    }


    @DeleteMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/workflow-definition/{workflowDefinitionKey}")
    @Operation(summary = "Delete a workflow definition")
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_204),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_409, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void deleteWorkflowDefinition(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                         @PathVariable(name = Tenant.API_PATH) String tenantId,
                                         @TenantResolved Tenant tenant,
                                         @Parameter(description = WorkflowDefinition.API_KEY_DOC_VALUE)
                                         @PathVariable(name = WorkflowDefinition.API_KEY_PATH) String workflowDefinitionKey,
                                         @WorkflowDefinitionResolved WorkflowDefinition workflowDefinition) {

        log.info("deleteWorkflowDefinition tenant:{} key:{}", tenant.getId(), workflowDefinitionKey);

        Page<Subtype> subtypeUsingDef = subtypeRepository.findAllByTenant_IdAndValidationWorkflowIdOrCreationWorkflowIdOrWorkflowSelectionScriptContaining(
                tenant.getId(), workflowDefinitionKey, workflowDefinitionKey, workflowDefinitionKey, Pageable.ofSize(5));

        if (!subtypeUsingDef.isEmpty()) {
            log.error("A subtype references this workflow definition, it cannot be deleted");
            throw new LocalizedStatusException(
                    CONFLICT,
                    "message.cannot_delete_a_workflow_definition_referenced_by_subtypes",
                    subtypeUsingDef.getTotalElements(),
                    subtypeUsingDef.stream().map(Subtype::getName).toList()
            );
        }

        workflowService.deleteWorkflowDefinition(tenant.getId(), workflowDefinition.getKey());
        statsService.registerAdminAction(tenant, WORKFLOW, DELETE, workflowDefinition.getKey());
    }


    /**
     * @deprecated since 5.1.0, and will be removed in 5.2.0.
     */
    @GetMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/workflowDefinition")
    @Operation(
            summary = "List workflow definitions",
            description = WorkflowDefinitionSortBy.Constants.API_DOC_SORT_BY_VALUES + "\n\n"
                    + "This endpoint is deprecated since the 5.1.0, and will be removed in 5.2.0.  \n"
                    + "Please use `GET /admin/tenant/{tenantId}/workflow-definition` instead.",
            deprecated = true
    )
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    @Deprecated(since = "5.1", forRemoval = true)
    public Page<WorkflowDefinitionRepresentation> listWorkflowDefinitionsLegacy(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                                                @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                                                @TenantResolved Tenant tenant,
                                                                                @PageableDefault(sort = WorkflowDefinitionSortBy.Constants.NAME_VALUE)
                                                                                @ParameterObject Pageable pageable,
                                                                                @Parameter(description = "Searching for a specific workflow name")
                                                                                @RequestParam(required = false) String searchTerm) {
        log.warn("Deprecated endpoint called : listWorkflowDefinitions. It will be removed in 5.2.");
        return this.listWorkflowDefinitions(tenantId, tenant, pageable, searchTerm);
    }


    @GetMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/workflow-definition")
    @Operation(summary = "List workflow definitions", description = WorkflowDefinitionSortBy.Constants.API_DOC_SORT_BY_VALUES)
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<WorkflowDefinitionRepresentation> listWorkflowDefinitions(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                                          @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                                          @TenantResolved Tenant tenant,
                                                                          @PageableDefault(sort = WorkflowDefinitionSortBy.Constants.NAME_VALUE)
                                                                          @ParameterObject Pageable pageable,
                                                                          @Parameter(description = "Searching for a specific workflow name")
                                                                          @RequestParam(required = false) String searchTerm) {

        log.info("getWorkflowDefinitions page:{} pageSize:{} searchTerm:{}", pageable.getPageNumber(), pageable.getPageSize(), searchTerm);
        Pageable innerPageable = RequestUtils.convertSortedPageable(pageable, WorkflowDefinitionSortBy.class, WorkflowDefinitionSortBy::name);
        return workflowService.listWorkflowDefinitions(tenant.getId(), innerPageable, searchTerm);
    }


    // </editor-fold desc="WorkflowDefinition CRUDL">


    @GetMapping(API_INTERNAL + "/admin/tenant/{tenantId}/workflowDefinitionByKey/{workflowDefinitionKey}/referencingSubtypes")
    @Operation(summary = "Get subtypes referencing the given workflow definition key")
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public List<Subtype> getSubtypesReferencingWorkflowDefinitionKey(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                                     @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                                     @TenantResolved Tenant tenant,
                                                                     @PathVariable(name = API_KEY_PATH) String workflowDefinitionKey,
                                                                     @WorkflowDefinitionResolved WorkflowDefinition workflowDefinition) {
        return subtypeRepository.getSubtypesReferencingWorkflowKey(tenant, workflowDefinitionKey);
    }


}
