/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller.admin;

import coop.libriciel.ipcore.business.auth.DeskBusinessService;
import coop.libriciel.ipcore.business.auth.UserBusinessService;
import coop.libriciel.ipcore.business.database.MetadataBusinessService;
import coop.libriciel.ipcore.business.typology.TypologyBusinessService;
import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.HierarchisedDeskRepresentation;
import coop.libriciel.ipcore.model.auth.requests.DeskDto;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.database.Type;
import coop.libriciel.ipcore.model.permission.Delegation;
import coop.libriciel.ipcore.model.permission.DelegationDto;
import coop.libriciel.ipcore.model.permission.DelegationSortBy;
import coop.libriciel.ipcore.model.permission.DelegationUpdateDto;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.WorkflowDefinition;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.services.resolvers.DelegationResolver.DelegationResolved;
import coop.libriciel.ipcore.services.resolvers.DeskResolver.DeskResolved;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.RequestUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.keycloak.admin.client.resource.ClientScopeResource;
import org.keycloak.admin.client.resource.ResourceResource;
import org.keycloak.admin.client.resource.RoleResource;
import org.modelmapper.ModelMapper;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import static coop.libriciel.ipcore.IpCoreApplication.*;
import static coop.libriciel.ipcore.business.database.MetadataBusinessService.METADATA_REPRESENTATION_NAME_COMPARATOR;
import static coop.libriciel.ipcore.model.permission.DelegationSortBy.Constants.SUBSTITUTE_DESK_VALUE;
import static coop.libriciel.ipcore.model.permission.DelegationSortBy.SUBSTITUTE_DESK;
import static coop.libriciel.ipcore.model.permission.DelegationSortBy.generateDelegationDtoComparator;
import static coop.libriciel.ipcore.model.stats.StatsCategory.DESK;
import static coop.libriciel.ipcore.model.workflow.Action.*;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.CollectionUtils.toMutableList;
import static coop.libriciel.ipcore.utils.PaginatedList.MAX_PAGE_SIZE;
import static coop.libriciel.ipcore.utils.RequestUtils.PREAUTHORIZE_TENANT_FUNCTIONAL_ADMIN_OR_MORE;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.springframework.http.HttpStatus.*;


@Log4j2
@RestController
@Tag(name = "admin-desk", description = "Reserved operations on desks")
public class AdminDeskController {


    // <editor-fold desc="Beans">


    private final AuthServiceInterface authService;
    private final DeskBusinessService deskBusinessService;
    private final MetadataBusinessService metadataBusinessService;
    private final ModelMapper modelMapper;
    private final PermissionServiceInterface permissionService;
    private final StatsServiceInterface statsService;
    private final TypologyBusinessService typologyBusinessService;
    private final UserBusinessService userBusinessService;
    private final WorkflowServiceInterface workflowService;


    @Autowired
    public AdminDeskController(AuthServiceInterface authService,
                               DeskBusinessService deskBusinessService,
                               MetadataBusinessService metadataBusinessService,
                               ModelMapper modelMapper,
                               PermissionServiceInterface permissionService,
                               StatsServiceInterface statsService,
                               TypologyBusinessService typologyBusinessService,
                               UserBusinessService userBusinessService,
                               WorkflowServiceInterface workflowService) {
        this.authService = authService;
        this.deskBusinessService = deskBusinessService;
        this.metadataBusinessService = metadataBusinessService;
        this.modelMapper = modelMapper;
        this.permissionService = permissionService;
        this.statsService = statsService;
        this.typologyBusinessService = typologyBusinessService;
        this.userBusinessService = userBusinessService;
        this.workflowService = workflowService;
    }


    // </editor-fold desc="Beans">


    // <editor-fold desc="Desks CRUDL">


    /**
     * {@link Desk} do have a particular Keycloak modelization :
     * - Since it gives specific capacities to users, they are (realm) {@link RoleResource}. Metadata are stored there, as Roles attributes
     * - Since they can be associated with actions/permissions/visibilities, Desks are {@link ResourceResource} too.
     * - Since they can be narrowed, and offers role-specific permissions, they are {@link ClientScopeResource} as well.
     * <p>
     * In a typical use case, any user linked to a Desk will have access/permissions defined in the Core client's Authorization tab.
     * <p>
     * This overly-complex representation may be too much for a simple use-case.
     * But it allows some additional constraints (removing some action permissions for a desk),
     * or some additional permissions (adding some kind of delegation between two desks).
     *
     * @param tenant  the target tenant
     * @param request any representation model, without any id
     * @return the generated (and internal) ider
     */
    @PostMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/desk")
    @Operation(summary = "Create a desk")
    @PreAuthorize(PREAUTHORIZE_TENANT_FUNCTIONAL_ADMIN_OR_MORE)
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_201),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_409, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_507, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public DeskDto createDesk(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                              @PathVariable(name = Tenant.API_PATH) String tenantId,
                              @TenantResolved Tenant tenant,
                              @Parameter(description = "body", required = true)
                              @RequestBody @Valid DeskDto request) {

        log.info("createDesk tenant:{} name:{} desc:{}", tenant.getId(), request.getName(), request.getDescription());

        // Integrity check

        deskBusinessService.createDeskIntegrityChecks(request, tenant);

        // Auth creation

        String createdDeskId = null;
        try {
            createdDeskId = authService.createDesk(
                    tenant.getId(),
                    request.getName(),
                    request.getShortName(),
                    request.getDescription(),
                    request.getParentDeskId()
            );
            authService.addUsersToDesk(createdDeskId, request.getOwnerIds());
            request.setId(createdDeskId);
            permissionService.createDeskResource(tenant.getId(), modelMapper.map(request, Desk.class));

        } catch (Exception e) {
            if (createdDeskId != null) {
                log.debug("Ann error occurred during the desk creation. Deleting incomplete desk : {}", createdDeskId);
                authService.deleteDesk(createdDeskId);
            }
            throw e;
        }

        // Permissions creation

        permissionService.associateDesks(tenant.getId(), createdDeskId, request.getAssociatedDeskIds());
        permissionService.addFilterableMetadata(tenant.getId(), createdDeskId, request.getFilterableMetadataIds());
        permissionService.addSupervisorsToDesk(tenant.getId(), createdDeskId, request.getSupervisorIds());
        permissionService.addDelegationManagersToDesk(tenant.getId(), createdDeskId, request.getDelegationManagerIds());

        // Update inner values

        DeskDto result = modelMapper.map(request, DeskDto.class);
        request.setTenantId(tenant.getId());
        userBusinessService.updateInnerUserValues(request);
        deskBusinessService.updateInnerDeskValues(tenant.getId(), request);
        metadataBusinessService.updateInnerValues(request, METADATA_REPRESENTATION_NAME_COMPARATOR);

        this.deskBusinessService.sanitizeDeskDto(result);

        statsService.registerAdminAction(tenant, DESK, CREATE, createdDeskId);
        return result;
    }


    @GetMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/desk/{deskId}")
    @Operation(summary = "Get a full desk description")
    @PreAuthorize(PREAUTHORIZE_TENANT_FUNCTIONAL_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public DeskDto getDeskAsAdmin(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                  @PathVariable(name = Tenant.API_PATH) String tenantId,
                                  @TenantResolved Tenant tenant,
                                  @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                  @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                  @DeskResolved Desk desk) {

        log.debug("getDeskAsAdmin id:{}", desk.getId());

        if (!permissionService.currentUserHasAdminRightOnDesk(tenant.getId(), desk.getId())) {
            log.debug("User is not super admin, tenant admin nor functional admin for this desk, abort");
            throw new LocalizedStatusException(FORBIDDEN, "message.you_can_t_administrate_this_desk");
        }

        permissionService.retrieveAndRefreshDeskPermissions(tenant.getId(), desk);

        // Retrieve internal permission elements

        desk.setOwners(authService.listUsersFromDesk(tenant.getId(), desk.getId(), 0, MAX_PAGE_SIZE).getData());
        desk.setFilterableMetadataIds(permissionService.getFilterableMetadataIds(tenant.getId(), desk.getId()));
        desk.setSupervisorIds(permissionService.getSupervisorIdsFromDesk(tenant.getId(), desk.getId()));
        desk.setDelegationManagerIds(permissionService.getDelegationManagerIdsFromDesk(tenant.getId(), desk.getId()));
        desk.setAssociatedDeskIds(permissionService.getAssociatedDeskIds(tenant.getId(), desk.getId()));

        // Retrieve names and return

        DeskDto result = modelMapper.map(desk, DeskDto.class);
        userBusinessService.updateInnerUserValues(result);
        deskBusinessService.updateInnerDeskValues(tenantId, result);
        metadataBusinessService.updateInnerValues(result, METADATA_REPRESENTATION_NAME_COMPARATOR);

        this.deskBusinessService.sanitizeDeskDto(result);

        return result;
    }


    @PutMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/desk/{deskId}")
    @Operation(summary = "Edit a desk")
    @PreAuthorize(PREAUTHORIZE_TENANT_FUNCTIONAL_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_409, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_507, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public DeskDto editDesk(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                            @PathVariable(name = Tenant.API_PATH) String tenantId,
                            @TenantResolved Tenant tenant,
                            @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                            @PathVariable(name = DeskRepresentation.API_PATH) String existingDeskId,
                            @DeskResolved Desk existingDesk,
                            @Parameter(description = "body", required = true)
                            @RequestBody @Valid DeskDto request) {

        log.info("edit desk name:{} desc:{}", request.getName(), request.getDescription());

        if (!permissionService.currentUserHasAdminRightOnDesk(tenant.getId(), existingDesk.getId())) {
            log.debug("User is not super admin, tenant admin nor functional admin for this desk, abort");
            throw new LocalizedStatusException(FORBIDDEN, "message.you_can_t_administrate_this_desk");
        }

        permissionService.retrieveAndRefreshDeskPermissions(tenant.getId(), existingDesk);
        deskBusinessService.updateDeskIntegrityChecks(existingDesk, request, tenant.getId());
        deskBusinessService.computeDifferencesAndUpdateDesk(existingDesk, request, tenant.getId());

        // Update inner values

        request.setId(existingDesk.getId());
        request.setTenantId(tenant.getId());
        userBusinessService.updateInnerUserValues(request);
        deskBusinessService.updateInnerDeskValues(tenant.getId(), request);
        metadataBusinessService.updateInnerValues(request, METADATA_REPRESENTATION_NAME_COMPARATOR);

        statsService.registerAdminAction(tenant, DESK, UPDATE, existingDesk.getId());

        this.deskBusinessService.sanitizeDeskDto(request);

        return request;
    }


    @DeleteMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/desk/{deskId}")
    @Operation(summary = "Delete a desk")
    @PreAuthorize(PREAUTHORIZE_TENANT_FUNCTIONAL_ADMIN_OR_MORE)
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_204),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_406, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void deleteDesk(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                           @PathVariable(name = Tenant.API_PATH) String tenantId,
                           @TenantResolved Tenant tenant,
                           @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                           @PathVariable String deskId,
                           @DeskResolved Desk desk) throws Exception {

        log.info("deleteDesk tenantId:{}, deskId:{}", tenant.getId(), desk.getId());

        // We need to search from DB to have the children count
        Page<Desk> desksSearchedFromDb = authService.searchDesks(tenantId, Pageable.unpaged(), emptyList(), desk.getName(), false);
        desksSearchedFromDb.getContent().stream()
                .filter(deskWithChildCount -> StringUtils.equals(deskWithChildCount.getId(), desk.getId()))
                .filter(deskWithChildCount -> deskWithChildCount.getDirectChildrenCount() != 0)
                .findFirst()
                .ifPresent(deskWithChildCount -> {
                    log.error("Desk deletion cancelled due to the presence of {} children desks", deskWithChildCount.getDirectChildrenCount());
                    throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.cannot_delete_a_desk_with_children");
                });


        Page<WorkflowDefinition> workflowsUsingDeskId = workflowService.getWorkflowsUsingDeskId(deskId, desk.getTenantId());

        if (!workflowsUsingDeskId.isEmpty()) {
            Long workflowsNb = workflowsUsingDeskId.getTotalElements();
            log.error("deleteDesk error: the desk is used by " + workflowsNb + " workflow(s)");
            throw new LocalizedStatusException(NOT_ACCEPTABLE,
                    "message.cannot_delete_a_desk_used_by_workflows",
                    workflowsNb,
                    workflowsUsingDeskId.get().map(WorkflowDefinition::getName).limit(3).toList());
        }

        Page<Folder> instancesUsingOldWorkflowsByDeskId = workflowService.getInstancesUsingOldWorkflowsByDeskId(desk.getTenantId(), deskId);

        if (!instancesUsingOldWorkflowsByDeskId.isEmpty()) {
            log.error("Desk deletion cancelled due to current folders");
            throw new LocalizedStatusException(
                    NOT_ACCEPTABLE,
                    "message.cannot_delete_a_desk_with_folders",
                    instancesUsingOldWorkflowsByDeskId.getTotalElements());
        }

        permissionService.deletePermission(deskId);
        // sometimes KC takes some time to finish this first part
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.error_thread_interrrupted");
        }
        authService.deleteDesk(deskId);
        statsService.registerAdminAction(tenant, DESK, DELETE, deskId);
    }


    @GetMapping(API_INTERNAL + "/admin/tenant/{tenantId}/desk")
    @Operation(summary = "List desks")
    @PreAuthorize(PREAUTHORIZE_TENANT_FUNCTIONAL_ADMIN_OR_MORE)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<HierarchisedDeskRepresentation> listHierarchicDesks(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                                    @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                                    @TenantResolved Tenant tenant,
                                                                    @Parameter(description = "Collapse (or expand) desks in the tree hierarchy.")
                                                                    @RequestParam(required = false, defaultValue = "false") boolean collapseAll,
                                                                    @Parameter(description = """
                                                                                             Reversed ID list.
                                                                                             * If `collapseAll` is `true`, children of given desk IDs will be retrieved.
                                                                                             * If `collapseAll` is `false`, children of given desk IDs won't be retrieved.
                                                                                             """)
                                                                    @RequestParam(required = false) List<String> reverseIdList,
                                                                    @PageableDefault(sort = SUBSTITUTE_DESK_VALUE)
                                                                    @ParameterObject Pageable pageable,
                                                                    @Parameter(description = "Searching for a specific desk name")
                                                                    @RequestParam(required = false) String searchTerm) {

        log.debug(
                "listHierarchicDesks page:{} pageSize:{} collapsedIds:{} searchTerm:{}",
                pageable.getPageNumber(),
                pageable.getPageSize(),
                reverseIdList,
                searchTerm
        );

        Page<Desk> result;
        if (isEmpty(searchTerm)) {
            result = authService.listDesks(tenant.getId(), pageable, firstNonNull(reverseIdList, emptyList()), collapseAll);
        } else {
            result = authService.searchDesks(tenant.getId(), pageable, firstNonNull(reverseIdList, emptyList()), searchTerm, collapseAll);
        }

        log.info("listHierarchicDesks result:{}", result.getContent().size());

        return new PageImpl<>(
                result.getContent().stream().map(desk -> modelMapper.map(desk, HierarchisedDeskRepresentation.class)).toList(),
                pageable,
                result.getTotalElements()
        );
    }


    @GetMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/desk")
    @Operation(summary = "List desks")
    @PreAuthorize(PREAUTHORIZE_TENANT_FUNCTIONAL_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<DeskRepresentation> listDesks(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                              @PathVariable(name = Tenant.API_PATH) String tenantId,
                                              @TenantResolved Tenant tenant,
                                              @PageableDefault(sort = SUBSTITUTE_DESK_VALUE)
                                              @ParameterObject Pageable pageable,
                                              @Parameter(description = "Searching for a specific desk name")
                                              @RequestParam(required = false) String searchTerm) {

        log.debug("listDesks page:{} pageSize:{} searchTerm:{}", pageable.getPageNumber(), pageable.getPageSize(), searchTerm);

        Page<Desk> deskResult = isEmpty(searchTerm)
                                ? authService.listDesks(tenant.getId(), pageable, emptyList(), false)
                                : authService.searchDesks(tenant.getId(), pageable, emptyList(), searchTerm, false);

        // Fix missing values

        Page<DeskRepresentation> result = deskResult.map(desk -> modelMapper.map(desk, DeskRepresentation.class));
        result.getContent().forEach(deskRepresentation -> deskRepresentation.setTenantId(tenant.getId()));

        log.debug("listDesks result:{}", result.getContent().size());
        return result;
    }


    // </editor-fold desc="Desks CRUDL">


    // <editor-fold desc="Delegations CRUDL">


    @PostMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/desk/{deskId}/delegation")
    @Operation(summary = "Create a new delegation (active or planned) from target desk")
    @PreAuthorize(PREAUTHORIZE_TENANT_FUNCTIONAL_ADMIN_OR_MORE)
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_201),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public DelegationDto createDelegationAsAdmin(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                 @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                 @TenantResolved Tenant tenant,
                                                 @Parameter(description = "Delegating desk Id")
                                                 @PathVariable String deskId,
                                                 @Parameter(description = DelegationDto.API_DOC, required = true)
                                                 @RequestBody DelegationDto delegationDto) {

        log.info("createDelegationAsAdmin delegating:{} substitute:{}", deskId, delegationDto.getSubstituteDeskId());
        log.info("                        type:{} subtype:{}", delegationDto.getTypeId(), delegationDto.getSubtypeId());

        if (!permissionService.currentUserHasAdminRightOnDesk(tenant.getId(), deskId)) {
            log.debug("User is not super admin, tenant admin nor functional admin for this desk, abort");
            throw new LocalizedStatusException(FORBIDDEN, "message.you_can_t_administrate_this_desk");
        }

        if (StringUtils.equals(deskId, delegationDto.getSubstituteDeskId())) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.redundant_delegation_target");
        }

        permissionService.addDelegation(
                tenant.getId(), delegationDto.getSubstituteDeskId(), deskId,
                delegationDto.getTypeId(), delegationDto.getSubtypeId(), delegationDto.getStart(), delegationDto.getEnd()
        );

        statsService.registerAdminAction(tenant, DESK, UPDATE, deskId);
        return delegationDto;
    }


    @GetMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/desk/{deskId}/delegation/{delegationId}")
    @Operation(summary = "Get a delegation with every information set")
    @PreAuthorize(PREAUTHORIZE_TENANT_FUNCTIONAL_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public DelegationDto getDelegationAsAdmin(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                              @PathVariable(name = Tenant.API_PATH) String tenantId,
                                              @TenantResolved Tenant tenant,
                                              @Parameter(description = "Delegating desk Id")
                                              @PathVariable String deskId,
                                              @Parameter(description = DelegationDto.API_DOC_ID_VALUE)
                                              @PathVariable String delegationId,
                                              @DelegationResolved Delegation delegation) {

        log.debug("getDelegationAsAdmin deskId:{}", deskId);

        if (!permissionService.currentUserHasAdminRightOnDesk(tenant.getId(), deskId)) {
            log.error("User is not super admin, tenant admin nor functional admin for this desk, abort");
            throw new LocalizedStatusException(FORBIDDEN, "message.you_can_t_administrate_this_desk");
        }

        // Fetch missing values

        DelegationDto delegationDto = modelMapper.map(delegation, DelegationDto.class);
        delegationDto.setDelegatingDeskId(deskId);
        deskBusinessService.updateInnerDeskValues(tenantId, singletonList(delegationDto));
        typologyBusinessService.updateInnerValues(tenantId, singletonList(delegationDto));

        log.debug("getDelegationAsAdmin delegating:{}", delegationDto);
        return delegationDto;
    }


    @DeleteMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/desk/{deskId}/delegation/{delegationId}")
    @Operation(summary = "Remove an active or planned delegation from target Desk")
    @PreAuthorize(PREAUTHORIZE_TENANT_FUNCTIONAL_ADMIN_OR_MORE)
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_204),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void deleteDelegationAsAdmin(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                        @PathVariable(name = Tenant.API_PATH) String tenantId,
                                        @TenantResolved Tenant tenant,
                                        @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                        @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                        @Parameter(description = DelegationDto.API_DOC_ID_VALUE)
                                        @PathVariable(name = DelegationDto.API_PATH) String delegationId,
                                        @DelegationResolved Delegation delegation) {

        log.info("deleteDelegationAsAdmin delegatingDeskId:{} delegationId:{}", deskId, delegationId);

        if (!permissionService.currentUserHasAdminRightOnDesk(tenant.getId(), deskId)) {
            log.error("User is not super admin, tenant admin nor functional admin for this desk, abort");
            throw new LocalizedStatusException(FORBIDDEN, "message.you_can_t_administrate_this_desk");
        }

        permissionService.deleteDelegation(
                delegationId,
                tenant.getId(),
                delegation.getSubstituteDesk().getId(),
                deskId,
                Optional.ofNullable(delegation.getType()).map(Type::getId).orElse(null),
                Optional.ofNullable(delegation.getSubtype()).map(Subtype::getId).orElse(null),
                delegation.getStart(),
                delegation.getEnd()
        );

        statsService.registerAdminAction(tenant, DESK, UPDATE, deskId);
    }


    @GetMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/desk/{deskId}/delegation")
    @Operation(
            summary = "List delegations (active and planned) for given delegating desk",
            description = DelegationSortBy.Constants.API_DOC_SORT_BY_VALUES
    )
    @PreAuthorize(PREAUTHORIZE_TENANT_FUNCTIONAL_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<DelegationDto> listDeskDelegationsAsAdmin(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                          @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                          @TenantResolved Tenant tenant,
                                                          @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                                          @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                                          @DeskResolved Desk desk,
                                                          @PageableDefault(sort = SUBSTITUTE_DESK_VALUE)
                                                          @ParameterObject Pageable pageable) {

        log.debug("listDeskDelegationsAsAdmin tenantId:{} deskId:{}", tenantId, desk.getId());

        if (!permissionService.currentUserHasAdminRightOnDesk(tenant.getId(), desk.getId())) {
            log.error("User is not super admin, tenant admin nor functional admin for this desk, abort");
            throw new LocalizedStatusException(FORBIDDEN, "message.you_can_t_administrate_this_desk");
        }

        List<DelegationDto> delegationsDtoList = permissionService.getDelegations(tenant.getId(), desk.getId()).stream()
                .map(delegation -> modelMapper.map(delegation, DelegationDto.class))
                .collect(toMutableList());;

        // Fetch missing values

        delegationsDtoList.forEach(dto -> dto.setDelegatingDeskId(desk.getId()));
        deskBusinessService.updateInnerDeskValues(tenant.getId(), delegationsDtoList);
        typologyBusinessService.updateInnerValues(tenant.getId(), delegationsDtoList);

        // Manual sort

        Pageable innerPageable = RequestUtils.convertSortedPageable(pageable, DelegationSortBy.class, Enum::toString);
        boolean asc = RequestUtils.isFirstOrderAsc(innerPageable);
        DelegationSortBy sortBy = RequestUtils.getFirstOderSort(innerPageable, DelegationSortBy.class, SUBSTITUTE_DESK);
        long page = RequestUtils.getPageNumber(innerPageable);
        long pageSize = RequestUtils.getPageSize(innerPageable);
        Comparator<DelegationDto> comparator = generateDelegationDtoComparator(sortBy, asc);

        PageImpl<DelegationDto> result = new PageImpl<>(
                delegationsDtoList.stream()
                        .sorted(comparator)
                        .skip(page * pageSize)
                        .limit(pageSize)
                        .toList(),
                innerPageable,
                delegationsDtoList.size()
        );

        log.debug("listDeskDelegationsAsAdmin result:{}", result);
        return result;
    }


    // </editor-fold desc="Delegations CRUDL">


}
