/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller.admin;

import coop.libriciel.ipcore.business.auth.DeskBusinessService;
import coop.libriciel.ipcore.business.auth.UserBusinessService;
import coop.libriciel.ipcore.business.typology.TypologyBusinessService;
import coop.libriciel.ipcore.business.workflow.FolderBusinessService;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.database.Type;
import coop.libriciel.ipcore.model.database.userPreferences.FolderFilter;
import coop.libriciel.ipcore.model.workflow.*;
import coop.libriciel.ipcore.model.workflow.requests.ColumnedTaskListRequest;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.database.DatabaseServiceInterface;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.services.resolvers.FolderResolver.FolderResolved;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.KeycloakSecurityUtils;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.PaginatedList;
import coop.libriciel.ipcore.utils.RequestUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.*;

import static coop.libriciel.ipcore.IpCoreApplication.*;
import static coop.libriciel.ipcore.model.database.userPreferences.FolderFilter.*;
import static coop.libriciel.ipcore.model.workflow.Action.ARCHIVE;
import static coop.libriciel.ipcore.model.workflow.Action.DELETE;
import static coop.libriciel.ipcore.model.workflow.FolderSortBy.CREATION_DATE;
import static coop.libriciel.ipcore.model.workflow.FolderSortBy.Constants.CREATION_DATE_VALUE;
import static coop.libriciel.ipcore.model.workflow.State.*;
import static coop.libriciel.ipcore.services.resolvers.FolderResolver.FolderResolved.Permission.IGNORE;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.CollectionUtils.computeTotal;
import static coop.libriciel.ipcore.utils.PaginatedList.*;
import static coop.libriciel.ipcore.utils.RequestUtils.PREAUTHORIZE_TENANT_FUNCTIONAL_ADMIN_OR_MORE;
import static coop.libriciel.ipcore.utils.RequestUtils.convertSortedPageable;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.springframework.http.HttpStatus.*;


@Log4j2
@RestController
@RequestMapping
@Tag(name = "admin-folder", description = "Reserved operations on folder")
public class AdminFolderController {


    // <editor-fold desc="Beans">


    private final AuthServiceInterface authService;
    private final DatabaseServiceInterface databaseService;
    private final DeskBusinessService deskBusinessService;
    private final FolderBusinessService folderBusinessService;
    private final ModelMapper modelMapper;
    private final PermissionServiceInterface permissionService;
    private final StatsServiceInterface statsService;
    private final TypologyBusinessService typologyBusinessService;
    private final UserBusinessService userBusinessService;
    private final WorkflowServiceInterface workflowService;


    @Autowired
    public AdminFolderController(AuthServiceInterface authService,
                                 DatabaseServiceInterface databaseService,
                                 DeskBusinessService deskBusinessService,
                                 FolderBusinessService folderBusinessService,
                                 ModelMapper modelMapper,
                                 PermissionServiceInterface permissionService,
                                 StatsServiceInterface statsService,
                                 TypologyBusinessService typologyBusinessService,
                                 UserBusinessService userBusinessService,
                                 WorkflowServiceInterface workflowService) {
        this.authService = authService;
        this.databaseService = databaseService;
        this.deskBusinessService = deskBusinessService;
        this.folderBusinessService = folderBusinessService;
        this.modelMapper = modelMapper;
        this.permissionService = permissionService;
        this.statsService = statsService;
        this.typologyBusinessService = typologyBusinessService;
        this.userBusinessService = userBusinessService;
        this.workflowService = workflowService;
    }


    // </editor-fold desc="Beans">


    @GetMapping(API_INTERNAL + "/admin/tenant/{tenantId}/folder/legacy-list")
    @Operation(summary = "Legacy method for Listing folders from <= 5.1 versions")
    @PreAuthorize(PREAUTHORIZE_TENANT_FUNCTIONAL_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<FolderDto> listFoldersAsAdminLegacy(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                    @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                    @TenantResolved Tenant tenant,
                                                    @PageableDefault(sort = CREATION_DATE_VALUE)
                                                    @ParameterObject Pageable pageable,
                                                    // FILTER_PARAMS START
                                                    @Parameter(description = API_DOC_TYPE_NAME)
                                                    @RequestParam(required = false) String typeId,
                                                    @Parameter(description = API_DOC_SUBTYPE_NAME)
                                                    @RequestParam(required = false) String subtypeId,
                                                    @Parameter(description = API_DOC_LEGACY_ID)
                                                    @RequestParam(required = false) String legacyId,
                                                    // FILTER_PARAMS END
                                                    @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                                    @RequestParam(required = false) String deskId,
                                                    @Parameter(description = "Searching for a specific folder name")
                                                    @RequestParam(required = false) String searchTerm,
                                                    @Parameter(description = "Filtering folders started before the given threshold")
                                                    @RequestParam(required = false) Long emitBeforeTime,
                                                    @Parameter(description = "Filtering folders that are still since given threshold")
                                                    @RequestParam(required = false) Long stillSinceTime,
                                                    @Parameter(description = "Filtering folders with given state")
                                                    @RequestParam(required = false) State state) {

        log.debug("listFolders tenant:{} state:{}", tenant.getId(), state);

        // Parse the request

        Pageable innerPageable = convertSortedPageable(pageable, FolderSortBy.class, Enum::toString);
        FolderSortBy sortBy = RequestUtils.getFirstOderSort(innerPageable, FolderSortBy.class, CREATION_DATE);
        boolean asc = RequestUtils.isFirstOrderAsc(innerPageable);
        int pageSize = RequestUtils.getPageSize(innerPageable);
        int page = (int) RequestUtils.getPageNumber(innerPageable);

        if (innerPageable.getPageSize() > MAX_PAGE_SIZE) {
            log.error("Request specify a pageSize of {}, which is superior to the max value: {}", pageSize, MAX_PAGE_SIZE);
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.exceeded_max_page_size", MAX_PAGE_SIZE);
        }

        Page<? extends Folder> result;
        FolderFilter filter = new FolderFilter(
                Optional.ofNullable(typeId).map(Type::new).orElse(null),
                Optional.ofNullable(subtypeId).map(Subtype::new).orElse(null),
                searchTerm,
                legacyId
        );

        if (KeycloakSecurityUtils.isSuperAdmin() || KeycloakSecurityUtils.currentUserIsTenantAdmin(tenantId)) {
            result = workflowService.listFolders(tenant.getId(), innerPageable, filter, deskId, emitBeforeTime, stillSinceTime, state);
        } else {
            // We rely on pre-authorize to assert that the user is a functional admin
            String userId = KeycloakSecurityUtils.getCurrentSessionUserId();
            List<String> administeredDeskIds =
                    permissionService.getAdministeredDesks(userId, tenantId)
                            .stream()
                            .map(DeskRepresentation::getId)
                            .toList();
            List<String> finalTargetDeskIds = administeredDeskIds;

            if (!StringUtils.isEmpty(deskId)) {
                if (administeredDeskIds.stream().noneMatch(dId -> StringUtils.equals(dId, deskId))) {
                    throw new LocalizedStatusException(FORBIDDEN, "message.you_can_t_view_this_desk");
                }

                finalTargetDeskIds = Collections.singletonList(deskId);
            }

            PaginatedList<? extends Folder> tempResult = workflowService
                    .listFoldersForDesks(tenantId, page, pageSize, sortBy, filter, asc, finalTargetDeskIds, emitBeforeTime, stillSinceTime, state);
            result = RequestUtils.toPage(tempResult, sortBy, asc);
        }

        // Fetch missing infos

        typologyBusinessService.updateTypology(tenant.getId(), result.getContent(), false);
        authService.updateFolderReferencedDeskNames(result.getContent());

        log.debug("listFolders result:{}", result.getContent().size());
        return result.map(f -> modelMapper.map(f, FolderDto.class));
    }


    @PostMapping(API_INTERNAL + "/admin/tenant/{tenantId}/folder")
    @Operation(summary = "List folders")
    @PreAuthorize(PREAUTHORIZE_TENANT_FUNCTIONAL_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<FolderDto> listFoldersAsAdmin(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                              @PathVariable(name = Tenant.API_PATH) String tenantId,
                                              @TenantResolved Tenant tenant,
                                              @Parameter(name = "body", description = "Every field is mandatory")
                                              @RequestBody ColumnedTaskListRequest request,
                                              @PageableDefault(sort = CREATION_DATE_VALUE)
                                              @ParameterObject Pageable pageable) {

        log.debug("listFolders tenant:{} state:{}", tenant.getId(), request.state());

        // Parse the request

        Pageable innerPageable = convertSortedPageable(pageable, FolderSortBy.class, Enum::toString);
        int pageSize = RequestUtils.getPageSize(innerPageable);
        int page = (int) RequestUtils.getPageNumber(innerPageable);
        if (innerPageable.getPageSize() > MAX_PAGE_SIZE) {
            log.error("Request specify a pageSize of {}, which is superior to the max value: {}", pageSize, MAX_PAGE_SIZE);
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.exceeded_max_page_size", MAX_PAGE_SIZE);
        }


        checkForDeskAdministrationRights(tenantId, request.fromDeskIds());


        // Fetch missing infos


        List<Folder> result = databaseService.getColumnedFolderList(
                tenantId,
                request.fromDeskIds(),
                emptyList(),
                KeycloakSecurityUtils.getCurrentSessionUserId(),
                request,
                emptyList(),
                pageable
        );


        long totalCount = computeTotal(
                result,
                page,
                pageSize,
                () -> databaseService.getColumnedFolderCount(
                        tenantId,
                        request.fromDeskIds(),
                        emptyList(),
                        KeycloakSecurityUtils.getCurrentSessionUserId(),
                        request,
                        emptyList()
                )
        );

        // Fetch missing values

        if (request.state() == DELEGATED) {
            result.stream()
                    .map(Folder::getStepList)
                    .flatMap(Collection::stream)
                    .forEach(task -> task.setState(CURRENT));
        }

        deskBusinessService.updateFoldersInnerDeskValues(result);
        typologyBusinessService.updateTypology(tenant.getId(), result, false);
        userBusinessService.updateInnerUserValues(result);


        log.debug("listFolders result:{}", result.size());
        return new PageImpl<>(result, innerPageable, totalCount)
                .map(folder -> modelMapper.map(folder, FolderDto.class));
    }


    @DeleteMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/folder/{folderId}")
    @Operation(summary = "Delete folder")
    @PreAuthorize(PREAUTHORIZE_TENANT_FUNCTIONAL_ADMIN_OR_MORE)
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_204),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void deleteFolderAsAdmin(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                    @PathVariable(name = Tenant.API_PATH) String tenantId,
                                    @TenantResolved Tenant tenant,
                                    @Parameter(description = Folder.API_DOC_ID_VALUE)
                                    @PathVariable(name = Folder.API_PATH) String folderId,
                                    @FolderResolved(permission = IGNORE, withHistory = false) Folder folder) {

        log.info("deleteFolderAsAdmin id:{}", folderId);

        List<String> administeredDeskIds = permissionService
                .getAdministeredDesks(KeycloakSecurityUtils.getCurrentSessionUserId(), tenantId)
                .stream()
                .map(DeskRepresentation::getId)
                .collect(toList());

        checkForDeskAdministrationRights(tenantId, administeredDeskIds);

        folderBusinessService.deleteFolder(tenantId, folder, false);

        // Building stats result

        Long timeToCompleteInHours = folder.getStepList()
                .stream()
                .filter(t -> (t.getAction() == ARCHIVE) || (t.getAction() == DELETE))
                .findFirst()
                .map(statsService::computeTimeToCompleteInHours)
                .orElse(null);

        statsService.registerFolderAction(tenant, DELETE, folder, folder.getFinalDesk(), timeToCompleteInHours);
    }


    private void checkForDeskAdministrationRights(String tenantId, List<String> deskIds) {
        boolean isCurrentUserAdmin = KeycloakSecurityUtils.isSuperAdmin() || KeycloakSecurityUtils.currentUserIsTenantAdmin(tenantId);

        if (!isCurrentUserAdmin) {
            // We rely on pre-authorize to assert that the user is a functional admin
            String userId = KeycloakSecurityUtils.getCurrentSessionUserId();
            Set<String> administeredDeskIds = permissionService
                    .getAdministeredDesks(userId, tenantId)
                    .stream()
                    .map(DeskRepresentation::getId)
                    .collect(toSet());

            List<String> currentlyInvolvedDeskIds = new ArrayList<>(deskIds);

            // If nothing is remaining in the intersection of the two,
            // the current user is not a functional admin of any involved desk
            currentlyInvolvedDeskIds.retainAll(administeredDeskIds);
            if (currentlyInvolvedDeskIds.isEmpty()) {
                throw new LocalizedStatusException(FORBIDDEN, "message.you_can_t_administrate_this_desk");
            }
        }
    }
}
