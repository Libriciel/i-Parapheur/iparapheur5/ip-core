/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller.admin;


import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.database.Metadata;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.pdfstamp.*;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.database.LayerRepository;
import coop.libriciel.ipcore.services.database.MetadataRepository;
import coop.libriciel.ipcore.services.database.StampRepository;
import coop.libriciel.ipcore.services.pdfstamp.PdfStampServiceInterface;
import coop.libriciel.ipcore.services.resolvers.LayerResolver.LayerResolved;
import coop.libriciel.ipcore.services.resolvers.StampResolver.StampResolved;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.RequestUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static coop.libriciel.ipcore.IpCoreApplication.API_INTERNAL;
import static coop.libriciel.ipcore.IpCoreApplication.API_PROVISIONING_V1;
import static coop.libriciel.ipcore.model.database.InternalMetadata.Constants.*;
import static coop.libriciel.ipcore.model.pdfstamp.StampType.IMAGE;
import static coop.libriciel.ipcore.model.pdfstamp.StampType.METADATA;
import static coop.libriciel.ipcore.model.stats.StatsCategory.LAYER;
import static coop.libriciel.ipcore.model.workflow.Action.*;
import static coop.libriciel.ipcore.services.content.ContentServiceInterface.FILE_TRANSFER_BUFFER_SIZE;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.RequestUtils.PREAUTHORIZE_TENANT_ADMIN_OR_MORE;
import static coop.libriciel.ipcore.utils.RequestUtils.convertSortedPageable;
import static coop.libriciel.ipcore.utils.TextUtils.MESSAGE_BUNDLE;
import static jakarta.ws.rs.core.MediaType.MULTIPART_FORM_DATA;
import static java.util.Comparator.*;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;
import static org.springframework.core.io.buffer.DataBufferUtils.readInputStream;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_PDF_VALUE;


@Log4j2
@RestController
@Tag(name = "admin-layer", description = "Reserved operations on layers")
public class AdminLayerController {


    public static final int MAX_STAMPS_PER_LAYER_COUNT = 250;

    private static final String A4_3_PAGES_EXAMPLE_RESOURCE_NAME = "A4_3_pages.pdf";


    // <editor-fold desc="Beans">


    private final ModelMapper modelMapper;
    private final StatsServiceInterface statsService;
    private final PdfStampServiceInterface pdfStampService;
    private final ContentServiceInterface contentService;
    private final PlatformTransactionManager platformTransactionManager;
    private final LayerRepository layerRepository;
    private final MetadataRepository metadataRepository;
    private final StampRepository stampRepository;


    @Autowired
    public AdminLayerController(ModelMapper modelMapper,
                                StatsServiceInterface statsService,
                                LayerRepository layerRepository,
                                StampRepository stampRepository,
                                MetadataRepository metadataRepository,
                                PdfStampServiceInterface pdfStampService,
                                ContentServiceInterface contentService,
                                PlatformTransactionManager platformTransactionManager) {
        this.modelMapper = modelMapper;
        this.statsService = statsService;
        this.contentService = contentService;
        this.layerRepository = layerRepository;
        this.stampRepository = stampRepository;
        this.pdfStampService = pdfStampService;
        this.metadataRepository = metadataRepository;
        this.platformTransactionManager = platformTransactionManager;
    }


    // </editor-fold desc="Beans">


    // <editor-fold desc="CRUDL layers">


    @PostMapping(API_INTERNAL + "/admin/tenant/{tenantId}/layer")
    @Operation(summary = "Create a layer")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_201),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_507, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public LayerDto createLayer(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                @PathVariable(name = Tenant.API_PATH) String tenantId,
                                @TenantResolved Tenant tenant,
                                @Parameter(description = "body", required = true)
                                @RequestBody @Valid LayerDto request) {

        log.info("createLayer tenantId:{}", tenant.getId());

        // Integrity checks

        long layersCount = layerRepository.countAllByTenant_Id(tenant.getId());
        int limit = tenant.getLayerLimit() != null ? tenant.getLayerLimit() : MAX_LAYERS_COUNT;
        if (layersCount >= limit) {
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE,
                    "message.already_n_layers_maximum_reached",
                    layersCount);
        }

        if (CollectionUtils.size(request.getStampList()) >= MAX_STAMPS_PER_LAYER_COUNT) {
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE, "message.already_n_stamps_maximum_reached", MAX_STAMPS_PER_LAYER_COUNT);
        }

        // Convert to entity

        Layer layer = modelMapper.map(request, Layer.class);
        layer.setId(UUID.randomUUID().toString());
        layer.setTenant(tenant);

        // Computing stamp diff

        Layer finalLayer = layer;
        List<Stamp> stamps = request.getStampList().stream()
                .map(stampDto -> modelMapper.map(stampDto, Stamp.class))
                .peek(stamp -> stamp.setParentLayer(finalLayer))
                .peek(stamp -> {
                    if (StringUtils.isEmpty(stamp.getId())) {
                        stamp.setId(UUID.randomUUID().toString());
                    }
                })
                .collect(Collectors.toList());

        IntStream.range(0, stamps.size()).forEach(i -> stamps.get(i).setIndex(i));

        // Actual registering

        try {
            layer = layerRepository.save(layer);
        } catch (DataIntegrityViolationException exception) {
            throw new LocalizedStatusException(CONFLICT, "message.this_layer_name_already_exists", exception);
        }

        stampRepository.saveAll(stamps);

        statsService.registerAdminAction(tenant, LAYER, CREATE, layer.getId());
        return modelMapper.map(layer, LayerDto.class);
    }


    @GetMapping(API_INTERNAL + "/admin/tenant/{tenantId}/layer/{layerId}")
    @Operation(summary = "Get a layer with every information set")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public LayerDto getLayer(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                             @PathVariable(name = Tenant.API_PATH) String tenantId,
                             @Parameter(description = LayerDto.API_DOC_ID_VALUE)
                             @PathVariable(name = LayerDto.API_PATH) String layerId,
                             @LayerResolved Layer layer) {
        Tenant tenant = layer.getTenant();
        log.debug("getLayer tenantId:{} layerId:{}", tenant.getId(), layer.getId());
        layer.getStampList().sort(comparing(Stamp::getIndex, nullsLast(naturalOrder())));
        return modelMapper.map(layer, LayerDto.class);
    }


    @PutMapping(API_INTERNAL + "/admin/tenant/{tenantId}/layer/{layerId}")
    @Operation(summary = "Edit a layer")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_201),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public LayerDto updateLayer(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                @PathVariable(name = Tenant.API_PATH) String tenantId,
                                @Parameter(description = LayerDto.API_DOC_ID_VALUE)
                                @PathVariable(name = LayerDto.API_PATH) String layerId,
                                @LayerResolved Layer existingLayer,
                                @Parameter(description = "body", required = true)
                                @RequestBody @Valid LayerDto request) {

        Tenant tenant = existingLayer.getTenant();
        log.info("updateLayer tenantId:{} layerId:{}", tenant.getId(), existingLayer.getId());

        // Integrity check

        if (request.getStampList().size() >= MAX_STAMPS_PER_LAYER_COUNT) {
            log.info("Aborting update of layer {}, too many stamps passed.", request.getName());
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE, "message.already_n_stamps_maximum_reached", MAX_STAMPS_PER_LAYER_COUNT);
        }

        Set<String> existingStampIds = existingLayer.getStampList().stream()
                .map(Stamp::getId)
                .collect(toSet());
        long remainingStamps = request.getStampList().stream()
                .map(StampDto::getId)
                .filter(existingStampIds::contains)
                .count();
        long newStamps = request.getStampList().stream()
                .map(StampDto::getId)
                .filter(StringUtils::isEmpty)
                .count();

        if ((remainingStamps + newStamps) >= MAX_STAMPS_PER_LAYER_COUNT) {
            log.info("Aborting update of layer {}, max stamp number reached.", request.getName());
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE, "message.already_n_stamps_maximum_reached", MAX_STAMPS_PER_LAYER_COUNT);
        }

        if (request.getStampList().stream()
                .filter(stamp -> stamp.getType() == IMAGE)
                .anyMatch(stamp -> StringUtils.isEmpty(stamp.getId()))) {
            log.error("Error trying to save updated layer {}, an image stamp ios missing", request.getName());
            // If the id is not set, that means that the image was not uploaded yet,
            // and Alfresco did not return its id
            throw new LocalizedStatusException(BAD_REQUEST, "message.stamp_image_cannot_be_empty");
        }

        // Convert to entity

        Layer updatedLayer = modelMapper.map(request, Layer.class);
        updatedLayer.setId(existingLayer.getId());
        updatedLayer.setTenant(existingLayer.getTenant());
        updatedLayer.setStampList(existingLayer.getStampList());
        updatedLayer.setSubtypeLayerList(existingLayer.getSubtypeLayerList());

        Layer finalUpdatedLayer = updatedLayer;
        List<Stamp> stamps = request.getStampList()
                .stream()
                .map(stampDto -> modelMapper.map(stampDto, Stamp.class))
                .peek(stamp -> stamp.setParentLayer(finalUpdatedLayer))
                .peek(stamp -> {
                    if (StringUtils.isEmpty(stamp.getId())) {
                        stamp.setId(UUID.randomUUID().toString());
                    }
                })
                .toList();

        IntStream.range(0, stamps.size()).forEach(i -> stamps.get(i).setIndex(i));

        // Remove deleted images

        Set<String> requestStampIds = request.getStampList().stream()
                .map(StampDto::getId)
                .collect(toSet());

        Set<Stamp> removedStamps = existingLayer.getStampList().stream()
                .filter(stamp -> !requestStampIds.contains(stamp.getId()))
                .collect(toSet());

        removedStamps.stream()
                .filter(stamp -> stamp.getType() == IMAGE)
                .map(Stamp::getId)
                .peek(stampId -> log.debug("Deleting stamp image " + stampId))
                .forEach(stampId -> deleteFileStamp(tenant.getId(), existingLayer.getId(), existingLayer, stampId));

        // Actual save

        try {
            updatedLayer = layerRepository.save(updatedLayer);
        } catch (DataIntegrityViolationException exception) {
            log.error("Error trying to save updated layer {}", updatedLayer.getName());
            log.debug("Error details : ", exception);
            throw new LocalizedStatusException(CONFLICT, "message.this_layer_name_already_exists", exception);
        }

        TransactionTemplate transactionTemplate = new TransactionTemplate(platformTransactionManager);

        transactionTemplate.execute((TransactionCallback<Void>) transactionStatus -> {
            stampRepository.saveAll(stamps);
            stampRepository.deleteAllByIdIn(removedStamps.stream().map(Stamp::getId).toList());
            return null;
        });


        statsService.registerAdminAction(tenant, LAYER, UPDATE, updatedLayer.getId());
        return modelMapper.map(updatedLayer, LayerDto.class);
    }


    @DeleteMapping(API_INTERNAL + "/admin/tenant/{tenantId}/layer/{layerId}")
    @Operation(summary = "Delete layer")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_204),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void deleteLayer(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                            @PathVariable(name = Tenant.API_PATH) String tenantId,
                            @Parameter(description = LayerDto.API_DOC_ID_VALUE)
                            @PathVariable(name = LayerDto.API_PATH) String layerId,
                            @LayerResolved Layer layer) {

        Tenant tenant = layer.getTenant();
        log.info("deleteLayer tenantId:{} layerId:{}", tenant.getId(), layer.getId());

        // TODO : Check if it is used

        // Delete stored images

        layer.getStampList().stream()
                .filter(stamp -> stamp.getType() == IMAGE)
                .map(Stamp::getId)
                .peek(stampId -> log.debug("Deleting stamp image " + stampId))
                .forEach(contentService::deleteLayerImage);

        // Actual layer deletion

        layerRepository.deleteById(layer.getId());
        statsService.registerAdminAction(tenant, LAYER, DELETE, layer.getId());
    }


    @GetMapping(API_INTERNAL + "/admin/tenant/{tenantId}/layer")
    @Operation(summary = "List layers", description = LayerSortBy.Constants.API_DOC_SORT_BY_VALUES)
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<LayerRepresentation> listLayers(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                @TenantResolved Tenant tenant,
                                                @PageableDefault(sort = LayerSortBy.Constants.NAME_VALUE)
                                                @ParameterObject Pageable pageable) {

        log.debug("listLayers page:{} pageSize:{}", pageable.getPageNumber(), pageable.getPageSize());

        Pageable innerPageable = convertSortedPageable(pageable, LayerSortBy.class, LayerSortBy::getColumnName);

        Page<LayerRepresentation> result = layerRepository.findAllByTenant_Id(tenant.getId(), innerPageable)
                .map(LayerRepresentation::new);

        log.debug("listLayers result size: {}", result.getSize());
        return result;
    }


    // </editor-fold desc="CRUDL layers">


    // <editor-fold desc="CRUD stamps">


    @PostMapping(value = API_INTERNAL + "/admin/tenant/{tenantId}/layer/{layerId}/stamp", consumes = MULTIPART_FORM_DATA)
    @Operation(summary = "Create a file stamp")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_201),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_507, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public StampDto createFileStamp(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                    @PathVariable(name = Tenant.API_PATH) String tenantId,
                                    @Parameter(description = LayerDto.API_DOC_ID_VALUE)
                                    @PathVariable(name = LayerDto.API_PATH) String layerId,
                                    @LayerResolved Layer layer,
                                    @Parameter(required = true)
                                    @RequestPart @Valid StampDto stamp,
                                    @Parameter(required = true, description = "file")
                                    @RequestPart MultipartFile file) {

        Tenant tenant = layer.getTenant();
        log.info("createFileStamp tenantId:{} layerId:{}", tenant.getId(), layer.getId());

        // Integrity check

        if (layer.getStampList().size() >= MAX_STAMPS_PER_LAYER_COUNT) {
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE, "message.already_n_stamps_maximum_reached", MAX_STAMPS_PER_LAYER_COUNT);
        }

        if (stamp.getType() != IMAGE) {
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE, "message.this_method_only_applies_to_image_stamps");
        }

        // Storing values

        Stamp stampEntity = modelMapper.map(stamp, Stamp.class);
        stampEntity.setParentLayer(layer);

        String imageNodeId;
        try (InputStream body = file.getInputStream()) {
            DocumentBuffer imageBuffer = new DocumentBuffer();
            imageBuffer.setName(file.getOriginalFilename());
            imageBuffer.setContentFlux(readInputStream(() -> body, new DefaultDataBufferFactory(), FILE_TRANSFER_BUFFER_SIZE));
            imageNodeId = contentService.createLayerImage(tenant, layer, imageBuffer);
        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_reading_file");
        }

        stampEntity.setValue(file.getOriginalFilename());
        stampEntity.setId(imageNodeId);
        stampEntity = stampRepository.save(stampEntity);

        // Sending back response

        statsService.registerAdminAction(tenant, LAYER, UPDATE, layer.getId());
        return modelMapper.map(stampEntity, StampDto.class);
    }


    @GetMapping(value = API_INTERNAL + "/admin/tenant/{tenantId}/layer/{layerId}/stamp/{stampId}/image")
    @Operation(summary = "get the image of a file stamp")
    @PreAuthorize(PREAUTHORIZE_TENANT_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_406, content = @Content(schema = @Schema(implementation = ErrorResponse.class)))
    })
    public void getStampImage(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                              @PathVariable(name = Tenant.API_PATH) String tenantId,
                              @Parameter(description = LayerDto.API_DOC_ID_VALUE)
                              @PathVariable(name = LayerDto.API_PATH) String layerId,
                              @LayerResolved Layer layer,
                              @Parameter(description = StampDto.API_DOC_ID_VALUE)
                              @PathVariable(name = StampDto.API_PATH) String stampId,
                              @StampResolved Stamp stamp,
                              HttpServletResponse response) {

        Tenant tenant = layer.getTenant();
        log.info("getStampImage tenantId:{} layerId:{} stampId:{}", tenant.getId(), layer.getId(), stamp.getId());

        if (stamp.getType() != IMAGE) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.this_method_only_applies_to_image_stamps");
        }

        DocumentBuffer documentBuffer = contentService.retrieveContent(stamp.getId());

        RequestUtils.writeBufferToResponse(documentBuffer, response);
    }


    @DeleteMapping(API_INTERNAL + "/admin/tenant/{tenantId}/layer/{layerId}/stamp/{stampId}")
    @Operation(summary = "Delete a stored image stamp")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_204),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void deleteFileStamp(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                @PathVariable(name = Tenant.API_PATH) String tenantId,
                                @Parameter(description = LayerDto.API_DOC_ID_VALUE)
                                @PathVariable(name = LayerDto.API_PATH) String layerId,
                                @LayerResolved Layer existingLayer,
                                @Parameter(description = StampDto.API_DOC_ID_VALUE)
                                @PathVariable(name = StampDto.API_PATH) String stampId) {

        Tenant tenant = existingLayer.getTenant();
        log.info("deleteFileStamp tenantId:{} layerId:{} stampId:{}", tenant.getId(), existingLayer.getId(), stampId);

        // Integrity check

        Stamp targetStamp = stampRepository.findByIdAndParentLayer_Id(stampId, existingLayer.getId())
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_stamp_id"));

        if (targetStamp.getType() != IMAGE) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.this_method_only_applies_to_image_stamps");
        }

        // Actual delete

        try {
            contentService.deleteLayerImage(targetStamp.getId());
        } catch (LocalizedStatusException e) {
            // Some stamp can mistakenly be created without content.
            // In this case, the stamp id is not a node id, and the deletion will fail. We have to ignore this specific error.
            // This only happened for layers saved on versions < 5.0.9.
            // TODO : This is very specific to a client. This ugly catch should be removed in... 5.2.
            if (StringUtils.contains(e.getMessage(), "404 Not Found from DELETE")) {
                log.warn("Content-less stamp found, skipping the error...", e);
            } else {
                throw e;
            }
        }
    }


    // </editor-fold desc="CRUD stamps">


    @PostMapping(API_INTERNAL + "/admin/tenant/{tenantId}/layer/example-pdf")
    @Operation(summary = "Test the given layer on a blank PDF")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200, content = @Content(mediaType = APPLICATION_PDF_VALUE)),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void getLayerExamplePdf(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                   @PathVariable(name = Tenant.API_PATH) String tenantId,
                                   @RequestBody @Valid LayerDto layerDto,
                                   HttpServletResponse response) {

        log.info("getLayerExamplePdf");
        ResourceBundle messageBundle = ResourceBundle.getBundle(MESSAGE_BUNDLE, Locale.getDefault());
        Layer layer = modelMapper.map(layerDto, Layer.class);

        // Metadata fine-tuning

        Set<String> metadataIds = layer.getStampList().stream()
                .filter(s -> s.getType() == METADATA)
                .map(Stamp::getValue)
                .collect(toSet());

        Map<String, Metadata> fullMetadataMap = metadataRepository
                .findAllByIdAndTenant_IdOrTenantLess(metadataIds, tenantId)
                .stream()
                .collect(toMap(Metadata::getId, m -> m));

        layer.getStampList().stream()
                .filter(stamp -> stamp.getType() == METADATA)
                .forEach(stamp -> {

                    // TODO: externalize those values in the resources.
                    //  Maybe put the resource "message.key" in the enum itself
                    Metadata currentMetadata = fullMetadataMap.get(stamp.getValue());
                    String computedValue = switch (currentMetadata.getKey()) {
                        case FOLDER_NAME_KEY -> "Nom du dossier";
                        case FOLDER_TYPE_KEY -> "Nom du type";
                        case FOLDER_SUBTYPE_KEY -> "Nom du Sous-type";
                        case LAST_VISA_DESK_KEY -> "Bureau de la dernière validation";
                        case LAST_VISA_USER_KEY -> "Dernier validateur";
                        case LAST_VISA_DATE_KEY -> "Date de la dernière validation";
                        case LAST_SIGNATURE_DESK_KEY -> "Bureau de la dernière signature";
                        case LAST_SIGNATURE_USER_KEY -> "Dernier signataire";
                        case LAST_SIGNATURE_DATE_KEY -> "Date de la dernière signature";
                        case LAST_SIGNATURE_DELEGATED_BY_KEY -> "Bureau absent en cas de suppléance";
                        case LAST_SIGNATURE_HASH_KEY -> "Hash SHA256 de la dernière signature";
                        default -> fullMetadataMap.get(stamp.getValue()).getName();
                    };

                    String placeholder = messageBundle.getString("message.metadata_placeholder");
                    stamp.setComputedValue(MessageFormat.format(placeholder, computedValue));
                });

        // Build response

        try (InputStream blankExamplePdfInputStream = getClass().getClassLoader().getResourceAsStream("templates/" + A4_3_PAGES_EXAMPLE_RESOURCE_NAME)) {

            if (blankExamplePdfInputStream == null) {
                throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.error_loading_example_pdf");
            }

            DocumentBuffer documentBuffer = new DocumentBuffer();
            documentBuffer.setName(A4_3_PAGES_EXAMPLE_RESOURCE_NAME);
            documentBuffer.setContentFlux(readInputStream(() -> blankExamplePdfInputStream, new DefaultDataBufferFactory(), FILE_TRANSFER_BUFFER_SIZE));
            DocumentBuffer layeredExamplePdfDocument = pdfStampService.createStamp(documentBuffer, layer.getStampList(), contentService::retrieveContent);

            try (InputStream layeredExamplePdfInputStream = RequestUtils.bufferToInputStream(layeredExamplePdfDocument);
                 OutputStream responseOutputStream = response.getOutputStream()) {

                response.setContentType(APPLICATION_PDF_VALUE);
                response.setHeader(CONTENT_DISPOSITION, String.format("attachment;filename=%s", A4_3_PAGES_EXAMPLE_RESOURCE_NAME));
                IOUtils.copy(layeredExamplePdfInputStream, responseOutputStream);
            } catch (IOException e) {
                log.error("Cannot reach PDF-Stamp service", e);
                throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.cannot_reach_pdf_stamp_service");
            }

        } catch (IOException e) {
            log.error("Cannot read the blank PDF", e);
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_reading_example_pdf");
        }
    }


}
