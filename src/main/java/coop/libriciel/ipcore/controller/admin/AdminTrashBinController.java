/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller.admin;

import coop.libriciel.ipcore.business.auth.DeskBusinessService;
import coop.libriciel.ipcore.business.auth.UserBusinessService;
import coop.libriciel.ipcore.business.typology.TypologyBusinessService;
import coop.libriciel.ipcore.business.workflow.FolderBusinessService;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.FolderListableDto;
import coop.libriciel.ipcore.model.workflow.FolderRepresentation;
import coop.libriciel.ipcore.model.workflow.FolderSortBy;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.resolvers.CurrentUserResolver.CurrentUserResolved;
import coop.libriciel.ipcore.services.resolvers.FolderResolver.FolderResolved;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.PaginatedList;
import coop.libriciel.ipcore.utils.XmlUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;
import org.modelmapper.ModelMapper;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Optional;

import static coop.libriciel.ipcore.IpCoreApplication.API_INTERNAL;
import static coop.libriciel.ipcore.IpCoreApplication.API_STANDARD_V1;
import static coop.libriciel.ipcore.model.workflow.Action.DELETE;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static java.util.Collections.singletonList;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM_VALUE;


@Log4j2
@RestController
@PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
@Tag(name = "admin-trash-bin", description = "Reserved operations on trash-bin")
public class AdminTrashBinController {


    private static final String MARKDOWN_DEPRECIATION_WARNING =
            """
            The trash-bin is deleting automatically its data after a few days.\s\s
            Using it is as a transit is not recommended, as **any delay in a folder process will result in a permanent loss of data**.
            
            This endpoint requires an admin role.\s\s
            Using it is as a transit is not recommended, as **sharing an admin login information to a third-party software is a security risk.**
            """;


    // <editor-fold desc="Beans">


    private final AuthServiceInterface authService;
    private final ContentServiceInterface contentService;
    private final DeskBusinessService deskBusinessService;
    private final FolderBusinessService folderBusinessService;
    private final ModelMapper modelMapper;
    private final StatsServiceInterface statsService;
    private final TypologyBusinessService typologyBusinessService;
    private final UserBusinessService userBusinessService;
    private final WorkflowServiceInterface workflowService;


    @Autowired
    public AdminTrashBinController(AuthServiceInterface authService,
                                   ContentServiceInterface contentService,
                                   DeskBusinessService deskBusinessService,
                                   FolderBusinessService folderBusinessService,
                                   ModelMapper modelMapper,
                                   StatsServiceInterface statsService,
                                   TypologyBusinessService typologyBusinessService,
                                   UserBusinessService userBusinessService,
                                   WorkflowServiceInterface workflowService) {
        this.authService = authService;
        this.contentService = contentService;
        this.deskBusinessService = deskBusinessService;
        this.folderBusinessService = folderBusinessService;
        this.modelMapper = modelMapper;
        this.statsService = statsService;
        this.typologyBusinessService = typologyBusinessService;
        this.userBusinessService = userBusinessService;
        this.workflowService = workflowService;
    }


    // </editor-fold desc="Beans">


    @GetMapping(API_STANDARD_V1 + "/admin/tenant/{tenantId}/trash-bin/{folderId}/zip")
    @Operation(
            summary = "Download a folder as ZIP",
            description = "Get every file, annexes and main document in a ZIP, with a PREMIS-formatted summary\n\n"
                    + "Note that the zipped PREMIS is definitely set when it enters the trash-bin, and could not be modified afterwards.\s\s\n"
                    + "This is one of the many reasons to not use this entrypoint, and use the standard FINISHED entrypoint.\n\n"
                    + MARKDOWN_DEPRECIATION_WARNING,
            deprecated = true
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200, content = @Content(mediaType = APPLICATION_OCTET_STREAM_VALUE)),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void downloadTrashBinFolderZip(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                          @PathVariable(name = Tenant.API_PATH) String tenantId,
                                          @TenantResolved Tenant tenant,
                                          @Parameter(description = Folder.API_DOC_ID_VALUE)
                                          @PathVariable(name = Folder.API_PATH) String folderId,
                                          @FolderResolved(withHistory = true) Folder folder,
                                          @CurrentUserResolved User currentUser,
                                          @NotNull HttpServletResponse response) {

        log.debug("downloadTrashBinFolderZip tenantId:{} folderId:{}", tenant.getId(), folder.getId());

        // Update inner values

        contentService.populateFolderWithAllDocumentTypes(folder);
        userBusinessService.updateInnerUserValues(singletonList(folder));
        deskBusinessService.updateFoldersInnerDeskValues(singletonList(folder));
        typologyBusinessService.updateTypology(tenant.getId(), singletonList(folder), false);

        // Build up

        String premisContent;
        try (InputStream bais = contentService.retrievePipedDocument(folder.getPremisNodeId())) {
            premisContent = XmlUtils.patchTrashBinPremis(getClass().getClassLoader(), bais);
        } catch (IOException | TransformerException e) {
            log.error("Cannot read/patch the trash-bin PREMIS content", e);
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.error_reading_premis_file", e);
        }

        // Trigger the client download

        folderBusinessService.downloadFolderAsZip(response, tenant, folder, premisContent);
    }


    @DeleteMapping(API_STANDARD_V1 + "/admin/tenant/{tenantId}/trash-bin/{folderId}")
    @Operation(
            summary = "Permanently delete a folder",
            description = MARKDOWN_DEPRECIATION_WARNING,
            deprecated = true
    )
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_204),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void deleteTrashBinFolder(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                     @PathVariable(name = Tenant.API_PATH) String tenantId,
                                     @TenantResolved Tenant tenant,
                                     @Parameter(description = Folder.API_DOC_ID_VALUE)
                                     @PathVariable(name = Folder.API_PATH) String folderId,
                                     @FolderResolved Folder folder) {

        log.info("deleteTrashBinFolder tenantId:{} folderId:{}", tenantId, folderId);

        // Actual delete

        folderBusinessService.deleteFolder(tenantId, folder, true);

        // Building stats result

        Long timeToCompleteInHours = Optional.ofNullable(folder.getSentToArchivesDate())
                .map(endTime -> MILLISECONDS.toHours(new Date().getTime() - endTime.getTime()))
                .orElse(null);

        statsService.registerFolderAction(tenant, DELETE, folder, folder.getFinalDesk(), timeToCompleteInHours);
    }


    @GetMapping(API_INTERNAL + "/admin/tenant/{tenantId}/trash-bin")
    @Operation(summary = "List folders in the trash-bin")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<FolderListableDto> listTrashBinListableFolders(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                               @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                               @TenantResolved Tenant tenant,
                                                               @PageableDefault(sort = FolderSortBy.Constants.FOLDER_NAME_VALUE)
                                                               @ParameterObject Pageable pageable) {

        log.debug("listTrashBinFolders tenant:{} page:{} pageSize:{}", tenant.getId(), pageable.getPageNumber(), pageable.getPageSize());
        PaginatedList<? extends Folder> result = workflowService.getArchives(tenant.getId(), pageable, null);

        // Compute missing values

        typologyBusinessService.updateTypology(tenant.getId(), result.getData(), false);
        authService.updateFolderReferencedDeskNames(result.getData());

        // Send back results

        log.debug("listTrashBinFolders result:{}", result.getData().size());
        return new PageImpl<>(result.getData(), pageable, result.getTotal())
                .map(folder -> modelMapper.map(folder, FolderListableDto.class));
    }


    @GetMapping(API_STANDARD_V1 + "/admin/tenant/{tenantId}/trash-bin")
    @Operation(
            summary = "List folders in the trash-bin",
            description = FolderSortBy.Constants.API_DOC_SORT_BY_VALUES + "\n\n" + MARKDOWN_DEPRECIATION_WARNING,
            deprecated = true)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<FolderRepresentation> listTrashBinFolders(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                          @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                          @TenantResolved Tenant tenant,
                                                          @PageableDefault(sort = FolderSortBy.Constants.FOLDER_NAME_VALUE)
                                                          @ParameterObject Pageable pageable) {

        return listTrashBinListableFolders(tenantId, tenant, pageable)
                .map(folder -> modelMapper.map(folder, FolderRepresentation.class));
    }


}
