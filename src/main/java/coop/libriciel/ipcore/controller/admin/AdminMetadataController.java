/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller.admin;

import coop.libriciel.ipcore.model.database.*;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.model.workflow.WorkflowDefinition;
import coop.libriciel.ipcore.services.database.*;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.services.resolvers.MetadataResolver.MetadataResolved;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.TextUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.PostConstruct;
import jakarta.validation.Valid;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Stream;

import static coop.libriciel.ipcore.IpCoreApplication.API_PROVISIONING_V1;
import static coop.libriciel.ipcore.model.stats.StatsCategory.METADATA;
import static coop.libriciel.ipcore.model.workflow.Action.*;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.RequestUtils.convertSortedPageable;
import static coop.libriciel.ipcore.utils.TextUtils.MESSAGE_BUNDLE;
import static coop.libriciel.ipcore.utils.TextUtils.RESERVED_PREFIX;
import static java.lang.Integer.MAX_VALUE;
import static java.util.Arrays.stream;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toSet;
import static org.springframework.data.domain.Pageable.unpaged;
import static org.springframework.http.HttpStatus.*;


@Log4j2
@RestController
@Tag(name = "admin-metadata", description = "Reserved operations on metadata")
public class AdminMetadataController {


    public static final int MAX_SERIALIZED_VALUE_LENGTH = 32767;
    public static final int MAX_RESTRICTED_VALUES_COUNT = 100;


    @Value("${" + DatabaseServiceInterface.PREFERENCES_PROVIDER_KEY + "}")
    private String databaseDriverClassName;


    // <editor-fold desc="Beans">


    private final ModelMapper modelMapper;
    private final StatsServiceInterface statsService;
    private final MetadataRepository metadataRepository;
    private final TableLayoutRepository tableLayoutRepository;
    private final PermissionServiceInterface permissionService;
    private final WorkflowServiceInterface workflowService;
    private final SubtypeMetadataRepository subtypeMetadataRepository;
    private final SubtypeRepository subtypeRepository;
    ResourceBundle messageResourceBundle = ResourceBundle.getBundle(MESSAGE_BUNDLE, Locale.getDefault());


    @Autowired
    public AdminMetadataController(ModelMapper modelMapper,
                                   StatsServiceInterface statsService,
                                   MetadataRepository metadataRepository, TableLayoutRepository tableLayoutRepository,
                                   PermissionServiceInterface permissionService,
                                   SubtypeMetadataRepository subtypeMetadataRepository,
                                   WorkflowServiceInterface workflowService,
                                   SubtypeRepository subtypeRepository) {
        this.modelMapper = modelMapper;
        this.statsService = statsService;
        this.metadataRepository = metadataRepository;
        this.tableLayoutRepository = tableLayoutRepository;
        this.permissionService = permissionService;
        this.subtypeMetadataRepository = subtypeMetadataRepository;
        this.workflowService = workflowService;
        this.subtypeRepository = subtypeRepository;
    }


    @PostConstruct
    public void init() {

        // Default case

        boolean isInBlankMode = StringUtils.equals(databaseDriverClassName, org.h2.Driver.class.getCanonicalName());
        if (isInBlankMode) {
            return;
        }

        // Init

        Set<String> missingInternalMetadataKeys = stream(InternalMetadata.values())
                .map(InternalMetadata::getKey)
                .collect(toSet());

        Set<String> existingMetadataKeys = metadataRepository
                .findByKeyIn(missingInternalMetadataKeys, unpaged())
                .getContent()
                .stream()
                .map(Metadata::getKey)
                .collect(toSet());

        missingInternalMetadataKeys.removeAll(existingMetadataKeys);
        missingInternalMetadataKeys.stream()
                .map(InternalMetadata::fromKey)
                .forEach(internalMetadata -> {
                    log.info("Creating the internal metadata %s...".formatted(internalMetadata.getKey()));
                    Metadata metadata = new Metadata(
                            UUID.randomUUID().toString(),
                            internalMetadata.getKey(),
                            null,
                            MAX_VALUE,
                            internalMetadata.getType(),
                            emptyList(),
                            null,
                            emptyList()
                    );
                    metadataRepository.save(metadata);
                });
    }


    // </editor-fold desc="Beans">


    @GetMapping(API_PROVISIONING_V1 + "/admin/internal-metadata")
    @Operation(summary = "List internal metadata", description = MetadataSortBy.Constants.API_DOC_SORT_BY_VALUES)
    @PreAuthorize("hasAnyRole('admin', 'tenant_*_admin')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class)))
    })
    public Page<MetadataRepresentation> listInternalMetadataAsAdmin(@PageableDefault(sort = MetadataSortBy.Constants.NAME_VALUE)
                                                                    @ParameterObject Pageable pageable) {
        log.debug("listInternalMetadataAsAdmin");

        List<String> internalMetadataKeys = Stream
                .of(InternalMetadata.values())
                .map(InternalMetadata::getKey)
                .toList();

        Pageable innerPageable = convertSortedPageable(pageable, MetadataSortBy.class, MetadataSortBy::getColumnName);
        return metadataRepository
                .findByKeyIn(internalMetadataKeys, innerPageable)
                .map(metadata -> modelMapper.map(metadata, MetadataRepresentation.class));
    }


    // <editor-fold desc="Metadata CRUDL">


    @PostMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/metadata")
    @Operation(summary = "Create a metadata")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_201),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_409, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_507, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public MetadataDto createMetadata(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                      @PathVariable(name = Tenant.API_PATH) String tenantId,
                                      @TenantResolved Tenant tenant,
                                      @Parameter(description = "body", required = true)
                                      @RequestBody @Valid MetadataDto metadataDto) {

        log.info("Create metadata name:{}", metadataDto.getName());

        // Integrity checks

        long metadataCount = metadataRepository.countAllByTenant_Id(tenant.getId());
        int limit = tenant.getMetadataLimit() != null ? tenant.getMetadataLimit() : MAX_METADATAS_COUNT;
        if (metadataCount >= limit) {
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE,
                    "message.already_n_metadata_maximum_reached",
                    metadataCount);
        }

        if (!TextUtils.isValidKey(metadataDto.getKey())) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.invalid_key_format");
        }

        if (metadataDto.getKey().startsWith(RESERVED_PREFIX)) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.invalid_key_prefix_reserved");
        }


        TextUtils.checkValues(metadataDto.getType(), metadataDto.getRestrictedValues());

        // Actual registering

        Metadata metadata = modelMapper.map(metadataDto, Metadata.class);
        metadata.setId(UUID.randomUUID().toString());
        metadata.setTenant(tenant);
        permissionService.createMetadataResource(tenant.getId(), metadata.getId());

        try {
            metadata = metadataRepository.save(metadata);
        } catch (DataIntegrityViolationException e) {
            throw new LocalizedStatusException(CONFLICT, e, "message.this_metadata_key_already_exists");
        }

        statsService.registerAdminAction(tenant, METADATA, CREATE, metadata.getId());
        return modelMapper.map(metadata, MetadataDto.class);
    }


    @GetMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/metadata/{metadataId}")
    @Operation(summary = "Get a full metadata description")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public MetadataDto getMetadata(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                   @PathVariable(name = Tenant.API_PATH) String tenantId,
                                   @Parameter(description = Metadata.API_DOC_ID_VALUE)
                                   @PathVariable(name = Metadata.API_PATH) String metadataId,
                                   @MetadataResolved Metadata metadata) {
        log.debug("getMetadata tenantId:{} metadataId:{}", tenantId, metadata.getId());
        return modelMapper.map(metadata, MetadataDto.class);
    }


    @PutMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/metadata/{metadataId}")
    @Operation(summary = "Edit a metadata")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_406, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_507, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void updateMetadata(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                               @PathVariable(name = Tenant.API_PATH) String tenantId,
                               @Parameter(description = Metadata.API_DOC_ID_VALUE)
                               @PathVariable(name = Metadata.API_PATH) String metadataId,
                               @MetadataResolved Metadata existingMetadata,
                               @Parameter(description = "body", required = true)
                               @RequestBody @Valid MetadataDto updatedMetadataDto) {

        log.info("editMetadata tenantId:{} metadataId:{}", tenantId, existingMetadata.getId());

        // Convert to entity

        Metadata updatedMetadata = modelMapper.map(updatedMetadataDto, Metadata.class);
        updatedMetadata.setId(existingMetadata.getId());
        updatedMetadata.setTenant(existingMetadata.getTenant());
        updatedMetadata.setSubtypeMetadataList(existingMetadata.getSubtypeMetadataList());

        // Integrity check

        if (!StringUtils.equals(updatedMetadata.getKey(), existingMetadata.getKey())) {
            log.error("Cannot modify metadata key, update cancelled");
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.cannot_modify_a_metadata_key");
        }

        if (updatedMetadata.getType() != existingMetadata.getType()) {
            log.error("Cannot modify metadata type, update cancelled");
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.cannot_modify_a_metadata_type");
        }

        TextUtils.checkValues(updatedMetadata.getType(), updatedMetadata.getRestrictedValues());

        List<String> deletedValues = existingMetadata.getRestrictedValues().stream()
                .filter(value -> !updatedMetadata.getRestrictedValues().contains(value)).toList();
        if (!deletedValues.isEmpty()) {
            List<String> messageList = new ArrayList<>();
            for (String value : deletedValues) {
                Page<SubtypeMetadata> subtypeMetadataPage = subtypeMetadataRepository.findByMetadata_IdAndDefaultValueEquals(
                        metadataId, value, PageRequest.ofSize(5));

                Page<Task> instanceUsingMetadataPage = workflowService.getInstancesByMetadataValue(existingMetadata.getKey(), value, tenantId);

                if (!subtypeMetadataPage.isEmpty() || !instanceUsingMetadataPage.isEmpty()) {
                    messageList.add(
                            value + "<br/>" + subtypeMetadataPage.getTotalElements() + " " + messageResourceBundle.getString("message.subtype") + "(s) "
                                    + subtypeMetadataPage.stream().map(s -> s.getSubtype().getName()).toList() + "<br/>"
                                    + " " + instanceUsingMetadataPage.getTotalElements() + " " + messageResourceBundle.getString("message.folder") + "(s) "
                                    + instanceUsingMetadataPage.stream().map(Task::getInstanceName).toList() + " "
                    );
                }
            }
            if (!messageList.isEmpty()) {
                log.error("Update metadata cancelled due to subtype and/or folder use(s)");
                throw new LocalizedStatusException(CONFLICT, "message.cannot_modify_metadata_with_used_value", messageList);
            }
        }

        // Actual save

        metadataRepository.save(updatedMetadata);
        statsService.registerAdminAction(existingMetadata.getTenant(), METADATA, UPDATE, existingMetadata.getId());
    }


    @DeleteMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/metadata/{metadataId}")
    @Operation(summary = "Delete a metadata")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_204),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_409, content = @Content(schema = @Schema(implementation = ErrorResponse.class)))
    })
    public void deleteMetadata(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                               @PathVariable(name = Tenant.API_PATH) String tenantId,
                               @Parameter(description = Metadata.API_DOC_ID_VALUE)
                               @PathVariable(name = Metadata.API_PATH) String metadataId,
                               @MetadataResolved Metadata metadata) {

        log.info("deleteMetadata metadataId:{} tenantId:{}", metadata.getId(), tenantId);

        Page<WorkflowDefinition> workflowDefinitionPage = workflowService.getWorkflowsUsingMetadataId(metadata.getId(), tenantId);
        Page<Folder> folderPage = workflowService.getInstancesUsingOldWorkflowsByMetadataId(metadata.getId(), tenantId);
        Page<SubtypeMetadata> subtypeMetadataPage = subtypeMetadataRepository.findByMetadata_Id(metadata.getId(), PageRequest.ofSize(5));
        Page<Subtype> subtypePage = subtypeRepository.findAllByIdIn(subtypeMetadataPage.stream().map(s -> s.getSubtype().getId()).toList(),
                PageRequest.ofSize(5));

        if (workflowDefinitionPage.getTotalElements() > 0 || folderPage.getTotalElements() > 0 || subtypePage.getTotalElements() > 0) {
            throw new LocalizedStatusException(
                    CONFLICT,
                    "message.cannot_delete_used_metadata",
                    workflowDefinitionPage.getTotalElements(),
                    workflowDefinitionPage.stream().map(WorkflowDefinition::getName).toList(),
                    folderPage.getTotalElements(),
                    folderPage.stream().map(Folder::getName).toList(),
                    subtypePage.getTotalElements(),
                    subtypePage.stream().map(Subtype::getName).toList());
        }

        tableLayoutRepository.deleteAllTableLayoutMetadataColumnByMetadataId(metadata.getId());
        permissionService.deletePermission(metadata.getId());
        metadataRepository.deleteById(metadata.getId());
        statsService.registerAdminAction(metadata.getTenant(), METADATA, DELETE, metadata.getId());
    }


    @GetMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/metadata")
    @Operation(summary = "List all metadata associated with the tenant", description = MetadataSortBy.Constants.API_DOC_SORT_BY_VALUES)
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin', 'tenant_' + #tenantId + '_functional_admin')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class)))
    })
    public Page<MetadataRepresentation> listMetadata(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                     @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                     @TenantResolved Tenant tenant,
                                                     @Parameter(description = "Add internal metadata to the list")
                                                     @RequestParam(defaultValue = "false") boolean addInternalMetadata,
                                                     @RequestParam(required = false) String searchTerm,
                                                     @PageableDefault(sort = MetadataSortBy.Constants.NAME_VALUE)
                                                     @ParameterObject Pageable pageable) {

        log.debug(
                "listMetadataAsAdmin addInternalMetadata:{} page:{} pageSize:{} searchTerm:{}",
                addInternalMetadata,
                pageable.getPageNumber(),
                pageable.getPageSize(),
                searchTerm
        );
        Pageable innerPageable = convertSortedPageable(pageable, MetadataSortBy.class, MetadataSortBy::getColumnName);

        Page<MetadataRepresentation> result = metadataRepository
                .findAll(tenantId, searchTerm, innerPageable, addInternalMetadata)
                .map(metadata -> modelMapper.map(metadata, MetadataRepresentation.class));

        log.debug("listMetadataAsAdmin result:{}", result.getContent().size());
        return result;
    }


    // </editor-fold desc="Metadata CRUDL">


}
