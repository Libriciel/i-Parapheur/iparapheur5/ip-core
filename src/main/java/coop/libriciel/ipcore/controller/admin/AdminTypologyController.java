/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller.admin;

import coop.libriciel.ipcore.business.auth.DeskBusinessService;
import coop.libriciel.ipcore.business.typology.SubtypeProperties;
import coop.libriciel.ipcore.business.typology.TypologyBusinessService;
import coop.libriciel.ipcore.model.crypto.SealCertificateRepresentation;
import coop.libriciel.ipcore.model.crypto.SignatureFormat;
import coop.libriciel.ipcore.model.database.*;
import coop.libriciel.ipcore.model.database.requests.*;
import coop.libriciel.ipcore.model.workflow.StepDefinition;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.model.workflow.WorkflowDefinition;
import coop.libriciel.ipcore.services.database.*;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.services.resolvers.SubtypeResolver.SubtypeResolved;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.services.resolvers.TypeResolver.TypeResolved;
import coop.libriciel.ipcore.services.secret.SecretServiceInterface;
import coop.libriciel.ipcore.services.securemail.SecureMailServiceInterface;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Nullable;
import jakarta.validation.Valid;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.modelmapper.ModelMapper;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static coop.libriciel.ipcore.IpCoreApplication.*;
import static coop.libriciel.ipcore.model.crypto.SignatureFormat.AUTO;
import static coop.libriciel.ipcore.model.crypto.SignatureFormat.PADES;
import static coop.libriciel.ipcore.model.database.SubtypeMetadata.SUBTYPE_METADATA_COMPARATOR;
import static coop.libriciel.ipcore.model.database.requests.SubtypeDto.WORKFLOW_SELECTION_SCRIPT_DEFAULT_MAX_SIZE;
import static coop.libriciel.ipcore.model.database.requests.SubtypeDto.WORKFLOW_SELECTION_SCRIPT_FULL_MAX_SIZE;
import static coop.libriciel.ipcore.model.stats.StatsCategory.SUBTYPE;
import static coop.libriciel.ipcore.model.stats.StatsCategory.TYPE;
import static coop.libriciel.ipcore.model.workflow.Action.*;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.PaginatedList.MAX_PAGE_SIZE;
import static coop.libriciel.ipcore.utils.RequestUtils.convertSortedPageable;
import static java.util.Collections.emptyList;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toSet;
import static org.springframework.data.domain.Pageable.unpaged;
import static org.springframework.http.HttpStatus.*;


@Log4j2
@RestController
@Tag(name = "admin-typology", description = "Reserved operations on types/subtypes")
public class AdminTypologyController {


    private static final int MAX_TYPES_COUNT = 250000;
    private static final int MAX_SUBTYPES_COUNT = 250000;


    // <editor-fold desc="Beans">


    private final DeskBusinessService deskBusinessService;
    private final DatabaseServiceInterface databaseService;
    private final ExternalSignatureConfigRepository externalSignatureConfigRepository;
    private final ModelMapper modelMapper;
    private final PermissionServiceInterface permissionService;
    private final SecretServiceInterface secretService;
    private final SecureMailServiceInterface secureMailService;
    private final StatsServiceInterface statsService;
    private final SubtypeLayerRepository subtypeLayerRepository;
    private final SubtypeMetadataRepository subtypeMetadataRepository;
    private final SubtypeProperties subtypeProperties;
    private final SubtypeRepository subtypeRepository;
    private final TypeRepository typeRepository;
    private final TypologyBusinessService typologyBusinessService;
    private final WorkflowServiceInterface workflowService;


    @Autowired
    public AdminTypologyController(DeskBusinessService deskBusinessService,
                                   DatabaseServiceInterface databaseService,
                                   ExternalSignatureConfigRepository externalSignatureConfigRepository,
                                   ModelMapper modelMapper,
                                   PermissionServiceInterface permissionService,
                                   SecretServiceInterface secretService,
                                   SecureMailServiceInterface secureMailService,
                                   StatsServiceInterface statsService,
                                   SubtypeLayerRepository subtypeLayerRepository,
                                   SubtypeMetadataRepository subtypeMetadataRepository,
                                   SubtypeProperties subtypeProperties,
                                   SubtypeRepository subtypeRepository,
                                   TypeRepository typeRepository,
                                   TypologyBusinessService typologyBusinessService,
                                   WorkflowServiceInterface workflowService) {
        this.deskBusinessService = deskBusinessService;
        this.databaseService = databaseService;
        this.externalSignatureConfigRepository = externalSignatureConfigRepository;
        this.modelMapper = modelMapper;
        this.permissionService = permissionService;
        this.secretService = secretService;
        this.secureMailService = secureMailService;
        this.statsService = statsService;
        this.subtypeLayerRepository = subtypeLayerRepository;
        this.subtypeMetadataRepository = subtypeMetadataRepository;
        this.subtypeProperties = subtypeProperties;
        this.subtypeRepository = subtypeRepository;
        this.typeRepository = typeRepository;
        this.typologyBusinessService = typologyBusinessService;
        this.workflowService = workflowService;
    }


    // </editor-fold desc="Beans">


    @GetMapping(API_INTERNAL + "/admin/tenant/{tenantId}/typology")
    @Operation(summary = "Get typology list, sorted by hierarchy.", description = TypologySortBy.Constants.API_DOC_HIERARCHY_SORT_BY_VALUES)
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_406, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<TypologyRepresentation> getTypologyHierarchy(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                             @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                             @TenantResolved Tenant tenant,
                                                             @Parameter(description = "Collapse (or expand) subtypes in the tree hierarchy.")
                                                             @RequestParam(required = false, defaultValue = "false") boolean collapseAll,
                                                             @Parameter(description = """
                                                                                      Reversed ID list.
                                                                                      * If `collapseAll` is `true`, children of given desk IDs will be retrieved.
                                                                                      * If `collapseAll` is `false`, children of given desk IDs won't be retrieved.
                                                                                      """)
                                                             @RequestParam(required = false) Set<String> reverseIdList,
                                                             @PageableDefault(sort = TypologySortBy.Constants.NAME_VALUE)
                                                             @ParameterObject Pageable pageable,
                                                             @Parameter(description = "Searching for a specific typology name")
                                                             @RequestParam(required = false) String searchTerm) {

        log.info("getTypologyHierarchy page:{} pageSize:{}", pageable.getPageNumber(), pageable.getPageSize());

        // Since we want a union between two tables, one of {Type|Subtype}Repository won't help much.
        // We have to go for a native query if we want a single SQL request.

        Pageable innerPageable = convertSortedPageable(pageable, TypologySortBy.class, TypologySortBy::getColumnName);
        List<TypologyRepresentation> result = databaseService.getTypologyHierarchy(tenant.getId(), innerPageable, collapseAll, reverseIdList, searchTerm);
        long total = databaseService.getTypologyHierarchyListTotal(tenant.getId(), collapseAll, reverseIdList, searchTerm);

        // Sending back result

        return new PageImpl<>(result, pageable, total);
    }


    // <editor-fold desc="Types CRUDL">


    @PostMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/typology/type")
    @Operation(summary = "Create a type")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_201),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_409, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_507, content = @Content(schema = @Schema(implementation = ErrorResponse.class)))
    })
    public TypeDto createType(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                              @PathVariable(name = Tenant.API_PATH) String tenantId,
                              @TenantResolved Tenant tenant,
                              @RequestBody @Valid TypeDto request) {

        log.info("Create Type tenant:{} name:{}", tenant.getId(), request.getName());
        Type type = modelMapper.map(request, Type.class);

        // Integrity check

        long typesCount = typeRepository.findAllByTenantId(tenantId, PageRequest.of(0, 1)).getTotalElements();
        int limit = tenant.getTypeLimit() != null ? tenant.getTypeLimit() : MAX_TYPES_COUNT;
        if (typesCount >= limit) {
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE, "message.already_n_types_maximum_reached", typesCount);
        }

        checkTypeNameUnicity(tenantId, request);

        // Update given model with hidden and immutable values

        type.setId(UUID.randomUUID().toString());
        type.setTenant(tenant);
        type.setSubtypes(emptyList());

        // Actual save

        Type result = typeRepository.save(type);
        statsService.registerAdminAction(tenant, TYPE, CREATE, result.getId());
        return modelMapper.map(result, TypeDto.class);
    }


    private void checkTypeNameUnicity(@NotNull String tenantId, TypeDto request) {
        boolean nameAlreadyUsed = typeRepository
                .findAllByTenantIdAndNameIgnoreCase(tenantId, request.getName(), Pageable.ofSize(1))
                .stream()
                .findAny()
                .isPresent();
        if (nameAlreadyUsed) {
            throw new LocalizedStatusException(CONFLICT, "message.already_existing_type_name");
        }
    }


    @GetMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/typology/type/{typeId}")
    @Operation(summary = "Get a type with every information set")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class)))
    })
    public TypeDto getType(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                           @PathVariable(name = Tenant.API_PATH) String tenantId,
                           @Parameter(description = Type.API_DOC_ID_VALUE)
                           @PathVariable(name = Type.API_PATH) String typeId,
                           @TypeResolved Type type) {

        log.debug("getTypeInfo tenant:{} name:{}", tenantId, type.getName());
        return modelMapper.map(type, TypeDto.class);
    }


    @PutMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/typology/type/{typeId}")
    @Operation(summary = "Edit a type")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class)))
    })
    public TypeDto updateType(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                              @PathVariable(name = Tenant.API_PATH) String tenantId,
                              @Parameter(description = Type.API_DOC_ID_VALUE)
                              @PathVariable(name = Type.API_PATH) String typeId,
                              @TypeResolved Type type,
                              @RequestBody @Valid TypeDto request) {

        log.info("updateType id:{}", typeId);
        Type updatedType = modelMapper.map(request, Type.class);

        // Integrity check

        if (!StringUtils.equals(type.getName(), request.getName())) {
            checkTypeNameUnicity(tenantId, request);
        }

        // Update given model with hidden and immutable values

        updatedType.setId(type.getId());
        updatedType.setTenant(type.getTenant());
        updatedType.setSubtypes(type.getSubtypes());

        // Save and return result

        Type result = typeRepository.save(updatedType);
        statsService.registerAdminAction(type.getTenant(), TYPE, UPDATE, updatedType.getId());
        return modelMapper.map(result, TypeDto.class);
    }


    @DeleteMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/typology/type/{typeId}")
    @Operation(summary = "Delete a type")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_204),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class)))
    })
    public void deleteType(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                           @PathVariable(name = Tenant.API_PATH) String tenantId,
                           @Parameter(description = Type.API_DOC_ID_VALUE)
                           @PathVariable(name = Type.API_PATH) String typeId,
                           @TypeResolved Type type) {

        log.info("deleteType id:{}", type.getId());

        List<Subtype> currentlyUsedSubtypeList = subtypeRepository.findAllByTenant_IdAndParentType_Id(tenantId, typeId, PageRequest.of(0, MAX_PAGE_SIZE))
                .stream()
                .filter(subtype -> !workflowService.getInstancesByTypology(tenantId, typeId, subtype.getId(), PageRequest.of(0, 3)).getContent().isEmpty())
                .toList();

        if (!currentlyUsedSubtypeList.isEmpty()) {
            int subtypeNb = currentlyUsedSubtypeList.size();
            log.error("deleteType error: the type has {} subtype(s) used by current folder(s)", subtypeNb);
            throw new LocalizedStatusException(NOT_ACCEPTABLE,
                    "message.cannot_delete_a_type_with_subtypes_used_by_current_folders",
                    subtypeNb,
                    currentlyUsedSubtypeList.stream().map(Subtype::getName).limit(3).toList()
            );
        }

        typologyBusinessService.deleteType(tenantId, type.getId());
        statsService.registerAdminAction(type.getTenant(), TYPE, DELETE, type.getId());
    }


    @GetMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/typology/type")
    @Operation(summary = "List types")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin', 'tenant_' + #tenantId + '_functional_admin')")
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<TypeRepresentation> listTypes(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                              @PathVariable(name = Tenant.API_PATH) String tenantId,
                                              @TenantResolved Tenant tenant,
                                              @ParameterObject Pageable pageable) {

        log.info("getTypes tenantId:{} page:{} pageSize:{}", tenant.getId(), pageable.getPageNumber(), pageable.getPageSize());
        Page<Type> result = typeRepository.findAllByTenantId(tenant.getId(), pageable);

        // Sending back result

        return result.map(type -> modelMapper.map(type, TypeRepresentation.class));
    }


    // </editor-fold desc="Types CRUDL">


    // <editor-fold desc="Subtypes CRUDL">


    @PostMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/typology/type/{typeId}/subtype")
    @Operation(summary = "Create a subtype")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_201),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_409, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_507, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public SubtypeDto createSubtype(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                    @PathVariable(name = Tenant.API_PATH) String tenantId,
                                    @Parameter(description = Type.API_DOC_ID_VALUE)
                                    @PathVariable(name = Type.API_PATH) String typeId,
                                    @TypeResolved Type type,
                                    @RequestBody @Valid SubtypeDto request) {

        Tenant tenant = type.getTenant();
        log.info("createSubtype tenantId:{} typeId:{} subtypeName:{}", tenant.getId(), typeId, request.getName());

        // Convert to entity

        Subtype subtype = modelMapper.map(request, Subtype.class);
        updateSubtypeParameters(subtype, type);
        log.debug("createSubtype generatedId:{}", subtype.getId());
        request.getSubtypeMetadataList().forEach(sm -> sm.setSubtypeId(subtype.getId()));
        request.getSubtypeLayers().forEach(sm -> sm.setSubtypeId(subtype.getId()));

        // Integrity check

        checkUniqueSignatureWorkflowForPes(tenantId, request, type);

        // TODO: remove unpaged, add workflow method to test name
        long subtypesCount = subtypeRepository.findAllByTenant_Id(tenantId, unpaged()).getTotalElements();
        int limit = tenant.getSubtypeLimit() != null ? tenant.getSubtypeLimit() : MAX_SUBTYPES_COUNT;
        if (subtypesCount >= limit) {
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE,
                    "message.already_n_subtypes_maximum_reached",
                    subtypesCount);
        }

        checkSubtypeNameUnicity(tenantId, typeId, null, request);

        SealCertificateRepresentation sealCertificate = Optional.ofNullable(request.getSealCertificateId())
                .filter(StringUtils::isNotEmpty)
                .map(sealCertId -> Optional
                        .ofNullable(secretService.getSealCertificate(tenant.getId(), sealCertId))
                        .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_seal_certificate_id")))
                .map(SealCertificateRepresentation::new)
                .orElse(null);

        checkSubtypeIntegrity(tenantId, type, subtype);

        // Actual registering

        // Main resource
        permissionService.createSubtypeResource(tenant.getId(), subtype.getId());

        // We'll save the linked elements later.
        // For now we don't want those to appear, since the subtype id does not exist yet.
        subtype.setSubtypeMetadataList(emptyList());
        subtype.setSubtypeLayers(emptyList());
        Subtype savedSubtype = subtypeRepository.save(subtype);

        // Linked elements

        List<SubtypeMetadata> subtypeMetadataToCreate = request.getSubtypeMetadataList().stream()
                .map(sm -> modelMapper.map(sm, SubtypeMetadata.class))
                .toList();

        List<SubtypeLayer> subtypeLayerToCreate = request.getSubtypeLayers().stream()
                .map(sl -> modelMapper.map(sl, SubtypeLayer.class))
                .toList();

        log.debug("createSubtype subtypeMetadataToCreate:{}", subtypeMetadataToCreate);
        log.debug("createSubtype subtypeLayerToCreate:{}", subtypeLayerToCreate);
        subtypeMetadataRepository.saveAll(subtypeMetadataToCreate);
        subtypeLayerRepository.saveAll(subtypeLayerToCreate);

        // Update permissions

        permissionService.setSubtypeCreationPermittedDeskIds(tenantId, subtype.getId(), request.getCreationPermittedDeskIds());
        permissionService.setSubtypeFilterableByDeskIds(tenantId, subtype.getId(), request.getFilterableByDeskIds());

        // Refresh inner values and return

        SubtypeDto result = modelMapper.map(savedSubtype, SubtypeDto.class);
        deskBusinessService.updateInnerDeskValues(tenantId, result);
        result.setSealCertificate(sealCertificate);

        // Send back result

        statsService.registerAdminAction(tenant, SUBTYPE, CREATE, subtype.getId());
        return result;
    }


    private void checkSubtypeNameUnicity(String tenantId, String typeId, @Nullable String ignoreResultForSubtypeId, SubtypeDto request) {
        boolean nameAlreadyUsed = subtypeRepository
                .findAllByTenant_IdAndParentType_IdAndNameIgnoreCase(tenantId, typeId, request.getName(), Pageable.ofSize(1))
                .stream()
                .findAny()
                .filter(subtype -> !StringUtils.equals(subtype.getId(), ignoreResultForSubtypeId))
                .isPresent();
        if (nameAlreadyUsed) {
            throw new LocalizedStatusException(CONFLICT, "message.already_existing_subtype_name");
        }
    }


    @GetMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/typology/type/{typeId}/subtype/{subtypeId}")
    @Operation(summary = "Get a subtype with every information set")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public SubtypeDto getSubtype(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                 @PathVariable(name = Tenant.API_PATH) String tenantId,
                                 @Parameter(description = Type.API_DOC_ID_VALUE)
                                 @PathVariable(name = Type.API_PATH) String typeId,
                                 @Parameter(description = Subtype.API_DOC_ID_VALUE)
                                 @PathVariable(name = Subtype.API_PATH) String subtypeId,
                                 @SubtypeResolved Subtype subtype) {

        subtype.getSubtypeMetadataList().sort(SUBTYPE_METADATA_COMPARATOR);
        subtype.setFilterableByDeskIds(permissionService.getSubtypeFilterableByDeskIds(tenantId, subtypeId));
        subtype.setCreationPermittedDeskIds(permissionService.getSubtypePermittedDeskIds(tenantId, subtypeId));

        // Update inner values

        SubtypeDto result = modelMapper.map(subtype, SubtypeDto.class);
        deskBusinessService.updateInnerDeskValues(tenantId, result);
        Optional.ofNullable(subtype.getSealCertificateId())
                .filter(StringUtils::isNotEmpty)
                .map(sealCertId -> secretService.getSealCertificate(tenantId, sealCertId))
                .map(SealCertificateRepresentation::new)
                .ifPresent(result::setSealCertificate);

        // Return result

        return result;
    }


    @PutMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/typology/type/{typeId}/subtype/{subtypeId}")
    @Operation(summary = "Edit a subtype")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_204),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public SubtypeDto updateSubtype(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                    @PathVariable(name = Tenant.API_PATH) String tenantId,
                                    @Parameter(description = Type.API_DOC_ID_VALUE)
                                    @PathVariable(name = Type.API_PATH) String typeId,
                                    @Parameter(description = Subtype.API_DOC_ID_VALUE)
                                    @PathVariable(name = Subtype.API_PATH) String subtypeId,
                                    @SubtypeResolved Subtype existingSubtype,
                                    @RequestBody @Valid SubtypeDto request) {

        log.info("editSubtype subtypeId:{}", subtypeId);
        Tenant tenant = existingSubtype.getTenant();
        Type type = existingSubtype.getParentType();

        // Integrity check

        checkUniqueSignatureWorkflowForPes(tenantId, request, type);

        if (!StringUtils.equals(existingSubtype.getName(), request.getName())) {
            checkSubtypeNameUnicity(tenantId, typeId, subtypeId, request);
        }

        // Convert to entity

        Optional.ofNullable(request.getSubtypeMetadataList()).orElse(emptyList()).forEach(sm -> sm.setSubtypeId(subtypeId));
        Optional.ofNullable(request.getSubtypeLayers()).orElse(emptyList()).forEach(layer -> layer.setSubtypeId(subtypeId));

        Subtype updatedSubtype = modelMapper.map(request, Subtype.class);
        updatedSubtype.setId(existingSubtype.getId());
        updatedSubtype.setParentType(type);
        updatedSubtype.setTenant(tenant);

        SealCertificateRepresentation sealCertificate = Optional.ofNullable(request.getSealCertificateId())
                .filter(StringUtils::isNotEmpty)
                .map(sealCertId -> Optional
                        .ofNullable(secretService.getSealCertificate(tenant.getId(), sealCertId))
                        .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_seal_certificate_id")))
                .map(SealCertificateRepresentation::new)
                .orElse(null);

        updateSubtypeParameters(updatedSubtype, type);
        checkSubtypeIntegrity(tenant.getId(), type, updatedSubtype);

        // Updating DB entities

        log.info("editSubtype request.getSubtypeMetadataRequestList:{}", request.getSubtypeMetadataList());

        if (request.getSubtypeMetadataList() != null) {  // We're not supposed to have a null SubtypeMetadataDto list

            Set<String> referencedMetadataIds = request.getSubtypeMetadataList().stream().map(SubtypeMetadataDto::getMetadataId).collect(toSet());
            List<SubtypeMetadata> subtypeMetadataToDelete = subtypeMetadataRepository.findBySubtype_Id(subtypeId, unpaged()).stream()
                    .filter(sm -> !referencedMetadataIds.contains(sm.getMetadata().getId()))
                    .toList();

            List<SubtypeMetadata> subtypeMetadataToSave = request.getSubtypeMetadataList().stream()
                    .map(dto -> modelMapper.map(dto, SubtypeMetadata.class))
                    .toList();

            log.trace("editSubtype referencedMetadataIds:{}", referencedMetadataIds);
            log.trace("editSubtype subtypeMetadataToDelete:{}", subtypeMetadataToDelete);
            log.trace("editSubtype subtypeMetadataToSave:{}", subtypeMetadataToSave);
            subtypeMetadataRepository.deleteAllByIdIn(subtypeMetadataToDelete.stream().map(SubtypeMetadata::getId).toList());
            subtypeMetadataRepository.saveAll(subtypeMetadataToSave);
        }

        if (request.getSubtypeLayers() != null) {  // We're not supposed to have a null SubtypeLayerDto list

            Set<String> referencedLayerIds = request.getSubtypeLayers().stream().map(SubtypeLayerDto::getLayerId).collect(toSet());
            List<SubtypeLayer> subtypeLayerToDelete = subtypeLayerRepository.findBySubtype_Id(subtypeId, unpaged()).stream()
                    .filter(sl -> !referencedLayerIds.contains(sl.getLayer().getId()))
                    .toList();

            List<SubtypeLayer> subtypeLayerToSave = request.getSubtypeLayers().stream()
                    .map(dto -> modelMapper.map(dto, SubtypeLayer.class))
                    .toList();

            log.trace("editSubtype referencedLayerIds:{}", referencedLayerIds);
            log.trace("editSubtype subtypeLayerToDelete:{}", subtypeLayerToDelete);
            log.trace("editSubtype subtypeLayerToSave:{}", subtypeLayerToSave);
            subtypeLayerRepository.deleteAllByIdIn(subtypeLayerToDelete.stream().map(SubtypeLayer::getId).toList());
            subtypeLayerRepository.saveAll(subtypeLayerToSave);
        }

        updatedSubtype = subtypeRepository.save(updatedSubtype);

        // Permissions

        permissionService.setSubtypeCreationPermittedDeskIds(tenantId, updatedSubtype.getId(), request.getCreationPermittedDeskIds());
        permissionService.setSubtypeFilterableByDeskIds(tenantId, updatedSubtype.getId(), request.getFilterableByDeskIds());

        // Refresh inner values and return

        SubtypeDto result = modelMapper.map(updatedSubtype, SubtypeDto.class);
        deskBusinessService.updateInnerDeskValues(tenantId, result);
        result.setSealCertificate(sealCertificate);

        statsService.registerAdminAction(tenant, SUBTYPE, UPDATE, updatedSubtype.getId());
        return result;
    }


    private void checkUniqueSignatureWorkflowForPes(String tenantId, SubtypeDto request, Type type) {

        if (type.getSignatureFormat() != SignatureFormat.PES_V2) {
            return;
        }

        if (StringUtils.isEmpty(request.getValidationWorkflowId())){
            return;
        }

        workflowService.getWorkflowDefinitionByKey(tenantId, request.getValidationWorkflowId())
                .ifPresent(workflowDefinition -> {
                    List<StepDefinition> signatureSteps = workflowDefinition.getSteps().stream()
                            .filter(stepDefinition -> stepDefinition.getType() == SIGNATURE)
                            .toList();

                    if (signatureSteps.size() > 1) {
                        throw new LocalizedStatusException(FORBIDDEN, "message.you_can_t_perform_a_second_signature_on_a_pes_folder");
                    }
                });

    }


    @DeleteMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/typology/type/{typeId}/subtype/{subtypeId}")
    @Operation(summary = "Delete a subtype")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin')")
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_204),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void deleteSubtype(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                              @PathVariable(name = Tenant.API_PATH) String tenantId,
                              @Parameter(description = Type.API_DOC_ID_VALUE)
                              @PathVariable(name = Type.API_PATH) String typeId,
                              @Parameter(description = Subtype.API_DOC_ID_VALUE)
                              @PathVariable(name = Subtype.API_PATH) String subtypeId,
                              @SubtypeResolved Subtype subtype) {

        log.info("deleteSubtype subtypeId:{}", subtype.getId());

        Page<Task> taskPaginatedList = workflowService.getInstancesByTypology(tenantId, typeId, subtypeId, PageRequest.of(0, 3));

        if (!taskPaginatedList.getContent().isEmpty()) {
            Long instancesNb = taskPaginatedList.getTotalElements();
            log.error("deleteSubtype error: the subtype is used by {} current folder(s)", instancesNb);
            throw new LocalizedStatusException(NOT_ACCEPTABLE,
                    "message.cannot_delete_a_subtype_used_by_current_folders",
                    instancesNb,
                    taskPaginatedList.getContent().stream().map(Task::getInstanceName).toList());
        }

        typologyBusinessService.deleteSubtype(subtype.getId());
        statsService.registerAdminAction(subtype.getTenant(), SUBTYPE, DELETE, subtype.getId());
    }


    @GetMapping(API_PROVISIONING_V1 + "/admin/tenant/{tenantId}/typology/type/{typeId}/subtype")
    @Operation(summary = "Get subtypes")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin', 'tenant_' + #tenantId + '_functional_admin')")
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<SubtypeRepresentation> listSubtypes(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                    @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                    @Parameter(description = Type.API_DOC_ID_VALUE)
                                                    @PathVariable(name = Type.API_PATH) String typeId,
                                                    @TypeResolved Type type,
                                                    @ParameterObject Pageable pageable) {

        log.info("getTypes tenantId:{} page:{} pageSize:{}", type.getTenant().getId(), pageable.getPageNumber(), pageable.getPageSize());
        Page<Subtype> result = subtypeRepository.findAllByTenant_IdAndParentType_Id(type.getTenant().getId(), type.getId(), pageable);

        // Sending back result

        return result.map(subtype -> modelMapper.map(subtype, SubtypeRepresentation.class));
    }


    // </editor-fold desc="Subtypes CRUDL">


    /**
     * FIXME : Remove this ? It should not be variable.
     *  Maybe we shall have some kind of global getServerConfig, and everything set.
     *
     * @param tenantId
     * @return
     */
    @GetMapping(API_V1 + "/admin/tenant/{tenantId}/typology/maxScriptSize")
    @Operation(summary = "Return the maximum allowed size for workflow selection scripts", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public int getMaxScriptSize(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                @PathVariable(name = Tenant.API_PATH) String tenantId) {
        log.info("getMaxScriptSize");
        return subtypeProperties.isUnlockSelectionScriptFullSize()
               ? WORKFLOW_SELECTION_SCRIPT_FULL_MAX_SIZE
               : WORKFLOW_SELECTION_SCRIPT_DEFAULT_MAX_SIZE;
    }


    private void updateSubtypeParameters(@NotNull Subtype subtype, @NotNull Type parentType) {

        Tenant tenant = parentType.getTenant();

        subtype.setId(StringUtils.firstNonEmpty(subtype.getId(), UUID.randomUUID().toString()));
        subtype.setTenant(tenant);
        subtype.setParentType(parentType);

        // Update ext-sig config

        ExternalSignatureConfig externalSignatureConfig = ofNullable(subtype.getExternalSignatureConfig())
                .map(ExternalSignatureConfig::getId)
                .filter(StringUtils::isNotEmpty)
                .map(id -> externalSignatureConfigRepository
                        .findByTenant_IdAndId(tenant.getId(), id)
                        .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_extsig_config_id")))
                .orElse(null);

        subtype.setExternalSignatureConfig(externalSignatureConfig);

        // Check secure-mail config

        if (subtype.getSecureMailServerId() != null && secureMailService.findServer(tenant.getId(), subtype.getSecureMailServerId()) == null) {
            throw new LocalizedStatusException(NOT_FOUND, "message.unknown_mailsec_config_id");
        }

        // Check selection script

        String selectionScript = subtype.getWorkflowSelectionScript();
        if (StringUtils.isNotEmpty(selectionScript) && StringUtils.isNotEmpty(subtype.getValidationWorkflowId())) {
            selectionScript = null;
        }

        boolean isScriptFullSizeUnlocked = subtypeProperties.isUnlockSelectionScriptFullSize();
        boolean isScriptBiggerThanRegularSize = StringUtils.length(selectionScript) > WORKFLOW_SELECTION_SCRIPT_DEFAULT_MAX_SIZE;
        if (isScriptBiggerThanRegularSize && !isScriptFullSizeUnlocked) {
            throw new LocalizedStatusException(UNPROCESSABLE_ENTITY, "message.invalid_subtype_data_script_too_big");
        }

        subtype.setWorkflowSelectionScript(selectionScript);
    }


    private void checkSubtypeIntegrity(@NotNull String tenantId, @NotNull Type type, @NotNull Subtype subtype) {

        if (StringUtils.isNotEmpty(subtype.getCreationWorkflowId())) {

            WorkflowDefinition definition = workflowService
                    .getWorkflowDefinitionByKey(tenantId, subtype.getCreationWorkflowId())
                    .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_workflow_definition_id"));

            if (definition.getSteps().stream().anyMatch(s -> s.getType() != VISA)) {
                throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.creation_workflow_should_only_contain_visa");
            }

            if (definition.getSteps().stream().anyMatch(s -> s.getValidatingDesks().size() > 1)) {
                throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.creation_workflow_should_only_contain_simple_steps");
            }
        }

        if (StringUtils.isNotEmpty(subtype.getValidationWorkflowId())) {

            WorkflowDefinition definition = workflowService
                    .getWorkflowDefinitionByKey(tenantId, subtype.getValidationWorkflowId())
                    .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_workflow_definition_id"));

            boolean containsSeal = definition.getSteps().stream().anyMatch(s -> s.getType() == SEAL);
            boolean isSealConfProperlySet = subtype.getSealCertificateId() != null;
            boolean isSealCompatibleType = List.of(PADES, AUTO).contains(type.getSignatureFormat());

            if (containsSeal && !isSealConfProperlySet) {
                throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.needed_seal_configuration_is_not_set");
            }

            if (containsSeal && !isSealCompatibleType) {
                throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.signature_format_not_compatible_with_seal");
            }

            boolean containsExternalSignature = definition.getSteps().stream().anyMatch(s -> s.getType() == EXTERNAL_SIGNATURE);
            boolean isExternalSignatureConfProperlySet = subtype.getExternalSignatureConfig() != null;
            boolean isExternalSignatureCompatibleType = List.of(PADES, AUTO).contains(type.getSignatureFormat());

            if (containsExternalSignature && !isExternalSignatureConfProperlySet) {
                throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.needed_external_signature_configuration_is_not_set");
            }

            if (containsExternalSignature && !isExternalSignatureCompatibleType) {
                throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.signature_format_not_compatible_with_external_signature");
            }
        }
    }


}
