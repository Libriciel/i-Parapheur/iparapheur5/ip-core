/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller.admin;


import coop.libriciel.ipcore.business.signatureproof.SignatureReportBusinessService;
import coop.libriciel.ipcore.model.signatureValidation.ValidationServiceConfigurationDto;
import coop.libriciel.ipcore.model.signatureValidation.ValidationServiceConfiguration;
import coop.libriciel.ipcore.services.database.ServerInfoService;
import coop.libriciel.ipcore.services.gdpr.GdprInformationDetailsDto;
import coop.libriciel.ipcore.services.secret.SecretServiceInterface;
import coop.libriciel.ipcore.services.signatureValidation.SignatureValidationProperties;
import coop.libriciel.ipcore.services.signatureValidation.SignatureValidationServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


import static coop.libriciel.ipcore.IpCoreApplication.*;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.RequestUtils.PREAUTHORIZE_SUPER_ADMIN;
import static coop.libriciel.ipcore.utils.RequestUtils.PREAUTHORIZE_TENANT_FUNCTIONAL_ADMIN_OR_MORE;
import static org.springframework.http.HttpStatus.NOT_FOUND;

import org.springframework.beans.factory.annotation.Autowired;
import jakarta.validation.Valid;


@Log4j2
@RestController
@PreAuthorize(PREAUTHORIZE_SUPER_ADMIN)
@Tag(name = "admin-advanced-config", description = "Reserved operations for global configuration")
public class AdminAdvancedConfigurationController {


    private final ModelMapper modelMapper;
    private final SecretServiceInterface secretService;
    private final ServerInfoService serverInfoService;
    private final SignatureReportBusinessService signatureReportBusinessService;
    private final SignatureValidationProperties signatureValidationProperties;
    private final SignatureValidationServiceInterface signatureValidationService;


    @Autowired
    public AdminAdvancedConfigurationController(ModelMapper modelMapper,
                                                SecretServiceInterface secretService,
                                                ServerInfoService serverInfoService,
                                                SignatureReportBusinessService signatureReportBusinessService,
                                                SignatureValidationProperties signatureValidationProperties,
                                                SignatureValidationServiceInterface signatureValidationService) {
        this.modelMapper = modelMapper;
        this.secretService = secretService;
        this.serverInfoService = serverInfoService;
        this.signatureReportBusinessService = signatureReportBusinessService;
        this.signatureValidationProperties = signatureValidationProperties;
        this.signatureValidationService = signatureValidationService;
    }


    @PutMapping(API_INTERNAL + "/admin/server-info/signature-validation-service")
    @Operation(summary = "Edit signature validation service configuration")
    @PreAuthorize(PREAUTHORIZE_TENANT_FUNCTIONAL_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void updateSignatureValidationServiceConfiguration(@RequestBody @Valid ValidationServiceConfigurationDto validationServiceConfiguration) {
        ValidationServiceConfiguration configuration = modelMapper.map(validationServiceConfiguration, ValidationServiceConfiguration.class);

        signatureValidationService.testConfiguration(configuration);

        secretService.storeValidationServiceConfiguration(
                configuration.getClientId(),
                configuration.getClientSecret());

        signatureReportBusinessService.onSetup();
    }


    @GetMapping(API_INTERNAL + "/admin/server-info/signature-validation-service")
    @Operation(summary = "Get signature validation service configuration")
    @PreAuthorize(PREAUTHORIZE_TENANT_FUNCTIONAL_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public ValidationServiceConfigurationDto getSignatureValidationServiceConfiguration() {

        log.debug("getSignatureValidationServiceConfiguration environment:{}", signatureValidationProperties.getProvider());

        ValidationServiceConfiguration validationServiceConfiguration = signatureValidationService.getSignatureValidationConfiguration();
        if (validationServiceConfiguration == null) {
            throw new LocalizedStatusException(NOT_FOUND, "message.error_service_identification_missing");
        }

        return modelMapper.map(validationServiceConfiguration, ValidationServiceConfigurationDto.class);
    }


    @DeleteMapping(API_INTERNAL + "/admin/server-info/signature-validation-service")
    @Operation(summary = "Delete signature validation service configuration")
    @PreAuthorize(PREAUTHORIZE_TENANT_FUNCTIONAL_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void deleteSignatureValidationServiceConfiguration() {

        log.debug("deleteSignatureValidationServiceConfiguration");

        secretService.deleteValidationServiceConfiguration();

        signatureReportBusinessService.onSetup();
    }


    @PutMapping(API_INTERNAL + "/admin/server-info/gdpr")
    @Operation(summary = "Register GDPR information details")
    @PreAuthorize(PREAUTHORIZE_TENANT_FUNCTIONAL_ADMIN_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void updateGdprInformationDetails(@RequestBody @Valid GdprInformationDetailsDto gdprDto) {

        log.debug("updateGdprInformationDetails:{}", gdprDto);

        serverInfoService.saveGdprInformationDetails(gdprDto);
    }


}
