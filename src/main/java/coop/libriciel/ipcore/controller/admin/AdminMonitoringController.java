/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller.admin;

import coop.libriciel.ipcore.model.monitoring.JobDataResponse;
import coop.libriciel.ipcore.model.monitoring.JobEnum;
import coop.libriciel.ipcore.services.monitoring.MonitoringServiceInterface;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static coop.libriciel.ipcore.IpCoreApplication.API_V1;
import static coop.libriciel.ipcore.IpCoreApplication.HIDE_UNSTABLE_API;
import static coop.libriciel.ipcore.utils.RequestUtils.PREAUTHORIZE_SUPER_ADMIN;
import static org.springframework.http.HttpStatus.OK;


@Log4j2
@RestController
@PreAuthorize(PREAUTHORIZE_SUPER_ADMIN)
@RequestMapping(API_V1 + "/admin/monitoring")
public class AdminMonitoringController {

    private final MonitoringServiceInterface monitoringService;


    public AdminMonitoringController(MonitoringServiceInterface monitoringService) {
        this.monitoringService = monitoringService;
    }


    @GetMapping("local_space_usage")
    @Operation(summary = "Get a job data", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)")
    })
    public Long getLocalSpaceUsage() {
        return this.monitoringService.getLocalSpaceUsage();
    }


    @GetMapping("job/{job}/data")
    @Operation(summary = "Get a job data", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)")
    })
    public JobDataResponse getJobData(@PathVariable JobEnum job) {
        return this.monitoringService.getJobData(job);
    }


    @GetMapping("jobs")
    @Operation(summary = "Get all jobs data", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)")
    })
    public List<JobDataResponse> getAllJobsData() {
        return this.monitoringService.getJobsData();
    }


}
