/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller;

import coop.libriciel.ipcore.business.workflow.FolderBusinessService;
import coop.libriciel.ipcore.business.workflow.WorkflowBusinessService;
import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.securemail.MailParams;
import coop.libriciel.ipcore.model.securemail.SecureMailDocument;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.database.SubtypeRepository;
import coop.libriciel.ipcore.services.resolvers.CurrentUserResolver.CurrentUserResolved;
import coop.libriciel.ipcore.services.resolvers.DeskResolver.DeskResolved;
import coop.libriciel.ipcore.services.resolvers.FolderResolver.FolderResolved;
import coop.libriciel.ipcore.services.resolvers.TaskResolver.TaskResolved;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.services.securemail.SecureMailServiceInterface;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

import static coop.libriciel.ipcore.IpCoreApplication.API_INTERNAL;
import static coop.libriciel.ipcore.IpCoreApplication.API_STANDARD_V1;
import static coop.libriciel.ipcore.model.workflow.Action.SECURE_MAIL;
import static coop.libriciel.ipcore.services.content.ContentServiceInterface.FILE_TRANSFER_BUFFER_SIZE;
import static coop.libriciel.ipcore.services.resolvers.FolderResolver.FolderResolved.Permission.CURRENT_DESK;
import static coop.libriciel.ipcore.services.resolvers.FolderResolver.FolderResolved.Permission.WORKFLOW_DESK;
import static coop.libriciel.ipcore.services.workflow.IpWorkflowService.METADATA_PASTELL_DOCUMENT_ID;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.RequestUtils.PREAUTHORIZE_TENANT_MEMBER_OR_MORE;
import static org.springframework.http.HttpStatus.*;


@Log4j2
@RestController
@Tag(name = "secure-mail", description = "Secure-mail operations for a regular logged user")
public class SecureMailController {


    // <editor-fold desc="Beans">


    private final ContentServiceInterface contentService;
    private final FolderBusinessService folderBusinessService;
    private final SecureMailServiceInterface secureMailService;
    private final StatsServiceInterface statsService;
    private final SubtypeRepository subtypeRepository;
    private final WorkflowBusinessService workflowBusinessService;
    private final WorkflowServiceInterface workflowService;


    @Autowired
    public SecureMailController(ContentServiceInterface contentService,
                                FolderBusinessService folderBusinessService,
                                SecureMailServiceInterface secureMailService,
                                SubtypeRepository subtypeRepository,
                                StatsServiceInterface statsService,
                                WorkflowBusinessService workflowBusinessService,
                                WorkflowServiceInterface workflowService) {
        this.contentService = contentService;
        this.folderBusinessService = folderBusinessService;
        this.secureMailService = secureMailService;
        this.statsService = statsService;
        this.subtypeRepository = subtypeRepository;
        this.workflowBusinessService = workflowBusinessService;
        this.workflowService = workflowService;
    }

    // </editor-fold desc="Beans">


    @PutMapping(API_STANDARD_V1 + "/tenant/{tenantId}/desk/{deskId}/folder/{folderId}/task/{taskId}/secure_mail")
    @Operation(summary = "Secure mail", description = "Send a secure mail")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void requestSecureMail(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                  @PathVariable(name = Tenant.API_PATH) String tenantId,
                                  @TenantResolved Tenant tenant,
                                  @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                  @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                  @DeskResolved Desk desk,
                                  @Parameter(description = Folder.API_DOC_ID_VALUE)
                                  @PathVariable(name = Folder.API_PATH) String folderId,
                                  @FolderResolved(permission = CURRENT_DESK) Folder folder,
                                  @Parameter(description = Task.API_DOC_ID_VALUE)
                                  @PathVariable(name = Task.API_PATH) String taskId,
                                  @TaskResolved Task task,
                                  @CurrentUserResolved User user,
                                  @Parameter(description = "body", required = true)
                                  @RequestBody @Valid MailParams mailParams) {

        if (task.getAction() != SECURE_MAIL) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.you_can_t_perform_a_x_action_on_a_y_task", SECURE_MAIL, task.getAction());
        }

        performSecureMail(tenant, desk, folder, task, user, mailParams);
    }


    // @PreAuthorize("hasRole(#task.candidateGroup)")
    private void performSecureMail(@NotNull Tenant tenant,
                                   @NotNull Desk desk,
                                   @NotNull Folder folder,
                                   @NotNull Task task,
                                   @NotNull User user,
                                   @NotNull MailParams mailParams) {

        workflowBusinessService.checkForNullOrEmptyDesksInNextStep(tenant.getId(), folder);
        workflowBusinessService.assertCurrentUserCanPerformTask(tenant.getId(),
                task,
                null,
                folder.getType().getId(),
                folder.getSubtype().getId(),
                desk.getId()
        );

        if (StringUtils.isEmpty(folder.getSubtype().getId())) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.unknown_subtype_id");
        }

        List<Document> documentList = contentService.getDocumentList(folder.getContentId());
        SecureMailDocument pastellDocument = null;
        if (mailParams.isIncludeDocket()) {
            try (ByteArrayInputStream docketPdfInputStream = folderBusinessService.generateDocketFromFolder(tenant, folder)) {
                Flux<DataBuffer> dataFlux = DataBufferUtils.readInputStream(
                        () -> docketPdfInputStream, new DefaultDataBufferFactory(), FILE_TRANSFER_BUFFER_SIZE
                );

                pastellDocument = secureMailService.createAndSendSecureMail(tenant, folder, mailParams, documentList, dataFlux);

            } catch (IOException e) {
                log.warn("An error occurred while generating Docket for Tenant : {}, Folder : {}", tenant.getId(), folder.getId());
            }
        } else {
            pastellDocument = secureMailService.createAndSendSecureMail(tenant, folder, mailParams, documentList, null);
        }

        workflowService.performSecureMail(task, user, folder, mailParams, pastellDocument.getId());
        Long timeToCompleteInHours = statsService.computeTimeToCompleteInHours(task);
        statsService.registerFolderAction(tenant, SECURE_MAIL, folder, desk, timeToCompleteInHours);

        workflowBusinessService.triggerPostTaskActions(tenant.getId(), desk, folder, SECURE_MAIL);
    }


    @GetMapping(API_INTERNAL + "/tenant/{tenantId}/folder/{folderId}/task/{taskId}/secure-mail")
    @Operation(summary = "Get the current state")
    @PreAuthorize(PREAUTHORIZE_TENANT_MEMBER_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public SecureMailDocument getSecureMailStatus(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                  @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                  @TenantResolved Tenant tenant,
                                                  @PathVariable(name = Folder.API_PATH) String folderId,
                                                  @FolderResolved(permission = WORKFLOW_DESK) Folder folder,
                                                  @PathVariable(name = Task.API_PATH) String taskId,
                                                  @TaskResolved Task task) {

        Subtype subtype = subtypeRepository.findByIdAndTenant_Id(folder.getSubtype().getId(), tenant.getId())
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_subtype_id"));

        // TODO: this can probably be made simpler with the task given in parameter
        String lastDocumentId = folder.getStepList().stream()
                .filter(step -> step.getMetadata().containsKey(METADATA_PASTELL_DOCUMENT_ID))
                .map(step -> step.getMetadata().get(METADATA_PASTELL_DOCUMENT_ID))
                .filter(StringUtils::isNotEmpty)
                .reduce((first, second) -> second) // Get the last one
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.pastell_document_metadata_not_found"));

        return secureMailService.findDocument(subtype.getSecureMailServerId().toString(), lastDocumentId);
    }


}
