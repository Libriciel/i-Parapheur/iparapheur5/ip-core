/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller;

import coop.libriciel.ipcore.model.database.*;
import coop.libriciel.ipcore.services.database.MetadataRepository;
import coop.libriciel.ipcore.services.resolvers.MetadataResolver.MetadataResolved;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import static coop.libriciel.ipcore.IpCoreApplication.API_V1;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.RequestUtils.convertSortedPageable;
import static org.springframework.http.HttpStatus.OK;


@Log4j2
@RestController
@RequestMapping(API_V1 + "/tenant/{tenantId}/metadata")
@Tag(name = "metadata", description = "Metadata access for a regular logged user")
public class MetadataController {


    private final MetadataRepository metadataRepository;
    private final ModelMapper modelMapper;


    // <editor-fold desc="Beans">


    @Autowired
    public MetadataController(MetadataRepository metadataRepository,
                              ModelMapper modelMapper) {
        this.metadataRepository = metadataRepository;
        this.modelMapper = modelMapper;
    }


    // </editor-fold desc="Beans">


    @GetMapping("{metadataId}")
    @Operation(summary = "Get a metadata with every informations set")
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public MetadataDto getMetadata(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                   @PathVariable(name = Tenant.API_PATH) String tenantId,
                                   @Parameter(description = Metadata.API_DOC_ID_VALUE)
                                   @PathVariable(name = Metadata.API_PATH) String metadataId,
                                   @MetadataResolved Metadata metadata) {

        log.info("getMetadata tenantId:{} metadataId:{}", tenantId, metadata.getId());
        return modelMapper.map(metadata, MetadataDto.class);
    }


    @GetMapping
    @Operation(summary = "List tenant's metadata accessible to the current user", description = MetadataSortBy.Constants.API_DOC_SORT_BY_VALUES)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<MetadataRepresentation> listMetadata(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                     @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                     @TenantResolved Tenant tenant,
                                                     @PageableDefault(sort = MetadataSortBy.Constants.NAME_VALUE)
                                                     @ParameterObject Pageable pageable) {

        log.debug("listMetadata not admin page:{} pageSize:{}", pageable.getPageNumber(), pageable.getPageSize());
        Pageable innerPageable = convertSortedPageable(pageable, MetadataSortBy.class, MetadataSortBy::getColumnName);
        Page<MetadataRepresentation> result = metadataRepository
                .findAllByTenant_Id(tenant.getId(), innerPageable)
                .map(metadata -> modelMapper.map(metadata, MetadataRepresentation.class));
        log.debug("listMetadata result:{}", result.getSize());
        return result;
    }


}
