/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.*;
import coop.libriciel.ipcore.model.ipng.*;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.database.MetadataRepository;
import coop.libriciel.ipcore.services.database.SubtypeRepository;
import coop.libriciel.ipcore.services.ipng.IpngBindingsInterface;
import coop.libriciel.ipcore.services.ipng.IpngBindingsInterface.DeskboxAssociation;
import coop.libriciel.ipcore.services.ipng.IpngBindingsInterface.MetadataAssociation;
import coop.libriciel.ipcore.services.ipng.IpngBindingsInterface.TypeAssociation;
import coop.libriciel.ipcore.services.ipng.IpngServiceInterface;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;

import static coop.libriciel.ipcore.IpCoreApplication.API_V1;
import static coop.libriciel.ipcore.IpCoreApplication.HIDE_UNSTABLE_API;
import static org.springframework.data.domain.Pageable.unpaged;
import static org.springframework.http.HttpStatus.*;


@Log4j2
@RestController
@RequestMapping(API_V1 + "/tenant/{tenantId}/ipng")
public class IpngController {


    private final AuthServiceInterface authService;
    private final IpngBindingsInterface ipngBindings;
    private final IpngServiceInterface ipngService;
    private final MetadataRepository metadataRepository;
    private final ModelMapper modelMapper;
    private final SubtypeRepository subtypeRepository;


    public IpngController(AuthServiceInterface authService,
                          IpngBindingsInterface ipngBindings,
                          IpngServiceInterface ipngService,
                          MetadataRepository metadataRepository,
                          ModelMapper modelMapper,
                          SubtypeRepository subtypeRepository) {
        this.authService = authService;
        this.ipngBindings = ipngBindings;
        this.ipngService = ipngService;
        this.metadataRepository = metadataRepository;
        this.modelMapper = modelMapper;
        this.subtypeRepository = subtypeRepository;
    }


    // FIXME : Lose the tenantId duplicate parameter, that seems to be unused
    @PostMapping("tenant/{tenantIdDuplicate}/proof")
    @Operation(summary = "Post a proof", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)")
    })
    public IpngProofResult postProof(@Parameter(hidden = true)
                                     @RequestHeader("Authorization") String token,
                                     @Parameter(description = Tenant.API_DOC_ID_VALUE)
                                     @PathVariable(name = Tenant.API_PATH) String tenantId,
                                     @TenantResolved Tenant tenant,
                                     @NotNull IpngProof ipngProof) throws JsonProcessingException {

        return ipngService.postProof(ipngProof, token);
    }


    // <editor-fold desc="Directory">


    @GetMapping("entity/{ipngEntityId}")
    @Operation(summary = "get full entity data", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)")
    })
    public IpngEntity getEntity(@Parameter(description = "IPNG entity id")
                                @PathVariable String ipngEntityId) {

        return ipngService.getEntity(ipngEntityId);
    }


    @GetMapping("entity")
    @Operation(summary = "List all entities active on the IPNG directory", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)")
    })
    public List<IpngEntity> listEntities(@PathVariable(name = Tenant.API_PATH) String tenantId,
                                         @TenantResolved Tenant tenant) {
        log.debug("listEntities");
        return ipngService.listAllEntities();
    }


    @GetMapping("entity/{ipngEntityId}/deskboxProfile/{deskBoxProfileId}")
    @Operation(summary = "get one deskboxProfile", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)")
    })
    public Deskbox getDeskBoxProfile(@PathVariable String ipngEntityId, @PathVariable String deskBoxProfileId) {
        return ipngService.getDeskBox(ipngEntityId, deskBoxProfileId);
    }


    // </editor-fold desc="Directory">


    @GetMapping("metadata/latest")
    @Operation(summary = "List latest metadata list", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)")
    })
    public IpngMetadataList getLatestMetadataList(@Parameter(hidden = true) @RequestHeader("Authorization") String token) {

        return ipngService.getLatestMetadataList(token);
    }


    @GetMapping("typology/latest")
    @Operation(summary = "List latest typology list", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)")
    })
    public IpngTypology getLatestTypology(@Parameter(hidden = true) @RequestHeader("Authorization") String token) {
        log.warn("getLatestTypology - token : {}", token);
        return ipngService.getLatestTypology(token);
    }


    // <editor-fold desc="Mapping">


    @GetMapping("linked-entity")
    @Operation(summary = "List IPNG entities linked to / managed by this tenant", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)")
    })
    public List<IpngEntity> listLinkedEntities(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                               @PathVariable(name = Tenant.API_PATH) String tenantId,
                                               @TenantResolved Tenant tenant) {
        log.debug("listLinkedEntities");
        String entityId = ipngBindings.getMappedIpngEntityIdForTenant(tenant.getId());
        log.debug("listLinkedEntities - found matched entity ID : {}", entityId);
        List<IpngEntity> result = new ArrayList<>();
        if (StringUtils.isNotEmpty(entityId)) {
            result.add(ipngService.getEntity(entityId));
        }

        return result;
    }


    @PutMapping("linked-entity/{ipngEntityId}/linked-deskboxes/desk/{deskId}/deskbox/{deskboxId}")
    @Operation(summary = "Add or update a metadata to the current subtype", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given tenantId, subtypeId, or metadataId does not exist"),
    })
    public void linkDeskAndDeskbox(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                   @PathVariable(name = Tenant.API_PATH) String tenantId,
                                   @TenantResolved Tenant tenant,
                                   @Parameter(description = "IPNG entity ID")
                                   @PathVariable String ipngEntityId,
                                   @Parameter(description = "Local desk ID")
                                   @PathVariable String deskId,
                                   @Parameter(description = "IPNG deskbox")
                                   @PathVariable String deskboxId) {

        log.info("linkDeskAndDeskbox deskId:{} deskboxId:{}", deskId, deskboxId);

        // Integrity check

        if (!StringUtils.equals(this.ipngBindings.getMappedIpngEntityIdForTenant(tenant.getId()), ipngEntityId)) {
            throw new LocalizedStatusException(UNAUTHORIZED, "message.unknown_ipng_deskbox_id_for_entity");
        }

        IpngEntity ipngEntity = this.getEntity(ipngEntityId);
        if (ipngEntity.getDeskboxProfiles().stream().filter(d -> StringUtils.equals(d.getId(), deskboxId)).count() < 1) {
            throw new LocalizedStatusException(NOT_FOUND, "message.unknown_ipng_deskbox_id_for_entity");
        }

        Desk desk = authService.findDeskById(tenant.getId(), deskId);
        if (desk == null) {
            throw new LocalizedStatusException(NOT_FOUND, "message.unknown_desk_id");
        }

        Map<String, String> deskboxMapping = ipngBindings.getDeskToDeskboxMapping(tenant.getId());
        if (deskboxMapping.containsValue(deskboxId)) {
            throw new ResponseStatusException(CONFLICT, String.format("The deskbox with id %s is already mapped to a desk", deskboxId));
        }

        if (deskboxMapping.containsKey(deskId)) {
            throw new ResponseStatusException(CONFLICT, String.format("The desk with id %s is already mapped to a deskbox", deskId));
        }

        this.ipngBindings.addDeskToDeskboxMapping(tenant.getId(), deskId, deskboxId);
    }


    @DeleteMapping("linked-entity/{ipngEntityId}/linked-deskboxes/desk/{deskId}/deskbox/{deskboxId}")
    @Operation(summary = "List the desk <-> deskbox mapping for this IPNG entity", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)")
    })
    public void unlinkDeskAndDeskbox(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                     @PathVariable(name = Tenant.API_PATH) String tenantId,
                                     @TenantResolved Tenant tenant,
                                     @PathVariable String deskId,
                                     @PathVariable String deskboxId,
                                     @Parameter(hidden = true)
                                     @RequestHeader("Authorization") String token) {
        log.debug("unlinkDeskAndDeskbox");
        ipngBindings.deleteDeskToDeskboxMapping(tenant.getId(), deskId, deskboxId);
    }


    @GetMapping("linked-entity/{ipngEntityId}/linked-deskboxes")
    @Operation(summary = "List the desk <-> deskbox mapping for this IPNG entity", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)")
    })
    public List<DeskboxAssociation> getDeskboxesMapping(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                        @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                        @TenantResolved Tenant tenant,
                                                        @Parameter(description = "IPNG entity id")
                                                        @PathVariable String ipngEntityId) {
        log.debug("listLinkedDeskboxes");
        Map<String, String> idMapping = ipngBindings.getDeskToDeskboxMapping(tenant.getId());

        Set<String> deskIds = idMapping.keySet();
        Map<String, String> deskNames = authService.getDeskNames(deskIds);

        IpngEntity entity = this.ipngService.getEntity(ipngEntityId);

        List<Deskbox> deskboxList = entity.getDeskboxProfiles();

        List<DeskboxAssociation> result = new ArrayList<>();
        idMapping.forEach((k, value) -> {
            DeskRepresentation desk = new DeskRepresentation(k, deskNames.get(k));
            Optional<Deskbox> box = deskboxList.stream().filter(db -> StringUtils.equals(db.getId(), value)).findFirst();
            box.ifPresent(deskbox -> result.add(new DeskboxAssociation(desk, deskbox)));
        });

        return result;
    }


    @PostMapping("linked-entity/{ipngEntityId}/outgoing-types")
    @Operation(summary = "List the desk <-> deskbox mapping for this IPNG entity", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)")
    })
    public void addOutgoingTypesMapping(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                        @PathVariable(name = Tenant.API_PATH) String tenantId,
                                        @TenantResolved Tenant tenant,
                                        @RequestBody List<TypeAssociation> newAssociations,
                                        @Parameter(hidden = true)
                                        @RequestHeader("Authorization") String token) {
        log.debug("addOutgoingTypesMapping");

        Collection<String> typeIds = newAssociations.stream()
                .map(TypeAssociation::tenantType)
                .map(TypologyEntity::getId)
                .toList();

        Page<Subtype> mappedSubtypesListPage = this.subtypeRepository.findAllByIdIn(typeIds, unpaged());
        // TODO if size different, fail

        List<IpngType> ipngTypes = this.ipngService.getLatestTypology(token).getTypes();
        // TODO if any type not in, fail

        this.ipngBindings.addOutgoingTypeMappings(tenant.getId(), newAssociations);
    }


    @DeleteMapping("linked-entity/{ipngEntityId}/outgoing-types/subtype/{subtypeId}")
    @Operation(summary = "List the desk <-> deskbox mapping for this IPNG entity", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)")
    })
    public void deleteOutgoingTypeAssociation(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                              @PathVariable(name = Tenant.API_PATH) String tenantId,
                                              @TenantResolved Tenant tenant,
                                              @PathVariable String ipngEntityId,
                                              @PathVariable(name = Subtype.API_PATH) String subtypeId,
                                              @Parameter(hidden = true)
                                              @RequestHeader("Authorization") String token) {
        log.debug("deleteOutgoingTypeAssociation");
        ipngBindings.deleteOutgoingTypeAssociation(tenant.getId(), subtypeId);
    }


    @GetMapping("linked-entity/{ipngEntityId}/outgoing-types")
    @Operation(summary = "List the desk <-> deskbox mapping for this IPNG entity", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)")
    })
    public List<TypeAssociation> getOutgoingTypesMapping(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                         @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                         @TenantResolved Tenant tenant,
                                                         @Parameter(hidden = true)
                                                         @RequestHeader("Authorization") String token) {
        log.debug("getOutgoingTypesMapping");
        Map<String, String> idMapping = ipngBindings.getOutgoingTypeMapping(tenant.getId());

        Set<String> typeIds = idMapping.keySet();
        Page<Subtype> mappedSubtypesListPage = this.subtypeRepository.findAllByIdIn(typeIds, unpaged());
//        Map<String, Subtype> allMappedSubtypes = mappedSubtypesListPage.stream().collect(Collectors.toMap(Subtype::getId, Function.identity()));

        List<IpngType> ipngTypes = this.ipngService.getLatestTypology(token).getTypes();

        List<TypeAssociation> result = new ArrayList<>();

        mappedSubtypesListPage.stream().forEach(subtype -> {
            String subtypeId = subtype.getId();
            String ipngTypeId = idMapping.get(subtypeId);
            ipngTypes.stream()
                    .filter(t -> StringUtils.equals(t.getKey(), ipngTypeId))
                    .findFirst()
                    .ifPresent(ipngType -> result.add(new TypeAssociation(subtype, ipngType)));
        });

        return result;
    }


    @PostMapping("linked-entity/{ipngEntityId}/incoming-types")
    @Operation(summary = "List the desk <-> deskbox mapping for this IPNG entity", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)")
    })
    public void addIncomingTypesMapping(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                        @PathVariable(name = Tenant.API_PATH) String tenantId,
                                        @TenantResolved Tenant tenant,
                                        @RequestBody List<TypeAssociation> newAssociations,
                                        @Parameter(hidden = true)
                                        @RequestHeader("Authorization") String token) {
        log.debug("addIncomingTypesMapping");

        Collection<String> typeIds = newAssociations.stream()
                .map(TypeAssociation::tenantType)
                .map(TypologyEntity::getId)
                .toList();

        Page<Subtype> mappedSubtypesListPage = this.subtypeRepository.findAllByIdIn(typeIds, unpaged());
        // TODO if size different, fail

        List<IpngType> ipngTypes = this.ipngService.getLatestTypology(token).getTypes();
        // TODO if any type not in, fail

        this.ipngBindings.addIncomingTypeMappings(tenant.getId(), newAssociations);
    }


    @DeleteMapping("linked-entity/{ipngEntityId}/incoming-types/ipngType/{ipngTypeId}")
    @Operation(summary = "List the desk <-> deskbox mapping for this IPNG entity", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)")
    })
    public void deleteIncomingTypeAssociation(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                              @PathVariable(name = Tenant.API_PATH) String tenantId,
                                              @TenantResolved Tenant tenant,
                                              @PathVariable String ipngTypeId,
                                              @Parameter(hidden = true)
                                              @RequestHeader("Authorization") String token) {
        log.debug("deleteOutgoingTypeAssociation");
        ipngBindings.deleteIncomingTypeAssociation(tenant.getId(), ipngTypeId);
    }


    @GetMapping("linked-entity/{ipngEntityId}/incoming-types")
    @Operation(summary = "List the desk <-> deskbox mapping for this IPNG entity", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)")
    })
    public List<TypeAssociation> getIncomingTypesMapping(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                         @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                         @TenantResolved Tenant tenant,
                                                         @Parameter(hidden = true)
                                                         @RequestHeader("Authorization") String token) {
        log.debug("getIncomingTypesMapping");
        Map<String, String> idMapping = ipngBindings.getIncomingTypeMapping(tenant.getId());


        Set<String> mappedIpngTypes = idMapping.keySet();
        List<IpngType> ipngTypes = this.ipngService.getLatestTypology(token).getTypes()
                .parallelStream()
                .filter(t -> mappedIpngTypes.contains(t.getKey()))
                .toList();


        Collection<String> typeIds = idMapping.values();
        Page<Subtype> mappedSubtypesListPage = this.subtypeRepository.findAllByIdIn(typeIds, unpaged());

        List<TypeAssociation> result = new ArrayList<>();

        ipngTypes.forEach(ipngType -> {
            String ipngTypeId = ipngType.getKey();
            String subtypeId = idMapping.get(ipngTypeId);
            mappedSubtypesListPage.get()
                    .filter(t -> StringUtils.equals(t.getId(), subtypeId))
                    .findFirst()
                    .ifPresent(subtype -> result.add(new TypeAssociation(subtype, ipngType)));
        });

        log.debug("getIncomingTypesMapping - result : {}", result);

        return result;
    }


    @PostMapping("linked-entity/{ipngEntityId}/metadata")
    @Operation(summary = "Add metadata mapping between this tenant and this IPNG entity", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)")
    })
    public void addMetadataMapping(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                   @PathVariable(name = Tenant.API_PATH) String tenantId,
                                   @TenantResolved Tenant tenant,
                                   @RequestBody List<MetadataAssociation> newAssociations,
                                   @Parameter(hidden = true)
                                   @RequestHeader("Authorization") String token) {
        log.debug("addMetadataMapping");

        Collection<String> metadataIds = newAssociations.stream()
                .map(MetadataAssociation::tenantMetadata)
                .map(MetadataRepresentation::getId)
                .toList();

        Page<Metadata> metadataList = this.metadataRepository.findAllByTenantIdAndKeyIn(tenant.getId(), metadataIds, unpaged());
        // TODO if size different, fail

        List<IpngMetadata> ipngMetadata = this.ipngService.getLatestMetadataList(token).getMetadatas();
        // TODO if any type not in, fail

        this.ipngBindings.addMetadataMappings(tenant.getId(), newAssociations);
    }


    @DeleteMapping("linked-entity/{ipngEntityId}/metadata/{tenantMetadataKey}")
    @Operation(summary = "Delete the mapping for this tenant's metadata key", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)")
    })
    public void deleteMetadataAssociation(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                          @PathVariable(name = Tenant.API_PATH) String tenantId,
                                          @TenantResolved Tenant tenant,
                                          @PathVariable String tenantMetadataKey,
                                          @Parameter(hidden = true)
                                          @RequestHeader("Authorization") String token) {
        log.debug("deleteMetadataAssociation");
        ipngBindings.deleteMetadataAssociation(tenant.getId(), tenantMetadataKey);
    }


    @GetMapping("linked-entity/{ipngEntityId}/metadata")
    @Operation(summary = "List the full metadata mapping for this IPNG entity and tenant", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin role.\n(You might want to set it in Keycloak)")
    })
    public List<MetadataAssociation> getMetadataMapping(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                        @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                        @TenantResolved Tenant tenant,
                                                        @Parameter(hidden = true)
                                                        @RequestHeader("Authorization") String token) {
        log.debug("getMetadataMapping");
        Map<String, String> idMapping = ipngBindings.getMetadataMapping(tenant.getId());

        Set<String> tenantMetadataIds = idMapping.keySet();
        Page<MetadataRepresentation> tenantMetadata = this.metadataRepository
                .findAllByTenantIdAndKeyIn(tenant.getId(), tenantMetadataIds, unpaged())
                .map(metadata -> modelMapper.map(metadata, MetadataRepresentation.class));

        List<IpngMetadata> ipngMetadataList = this.ipngService.getLatestMetadataList(token).getMetadatas();
        List<MetadataAssociation> result = new ArrayList<>();

        tenantMetadata.forEach(metadata -> {
            String metadataKey = metadata.getKey();
            String ipngMetadataKey = idMapping.get(metadataKey);
            ipngMetadataList.stream()
                    .filter(m -> StringUtils.equals(m.getKey(), ipngMetadataKey))
                    .findFirst()
                    .ifPresent(ipngMetadata -> result.add(new MetadataAssociation(metadata, ipngMetadata)));
        });

        return result;
    }


    // </editor-fold desc="Mapping">


}
