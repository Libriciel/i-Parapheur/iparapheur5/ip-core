/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller;

import coop.libriciel.ipcore.business.template.TemplateBusinessService;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.database.TemplateType;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.templates.TemplateInfo;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.crypto.CryptoServiceInterface;
import coop.libriciel.ipcore.services.resolvers.CurrentUserResolver.CurrentUserResolved;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.RequestUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import jakarta.annotation.Nullable;

import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

import static coop.libriciel.ipcore.IpCoreApplication.API_INTERNAL;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.RequestUtils.PREAUTHORIZE_TENANT_MEMBER_OR_MORE;
import static org.apache.tomcat.util.http.fileupload.FileUploadBase.MULTIPART_FORM_DATA;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.MediaType.APPLICATION_PDF_VALUE;


@Log4j2
@RestController
@Tag(name = "template", description = "Templates access for a regular logged user")
public class TemplateController {


    // <editor-fold desc="Beans">


    private final ContentServiceInterface contentService;
    private final CryptoServiceInterface cryptoService;
    private final TemplateBusinessService templateBusinessService;


    @Autowired
    public TemplateController(ContentServiceInterface contentService, CryptoServiceInterface cryptoService, TemplateBusinessService templateBusinessService) {
        this.contentService = contentService;
        this.cryptoService = cryptoService;
        this.templateBusinessService = templateBusinessService;
    }


    // </editor-fold desc="Beans">


    @PostMapping(value = API_INTERNAL + "/tenant/{tenantId}/templates/{templateType}/example", consumes = MULTIPART_FORM_DATA)
    @Operation(summary = "Test the tenant signature template")
    @PreAuthorize(PREAUTHORIZE_TENANT_MEMBER_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200, content = @Content(mediaType = APPLICATION_PDF_VALUE)),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void testSignatureTemplate(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                      @PathVariable(name = Tenant.API_PATH) String tenantId,
                                      @TenantResolved Tenant tenant,
                                      @PathVariable TemplateType templateType,
                                      @CurrentUserResolved User currentUser,
                                      @RequestPart(required = false) MultipartFile signatureImage,
                                      boolean useSmallDoc,
                                      HttpServletResponse response) {

        DocumentBuffer customTemplate = templateBusinessService.getCustomTemplate(tenant, templateType);
        TemplateInfo templateInfo = useSmallDoc ? this.templateBusinessService.getTemplateInfo(tenantId, tenant, templateType) : null;

        String signatureImageBase64;
        if (templateType.isSignatureTemplate() && signatureImage != null) {
            try {
                signatureImageBase64 = Base64.getEncoder().encodeToString(signatureImage.getBytes());
            } catch (IOException e) {
                log.error("Error while writing the response", e);
                throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "Something went wrong reading the signature image", e);
            }
        } else if (templateType.isSignatureTemplate()) {
            signatureImageBase64 = Optional.ofNullable(currentUser.getSignatureImageContentId())
                    .filter(StringUtils::isNotEmpty)
                    .map(contentService::retrieveContentAsBase64)
                    .orElse(cryptoService.getDefaultPdfSignatureBase64());
        } else {
            signatureImageBase64 = cryptoService.getDefaultPdfSignatureBase64();
        }

        RequestUtils.writeBufferToResponse(
                executeFakeSeal(templateType, signatureImageBase64, templateInfo, customTemplate),
                response
        );

    }


    private DocumentBuffer executeFakeSeal(@NotNull TemplateType templateType,
                                           @NotNull String signatureImageBase64,
                                           @Nullable TemplateInfo templateInfo,
                                           @Nullable DocumentBuffer customTemplate) {

        if (customTemplate != null) {
            try (InputStream customTemplateStream = RequestUtils.bufferToInputStream(customTemplate)) {
                return templateBusinessService.fakeSeal(new InputStreamResource(customTemplateStream),
                        signatureImageBase64,
                        templateInfo);
            } catch (IOException e) {
                log.error("Error while writing the response", e);
                throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "Something went wrong reading the custom template", e);
            }
        }

        Resource defaultTemplate = templateBusinessService.getDefaultTemplateResource(templateType);
        return templateBusinessService.fakeSeal(defaultTemplate, signatureImageBase64, templateInfo);

    }


}
