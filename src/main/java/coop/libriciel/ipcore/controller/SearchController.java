/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller;

import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.database.Type;
import coop.libriciel.ipcore.model.database.userPreferences.FolderFilter;
import coop.libriciel.ipcore.model.search.FolderDtoSearchResult;
import coop.libriciel.ipcore.model.search.FolderSearchResult;
import coop.libriciel.ipcore.model.workflow.*;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.KeycloakSecurityUtils;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.PaginatedList;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BinaryOperator;

import static coop.libriciel.ipcore.IpCoreApplication.API_V1;
import static coop.libriciel.ipcore.IpCoreApplication.HIDE_UNSTABLE_API;
import static coop.libriciel.ipcore.model.database.userPreferences.FolderFilter.*;
import static coop.libriciel.ipcore.model.workflow.FolderSortBy.Constants.FOLDER_NAME_VALUE;
import static coop.libriciel.ipcore.utils.PaginatedList.*;
import static java.util.Collections.singleton;
import static java.util.stream.Collectors.toMap;
import static org.springframework.http.HttpStatus.*;


@Log4j2
@RestController
@RequestMapping(API_V1 + "/search")
public class SearchController {


    private final ModelMapper modelMapper;
    private final PermissionServiceInterface permissionService;
    private final WorkflowServiceInterface workflowService;


    public SearchController(ModelMapper modelMapper,
                            PermissionServiceInterface permissionService,
                            WorkflowServiceInterface workflowService) {
        this.modelMapper = modelMapper;
        this.permissionService = permissionService;
        this.workflowService = workflowService;
    }


    @GetMapping("/folders")
    @Operation(summary = "Search folders", hidden = HIDE_UNSTABLE_API)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role.\n(You might want to set it in Keycloak)"),
    })
    public PaginatedList<FolderDtoSearchResult> searchFolders(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                              @RequestParam(required = false) String tenantId,
                                                              // FILTER_PARAMS START
                                                              @Parameter(description = API_DOC_TYPE_NAME)
                                                              @RequestParam(required = false) String typeId,
                                                              @Parameter(description = API_DOC_SUBTYPE_NAME)
                                                              @RequestParam(required = false) String subtypeId,
                                                              @Parameter(description = API_DOC_FROM)
                                                              @RequestParam(required = false) Long from,
                                                              @Parameter(description = API_DOC_TO)
                                                              @RequestParam(required = false) Long to,
                                                              @Parameter(description = API_DOC_LEGACY_ID)
                                                              @RequestParam(required = false) String legacyId,
                                                              @Parameter(description = API_DOC_STATE)
                                                              @RequestParam(required = false) State state,
                                                              @Parameter(description = API_DOC_DESK_ID)
                                                              @RequestParam(required = false) String deskId,
                                                              // FILTER_PARAMS END
                                                              @Parameter(description = API_DOC_PAGE)
                                                              @RequestParam(defaultValue = API_DOC_PAGE_DEFAULT) int page,
                                                              @Parameter(description = API_DOC_PAGE_SIZE)
                                                              @RequestParam(defaultValue = API_DOC_PAGE_SIZE_DEFAULT) int pageSize,
                                                              @Parameter(description = API_DOC_SORT_BY)
                                                              @RequestParam(required = false, defaultValue = FOLDER_NAME_VALUE) FolderSortBy sortBy,
                                                              @Parameter(description = API_DOC_ASC)
                                                              @RequestParam(required = false, defaultValue = API_DOC_ASC_DEFAULT) boolean asc,
                                                              @Parameter(description = "Searching for a specific folder name")
                                                              @RequestParam(required = false) String searchTerm) {

        log.info("searchFolders tenant:{} page:{} pageSize:{}", tenantId, page, pageSize);

        if (pageSize > MAX_PAGE_SIZE) {
            log.error("Request specify a pageSize of {}, which is superior to the max value: {}", pageSize, MAX_PAGE_SIZE);
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.exceeded_max_page_size", MAX_PAGE_SIZE);
        }

        // Integrity check

        if (deskId != null) {
            if (StringUtils.isEmpty(tenantId)) {
                throw new LocalizedStatusException(NOT_FOUND, "message.unknown_desk_id");
            }
            if (!permissionService.currentUserHasViewingRightOnSomeDeskIn(tenantId, singleton(deskId), null, null)) {
                throw new LocalizedStatusException(FORBIDDEN, "message.you_can_t_view_this_desk");
            }
        }

        // Prepare the request parameters,
        // Computing default values if needed

        Set<String> targetTenantIds = Optional.ofNullable(tenantId)
                .filter(StringUtils::isNotEmpty)
                .map(Collections::singleton)
                .orElseGet(KeycloakSecurityUtils::getCurrentUserTenantIds);

        List<String> targetDeskIds = Optional.ofNullable(deskId)
                .map(Collections::singletonList)
                .orElseGet(permissionService::getCurrentUserViewableDeskIds);

        log.debug("searchFolders targetDeskIds:{}", targetDeskIds);
        log.debug("searchFolders targetTenantIds:{}", targetTenantIds);

        final FolderFilter dummyFilter = new FolderFilter(
                Optional.ofNullable(typeId).map(Type::new).orElse(null),
                Optional.ofNullable(subtypeId).map(Subtype::new).orElse(null),
                searchTerm,
                legacyId
        );

        // Actual search

        PaginatedList<FolderSearchResult> result = new PaginatedList<>();
        result.setData(new ArrayList<>());

        AtomicLong totalCount = new AtomicLong(0);

        targetTenantIds.stream()
                .takeWhile(id -> result.getData().size() < pageSize)
                .map(tId -> {

                    PaginatedList<? extends Folder> tempResult = workflowService.listFoldersForDesks(
                            tId,
                            page,
                            pageSize - result.getData().size(),
                            sortBy,
                            dummyFilter,
                            asc,
                            targetDeskIds,
                            null,
                            null,
                            state
                    );

                    totalCount.addAndGet(tempResult.getTotal());

                    log.trace("searchFolders tempResult:{}", tempResult);

                    return tempResult.getData()
                            .stream()
                            // TODO externalize this to a Utils
                            // Remove duplicates using a hashmap
                            .collect(toMap(Folder::getId, folder -> folder, folderMergeFnct()))
                            .values()
                            .stream()
                            .map(folder -> new FolderSearchResult(folder, tId))
                            .toList();
                })
                .forEach(folderList -> result.getData().addAll(folderList));

        log.info("listFolders result:{}", result.getData().size());
        return new PaginatedList<>(
                result.getData().stream()
                        .map(searchResult -> new FolderDtoSearchResult(
                                modelMapper.map(searchResult.getFolder(), FolderDto.class),
                                searchResult.getTenantId())
                        ).toList(),
                result.getPage(),
                result.getPageSize(),
                totalCount.get()
        );
    }


    /**
     * "Merge" folders that have identical ids.
     * Should be called in the context of folders built from a single task, where each of said folders
     * is in fact one of the parallel tasks currently pending.
     *
     * @return
     */
    private static BinaryOperator<Folder> folderMergeFnct() {
        return ((folder1, folder2) -> {
            List<Task> mergedStepList = new ArrayList<>();
            mergedStepList.addAll(folder1.getStepList());
            mergedStepList.addAll(folder2.getStepList());
            folder1.setStepList(mergedStepList);
            return folder1;
        });
    }

}


