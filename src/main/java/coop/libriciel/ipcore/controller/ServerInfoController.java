/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller;

import coop.libriciel.ipcore.business.signatureproof.SignatureReportBusinessService;
import coop.libriciel.ipcore.model.auth.ServerInfoDto;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.database.ServerInfoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import static coop.libriciel.ipcore.IpCoreApplication.API_INTERNAL;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.RequestUtils.PREAUTHORIZE_IS_AUTHENTICATED;


@Log4j2
@RestController
@Tag(name = "server-info", description = "Server infos access for a regular logged user")
public class ServerInfoController {


    // <editor-fold desc="Beans">


    private final AuthServiceInterface authService;
    private final ServerInfoService serverInfoService;
    private final SignatureReportBusinessService signatureReportBusinessService;


    @Autowired
    public ServerInfoController(AuthServiceInterface authService,
                                ServerInfoService serverInfoService,
                                SignatureReportBusinessService signatureReportBusinessService) {
        this.authService = authService;
        this.serverInfoService = serverInfoService;
        this.signatureReportBusinessService = signatureReportBusinessService;
    }


    // </editor-fold desc="Beans">


    @GetMapping(API_INTERNAL + "/server-info")
    @Operation(summary = "Get server information: GDPR definition, password rules and signature validation service activation status")
    @PreAuthorize(PREAUTHORIZE_IS_AUTHENTICATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public ServerInfoDto getServerInfo() {
        ServerInfoDto serverInfoDto = new ServerInfoDto();
        serverInfoDto.setGdprInformationDetailsDto(serverInfoService.getGdprInformationDetails());
        serverInfoDto.setPasswordPolicies(authService.getPasswordPolicies());
        serverInfoDto.setSignatureValidationServiceActivated(this.signatureReportBusinessService.isSignatureValidationServiceActivated());
        return serverInfoDto;
    }

}
