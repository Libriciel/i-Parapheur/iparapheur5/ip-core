/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import coop.libriciel.ipcore.business.typology.TypologyBusinessService;
import coop.libriciel.ipcore.business.workflow.FolderBusinessService;
import coop.libriciel.ipcore.business.workflow.WorkflowBusinessService;
import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.auth.requests.UserRepresentation;
import coop.libriciel.ipcore.model.content.CreateFolderRequest;
import coop.libriciel.ipcore.model.crypto.SignatureFormat;
import coop.libriciel.ipcore.model.database.ExternalSignatureConfig;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.database.Type;
import coop.libriciel.ipcore.model.externalsignature.ExternalSignatureParams;
import coop.libriciel.ipcore.model.externalsignature.Status;
import coop.libriciel.ipcore.model.ipng.*;
import coop.libriciel.ipcore.model.workflow.*;
import coop.libriciel.ipcore.model.workflow.requests.SealTaskParams;
import coop.libriciel.ipcore.model.workflow.requests.SignatureTaskParams;
import coop.libriciel.ipcore.model.workflow.requests.SimpleTaskParams;
import coop.libriciel.ipcore.model.workflow.requests.TransferTaskParams;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.crypto.CryptoServiceInterface;
import coop.libriciel.ipcore.services.externalsignature.ExternalSignatureInterface;
import coop.libriciel.ipcore.services.groovy.GroovyService;
import coop.libriciel.ipcore.services.ipng.IpngBindingsInterface;
import coop.libriciel.ipcore.services.ipng.IpngServiceInterface;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.services.redis.RedisMessagePublisherService;
import coop.libriciel.ipcore.services.resolvers.CurrentUserResolver.CurrentUserResolved;
import coop.libriciel.ipcore.services.resolvers.DeskResolver.DeskResolved;
import coop.libriciel.ipcore.services.resolvers.ExternalSignatureConfigResolver.ExternalSignatureConfigResolved;
import coop.libriciel.ipcore.services.resolvers.FolderResolver.FolderResolved;
import coop.libriciel.ipcore.services.resolvers.TaskResolver.TaskResolved;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.services.resolvers.WorkflowDefinitionResolver.WorkflowDefinitionResolved;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.KeycloakSecurityUtils;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.pdfbox.Loader;
import org.apache.pdfbox.io.RandomAccessReadBuffer;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.util.*;

import static coop.libriciel.ipcore.IpCoreApplication.*;
import static coop.libriciel.ipcore.configuration.RedisConfigurer.*;
import static coop.libriciel.ipcore.model.workflow.Action.*;
import static coop.libriciel.ipcore.model.workflow.State.*;
import static coop.libriciel.ipcore.services.resolvers.FolderResolver.FolderResolved.Permission.CURRENT_DESK;
import static coop.libriciel.ipcore.services.resolvers.FolderResolver.FolderResolved.Permission.WORKFLOW_DESK;
import static coop.libriciel.ipcore.services.workflow.IpWorkflowService.META_PREMIS_NODE_ID;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.RequestUtils.PREAUTHORIZE_TENANT_MEMBER_OR_MORE;
import static coop.libriciel.ipcore.utils.RequestUtils.returnRedactedResponse;
import static java.util.Arrays.asList;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static java.util.Optional.ofNullable;
import static java.util.UUID.randomUUID;
import static org.apache.commons.lang3.StringUtils.length;
import static org.apache.tomcat.util.http.fileupload.FileUploadBase.MULTIPART_FORM_DATA;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.*;


@Log4j2
@RestController
@Tag(name = "workflow", description = "Workflow access for a regular logged user")
public class WorkflowController {


    // <editor-fold desc="Beans">


    private final AuthServiceInterface authService;
    private final ContentServiceInterface contentService;
    private final CryptoServiceInterface cryptoService;
    private final ExternalSignatureInterface externalSignatureService;
    private final FolderBusinessService folderBusinessService;
    private final GroovyService groovyService;
    private final IpngBindingsInterface ipngBindings;
    private final IpngServiceInterface ipngService;
    private final ModelMapper modelMapper;
    private final ObjectMapper objectMapper;
    private final PermissionServiceInterface permissionService;
    private final RedisMessagePublisherService redisMessagePublisher;
    private final StatsServiceInterface statsService;
    private final TypologyBusinessService typologyBusinessService;
    private final WorkflowBusinessService workflowBusinessService;
    private final WorkflowServiceInterface workflowService;


    @Autowired
    public WorkflowController(AuthServiceInterface authService,
                              ContentServiceInterface contentService,
                              CryptoServiceInterface cryptoService,
                              ExternalSignatureInterface externalSignatureService,
                              FolderBusinessService folderBusinessService,
                              GroovyService groovyService,
                              IpngBindingsInterface ipngBindings,
                              IpngServiceInterface ipngService,
                              ModelMapper modelMapper,
                              ObjectMapper objectMapper,
                              PermissionServiceInterface permissionService,
                              RedisMessagePublisherService redisMessagePublisher,
                              TypologyBusinessService typologyBusinessService,
                              StatsServiceInterface statsService,
                              WorkflowBusinessService workflowBusinessService,
                              WorkflowServiceInterface workflowService) {
        this.authService = authService;
        this.contentService = contentService;
        this.cryptoService = cryptoService;
        this.externalSignatureService = externalSignatureService;
        this.folderBusinessService = folderBusinessService;
        this.groovyService = groovyService;
        this.ipngBindings = ipngBindings;
        this.ipngService = ipngService;
        this.modelMapper = modelMapper;
        this.objectMapper = objectMapper;
        this.permissionService = permissionService;
        this.redisMessagePublisher = redisMessagePublisher;
        this.statsService = statsService;
        this.typologyBusinessService = typologyBusinessService;
        this.workflowBusinessService = workflowBusinessService;
        this.workflowService = workflowService;
    }


    // </editor-fold desc="Beans">


    // <editor-fold desc="Requests">


    @PostMapping(value = API_INTERNAL + "/tenant/{tenantId}/desk/{deskId}/evaluate-workflow-selection-script")
    @PreAuthorize(PREAUTHORIZE_TENANT_MEMBER_OR_MORE)
    @Operation(summary = "Returns the evaluated workflow definition, using current parent desks, and given metadata on a possible selection script")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public WorkflowDefinitionDto evaluateWorkflowSelectionScript(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                                 @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                                 @TenantResolved Tenant tenant,
                                                                 @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                                                 @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                                                 @DeskResolved Desk desk,
                                                                 @Parameter(description = "Params")
                                                                 @RequestBody CreateFolderRequest draftFolderParams) {

        log.debug("evaluateWorkflowSelectionScript deskId:{} name:{} typeId:{} subtypeId:{}",
                desk.getId(), draftFolderParams.getName(), draftFolderParams.getTypeId(), draftFolderParams.getSubtypeId());

        Folder mockFolder = new Folder();
        mockFolder.setOriginDesk(new DeskRepresentation(desk.getId(), desk.getName()));
        mockFolder.setMetadata(new HashMap<>());
        mockFolder.setType(ofNullable(draftFolderParams.getTypeId()).map(Type::new).orElse(null));
        mockFolder.setSubtype(ofNullable(draftFolderParams.getSubtypeId()).map(Subtype::new).orElse(null));

        typologyBusinessService.updateTypology(tenant.getId(), singletonList(mockFolder), true);
        workflowBusinessService.checkAndCopyMetadataFromParams(draftFolderParams, mockFolder);

        WorkflowDefinition result = groovyService.computeSelectionScriptForFolder(tenantId, mockFolder)
                .map(scriptResult -> workflowBusinessService.computeWorkflowDefinitionFromScriptResult(scriptResult, tenantId, desk.getId()))
                .orElseThrow(() -> new LocalizedStatusException(BAD_REQUEST, "message.no_result_from_workflow_selection_script"));

        log.debug("evaluateWorkflowSelectionScript result deskIds:{}", result.getSteps().stream().map(StepDefinition::getValidatingDesks).toList());
        return workflowBusinessService.convertToDto(result);
    }


    @PostMapping(value = API_V1 + "/tenant/{tenantId}/desk/{deskId}/draft", consumes = MULTIPART_FORM_DATA)
    // @PreAuthorize("hasRole('" + DESK_ROLE_PREFIX + "' + #deskId)")
    @Operation(summary = "Create a draft folder")
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_201),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_406, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public FolderDto createDraftFolder(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                       @PathVariable(name = Tenant.API_PATH) String tenantId,
                                       @TenantResolved Tenant tenant,
                                       @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                       @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                       @DeskResolved Desk desk,
                                       @Parameter(description = "Main files")
                                       @RequestPart List<MultipartFile> mainFiles,
                                       @Parameter(description = "Annexe files")
                                       @RequestPart(required = false) List<MultipartFile> annexeFiles,
                                       @Parameter(description = "Detached signature files")
                                       @RequestPart(required = false) List<MultipartFile> detachedSignatures,
                                       @Parameter(description = "Parameters")
                                       @RequestPart @Valid CreateFolderRequest createFolderRequest) {

        folderBusinessService.checkCreateFolderLimitations(tenant);
        Folder folder = folderBusinessService.createDraftFolder(tenant, desk, mainFiles, annexeFiles, detachedSignatures, createFolderRequest);

        // Signature report

        try {
            Map<String, String> message = new HashMap<>();
            message.put(TENANT_ID_KEY, tenantId);
            message.put(FOLDER_ID_KEY, folder.getId());
            redisMessagePublisher.publish(SIGNATURE_PROOF_GENERATION_QUEUE, objectMapper.writeValueAsString(message));
        } catch (Exception e) {
            log.warn("createDraftFolder - error during publishing signature report generation with redis: {}", e.getMessage());
        }

        return returnRedactedResponse(modelMapper, folder, FolderDto::new);
    }


    @PutMapping(API_STANDARD_V1 + "/tenant/{tenantId}/desk/{deskId}/folder/{folderId}/task/{taskId}/start")
    @Operation(summary = "Start", description = "Start the given Folder")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_407, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void start(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                      @PathVariable(name = Tenant.API_PATH) String tenantId,
                      @TenantResolved Tenant tenant,
                      @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                      @PathVariable String deskId,
                      @Parameter(description = Folder.API_DOC_ID_VALUE)
                      @PathVariable(name = Folder.API_PATH) String folderId,
                      @FolderResolved(permission = WORKFLOW_DESK) Folder folder,
                      @PathVariable(name = Task.API_PATH) String taskId, // TODO : use this, and prevent a useless find in the sub methods
                      @RequestBody @Valid SimpleTaskParams simpleTaskParams) {

        log.info("startWorkflow deskId:{} folderId:{}", deskId, folderId);

        // Integrity check

        if (folder.getStepList().stream()
                .filter(s -> s.getAction() == START)
                .anyMatch(s -> s.getState() != CURRENT)) {
            log.warn("Trying to start a workflow already started, abort");
            throw new LocalizedStatusException(CONFLICT, "message.this_workflow_has_already_been_started");
        }

        if (!StringUtils.equals(deskId, folder.getOriginDesk().getId())) {
            log.warn("Trying to start a workflow from a desk that is not the origin desk, abort");
            throw new LocalizedStatusException(FORBIDDEN, "message.you_can_t_start_this_folder_from_this_desk");
        }

        workflowBusinessService.doStartWorkflow(tenantId, folder, deskId, simpleTaskParams);
        workflowBusinessService.disableDeleteCapabilityOnCurrentDocuments(folder);
    }


    @PutMapping(API_STANDARD_V1 + "/tenant/{tenantId}/desk/{deskId}/folder/{folderId}/task/{taskId}/visa")
    @Operation(summary = "Visa", description = "Perform a straightforward Visa on a Task")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void visa(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                     @PathVariable(name = Tenant.API_PATH) String tenantId,
                     @TenantResolved Tenant tenant,
                     @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                     @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                     @DeskResolved Desk desk,
                     @Parameter(description = Folder.API_DOC_ID_VALUE)
                     @PathVariable(name = Folder.API_PATH) String folderId,
                     @FolderResolved Folder folder,
                     @Parameter(description = Task.API_DOC_ID_VALUE)
                     @PathVariable(name = Task.API_PATH) String taskId,
                     @TaskResolved Task task,
                     @Parameter(name = SimpleTaskParams.API_NAME, description = SimpleTaskParams.API_VALUE, required = true)
                     @RequestBody @Valid SimpleTaskParams performTaskRequest) {
        performSimpleAction(tenant, desk, folder, task, VISA, performTaskRequest, null);
    }


    @PutMapping(API_STANDARD_V1 + "/tenant/{tenantId}/desk/{deskId}/folder/{folderId}/task/{taskId}/second_opinion")
    @Operation(summary = "Second opinion", description = "Perform a straightforward Second opinion on a Task")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void secondOpinion(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                              @PathVariable(name = Tenant.API_PATH) String tenantId,
                              @TenantResolved Tenant tenant,
                              @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                              @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                              @DeskResolved Desk desk,
                              @Parameter(description = Folder.API_DOC_ID_VALUE)
                              @PathVariable(name = Folder.API_PATH) String folderId,
                              @FolderResolved Folder folder,
                              @Parameter(description = Task.API_DOC_ID_VALUE)
                              @PathVariable(name = Task.API_PATH) String taskId,
                              @TaskResolved Task task,
                              @Parameter(name = SimpleTaskParams.API_NAME, description = SimpleTaskParams.API_VALUE, required = true)
                              @RequestBody @Valid SimpleTaskParams performTaskRequest) {
        performSimpleAction(tenant, desk, folder, task, SECOND_OPINION, performTaskRequest, null);
    }


    @PutMapping(API_V1 + "/tenant/{tenantId}/desk/{deskId}/folder/{folderId}/task/{taskId}/external_signature")
    @Operation(summary = "External signature", description = "Perform an external signature")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void requestExternalSignatureProcedure(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                  @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                  @TenantResolved Tenant tenant,
                                                  @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                                  @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                                  @DeskResolved Desk desk,
                                                  @Parameter(description = Folder.API_DOC_ID_VALUE)
                                                  @PathVariable(name = Folder.API_PATH) String folderId,
                                                  @FolderResolved(permission = CURRENT_DESK) Folder folder,
                                                  @Parameter(description = Task.API_DOC_ID_VALUE)
                                                  @PathVariable(name = Task.API_PATH) String taskId,
                                                  @TaskResolved Task task,
                                                  @Parameter(description = "body", required = true)
                                                  @CurrentUserResolved User user,
                                                  @RequestBody @Valid ExternalSignatureParams externalSignatureParams) {

        if (task.getAction() != EXTERNAL_SIGNATURE) {
            log.warn("Trying to perform a {} action on an EXTERNAL_SIGNATURE task, abort", task.getAction());
            throw new LocalizedStatusException(BAD_REQUEST, "message.you_can_t_perform_a_x_action_on_a_y_task", EXTERNAL_SIGNATURE, task.getAction());
        }

        workflowBusinessService.assertCurrentUserCanPerformTask(tenant.getId(),
                task,
                user,
                folder.getType().getId(),
                folder.getSubtype().getId(),
                desk.getId());

        workflowBusinessService.performExternalSignature(tenant, desk, folder, task, user, externalSignatureParams);

        workflowBusinessService.triggerPostTaskActions(tenant.getId(), desk, folder, EXTERNAL_SIGNATURE);
    }


    @PutMapping(API_V1 + "/tenant/{tenantId}/desk/{deskId}/folder/{folderId}/task/{taskId}/config/{configId}/external_signature/force")
    @Operation(summary = "External signature", description = "Perform an external signature")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void finalizeExternalSignature(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                          @PathVariable(name = Tenant.API_PATH) String tenantId,
                                          @TenantResolved Tenant tenant,
                                          @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                          @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                          @DeskResolved Desk desk,
                                          @Parameter(description = Folder.API_DOC_ID_VALUE)
                                          @PathVariable(name = Folder.API_PATH) String folderId,
                                          @FolderResolved(permission = CURRENT_DESK) Folder folder,
                                          @Parameter(description = Task.API_DOC_ID_VALUE)
                                          @PathVariable(name = Task.API_PATH) String taskId,
                                          @TaskResolved Task task,
                                          @CurrentUserResolved User user,
                                          @PathVariable(name = ExternalSignatureConfig.API_PATH) String externalSignatureConfigId,
                                          @ExternalSignatureConfigResolved ExternalSignatureConfig externalSignatureConfig) {

        if (task.getAction() != EXTERNAL_SIGNATURE) {
            log.warn("Trying to perform a {} action on an EXTERNAL_SIGNATURE task, abort", task.getAction());
            throw new LocalizedStatusException(BAD_REQUEST, "message.you_can_t_perform_a_x_action_on_a_y_task", EXTERNAL_SIGNATURE, task.getAction());
        }

        Status status = this.externalSignatureService.getProcedureStatus(externalSignatureConfig, task.getExternalSignatureProcedureId());
        if (status != Status.SIGNED) {
            log.warn("Trying to finalize an EXTERNAL_SIGNATURE step, but it has not been signed yet, abort");
            throw new LocalizedStatusException(BAD_REQUEST, "message.you_can_t_force_an_unsigned_external_signature_task");
        }

        this.externalSignatureService.updateFilesAndPerformTask(externalSignatureConfig, user, task.getExternalSignatureProcedureId());

        // Signature report

        try {
            Map<String, String> message = new HashMap<>();
            message.put(TENANT_ID_KEY, tenantId);
            message.put(FOLDER_ID_KEY, folderId);
            redisMessagePublisher.publish(SIGNATURE_PROOF_GENERATION_QUEUE, objectMapper.writeValueAsString(message));
        } catch (Exception e) {
            log.warn("signature - error during publishing signature report generation with redis : {}", e.getMessage());
        }
    }


    @PutMapping(API_V1 + "/tenant/{tenantId}/desk/{deskId}/folder/{folderId}/task/{taskId}/ipng")
    @Operation(summary = "ipng", description = "Perform an ipng request", hidden = HIDE_UNSTABLE_API)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Action performed"),
            @ApiResponse(responseCode = "400", description = "Wrong action attempt"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role of given task.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given taskId does not exist")
    })
    public void requestIpng(@RequestHeader("Authorization") String token,
                            @Parameter(description = Tenant.API_DOC_ID_VALUE)
                            @PathVariable(name = Tenant.API_PATH) String tenantId,
                            @TenantResolved Tenant tenant,
                            @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                            @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                            @DeskResolved Desk desk,
                            @Parameter(description = Folder.API_DOC_ID_VALUE)
                            @PathVariable(name = Folder.API_PATH) String folderId,
                            @FolderResolved(permission = CURRENT_DESK) Folder folder,
                            @Parameter(description = Task.API_DOC_ID_VALUE)
                            @PathVariable(name = Task.API_PATH) String taskId,
                            @TaskResolved Task task,
                            @Parameter(description = "body", required = true)
                            @RequestBody IpngParams ipngParams) {

        log.info("requestIpng - ipngParams : {}", ipngParams);

        if (task.getAction() != IPNG) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.you_can_t_perform_a_x_action_on_a_y_task", IPNG, task.getAction());
        }

        performIpng(tenant, desk, folder, task, ipngParams, token);
    }


    @PutMapping(API_V1 + "/tenant/{tenantId}/desk/{deskId}/folder/{folderId}/task/{taskId}/reject")
    @Operation(summary = "Reject", description = "Perform a straightforward Reject on a Task")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void reject(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                       @PathVariable(name = Tenant.API_PATH) String tenantId,
                       @TenantResolved Tenant tenant,
                       @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                       @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                       @DeskResolved Desk desk,
                       @Parameter(description = Folder.API_DOC_ID_VALUE)
                       @PathVariable(name = Folder.API_PATH) String folderId,
                       @FolderResolved Folder folder,
                       @Parameter(description = Task.API_DOC_ID_VALUE)
                       @PathVariable(name = Task.API_PATH) String taskId,
                       @TaskResolved Task task,
                       @Parameter(name = SimpleTaskParams.API_NAME, description = SimpleTaskParams.API_VALUE, required = true)
                       @RequestBody @Valid SimpleTaskParams performTaskRequest) {

        if (length(performTaskRequest.getPublicAnnotation()) < 3) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.rejecting_a_tasks_need_a_public_annotation_longer_than_3_chars");
        }

        String userId = KeycloakSecurityUtils.getCurrentSessionUserId();
        log.debug("getFolder current user:{}", userId);

        workflowBusinessService.handleExternalTasksRejection(folder, task);
        performSimpleAction(tenant, desk, folder, task, REJECT, performTaskRequest, null);
    }


    @PutMapping(API_STANDARD_V1 + "/tenant/{tenantId}/desk/{deskId}/folder/{folderId}/task/{taskId}/bypass")
    @Operation(summary = "Bypass", description = "Force an external task stuck in a waiting state.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void bypass(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                       @PathVariable(name = Tenant.API_PATH) String tenantId,
                       @TenantResolved Tenant tenant,
                       @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                       @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                       @DeskResolved Desk desk,
                       @Parameter(description = Folder.API_DOC_ID_VALUE)
                       @PathVariable(name = Folder.API_PATH) String folderId,
                       @FolderResolved Folder folder,
                       @Parameter(description = Task.API_DOC_ID_VALUE)
                       @PathVariable(name = Task.API_PATH) String taskId,
                       @TaskResolved Task task) {

        String userId = KeycloakSecurityUtils.getCurrentSessionUserId();
        log.debug("getFolder current user:{}", userId);

        workflowBusinessService.handleExternalTasksRejection(folder, task);
        performSimpleAction(tenant, desk, folder, task, BYPASS, new SimpleTaskParams(), null);
    }


    @PutMapping(API_V1 + "/tenant/{tenantId}/desk/{deskId}/folder/{folderId}/task/{taskId}/transfer")
    @Operation(
            summary = "Transfer",
            description = """
                          Transfers the task to another desk.
                          This action adds a step to the workflow history.
                          """
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void transfer(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                         @PathVariable(name = Tenant.API_PATH) String tenantId,
                         @TenantResolved Tenant tenant,
                         @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                         @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                         @DeskResolved Desk desk,
                         @Parameter(description = Folder.API_DOC_ID_VALUE)
                         @PathVariable(name = Folder.API_PATH) String folderId,
                         @FolderResolved Folder folder,
                         @Parameter(description = Task.API_DOC_ID_VALUE)
                         @PathVariable(name = Task.API_PATH) String taskId,
                         @TaskResolved Task task,
                         @Parameter(
                                 name = "body",
                                 description = "Target desk ID that will have to perform the action.",
                                 required = true)
                         @RequestBody @Valid TransferTaskParams transferTaskParams) {
        performSimpleAction(tenant, desk, folder, task, TRANSFER, transferTaskParams, transferTaskParams.getTargetDeskId());
    }


    @PutMapping(API_V1 + "/tenant/{tenantId}/desk/{deskId}/folder/{folderId}/task/{taskId}/ask_second_opinion")
    @Operation(
            summary = "Ask second opinion",
            description = """
                          Adds a simple visa to another desk.
                          This action adds a step to the workflow history.
                          
                          Once this second opinion is performed, the task will return to the original desk, leaving the final choice to the original user.
                          """
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void askSecondOpinion(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                 @PathVariable(name = Tenant.API_PATH) String tenantId,
                                 @TenantResolved Tenant tenant,
                                 @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                 @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                 @DeskResolved Desk desk,
                                 @Parameter(description = Folder.API_DOC_ID_VALUE)
                                 @PathVariable(name = Folder.API_PATH) String folderId,
                                 @FolderResolved Folder folder,
                                 @Parameter(description = Task.API_DOC_ID_VALUE)
                                 @PathVariable(name = Task.API_PATH) String taskId,
                                 @TaskResolved Task task,
                                 @Parameter(
                                         name = "body",
                                         description = "Target desk ID that will have to perform the action.",
                                         required = true)
                                 @RequestBody @Valid TransferTaskParams transferTaskParams) {
        performSimpleAction(tenant, desk, folder, task, ASK_SECOND_OPINION, transferTaskParams, transferTaskParams.getTargetDeskId());
    }


    @PutMapping(API_STANDARD_V1 + "/tenant/{tenantId}/desk/{deskId}/folder/{folderId}/task/{taskId}/undo")
    @Operation(summary = "Undo", description = "Rollback the action to the previous state.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void undo(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                     @PathVariable(name = Tenant.API_PATH) String tenantId,
                     @TenantResolved Tenant tenant,
                     @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                     @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                     @DeskResolved Desk desk,
                     @Parameter(description = Folder.API_DOC_ID_VALUE)
                     @PathVariable(name = Folder.API_PATH) String folderId,
                     @FolderResolved(permission = CURRENT_DESK) Folder folder,
                     @Parameter(description = Task.API_DOC_ID_VALUE)
                     @PathVariable(name = Task.API_PATH) String taskId,
                     @TaskResolved Task task) {

        String userId = KeycloakSecurityUtils.getCurrentSessionUserId();
        log.debug("getFolder current user:{}", userId);

        workflowBusinessService.handleExternalTasksRejection(folder, task);
        performSimpleAction(tenant, desk, folder, task, UNDO, new SimpleTaskParams(), null);
    }


    @PutMapping(API_V1 + "/tenant/{tenantId}/desk/{deskId}/folder/{folderId}/task/{taskId}/recycle")
    @Operation(summary = "Recycle", description = "Returns a rejected folder in its draft task.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void recycle(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                        @PathVariable(name = Tenant.API_PATH) String tenantId,
                        @TenantResolved Tenant tenant,
                        @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                        @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                        @DeskResolved Desk desk,
                        @Parameter(description = Folder.API_DOC_ID_VALUE)
                        @PathVariable(name = Folder.API_PATH) String folderId,
                        @FolderResolved Folder folder,
                        @Parameter(description = Task.API_DOC_ID_VALUE)
                        @PathVariable(name = Task.API_PATH) String taskId,
                        @TaskResolved Task task,
                        @Parameter(name = SimpleTaskParams.API_NAME, description = SimpleTaskParams.API_VALUE, required = true)
                        @RequestBody @Valid SimpleTaskParams simpleTaskParams) {

        // Integrity checks

        if (task.getAction() != DELETE) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.you_can_t_perform_a_x_action_on_a_y_task", RECYCLE, task.getAction());
        }

        // Action

        performSimpleAction(tenant, desk, folder, task, RECYCLE, simpleTaskParams, null);
    }


    @PutMapping(API_STANDARD_V1 + "/tenant/{tenantId}/desk/{deskId}/folder/{folderId}/task/{taskId}/seal")
    @Operation(summary = "Seal", description = "Perform a Seal on a Task.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void seal(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                     @PathVariable(name = Tenant.API_PATH) String tenantId,
                     @TenantResolved Tenant tenant,
                     @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                     @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                     @DeskResolved Desk desk,
                     @Parameter(description = Folder.API_DOC_ID_VALUE)
                     @PathVariable(name = Folder.API_PATH) String folderId,
                     @FolderResolved(permission = CURRENT_DESK) Folder folder,
                     @Parameter(description = Task.API_DOC_ID_VALUE)
                     @PathVariable(name = Task.API_PATH) String taskId,
                     @TaskResolved Task task,
                     @CurrentUserResolved User user,
                     @Parameter(name = SimpleTaskParams.API_NAME, description = SimpleTaskParams.API_VALUE, required = true)
                     @RequestBody @Valid SealTaskParams sealTaskParams) {

        if (task.getAction() != SEAL) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.you_can_t_perform_a_x_action_on_a_y_task", SEAL, task.getAction());
        }

        workflowBusinessService.assertCurrentUserCanPerformTask(tenant.getId(),
                task,
                user,
                folder.getType().getId(),
                folder.getSubtype().getId(),
                desk.getId());

        workflowBusinessService.performSeal(tenant, desk, folder, task, user, sealTaskParams);

        // Signature report

        try {
            Map<String, String> message = new HashMap<>();
            message.put(TENANT_ID_KEY, tenantId);
            message.put(FOLDER_ID_KEY, folderId);
            redisMessagePublisher.publish(SIGNATURE_PROOF_GENERATION_QUEUE, objectMapper.writeValueAsString(message));
        } catch (Exception e) {
            log.warn("seal - error during publishing signature report generation with redis : {}", e.getMessage());
        }

        workflowBusinessService.triggerPostTaskActions(tenant.getId(), desk, folder, SEAL);
    }


    @PutMapping(API_V1 + "/tenant/{tenantId}/desk/{deskId}/folder/{folderId}/task/{taskId}/paper_signature")
    @Operation(summary = "Paper signature", description = "Bypass the current Signature Task with a straightforward Visa.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_406, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void paperSignature(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                               @PathVariable(name = Tenant.API_PATH) String tenantId,
                               @TenantResolved Tenant tenant,
                               @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                               @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                               @DeskResolved Desk desk,
                               @Parameter(description = Folder.API_DOC_ID_VALUE)
                               @PathVariable(name = Folder.API_PATH) String folderId,
                               @FolderResolved(permission = CURRENT_DESK) Folder folder,
                               @Parameter(description = Task.API_DOC_ID_VALUE)
                               @PathVariable(name = Task.API_PATH) String taskId,
                               @TaskResolved Task task,
                               @Parameter(name = SimpleTaskParams.API_NAME, description = SimpleTaskParams.API_VALUE, required = true)
                               @RequestBody @Valid SimpleTaskParams simpleTaskParams) {

        // Integrity check

        if (task.getAction() != SIGNATURE) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.you_can_t_perform_a_x_action_on_a_y_task", PAPER_SIGNATURE, task.getAction());
        }

        typologyBusinessService.updateTypology(tenant.getId(), singletonList(folder), false);

        if (folder.getSubtype().isDigitalSignatureMandatory()) {
            throw new ResponseStatusException(NOT_ACCEPTABLE, "Subtype parameters forbid paper-signing this folder");
        }

        // Signature

        performSimpleAction(tenant, desk, folder, task, PAPER_SIGNATURE, simpleTaskParams, null);
    }


    @PutMapping(API_V1 + "/tenant/{tenantId}/desk/{deskId}/folder/{folderId}/task/{taskId}/sign")
    @Operation(summary = "Signature", description = "Perform a straightforward Signature on a Task.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_406, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void signature(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                          @PathVariable(name = Tenant.API_PATH) String tenantId,
                          @TenantResolved Tenant tenant,
                          @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                          @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                          @DeskResolved Desk desk,
                          @Parameter(description = Folder.API_DOC_ID_VALUE)
                          @PathVariable(name = Folder.API_PATH) String folderId,
                          @FolderResolved(permission = CURRENT_DESK) Folder folder,
                          @Parameter(description = Task.API_DOC_ID_VALUE)
                          @PathVariable(name = Task.API_PATH) String taskId,
                          @TaskResolved Task task,
                          @Parameter(description = """
                                                   The publicAnnotation is visible to any users, and will be stored in task history.
                                                   
                                                   The privateAnnotation will only be sent to the workflow's next user.
                                                   It won't be archived anywhere.
                                                   
                                                   Every field should be properly set.
                                                   """)
                          @RequestBody @Valid SignatureTaskParams signatureTaskParams,
                          @CurrentUserResolved User currentUser) {

        // Integrity check

        typologyBusinessService.updateTypology(tenant.getId(), singletonList(folder), false);
        contentService.populateFolderWithAllDocumentTypes(folder);

        workflowBusinessService.checkActionAllowedWithDetachedSignatures(folder, SIGNATURE);

        if (task.getAction() != SIGNATURE) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.you_can_t_perform_a_x_action_on_a_y_task", SIGNATURE, task.getAction());
        }

        List<Task> readTasks = workflowService.getReadTasks(folder);
        boolean isReadingMandatory = folder.getSubtype().isReadingMandatory();
        Pair<Long, Long> currentIndex = Pair.of(task.getWorkflowIndex(), task.getStepIndex());
        boolean isAlreadyReadByUser = workflowService.hasAlreadyBeenReadAtIndex(currentIndex, currentUser, readTasks);
        if (isReadingMandatory && !isAlreadyReadByUser) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.you_have_to_read_this_folder_before_signing_it");
        }

        // Signature

        workflowBusinessService.performSignature(tenant, desk, task, folder, currentUser, signatureTaskParams);

        // Signature report

        try {
            Map<String, String> message = new HashMap<>();
            message.put(TENANT_ID_KEY, tenantId);
            message.put(FOLDER_ID_KEY, folderId);
            redisMessagePublisher.publish(SIGNATURE_PROOF_GENERATION_QUEUE, objectMapper.writeValueAsString(message));
        } catch (Exception e) {
            log.warn("signature - error during publishing signature report generation with redis : {}", e.getMessage());
        }
    }


    @PutMapping(API_V1 + "/tenant/{tenantId}/desk/{deskId}/folder/{folderId}/task/{taskId}/chain")
    @Operation(summary = "Chain", description = "Perform a Chain on a Task.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void chain(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                      @PathVariable(name = Tenant.API_PATH) String tenantId,
                      @TenantResolved Tenant tenant,
                      @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                      @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                      @DeskResolved Desk desk,
                      @Parameter(description = Folder.API_DOC_ID_VALUE)
                      @PathVariable(name = Folder.API_PATH) String folderId,
                      @FolderResolved(permission = CURRENT_DESK) Folder existingFolder,
                      @Parameter(description = Task.API_DOC_ID_VALUE)
                      @PathVariable(name = Task.API_PATH) String taskId,
                      @TaskResolved Task task,
                      @Parameter(name = "body", description = "subtypeId field is mandatory.")
                      @RequestBody @Valid CreateFolderRequest draftFolderParams) {

        // Integrity check

        if (task.getAction() != ARCHIVE) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.you_can_t_perform_a_x_action_on_a_y_task", CHAIN, task.getAction());
        }

        boolean hasChainActionPermission = permissionService.currentUserHasChainingRightOnSomeDeskIn(tenantId, singleton(deskId), null, null);
        if (!hasChainActionPermission) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.you_can_t_chain_from_this_desk");
        }

        Folder updateRequest = modelMapper.map(draftFolderParams, Folder.class);
        updateRequest.setContentId(existingFolder.getContentId());
        updateRequest.setType(ofNullable(draftFolderParams.getTypeId()).map(i -> Type.builder().id(i).build()).orElse(null));
        updateRequest.setSubtype(ofNullable(draftFolderParams.getSubtypeId()).map(i -> Subtype.builder().id(i).build()).orElse(null));
        typologyBusinessService.updateTypology(tenant.getId(), singletonList(existingFolder), false);
        typologyBusinessService.updateTypology(tenant.getId(), singletonList(updateRequest), true);

        contentService.populateFolderWithAllDocumentTypes(existingFolder);
        contentService.populateFolderWithAllDocumentTypes(updateRequest);

        // Chain

        handleVariableDesksMetadata(
                updateRequest,
                desk,
                draftFolderParams.getVariableDesksIds(),
                tenant.getId()
        );

        // On a chain action, the current desk becomes the new emitter. That's a part of the feature.
        updateRequest.setOriginDesk(new DeskRepresentation(desk.getId(), desk.getName()));
        folderBusinessService.updateFolder(tenant, existingFolder, updateRequest);

        workflowService.performTask(
                task,
                CHAIN,
                desk.getId(),
                existingFolder,
                deskId,
                null,
                null,
                null,
                null,
                null,
                null
        );

        Long timeToCompleteInHours = statsService.computeTimeToCompleteInHours(task);
        statsService.registerFolderAction(tenant, CHAIN, existingFolder, desk, timeToCompleteInHours);
    }


    @PutMapping(API_STANDARD_V1 + "/tenant/{tenantId}/desk/{deskId}/folder/{folderId}/task/{taskId}/send_to_trash_bin")
    @Operation(
            summary = "Send to trash-bin",
            description = "Only applicable to ended folders, this will remove the folder content into the trash-bin, where it will soon be auto-erased."
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_409, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void sendToTrashBin(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                               @PathVariable(name = Tenant.API_PATH) String tenantId,
                               @TenantResolved Tenant tenant,
                               @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE, required = true)
                               @PathVariable String deskId,
                               @Parameter(description = Folder.API_DOC_ID_VALUE)
                               @PathVariable(name = Folder.API_PATH) String folderId,
                               @FolderResolved(permission = WORKFLOW_DESK) Folder folder,
                               @PathVariable(name = Task.API_PATH) String taskId,
                               @TaskResolved Task task,
                               @CurrentUserResolved User currentUser) {

        log.info("requestArchive folderId:{}", folderId);

        // Integrity check

        if (!Set.of(ARCHIVE, DELETE).contains(task.getAction())) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.you_can_t_perform_a_x_action_on_a_y_task", ARCHIVE, task.getAction());
        }

        boolean isFinished = folder.getState() == FINISHED;
        boolean isRejected = folder.getState() == REJECTED;
        boolean isInTheFinalDesk = StringUtils.equals(deskId, folder.getFinalDesk().getId());
        boolean isInTheEmitterDesk = StringUtils.equals(deskId, folder.getOriginDesk().getId());
        boolean isInTheAppropriateDesk = (isFinished && isInTheFinalDesk) || (isRejected && isInTheEmitterDesk);
        boolean hasArchiveActionPermission = permissionService.currentUserHasArchivingRightOnSomeDeskIn(tenantId, singleton(deskId), null, null);
        log.debug("deskId:{} getOriginDesk:{}", deskId, folder.getOriginDesk().getId());
        log.debug("isFinished:{} isInTheFinalDesk:{} isRejected:{} isInTheEmitterDesk:{} hasArchiveActionPermission:{}",
                isFinished, isInTheFinalDesk, isRejected, isInTheEmitterDesk, hasArchiveActionPermission);
        if (!isInTheAppropriateDesk || !hasArchiveActionPermission) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.you_can_t_archive_this_folder_from_this_desk");
        }

        log.debug("requestArchive folderId:{} contentId:{}", folder.getId(), folder.getContentId());

        // Setting up some dummy archive's Task action data, locally, prior to the real Archive action.
        // We need those for the PREMIS generation, yet we want to perform the real task only if everything went well.
        // In case of error, the actual workflow won't be locked.

        // We have to retrieve the pointer from the folder-one, first.
        Task folderFinalTask = folder.getStepList().stream()
                .filter(t -> asList(DELETE, ARCHIVE).contains(t.getAction()))
                .filter(t -> t.getState() == CURRENT)
                .findFirst()
                .orElseThrow(() -> new LocalizedStatusException(CONFLICT, "message.this_workflow_is_not_finished_yet"));

        folderFinalTask.setDate(new Date());
        folderFinalTask.setPerformedAction(ARCHIVE);
        folderFinalTask.setUser(modelMapper.map(currentUser, UserRepresentation.class));

        // Compute missing document infos

        contentService.populateFolderWithAllDocumentTypes(folder);
        typologyBusinessService.updateTypology(tenant.getId(), singletonList(folder), false);
        authService.updateFolderReferencedDeskNames(singletonList(folder));

        // It should not be allowed (it is hidden in the UI) to archive when the type/subtype is missing
        // Nevertheless, we cannot be sure about webservices, it IS handled down the road and I don't like NPEs so...
        if (folder.getType() != null) {
            SignatureFormat signatureFormat = cryptoService.getSignatureFormat(folder.getType(), folder.getDocumentList());
            folder.getType().setSignatureFormat(signatureFormat);
        }

        folder.getDocumentList()
                .stream()
                .filter(d -> d.getMediaVersion() == null)
                .filter(d -> d.getMediaType() != null)
                // Retrieving PDF version
                .forEach(d -> {
                    switch (d.getMediaType().toString()) {
                        case APPLICATION_PDF_VALUE -> {
                            try (InputStream ios = contentService.retrievePipedDocument(d.getId());
                                 PDDocument doc = Loader.loadPDF(new RandomAccessReadBuffer(ios))) {
                                d.setMediaType(APPLICATION_PDF);
                                d.setMediaVersion(doc.getVersion());
                            } catch (IOException e) {
                                throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_retrieving_PDF_version");
                            }
                        }
                        case APPLICATION_XML_VALUE, TEXT_XML_VALUE -> d.setMediaVersion(1.0F); // TODO : parse real XML version
                    }
                });

        folderBusinessService.prepareChecksumDataForPremisFile(folder.getDocumentList());

        // Actual Premis generation

        String premisContent = folderBusinessService.generatePremisContent(folder);
        String premisNodeId = contentService.createPremisDocument(folder, premisContent);

        folder.getMetadata().put(META_PREMIS_NODE_ID, premisNodeId);
        workflowService.editFolder(tenantId, folderId, folder);

        folder.setPremisNodeId(premisNodeId);

        // Actually perform the workflow task, once everything went well.

        workflowService.performTask(task, ARCHIVE, folder, deskId, null, null, null);

        // Stats

        Long timeToCompleteInHours = statsService.computeTimeToCompleteInHours(task);
        statsService.registerFolderAction(tenant, ARCHIVE, folder, folder.getFinalDesk(), timeToCompleteInHours);
    }


    private void handleVariableDesksMetadata(Folder dummyUpdatedFolder,
                                             Desk desk,
                                             Map<String, String> variableDesksIds,
                                             String tenantId) {

        Map<Integer, String> variableDesksIdsMap = new HashMap<>();
        if (variableDesksIds != null) {
            variableDesksIds.forEach((key, value) -> variableDesksIdsMap.put(Integer.parseInt(key), value));
        }

        groovyService.updateWorkflowDataFromScript(tenantId, dummyUpdatedFolder, variableDesksIdsMap);

        if (dummyUpdatedFolder.getSubtype().getValidationWorkflowId() == null) {
            dummyUpdatedFolder.getSubtype().setValidationWorkflowId(dummyUpdatedFolder.getValidationWorkflowDefinitionKey());
        }

        WorkflowDefinition validationWorkflowDefinition = workflowService.getWorkflowDefinitionByKeyAndMapPlaceholders(
                        tenantId,
                        dummyUpdatedFolder.getSubtype().getValidationWorkflowId(),
                        desk.getId()
                )
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.unknown_workflow_definition_id"));

        dummyUpdatedFolder.setFinalDesk(validationWorkflowDefinition.getFinalDesk());

        Map<String, String> additionalMetadata = workflowService.computePlaceholderDesksConcreteValues(
                validationWorkflowDefinition,
                desk.getId(),
                variableDesksIdsMap
        );
        workflowService.mapIndexedPlaceholdersToActualDesk(dummyUpdatedFolder.getFinalDesk(), desk.getId(), additionalMetadata);

        dummyUpdatedFolder.getStepList()
                .stream()
                .map(Task::getDesks)
                .flatMap(Collection::stream)
                .forEach(d -> workflowService.mapIndexedPlaceholdersToActualDesk(d, desk.getId(), additionalMetadata));


        dummyUpdatedFolder.getMetadata().putAll(additionalMetadata);
    }


    // </editor-fold desc="Requests">


    // <editor-fold desc="Common check">


    // </editor-fold desc="Common check">


    @GetMapping(API_INTERNAL + "/tenant/{tenantId}/desk/{deskId}/workflow-definition/{workflowDefinitionKey}")
    @Operation(summary = "Get a workflow definition with every information set")
    @PreAuthorize(PREAUTHORIZE_TENANT_MEMBER_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public WorkflowDefinitionDto getWorkflowDefinition(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                       @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                       @TenantResolved Tenant tenant,
                                                       @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                                       @DeskResolved Desk desk,
                                                       @Parameter(description = WorkflowDefinition.API_KEY_DOC_VALUE)
                                                       @PathVariable(name = WorkflowDefinition.API_KEY_PATH) String workflowDefinitionKey,
                                                       @WorkflowDefinitionResolved WorkflowDefinition workflowDefinition) {
        log.debug("getWorkflowDefinition tenantId:{} key:{}", tenantId, workflowDefinitionKey);
        workflowBusinessService.populateDesksAndValidators(workflowDefinition);
        return workflowBusinessService.convertToDto(workflowDefinition);
    }


    /**
     * Factorized task for every simple action (VISA, REJECT...)
     * Every task except SIGNATURE, SEAL, or EXTERNAL_SIGNATURE, basically.
     *
     * @param task             Should exists, and target the relevant action
     * @param simpleTaskParams Contains step metadata, public & private annotations
     * @param targetDesk       Only relevant on {@link Action#TRANSFER} and {@link Action#ASK_SECOND_OPINION}.
     */
    private void performSimpleAction(@NotNull Tenant tenant,
                                     @NotNull Desk desk,
                                     @NotNull Folder folder,
                                     @NotNull Task task,
                                     @NotNull Action action,
                                     @NotNull SimpleTaskParams simpleTaskParams,
                                     @Nullable String targetDesk) {

        if (!action.equalsOneOf(REJECT, READ, CHAIN, UNDO, TRANSFER, RECYCLE, ASK_SECOND_OPINION)) {
            workflowBusinessService.checkForNullOrEmptyDesksInNextStep(tenant.getId(), folder);
        }

        // TODO : Maybe we should do this check directly in Workflow, to avoid this request.
        boolean isNotSecondaryAction = !List.of(REJECT, BYPASS, UNDO, TRANSFER, ASK_SECOND_OPINION).contains(action);
        boolean isNotTheSameAction = (task.getAction() != action);
        boolean isAnAcceptableSignature = (task.getAction() == SIGNATURE) && (action == PAPER_SIGNATURE);
        boolean isAnAcceptableRecycle = (task.getAction() == DELETE) && (action == RECYCLE);
        boolean isNotATransferableTask = (!List.of(TRANSFER, ASK_SECOND_OPINION).contains(action)) && (List.of(ARCHIVE, START).contains(task.getAction()));

        if ((isNotSecondaryAction && isNotTheSameAction && !isAnAcceptableSignature && !isAnAcceptableRecycle) || isNotATransferableTask) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.you_can_t_perform_a_x_action_on_a_y_task", action, task.getAction());
        }

        workflowBusinessService.assertCurrentUserCanPerformTask(tenant.getId(),
                task,
                null,
                folder.getType().getId(),
                folder.getSubtype().getId(),
                desk.getId());

        // Verify metadata

        if (!List.of(REJECT, UNDO, ARCHIVE, RECYCLE).contains(action)) {
            typologyBusinessService.verifyFolderMetadataForValidationWorkflow(tenant.getId(), folder);
        }

        // Check format transformation needs

        workflowBusinessService.convertToPdfOnWorkflowSwitch(tenant.getId(), folder, task, action);

        // Perform task

        workflowService.performTask(task, action, folder, desk.getId(), simpleTaskParams, targetDesk, null);

        Long timeToCompleteInHours = statsService.computeTimeToCompleteInHours(task);
        statsService.registerFolderAction(tenant, action, folder, desk, timeToCompleteInHours);

        workflowBusinessService.triggerPostTaskActions(tenant.getId(), desk, folder, action);
        log.info("PerformAction taskId:{} action:{}", task.getId(), task.getAction());
    }


    // @PreAuthorize("hasRole(#task.candidateGroup)")
    private void performIpng(@NotNull Tenant tenant,
                             @NotNull Desk desk,
                             @NotNull Folder folder,
                             @NotNull Task task,
                             @NotNull IpngParams ipngParams,
                             String token) {

        workflowBusinessService.checkForNullOrEmptyDesksInNextStep(tenant.getId(), folder);

        if (StringUtils.isEmpty(folder.getSubtype().getId())) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.unknown_subtype_id");
        }
        log.info("performIpng - ipngParams : {}", ipngParams);

        IpngProof proof = ipngParams.getIpngProof();
        populateIpngProofFromFolder(token, tenant.getId(), folder, proof);

        String senderDeskboxId = this.ipngBindings.getMappedDeskboxIdForDesk(tenant.getId(), desk.getId());
        log.info("performIpng - senderDeskboxId : {}", senderDeskboxId);
        proof.setSenderDeskboxId(senderDeskboxId);

        // Send files

        String cleanFolderName = folder.getName().strip();
        String proofRandomUid = randomUUID().toString();
        String proofId = this.ipngService.buildProofId(proofRandomUid, cleanFolderName);
        log.warn("generated file name = {}", proofId);
        proof.setId(proofId);


        String businessRandomUid = randomUUID().toString();
        proof.setBusinessId(this.ipngService.buildBusinessId(businessRandomUid, cleanFolderName));

        String userId = KeycloakSecurityUtils.getCurrentSessionUserId();

        this.workflowService.setFolderWaitingForIpngResponse(proof, tenant.getId(), folder);

        try {
            workflowBusinessService.sendIpngProof(token, folder, proof, proofId);

            workflowService.performIpng(task, folder, proof.getBusinessId(), userId, ipngParams);
            Long timeToCompleteInHours = statsService.computeTimeToCompleteInHours(task);
            statsService.registerFolderAction(tenant, IPNG, folder, desk, timeToCompleteInHours);

        } catch (NoSuchAlgorithmException e) {
            log.debug("An unlikely exception occurred while trying to transfer the folder {}, aborting IPNG post", proofId);
            throw new ResponseStatusException(INTERNAL_SERVER_ERROR, "Error building or sending folder for transfer");
        } catch (IOException e) {
            throw new ResponseStatusException(INTERNAL_SERVER_ERROR, "Error building folder for transfer");
        }
    }


    private void populateIpngProofFromFolder(String token, String tenantId, Folder folder, IpngProof proof) {
        IpngTypology ipngFullTypo = this.ipngService.getLatestTypology(token);
        IpngMetadataList ipngFullMeta = this.ipngService.getLatestMetadataList(token);

        log.info("folder metadatas : {}", folder.getMetadata());

        List<IpngMetadata> proofMetadata = new ArrayList<>();
        folder.getMetadata().keySet().stream()
                .filter(Objects::nonNull)
                .forEach(tenantKey -> {
                    String ipngKey = this.ipngBindings.getIpngMetadataKeyForOutgoingTenantMetadata(tenantId, tenantKey);
                    ipngFullMeta.getMetadatas().stream().filter(m -> m.getKey().equals(ipngKey))
                            .findFirst().ifPresent(proofMetadata::add);
                });

        proof.setMetadatas(proofMetadata);

        Subtype subtype = folder.getSubtype();
        log.info("::populateIpngProofFromFolder - current folder subtype : {}", subtype);

        final String mappedType = ofNullable(subtype)
                .map(s -> this.ipngBindings.getOutgoingIpngTypeIdForLocalType(tenantId, s.getId()))
                .orElse("000-default");
        proof.setType(ipngFullTypo.getTypes()
                .stream()
                .filter(ipngType -> StringUtils.equals(ipngType.getKey(), mappedType))
                .findAny()
                .orElseThrow(() -> new ResponseStatusException(INTERNAL_SERVER_ERROR, "Error retrievin IPNG type"))
        );
    }
}

