/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller;

import coop.libriciel.ipcore.business.auth.DeskBusinessService;
import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.auth.requests.DeskDto;
import coop.libriciel.ipcore.model.auth.requests.DeskSortBy;
import coop.libriciel.ipcore.model.common.StandardApiPageImpl;
import coop.libriciel.ipcore.model.database.*;
import coop.libriciel.ipcore.model.permission.Delegation;
import coop.libriciel.ipcore.model.permission.DelegationDto;
import coop.libriciel.ipcore.model.permission.DelegationSortBy;
import coop.libriciel.ipcore.model.websockets.DeskCount;
import coop.libriciel.ipcore.model.workflow.DelegationRule;
import coop.libriciel.ipcore.model.workflow.State;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.database.MetadataRepository;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.services.resolvers.CurrentUserResolver.CurrentUserResolved;
import coop.libriciel.ipcore.services.resolvers.DelegationResolver.DelegationResolved;
import coop.libriciel.ipcore.services.resolvers.DeskResolver.DeskResolved;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.*;

import static coop.libriciel.ipcore.IpCoreApplication.*;
import static coop.libriciel.ipcore.model.auth.DeskRepresentation.DEFAULT_SORT_NAME;
import static coop.libriciel.ipcore.model.auth.requests.DeskSortBy.Constants.NAME_VALUE;
import static coop.libriciel.ipcore.model.auth.requests.DeskSortBy.ID;
import static coop.libriciel.ipcore.model.auth.requests.DeskSortBy.NAME;
import static coop.libriciel.ipcore.model.permission.DelegationSortBy.Constants.SUBSTITUTE_DESK_VALUE;
import static coop.libriciel.ipcore.model.permission.DelegationSortBy.SUBSTITUTE_DESK;
import static coop.libriciel.ipcore.model.permission.DelegationSortBy.generateDelegationDtoComparator;
import static coop.libriciel.ipcore.model.stats.StatsCategory.DESK;
import static coop.libriciel.ipcore.model.workflow.Action.UPDATE;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.CollectionUtils.toMutableList;
import static coop.libriciel.ipcore.utils.PaginatedList.MAX_PAGE_SIZE;
import static coop.libriciel.ipcore.utils.RequestUtils.*;
import static java.util.Collections.*;
import static java.util.Comparator.*;
import static java.util.Locale.ROOT;
import static org.springframework.data.domain.Sort.Direction.ASC;
import static org.springframework.http.HttpStatus.*;


@Log4j2
@RestController
@Tag(name = "desk", description = "Desk access for a regular logged user")
public class DeskController {


    /**
     * On a 30 main docs basis : (30 x 5) original detached signatures + 300 detached signatures
     * Maximum of 450 nodes in Alfresco already, just for the signature files.
     * That's good enough.
     */
    public static final int MAX_DETACHED_SIGNATURES_PER_DOCUMENT_ON_STARTUP = 5;
    public static final int MAX_DETACHED_SIGNATURES_GENERATED_PER_FOLDER = 300;


    // <editor-fold desc="Beans">


    private final AuthServiceInterface authService;
    private final DeskBusinessService deskBusinessService;
    private final MetadataRepository metadataRepository;
    private final ModelMapper modelMapper;
    private final PermissionServiceInterface permissionService;
    private final StatsServiceInterface statsService;
    private final WorkflowServiceInterface workflowService;


    @Autowired
    public DeskController(AuthServiceInterface authService,
                          DeskBusinessService deskBusinessService,
                          MetadataRepository metadataRepository,
                          ModelMapper modelMapper,
                          PermissionServiceInterface permissionService,
                          StatsServiceInterface statsService,
                          WorkflowServiceInterface workflowService) {
        this.authService = authService;
        this.deskBusinessService = deskBusinessService;
        this.metadataRepository = metadataRepository;
        this.modelMapper = modelMapper;
        this.permissionService = permissionService;
        this.statsService = statsService;
        this.workflowService = workflowService;
    }


    // </editor-fold desc="Beans">


    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(State.class, new EnumLowercaseConverter<>(State.class));
    }


    // <editor-fold desc="Desks CRUDL">


    @GetMapping(API_V1 + "/tenant/{tenantId}/desk/{deskId}")
    @Operation(summary = "Get the target desk and its data")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public DeskDto getDesk(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                           @PathVariable(name = Tenant.API_PATH) String tenantId,
                           @TenantResolved Tenant tenant,
                           @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                           @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                           @DeskResolved Desk desk) {

        log.debug("getDesk deskId:{} ", deskId);

        boolean userIsAdmin = KeycloakSecurityUtils.isSuperAdmin() || KeycloakSecurityUtils.currentUserIsTenantAdmin(tenantId);
        boolean userHasViewingRight = permissionService.currentUserHasDirectViewingRightOnSomeDeskIn(tenantId, singleton(deskId), null, null);
        if (!userIsAdmin && !userHasViewingRight) {
            throw new LocalizedStatusException(FORBIDDEN, "message.you_can_t_view_this_desk");
        }

        // Missing infos

        Set<String> deskIdSet = singleton(desk.getId());

        Map<String, Set<DelegationRule>> delegations = permissionService.getActiveDelegationsToDesksForCurrentUser(deskIdSet);
        deskBusinessService.factorizeDelegations(delegations);
        desk.getDelegationRules().addAll(MapUtils.getObject(delegations, desk.getId(), emptySet()));

        Map<DelegationRule, Integer> deskCountMap = workflowService.countFolders(deskIdSet, desk.getDelegationRules());
        log.debug("getDesks deskCountMap:{}", deskCountMap);

        boolean userHasFolderCreationRight = permissionService.currentUserHasFolderCreationRightOnSomeDeskIn(tenantId, deskIdSet);
        boolean userHasActionRight = permissionService.currentUserHasFolderActionRightOnSomeDeskIn(tenantId, deskIdSet, null, null);
        boolean hasArchiveActionPermission = permissionService.currentUserHasArchivingRightOnSomeDeskIn(tenantId, deskIdSet, null, null);
        boolean hasChainActionPermission = permissionService.currentUserHasChainingRightOnSomeDeskIn(tenantId, deskIdSet, null, null);
        desk.setFolderCreationAllowed(userHasFolderCreationRight);
        desk.setActionAllowed(desk.isActionAllowed() && userHasActionRight);
        desk.setArchivingAllowed(hasArchiveActionPermission);
        desk.setChainAllowed(hasChainActionPermission);

        return modelMapper.map(desk, DeskDto.class);
    }


    @GetMapping(API_STANDARD_V1 + "/tenant/{tenantId}/desk")
    @Operation(
            summary = "List desks attached to the current user",
            description = DeskSortBy.Constants.API_DOC_SORT_BY_VALUES
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public StandardApiPageImpl<DeskRepresentation> listUserDesks(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                                 @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                                 @TenantResolved Tenant tenant,
                                                                 @CurrentUserResolved User currentUser,
                                                                 @PageableDefault(sort = NAME_VALUE)
                                                                 @ParameterObject Pageable pageable) {

        // Integrity check

        Pageable innerPageable = convertSortedPageable(pageable, DeskSortBy.class, Enum::toString);

        // Fetch data

        List<DeskRepresentation> attachedDesks = KeycloakSecurityUtils.getCurrentUserDesks()
                .stream()
                .filter(desk -> StringUtils.equals(desk.getTenantId(), tenant.getId()))
                .toList();

        deskBusinessService.updateInnerDeskValues(attachedDesks);

        // Manually update the result pagination

        DeskSortBy sortBy = RequestUtils.getFirstOderSort(innerPageable, DeskSortBy.class, NAME);
        boolean asc = RequestUtils.isFirstOrderAsc(innerPageable);
        Comparator<DeskRepresentation> requestComparator = Comparator.comparing(
                desk -> sortBy == ID ? desk.getId() : desk.getName(),
                (o1, o2) -> StringUtils.compareIgnoreCase(o1, o2) * (asc ? 1 : -1)
        );

        log.trace("getDesks paginated desk list:{}", attachedDesks);

        List<DeskRepresentation> resultContent = attachedDesks.stream()
                .sorted(requestComparator.thenComparing(DeskRepresentation::getId, nullsFirst(naturalOrder())))
                .skip((long) innerPageable.getPageNumber() * innerPageable.getPageSize())
                .limit(innerPageable.getPageSize())
                .toList();

        return new StandardApiPageImpl<>(resultContent, pageable, attachedDesks.size());
    }


    // </editor-fold desc="Desks CRUDL">


    @GetMapping(API_V1 + "/tenant/{tenantId}/desk/{deskId}/folderCount")
    @Operation(summary = "Get the target desk folder count")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public DeskCount getDeskFolderCount(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                        @PathVariable(name = Tenant.API_PATH) String tenantId,
                                        @TenantResolved Tenant tenant,
                                        @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                        @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                        @DeskResolved Desk desk) {

        log.info("getDeskCount :{} ", deskId);

        deskBusinessService.populateCountsAndDelegations(singletonMap(desk.getId(), desk), singletonList(desk));

        return deskBusinessService.getDeskCount(desk);
    }


    @GetMapping(API_INTERNAL + "/tenant/{tenantId}/desk/{deskId}/associatedDesks")
    @Operation(summary = "Get the target desk's associated ones")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<DeskRepresentation> getAssociatedDesks(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                       @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                       @TenantResolved Tenant tenant,
                                                       @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                                       @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                                       @DeskResolved Desk desk,
                                                       @PageableDefault(sort = DEFAULT_SORT_NAME)
                                                       @ParameterObject Pageable pageable,
                                                       @Parameter(description = "Searching for a specific desk name")
                                                       @RequestParam(required = false) String searchTerm) {

        this.getAssociatedDesksIntegrityChecks(tenant.getId(), desk.getId());

        List<DeskRepresentation> associatedDesks = permissionService.getAssociatedDeskIds(tenant.getId(), desk.getId()).stream()
                .map(DeskRepresentation::new)
                .toList();

        authService.updateDeskNames(associatedDesks);

        int total = associatedDesks.size();

        if (StringUtils.isNotEmpty(searchTerm)) {
            associatedDesks = associatedDesks.stream()
                    .filter(d -> d.getName().toUpperCase(ROOT).contains(searchTerm.toUpperCase(ROOT)))
                    .toList();
        }

        List<DeskRepresentation> paginatedResult = associatedDesks
                .stream()
                .sorted(comparing(DeskRepresentation::getName, nullsLast(naturalOrder())))
                .skip((long) pageable.getPageNumber() * pageable.getPageSize())
                .limit(pageable.getPageSize())
                .toList();

        return new PageImpl<>(paginatedResult, pageable, total);
    }


    @GetMapping(API_V1 + "/tenant/{tenantId}/desk/{deskId}/filterableMetadata")
    @PreAuthorize("""
                  hasAnyRole(
                    'tenant_' + #tenantId + '_desk_' + #deskId,
                    'tenant_' + #tenantId + '_functional_admin',
                    'tenant_' + #tenantId + '_admin',
                    'admin'
                  )
                  """)
    @Operation(summary = "Get the target desk's filterable metadata", hidden = HIDE_UNSTABLE_API)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "A JSON object containing data and pagination infos"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's permission, or the desk doesn't exist"),
            @ApiResponse(responseCode = "404", description = "The given Tenant/Desk Id doesn't exist")
    })
    public Page<MetadataRepresentation> getFilterableMetadata(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                              @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                              @TenantResolved Tenant tenant,
                                                              @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                                              @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                                              @DeskResolved Desk desk) {

        log.debug("getFilterableMetadata tenantId:{} deskId:{}", tenant.getId(), deskId);

        // Fetch metadata

        PageRequest pageSort = PageRequest.of(0, MAX_PAGE_SIZE, ASC, Metadata.COLUMN_INDEX, Metadata.COLUMN_NAME, Metadata.COLUMN_KEY, Metadata.COLUMN_ID);
        List<String> metadataIds = permissionService.getFilterableMetadataIds(tenantId, deskId);
        Page<MetadataRepresentation> result = metadataRepository
                .findAllByTenant_IdAndIdIn(tenant.getId(), metadataIds, pageSort)
                .map((element) -> modelMapper.map(element, MetadataRepresentation.class));

        log.trace("getFilterableMetadata ids:{} result:{}", metadataIds, result);
        return result;
    }


    // <editor-fold desc="Delegations CRUDL">


    @PostMapping(API_INTERNAL + "/tenant/{tenantId}/desk/{deskId}/delegation")
    @Operation(summary = "Create a new delegation (active or planned) from target desk")
    @PreAuthorize(PREAUTHORIZE_TENANT_MEMBER_OR_MORE)
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_201),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public DelegationDto createDelegation(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                          @PathVariable(name = Tenant.API_PATH) String tenantId,
                                          @TenantResolved Tenant tenant,
                                          @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                          @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                          @Parameter(description = DelegationDto.API_DOC, required = true)
                                          @RequestBody DelegationDto delegationDto) {

        log.info("createDelegation delegating:{} substitute:{}", deskId, delegationDto.getSubstituteDeskId());
        log.info("                 type:{} subtype:{}", delegationDto.getTypeId(), delegationDto.getSubtypeId());

        // TODO: Check if the current user is the desk owner or a delegationManager

        if (StringUtils.equals(deskId, delegationDto.getSubstituteDeskId())) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.redundant_delegation_target");
        }

        permissionService.addDelegation(
                tenant.getId(),
                delegationDto.getSubstituteDeskId(),
                deskId,
                delegationDto.getTypeId(),
                delegationDto.getSubtypeId(),
                delegationDto.getStart(),
                delegationDto.getEnd()
        );

        statsService.registerAdminAction(tenant, DESK, UPDATE, deskId);
        return delegationDto;
    }


    @DeleteMapping(API_INTERNAL + "/tenant/{tenantId}/desk/{deskId}/delegation/{delegationId}")
    @Operation(summary = "Remove an active or planned delegation from target Desk")
    @PreAuthorize(PREAUTHORIZE_TENANT_MEMBER_OR_MORE)
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_204),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void deleteDelegation(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                 @PathVariable(name = Tenant.API_PATH) String tenantId,
                                 @TenantResolved Tenant tenant,
                                 @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                 @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                 @Parameter(description = DelegationDto.API_DOC_ID_VALUE)
                                 @PathVariable(name = DelegationDto.API_PATH) String delegationId,
                                 @DelegationResolved Delegation delegation) {

        log.info("deleteDelegation tenantId:{} deskId:{} delegationId:{}", tenantId, deskId, delegation.getId());

        // TODO: Check if the current user is the desk owner or a delegationManager

        permissionService.deleteDelegation(
                delegationId,
                tenant.getId(),
                delegation.getSubstituteDesk().getId(),
                deskId,
                Optional.ofNullable(delegation.getType()).map(Type::getId).orElse(null),
                Optional.ofNullable(delegation.getSubtype()).map(Subtype::getId).orElse(null),
                delegation.getStart(),
                delegation.getEnd()
        );

        statsService.registerAdminAction(tenant, DESK, UPDATE, deskId);
    }


    @GetMapping(API_INTERNAL + "/tenant/{tenantId}/desk/{deskId}/delegation")
    @Operation(summary = "List delegations (active and planned) for the given delegating desk", description = DelegationSortBy.Constants.API_DOC_SORT_BY_VALUES)
    @PreAuthorize(PREAUTHORIZE_TENANT_MEMBER_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<DelegationDto> listDelegations(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                               @PathVariable(name = Tenant.API_PATH) String tenantId,
                                               @TenantResolved Tenant tenant,
                                               @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                               @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                               @DeskResolved Desk desk,
                                               @PageableDefault(sort = SUBSTITUTE_DESK_VALUE)
                                               @ParameterObject Pageable pageable) {

        log.debug("listDelegations tenantId:{} deskId:{}", tenantId, deskId);

        List<DelegationDto> delegationsDtoList = permissionService.getDelegations(tenant.getId(), deskId).stream()
                .map(delegation -> modelMapper.map(delegation, DelegationDto.class))
                .collect(toMutableList());

        delegationsDtoList.forEach(dto -> dto.setDelegatingDeskId(deskId));
        deskBusinessService.updateInnerDeskValues(tenantId, delegationsDtoList);

        // TODO: Check if the current user is the desk owner or a delegationManager

        // Manual sort

        Pageable innerPageable = RequestUtils.convertSortedPageable(pageable, DelegationSortBy.class, Enum::toString);
        boolean asc = RequestUtils.isFirstOrderAsc(innerPageable);
        DelegationSortBy sortBy = RequestUtils.getFirstOderSort(innerPageable, DelegationSortBy.class, SUBSTITUTE_DESK);
        long page = RequestUtils.getPageNumber(innerPageable);
        long pageSize = RequestUtils.getPageSize(innerPageable);
        Comparator<DelegationDto> comparator = generateDelegationDtoComparator(sortBy, asc);

        PageImpl<DelegationDto> result = new PageImpl<>(
                delegationsDtoList.stream()
                        .sorted(comparator)
                        .skip(page * pageSize)
                        .limit(pageSize)
                        .toList(),
                innerPageable,
                delegationsDtoList.size()
        );

        log.debug("listDelegations result:{}", result);
        return result;
    }


    // </editor-fold desc="Delegations CRUDL">


    private void getAssociatedDesksIntegrityChecks(String tenantId, String deskId) {
        boolean userIsAdmin = KeycloakSecurityUtils.isSuperAdmin() || KeycloakSecurityUtils.currentUserIsTenantAdmin(tenantId);
        String userId = KeycloakSecurityUtils.getCurrentSessionUserId();

        boolean userIsDelegationManager = permissionService.getDelegationManagedDesks(userId, tenantId)
                .stream()
                .map(DeskRepresentation::getId)
                .anyMatch(id -> StringUtils.equals(deskId, id));

        boolean userIsFunctionalAdmin = permissionService.getAdministeredDesks(userId, tenantId)
                .stream()
                .map(DeskRepresentation::getId)
                .anyMatch(id -> StringUtils.equals(deskId, id));

        boolean userIsDeskOwner = KeycloakSecurityUtils.getCurrentUserDeskIds().contains(deskId);

        if (!userIsAdmin && !userIsDelegationManager && !userIsFunctionalAdmin && !userIsDeskOwner) {
            throw new LocalizedStatusException(FORBIDDEN, "message.you_can_t_view_this_desk");
        }
    }


}
