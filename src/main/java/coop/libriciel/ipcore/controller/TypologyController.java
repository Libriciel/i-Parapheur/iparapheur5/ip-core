/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller;

import coop.libriciel.ipcore.business.typology.TypologyBusinessService;
import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.common.StandardApiPageImpl;
import coop.libriciel.ipcore.model.database.*;
import coop.libriciel.ipcore.model.database.requests.SubtypeDto;
import coop.libriciel.ipcore.model.database.requests.SubtypeRepresentation;
import coop.libriciel.ipcore.model.database.requests.TypeDto;
import coop.libriciel.ipcore.model.database.requests.TypeRepresentation;
import coop.libriciel.ipcore.services.database.SubtypeRepository;
import coop.libriciel.ipcore.services.database.TypeRepository;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.services.resolvers.DeskResolver.DeskResolved;
import coop.libriciel.ipcore.services.resolvers.SubtypeResolver.SubtypeResolved;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.services.resolvers.TypeResolver.TypeResolved;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static coop.libriciel.ipcore.IpCoreApplication.API_INTERNAL;
import static coop.libriciel.ipcore.IpCoreApplication.API_STANDARD_V1;
import static coop.libriciel.ipcore.model.database.SubtypeMetadata.SUBTYPE_METADATA_COMPARATOR;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.RequestUtils.convertSortedPageable;


@Log4j2
@RestController
@Tag(name = "typology", description = "Types and Subtypes access for a regular logged user")
@RequestMapping
public class TypologyController {


    // <editor-fold desc="Beans">


    private final ModelMapper modelMapper;
    private final PermissionServiceInterface permissionService;
    private final SubtypeRepository subtypeRepository;
    private final TypeRepository typeRepository;
    private final TypologyBusinessService typologyBusinessService;


    public TypologyController(ModelMapper modelMapper,
                              PermissionServiceInterface permissionService,
                              SubtypeRepository subtypeRepository,
                              TypeRepository typeRepository,
                              TypologyBusinessService typologyBusinessService) {
        this.modelMapper = modelMapper;
        this.permissionService = permissionService;
        this.subtypeRepository = subtypeRepository;
        this.typeRepository = typeRepository;
        this.typologyBusinessService = typologyBusinessService;
    }


    // </editor-fold desc="Beans">


    // <editor-fold desc="Types">


    @GetMapping(API_STANDARD_V1 + "/tenant/{tenantId}/types")
    @Operation(
            summary = "List all types on the current tenant",
            description = TypeSortBy.Constants.API_DOC_SORT_BY_VALUES
    )
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin', 'tenant_' + #tenantId)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public StandardApiPageImpl<TypeRepresentation> listTypes(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                             @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                             @TenantResolved Tenant tenant,
                                                             @PageableDefault(sort = TypeSortBy.Constants.NAME_VALUE)
                                                             @ParameterObject Pageable pageable) {

        log.debug("getTypes page:{} pageSize:{}", pageable.getPageNumber(), pageable.getPageSize());

        Pageable innerPageable = convertSortedPageable(pageable, TypeSortBy.class, TypeSortBy::getColumnName);

        Page<TypeRepresentation> internalRes = typeRepository
                .findAllByTenantId(tenantId, innerPageable)
                .map(type -> modelMapper.map(type, TypeRepresentation.class));

        return new StandardApiPageImpl<>(internalRes);
    }


    @GetMapping(API_STANDARD_V1 + "/tenant/{tenantId}/desk/{deskId}/types/creation-allowed")
    @Operation(
            summary = "List types parent to a creation-allowed subtype on the given desk",
            description = TypeSortBy.Constants.API_DOC_SORT_BY_VALUES
    )
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin', 'tenant_' + #tenantId)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public StandardApiPageImpl<TypeRepresentation> listCreationAllowedTypes(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                                            @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                                            @TenantResolved Tenant tenant,
                                                                            @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                                                            @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                                                            @DeskResolved Desk desk,
                                                                            @PageableDefault(sort = TypeSortBy.Constants.NAME_VALUE)
                                                                            @ParameterObject Pageable pageable) {

        log.debug("listCreationAllowedTypes page:{} pageSize:{}", pageable.getPageNumber(), pageable.getPageSize());
        Pageable innerPageable = convertSortedPageable(pageable, TypeSortBy.class, TypeSortBy::getColumnName);

        List<String> subtypesIdsAllowed = permissionService.getAllowedSubtypeIds(tenantId, deskId);

        Page<TypeRepresentation> internalRes = typeRepository
                .findAllByTenantIdAndSubtypesIn(tenantId, subtypesIdsAllowed, innerPageable)
                .map(type -> modelMapper.map(type, TypeRepresentation.class));

        return new StandardApiPageImpl<>(internalRes);
    }


    @GetMapping(API_INTERNAL + "/tenant/{tenantId}/typology/type/{typeId}")
    @Operation(summary = "Get a type with every informations set")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin', 'tenant_' + #tenantId)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public TypeDto getType(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                           @PathVariable(name = Tenant.API_PATH) String tenantId,
                           @Parameter(description = Type.API_DOC_ID_VALUE)
                           @PathVariable(name = Type.API_PATH) String typeId,
                           @TypeResolved Type type) {
        return modelMapper.map(type, TypeDto.class);
    }


    // </editor-fold desc="Types">


    // <editor-fold desc="Subtypes">


    @GetMapping(API_STANDARD_V1 + "/tenant/{tenantId}/types/{typeId}/subtypes")
    @Operation(
            summary = "List all subtypes of a specific type on the current tenant",
            description = SubtypeSortBy.Constants.API_DOC_SORT_BY_VALUES
    )
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin', 'tenant_' + #tenantId)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public StandardApiPageImpl<SubtypeRepresentation> listSubtypes(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                                   @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                                   @Parameter(description = Type.API_DOC_ID_VALUE)
                                                                   @PathVariable(name = Type.API_PATH) String typeId,
                                                                   @TypeResolved Type type,
                                                                   @PageableDefault(sort = SubtypeSortBy.Constants.NAME_VALUE)
                                                                   @ParameterObject Pageable pageable) {

        log.debug("listSubtypes page:{} pageSize:{}", pageable.getPageNumber(), pageable.getPageSize());
        Pageable innerPageable = convertSortedPageable(pageable, SubtypeSortBy.class, SubtypeSortBy::getColumnName);

        Page<SubtypeRepresentation> internalResult = subtypeRepository.findAllByTenant_IdAndParentType_Id(type.getTenant().getId(), type.getId(), innerPageable)
                .map(subtype -> modelMapper.map(subtype, SubtypeRepresentation.class));

        return new StandardApiPageImpl<>(internalResult);
    }


    @GetMapping(API_STANDARD_V1 + "/tenant/{tenantId}/desk/{deskId}/types/{typeId}/subtypes/creation-allowed")
    @Operation(
            summary = "List creation-allowed subtypes from a given type, for the given desk",
            description = SubtypeSortBy.Constants.API_DOC_SORT_BY_VALUES
    )
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin', 'tenant_' + #tenantId)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class)))
    })
    public StandardApiPageImpl<SubtypeRepresentation> listCreationAllowedSubtypes(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                                                  @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                                                  @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                                                                  @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                                                                  @DeskResolved Desk desk,
                                                                                  @Parameter(description = Type.API_DOC_ID_VALUE, required = true)
                                                                                  @PathVariable(name = Type.API_PATH) String typeId,
                                                                                  @TypeResolved Type type,
                                                                                  @PageableDefault(sort = SubtypeSortBy.Constants.NAME_VALUE)
                                                                                  @ParameterObject Pageable pageable) {

        log.debug("listCreationAllowedSubtypes deskId:{} page:{} pageSize:{}", deskId, pageable.getPageNumber(), pageable.getPageSize());
        Pageable innerPageable = convertSortedPageable(pageable, SubtypeSortBy.class, SubtypeSortBy::getColumnName);

        // Permission fetch
        List<String> subtypesAllowed = permissionService.getAllowedSubtypeIds(tenantId, deskId);
        // Actual fetch
        Page<SubtypeRepresentation> internalResult = subtypeRepository
                .findAllByTenant_IdAndParentType_IdAndIdIn(type.getTenant().getId(), type.getId(), subtypesAllowed, innerPageable)
                .map(subtype -> modelMapper.map(subtype, SubtypeRepresentation.class));

        // Sending back result

        return new StandardApiPageImpl<>(internalResult);
    }


    @GetMapping(API_INTERNAL + "/tenant/{tenantId}/typology/type/{typeId}/subtype/{subtypeId}")
    @Operation(summary = "Get a subtype")
    @PreAuthorize("hasAnyRole('admin', 'tenant_' + #tenantId + '_admin', 'tenant_' + #tenantId)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public SubtypeDto getSubtype(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                 @PathVariable(name = Tenant.API_PATH) String tenantId,
                                 @Parameter(description = Tenant.API_DOC_ID_VALUE)
                                 @PathVariable(name = Tenant.API_PATH) Tenant tenant,
                                 @Parameter(description = Type.API_DOC_ID_VALUE)
                                 @PathVariable(name = Type.API_PATH) String typeId,
                                 @PathVariable(name = Subtype.API_PATH) String subtypeId,
                                 @SubtypeResolved Subtype subtype) {

        subtype.getSubtypeMetadataList().sort(SUBTYPE_METADATA_COMPARATOR);

        return modelMapper.map(subtype, SubtypeDto.class);
    }


    // </editor-fold desc="Subtypes">


}
