/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller;

import coop.libriciel.ipcore.model.database.ExternalSignatureConfig;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.externalsignature.response.ExternalSignatureProcedure;
import coop.libriciel.ipcore.services.externalsignature.ExternalSignatureInterface;
import coop.libriciel.ipcore.services.resolvers.ExternalSignatureConfigResolver.ExternalSignatureConfigResolved;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static coop.libriciel.ipcore.IpCoreApplication.API_V1;
import static coop.libriciel.ipcore.utils.ApiUtils.*;


@Log4j2
@RestController
@RequestMapping(API_V1 + "/tenant/{tenantId}/desk/{deskId}/external_signature")
@Tag(name = "external-signature", description = "External Signature access for a regular logged user")
public class ExternalSignatureController {

    private final ExternalSignatureInterface externalSignatureService;


    @Autowired
    public ExternalSignatureController(ExternalSignatureInterface externalSignatureService) {
        this.externalSignatureService = externalSignatureService;
    }


    @PostMapping("/config/{configId}/procedure")
    @Operation(summary = "Get External Signature Procedure Data")
    @PreAuthorize("hasRole('tenant_' + #tenantId + '_desk_' + #deskId)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public ExternalSignatureProcedure getProcedureData(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                       @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                       @PathVariable String deskId,
                                                       @PathVariable String configId,
                                                       @ExternalSignatureConfigResolved ExternalSignatureConfig externalSignatureConfig,
                                                       @RequestBody String procedureId) {

        return this.externalSignatureService.getProcedureData(externalSignatureConfig, procedureId);
    }
}
