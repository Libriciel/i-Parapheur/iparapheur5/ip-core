/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller;

import coop.libriciel.ipcore.business.template.TemplateBusinessService;
import coop.libriciel.ipcore.business.typology.TypologyBusinessService;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.auth.requests.UserRepresentation;
import coop.libriciel.ipcore.model.content.DetachedSignature;
import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.crypto.SignatureFormat;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.pdfstamp.Origin;
import coop.libriciel.ipcore.model.pdfstamp.SignaturePlacement;
import coop.libriciel.ipcore.model.pdfstamp.StickyNote;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.State;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.content.ContentServiceProperties;
import coop.libriciel.ipcore.services.crypto.CryptoServiceInterface;
import coop.libriciel.ipcore.services.pdfstamp.PdfStampServiceInterface;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.services.pesviewer.PesViewerServiceInterface;
import coop.libriciel.ipcore.services.resolvers.CurrentUserResolver.CurrentUserResolved;
import coop.libriciel.ipcore.services.resolvers.FolderResolver.FolderResolved;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.services.transformation.TransformationServiceInterface;
import coop.libriciel.ipcore.utils.FolderUtils;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.RequestUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static coop.libriciel.ipcore.IpCoreApplication.*;
import static coop.libriciel.ipcore.controller.DeskController.MAX_DETACHED_SIGNATURES_PER_DOCUMENT_ON_STARTUP;
import static coop.libriciel.ipcore.model.crypto.SignatureFormat.AUTO;
import static coop.libriciel.ipcore.model.workflow.Action.EXTERNAL_SIGNATURE;
import static coop.libriciel.ipcore.model.workflow.Action.START;
import static coop.libriciel.ipcore.model.workflow.State.PENDING;
import static coop.libriciel.ipcore.services.content.ContentServiceInterface.FILE_TRANSFER_BUFFER_SIZE;
import static coop.libriciel.ipcore.services.resolvers.FolderResolver.FolderResolved.Permission.CURRENT_DESK;
import static coop.libriciel.ipcore.services.resolvers.FolderResolver.FolderResolved.Permission.WORKFLOW_DESK;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.CryptoUtils.checkIfDetachedSignatureAllowed;
import static coop.libriciel.ipcore.utils.CryptoUtils.checkSignatureFormat;
import static coop.libriciel.ipcore.utils.RequestUtils.PREAUTHORIZE_TENANT_MEMBER_OR_MORE;
import static java.util.Collections.singletonList;
import static org.apache.tomcat.util.http.fileupload.FileUploadBase.MULTIPART_FORM_DATA;
import static org.springframework.core.io.buffer.DataBufferUtils.readInputStream;
import static org.springframework.http.HttpStatus.*;


@Log4j2
@RestController
@Tag(name = "document", description = "Document access for a regular logged user")
public class DocumentController {


    public static final int MAX_MAIN_DOCUMENTS = 30;
    public static final int MAX_DOCUMENTS = 100;


    // <editor-fold desc="Beans">


    private final AuthServiceInterface authService;
    private final ContentServiceInterface contentService;
    private final ContentServiceProperties contentServiceProperties;
    private final CryptoServiceInterface cryptoService;
    private final ModelMapper modelMapper;
    private final PdfStampServiceInterface pdfStampService;
    private final PermissionServiceInterface permissionService;
    private final PesViewerServiceInterface pesViewerService;
    private final TemplateBusinessService templateBusinessService;
    private final TransformationServiceInterface transformationService;
    private final TypologyBusinessService typologyBusinessService;


    @Autowired
    public DocumentController(AuthServiceInterface authService,
                              ContentServiceInterface contentService,
                              ContentServiceProperties contentServiceProperties,
                              CryptoServiceInterface cryptoService,
                              ModelMapper modelMapper,
                              PdfStampServiceInterface pdfStampService,
                              PermissionServiceInterface permissionService,
                              PesViewerServiceInterface pesViewerService,
                              TemplateBusinessService templateBusinessService,
                              TransformationServiceInterface transformationService,
                              TypologyBusinessService typologyBusinessService) {
        this.authService = authService;
        this.contentService = contentService;
        this.contentServiceProperties = contentServiceProperties;
        this.cryptoService = cryptoService;
        this.modelMapper = modelMapper;
        this.pdfStampService = pdfStampService;
        this.permissionService = permissionService;
        this.pesViewerService = pesViewerService;
        this.templateBusinessService = templateBusinessService;
        this.transformationService = transformationService;
        this.typologyBusinessService = typologyBusinessService;
    }


    // </editor-fold desc="Beans">


    // <editor-fold desc="Document CRUD">


    @PostMapping(value = API_V1 + "/tenant/{tenantId}/folder/{folderId}/document", consumes = MULTIPART_FORM_DATA)
    @Operation(summary = "Adding a document to given Folder", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given folderId does not exist"),
            @ApiResponse(responseCode = "507", description = "The document limit ("
                    + MAX_DOCUMENTS
                    + ") or the main document limit ("
                    + MAX_MAIN_DOCUMENTS
                    + ") "
                    + "has been reached.\nConsider deleting some.")
    })
    public void addDocument(@Parameter(description = Tenant.API_DOC_ID_VALUE, required = true)
                            @PathVariable(name = Tenant.API_PATH) String tenantId,
                            @Parameter(hidden = true)
                            @TenantResolved Tenant tenant,
                            @Parameter(description = Folder.API_DOC_ID_VALUE)
                            @PathVariable(name = Folder.API_PATH) String folderId,
                            @FolderResolved(permission = CURRENT_DESK) Folder folder,
                            @Parameter(description = "Is a Main Document")
                            @RequestParam(defaultValue = "true", required = false) boolean isMainDocument,
                            @Parameter(description = "Document index (note that main documents and annexes have separate ones)")
                            @RequestParam(defaultValue = "0", required = false) int index,
                            @Parameter(description = "File")
                            @RequestPart MultipartFile file) {

        log.info("Add document document:{} size:{} name:{}", folderId, file.getSize(), file.getOriginalFilename());

        // Permission check

        checkCurrentUserActionPermissionForFolder(tenantId, folder);

        // Prepare dummy values, to ease tests

        Document currentDocument = new Document(file, isMainDocument, index);
        contentService.populateFolderWithAllDocumentTypes(folder);
        folder.getDocumentList().add(currentDocument);

        typologyBusinessService.updateTypology(tenantId, singletonList(folder), false);
        SignatureFormat signatureFormat = cryptoService.getSignatureFormat(folder.getType(), folder.getDocumentList());

        // Integrity checks

        checkSignatureFormat(signatureFormat, folder.getDocumentList(), null);

        boolean hasBeenStarted = FolderUtils.hasBeenStarted(folder);
        if (isMainDocument && hasBeenStarted) {
            throw new ResponseStatusException(CONFLICT, "message.main_documents_cannot_be_edited_in_a_started_folder");
        }

        if (folder.getDocumentList().size() >= MAX_DOCUMENTS) {
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE, "message.documents_count_reached_the_limit_of_n", MAX_DOCUMENTS);
        }

        long mainDocumentSize = folder.getDocumentList().stream().filter(Objects::nonNull).filter(Document::isMainDocument).count();
        int maxMainDocuments = folder.getSubtype().isMultiDocuments() ? contentServiceProperties.getMaxMainFiles() : 1;
        if (isMainDocument && (mainDocumentSize > maxMainDocuments)) {
            throw new LocalizedStatusException(
                    INSUFFICIENT_STORAGE,
                    (maxMainDocuments > 1) ? "message.main_documents_count_reached_the_limit_of_n" : "message.cannot_add_multiple_documents_to_this_typology",
                    maxMainDocuments
            );
        }

        if (file.getSize() > MAX_DOCUMENT_SIZE) {
            log.error("Error adding document, the file is above the maximum size, abort.");
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE, "message.some_document_exceeds_size_limit", MAX_DOCUMENT_SIZE / (1024 * 1024));
        }

        // Actual save

        contentService.storeMultipartList(tenant.getId(), folder, List.of(file), isMainDocument, null, transformationService::toPdf);
    }


    @GetMapping(API_V1 + "/tenant/{tenantId}/folder/{folderId}/document/{documentId}")
    @Operation(summary = "Retrieve the PDF/XML/... document", hidden = HIDE_UNSTABLE_API)
    @PreAuthorize(PREAUTHORIZE_TENANT_MEMBER_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given folderId/documentId does not exist")
    })
    public void getDocument(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                            @PathVariable(name = Tenant.API_PATH) String tenantId,
                            @TenantResolved Tenant tenant,
                            @Parameter(description = Folder.API_DOC_ID_VALUE)
                            @PathVariable(name = Folder.API_PATH) String folderId,
                            @FolderResolved(permission = WORKFLOW_DESK) Folder folder,
                            @Parameter(description = Document.API_DOC_ID_VALUE)
                            @PathVariable String documentId,
                            HttpServletResponse response) {
        log.debug("Download folderId:{} documentId:{}", folder.getId(), documentId);
        getContent(folder, documentId, false, response);
    }


    @PutMapping(API_V1 + "/tenant/{tenantId}/folder/{folderId}/document/{documentId}")
    @Operation(summary = "Replace the PDF/XML/... document", hidden = HIDE_UNSTABLE_API)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given folderId/documentId does not exist")
    })
    public void updateDocument(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                               @PathVariable(name = Tenant.API_PATH) String tenantId,
                               @TenantResolved Tenant tenant,
                               @Parameter(description = Folder.API_DOC_ID_VALUE)
                               @PathVariable(name = Folder.API_PATH) String folderId,
                               @FolderResolved Folder folder,
                               @Parameter(description = Document.API_DOC_ID_VALUE)
                               @PathVariable String documentId,
                               @Parameter(description = "File")
                               @RequestPart MultipartFile file) {

        log.info("Update folderId:{} documentId:{}", folder.getId(), documentId);

        // Permission check

        checkCurrentUserActionPermissionForFolder(tenantId, folder);

        // Integrity check

        contentService.populateFolderWithAllDocumentTypes(folder);

        Document existingDocument = folder.getDocumentList().stream()
                .filter(d -> StringUtils.equals(d.getId(), documentId))
                .findAny()
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_document_id"));

        // Prepare dummy values, to ease tests

        Document newDocument = new Document(file, existingDocument.isMainDocument(), existingDocument.getIndex());
        folder.getDocumentList().removeIf(document -> StringUtils.equals(document.getId(), existingDocument.getId()));
        folder.getDocumentList().add(newDocument);

        typologyBusinessService.updateTypology(tenantId, singletonList(folder), false);
        SignatureFormat signatureFormat = cryptoService.getSignatureFormat(folder.getType(), folder.getDocumentList());

        // Integrity checks

        checkSignatureFormat(signatureFormat, folder.getDocumentList(), null);

        boolean hasBeenStarted = FolderUtils.hasBeenStarted(folder);
        boolean isMainDocument = existingDocument.isMainDocument();
        if (isMainDocument && hasBeenStarted) {
            throw new ResponseStatusException(CONFLICT, "message.main_documents_cannot_be_edited_in_a_started_folder");
        }

        // Actual save

        existingDocument.getDetachedSignatures().stream()
                .map(DetachedSignature::getId)
                .filter(StringUtils::isNotEmpty)
                .forEach(contentService::deleteDocument);

        Optional.ofNullable(existingDocument.getPdfVisualId())
                .filter(StringUtils::isNotEmpty)
                .ifPresent(contentService::deleteDocument);

        contentService.storeMultipartList(tenant.getId(), folder, List.of(file), isMainDocument, singletonList(documentId), transformationService::toPdf);
    }


    @DeleteMapping(API_V1 + "/tenant/{tenantId}/folder/{folderId}/document/{documentId}")
    @Operation(summary = "Remove the document from the folder, and delete the copy stored", hidden = HIDE_UNSTABLE_API)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given folderId/documentId does not exist")
    })
    public void deleteDocument(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                               @PathVariable(name = Tenant.API_PATH) String tenantId,
                               @Parameter(description = Folder.API_DOC_ID_VALUE)
                               @PathVariable(name = Folder.API_PATH) String folderId,
                               @FolderResolved(permission = CURRENT_DESK) Folder folder,
                               @Parameter(description = Document.API_DOC_ID_VALUE)
                               @PathVariable String documentId) {

        log.info("Delete folderId:{} documentId:{}", folder.getId(), documentId);

        // Permission check

        checkCurrentUserActionPermissionForFolder(tenantId, folder);

        // Integrity check

        contentService.populateFolderWithAllDocumentTypes(folder);

        Document document = folder.getDocumentList().stream()
                .filter(d -> StringUtils.equals(d.getId(), documentId))
                .findAny()
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_document_id"));

        boolean hasBeenStarted = FolderUtils.hasBeenStarted(folder);
        boolean isMainDocument = document.isMainDocument();
        if (isMainDocument && hasBeenStarted) {
            throw new ResponseStatusException(CONFLICT, "message.main_documents_cannot_be_edited_in_a_started_folder");
        }

        long mainDocCount = folder.getDocumentList().stream()
                .filter(Document::isMainDocument)
                .count();
        if (isMainDocument && mainDocCount == 1) {
            throw new ResponseStatusException(CONFLICT, "message.main_documents_cannot_be_edited_in_a_started_folder");
        }

        document.getDetachedSignatures()
                .forEach(s -> contentService.deleteDocument(s.getId()));

        contentService.deleteDocument(documentId);
    }


    // </editor-fold desc="Document CRUD">


    @GetMapping(API_V1 + "/tenant/{tenantId}/folder/{folderId}/document/{documentId}/pdfVisual")
    @Operation(summary = "Retrieve the PDF/XML/... document", hidden = HIDE_UNSTABLE_API)
    @PreAuthorize(PREAUTHORIZE_TENANT_MEMBER_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given folderId/documentId does not exist")
    })
    public void getPdfVisual(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                             @PathVariable(name = Tenant.API_PATH) String tenantId,
                             @TenantResolved Tenant tenant,
                             @Parameter(description = Folder.API_DOC_ID_VALUE)
                             @PathVariable(name = Folder.API_PATH) String folderId,
                             @FolderResolved(permission = WORKFLOW_DESK) Folder folder,
                             @Parameter(description = Document.API_DOC_ID_VALUE)
                             @PathVariable String documentId,
                             HttpServletResponse response) {
        log.info("Download pdfVisual folderId:{} documentId:{}", folder.getId(), documentId);
        getContent(folder, documentId, true, response);
    }


    private void getContent(@NotNull Folder folder,
                            @NotNull String documentId,
                            boolean pdfVisual,
                            HttpServletResponse response) {

        contentService.populateFolderWithAllDocumentTypes(folder);

        List<Document> documentList = new ArrayList<>(folder.getDocumentList());
        List<Document> signatureProofList = new ArrayList<>(folder.getSignatureProofList());
        documentList.addAll(signatureProofList);

        Document document = documentList
                .stream()
                .filter(Objects::nonNull)
                .filter(d -> StringUtils.equals(d.getId(), documentId))
                .findFirst()
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_document_id"));

        log.debug("getContent id:{} pdfVisual:{}", document.getId(), document.getPdfVisualId());

        if (pdfVisual && StringUtils.isEmpty(document.getPdfVisualId())) {
            throw new LocalizedStatusException(NOT_FOUND, "message.unknown_pdf_visual_for_this_document");
        }

        // Retrieve the proper document

        DocumentBuffer documentBuffer = contentService.retrieveContent(pdfVisual ? document.getPdfVisualId() : document.getId());
        log.debug("getContent retrieve from Alfresco buffer currentDocumentId:{} id:{} buffer:{}",
                document.getId(), document.getId(), documentBuffer);

        // Build Response

        RequestUtils.writeBufferToResponse(documentBuffer, response);
    }


    @GetMapping(API_V1 + "/tenant/{tenantId}/folder/{folderId}/document/{documentId}/pes-viewer")
    @Operation(summary = "Getting the root HTML page returned by the PES-viewer", hidden = HIDE_UNSTABLE_API)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given folderId/documentId does not exist")
    })
    public void getPesViewerContent(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                    @PathVariable(name = Tenant.API_PATH) String tenantId,
                                    @Parameter(description = Folder.API_DOC_ID_VALUE)
                                    @PathVariable(name = Folder.API_PATH) String folderId,
                                    @FolderResolved(permission = WORKFLOW_DESK) Folder folder,
                                    @Parameter(description = Document.API_DOC_ID_VALUE)
                                    @PathVariable String documentId,
                                    HttpServletResponse response) {

        log.info("getPesViewerContent folderId:{} documentId:{}", folder.getId(), documentId);

        contentService.populateFolderWithAllDocumentTypes(folder);

        Document document = folder.getDocumentList().stream()
                .filter(Objects::nonNull)
                .filter(d -> StringUtils.equals(d.getId(), documentId))
                .findFirst()
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_document_id"));

        DocumentBuffer documentBuffer = contentService.retrieveContent(documentId);
        documentBuffer.setName(document.getName());
        log.debug("getPesViewerContent retrieve from Alfresco buffer id:{} buffer:{}", documentId, documentBuffer);
        pesViewerService.prepare(documentBuffer, response);
    }


    // <editor-fold desc="Detached signature CRUD">


    @PostMapping(value = API_V1 + "/tenant/{tenantId}/folder/{folderId}/document/{documentId}/detachedSignature", consumes = MULTIPART_FORM_DATA)
    @Operation(summary = "Adding a detached signature for the given Document", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given folderId/deskId/documentId does not exist"),
            @ApiResponse(responseCode = "406", description = "Cannot update detached signatures of pending folders")
    })
    public void addDetachedSignature(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                     @PathVariable(name = Tenant.API_PATH) String tenantId,
                                     @Parameter(hidden = true)
                                     @TenantResolved Tenant tenant,
                                     @Parameter(description = Folder.API_DOC_ID_VALUE)
                                     @PathVariable(name = Folder.API_PATH) String folderId,
                                     @FolderResolved(permission = CURRENT_DESK) Folder folder,
                                     @Parameter(description = Document.API_DOC_ID_VALUE)
                                     @PathVariable String documentId,
                                     @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                     @RequestPart String deskId,
                                     @CurrentUserResolved User currentUser,
                                     @Parameter(description = "File")
                                     @RequestPart MultipartFile file) {

        log.info("setDetachedSignature folderId:{} documentId:{}", folder.getId(), documentId);

        // Permission check

        checkCurrentUserActionPermissionForFolder(tenantId, folder);

        // Integrity check

        Optional.ofNullable(authService.findDeskByIdNoException(tenant.getId(), deskId))
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_desk_id"));

        if (FolderUtils.hasBeenStarted(folder)) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.cannot_update_detached_signatures_of_a_pending_folder");
        }
        typologyBusinessService.updateTypology(tenantId, singletonList(folder), false);

        if (folder.getType() == null) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_add_detached_signatures_on_a_folder_with_a_deleted_type");
        }

        contentService.populateFolderWithAllDocumentTypes(folder);

        if (!folder.getType().getSignatureFormat().equals(AUTO)) {
            SignatureFormat signatureFormat = cryptoService.getSignatureFormat(folder.getType(), folder.getDocumentList());
            checkIfDetachedSignatureAllowed(signatureFormat, true);
        }

        Document targetDocument = folder.getDocumentList().stream()
                .filter(d -> StringUtils.equals(d.getId(), documentId))
                .findAny()
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_document_id"));

        int taskIndex = IntStream.range(0, folder.getStepList().size())
                .filter(i -> FolderUtils.isActive(folder.getStepList().get(i).getState()))
                .findFirst()
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_task_id"));

        Task currentTask = folder.getStepList().get(taskIndex);
        currentTask.setStepIndex((long) taskIndex);

        long existingSignaturesCount = targetDocument.getDetachedSignatures().size();
        if (existingSignaturesCount >= MAX_DETACHED_SIGNATURES_PER_DOCUMENT_ON_STARTUP) {
            throw new LocalizedStatusException(
                    INSUFFICIENT_STORAGE,
                    "message.already_n_detached_signatures_for_a_limit_of_m",
                    existingSignaturesCount, MAX_DETACHED_SIGNATURES_PER_DOCUMENT_ON_STARTUP
            );
        }

        currentTask.setUser(modelMapper.map(currentUser, UserRepresentation.class));

        // Registering things

        DocumentBuffer documentBuffer = new DocumentBuffer(file, false, -1);

        try (InputStream body = file.getInputStream()) {
            documentBuffer.setContentFlux(readInputStream(() -> body, new DefaultDataBufferFactory(), FILE_TRANSFER_BUFFER_SIZE));
            String detachedSignatureDocumentId = contentService.createDetachedSignature(folder.getContentId(), targetDocument, documentBuffer, currentTask);
            documentBuffer.setId(detachedSignatureDocumentId);
        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_reading_file");
        }
    }


    @GetMapping(API_V1 + "/tenant/{tenantId}/folder/{folderId}/document/{documentId}/detachedSignature/{detachedSignatureId}")
    @Operation(summary = "Getting the detachedSignature of the given Document", hidden = HIDE_UNSTABLE_API)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given folderId/documentId/detachedSignatureId does not exist")
    })
    public void getDetachedSignature(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                     @PathVariable(name = Tenant.API_PATH) String tenantId,
                                     @Parameter(description = Folder.API_DOC_ID_VALUE)
                                     @PathVariable(name = Folder.API_PATH) String folderId,
                                     @FolderResolved(permission = WORKFLOW_DESK) Folder folder,
                                     @Parameter(description = Document.API_DOC_ID_VALUE)
                                     @PathVariable String documentId,
                                     @Parameter(description = "Detached signature Id")
                                     @PathVariable String detachedSignatureId,
                                     HttpServletResponse response) {

        log.info("getDetachedSignature folderId:{} documentId:{} detachedSignatureId:{}", folderId, documentId, detachedSignatureId);

        // Integrity check

        contentService.populateFolderWithAllDocumentTypes(folder);

        Document document = folder.getDocumentList()
                .stream()
                .filter(d -> StringUtils.equals(documentId, d.getId()))
                .findAny()
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_document_id"));

        DetachedSignature detachedSignature = document.getDetachedSignatures()
                .stream()
                .filter(s -> StringUtils.equals(detachedSignatureId, s.getId()))
                .findAny()
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_detached_signature_id"));

        // Actual retrieve

        DocumentBuffer documentBuffer = contentService.retrieveContent(detachedSignature.getId());
        RequestUtils.writeBufferToResponse(documentBuffer, response);
    }


    @DeleteMapping(API_V1 + "/tenant/{tenantId}/folder/{folderId}/document/{documentId}/detachedSignature/{detachedSignatureId}")
    @Operation(summary = "Delete the detachedSignature of the given Document", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Deleted"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given folderId/documentId/detachedSignatureId does not exist"),
            @ApiResponse(responseCode = "406", description = "Cannot update detached signatures of pending folders")
    })
    public void deleteDetachedSignature(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                        @PathVariable(name = Tenant.API_PATH) String tenantId,
                                        @Parameter(description = Folder.API_DOC_ID_VALUE)
                                        @PathVariable(name = Folder.API_PATH) String folderId,
                                        @FolderResolved(permission = CURRENT_DESK) Folder folder,
                                        @Parameter(description = Document.API_DOC_ID_VALUE)
                                        @PathVariable String documentId,
                                        @Parameter(description = "Detached signature Id")
                                        @PathVariable String detachedSignatureId) {

        log.info("deleteDetachedSignature folderId:{} documentId:{} detachedSignatureId:{}", folderId, documentId, detachedSignatureId);

        // Permission check

        checkCurrentUserActionPermissionForFolder(tenantId, folder);

        // Integrity check

        folder.getStepList().stream()
                .filter(t -> t.getAction() == START)
                .filter(t -> t.getDate() == null)
                .findFirst()
                .orElseThrow(() -> new LocalizedStatusException(NOT_ACCEPTABLE, "message.cannot_update_detached_signatures_of_a_pending_folder"));

        contentService.populateFolderWithAllDocumentTypes(folder);

        Document document = folder.getDocumentList()
                .stream()
                .filter(d -> StringUtils.equals(documentId, d.getId()))
                .findAny()
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_document_id"));

        DetachedSignature detachedSignature = document.getDetachedSignatures()
                .stream()
                .filter(s -> StringUtils.equals(detachedSignatureId, s.getId()))
                .findAny()
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_detached_signature_id"));

        // Actual delete

        contentService.deleteDocument(detachedSignature.getId());
    }


    // </editor-fold desc="Detached signature CRUD">


    @PostMapping(API_INTERNAL + "/tenant/{tenantId}/folder/{folderId}/document/{documentId}/annotations")
    @Operation(summary = "Creates a PDF annotation")
    @PreAuthorize(PREAUTHORIZE_TENANT_MEMBER_OR_MORE)
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_201),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void createAnnotation(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                 @PathVariable(name = Tenant.API_PATH) String tenantId,
                                 @Parameter(description = Folder.API_DOC_ID_VALUE)
                                 @PathVariable(name = Folder.API_PATH) String folderId,
                                 @FolderResolved(permission = CURRENT_DESK) Folder folder,
                                 @Parameter(description = Document.API_DOC_ID_VALUE)
                                 @PathVariable String documentId,
                                 @Parameter(description = "body")
                                 @RequestBody StickyNote stickyNote,
                                 @CurrentUserResolved User currentUser) {

        log.debug("createAnnotation document:{} user:{}", documentId, currentUser.getId());

        // Permission check

        checkCurrentUserActionPermissionForFolder(tenantId, folder);

        // Integrity check

        contentService.populateFolderWithAllDocumentTypes(folder);
        Document document = folder.getDocumentList()
                .stream()
                .filter(d -> StringUtils.equals(documentId, d.getId()))
                .findAny()
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_document_id"));

        // Actual stamp

        DocumentBuffer documentBuffer = contentService.retrieveContent(document.getId());
        DocumentBuffer documentAnnotatedBuffer = pdfStampService.createAnnotation(documentBuffer, stickyNote, currentUser);

        log.debug("createAnnotation pdfSize:{} patchedPdfSize:{}", documentBuffer.getContentLength(), documentAnnotatedBuffer.getContentLength());
        contentService.updateDocument(documentId, documentAnnotatedBuffer, false);
    }


    @DeleteMapping(API_INTERNAL + "/tenant/{tenantId}/folder/{folderId}/document/{documentId}/annotations/{annotationId}")
    @Operation(summary = "Deletes the given PDF annotation")
    @PreAuthorize(PREAUTHORIZE_TENANT_MEMBER_OR_MORE)
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_204),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void deleteAnnotation(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                 @PathVariable(name = Tenant.API_PATH) String tenantId,
                                 @Parameter(description = Folder.API_DOC_ID_VALUE)
                                 @PathVariable(name = Folder.API_PATH) String folderId,
                                 @FolderResolved(permission = CURRENT_DESK) Folder folder,
                                 @Parameter(description = Document.API_DOC_ID_VALUE)
                                 @PathVariable String documentId,
                                 @Parameter(description = "Annotation Id")
                                 @PathVariable String annotationId) {

        // Permission check

        checkCurrentUserActionPermissionForFolder(tenantId, folder);

        // Integrity check

        contentService.populateFolderWithAllDocumentTypes(folder);

        Document document = folder.getDocumentList()
                .stream()
                .filter(d -> StringUtils.equals(documentId, d.getId()))
                .findFirst()
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_document_id"));

        // Actual stamp

        DocumentBuffer documentBuffer = contentService.retrieveContent(document.getId());
        boolean signaturePlacementRemoved = document.getSignaturePlacementAnnotations()
                .removeIf(signaturePlacement -> signaturePlacement.getId().equals(annotationId));
        if (signaturePlacementRemoved) {
            contentService.updateDocumentInternalProperties(document);
        } else {
            DocumentBuffer documentPatchedBuffer = pdfStampService.deleteComment(documentBuffer, annotationId);
            log.debug("deleteAnnotation pdfSize:{} patchedPdfSize:{}", documentBuffer.getContentLength(), documentPatchedBuffer.getContentLength());
            contentService.updateDocument(documentId, documentPatchedBuffer, false);
        }
    }


    @GetMapping(API_INTERNAL + "/tenant/{tenantId}/folder/{folderId}/document/{documentId}/signaturePlacement")
    @Operation(summary = "Get all signaturePlacement Annotations of a document")
    @PreAuthorize(PREAUTHORIZE_TENANT_MEMBER_OR_MORE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public List<SignaturePlacement> getSignaturePlacementAnnotations(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                                     @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                                     @Parameter(description = Folder.API_DOC_ID_VALUE)
                                                                     @PathVariable(name = Folder.API_PATH) String folderId,
                                                                     @FolderResolved(permission = WORKFLOW_DESK) Folder folder,
                                                                     @Parameter(description = Document.API_DOC_ID_VALUE)
                                                                     @PathVariable String documentId) {

        contentService.populateFolderWithAllDocumentTypes(folder);

        Document document = folder.getDocumentList()
                .stream()
                .filter(d -> StringUtils.equals(documentId, d.getId()))
                .findAny()
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_document_id"));

        return document.getSignaturePlacementAnnotations();
    }


    @PostMapping(API_INTERNAL + "/tenant/{tenantId}/folder/{folderId}/document/{documentId}/signaturePlacement")
    @Operation(summary = "Creates a signature placement annotation")
    @PreAuthorize(PREAUTHORIZE_TENANT_MEMBER_OR_MORE)
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_201),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void createSignaturePlacementAnnotation(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                   @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                   @TenantResolved Tenant tenant,
                                                   @Parameter(description = Folder.API_DOC_ID_VALUE)
                                                   @PathVariable(name = Folder.API_PATH) String folderId,
                                                   @FolderResolved(permission = CURRENT_DESK) Folder folder,
                                                   @Parameter(description = Document.API_DOC_ID_VALUE)
                                                   @PathVariable String documentId,
                                                   @Parameter(description = "body")
                                                   @RequestBody SignaturePlacement signaturePlacement) {

        log.debug("createSignaturePlacementAnnotation document:{}", documentId);

        // Permission check

        checkCurrentUserActionPermissionForFolder(tenantId, folder);

        // Integrity check

        contentService.populateFolderWithAllDocumentTypes(folder);
        Document document = folder.getDocumentList()
                .stream()
                .filter(d -> StringUtils.equals(documentId, d.getId()))
                .findAny()
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_document_id"));

        if (StringUtils.isEmpty(signaturePlacement.getId())) {
            document.setSignaturePlacementAnnotations(new ArrayList<>());
            signaturePlacement.setId(UUID.randomUUID().toString());
            document.getSignaturePlacementAnnotations().add(signaturePlacement);
        } else {
            document.getSignaturePlacementAnnotations().forEach(s -> {
                if (s.getId().equals(signaturePlacement.getId())) {
                    s.setSignatureNumber(signaturePlacement.getSignatureNumber());
                    s.setTemplateType(signaturePlacement.getTemplateType());
                }
            });
        }

        document.getSignaturePlacementAnnotations().forEach(placementAnnotation -> {

            Resource templateResource = Optional.ofNullable(templateBusinessService.getCustomTemplate(tenant, placementAnnotation.getTemplateType()))
                    .map(documentBuffer -> {
                        try {
                            return (Resource) new InputStreamResource(RequestUtils.bufferToInputStream(documentBuffer));
                        } catch (IOException e) {
                            log.error("Error while reading custom template resource: {}", placementAnnotation.getTemplateType(), e);
                            return null;
                        }
                    })
                    .orElseGet(() -> templateBusinessService.getDefaultTemplateResource(placementAnnotation.getTemplateType()));

            Map<String, Object> template;
            try (InputStream inputStream = templateResource.getInputStream()) {
                template = new Yaml().load(inputStream);
            } catch (IOException e) {
                log.error("Error while reading custom or default template resource : {}", placementAnnotation.getTemplateType());
                throw new RuntimeException("Error while reading custom or default template resource : {}", e);
            }

            boolean isCurrentStepExternalSignature = folder.getStepList()
                    .stream().filter(step -> step.getState().equals(PENDING))
                    .anyMatch(step -> step.getAction().equals(EXTERNAL_SIGNATURE));

            if (!isCurrentStepExternalSignature) {
                placementAnnotation.setWidth((int) template.get("width"));
                placementAnnotation.setHeight((int) template.get("height"));
            }

            if (placementAnnotation.getRectangleOrigin() == Origin.CENTER) {

                int bottomLeftX = placementAnnotation.getX() - placementAnnotation.getWidth() / 2;
                int bottomLeftY = placementAnnotation.getY() - placementAnnotation.getHeight() / 2;

                boolean isOnTheSide = placementAnnotation.getPageRotation() == 90 || placementAnnotation.getPageRotation() == 270;

                // Coordinate translation is weird on crypto, we must also shift page dimensions
                if (isOnTheSide) {
                    int pageWidth = placementAnnotation.getPageWidth();
                    int pageHeight = placementAnnotation.getPageHeight();
                    if (pageHeight > pageWidth) {
                        bottomLeftX += (pageHeight - pageWidth) / 2;
                        bottomLeftY -= (pageHeight - pageWidth) / 2;
                    } else {
                        // For some reason, in landscape only (considering the un-rotated page size),
                        // the placement is different depending on the page rotation.
                        // This doesn't feel right, but behaviour is consistent with this formula.
                        if (placementAnnotation.getPageRotation() == 90) {
                            bottomLeftX += (pageWidth - pageHeight) / 2;
                            bottomLeftY += (pageWidth - pageHeight) / 2;
                        } else {
                            bottomLeftX -= (pageWidth - pageHeight) / 2;
                            bottomLeftY -= (pageWidth - pageHeight) / 2;
                        }
                    }
                }

                placementAnnotation.setX(bottomLeftX);
                placementAnnotation.setY(bottomLeftY);

                placementAnnotation.setRectangleOrigin(Origin.BOTTOM_LEFT);
            }
        });

        contentService.updateDocumentInternalProperties(document);
    }


    /**
     * Raises an exception if the current connected user does not have "folderAction" permission on (at least one) desk from a pending task of the folder.
     */
    private void checkCurrentUserActionPermissionForFolder(String tenantId, Folder folder) {

        log.debug("checkCurrentUserActionPermissionForFolder");
        Set<String> currentDeskIds = folder.getStepList().stream()
                .filter(s -> s.getState() == State.PENDING || s.getState() == State.CURRENT)
                .flatMap(s -> s.getDesks().stream())
                .map(DeskRepresentation::getId)
                .collect(Collectors.toSet());

        String typeId = folder.getType().getId();
        String subtypeId = folder.getSubtype().getId();

        boolean userHasDelegation = permissionService.currentUserHasDelegationOnSomeDeskIn(tenantId, currentDeskIds, typeId, subtypeId);
        boolean userCanPerformActionOnFolderAsDesk = permissionService.currentUserHasFolderActionRightOnSomeDeskIn(tenantId, currentDeskIds, typeId, subtypeId);
        boolean userIsAdmin = permissionService.currentUserHasAdminRightsOnTenant(tenantId);

        if (!(userCanPerformActionOnFolderAsDesk || userHasDelegation || userIsAdmin)) {
            log.warn("checkCurrentUserActionPermissionForFolder : insufficient rights, stopping process");
            throw new LocalizedStatusException(FORBIDDEN, "message.you_can_t_perform_an_action_on_this_folder");
        }
    }


}
