/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller;

import coop.libriciel.ipcore.business.auth.DeskBusinessService;
import coop.libriciel.ipcore.business.auth.UserBusinessService;
import coop.libriciel.ipcore.business.signatureproof.SignatureReportBusinessService;
import coop.libriciel.ipcore.business.template.TemplateBusinessService;
import coop.libriciel.ipcore.business.typology.TypologyBusinessService;
import coop.libriciel.ipcore.business.workflow.FolderBusinessService;
import coop.libriciel.ipcore.business.workflow.WorkflowBusinessService;
import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.common.StandardApiPageImpl;
import coop.libriciel.ipcore.model.content.CreateFolderRequest;
import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.content.SignatureProof;
import coop.libriciel.ipcore.model.crypto.FolderDataToSignHolder;
import coop.libriciel.ipcore.model.crypto.PdfSignaturePosition;
import coop.libriciel.ipcore.model.crypto.SignatureFormat;
import coop.libriciel.ipcore.model.database.*;
import coop.libriciel.ipcore.model.database.requests.StringResult;
import coop.libriciel.ipcore.model.database.userPreferences.FolderFilter;
import coop.libriciel.ipcore.model.database.userPreferences.FolderFilterDto;
import coop.libriciel.ipcore.model.database.userPreferences.UserPreferences;
import coop.libriciel.ipcore.model.externalsignature.Status;
import coop.libriciel.ipcore.model.ipng.IpngFolderProofs;
import coop.libriciel.ipcore.model.ipng.PendingIpngFolder;
import coop.libriciel.ipcore.model.ipng.PendingIpngFolderKey;
import coop.libriciel.ipcore.model.mail.MailContent;
import coop.libriciel.ipcore.model.mail.MailTarget;
import coop.libriciel.ipcore.model.securemail.MailParams;
import coop.libriciel.ipcore.model.workflow.*;
import coop.libriciel.ipcore.model.workflow.requests.ColumnedTaskListRequest;
import coop.libriciel.ipcore.model.workflow.requests.SimpleTaskParams;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.crypto.CryptoServiceInterface;
import coop.libriciel.ipcore.services.database.*;
import coop.libriciel.ipcore.services.externalsignature.ExternalSignatureInterface;
import coop.libriciel.ipcore.services.ipng.PendingIpngFolderRepository;
import coop.libriciel.ipcore.services.mail.NotificationServiceInterface;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.services.resolvers.CurrentUserResolver.CurrentUserResolved;
import coop.libriciel.ipcore.services.resolvers.DeskResolver.DeskResolved;
import coop.libriciel.ipcore.services.resolvers.FolderResolver.FolderResolved;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.services.securemail.SecureMailServiceInterface;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.services.workflow.LegacyIdRepository;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.*;
import gov.loc.premis.v3.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.modelmapper.ModelMapper;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.*;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.server.ResponseStatusException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.*;

import static coop.libriciel.ipcore.IpCoreApplication.*;
import static coop.libriciel.ipcore.model.database.TemplateType.MAIL_ACTION_SEND;
import static coop.libriciel.ipcore.model.database.TemplateType.SIGNATURE_MEDIUM;
import static coop.libriciel.ipcore.model.workflow.Action.*;
import static coop.libriciel.ipcore.model.workflow.FolderSortBy.CREATION_DATE;
import static coop.libriciel.ipcore.model.workflow.FolderSortBy.Constants.CREATION_DATE_VALUE;
import static coop.libriciel.ipcore.model.workflow.FolderSortBy.Constants.CURRENT_DESK_VALUE;
import static coop.libriciel.ipcore.model.workflow.FolderSortBy.FOLDER_NAME;
import static coop.libriciel.ipcore.model.workflow.State.*;
import static coop.libriciel.ipcore.model.workflow.Task.ExternalState.ERROR;
import static coop.libriciel.ipcore.model.workflow.Task.ExternalState.FORM;
import static coop.libriciel.ipcore.services.mail.NotificationServiceInterface.MAX_MAIL_ATTACHMENT_SIZE;
import static coop.libriciel.ipcore.services.resolvers.FolderResolver.FolderResolved.Permission.CURRENT_DESK;
import static coop.libriciel.ipcore.services.resolvers.FolderResolver.FolderResolved.Permission.WORKFLOW_DESK;
import static coop.libriciel.ipcore.services.workflow.IpWorkflowService.*;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static coop.libriciel.ipcore.utils.CollectionUtils.*;
import static coop.libriciel.ipcore.utils.FolderUtils.isActive;
import static coop.libriciel.ipcore.utils.PaginatedList.MAX_PAGE_SIZE;
import static coop.libriciel.ipcore.utils.RequestUtils.returnRedactedResponse;
import static coop.libriciel.ipcore.utils.TextUtils.ISO8601_DATE_FORMAT;
import static coop.libriciel.ipcore.utils.TextUtils.MESSAGE_BUNDLE;
import static coop.libriciel.ipcore.utils.XmlUtils.*;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import static org.apache.commons.lang3.StringUtils.*;
import static org.apache.tomcat.util.http.fileupload.FileUploadBase.MULTIPART_FORM_DATA;
import static org.springframework.data.domain.Pageable.unpaged;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.*;
import static org.springframework.util.ResourceUtils.CLASSPATH_URL_PREFIX;


@Log4j2
@RestController
@Tag(name = "folder", description = "Folder access for a regular logged user")
public class FolderController {


    private @Value(CLASSPATH_URL_PREFIX + "templates/mail/mail_action_send.ftl") Resource mailActionSendTemplateResource;


    // <editor-fold desc="Beans">


    private final AuthServiceInterface authService;
    private final ContentServiceInterface contentService;
    private final CryptoServiceInterface cryptoService;
    private final DatabaseServiceInterface dbService;
    private final DeskBusinessService deskBusinessService;
    private final ExternalSignatureInterface externalSignatureService;
    private final FolderBusinessService folderBusinessService;
    private final LegacyIdRepository legacyIdRepository;
    private final MetadataRepository metadataRepository;
    private final ModelMapper modelMapper;
    private final NotificationServiceInterface notificationService;
    private final PendingIpngFolderRepository pendingIpngFolderRepository;
    private final PermissionServiceInterface permissionService;
    private final SecureMailServiceInterface secureMailService;
    private final SignatureReportBusinessService signatureReportBusinessService;
    private final StatsServiceInterface statsService;
    private final SubtypeRepository subtypeRepository;
    private final TemplateBusinessService templateBusinessService;
    private final TypeRepository typeRepository;
    private final TypologyBusinessService typologyBusinessService;
    private final UserBusinessService userBusinessService;
    private final UserPreferencesRepository userPreferencesRepository;
    private final WorkflowBusinessService workflowBusinessService;
    private final WorkflowServiceInterface workflowService;


    @Autowired
    public FolderController(AuthServiceInterface authService,
                            ContentServiceInterface contentService,
                            CryptoServiceInterface cryptoService,
                            DatabaseServiceInterface dbService,
                            DeskBusinessService deskBusinessService,
                            ExternalSignatureInterface externalSignatureService,
                            FolderBusinessService folderBusinessService,
                            LegacyIdRepository legacyIdRepository,
                            MetadataRepository metadataRepository,
                            ModelMapper modelMapper,
                            NotificationServiceInterface notificationService,
                            PendingIpngFolderRepository pendingIpngFolderRepository,
                            PermissionServiceInterface permissionService,
                            SecureMailServiceInterface secureMailService,
                            SignatureReportBusinessService signatureReportBusinessService,
                            StatsServiceInterface statsService,
                            SubtypeRepository subtypeRepository,
                            TemplateBusinessService templateBusinessService,
                            TypeRepository typeRepository,
                            TypologyBusinessService typologyBusinessService,
                            UserBusinessService userBusinessService,
                            UserPreferencesRepository userPreferencesRepository,
                            WorkflowBusinessService workflowBusinessService,
                            WorkflowServiceInterface workflowService) {
        this.authService = authService;
        this.contentService = contentService;
        this.cryptoService = cryptoService;
        this.dbService = dbService;
        this.deskBusinessService = deskBusinessService;
        this.externalSignatureService = externalSignatureService;
        this.folderBusinessService = folderBusinessService;
        this.legacyIdRepository = legacyIdRepository;
        this.metadataRepository = metadataRepository;
        this.modelMapper = modelMapper;
        this.notificationService = notificationService;
        this.pendingIpngFolderRepository = pendingIpngFolderRepository;
        this.permissionService = permissionService;
        this.secureMailService = secureMailService;
        this.signatureReportBusinessService = signatureReportBusinessService;
        this.statsService = statsService;
        this.subtypeRepository = subtypeRepository;
        this.templateBusinessService = templateBusinessService;
        this.typeRepository = typeRepository;
        this.typologyBusinessService = typologyBusinessService;
        this.userBusinessService = userBusinessService;
        this.userPreferencesRepository = userPreferencesRepository;
        this.workflowBusinessService = workflowBusinessService;
        this.workflowService = workflowService;
    }


    // </editor-fold desc="Beans">


    @GetMapping(API_V1 + "/tenant/{tenantId}/folder/{folderId}/dataToSign")
    @Operation(hidden = HIDE_UNSTABLE_API)
    public List<FolderDataToSignHolder> getDataToSign(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                      @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                      @TenantResolved Tenant tenant,
                                                      @Parameter(description = Folder.API_DOC_ID_VALUE)
                                                      @PathVariable(name = Folder.API_PATH) String folderId,
                                                      @FolderResolved(permission = CURRENT_DESK) Folder folder,
                                                      @RequestParam String certBase64,
                                                      @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                                      @RequestParam String deskId,
                                                      @RequestParam @Nullable String customSignatureField,
                                                      @CurrentUserResolved User user) {

        log.debug("getDataToSign folderId:{}, name: {}, publicKeyBase64:{} originDeskId:{}", folderId, folder.getName(), certBase64, deskId);

        // Integrity check

        typologyBusinessService.updateTypology(tenant.getId(), singletonList(folder), false);
        contentService.populateFolderWithAllDocumentTypes(folder);

        Desk desk = authService.findDeskByIdNoException(tenant.getId(), deskId);

        if (desk == null) {
            throw new LocalizedStatusException(NOT_FOUND, "message.unknown_desk_id");
        }


        Desk delegatingDesk = workflowBusinessService.getDelegatingDesk(tenant, desk, folder);


        // Building

        long signatureDateTime = new Date().getTime();
        String signatureImageBase64 = Optional.ofNullable(user.getSignatureImageContentId())
                .filter(StringUtils::isNotEmpty)
                .map(contentService::retrieveContentAsBase64)
                .orElse(null);

        folder.setType(typeRepository.findById(folder.getType().getId())
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.unknown_type_id")));

        SignatureFormat signatureFormat = cryptoService.getSignatureFormat(folder.getType(), folder.getDocumentList());
        UserPreferences currentUserPreferences = userPreferencesRepository.getByUserId(user.getId())
                .orElse(new UserPreferences());

        List<FolderDataToSignHolder> result = folder.getDocumentList()
                .stream()
                .filter(Document::isMainDocument)
                .map(d -> {
                    // TODO : a proper constructor, to transfer parent params ?
                    DocumentBuffer documentBuffer = contentService.retrieveContent(d.getId());
                    documentBuffer.setId(d.getId());
                    documentBuffer.setName(d.getName());
                    documentBuffer.setSignaturePlacementAnnotations(d.getSignaturePlacementAnnotations());
                    documentBuffer.setSignatureTags(d.getSignatureTags());
                    documentBuffer.setSealTags(d.getSealTags());
                    documentBuffer.setDetachedSignatures(d.getDetachedSignatures());
                    return documentBuffer;
                })
                .map(d -> {
                    PdfSignaturePosition position = CryptoUtils.getPosition(folder, d, currentUserPreferences, SIGNATURE);
                    TemplateType templateType = firstNonNull(position.getTemplateType(), SIGNATURE_MEDIUM); // TODO: Link the user's default template
                    Resource templateResource = templateBusinessService.getTemplateResource(tenant, templateType);

                    return cryptoService.getDataToSign(
                            tenant,
                            desk,
                            folder,
                            user,
                            d,
                            signatureFormat,
                            position,
                            certBase64,
                            user.getUserName(),
                            signatureDateTime,
                            false,
                            templateResource,
                            signatureImageBase64,
                            customSignatureField,
                            delegatingDesk);
                })
                .peek(data -> {
                    if (data == null) {
                        log.error("Error while retrieving data to sign from crypto for folder: {}", folder.getName());
                        throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_crypto_service");
                    }
                })
                .map(dataToSignHolder -> new FolderDataToSignHolder(dataToSignHolder, folderId))
                .toList();

        log.debug("getDataToSign (folder {}) prepared:{}", folder.getName(), result);
        return result;
    }


    // <editor-fold desc="Folder CRUDL">


    @PostMapping(value = API_STANDARD_V1 + "/tenant/{tenantId}/desk/{deskId}/folder", consumes = MULTIPART_FORM_DATA)
    @Operation(
            summary = "Create a folder",
            description = """
                          The PREMIS-formatted XML multipart-file is mandatory, and must contain every needed data.\s\s
                          Every other file has to be sent as it is, with an appropriate MediaType.
                                                    
                          Here's an example of a PREMIS file. The same structure than the one downloaded in a ZIP:
                                                    
                          ```xml
                          <?xml version="1.0" encoding="UTF-8"?>
                          <premis xmlns="http://www.loc.gov/premis/v3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="3.0" xsi:schemaLocation="http://www.loc.gov/premis/v3 http://www.loc.gov/standards/premis/v3/premis.xsd">
                            <!-- Folder -->
                            <object xsi:type="intellectualEntity">
                              <significantProperties>
                                <significantPropertiesType>i_Parapheur_reserved_type</significantPropertiesType>
                                <significantPropertiesValue>type-id-or-name</significantPropertiesValue>
                              </significantProperties>
                              <significantProperties>
                                <significantPropertiesType>i_Parapheur_reserved_subtype</significantPropertiesType>
                                <significantPropertiesValue>subtype-id-or-name</significantPropertiesValue>
                              </significantProperties>
                              <significantProperties>
                                <significantPropertiesType>i_Parapheur_reserved_dueDate</significantPropertiesType>
                                <significantPropertiesValue>iso8601_date</significantPropertiesValue>
                              </significantProperties>
                              <originalName>Folder name</originalName>
                            </object>
                            <!-- Files -->
                            <object xsi:type="file">
                              <significantProperties>
                                <significantPropertiesType>i_Parapheur_reserved_mainDocument</significantPropertiesType>
                                <significantPropertiesValue>true</significantPropertiesValue>
                              </significantProperties>
                              <originalName>my.pdf</originalName>
                            </object>
                          </premis>
                          ```
                          """)
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_201),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_406, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    @PreAuthorize("hasRole('tenant_' + #tenantId + '_desk_' + #deskId)")
    public FolderRepresentation createFolder(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                             @PathVariable(name = Tenant.API_PATH) String tenantId,
                                             @TenantResolved Tenant tenant,
                                             @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                             @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                             @DeskResolved Desk originDesk,
                                             @Parameter(description = "If true, starts automatically the folder right after the draft creation")
                                             @RequestParam(required = false, defaultValue = "true") boolean autoStart,
                                             @Parameter(description = "Folder")
                                             @RequestPart(name = "folder") MultipartFile folderPremis,
                                             @Parameter(description = "Documents")
                                             @RequestPart(name = "documents") final MultipartFile[] requestDocuments) {

        // Parse request

        PremisComplexType premisComplexType;
        try (InputStream folderPremisInputStream = folderPremis.getInputStream()) {
            JAXBContext jaxbContext = JAXBContext.newInstance(PremisComplexType.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            //noinspection unchecked
            JAXBElement<PremisComplexType> premisComplexTypeElement = (JAXBElement<PremisComplexType>) unmarshaller.unmarshal(folderPremisInputStream);
            premisComplexType = premisComplexTypeElement.getValue();
        } catch (JAXBException | IOException ex) {
            log.error("Error on PREMIS parse: {}", ex.getMessage());
            log.debug("Error details: ", ex);
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.error_reading_premis_file");
        }

        IntellectualEntity intellectualEntity = premisComplexType.getObject().stream()
                .filter(IntellectualEntity.class::isInstance)
                .map(IntellectualEntity.class::cast)
                .findFirst()
                .orElseThrow(() -> {
                    log.error("Error reading premis, no 'intellectualEntity' element");
                    return new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.error_reading_premis_file");
                });

        List<File> premisDocumentList = premisComplexType.getObject().stream()
                .filter(File.class::isInstance)
                .map(File.class::cast)
                .toList();

        // Integrity checks

        folderBusinessService.checkCreateFolderLimitations(tenant);

        Type type = Optional
                .ofNullable(getSignificantPropertyValue(intellectualEntity.getSignificantProperties(), SIGNIFICANT_PROPERTY_TYPE_KEY))
                .flatMap(typeId -> typeRepository.findByIdAndTenant_Id(typeId, tenantId)
                        .or(() -> typeRepository.findByNameIgnoreCaseAndTenant_Id(typeId, tenantId)))
                .orElseThrow(() -> new LocalizedStatusException(NOT_ACCEPTABLE, "message.unknown_type_id"));

        Subtype subtype = Optional
                .ofNullable(getSignificantPropertyValue(intellectualEntity.getSignificantProperties(), SIGNIFICANT_PROPERTY_SUBTYPE_KEY))
                .flatMap(subtypeId -> subtypeRepository.findByIdAndParentType_IdAndTenant_Id(subtypeId, type.getId(), tenantId)
                        .or(() -> subtypeRepository.findByNameIgnoreCaseAndParentType_IdAndTenant_Id(subtypeId, type.getId(), tenantId)))
                .orElseThrow(() -> new LocalizedStatusException(NOT_ACCEPTABLE, "message.unknown_subtype_id"));

        Date dueDate = Optional
                .ofNullable(getSignificantPropertyValue(intellectualEntity.getSignificantProperties(), SIGNIFICANT_PROPERTY_DUE_DATE))
                .map(serializedDate -> TextUtils.deserializeDate(serializedDate, ISO8601_DATE_FORMAT))
                .orElse(null);

        Map<String, String> parsedMetadata = intellectualEntity.getSignificantProperties().stream()
                .map(SignificantPropertiesComplexType::getContent)
                .map(keyValueCollection -> Pair.of(
                        extractSignificantPropertyKey(keyValueCollection),
                        extractSignificantPropertyValue(keyValueCollection)
                ))
                .filter(pair -> isNotEmpty(pair.getKey()))
                .filter(pair -> isNotEmpty(pair.getValue()))
                .filter(pair -> !StringUtils.equals(pair.getKey(), SIGNIFICANT_PROPERTY_TYPE_KEY))
                .filter(pair -> !StringUtils.equals(pair.getKey(), SIGNIFICANT_PROPERTY_SUBTYPE_KEY))
                .filter(pair -> !StringUtils.equals(pair.getKey(), SIGNIFICANT_PROPERTY_DUE_DATE))
                .collect(toMap(Pair::getKey, Pair::getValue));

        Map<String, MultipartFile> requestDocumentMap = Arrays.stream(requestDocuments)
                .collect(toMap(
                        multipartFile -> firstNonEmpty(multipartFile.getOriginalFilename(), multipartFile.getName()),
                        multipartFile -> multipartFile
                ));
        Set<String> premisDocumentNamesSet = premisDocumentList.stream()
                .map(File::getOriginalName)
                .map(OriginalNameComplexType::getValue)
                .collect(toSet());

        log.debug("request documents: {}", premisDocumentNamesSet);
        log.debug("sent documents: {}", requestDocumentMap.keySet());

        CollectionUtils.subtract(requestDocumentMap.keySet(), premisDocumentNamesSet).stream()
                .findFirst()
                .ifPresent(fileName -> {
                    throw new LocalizedStatusException(BAD_REQUEST, "message.error_missing_document_in_premis", fileName);
                });
        CollectionUtils.subtract(premisDocumentNamesSet, requestDocumentMap.keySet()).stream()
                .findFirst()
                .ifPresent(fileName -> {
                    throw new LocalizedStatusException(BAD_REQUEST, "message.error_missing_document_in_request", fileName);
                });

        log.debug("createFolder type:{} subtype:{}", type.getId(), subtype.getId());
        log.debug("createFolder requestDocumentMap:{}", requestDocumentMap.keySet());

        // Call the internal request

        CreateFolderRequest createFolderRequest = new CreateFolderRequest();
        createFolderRequest.setName(intellectualEntity.getOriginalName().getValue());
        createFolderRequest.setTypeId(type.getId());
        createFolderRequest.setSubtypeId(subtype.getId());
        createFolderRequest.setDueDate(dueDate);
        createFolderRequest.setMetadata(parsedMetadata);

        List<MultipartFile> mainDocuments = premisDocumentList.stream()
                .filter(file -> getSignificantBooleanPropertyValue(file.getSignificantProperties(), SIGNIFICANT_PROPERTY_MAIN_DOCUMENT_KEY))
                .map(File::getOriginalName)
                .map(OriginalNameComplexType::getValue)
                .map(requestDocumentMap::get)
                .filter(Objects::nonNull)
                .toList();

        List<MultipartFile> annexes = premisDocumentList.stream()
                .filter(file -> !getSignificantBooleanPropertyValue(file.getSignificantProperties(), SIGNIFICANT_PROPERTY_MAIN_DOCUMENT_KEY))
                .map(File::getOriginalName)
                .map(OriginalNameComplexType::getValue)
                .map(requestDocumentMap::get)
                .filter(Objects::nonNull)
                .toList();

        createFolderRequest.setDetachedSignaturesMapping(new HashMap<>());
        List<MultipartFile> detachedSignatureList = new ArrayList<>();

        for (File document : premisDocumentList) {
            //noinspection unchecked
            detachedSignatureList.addAll(document.getSignatureInformation().stream()
                    .map(SignatureInformationComplexType::getContent)
                    .flatMap(Collection::stream)
                    .map(object -> (JAXBElement<SignatureComplexType>) object)
                    .map(JAXBElement::getValue)
                    .map(oi -> {
                        // We have to add it into the current request mapping
                        String currentSignatureId = UUID.randomUUID().toString();
                        safeAddValue(createFolderRequest.getDetachedSignaturesMapping(), document.getOriginalName().getValue(), currentSignatureId);
                        // Convert the String input into a dummy InputStream
                        return new InputStreamMultipartFile(
                                currentSignatureId,
                                new ByteArrayInputStream(oi.getSignatureValue().getBytes()),
                                SignatureFormat.fromPremisValue(oi.getSignatureMethod().getValue()).getMediaType()
                        );
                    })
                    .map(mpf -> (MultipartFile) mpf)
                    .toList());
        }

        Folder result = folderBusinessService.createDraftFolder(tenant, originDesk, mainDocuments, annexes, detachedSignatureList, createFolderRequest);

        if (autoStart) {
            final Folder refreshedResult = workflowService.getFolder(result.getId(), tenant.getId(), false);
            try {
                refreshedResult.getStepList().stream()
                        .filter(task -> task.getAction() == START)
                        .findFirst()
                        .ifPresent(task -> workflowService.performTask(task, START, refreshedResult, originDesk.getId(), new SimpleTaskParams(), null, null));
            } catch (Exception e) {
                log.error("Error while auto starting folder", e);
                deleteFolder(tenant.getId(), tenant, deskId, result.getId(), refreshedResult);
                throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_while_auto_starting_folder");
            }
        }

        return returnRedactedResponse(modelMapper, result, FolderRepresentation::new);
    }


    @GetMapping(API_V1 + "/tenant/{tenantId}/folder/{folderId}")
    @Operation(summary = "Get the current folder and its data")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public FolderDto getFolder(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                               @PathVariable(name = Tenant.API_PATH) String tenantId,
                               @TenantResolved Tenant tenant,
                               @Parameter(description = Folder.API_DOC_ID_VALUE)
                               @PathVariable(name = Folder.API_PATH) String folderId,
                               @RequestParam(required = false) String asDeskId,
                               @RequestParam(required = false, defaultValue = "false") boolean doNotMarkAsRead,
                               @FolderResolved(permission = WORKFLOW_DESK) Folder folder,
                               @CurrentUserResolved User currentUser) {

        log.debug("getFolder tenantId:{} id:{}", tenantId, folderId);

        // Retrieve inner values

        authService.updateFolderReferencedDeskNames(singletonList(folder));
        authService.updateFolderReferencedUserNames(singletonList(folder));
        typologyBusinessService.updateTypology(tenant.getId(), singletonList(folder), false);

        // Retrieve Status info on external Signature

        folder.getStepList()
                .stream()
                .filter(t -> Action.isExternal(t.getAction()))
                .filter(t -> isActive(t.getState()))
                .forEach(t -> putExternalStateInMetadata(tenantId, folder, t));

        Set<Task> currentActiveTasks = folder.getStepList()
                .stream()
                .filter(t -> !Action.isExternal(t.getAction()))
                .filter(t -> !Action.isSecondary(t.getAction()))
                .filter(t -> isActive(t.getState()))
                .collect(toSet());

        // We'll mark the folder as read only if the current user had accessed the folder from one of the current active desks.
        // If anyone has accessed the folder from the previous desk, we won't do anything.

        if (!doNotMarkAsRead && currentActiveTasks.stream()
                .map(Task::getDesks)
                .flatMap(Collection::stream)
                .map(DeskRepresentation::getId)
                .anyMatch(id -> StringUtils.equals(asDeskId, id))) {

            markAsRead(folder, currentUser);
            // Undo task has been revoked, we can simply filter out these.
            folder.getStepList().removeIf(t -> t.getAction() == UNDO);
        }

        folder.getStepList().removeIf(task -> task.getAction() == READ);

        // Retrieve documents infos

        contentService.populateFolderWithAllDocumentTypes(folder);

        // Add missing values

        if (folder.getType() != null) {
            folder.getType().setSignatureFormat(cryptoService.getSignatureFormat(folder.getType(), folder.getDocumentList()));
        }

        // Sending back result

        log.debug("getFolder result:{}", folder);
        return modelMapper.map(folder, FolderDto.class);
    }

    @GetMapping(API_INTERNAL + "/tenant/{tenantId}/folder/by-legacy-id/{legacyId}")
    @Operation(summary = "Get the current folder and its data, from a 'legacyId' (an id set at creation time)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public FolderDto getFolderByLegacyId(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                               @PathVariable(name = Tenant.API_PATH) String tenantId,
                               @TenantResolved Tenant tenant,
                               @Parameter(description = Folder.API_DOC_ID_VALUE)
                               @PathVariable(name = "legacyId") String legacyId,
                               @RequestParam(required = false) String asDeskId,
                               @RequestParam(required = false, defaultValue = "false") boolean doNotMarkAsRead,
                               @CurrentUserResolved User currentUser) {
        log.debug("getFolderByLegacyId legacyId: {}", legacyId);
        Optional<LegacyIdRecord> recordOpt = legacyIdRepository.findById(legacyId);

        String actualFolderId = recordOpt.map(LegacyIdRecord::getFolderId)
                .orElseGet(() -> {
                log.debug("getFolderByLegacyId in fallback lookup");
                List<String> targetDeskIds = permissionService.getCurrentUserViewableDeskIds();
                final FolderFilter dummyFilter = new FolderFilter(null,null,null, legacyId);

                PaginatedList<? extends Folder> tempResult = workflowService.listFoldersForDesks(
                        tenantId,
                        0,
                        MAX_PAGE_SIZE,
                        FOLDER_NAME,
                        dummyFilter,
                        true,
                        targetDeskIds,
                        null,
                        null,
                        null
                );

                return tempResult.getData().stream()
                        .filter(folder -> folder.getLegacyId() != null)
                        .filter(folder -> StringUtils.equals(folder.getLegacyId(), legacyId))
                        .map(Folder::getId)
                        .findFirst()
                        .orElse(null);
        });

        if (actualFolderId == null) {
            log.error("Folder not found for legacyId: {}", legacyId);
            throw new LocalizedStatusException(NOT_FOUND, "message.unknown_folder_id");
        }

        Folder folder = workflowService.getFolder(actualFolderId, tenantId, true);

        return this.getFolder(tenantId, tenant, actualFolderId, null, false, folder, currentUser);
    }


    @PutMapping(API_V1 + "/tenant/{tenantId}/folder/{folderId}")
    @Operation(summary = "Edit folder parameters")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public FolderDto updateFolder(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                  @PathVariable(name = Tenant.API_PATH) String tenantId,
                                  @TenantResolved Tenant tenant,
                                  @Parameter(description = Folder.API_DOC_ID_VALUE)
                                  @PathVariable(value = Folder.API_PATH) String folderId,
                                  @FolderResolved(permission = CURRENT_DESK) Folder existingFolder,
                                  @RequestBody @Valid FolderDto updatedFolder) {

        log.info("updateFolder id:{} name:{} typology:{}/{}", folderId, updatedFolder.getName(), updatedFolder.getTypeId(), updatedFolder.getSubtypeId());

        // Permission check

        boolean userIsTenantAdminOrMore = KeycloakSecurityUtils.currentUserIsTenantOrSuperAdmin(tenantId);
        boolean deskHasActionRights = userIsTenantAdminOrMore || currentUserHaveFolderActionRightOn(tenantId, existingFolder);

        if (!deskHasActionRights) {
            log.info("updateFolder - user {} does not have folder action right", KeycloakSecurityUtils.getCurrentSessionUserId());
            throw new LocalizedStatusException(FORBIDDEN, "message.you_can_t_perform_an_action_on_this_folder");
        }

        boolean folderIsDraft = existingFolder.getState() == DRAFT;
        boolean folderIsInCreationWorkflow = existingFolder.getStepList().stream()
                .filter(task -> FolderUtils.isActive(task.getState()))
                .anyMatch(task -> task.getWorkflowIndex() == FolderUtils.CREATION_WORKFLOW_INDEX);

        log.debug("updateFolder - folderIsInCreationWorkflow : {}", folderIsInCreationWorkflow);

        if (!folderIsDraft && !folderIsInCreationWorkflow) {
            log.info("updateFolder - folder is not in draft, nor creation workflow abort.");
            throw new LocalizedStatusException(FORBIDDEN, "message.you_can_t_edit_this_folder_in_this_state");
        }

        Folder updateRequest = modelMapper.map(updatedFolder, Folder.class);
        // the Web's edit request only send the name/typeId/subtypeId/dueDate
        // We have to report the existing metadata
        updateRequest.setMetadata(existingFolder.getMetadata());

        Folder result = folderBusinessService.updateFolder(tenant, existingFolder, updateRequest);
        return modelMapper.map(result, FolderDto.class);
    }


    @DeleteMapping(API_STANDARD_V1 + "/tenant/{tenantId}/desk/{deskId}/folder/{folderId}")
    @Operation(
            summary = "Delete folder",
            description = """
                          Deletes the given folder.
                          Note that current pending folders cannot be deleted here. Only drafts/rejected/finished ones.
                          To delete a currently pending folder, you need to go through the admin's access point.
                          """
    )
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_204),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_406, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    @PreAuthorize("hasRole('tenant_' + #tenantId + '_desk_' + #deskId)")
    public void deleteFolder(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                             @PathVariable(name = Tenant.API_PATH) String tenantId,
                             @TenantResolved Tenant tenant,
                             @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                             @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                             // @DeskResolved Desk desk, TODO: use this along with read/write permissions.
                             @Parameter(description = Folder.API_DOC_ID_VALUE)
                             @PathVariable(name = Folder.API_PATH) String folderId,
                             @FolderResolved(permission = CURRENT_DESK) Folder folder) {

        log.info("deleteFolder {}", folder.getId());

        // Permission check

        boolean userIsTenantAdminOrMore = KeycloakSecurityUtils.currentUserIsTenantOrSuperAdmin(tenantId);
        boolean deskHasActionRights = userIsTenantAdminOrMore || currentUserHaveFolderActionRightOn(tenantId, folder);

        if (!deskHasActionRights) {
            log.info("deleteFolder - user {} does not have folder action right", KeycloakSecurityUtils.getCurrentSessionUserId());
            throw new LocalizedStatusException(FORBIDDEN, "message.you_can_t_perform_an_action_on_this_folder");
        }

        // Integrity check

        if (folder.getState() == PENDING) {
            log.info("deleteFolder - folder is in pending state, abort");
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.this_workflow_is_still_in_a_pending_state");
        }

        // Deleting

        folderBusinessService.deleteFolder(tenantId, folder, false);

        // Building stats result

        Long timeToCompleteInHours = folder.getStepList()
                .stream()
                .filter(t -> (t.getAction() == ARCHIVE) || (t.getAction() == DELETE))
                .findFirst()
                .map(statsService::computeTimeToCompleteInHours)
                .orElse(null);

        statsService.registerFolderAction(tenant, DELETE, folder, folder.getOriginDesk(), timeToCompleteInHours);
    }


    @GetMapping(API_STANDARD_V1 + "/tenant/{tenantId}/desk/{deskId}/{state}")
    @Operation(
            summary = "List folders in the given state",
            description = """
                          Returns every folders of the given state, on the the given desk.
                                                    
                          - DRAFT (yellow pill)     : Non-started folders
                          - PENDING (blue pill)     : Every pending folders on current desk
                          - LATE (red pill)         : Same as PENDING, but only late ones
                          - DELEGATED (purple pill) : Every pending folders that can be acted upon by delegation
                          - FINISHED (green pill)   : Every finished workflow, ready to export
                          - REJECTED (black pill)   : Every rejected workflow, ready to delete
                          - RETRIEVABLE             : Available UNDO to perform
                          - DOWNSTREAM              : Every folders that had an action performed by the current desk
                          """
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    // @PreAuthorize("hasRole('" + DESK_ROLE_PREFIX + "' + #deskId)")
    public StandardApiPageImpl<FolderRepresentation> listFolders(@Parameter(description = Tenant.API_DOC_ID_VALUE, required = true)
                                                                 @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                                 @TenantResolved Tenant tenant,
                                                                 @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                                                 @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                                                 @PathVariable State state,
                                                                 @DeskResolved Desk desk,
                                                                 @Parameter(description = Type.API_DOC_ID_VALUE)
                                                                 @RequestParam(required = false) String typeId,
                                                                 @Parameter(description = Subtype.API_DOC_ID_VALUE)
                                                                 @RequestParam(required = false) String subtypeId,
                                                                 @PageableDefault(sort = CREATION_DATE_VALUE)
                                                                 @ParameterObject Pageable pageable) {

        // We don't want to add any kind of FolderFilter in the standard API.
        // We restrict the allowed filters to these 2 parameters, and build an "internal" FolderFilter with it.
        FolderFilterDto folderFilter = new FolderFilterDto();
        folderFilter.setTypeId(typeId);
        folderFilter.setSubtypeId(subtypeId);

        Page<FolderRepresentation> internalResult = this.listFolders(tenantId, tenant, deskId, state, desk, folderFilter, pageable)
                .map(folder -> modelMapper.map(folder, FolderRepresentation.class));

        return new StandardApiPageImpl<>(internalResult);
    }


    @PostMapping(API_INTERNAL + "/tenant/{tenantId}/desk/{deskId}/search/{state}")
    @Operation(summary = "Query folders")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    // @PreAuthorize("hasRole('" + DESK_ROLE_PREFIX + "' + #deskId)")
    public Page<FolderDto> listFolders(@Parameter(description = Tenant.API_DOC_ID_VALUE, required = true)
                                       @PathVariable(name = Tenant.API_PATH) String tenantId,
                                       @TenantResolved Tenant tenant,
                                       @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                       @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                       @PathVariable State state,
                                       @DeskResolved Desk desk,
                                       @RequestBody FolderFilterDto folderFilterDto,
                                       @PageableDefault(sort = CREATION_DATE_VALUE)
                                       @ParameterObject Pageable pageable) {

        log.debug("listFolders desk:{} state:{}", desk.getName(), state);

        if (pageable.getPageSize() > MAX_PAGE_SIZE) {
            log.error("Request specify a pageSize of {}, which is superior to the max value: {}", pageable.getPageSize(), MAX_PAGE_SIZE);
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.exceeded_max_page_size", MAX_PAGE_SIZE);
        }

        FolderFilter folderFilter = modelMapper.map(folderFilterDto, FolderFilter.class);

        // Permission request

        List<DelegationRule> delegationRules = (state == DELEGATED) ? getDelegationRules(desk) : emptyList();

        // Request

        FolderSortBy sortBy = RequestUtils.getFirstOderSort(pageable, FolderSortBy.class, CREATION_DATE);

        Page<Folder> folders = switch (sortBy) {

            // Special case : Sorting by Typology needs a cross-DB request
            // The total is still retrieved from the Workflow service, though.
            case TYPE_NAME, SUBTYPE_NAME -> {
                List<Folder> content = dbService.getFoldersSortedByTypologyName(desk, state, pageable);
                long totalCount = computeTotal(
                        content,
                        pageable.getPageNumber(),
                        pageable.getPageSize(),
                        () -> workflowService.countFolders(desk.getId(), state)
                );
                yield new PageImpl<>(content, pageable, totalCount);
            }

            // Regular case:
            // We go through the Workflow service
            default -> workflowService.listFoldersByState(desk.getId(), state, delegationRules, folderFilter, pageable);
        };

        // Setup proper values

        typologyBusinessService.updateTypology(tenant.getId(), folders.getContent(), false);
        authService.updateFolderReferencedDeskNames(folders.getContent());
        authService.updateFolderReferencedUserNames(folders.getContent());

        String currentUserId = KeycloakSecurityUtils.getCurrentSessionUserId();
        folders.getContent().stream()
                .filter(folder -> CollectionUtils.isNotEmpty(folder.getReadByUserIds()))
                .filter(folder -> folder.getReadByUserIds().contains(currentUserId))
                .forEach(folder -> folder.setReadByCurrentUser(true));

        // Sending back the result

        log.debug("listTasks deskId:{} list:{}", desk.getId(), folders.getContent().stream().map(Folder::getName).toList());
        Page<FolderDto> result = new PageImpl<>(
                folders.getContent().stream().map(f -> modelMapper.map(f, FolderDto.class)).toList(),
                folders.getPageable(),
                folders.getTotalElements()
        );

        return result;
    }


    // </editor-fold desc="Folder CRUDL">


    @PostMapping(API_INTERNAL + "/tenant/{tenantId}/desk/{deskId}/folder/{state}")
    @Operation(description = "List folders, narrowing the result data to the given columns.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<FolderDto> listFolderWithColumns(@Parameter(description = Tenant.API_DOC_ID_VALUE, required = true)
                                                 @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                 @TenantResolved Tenant tenant,
                                                 @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                                 @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                                 @DeskResolved Desk desk,
                                                 @PathVariable State state,
                                                 @Parameter(name = "body", description = "Every field is mandatory")
                                                 @RequestBody ColumnedTaskListRequest columnedTaskListRequest,
                                                 @PageableDefault(sort = FolderSortBy.Constants.FOLDER_NAME_VALUE)
                                                 @ParameterObject Pageable pageable) {

        log.debug("listFolders tenantId:{} columnedTaskListRequest:{} pageable:{}", tenantId, columnedTaskListRequest, pageable);

        // Integrity check

        if (columnedTaskListRequest.fromDeskIds().size() != 1 || !desk.getId().equals(columnedTaskListRequest.fromDeskIds().get(0))) {
            throw new ResponseStatusException(NOT_ACCEPTABLE, "The desk id in the request body should be the same as the one in the request path.");
        }

        if (state != columnedTaskListRequest.state()) {
            throw new ResponseStatusException(NOT_ACCEPTABLE, "The state in the request body is different than the one in the request path.");
        }

        Set<String> metadataFiltered = columnedTaskListRequest.metadataValueFilterMap().keySet();
        Set<String> metadataIncluded = new HashSet<>(columnedTaskListRequest.includeMetadata());
        Collection<String> nonIncludedMetadata = CollectionUtils.removeAll(metadataFiltered, metadataIncluded);

        if (CollectionUtils.isNotEmpty(nonIncludedMetadata)) {
            throw new ResponseStatusException(NOT_ACCEPTABLE, "Some filtered metadata is not included: %s".formatted(nonIncludedMetadata));
        }

        // TODO patch saved values to unify TaskViewColumn and FolderSortBy
        String originalSortBy = pageable.getSort().stream()
                .map(Sort.Order::getProperty)
                .findFirst()
                .orElse(EMPTY);

        boolean isAscending = Objects.requireNonNull(pageable.getSort().getOrderFor(originalSortBy)).getDirection().isAscending();

        String sortBy = pageable.getSort().stream()
                .map(Sort.Order::getProperty)
                .findFirst()
                .map(sort -> switch (sort) {
                    case "LIMIT_DATE" -> FolderSortBy.LATE_DATE.name();
                    case "ORIGIN_DESK" -> FolderSortBy.EMITTER_DESK_NAME.name();
                    case "ORIGIN_USER" -> FolderSortBy.EMITTER_USER_NAME.name();
                    case "STATE" -> FolderSortBy.ACTION_TYPE.name();
                    case "SUBTYPE" -> FolderSortBy.SUBTYPE_NAME.name();
                    case "TYPE" -> FolderSortBy.TYPE_NAME.name();
                    default -> sort;
                })
                .orElse(EMPTY);

        Sort sort = Sort.by(sortBy).descending();
        if (isAscending) {
            sort = Sort.by(sortBy).ascending();
        }
        Pageable updatedPageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);

        boolean isUuid = TextUtils.isUuid(sortBy);
        boolean isSortByInIncludeMetadata = CollectionUtils.containsAny(columnedTaskListRequest.includeMetadata(), sortBy);

        if (isUuid && !isSortByInIncludeMetadata) {
            throw new ResponseStatusException(NOT_ACCEPTABLE, "Some sorted metadata is not included: %s".formatted(sortBy));
        }

        // TODO simplify with just the values() when we don't need  CURRENT_DESK_VALUE anymore
        List<String> sortByValues = Arrays.stream(FolderSortBy.values()).map(Enum::name).collect(toMutableList());
        sortByValues.add(CURRENT_DESK_VALUE);
        if (sortByValues.stream().noneMatch(enumValue -> StringUtils.equals(sortBy, enumValue))) {
            log.error("the value '{}' is not part of folder sortBy", sortBy);
            throw new ResponseStatusException(NOT_ACCEPTABLE, "The sort asked ('%s') is not handled".formatted(sortBy));
        }

        List<MetadataRepresentation> includedMetadata = metadataRepository
                .findAllByTenant_IdAndIdIn(tenantId, columnedTaskListRequest.includeMetadata(), unpaged())
                .map(metadata -> modelMapper.map(metadata, MetadataRepresentation.class))
                .getContent();

//        boolean isNotPending = state != State.PENDING;
//        boolean isMultiGroup = CollectionUtils.size(columnedTaskListRequest.groupIds()) > 1;
//        if (isMultiGroup && isNotPending) {
//            throw new ResponseStatusException(BAD_REQUEST, "Multi-group request is only allowed in PENDING state");
//        }

        // Actual request

        List<DelegationRule> delegationRules = (state == DELEGATED) ? getDelegationRules(desk) : emptyList();
        List<String> deskIds = singletonList(desk.getId());

        String currentUserId = KeycloakSecurityUtils.getCurrentSessionUserId();
        List<Folder> resultFolderList = dbService
                .getColumnedFolderList(tenantId, deskIds, delegationRules, currentUserId, columnedTaskListRequest, includedMetadata, updatedPageable);
        long totalCount = computeTotal(
                resultFolderList,
                updatedPageable.getPageNumber(),
                updatedPageable.getPageSize(),
                () -> dbService.getColumnedFolderCount(tenantId, deskIds, delegationRules, currentUserId, columnedTaskListRequest, includedMetadata)
        );

        // Fetch missing values

        if (state == DELEGATED) {
            resultFolderList.stream()
                    .map(Folder::getStepList)
                    .flatMap(Collection::stream)
                    .forEach(task -> task.setState(CURRENT));
        }

        deskBusinessService.updateFoldersInnerDeskValues(resultFolderList);
        typologyBusinessService.updateTypology(tenant.getId(), resultFolderList, false);
        userBusinessService.updateInnerUserValues(resultFolderList);

        // TODO: Use this when the emitter USER will be in the model.
        //  For now, it is only the emitter DESK that is used, and I did the job too soon.
        // userBusinessService.updateInnerUsersValues(resultFolderList);

        return new PageImpl<>(resultFolderList, updatedPageable, totalCount)
                .map(folder -> modelMapper.map(folder, FolderDto.class));
    }


    @GetMapping(API_STANDARD_V1 + "/tenant/{tenantId}/desk/{deskId}/folder/{folderId}/zip")
    @Operation(summary = "Get every file as ZIP, with a PREMIS-formatted summary")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200, content = @Content(mediaType = APPLICATION_OCTET_STREAM_VALUE)),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void downloadFolderZip(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                  @PathVariable(name = Tenant.API_PATH) String tenantId,
                                  @TenantResolved Tenant tenant,
                                  @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                  @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                  // @DeskResolved Desk desk, TODO: use this along with read permissions.
                                  @Parameter(description = Folder.API_DOC_ID_VALUE)
                                  @PathVariable(name = Folder.API_PATH) String folderId,
                                  @FolderResolved(permission = WORKFLOW_DESK) Folder folder,
                                  @CurrentUserResolved User currentUser,
                                  @NotNull HttpServletResponse response) {

        log.debug("downloadFolderZip tenantId:{} folderId:{}", tenant.getId(), folder.getId());

        // Update inner values

        contentService.populateFolderWithAllDocumentTypes(folder);
        userBusinessService.updateInnerUserValues(singletonList(folder));
        deskBusinessService.updateFoldersInnerDeskValues(singletonList(folder));
        typologyBusinessService.updateTypology(tenant.getId(), singletonList(folder), false);

        signatureReportBusinessService.addSignatureProofAsAnnexe(folder);
        folderBusinessService.prepareChecksumDataForPremisFile(folder.getDocumentList());

        // Generate an (in-memory) temp Premis file

        String premisContent = folderBusinessService.generatePremisContent(folder);

        // Trigger the client download

        folderBusinessService.downloadFolderAsZip(response, tenant, folder, premisContent);
    }


    private @NotNull List<DelegationRule> getDelegationRules(@NotNull Desk desk) {

        List<DelegationRule> delegationRules = new ArrayList<>();

        Map<String, Desk> delegatingRoles = new HashMap<>();
        Optional.of(permissionService.getActiveDelegationsToDesksForCurrentUser(Set.of(desk.getId())))
                .map(a -> a.get(desk.getId()))
                .filter(CollectionUtils::isNotEmpty)
                .map(ArrayList::new)
                .ifPresent(desk::setDelegationRules);

        // TODO check that this is not useless
        ofNullable(desk.getDelegatingDesks())
                .orElse(emptyList())
                .forEach(d -> delegatingRoles.put(d.getId(), d));

        ofNullable(desk.getDelegationRules())
                .ifPresent(delegationRules::addAll);

        delegationRules.addAll(
                delegatingRoles.keySet().stream()
                        .map(d -> new DelegationRule(d, null, null, null))
                        .toList()
        );

        return delegationRules;
    }


    @GetMapping(API_STANDARD_V1 + "/tenant/{tenantId}/desk/{deskId}/folder/{folderId}/premis")
    @Operation(
            summary = "Get a PREMIS-formatted file, containing the folder summary",
            description = """
                          The same PREMIS than the one in the ZIP file.
                                                    
                          Please note that in the PREMIS 3.0 format, some nodes aren't mandatory, most can be empty.<br>
                          Please refer to <a href="https://www.loc.gov/standards/premis/premis.xsd">the public XSD validator file</a>.
                                                    
                          Existing nodes (`<event>`, `<agent>`...) will always be returned the same way, but additional nodes can (and will) be added along the software lifespan.<br>
                          New data can have different structure, or different content.<br>
                          Everything should be parsed with fail-safe default behavior.
                                                    
                          Here's an exemple of a single `<event>`:
                          ```
                          <event>
                            <eventIdentifier>
                              <eventIdentifierType>taskId</eventIdentifierType>
                              <eventIdentifierValue>dc6559f8-dfbc-11ef-a1cd-0242ac120015</eventIdentifierValue>
                            </eventIdentifier>
                            <eventType>VISA</eventType>
                            <eventDateTime>2025-01-31T11:19:32.725+0100</eventDateTime>
                            <linkingAgentIdentifier>
                              <linkingAgentIdentifierType>userId</linkingAgentIdentifierType>
                              <linkingAgentIdentifierValue>5f028e82-47aa-4957-abb4-761c1a62a321</linkingAgentIdentifierValue>
                              <linkingAgentRole>Bureau du maire</linkingAgentRole>
                            </linkingAgentIdentifier>
                          </event>
                          ```
                                                    
                          Please note that:
                          - Other `<eventIdentifierType>` than `taskId` may appear in the future, please parse the data with fail-safe behaviors.
                          - The `<eventType>` will be set with an `Action` enum value. New enum values can appear in the future, it should be parsed it with a fail-safe default case.
                          - Non-empty `<eventDateTime>` nodes will always be set with an ISO8601 formatted string.
                          - The `<linkingAgentIdentifier>` and `<linkingAgentRole>` are not mandatory nodes.
                          - The user ID set in `linkingAgentIdentifierValue` can be mapped to one of the `<agent>` nodes.
                          - No `<event>` at all can occur, on a short period of time between the draft folder creation, and before the workflow initialization.
                          - Every mandatory node can be empty.
                                                    
                          Tasks states can be determined as:
                          - Performed (past) will have non-empty `eventIdentifierValue`, `linkingAgentIdentifierValue` and `eventDateTime`.
                          - Pending (current) will have a non-empty `eventIdentifierValue` and an empty `linkingAgentIdentifierValue` and `eventDateTime`.
                          - Upcoming (future) will have empty `eventIdentifierValue`, `linkingAgentIdentifierValue` and `eventDateTime`.
                                                    
                          Here's an exemple of an `<agent>` node:
                          ```
                          <agent>
                            <agentIdentifier>
                              <agentIdentifierType>userId</agentIdentifierType>
                              <agentIdentifierValue>5f028e82-47aa-4957-abb4-761c1a62a321</agentIdentifierValue>
                            </agentIdentifier>
                            <agentName>Jean DUPONT</agentName>
                          </agent>
                          ```
                                                    
                          Please note that:
                          - Other `<agentIdentifierType>` than `userId` ay appear in the future, please parse the data with fail-safe behaviors.
                          - The `<agentName>` may be empty, if a user has been forcefully deleted from the iparapheur.
                          - The `<agentName>` is not a mandatory node.
                          - No `<agent>` node at all can occur, on a draft folder.
                          - Every mandatory node can be empty.
                          """
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200, content = @Content(mediaType = APPLICATION_XML_VALUE)),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void downloadFolderPremis(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                     @PathVariable(name = Tenant.API_PATH) String tenantId,
                                     @TenantResolved Tenant tenant,
                                     @Parameter(description = DeskRepresentation.API_DOC_ID_VALUE)
                                     @PathVariable(name = DeskRepresentation.API_PATH) String deskId,
                                     @Parameter(description = Folder.API_DOC_ID_VALUE)
                                     @PathVariable(name = Folder.API_PATH) String folderId,
                                     @FolderResolved(permission = WORKFLOW_DESK) Folder folder,
                                     @CurrentUserResolved User currentUser,
                                     @NotNull HttpServletResponse response) {

        log.debug("downloadFolderPremis tenantId:{} folderId:{}", tenant.getId(), folder.getId());

        // Update inner values

        contentService.populateFolderWithAllDocumentTypes(folder);
        typologyBusinessService.updateTypology(tenant.getId(), singletonList(folder), false);
        userBusinessService.updateInnerUserValues(singletonList(folder));

        // Generate an (in-memory) temp Premis file

        folderBusinessService.prepareChecksumDataForPremisFile(folder.getDocumentList());
        deskBusinessService.updateFoldersInnerDeskValues(singletonList(folder));
        String premisContent = folderBusinessService.generatePremisContent(folder);

        // Trigger the client download

        response.setContentType(APPLICATION_XML_VALUE);
        try (OutputStream outputStream = response.getOutputStream();
             ByteArrayInputStream inputStream = new ByteArrayInputStream(premisContent.getBytes(UTF_8))) {
            IOUtils.copy(inputStream, outputStream);
        } catch (IOException e) {
            log.error("Error while writing premis file : {}", e.getMessage());
            log.debug("Error details : ", e);
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_generating_premis_file");
        }
    }


    @GetMapping(API_V1 + "/tenant/{tenantId}/folder/{folderId}/ipngProofs")
    @Operation(summary = "Get the current folder and its data", hidden = HIDE_UNSTABLE_API)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given folderId does not exist")
    })
    public List<IpngFolderProofs> getFolderIpngProofs(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                      @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                      @TenantResolved Tenant tenant,
                                                      @Parameter(description = Folder.API_DOC_ID_VALUE)
                                                      @PathVariable(name = Folder.API_PATH) String folderId,
//                                                    @RequestParam(required = false) String asDeskId,
                                                      @FolderResolved(permission = WORKFLOW_DESK) Folder folder,
                                                      @CurrentUserResolved User currentUser) {

        return folderBusinessService.gatherIpngProofsForFolder(folder);
    }


    @GetMapping(API_V1 + "/tenant/{tenantId}/folder/{folderId}/ipngProofsFile")
    @Operation(summary = "Get the current folder and its data", hidden = HIDE_UNSTABLE_API)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given folderId does not exist")
    })
    public void getFolderIpngProofsFile(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                        @PathVariable(name = Tenant.API_PATH) String tenantId,
                                        @TenantResolved Tenant tenant,
                                        @Parameter(description = Folder.API_DOC_ID_VALUE)
                                        @PathVariable(name = Folder.API_PATH) String folderId,
                                        @FolderResolved(permission = WORKFLOW_DESK) Folder folder,
                                        @CurrentUserResolved User currentUser,
                                        HttpServletResponse response) throws IOException {

        Path tempFileResult = folderBusinessService.createIpngProofFileForFolder(folder);
        try (InputStream is = Files.newInputStream(tempFileResult);
             OutputStream os = response.getOutputStream()) {
            response.setHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE);
            IOUtils.copy(is, os);
        }
    }


    /**
     * @param folder      with a {@link Folder#getStepList()} properly available
     * @param currentUser
     */
    private void markAsRead(@NotNull Folder folder, @NotNull User currentUser) {
        log.debug("markAsRead folderId:{} currentUser:{}", folder.getId(), currentUser.getUserName());
        List<Task> readTasks = workflowService.getReadTasks(folder);

        Pair<Long, Long> currentIndex = folder.getStepList().stream()
                .filter(t -> t.getState() == CURRENT)
                .map(t -> Pair.of(t.getWorkflowIndex(), t.getStepIndex()))
                .findFirst()
                .orElse(Pair.of(null, null));

        boolean hasAlreadyBeenReadThisStep = workflowService.hasAlreadyBeenReadAtIndex(currentIndex, currentUser, readTasks);
        log.debug("markAsRead hasAlreadyBeenReadThisStep:{}", hasAlreadyBeenReadThisStep);
        if (!hasAlreadyBeenReadThisStep) {
            readTasks.stream()
                    .filter(t -> t.getDate() == null)
                    .findFirst()
                    .ifPresent(t -> workflowService.performTask(t, READ, folder, null, null, null, null));
        }
    }


    private void putExternalStateInMetadata(String tenantId, Folder folder, Task task) {

        if (folder.getSubtype() == null) {
            return;
        }

        switch (task.getAction()) {
            case EXTERNAL_SIGNATURE -> {
                ExternalSignatureConfig extSignConfig = folder.getSubtype().getExternalSignatureConfig();
                String transactionId = task.getExternalSignatureProcedureId();
                if (transactionId != null && extSignConfig != null) {
                    Status status = externalSignatureService.getProcedureStatus(extSignConfig, transactionId);
                    task.getMetadata().put(METADATA_STATUS, status.name());
                } else if (extSignConfig != null) {
                    task.getMetadata().put(METADATA_STATUS, FORM.name());
                } else {
                    task.getMetadata().put(METADATA_STATUS, ERROR.name());
                }
            }
            case SECURE_MAIL -> {
                if (Objects.nonNull(folder.getSubtype().getSecureMailServerId())
                        && Objects.nonNull(task.getMetadata().get(METADATA_PASTELL_DOCUMENT_ID))) {
                    String lastAction;
                    try {
                        lastAction = secureMailService.findDocument(
                                folder.getSubtype().getSecureMailServerId().toString(),
                                task.getMetadata().get(METADATA_PASTELL_DOCUMENT_ID)
                        ).getLastAction();
                    } catch (WebClientResponseException e) {
                        lastAction = "ERROR";
                    }

                    task.getMetadata().put(METADATA_STATUS, lastAction);

                }
            }
            case IPNG -> {
                String ipngBusinessId = task.getMetadata().get(METADATA_IPNG_BUSINESS_ID);
                if (ipngBusinessId != null) {
                    Optional<PendingIpngFolder> pendingIpngOpt = this.pendingIpngFolderRepository.findById(new PendingIpngFolderKey(ipngBusinessId, tenantId));
                    pendingIpngOpt.ifPresent(pendingIpngData -> {
                        Task.ExternalState extState = switch (pendingIpngData.getState()) {
                            case SENT -> Task.ExternalState.CREATED;
                            case IN_NETWORK -> Task.ExternalState.SENT;
                            case RECEIVED -> Task.ExternalState.RECEIVED;
                            default -> Task.ExternalState.ERROR;
                        };
                        task.getMetadata().put(METADATA_STATUS, extState.name());
                    });
                }
            }
        }

        setTaskState(task);
    }


    @GetMapping(API_V1 + "/tenant/{tenantId}/folder/{folderId}/historyTasks")
    @Operation(summary = "Get the current folder and its data", hidden = HIDE_UNSTABLE_API)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public List<Task> getFolderHistoryTasks(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                            @PathVariable(name = Tenant.API_PATH) String tenantId,
                                            @Parameter(description = Folder.API_DOC_ID_VALUE)
                                            @PathVariable(name = Folder.API_PATH) String folderId,
                                            @FolderResolved(permission = WORKFLOW_DESK) Folder folder) {

        log.debug("getFolderHistoryTasks folderId:{}", folder.getId());

        List<Task> tasks = workflowService.getHistoricTasks(folderId);
        authService.updateTaskReferencedDeskNames(tasks);
        authService.updateTaskReferencedUserNames(tasks);
        parseChainStepsForHistory(tasks);
        parseRecycleStepsForHistory(tasks);

        log.debug("getFolderHistoryTasks result:{}", tasks);
        return tasks;
    }


    @PostMapping(API_INTERNAL + "/tenant/{tenantId}/folder/{folderId}/mail")
    @Operation(summary = "Send given folder by mail")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void sendFolderByMail(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                 @PathVariable(name = Tenant.API_PATH) String tenantId,
                                 @TenantResolved Tenant tenant,
                                 @Parameter(description = Folder.API_DOC_ID_VALUE)
                                 @PathVariable(name = Folder.API_PATH) String folderId,
                                 @FolderResolved(permission = WORKFLOW_DESK) Folder folder,
                                 @RequestBody @Valid MailParams params,
                                 @CurrentUserResolved User currentUser) {

        log.info("sendFolder id:{} mail:{}", folderId, params);

        // Fetch missing infos

        contentService.populateFolderWithAllDocumentTypes(folder);
        userBusinessService.updateInnerUserValues(singletonList(folder));
        deskBusinessService.updateFoldersInnerDeskValues(singletonList(folder));
        typologyBusinessService.updateTypology(tenant.getId(), singletonList(folder), false);

        // Prepare mail

        MailTarget mailTarget = new MailTarget(params);

        Resource mailTemplateResource = Optional.ofNullable(tenant.getCustomTemplates())
                .filter(t -> t.containsKey(MAIL_ACTION_SEND))
                .map(i -> contentService.getCustomTemplate(tenant, MAIL_ACTION_SEND))
                .filter(Objects::nonNull)
                .map(RequestUtils::writeBufferToString)
                .map(s -> (Resource) new ByteArrayResource(s.getBytes(UTF_8)))
                .orElse(mailActionSendTemplateResource);

        Desk desk = folder
                .getStepList()
                .stream()
                .filter(s -> s.getState().equals(PENDING))
                .findFirst()
                .map(Task::getDesks)
                // FIXME we should not do a "findfirst" here,
                //  we should instead have a param from the request (like "asDesk" in getFolder)
                .orElse(emptyList()).stream().findFirst().map(DeskRepresentation::getId)
                .map(deskId -> Desk
                        .builder()
                        .id(deskId)
                        .build()
                )
                .orElse(null);

        MailContent mailContent = new MailContent(
                tenant,
                folder,
                null,
                // FIXME remove modelMapper when all internal actions are mapped to Desk in controllers
                modelMapper.map(desk, DeskRepresentation.class),
                null,
                false,
                null,
                false,
                null,
                null);

        Set<String> annexesToIncludeIds =
                folder.getSubtype().isAnnexeIncluded()
                ? folder.getDocumentList().stream()
                        .filter(d -> !d.isMainDocument())
                        .map(Document::getId)
                        .collect(toSet())
                : Collections.emptySet();

        Path printDocTempFile = folderBusinessService.generatePrintDocAsTempFile(tenant, folder, params.isIncludeDocket(), annexesToIncludeIds);
        String targetAttachmentName = "%s.pdf".formatted(FileUtils.sanitizeFileName(folder.getName()));

        try (InputStream printDocInputStream = Files.newInputStream(printDocTempFile)) {
            long printDocSize = Files.size(printDocTempFile);
            if (printDocSize > MAX_MAIL_ATTACHMENT_SIZE) {
                log.error("Cannot send folder by mail, the print document is too heavy for folder {}, size : {}K ", folder.getId(), printDocSize / 1024);
                log.debug("tenant name : {}, folder name : {}", tenant.getName(), folder.getName());
                throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_send_mail_file_too_big");
            }

            notificationService.mailFolder(
                    singletonList(mailTarget),
                    mailTemplateResource,
                    singletonList(mailContent),
                    currentUser,
                    params.getObject(),
                    params.getMessage(),
                    printDocInputStream,
                    targetAttachmentName
            );
        } catch (IOException e) {
            log.error("An error occurred while reading generated print document to send by mail for Tenant : {}, Folder : {}", tenant.getId(), folder.getId());
            log.debug("tenant name : {}, folder name : {}, exception details : ", tenant.getName(), folder.getName(), e);
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.error_reading_file");
        } finally {
            try {
                Files.delete(printDocTempFile);
            } catch (IOException e) {
                log.error("Error deleting temporary print document at path : {}", printDocTempFile.toString());
                log.debug("Error detail : ", e);
            }
        }
    }


    @GetMapping(API_INTERNAL + "/tenant/{tenantId}/folder/{folderId}/print")
    @Operation(summary = "Get the PDF printable version of a folder")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200, content = @Content(mediaType = APPLICATION_PDF_VALUE)),
            @ApiResponse(responseCode = CODE_400, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public void printFolder(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                            @PathVariable(name = Tenant.API_PATH) String tenantId,
                            @TenantResolved Tenant tenant,
                            @Parameter(description = Folder.API_DOC_ID_VALUE)
                            @PathVariable(name = Folder.API_PATH) String folderId,
                            @FolderResolved(permission = WORKFLOW_DESK) Folder folder,
                            @RequestParam(required = false, defaultValue = "true") boolean includeDocket,
                            @RequestParam(required = false) List<String> annexesIds,
                            HttpServletResponse response) {

        log.info("printFolder id:{} includeDocket:{} annexesIds:{}", folderId, includeDocket, annexesIds);

        // Fetch missing infos

        contentService.populateFolderWithAllDocumentTypes(folder);
        userBusinessService.updateInnerUserValues(singletonList(folder));
        deskBusinessService.updateFoldersInnerDeskValues(singletonList(folder));
        typologyBusinessService.updateTypology(tenant.getId(), singletonList(folder), false);

        // Integrity check

        Set<String> existingAnnexesIds = folder.getDocumentList()
                .stream()
                .filter(d -> !d.isMainDocument())
                .map(Document::getId)
                .collect(toSet());

        Set<String> finalAnnexesIds = new HashSet<>(Optional.ofNullable(annexesIds).orElse(emptyList()));
        if (!CollectionUtils.containsAll(existingAnnexesIds, finalAnnexesIds)) {
            throw new LocalizedStatusException(NOT_FOUND, "message.unknown_document_id");
        }

        // Generate document

        Path printDocTempFile = folderBusinessService.generatePrintDocAsTempFile(tenant, folder, includeDocket, finalAnnexesIds);

        ResourceBundle messageResourceBundle = ResourceBundle.getBundle(MESSAGE_BUNDLE, Locale.getDefault());
        String resultFileNameTemplate = messageResourceBundle.getString("message.printable_pdf_name");
        String resultFileName = MessageFormat.format(resultFileNameTemplate, folder.getName());

        response.setHeader(CONTENT_DISPOSITION, "attachment; filename=" + URLEncoder.encode(resultFileName, UTF_8));
        response.setHeader(CONTENT_TYPE, APPLICATION_PDF.toString());

        log.debug("printFolder - merged the doc into temp file : {}", printDocTempFile.toString());
        try (InputStream is = Files.newInputStream(printDocTempFile)) {
            is.transferTo(response.getOutputStream());
            response.flushBuffer();
        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_merging_pdf");
        } finally {
            try {
                Files.delete(printDocTempFile);
            } catch (IOException e) {
                log.error("Error deleting temporary print document at path : {}", printDocTempFile.toString());
                log.debug("Error detail : ", e);
            }
        }
    }


    @PutMapping(API_V1 + "/tenant/{tenantId}/folder/{folderId}/metadata/{metadataId}")
    @Operation(summary = "Update folder's metadata value", hidden = HIDE_UNSTABLE_API)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "400", description = "The given value does not match the subtype values restrictions"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have desk's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given folderId/metadataId does not exist")
    })
    public void setMetadata(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                            @PathVariable(name = Tenant.API_PATH) String tenantId,
                            @TenantResolved Tenant tenant,
                            @Parameter(description = Folder.API_DOC_ID_VALUE)
                            @PathVariable(name = Folder.API_PATH) String folderId,
                            @FolderResolved(permission = CURRENT_DESK) Folder folder,
                            @Parameter(description = Metadata.API_DOC_ID_VALUE)
                            @PathVariable(name = Metadata.API_PATH) String metadataId,
                            @RequestBody StringResult stringRequest) {

        log.info("setMetadata folderId:{} metadataId:{} value:{}", folderId, metadataId, stringRequest.getValue());

        // Permission check

        boolean userIsTenantAdminOrMore = KeycloakSecurityUtils.currentUserIsTenantOrSuperAdmin(tenantId);
        boolean deskHasActionRights = userIsTenantAdminOrMore || currentUserHaveFolderActionRightOn(tenantId, folder);

        if (!deskHasActionRights) {
            log.info("setMetadata - user {} does not have folder action right", KeycloakSecurityUtils.getCurrentSessionUserId());
            throw new LocalizedStatusException(FORBIDDEN, "message.you_can_t_perform_an_action_on_this_folder");
        }

        boolean folderIsDraft = folder.getState() == DRAFT;
        boolean folderIsInCreationWorkflow = folder.getStepList().stream()
                .filter(task -> FolderUtils.isActive(task.getState()))
                .anyMatch(task -> task.getWorkflowIndex() == FolderUtils.CREATION_WORKFLOW_INDEX);

        SubtypeMetadata subtypeMetadata = Optional.ofNullable(folder.getSubtype())
                .map(TypologyEntity::getId)
                .flatMap(subtypeRepository::findById)
                .map(Subtype::getSubtypeMetadataList)
                .orElse(emptyList())
                .stream()
                .filter(subtypeMeta -> StringUtils.equals(metadataId, subtypeMeta.getMetadata().getId()))
                .findFirst()
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_metadata_id"));

        if (!folderIsDraft && !folderIsInCreationWorkflow && !subtypeMetadata.isEditable()) {
            log.info("setMetadata - folder is not in draft nor creation workflow, and metadata is not editable, abort.");
            throw new LocalizedStatusException(FORBIDDEN, "message.you_can_t_edit_this_metadata_in_this_state");
        }

        // Integrity check

        Metadata metadata = subtypeMetadata.getMetadata();

        if (CollectionUtils.isNotEmpty(metadata.getRestrictedValues())
                && metadata.getRestrictedValues().stream().noneMatch(s -> StringUtils.equals(s, stringRequest.getValue()))) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.metadata_given_value_is_not_in_restricted_values");
        }

        // Actual edit

        Folder editRequest = modelMapper.map(folder, Folder.class);
        editRequest.getMetadata().put(metadata.getKey(), stringRequest.getValue());

        folderBusinessService.updateFolder(tenant, folder, editRequest);
    }


    private void setTaskState(Task task) {
        if (Objects.nonNull(task.getMetadata())
                && task.getMetadata().containsKey(METADATA_STATUS)
                && isNotEmpty(task.getMetadata().get(METADATA_STATUS))) {
            task.setExternalState(FolderUtils.mapTaskStatus(task.getMetadata().get(METADATA_STATUS)));
        }
    }


    private void parseChainStepsForHistory(List<Task> tasks) {
        tasks.forEach(task -> {
            if (task.getPerformedAction() != null && task.getPerformedAction().equals(CHAIN)) {
                task.setState(VALIDATED);
                task.setAction(CHAIN);
            }
        });
    }


    /**
     * On recycle, all the pending actions are marked as "refused".
     * On the history, we just want one task marked as "recycled".
     * So this method will delete all duplicates for each recycle, keeping just one for the record.
     *
     * @param tasks the historic task list
     */
    private void parseRecycleStepsForHistory(List<Task> tasks) {

        tasks.forEach(task -> {
            if (task.getPerformedAction() != null && task.getPerformedAction().equals(RECYCLE)) {
                task.setState(VALIDATED);
                task.setAction(RECYCLE);
            }
        });

        for (int i = 0; i < tasks.size() - 1; i++) {

            Task currentTask = tasks.get(i);
            Task nextTask = tasks.get(i + 1);
            if (currentTask.getPerformedAction() != null
                    && currentTask.getPerformedAction().equals(RECYCLE)
                    && nextTask.getPerformedAction() != null
                    && nextTask.getPerformedAction().equals(RECYCLE)) {
                currentTask.setAction(null);
            }
        }

        tasks.removeIf(t -> t.getAction() == null);
    }


    @PostMapping(API_INTERNAL + "/tenant/{tenantId}/folder/{folderId}/signature-report")
    @Operation(summary = "Get the document(s) signature validation report")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_406, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public SignatureProof generateDocumentSignatureVerification(@Parameter(description = Tenant.API_DOC_ID_VALUE)
                                                                @PathVariable(name = Tenant.API_PATH) String tenantId,
                                                                @TenantResolved Tenant tenant,
                                                                @Parameter(description = Folder.API_DOC_ID_VALUE)
                                                                @PathVariable(name = Folder.API_PATH) String folderId,
                                                                @FolderResolved Folder folder,
                                                                @CurrentUserResolved User currentUser) {

        log.info("Get signature validation folderId:{}", folder.getId());

        // Signature report generation

        return signatureReportBusinessService.generateAllSignaturesReport(folder);
    }


    private boolean currentUserHaveFolderActionRightOn(String tenantId, Folder folder) {
        Set<String> concernedDeskIds = folder.getStepList().stream()
                .filter(t -> FolderUtils.isActive(t.getState()))
                .flatMap(t -> t.getDesks().stream())
                .map(DeskRepresentation::getId)
                .collect(toSet());

        return permissionService.currentUserHasFolderActionRightOnSomeDeskIn(
                tenantId,
                concernedDeskIds,
                folder.getType().getId(),
                folder.getSubtype().getId()
        );
    }


}
