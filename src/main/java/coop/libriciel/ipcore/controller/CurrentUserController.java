/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller;

import coop.libriciel.ipcore.business.auth.DeskBusinessService;
import coop.libriciel.ipcore.business.auth.UserBusinessService;
import coop.libriciel.ipcore.business.database.MetadataBusinessService;
import coop.libriciel.ipcore.business.database.TenantBusinessService;
import coop.libriciel.ipcore.business.signatureproof.SignatureReportBusinessService;
import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.HierarchisedDeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.auth.requests.UserDto;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.database.requests.StringResult;
import coop.libriciel.ipcore.model.database.userPreferences.*;
import coop.libriciel.ipcore.model.websockets.DeskCount;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.database.DatabaseServiceInterface;
import coop.libriciel.ipcore.services.database.FolderFilterRepository;
import coop.libriciel.ipcore.services.database.TableLayoutRepository;
import coop.libriciel.ipcore.services.database.UserPreferencesRepository;
import coop.libriciel.ipcore.services.mail.NotificationServiceInterface;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.services.resolvers.CurrentUserResolver.CurrentUserResolved;
import coop.libriciel.ipcore.services.resolvers.TenantResolver.TenantResolved;
import coop.libriciel.ipcore.utils.KeycloakSecurityUtils;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.RequestUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import static coop.libriciel.ipcore.IpCoreApplication.*;
import static coop.libriciel.ipcore.model.database.userPreferences.LabelledColumnType.METADATA;
import static coop.libriciel.ipcore.model.database.userPreferences.TableName.*;
import static coop.libriciel.ipcore.model.database.userPreferences.TaskViewColumn.FOLDER_NAME;
import static coop.libriciel.ipcore.model.permission.DelegationSortBy.Constants.SUBSTITUTE_DESK_VALUE;
import static coop.libriciel.ipcore.services.content.ContentServiceInterface.FILE_TRANSFER_BUFFER_SIZE;
import static coop.libriciel.ipcore.utils.ApiUtils.*;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.tomcat.util.http.fileupload.FileUploadBase.MULTIPART_FORM_DATA;
import static org.springframework.core.io.buffer.DataBufferUtils.readInputStream;
import static org.springframework.http.HttpStatus.*;


@Log4j2
@RestController
@Tag(name = "current-user", description = "User access for regular logged users")
public class CurrentUserController {


    // <editor-fold desc="Beans">


    private final AuthServiceInterface authService;
    private final ContentServiceInterface contentService;
    private final DatabaseServiceInterface dbService;
    private final DeskBusinessService deskBusinessService;
    private final FolderFilterRepository folderFilterRepository;
    private final MetadataBusinessService metadataBusinessService;
    private final ModelMapper modelMapper;
    private final NotificationServiceInterface notificationService;
    private final PermissionServiceInterface permissionService;
    private final SignatureReportBusinessService signatureReportBusinessService;
    private final TableLayoutRepository tableLayoutRepository;
    private final TenantBusinessService tenantBusinessService;
    private final UserBusinessService userBusinessService;
    private final UserPreferencesRepository userPreferencesRepository;


    @Autowired
    public CurrentUserController(AuthServiceInterface authService,
                                 ContentServiceInterface contentService,
                                 DatabaseServiceInterface dbService,
                                 DeskBusinessService deskBusinessService,
                                 FolderFilterRepository folderFilterRepository,
                                 MetadataBusinessService metadataBusinessService,
                                 ModelMapper modelMapper,
                                 NotificationServiceInterface notificationService,
                                 PermissionServiceInterface permissionService,
                                 SignatureReportBusinessService signatureReportBusinessService,
                                 TableLayoutRepository tableLayoutRepository,
                                 TenantBusinessService tenantBusinessService,
                                 UserBusinessService userBusinessService,
                                 UserPreferencesRepository userPreferencesRepository) {

        this.authService = authService;
        this.contentService = contentService;
        this.dbService = dbService;
        this.deskBusinessService = deskBusinessService;
        this.folderFilterRepository = folderFilterRepository;
        this.metadataBusinessService = metadataBusinessService;
        this.modelMapper = modelMapper;
        this.notificationService = notificationService;
        this.permissionService = permissionService;
        this.signatureReportBusinessService = signatureReportBusinessService;
        this.tableLayoutRepository = tableLayoutRepository;
        this.tenantBusinessService = tenantBusinessService;
        this.userBusinessService = userBusinessService;
        this.userPreferencesRepository = userPreferencesRepository;
    }


    // </editor-fold desc="Beans">


    @GetMapping(API_V1 + "/currentUser")
    @Operation(summary = "Get current user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public UserDto getCurrentUser(@CurrentUserResolved User currentUser) {
        log.debug("getCurrentUser userId:{}", currentUser.getId());

        UserDto result = modelMapper.map(currentUser, UserDto.class);

        // Fetch missing values

        userBusinessService.updateGlobalInnerValues(result);
        tenantBusinessService.updateInnerTenantValues(result);

        return result;
    }


    /**
     * TODO: Move this into the DeskController
     */
    @GetMapping(API_V1 + "/currentUser/managedDesks")
    @Operation(
            summary = "List delegation-managed desks",
            description = "Returns every desks attached to the current user, and those for which the user is delegation manager."
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<DeskRepresentation> getManagedDesks(@CurrentUserResolved User currentUser,
                                                    @PageableDefault(sort = SUBSTITUTE_DESK_VALUE)
                                                    @ParameterObject Pageable pageable,
                                                    @Parameter(description = "Searching for a specific desk name")
                                                    @RequestParam(required = false) String searchTerm) {

        Map<String, Desk> deskIdNameMap = KeycloakSecurityUtils
                .getCurrentUserDeskIds()
                .stream()
                .collect(toMap(id -> id, Desk::new));

        permissionService.getDelegationManagedDesks(currentUser.getId(), null)
                .forEach(desk -> deskIdNameMap.put(desk.getId(), new Desk(desk)));

        int userDeskTotal = deskIdNameMap.values().size();

        List<DeskRepresentation> userDeskList = deskBusinessService.populateAndPaginateDeskList(pageable, searchTerm, deskIdNameMap)
                .stream()
                .map(desk -> modelMapper.map(desk, DeskRepresentation.class))
                .toList();

        return new PageImpl<>(userDeskList, pageable, userDeskTotal);
    }


    /**
     * TODO: Move this into the DeskController
     */
    @GetMapping(API_V1 + "/currentUser/desks")
    @Operation(
            summary = "List desks",
            description = "Returns every desks attached to the current user, and those for which the user is supervisor."
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<DeskRepresentation> getDesks(@Parameter(description = "Searching for a specific desk name")
                                             @RequestParam(required = false) String searchTerm,
                                             @CurrentUserResolved User currentUser,
                                             @PageableDefault(sort = SUBSTITUTE_DESK_VALUE)
                                             @ParameterObject Pageable pageable) {

        // Computing user deskId list

        Map<String, Desk> deskIdNameMap = KeycloakSecurityUtils.getCurrentUserDeskIds()
                .stream()
                .collect(toMap(id -> id, Desk::new));

        permissionService.getSupervisedDesks(currentUser.getId(), null)
                .forEach(dr -> deskIdNameMap.put(dr.getId(), new Desk(dr)));

        int userDeskTotal = deskIdNameMap.values().size();

        List<Desk> userDeskList = deskBusinessService.populateAndPaginateDeskList(pageable, searchTerm, deskIdNameMap);

        log.trace("getDesks paginated desk list:{}", userDeskList);

        return new PageImpl<>(
                userDeskList.stream().map(d -> modelMapper.map(d, DeskRepresentation.class)).toList(),
                pageable,
                userDeskTotal
        );
    }


    /**
     * TODO: Move this into the DeskController
     */
    @GetMapping(API_V1 + "/currentUser/desk/folderCount")
    @Operation(
            summary = "List desks folder count",
            description = "Returns the folder count for every desks attached to the current user, and those for which the user is supervisor."
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<DeskCount> getDesksFolderCount(@Parameter(description = "Searching for a specific desk name")
                                               @RequestParam(required = false) String searchTerm,
                                               @CurrentUserResolved User currentUser,
                                               @PageableDefault(sort = SUBSTITUTE_DESK_VALUE)
                                               @ParameterObject Pageable pageable) {
        Map<String, Desk> deskIdNameMap = KeycloakSecurityUtils.getCurrentUserDeskIds()
                .stream()
                .collect(toMap(id -> id, Desk::new));

        permissionService.getSupervisedDesks(currentUser.getId(), null)
                .forEach(dr -> deskIdNameMap.put(dr.getId(), new Desk(dr)));

        int total = deskIdNameMap.values().size();

        List<Desk> userDeskList = deskBusinessService.populateAndPaginateDeskList(pageable, searchTerm, deskIdNameMap);

        deskBusinessService.populateCountsAndDelegations(deskIdNameMap, userDeskList);
        // To Desk Counts

        List<DeskCount> deskCounts = userDeskList
                .stream()
                .map(deskBusinessService::getDeskCount)
                .toList();

        return new PageImpl<>(
                deskCounts,
                pageable,
                total
        );
    }


    /**
     * TODO: Move this into the DeskController
     */
    @GetMapping(API_V1 + "/currentUser/administeredDesks")
    @Operation(summary = "List desks", description = "Returns every desks attached to the current functional admin user.", hidden = HIDE_UNSTABLE_API)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "A JSON object containing data and pagination infos"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
    })
    public List<DeskRepresentation> getAdministeredDesks() {

        String userId = KeycloakSecurityUtils.getCurrentSessionUserId();
        log.debug("getAdministeredDesks current user:{}", userId);

        List<DeskRepresentation> result = permissionService.getAdministeredDesks(userId, null);
        log.debug("getAdministeredDesks list:{}", result);

        authService.updateDeskNames(result);

        return result;
    }


    /**
     * TODO: Move this into the DeskController
     */
    @GetMapping(API_V1 + "/currentUser/administeredDesksForTenant/{tenantId}")
    @Operation(summary = "List desks", description = "Returns every desks administered by this user for this tenant.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public Page<HierarchisedDeskRepresentation> getAdministeredDesksForTenant(@PathVariable String tenantId,
                                                                              @TenantResolved Tenant tenant,
                                                                              @PageableDefault(sort = SUBSTITUTE_DESK_VALUE)
                                                                              @ParameterObject Pageable pageable,
                                                                              @Parameter(description = "Searching for a specific desk name")
                                                                              @RequestParam(required = false) String searchTerm,
                                                                              @CurrentUserResolved User currentUser) {
        String userId = currentUser.getId();
        log.debug("getAdministeredDesksForTenant current user : {}", userId);

        List<DeskRepresentation> administeredDesks = permissionService.getAdministeredDesks(userId, tenantId);
        log.debug("getAdministeredDesksForTenant list : {}", administeredDesks);

        Map<String, Desk> deskIdNameMap = new HashMap<>();
        administeredDesks.forEach(desk -> deskIdNameMap.put(desk.getId(), new Desk(desk)));
        List<Desk> result = deskBusinessService.populateAndPaginateDeskList(pageable, searchTerm, deskIdNameMap);

        return new PageImpl<>(
                result.stream().map(desk -> modelMapper.map(desk, HierarchisedDeskRepresentation.class)).toList(),
                pageable,
                administeredDesks.size()
        );
    }


    /**
     * This should be removed on a 5.1 or 5.2 release,
     * In favor of a forgot-my-password-only process.
     *
     * @param request
     * @param user
     * @deprecated since 5.0.18, for removal in 5.1 or 5.2
     */
    @PutMapping(API_INTERNAL + "/currentUser/password")
    @Operation(summary = "Update user password", deprecated = true)
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    @Deprecated(since = "5.1", forRemoval = true)
    public void updateCurrentUserPassword(@RequestBody UserDto request, @CurrentUserResolved User user) {
        Optional.ofNullable(request.getPassword())
                .filter(StringUtils::isNotEmpty)
                .ifPresent(p -> authService.resetUserPassword(user.getId(), p));
    }


    // <editor-fold desc="Signature image CRUD">


    @PostMapping(value = API_V1 + "/currentUser/signatureImage", consumes = MULTIPART_FORM_DATA)
    @Operation(summary = "Create user's signature image", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given userId does not exist"),
            @ApiResponse(responseCode = "409", description = "This user already have a signature image set. You should use the PUT method")
    })
    public StringResult setCurrentUserSignatureImage(@Parameter(description = "File")
                                                     @RequestPart MultipartFile file,
                                                     @CurrentUserResolved User user) {
        if (StringUtils.isNotEmpty(user.getSignatureImageContentId())) {
            throw new LocalizedStatusException(CONFLICT, "message.user_already_have_a_signature_image_set");
        }

        // Init User content data if needed

        if (StringUtils.isEmpty(user.getContentNodeId())) {

            int contentGroupIndex = Optional
                    .ofNullable(user.getContentGroupIndex())
                    .orElseGet(dbService::getUserDataGroupIndexAvailable);

            String userDataContentNodeId = contentService.createUserData(user.getId(), contentGroupIndex);
            authService.updateUserInternalMetadata(user.getId(), contentGroupIndex, userDataContentNodeId, null);

            user.setContentGroupIndex(contentGroupIndex);
            user.setContentNodeId(userDataContentNodeId);
        }

        // Updating data

        String imageSignatureNodeId;
        try (InputStream body = file.getInputStream()) {
            DocumentBuffer imageBuffer = new DocumentBuffer();
            imageBuffer.setName(file.getOriginalFilename());
            imageBuffer.setContentFlux(readInputStream(() -> body, new DefaultDataBufferFactory(), FILE_TRANSFER_BUFFER_SIZE));
            imageSignatureNodeId = contentService.createUserSignatureImage(user.getContentNodeId(), imageBuffer);
        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_reading_file");
        }

        authService.updateUserInternalMetadata(user.getId(), null, null, imageSignatureNodeId);
        return new StringResult(imageSignatureNodeId);
    }


    @GetMapping(API_V1 + "/currentUser/signatureImage")
    @Operation(summary = "Get user's signature image", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The current user does not exist, or the user does not have any signature image set")
    })
    public void getCurrentUserSignatureImage(@CurrentUserResolved User user,
                                             HttpServletResponse response) {

        if (StringUtils.isEmpty(user.getSignatureImageContentId())) {
            throw new LocalizedStatusException(NOT_FOUND, "message.no_signature_for_the_given_user_id");
        }

        // Image fetch

        DocumentBuffer documentBuffer = contentService.getSignatureImage(user.getSignatureImageContentId());
        log.debug("getSignatureImage retrieve from Alfresco buffer imageContentId:{} buffer:{}", user.getSignatureImageContentId(), documentBuffer);

        // Build Response

        RequestUtils.writeBufferToResponse(documentBuffer, response);
    }


    @PutMapping(API_V1 + "/currentUser/signatureImage")
    @Operation(summary = "Replace user's signature image", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given userId or tenantId does not exist, or the user doesn't have any signature image to update.")
    })
    public void updateCurrentUserSignatureImage(@CurrentUserResolved User user,
                                                @Parameter(description = "File")
                                                @RequestPart MultipartFile file) {

        String imageNodeId = Optional.ofNullable(user.getSignatureImageContentId())
                .filter(StringUtils::isNotEmpty)
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.no_signature_for_the_given_user_id"));

        // Updating data

        try (InputStream body = file.getInputStream()) {
            DocumentBuffer imageBuffer = new DocumentBuffer();
            imageBuffer.setName(file.getOriginalFilename());
            imageBuffer.setContentFlux(readInputStream(() -> body, new DefaultDataBufferFactory(), FILE_TRANSFER_BUFFER_SIZE));
            contentService.updateSignatureImage(imageNodeId, imageBuffer);
        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_reading_file");
        }
    }


    @DeleteMapping(API_V1 + "/currentUser/signatureImage")
    @Operation(summary = "Delete user's signature image", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(NO_CONTENT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "No content"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin's role.\n(You might want to set it in Keycloak)"),
            @ApiResponse(responseCode = "404", description = "The given userId does not exist")
    })
    public void deleteCurrentUserSignatureImage(@CurrentUserResolved User user) {

        String imageNodeId = Optional.ofNullable(user.getSignatureImageContentId())
                .filter(StringUtils::isNotEmpty)
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.no_signature_for_the_given_user_id"));

        contentService.deleteSignatureImage(imageNodeId);
        authService.updateUserInternalMetadata(user.getId(), null, null, EMPTY);
    }


    // </editor-fold desc="Signature image CRUD">


    // <editor-fold desc="Preferences CRUD">


    @GetMapping(API_V1 + "/currentUser/preferences")
    @Operation(summary = "Get user preferences")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public UserPreferencesDto getPreferences(@CurrentUserResolved User user) {

        String userId = KeycloakSecurityUtils.getCurrentSessionUserId();
        UserPreferences userPreferences = this.userPreferencesRepository
                .getByUserId(userId)
                .orElse(new UserPreferences());

        // Default values

        if (userPreferences.getShowDelegations() == null) {userPreferences.setShowDelegations(true);}
        if (userPreferences.getShowAdminIds() == null) {userPreferences.setShowAdminIds(false);}
        if (userPreferences.getTaskViewDefaultAsc() == null) {userPreferences.setTaskViewDefaultAsc(false);}
        if (userPreferences.getTaskViewDefaultPageSize() == null) {userPreferences.setTaskViewDefaultPageSize(10);}
        if (userPreferences.getArchiveViewDefaultPageSize() == null) {userPreferences.setArchiveViewDefaultPageSize(10);}
        if (userPreferences.getTaskViewDefaultSortBy() == null) {userPreferences.setTaskViewDefaultSortBy(FOLDER_NAME);}
        if (CollectionUtils.isEmpty(userPreferences.getFolderViewBlockList())) {
            userPreferences.setFolderViewBlockList(FolderViewBlock.Constants.DEFAULT_ORDER);
        }
        if (CollectionUtils.isEmpty(userPreferences.getTaskViewColumnList())) {
            userPreferences.setTaskViewColumnList(TaskViewColumn.Constants.DEFAULT_ORDER);
        }
        if (CollectionUtils.isEmpty(userPreferences.getArchiveViewColumnList())) {
            userPreferences.setArchiveViewColumnList(ArchiveViewColumn.Constants.DEFAULT_ORDER);
        }

        // TODO 5.3 : delete the following lines and the deprecated "TaskViewColumnList", "ArchiveViewColumnList" and "FolderViewBlockList" from userPreferences
        if (CollectionUtils.isEmpty(userPreferences.getTableLayoutList())) {
            ensureRetroCompatibilityForTableLayout(userPreferences);
        }

        // Conversion

        UserPreferencesDto result = modelMapper.map(userPreferences, UserPreferencesDto.class);
        result.setNotificationsCronFrequency(user.getNotificationsCronFrequency());
        result.setSignatureImageContentId(user.getSignatureImageContentId());
        result.setNotifiedOnConfidentialFolders(user.isNotifiedOnConfidentialFolders());
        result.setNotifiedOnFollowedFolders(user.isNotifiedOnFollowedFolders());
        result.setNotifiedOnLateFolders(user.isNotifiedOnLateFolders());
        result.setNotificationsRedirectionMail(user.getNotificationsRedirectionMail());

        metadataBusinessService.updateInnerValues(result);

        result.getTableLayoutList().forEach(tableLayout -> tableLayout
                .getLabelledColumnList()
                .removeIf(column -> column.getType() == METADATA && column.getMetadata() == null)
        );

        // Fetch transient values

        Set<String> deskIds = new HashSet<>(result.getFavoriteDeskIds());
        Map<String, String> deskNamesMap = authService.getDeskNames(deskIds);
        result.setFavoriteDeskIds(new ArrayList<>(result.getFavoriteDeskIds())); // Mutable copy
        result.getFavoriteDeskIds().removeIf(deskId -> !deskNamesMap.containsKey(deskId));
        result.setFavoriteDesks(
                result.getFavoriteDeskIds().stream()
                        .map(deskId -> new DeskRepresentation(deskId, deskNamesMap.get(deskId)))
                        .toList()
        );

        // Return result

        return result;
    }


    @PutMapping(API_V1 + "/currentUser/preferences")
    @Operation(summary = "Update user preferences")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
    })
    public UserPreferencesDto updatePreferences(@RequestBody UserPreferencesDto userPreferencesRequest,
                                                @CurrentUserResolved User user) {

        // Split the preference into both internal entities

        UserPreferences newUserPreferences = modelMapper.map(userPreferencesRequest, UserPreferences.class);
        newUserPreferences.setUserId(user.getId());

        User newUser = modelMapper.map(userPreferencesRequest, User.class);
        newUser.setId(user.getId());

        // Integrity check

        Set<String> targetPreferenceDeskIds = new HashSet<>(Optional.ofNullable(newUserPreferences.getFavoriteDeskIdList()).orElse(emptyList()));
        Map<String, String> deskNamesMap = authService.getDeskNames(targetPreferenceDeskIds);
        targetPreferenceDeskIds.stream()
                .filter(i -> !deskNamesMap.containsKey(i))
                .findFirst()
                .ifPresent(i -> {throw new LocalizedStatusException(NOT_FOUND, "message.unknown_desk_id");});

        newUserPreferences.setCurrentFilter(
                StringUtils.isEmpty(userPreferencesRequest.getCurrentFilterId())
                ? null
                : folderFilterRepository
                        .findByIdAndUserId(userPreferencesRequest.getCurrentFilterId(), user.getId())
                        .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_folder_filter_id"))
        );

        // Save

        String newNotificationFrequency = userPreferencesRequest.getNotificationsCronFrequency();

        notificationService.flushPendingNotifications(
                user.getId(),
                user.getNotificationsCronFrequency(),
                newNotificationFrequency
        );

        if (!StringUtils.equals(user.getNotificationsCronFrequency(), newNotificationFrequency)) {
            notificationService.resetNotificationScheduler(user.getId(), newNotificationFrequency);
        }

        authService.updateUserPreferences(user.getId(), user, newUser);
        newUserPreferences.setTableLayoutList(new ArrayList<>());
        userPreferencesRepository.save(newUserPreferences);

        userPreferencesRequest.getTableLayoutList()
                .stream()
                .map(sm -> modelMapper.map(sm, TableLayout.class))
                .forEach(tl -> {
                    tl.setUserPreferences(newUserPreferences);

                    List<TableLayout> existingTableLayouts = this.tableLayoutRepository.findFirstByTableNameAndUserPreferencesAndDeskId(
                            tl.getTableName(),
                            newUserPreferences,
                            tl.getDeskId()
                    );

                    if (!existingTableLayouts.isEmpty()) {
                        tl.setId(existingTableLayouts.get(0).getId());
                    }

                    if (tl.getId() == null) {
                        tl.setId(UUID.randomUUID().toString());
                    }

                    this.tableLayoutRepository.save(tl);

                    newUserPreferences.getTableLayoutList().add(tl);
                });

        return modelMapper.map(newUserPreferences, UserPreferencesDto.class);
    }


    // </editor-fold desc="Preferences CRUD">


    // <editor-fold desc="Filters CRUDL">


    @GetMapping(API_V1 + "/currentUser/userFilters")
    @Operation(summary = "Get userFilters", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "404", description = "The current user does not have user preferences")
    })
    public List<FolderFilterDto> getCurrentUserFilters() {
        String userId = KeycloakSecurityUtils.getCurrentSessionUserId();

        try {
            return this.folderFilterRepository.findAllByUserId(userId)
                    .stream()
                    .map(filter -> modelMapper.map(filter, FolderFilterDto.class))
                    .toList();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }


    @PostMapping(API_V1 + "/currentUser/userFilters")
    @Operation(summary = "Create filter", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin's role.\n(You might want to set it in Keycloak)")
    })
    public FolderFilterDto createFilter(@RequestBody @Valid FolderFilterDto folderFilterDto) {
        log.debug("createFilter - folderFilterDto : {}", folderFilterDto);

        String userId = KeycloakSecurityUtils.getCurrentSessionUserId();
        folderFilterDto.setId(UUID.randomUUID().toString());
        folderFilterDto.setUserId(userId);
        FolderFilter folderFilter = modelMapper.map(folderFilterDto, FolderFilter.class);
        try {
            folderFilter = folderFilterRepository.save(folderFilter);
            return modelMapper.map(folderFilter, FolderFilterDto.class);
        } catch (IllegalArgumentException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.unknown_folder_filter_id", e);
        }
    }


    @PutMapping(API_V1 + "/currentUser/userFilters/{folderFilterId}")
    @Operation(summary = "Update filter", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin's role.\n(You might want to set it in Keycloak)")
    })
    public FolderFilterDto updateFilter(@PathVariable String folderFilterId, @RequestBody @Valid FolderFilterDto folderFilterDto) {
        log.debug("createOrUpdateFilter - filter Id : {}, filter data : {}", folderFilterId, folderFilterDto);
        FolderFilter folderFilter = modelMapper.map(folderFilterDto, FolderFilter.class);
        folderFilter.setId(folderFilterId);

        String userId = KeycloakSecurityUtils.getCurrentSessionUserId();
        folderFilter.setUserId(userId);
        try {
            folderFilter = this.folderFilterRepository.save(folderFilter);
            return modelMapper.map(folderFilter, FolderFilterDto.class);
        } catch (IllegalArgumentException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.unknown_folder_filter_id", e);
        }
    }


    @DeleteMapping(API_V1 + "/currentUser/userFilters/{folderFilterId}")
    @Operation(summary = "Delete filter", hidden = HIDE_UNSTABLE_API)
    @ResponseStatus(CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created"),
            @ApiResponse(responseCode = "401", description = "The user is not logged, or its session has expired"),
            @ApiResponse(responseCode = "403", description = "Authenticated user doesn't have admin's role.\n(You might want to set it in Keycloak)")
    })
    public void deleteFilter(@PathVariable String folderFilterId) {
        String userId = KeycloakSecurityUtils.getCurrentSessionUserId();

        FolderFilter folderFilter = this.folderFilterRepository.findByIdAndUserId(folderFilterId, userId)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.unknown_folder_filter_id"));

        userBusinessService.deleteFilterForUser(userId, folderFilter);
    }


    // </editor-fold desc="Filters CRUDL">

    // <editor-fold desc="TableLayout CRUDL">


    @PutMapping(API_INTERNAL + "/currentUser/table-layout/{tableLayoutName}")
    @Operation(summary = "Update tableLayout")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class)))
    })
    public TableLayoutDto updateTableLayout(@RequestBody @Valid TableLayoutDto tableLayoutDto,
                                            @CurrentUserResolved User user,
                                            @PathVariable TableName tableLayoutName) {
        log.debug("updateTableLayout - tableName : {}, userId : {}", tableLayoutDto.getTableName(), user.getId());

        if (!tableLayoutDto.getTableName().equals(tableLayoutName)) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.bad_request_table_layout_name_in_dto_does_not_match_request_parameters");
        }

        if (tableLayoutDto.getDeskId() == null && !tableLayoutDto.getTableName().equalsOneOf(ARCHIVE_LIST, ADMIN_FOLDER_LIST)) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.bad_request_table_layout_desk_id_in_dto_does_should_be_present");
        }

        UserPreferences userPreferences = this.userPreferencesRepository
                .getByUserId(user.getId())
                .orElseGet(() -> {
                    log.warn("updateTableLayout - did not find userPreferences for user : {}, creating one on the fly", user.getId());
                    UserPreferences userPref = new UserPreferences();
                    userPref.setUserId(user.getId());
                    return userPref;
                });

        TableLayout currentTableLayout = Optional.ofNullable(userPreferences.getTableLayoutList())
                .orElse(emptyList())
                .stream()
                .filter(t -> t.getTableName().equals(tableLayoutName))
                .filter(t -> !t.getTableName().equals(TASK_LIST) || (t.getDeskId() != null && t.getDeskId().equals(tableLayoutDto.getDeskId())))
                .findFirst()
                .orElse(null);

        TableLayout tableLayoutRequest = modelMapper.map(tableLayoutDto, TableLayout.class);
        tableLayoutRequest.setId(currentTableLayout != null ? currentTableLayout.getId() : UUID.randomUUID().toString());
        tableLayoutRequest.setUserPreferences(userPreferences);

        TableLayout savedTableLayout = this.tableLayoutRepository.save(tableLayoutRequest);

        return modelMapper.map(savedTableLayout, TableLayoutDto.class);
    }


    @DeleteMapping(API_INTERNAL + "/currentUser/table-layout/{tableLayoutId}")
    @Operation(summary = "Delete tableLayout")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CODE_200),
            @ApiResponse(responseCode = CODE_401, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_403, content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = CODE_404, content = @Content(schema = @Schema(implementation = ErrorResponse.class)))
    })
    public void deleteTableLayout(@PathVariable String tableLayoutId, @CurrentUserResolved User user) {
        log.debug("deleteTableLayout - tableLayoutId : {}, userId : {}", tableLayoutId, user.getId());

        this.tableLayoutRepository.findById(tableLayoutId)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.unknown_table_layout_id"));

        this.tableLayoutRepository.deleteById(tableLayoutId);
    }


    // </editor-fold desc="TableLayout CRUDL">


    /**
     * TODO 5.3 : delete the following method and the deprecated "TaskViewColumnList", "ArchiveViewColumnList" and "FolderViewBlockList" from userPreferences
     */
    private void ensureRetroCompatibilityForTableLayout(UserPreferences userPreferences) {
        if (userPreferences.getTableLayoutList() == null) {
            userPreferences.setTableLayoutList(new ArrayList<>());
        }

        TableLayout taskListTableLayout = new TableLayout();
        taskListTableLayout.setId(UUID.randomUUID().toString());
        taskListTableLayout.setUserPreferences(userPreferences);
        taskListTableLayout.setTableName(TableName.TASK_LIST);
        taskListTableLayout.setDefaultAsc(userPreferences.getTaskViewDefaultAsc());
        taskListTableLayout.setDefaultSortBy(userPreferences.getTaskViewDefaultSortBy().toString());
        taskListTableLayout.setDefaultPageSize(userPreferences.getTaskViewDefaultPageSize());
        taskListTableLayout.setColumnList(userPreferences.getTaskViewColumnList().stream().map(Object::toString).collect(toList()));
        userPreferences.getTableLayoutList().add(taskListTableLayout);

        TableLayout trashBinListTableLayout = new TableLayout();
        trashBinListTableLayout.setId(UUID.randomUUID().toString());
        trashBinListTableLayout.setUserPreferences(userPreferences);
        trashBinListTableLayout.setTableName(ARCHIVE_LIST);
        trashBinListTableLayout.setDefaultPageSize(userPreferences.getArchiveViewDefaultPageSize());
        trashBinListTableLayout.setColumnList(userPreferences.getArchiveViewColumnList().stream().map(Object::toString).collect(toList()));
        userPreferences.getTableLayoutList().add(trashBinListTableLayout);

        TableLayout folderMainViewTableLayout = new TableLayout();
        folderMainViewTableLayout.setId(UUID.randomUUID().toString());
        folderMainViewTableLayout.setUserPreferences(userPreferences);
        folderMainViewTableLayout.setTableName(TableName.FOLDER_MAIN_LAYOUT);
        folderMainViewTableLayout.setColumnList(userPreferences.getFolderViewBlockList().stream().map(Object::toString).collect(toList()));
        userPreferences.getTableLayoutList().add(folderMainViewTableLayout);

        if (userPreferences.getUserId() != null) {
            tableLayoutRepository.saveAll(userPreferences.getTableLayoutList());
        }
    }


}
