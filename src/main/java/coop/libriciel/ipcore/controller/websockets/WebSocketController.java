/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller.websockets;

import coop.libriciel.ipcore.business.auth.DeskBusinessService;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.server.ResponseStatusException;


@Log4j2
@Controller
public class WebSocketController {

    private final AuthServiceInterface authService;
    private final SimpMessagingTemplate simpMessagingTemplate;
    private final DeskBusinessService deskBusinessService;


    public WebSocketController(AuthServiceInterface authService,
                               SimpMessagingTemplate simpMessagingTemplate,
                               DeskBusinessService deskBusinessService) {
        this.authService = authService;
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.deskBusinessService = deskBusinessService;
    }


    @Deprecated
    @MessageMapping("/websocket/tenant/{tenantId}/desk/{deskId}/folder/notify")
    public void notifyAllOfFolderAction(@DestinationVariable String tenantId,
                                        @DestinationVariable String deskId) {
        log.error("notifyAllOfFolderAction called");
        throw new ResponseStatusException(HttpStatus.NOT_IMPLEMENTED, "Operation not supported");
    }


}
