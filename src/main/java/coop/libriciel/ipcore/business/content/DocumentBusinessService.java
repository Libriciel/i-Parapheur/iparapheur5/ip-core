/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.business.content;

import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.RequestUtils;
import lombok.extern.log4j.Log4j2;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Files;
import java.util.List;

import static coop.libriciel.ipcore.utils.FileUtils.safeCreateTempFile;
import static org.apache.pdfbox.io.IOUtils.createTempFileOnlyStreamCache;
import static org.apache.pdfbox.multipdf.PDFMergerUtility.DocumentMergeMode.OPTIMIZE_RESOURCES_MODE;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;


@Log4j2
@Service
public class DocumentBusinessService {


    public static final String TMP_PRINT_DOC_PREFIX = "ip_print_doc_";
    public static final String TMP_PRINT_DOC_SUFFIX = ".pdf";


    ContentServiceInterface contentService;


    public DocumentBusinessService(ContentServiceInterface contentService) {
        this.contentService = contentService;
    }


    /**
     * A safe-wrapped {@link RequestUtils#bufferToInputStream}, with an appropriate {@link ContentServiceInterface} error message
     *
     * @param documentBuffer
     * @return an inputStream
     */
    public static @NotNull InputStream safeContentServiceBufferToInputStream(@NotNull DocumentBuffer documentBuffer) {
        try {
            return RequestUtils.bufferToInputStream(documentBuffer);
        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_reach_content_service");
        }
    }


    /**
     * Merges every PDF into one.
     *
     * @param documentIds a list of documents to merge
     * @return A file that will be deleted on application exit
     * @throws IOException
     */
    public InputStream mergeDocumentsByIds(List<String> documentIds) throws IOException {

        List<File> filesToMerge = documentIds.stream()
                .map(contentService::retrievePipedDocument)
                .map(is -> safeCreateTempFile(is, TMP_PRINT_DOC_PREFIX, TMP_PRINT_DOC_SUFFIX))
                .toList();

        File resultFile = mergeStreamToPdf(filesToMerge);
        return new FileInputStream(resultFile);
    }


    /**
     * Merges every PDF into one.
     *
     * @param sources a list of files
     * @return A file that will be deleted on application exit
     * @throws IOException
     */
    public @NotNull File mergeStreamToPdf(@NotNull List<File> sources) throws IOException {

        File resultFile = File.createTempFile(TMP_PRINT_DOC_PREFIX, TMP_PRINT_DOC_SUFFIX);
        resultFile.deleteOnExit();

        try (OutputStream resultOutputStream = Files.newOutputStream(resultFile.toPath())) {

            PDFMergerUtility pdfMerger = new PDFMergerUtility();
            pdfMerger.setDocumentMergeMode(OPTIMIZE_RESOURCES_MODE);
            pdfMerger.setDestinationStream(resultOutputStream);

            for (File source : sources) {
                pdfMerger.addSource(source);
            }

            pdfMerger.mergeDocuments(createTempFileOnlyStreamCache());
        }

        return resultFile;
    }


}
