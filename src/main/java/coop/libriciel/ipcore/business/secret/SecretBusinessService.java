/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.business.secret;

import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.crypto.SealCertificateRepresentation;
import coop.libriciel.ipcore.model.database.TenantRepresentation;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.database.DatabaseServiceInterface;
import coop.libriciel.ipcore.services.database.TenantRepository;
import coop.libriciel.ipcore.services.mail.NotificationServiceInterface;
import coop.libriciel.ipcore.services.secret.SecretServiceInterface;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

import static java.time.temporal.ChronoUnit.DAYS;
import static java.util.Comparator.*;
import static java.util.stream.Collectors.toMap;
import static org.springframework.data.domain.Pageable.unpaged;


@Log4j2
@Service
public class SecretBusinessService {


    private final AuthServiceInterface authService;
    private final DatabaseServiceInterface databaseService;
    private final ModelMapper modelMapper;
    private final NotificationServiceInterface notificationService;
    private final SecretServiceInterface secretService;
    private final TenantRepository tenantRepository;


    @Autowired
    public SecretBusinessService(AuthServiceInterface authService,
                                 DatabaseServiceInterface databaseService,
                                 ModelMapper modelMapper,
                                 NotificationServiceInterface notificationService,
                                 SecretServiceInterface secretService,
                                 TenantRepository tenantRepository) {
        this.authService = authService;
        this.databaseService = databaseService;
        this.modelMapper = modelMapper;
        this.notificationService = notificationService;
        this.secretService = secretService;
        this.tenantRepository = tenantRepository;
    }


    /**
     * By default, every monday, at 08:00,
     * we send a warning on possible seal-certificate expiration to tenant admins.
     */
    @Scheduled(cron = "${services.secret.expiration-mail-cron:-}")
    public void notifyExpiredCertificate() {

        log.debug("notifyExpiredCertificate...");

        // Compute values

        // FIXME unpaged() is probably a bad idea
        LocalDate now = LocalDate.now();
        Map<TenantRepresentation, List<SealCertificateRepresentation>> expiredCertificatesMap = tenantRepository
                .findDistinctBy(unpaged())
                .map(tenant -> modelMapper.map(tenant, TenantRepresentation.class))
                .stream()
                .collect(toMap(
                        tenant -> tenant,
                        tenant -> secretService
                                .getSealCertificateList(tenant.getId(), unpaged()).getContent().stream()
                                .filter(sealCertificate -> {
                                    LocalDate expirationDate = sealCertificate.getExpirationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                                    return DAYS.between(now, expirationDate) <= 30;
                                })
                                .sorted(comparing(SealCertificateRepresentation::getName, nullsLast(naturalOrder())))
                                .toList()
                ));

        expiredCertificatesMap.values().removeIf(CollectionUtils::isEmpty);
        log.debug("expiredCertificates count:{}", expiredCertificatesMap.size());

        // Building users to notify

        Map<TenantRepresentation, List<User>> usersToNotify = expiredCertificatesMap.keySet().stream()
                .collect(toMap(
                        tenant -> tenant,
                        tenant -> {
                            Set<String> userIds = new HashSet<>(databaseService.listAdminUsersIdForTenant(tenant));
                            return authService.getUsersByIds(userIds);
                        }
                ));

        log.debug("usersToNotify:{}", usersToNotify.values().stream().flatMap(Collection::stream).map(User::getUserName).toList());

        // Send mails

        usersToNotify.forEach((tenant, users) -> notificationService.mailExpiredCertificates(users, tenant, expiredCertificatesMap.get(tenant)));
    }


}
