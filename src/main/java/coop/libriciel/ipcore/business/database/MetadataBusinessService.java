/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.business.database;

import coop.libriciel.ipcore.model.auth.requests.DeskDto;
import coop.libriciel.ipcore.model.database.MetadataRepresentation;
import coop.libriciel.ipcore.model.database.userPreferences.LabelledColumn;
import coop.libriciel.ipcore.model.database.userPreferences.UserPreferencesDto;
import coop.libriciel.ipcore.services.database.MetadataRepository;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Supplier;

import static coop.libriciel.ipcore.model.database.userPreferences.LabelledColumnType.METADATA;
import static coop.libriciel.ipcore.utils.CollectionUtils.toMutableList;
import static java.util.Collections.emptyList;
import static java.util.Comparator.*;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;


@Log4j2
@Service
public class MetadataBusinessService {


    public static final Comparator<MetadataRepresentation> METADATA_REPRESENTATION_NAME_COMPARATOR = Comparator
            .comparing(MetadataRepresentation::getName, nullsLast(naturalOrder()))
            .thenComparing(MetadataRepresentation::getKey, nullsLast(naturalOrder()));

    public static final Comparator<MetadataRepresentation> METADATA_REPRESENTATION_INDEX_COMPARATOR = Comparator
            .comparing(MetadataRepresentation::getIndex, nullsLast(naturalOrder()))
            .thenComparing(MetadataRepresentation::getName, nullsLast(naturalOrder()))
            .thenComparing(MetadataRepresentation::getKey, nullsLast(naturalOrder()));


    // <editor-fold desc="Beans">


    private final MetadataRepository metadataRepository;
    private final ModelMapper modelMapper;


    public MetadataBusinessService(MetadataRepository metadataRepository,
                                   ModelMapper modelMapper) {
        this.metadataRepository = metadataRepository;
        this.modelMapper = modelMapper;
    }


    // </editor-fold desc="Beans">


    private @NotNull Map<String, MetadataRepresentation> createMetadataNameMap(@NotNull Set<String> metadataIds) {
        return metadataRepository.findAllByIdIn(metadataIds).stream()
                .map(metadata -> modelMapper.map(metadata, MetadataRepresentation.class))
                .collect(toMap(
                        MetadataRepresentation::getId,
                        metadataRepresentation -> metadataRepresentation
                ));
    }


    private void updateInnerValues(@NotNull Map<Supplier<List<String>>, Consumer<List<MetadataRepresentation>>> metadataIdsAccessors) {

        Set<String> referencedMetadataIds = new HashSet<>();
        metadataIdsAccessors.keySet().stream()
                .map(Supplier::get)
                .filter(CollectionUtils::isNotEmpty)
                .forEach(referencedMetadataIds::addAll);

        Map<String, MetadataRepresentation> metadataMap = createMetadataNameMap(referencedMetadataIds);

        metadataIdsAccessors.forEach((key, value) -> {

            if (key.get() == null) {
                value.accept(null);
                return;
            }

            List<MetadataRepresentation> metadataRepresentationList = key.get().stream()
                    .map(metadataMap::get)
                    .collect(toMutableList());

            value.accept(metadataRepresentationList);
        });
    }


    public void updateInnerValues(@Nullable DeskDto deskDto, Comparator<MetadataRepresentation> comparator) {

        if (deskDto == null) {
            return;
        }

        // Fetch real models

        updateInnerValues(Map.of(
                deskDto::getFilterableMetadataIds, deskDto::setFilterableMetadata
        ));

        if (CollectionUtils.isNotEmpty(deskDto.getFilterableMetadata())) {
            deskDto.getFilterableMetadata().sort(comparator);
        }
    }


    public void updateInnerValues(@Nullable UserPreferencesDto userPreferences) {

        List<LabelledColumn> metadataLabelledColumns = Optional.ofNullable(userPreferences)
                .map(UserPreferencesDto::getTableLayoutList)
                .orElse(emptyList())
                .stream()
                .flatMap(tableLayoutDto -> tableLayoutDto.getLabelledColumnList().stream())
                .filter(labelledColumn -> labelledColumn.getType() == METADATA)
                .toList();

        Set<String> metadataIds = metadataLabelledColumns.stream()
                .map(LabelledColumn::getId)
                .collect(toSet());

        Map<String, MetadataRepresentation> metadataMap = createMetadataNameMap(metadataIds);

        metadataLabelledColumns.forEach(labelledColumn -> labelledColumn.setMetadata(metadataMap.get(labelledColumn.getId())));
    }


}
