/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.business.database;


import coop.libriciel.ipcore.model.auth.requests.UserDto;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.database.TenantRepresentation;
import coop.libriciel.ipcore.model.database.requests.TenantDto;
import coop.libriciel.ipcore.services.database.TenantRepository;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static java.util.Comparator.*;


@Log4j2
@Service
public class TenantBusinessService {


    // <editor-fold desc="Beans">


    private final ModelMapper modelMapper;
    private final TenantRepository tenantRepository;


    public TenantBusinessService(ModelMapper modelMapper,
                                 TenantRepository tenantRepository) {
        this.modelMapper = modelMapper;
        this.tenantRepository = tenantRepository;
    }


    // </editor-fold desc="Beans">


    private void updateInnerTenantValues(@NotNull Map<Supplier<List<String>>, Consumer<List<TenantRepresentation>>> tenantIdsAccessors) {

        Set<String> referencedTenantIds = new HashSet<>();
        tenantIdsAccessors.keySet().stream()
                .map(Supplier::get)
                .filter(CollectionUtils::isNotEmpty)
                .forEach(referencedTenantIds::addAll);

        Map<String, Tenant> tenantMap = tenantRepository.findAllById(referencedTenantIds)
                .stream()
                .collect(Collectors.toMap(
                        Tenant::getId,
                        tenant -> tenant
                ));

        tenantIdsAccessors.forEach((key, value) -> {

            if (key.get() == null) {
                value.accept(null);
                return;
            }

            List<TenantRepresentation> tenantRepresentationList = key.get().stream()
                    .filter(tenantMap::containsKey)
                    .map(id -> new TenantRepresentation(id, tenantMap.get(id).getName()))
                    .sorted(comparing(TenantRepresentation::getName, nullsLast(naturalOrder())))
                    .toList();

            value.accept(tenantRepresentationList);
        });
    }


    public void updateInnerTenantValues(@Nullable UserDto user) {

        if (user == null) {
            return;
        }

        updateInnerTenantValues(Map.of(
                user::getAssociatedTenantIds, user::setAssociatedTenants,
                user::getAdministeredTenantIds, user::setAdministeredTenants
        ));
    }


    public void updateExistingTenant(Tenant existingTenant, TenantDto request) {
        Optional.ofNullable(request.getName())
                .filter(StringUtils::isNotEmpty)
                .ifPresent(existingTenant::setName);
        existingTenant.setZipCode(request.getZipCode());
        existingTenant.setDeskLimit(request.getDeskLimit());
        existingTenant.setUserLimit(request.getUserLimit());
        existingTenant.setWorkflowLimit(request.getWorkflowLimit());
        existingTenant.setTypeLimit(request.getTypeLimit());
        existingTenant.setSubtypeLimit(request.getSubtypeLimit());
        existingTenant.setFolderLimit(request.getFolderLimit());
        existingTenant.setMetadataLimit(request.getMetadataLimit());
        existingTenant.setLayerLimit(request.getLayerLimit());
    }
}
