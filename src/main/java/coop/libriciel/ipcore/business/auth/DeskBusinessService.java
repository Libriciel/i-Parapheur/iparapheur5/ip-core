/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.business.auth;

import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.auth.requests.DeskDto;
import coop.libriciel.ipcore.model.auth.requests.UserDto;
import coop.libriciel.ipcore.model.auth.requests.UserRepresentation;
import coop.libriciel.ipcore.model.database.*;
import coop.libriciel.ipcore.model.database.requests.SubtypeDto;
import coop.libriciel.ipcore.model.database.requests.SubtypeRepresentation;
import coop.libriciel.ipcore.model.database.requests.TypeRepresentation;
import coop.libriciel.ipcore.model.permission.DelegationDto;
import coop.libriciel.ipcore.model.websockets.DeskCount;
import coop.libriciel.ipcore.model.workflow.DelegationRule;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.database.SubtypeRepository;
import coop.libriciel.ipcore.services.database.TypeRepository;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.CollectionUtils;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static coop.libriciel.ipcore.model.workflow.State.*;
import static coop.libriciel.ipcore.services.auth.AuthServiceInterface.INTERNAL_USERNAMES;
import static coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface.META_SUBTYPE_ID;
import static coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface.META_TYPE_ID;
import static coop.libriciel.ipcore.utils.ApiUtils.MAX_DESKS_COUNT;
import static coop.libriciel.ipcore.utils.ApiUtils.MAX_OWNERS_PER_DESKS_COUNT;
import static coop.libriciel.ipcore.utils.RequestUtils.hasChangesBetween;
import static coop.libriciel.ipcore.utils.RequestUtils.returnRedactedResponse;
import static java.util.Collections.*;
import static java.util.Comparator.*;
import static java.util.Locale.ROOT;
import static java.util.stream.Collectors.*;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static org.apache.commons.collections4.CollectionUtils.subtract;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.springframework.data.domain.Pageable.unpaged;
import static org.springframework.http.HttpStatus.*;


@Log4j2
@Service
public class DeskBusinessService {


    // <editor-fold desc="Beans">


    private final AuthServiceInterface authService;
    private final ModelMapper modelMapper;
    private final PermissionServiceInterface permissionService;
    private final SubtypeRepository subtypeRepository;
    private final TypeRepository typeRepository;
    private final UserBusinessService userBusinessService;
    private final WorkflowServiceInterface workflowService;


    public DeskBusinessService(AuthServiceInterface authService,
                               ModelMapper modelMapper,
                               PermissionServiceInterface permissionService,
                               SubtypeRepository subtypeRepository,
                               TypeRepository typeRepository,
                               UserBusinessService userBusinessService,
                               WorkflowServiceInterface workflowService) {
        this.authService = authService;
        this.modelMapper = modelMapper;
        this.permissionService = permissionService;
        this.subtypeRepository = subtypeRepository;
        this.typeRepository = typeRepository;
        this.userBusinessService = userBusinessService;
        this.workflowService = workflowService;
    }


    // </editor-fold desc="Beans">


    public @NotNull List<Desk> populateAndPaginateDeskList(@NotNull Pageable pageable, @Nullable String searchTerm, @NotNull Map<String, Desk> deskIdNameMap) {

        authService.populateDeskNamesAndTenantIds(deskIdNameMap);

        // Manually paginating it to ease the following count request.
        // (note : every user should have a maximum of 200 desk/roles),
        // and those are already retrieved from the accessToken.
        List<Desk> userDeskList = deskIdNameMap.values().stream().toList();

        if (StringUtils.isNotEmpty(searchTerm)) {
            userDeskList = userDeskList.stream()
                    .filter(d -> d.getName().toUpperCase(ROOT).contains(searchTerm.toUpperCase(ROOT)))
                    .toList();
        }

        userDeskList = userDeskList.stream()
                // After deleting a desk, the desk will still be present here, but with a null name, causing a NullPointerException
                .filter(desk -> Objects.nonNull(desk.getName()))
                .sorted(comparing(Desk::getTenantId)
                        .thenComparing(Desk::getName, nullsLast(naturalOrder())))
                .skip((long) pageable.getPageNumber() * pageable.getPageSize())
                .limit(pageable.getPageSize())
                .toList();

        return userDeskList;
    }


    public void updateInnerDeskValues(@NotNull String tenantId, @Nullable Collection<DelegationDto> delegationDtoList) {

        if (isEmpty(delegationDtoList)) {
            return;
        }

        // Fetch real models

        Set<String> deskIds = Stream
                .concat(delegationDtoList.stream().map(DelegationDto::getDelegatingDeskId),
                        delegationDtoList.stream().map(DelegationDto::getSubstituteDeskId))
                .collect(toSet());

        // TODO: this, with both delegation and delegating desks
        //  updateInnerValues(
        //      delegationDtoList.stream().collect(toMap(
        //          delegationDto -> () -> singletonList(delegationDto.getDelegatingDeskId()),
        //          delegationDto -> desks -> delegationDto.setDelegatingDesk(desks.stream().findFirst().orElse(null))
        //      ))
        //  );

        Map<String, String> deskNames = authService.getDeskNames(deskIds);

        Set<String> typeIds = delegationDtoList.stream().map(DelegationDto::getTypeId).collect(toSet());
        Map<String, Type> typeMap = typeRepository
                .findAllByTenant_IdAndIdIn(tenantId, typeIds, unpaged()).getContent().stream()
                .collect(toMap(
                        TypologyEntity::getId,
                        type -> type
                ));

        Set<String> subtypeIds = delegationDtoList.stream().map(DelegationDto::getSubtypeId).collect(toSet());
        Map<String, Subtype> subtypeMap = subtypeRepository
                .findAllByTenant_IdAndIdIn(tenantId, subtypeIds, unpaged()).getContent().stream()
                .collect(toMap(
                        TypologyEntity::getId,
                        subtype -> subtype
                ));

        // Compute missing values and send it back

        delegationDtoList.forEach(delegationDto -> {

            String delegatingDeskId = delegationDto.getDelegatingDeskId();
            delegationDto.setDelegatingDesk(new DeskRepresentation(delegatingDeskId, deskNames.get(delegatingDeskId)));

            String substituteDeskId = delegationDto.getSubstituteDeskId();
            delegationDto.setSubstituteDesk(new DeskRepresentation(substituteDeskId, deskNames.get(substituteDeskId)));

            Optional.ofNullable(delegationDto.getTypeId())
                    .filter(StringUtils::isNotEmpty)
                    .filter(typeMap::containsKey)
                    .map(typeMap::get)
                    .ifPresent(type ->
                            delegationDto.setType(returnRedactedResponse(modelMapper, type, TypeRepresentation::new))
                    );

            Optional.ofNullable(delegationDto.getSubtypeId())
                    .filter(StringUtils::isNotEmpty)
                    .filter(subtypeMap::containsKey)
                    .map(subtypeMap::get)
                    .ifPresent(subtype -> {
                        delegationDto.setSubtype(returnRedactedResponse(modelMapper, subtype, SubtypeRepresentation::new));
                        delegationDto.setType(returnRedactedResponse(modelMapper, subtype.getParentType(), TypeRepresentation::new));
                    });
        });
    }


    public void updateInnerDeskValues(@NotNull String tenantId, @Nullable SubtypeDto subtype) {

        if (subtype == null) {
            return;
        }

        updateInnerDeskValues(
                tenantId,
                Map.of(
                        subtype::getCreationPermittedDeskIds, subtype::setCreationPermittedDesks,
                        subtype::getFilterableByDeskIds, subtype::setFilterableByDesks
                )
        );
    }


    public void updateInnerDeskValues(@NotNull String tenantId, @Nullable DeskDto deskDto) {

        if (deskDto == null) {
            return;
        }

        updateInnerDeskValues(
                tenantId,
                Map.of(deskDto::getAssociatedDeskIds, deskDto::setAssociatedDesks)
        );
    }


    public void updateInnerDeskValues(@NotNull Collection<DeskRepresentation> desks) {

        List<DeskRepresentation> referencedDesks = desks.stream()
                .filter(Objects::nonNull)
                .filter(desk -> StringUtils.isEmpty(desk.getName()))
                .toList();

        Set<String> referencedDeskIds = referencedDesks.stream()
                .map(DeskRepresentation::getId)
                .collect(toSet());

        Map<String, String> deskNames = authService.getDeskNames(referencedDeskIds);

        referencedDesks.stream()
                .filter(Objects::nonNull)
                .filter(desk -> StringUtils.isEmpty(desk.getName()))
                .forEach(desk -> desk.setName(deskNames.get(desk.getId())));
    }


    public void updateInnerDeskValues(@NotNull String tenantId, @Nullable UserDto userDto) {

        if (userDto == null) {
            return;
        }

        updateInnerDeskValues(
                tenantId,
                Map.of(
                        userDto::getAdministeredDeskIds, userDto::setAdministeredDesks,
                        userDto::getAssociatedDeskIds, userDto::setAssociatedDesks,
                        userDto::getDelegationManagedDeskIds, userDto::setDelegationManagedDesks,
                        userDto::getSupervisedDeskIds, userDto::setSupervisedDesks
                )
        );
    }


    public void updateFoldersInnerDeskValues(@NotNull Collection<Folder> folders) {

        if (isEmpty(folders)) {
            return;
        }

        List<Task> internalTasks = folders.stream().map(Folder::getStepList).flatMap(CollectionUtils::stream).toList();
        updateInnerDeskValues(
                Stream.of(
                                internalTasks.stream().map(Task::getDelegatedByDesk),
                                internalTasks.stream().map(Task::getNotifiedDesks).flatMap(CollectionUtils::stream),
                                internalTasks.stream().map(Task::getDesks).flatMap(CollectionUtils::stream),
                                folders.stream().map(Folder::getOriginDesk),
                                folders.stream().map(Folder::getFinalDesk)
                        )
                        .filter(Objects::nonNull)
                        .flatMap(s -> s)
                        .toList()
        );
    }


    private void updateInnerDeskValues(@NotNull String tenantId,
                                       @NotNull Map<Supplier<List<String>>, Consumer<List<DeskRepresentation>>> deskIdsAccessors) {

        Set<String> referencedDeskIds = new HashSet<>();
        deskIdsAccessors.keySet().stream()
                .map(Supplier::get)
                .filter(org.apache.commons.collections4.CollectionUtils::isNotEmpty)
                .forEach(referencedDeskIds::addAll);

        Map<String, String> deskNames = authService.getDeskNames(referencedDeskIds);

        deskIdsAccessors.forEach((key, value) -> {

            if (key.get() == null) {
                value.accept(null);
                return;
            }

            List<DeskRepresentation> deskRepresentationList = key.get().stream()
                    .map(id -> new DeskRepresentation(id, deskNames.get(id), tenantId))
                    .sorted(comparing(DeskRepresentation::getName, nullsLast(naturalOrder())))
                    .toList();

            value.accept(deskRepresentationList);
        });
    }


    /**
     * If the same desk has duplicate entries in partial delegations,
     * we may want to simplify these cases.
     */
    public void factorizeDelegations(Map<String, Set<DelegationRule>> delegationRulesDeskMap) {

        List<String> referencedSubtypesId = delegationRulesDeskMap.values().stream()
                .flatMap(Collection::stream)
                .filter(rule -> StringUtils.equals(rule.getMetadataKey(), META_SUBTYPE_ID))
                .map(DelegationRule::getMetadataValue)
                .toList();

        Map<String, String> subtypesAndParentTypeMap = subtypeRepository.findAllByIdIn(referencedSubtypesId, unpaged())
                .getContent()
                .stream()
                .collect(toMap(
                        TypologyEntity::getId,
                        subtype -> subtype.getParentType().getId()
                ));

        delegationRulesDeskMap.values().stream()
                .filter(org.apache.commons.collections4.CollectionUtils::isNotEmpty)
                .forEach(delegationRules -> {

                    // If we have a full-desk delegation, and any partial one on the same target :
                    // We can remove the partial ones

                    Set<String> fullDeskIdDelegation = delegationRules.stream()
                            .filter(rule -> StringUtils.isEmpty(rule.getMetadataKey()))
                            .map(DelegationRule::getDelegatingGroup)
                            .collect(toSet());

                    delegationRules.removeIf(rule -> {
                        boolean isPartialDelegation = StringUtils.isNotEmpty(rule.getMetadataKey());
                        boolean isFullDeskDelegationAlreadyExists = fullDeskIdDelegation.contains(rule.getDelegatingGroup());
                        return isPartialDelegation && isFullDeskDelegationAlreadyExists;
                    });

                    // A partial delegation on a type, and another partial one on one of its subtype :
                    // The type-one is enough.

                    Map<String, Set<String>> fullTypeIdDelegationMap = new HashMap<>();
                    delegationRules.stream()
                            .filter(rule -> StringUtils.equals(rule.getMetadataKey(), META_TYPE_ID))
                            .forEach(rule -> {
                                String targetDeskId = rule.getDelegatingGroup();
                                fullTypeIdDelegationMap.computeIfAbsent(targetDeskId, k -> new HashSet<>());
                                fullTypeIdDelegationMap.get(targetDeskId).add(rule.getMetadataValue());
                            });

                    delegationRules.removeIf(rule -> {
                        String targetDeskId = rule.getDelegatingGroup();
                        String parentTypeId = subtypesAndParentTypeMap.get(rule.getMetadataValue());
                        boolean isSubtypeDelegation = StringUtils.equals(rule.getMetadataKey(), META_SUBTYPE_ID);
                        boolean isFullTypeDelegationAlreadyExists = fullTypeIdDelegationMap.getOrDefault(targetDeskId, emptySet()).contains(parentTypeId);
                        return isSubtypeDelegation && isFullTypeDelegationAlreadyExists;
                    });
                });
    }


    public void createDeskIntegrityChecks(@NotNull DeskDto request, @NotNull Tenant tenant) {


        long desksCount = authService.listDesks(tenant.getId(), Pageable.ofSize(1), emptyList(), false).getTotalElements();
        int limit = tenant.getDeskLimit() != null ? tenant.getDeskLimit() : MAX_DESKS_COUNT;
        if (desksCount >= limit) {
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE,
                    "message.already_n_desks_maximum_reached",
                    desksCount);
        }

        if (isNotEmpty(request.getParentDeskId())) {
            Optional.of(request.getParentDeskId())
                    .map(i -> authService.findDeskByIdNoException(tenant.getId(), i))
                    .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_parent_desk_id"));
        }

        authService.searchDesks(tenant.getId(), unpaged(), emptyList(), request.getName(), false)
                .getContent()
                .stream()
                .filter(d -> StringUtils.equalsIgnoreCase(d.getName(), request.getName()))
                .findFirst()
                .ifPresent(d -> {throw new LocalizedStatusException(CONFLICT, "message.already_existing_desk_title");});

        authService.findDeskByShortName(tenant.getId(), request.getShortName()).stream()
                .findFirst()
                .ifPresent(desk -> {throw new LocalizedStatusException(CONFLICT, "message.already_existing_desk_short_name");});
    }


    public void updateDeskIntegrityChecks(@NotNull Desk existingDesk, @NotNull DeskDto request, @NotNull String tenantId) {

        if (StringUtils.isNotEmpty(request.getParentDeskId())) {
            Optional.of(request.getParentDeskId())
                    .map(i -> authService.findDeskByIdNoException(tenantId, i))
                    .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_parent_desk_id"));
        }

        if (!StringUtils.equals(existingDesk.getName(), request.getName())) {
            // If the name has been updated, we check it the new one don't collide an already existing one.
            authService.searchDesks(tenantId, unpaged(), emptyList(), request.getName(), false)
                    .getContent()
                    .stream()
                    .filter(d -> StringUtils.equals(d.getName(), request.getName()))
                    .findFirst()
                    .ifPresent(d -> {throw new LocalizedStatusException(CONFLICT, "message.already_existing_desk_title");});
        }

        authService.findDeskByShortName(tenantId, request.getShortName()).stream()
                // Avoid fake conflict with the currently modified desk
                .filter(d -> !StringUtils.equals(d.getId(), existingDesk.getId()))
                .findFirst()
                .ifPresent(d -> {throw new LocalizedStatusException(CONFLICT, "message.already_existing_desk_short_name");});
    }


    public void computeDifferencesAndUpdateDesk(Desk existingDesk, DeskDto request, String tenantId) {

        this.updateDeskUsers(request, tenantId, existingDesk.getId());
        this.updateDeskAssociatedDesks(request, tenantId, existingDesk.getId());
        this.updateDeskFilterableMetadata(request, tenantId, existingDesk.getId());
        this.updateDeskSupervisors(request, tenantId, existingDesk.getId());
        this.updateDeskDelegations(request, tenantId, existingDesk.getId());

        // Actual edit

        authService.editDesk(tenantId, existingDesk, request.getName(), request.getShortName(), request.getDescription(), request.getParentDeskId());

        // Permissions

        Desk dummyDesk = modelMapper.map(request, Desk.class);
        dummyDesk.setId(existingDesk.getId());
        if (hasChangesBetween(existingDesk, dummyDesk, Desk::isActionAllowed, Desk::isArchivingAllowed, Desk::isChainAllowed, Desk::isFolderCreationAllowed)) {
            permissionService.editDeskPermission(tenantId, dummyDesk);
        }

    }


    void updateDeskUsers(DeskDto request, String tenantId, String existingDeskId) {

        String serviceAccountUserId = authService.getServiceAccountUserId();

        Set<String> existingUserIds = authService.listUsersFromDesk(tenantId, existingDeskId, 0, MAX_OWNERS_PER_DESKS_COUNT)
                .getData()
                .stream()
                .map(User::getId)
                .collect(toSet());

        Collection<String> userIdsToAdd = subtract(request.getOwnerIds(), existingUserIds);
        Collection<String> userIdsToRemove = subtract(existingUserIds, request.getOwnerIds());
        userIdsToRemove.remove(serviceAccountUserId);

        if (!existingUserIds.contains(serviceAccountUserId)) {
            userIdsToAdd.add(serviceAccountUserId);
        }

        log.trace("updateDeskUsers request.getOwnerUserIdsList:{}", request.getOwnerIds());
        log.trace("updateDeskUsers existingUserIds:{}", existingUserIds);
        log.trace("updateDeskUsers userIdsToAdd:{}", userIdsToAdd);
        log.trace("updateDeskUsers userIdsToRemove:{}", userIdsToRemove);

        authService.addUsersToDesk(existingDeskId, userIdsToAdd);
        authService.removeUsersFromDesk(existingDeskId, userIdsToRemove);
    }


    void updateDeskAssociatedDesks(DeskDto request, String tenantId, String existingDeskId) {

        Collection<String> existingAssociatedDeskIds = permissionService.getAssociatedDeskIds(tenantId, existingDeskId);
        Collection<String> associatedDeskIdsToAdd = subtract(request.getAssociatedDeskIds(), existingAssociatedDeskIds);
        Collection<String> associatedDeskIdsToRemove = subtract(existingAssociatedDeskIds, request.getAssociatedDeskIds());

        log.trace("updateDeskAssociatedDesks request.getAssociatedDeskIdsList:{}", request.getAssociatedDeskIds());
        log.trace("updateDeskAssociatedDesks existingAssociatedDeskIds:{}", existingAssociatedDeskIds);
        log.trace("updateDeskAssociatedDesks associatedDeskIdsToAdd:{}", associatedDeskIdsToAdd);
        log.trace("updateDeskAssociatedDesks associatedDeskIdsToRemove:{}", associatedDeskIdsToRemove);

        permissionService.associateDesks(tenantId, existingDeskId, associatedDeskIdsToAdd);
        permissionService.removeAssociatedDesks(tenantId, existingDeskId, associatedDeskIdsToRemove);
    }


    void updateDeskFilterableMetadata(DeskDto request, String tenantId, String existingDeskId) {

        Set<String> existingFilterableMetadataIds = new HashSet<>(permissionService.getFilterableMetadataIds(tenantId, existingDeskId));
        Collection<String> filterableMetadataIdsToAdd = subtract(request.getFilterableMetadataIds(), existingFilterableMetadataIds);
        Collection<String> filterableMetadataIdsToRemove = subtract(existingFilterableMetadataIds, request.getFilterableMetadataIds());

        log.trace("updateDeskFilterableMetadata request.getFilterableMetadataIds:{}", request.getFilterableMetadataIds());
        log.trace("updateDeskFilterableMetadata existingFilterableMetadataIds:{}", existingFilterableMetadataIds);
        log.trace("updateDeskFilterableMetadata filterableMetadataIdsToAdd:{}", filterableMetadataIdsToAdd);
        log.trace("updateDeskFilterableMetadata filterableMetadataIdsToRemove:{}", filterableMetadataIdsToRemove);

        permissionService.addFilterableMetadata(tenantId, existingDeskId, filterableMetadataIdsToAdd);
        permissionService.removeFilterableMetadata(tenantId, existingDeskId, filterableMetadataIdsToRemove);
    }


    void updateDeskSupervisors(DeskDto request, String tenantId, String existingDeskId) {

        Set<String> existingSupervisorIds = new HashSet<>(permissionService.getSupervisorIdsFromDesk(tenantId, existingDeskId));
        Collection<String> supervisorIdsToAdd = subtract(request.getSupervisorIds(), existingSupervisorIds);
        Collection<String> supervisorIdsToRemove = subtract(existingSupervisorIds, request.getSupervisorIds());

        log.trace("updateDeskSupervisors request.getSupervisorIdsList:{}", request.getSupervisorIds());
        log.trace("updateDeskSupervisors existingSupervisorIds:{}", existingSupervisorIds);
        log.trace("updateDeskSupervisors supervisorIdsToAdd:{}", supervisorIdsToAdd);
        log.trace("updateDeskSupervisors supervisorIdsToRemove:{}", supervisorIdsToRemove);

        permissionService.addSupervisorsToDesk(tenantId, existingDeskId, supervisorIdsToAdd);
        permissionService.removeSupervisorsFromDesk(tenantId, existingDeskId, supervisorIdsToRemove);
    }


    void updateDeskDelegations(DeskDto request, String tenantId, String existingDeskId) {

        Set<String> existingDelegationManagerIds = new HashSet<>(permissionService.getDelegationManagerIdsFromDesk(tenantId, existingDeskId));
        Collection<String> delegationManagerIdsToAdd = subtract(request.getDelegationManagerIds(), existingDelegationManagerIds);
        Collection<String> delegationManagerIdsToRemove = subtract(existingDelegationManagerIds, request.getDelegationManagerIds());

        log.trace("updateDeskDelegations request.getDelegationManagerIdsList:{}", request.getDelegationManagerIds());
        log.trace("updateDeskDelegations existingDelegationManagerIds:{}", existingDelegationManagerIds);
        log.trace("updateDeskDelegations delegationManagerIdsToAdd:{}", delegationManagerIdsToAdd);
        log.trace("updateDeskDelegations delegationManagerIdsToRemove:{}", delegationManagerIdsToRemove);

        permissionService.addDelegationManagersToDesk(tenantId, existingDeskId, delegationManagerIdsToAdd);
        permissionService.removeDelegationManagersFromDesk(tenantId, existingDeskId, delegationManagerIdsToRemove);
    }


    public DeskCount getDeskCount(Desk desk) {
        Map<DelegationRule, Integer> deskCountMap = workflowService.countFolders(singleton(desk.getId()), desk.getDelegationRules());
        List<DeskCount> delegatingDeskCounts = desk.getDelegatingDesks()
                .stream()
                .map(delegatingDesk -> new DeskCount(
                        delegatingDesk.getTenantId(),
                        delegatingDesk.getId(),
                        0,
                        0,
                        delegatingDesk.getPendingFoldersCount(),
                        0,
                        0,
                        emptyList()
                )).toList();

        return new DeskCount(
                desk.getTenantId(),
                desk.getId(),
                deskCountMap.getOrDefault(new DelegationRule(desk.getId(), DRAFT, null, null), 0),
                deskCountMap.getOrDefault(new DelegationRule(desk.getId(), REJECTED, null, null), 0),
                deskCountMap.getOrDefault(new DelegationRule(desk.getId(), PENDING, null, null), 0),
                deskCountMap.getOrDefault(new DelegationRule(desk.getId(), FINISHED, null, null), 0),
                deskCountMap.getOrDefault(new DelegationRule(desk.getId(), LATE, null, null), 0),
                delegatingDeskCounts
        );
    }


    public void populateCountsAndDelegations(Map<String, Desk> deskIdNameMap, List<Desk> userDeskList) {
        Map<String, Set<DelegationRule>> delegations = permissionService.getActiveDelegationsToDesksForCurrentUser(
                userDeskList.stream().map(Desk::getId).collect(toSet())
        );

        factorizeDelegations(delegations);
        log.debug("populateCountsAndDelegations delegations:{}", delegations);
        userDeskList.stream()
                .filter(d -> delegations.containsKey(d.getId()))
                .forEach(d -> d.getDelegationRules().addAll(delegations.get(d.getId())));


        Map<DelegationRule, Integer> deskCountMap = workflowService.countFolders(
                deskIdNameMap.keySet(),
                delegations.values().stream().flatMap(Collection::stream).toList()
        );

        log.trace("populateCountsAndDelegations deskCountMap:{}", deskCountMap);
        userDeskList.forEach(d -> populateCountsAndSubDesks(d, deskCountMap));
    }


    public void populateCountsAndDelegationNoAuth(String tenantId, Map<String, Desk> deskIdNameMap, List<Desk> userDeskList) {
        log.debug("populateCountsAndDelegationNoAuth");
        Map<String, Set<DelegationRule>> delegations = permissionService.getActiveDelegationsForDesks(
                tenantId,
                userDeskList.stream().map(Desk::getId).collect(toSet())
        );

        factorizeDelegations(delegations);
        log.debug("populateCountsAndDelegationNoAuth delegations:{}", delegations);
        userDeskList.stream()
                .filter(d -> delegations.containsKey(d.getId()))
                .forEach(d -> d.getDelegationRules().addAll(delegations.get(d.getId())));

        Map<DelegationRule, Integer> deskCountMap = workflowService.countFolders(
                deskIdNameMap.keySet(),
                delegations.values().stream().flatMap(Collection::stream).toList()
        );

        log.trace("populateCountsAndDelegationNoAuth deskCountMap:{}", deskCountMap);
        userDeskList.forEach(d -> populateCountsAndSubDesks(d, deskCountMap));
    }


    private void populateCountsAndSubDesks(@NotNull Desk desk, @NotNull Map<DelegationRule, Integer> countMap) {

        // Adding appropriate SubDesks in the delegatingDesks parameter

        Map<String, Desk> desks = new HashMap<>();

        desk.getDelegationRules()
                .stream()
                .filter(r -> !desks.containsKey(r.getDelegatingGroup()))
                .forEach(r -> desks.put(r.getDelegatingGroup(), Desk.builder().id(r.getDelegatingGroup()).build()));

        desk.setDelegatingDesks(new ArrayList<>(desks.values()));

        // Init every Desks involved, with counts to 0

        desks.values().forEach(d -> {
            d.setDraftFoldersCount(0);
            d.setLateFoldersCount(0);
            d.setPendingFoldersCount(0);
            d.setFinishedFoldersCount(0);
            d.setRejectedFoldersCount(0);
        });

        // Parsing results starts

        desk.setDraftFoldersCount(countMap.getOrDefault(new DelegationRule(desk.getId(), DRAFT, null, null), 0));
        desk.setLateFoldersCount(countMap.getOrDefault(new DelegationRule(desk.getId(), LATE, null, null), 0));
        desk.setPendingFoldersCount(countMap.getOrDefault(new DelegationRule(desk.getId(), PENDING, null, null), 0));
        desk.setFinishedFoldersCount(countMap.getOrDefault(new DelegationRule(desk.getId(), FINISHED, null, null), 0));
        desk.setRejectedFoldersCount(countMap.getOrDefault(new DelegationRule(desk.getId(), REJECTED, null, null), 0));

        // Summing retrieved results may be necessary,
        // in case multiple metadata-based delegations are set on the same desk

        desk.getDelegationRules().forEach(r -> {
            Desk currentDesk = desks.get(r.getDelegatingGroup());

            currentDesk.setDraftFoldersCount(currentDesk.getDraftFoldersCount()
                    + countMap.getOrDefault(new DelegationRule(currentDesk.getId(), DRAFT, r.getMetadataKey(), r.getMetadataValue()), 0));

            currentDesk.setLateFoldersCount(currentDesk.getLateFoldersCount()
                    + countMap.getOrDefault(new DelegationRule(currentDesk.getId(), LATE, r.getMetadataKey(), r.getMetadataValue()), 0));

            currentDesk.setPendingFoldersCount(currentDesk.getPendingFoldersCount()
                    + countMap.getOrDefault(new DelegationRule(currentDesk.getId(), PENDING, r.getMetadataKey(), r.getMetadataValue()), 0));

            currentDesk.setFinishedFoldersCount(currentDesk.getFinishedFoldersCount()
                    + countMap.getOrDefault(new DelegationRule(currentDesk.getId(), FINISHED, r.getMetadataKey(), r.getMetadataValue()), 0));

            currentDesk.setRejectedFoldersCount(currentDesk.getRejectedFoldersCount()
                    + countMap.getOrDefault(new DelegationRule(currentDesk.getId(), REJECTED, r.getMetadataKey(), r.getMetadataValue()), 0));
        });
    }


    public void sanitizeDeskDto(DeskDto dto) {

        if (dto.getOwners() == null) return;

        // Filter internal users from desk owners

        List<User> owners = dto.getOwners()
                .stream()
                .filter(o -> !INTERNAL_USERNAMES.contains(o.getUserName()))
                .map(owner -> modelMapper.map(owner, User.class))
                .collect(toList());

        userBusinessService.updatePrivileges(owners, null);
        dto.setOwners(owners.stream().map(owner -> modelMapper.map(owner, UserRepresentation.class)).toList());
    }

}
