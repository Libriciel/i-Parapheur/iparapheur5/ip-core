/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.business.auth;

import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.auth.requests.DeskDto;
import coop.libriciel.ipcore.model.auth.requests.UserDto;
import coop.libriciel.ipcore.model.auth.requests.UserRepresentation;
import coop.libriciel.ipcore.model.database.TenantRepresentation;
import coop.libriciel.ipcore.model.database.userPreferences.FolderFilter;
import coop.libriciel.ipcore.model.database.userPreferences.UserPreferences;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.database.DatabaseServiceInterface;
import coop.libriciel.ipcore.services.database.FolderFilterRepository;
import coop.libriciel.ipcore.services.database.UserPreferencesRepository;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static coop.libriciel.ipcore.model.auth.UserPrivilege.*;
import static java.util.Comparator.*;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.collections4.CollectionUtils.subtract;
import static org.springframework.data.domain.Pageable.unpaged;
import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE;
import static org.springframework.util.CollectionUtils.isEmpty;


@Log4j2
@Service
public class UserBusinessService {


    // <editor-fold desc="Beans">


    private final AuthServiceInterface authService;
    private final DatabaseServiceInterface dbService;
    private final FolderFilterRepository folderFilterRepository;
    private final ModelMapper modelMapper;
    private final PermissionServiceInterface permissionService;
    private final UserPreferencesRepository userPreferencesRepository;


    public UserBusinessService(AuthServiceInterface authService,
                               DatabaseServiceInterface dbService,
                               FolderFilterRepository folderFilterRepository,
                               ModelMapper modelMapper,
                               PermissionServiceInterface permissionService,
                               UserPreferencesRepository userPreferencesRepository) {
        this.authService = authService;
        this.dbService = dbService;
        this.folderFilterRepository = folderFilterRepository;
        this.modelMapper = modelMapper;
        this.permissionService = permissionService;
        this.userPreferencesRepository = userPreferencesRepository;
    }


    // </editor-fold desc="Beans">


    private void updateInnerValues(@NotNull Map<Supplier<List<String>>, Consumer<List<UserRepresentation>>> userIdsAccessors) {

        Set<String> referencedUserIds = new HashSet<>();
        userIdsAccessors.keySet().stream()
                .map(Supplier::get)
                .filter(CollectionUtils::isNotEmpty)
                .forEach(referencedUserIds::addAll);

        Map<String, UserRepresentation> fullUsers = authService.getUsersByIds(referencedUserIds).stream()
                .collect(toMap(
                        User::getId,
                        user -> modelMapper.map(user, UserRepresentation.class)
                ));

        userIdsAccessors.forEach((key, value) -> {

            if (CollectionUtils.isEmpty(key.get())) {
                return;
            }

            List<UserRepresentation> userRepresentationList = key.get().stream()
                    .map(fullUsers::get)
                    .sorted(comparing(UserRepresentation::getUserName, nullsLast(naturalOrder())))
                    .toList();

            value.accept(userRepresentationList);
        });
    }


    public void updateGlobalInnerValues(@Nullable UserDto userDto) {

        if (userDto == null) {
            return;
        }

        Set<String> associatedTenants = authService.listTenantsForUser(userDto.getId());

        List<TenantRepresentation> administeredTenants = dbService
                .listTenantsForUser(userDto.getId(), unpaged(), null, false, true, false)
                .getContent();

        userDto.setAssociatedTenantIds(new ArrayList<>(associatedTenants));
        userDto.setAdministeredTenantIds(administeredTenants.stream().map(TenantRepresentation::getId).toList());
    }


    public void updateInnerUserValues(@Nullable DeskDto deskDto) {

        if (deskDto == null) {
            return;
        }

        updateInnerValues(Map.of(
                deskDto::getOwnerIds, deskDto::setOwners,
                deskDto::getSupervisorIds, deskDto::setSupervisors,
                deskDto::getDelegationManagerIds, deskDto::setDelegationManagers
        ));
    }


    public void updateInnerUserValues(@Nullable Collection<Folder> folders) {

        if (CollectionUtils.isEmpty(folders)) {
            return;
        }

        // Compute some lists

        List<UserRepresentation> referencedUsers = Stream.of(
                        folders.stream()
                                .filter(Objects::nonNull)
                                .map(Folder::getStepList)
                                .flatMap(Collection::stream)
                                .map(Task::getUser),
                        folders.stream()
                                .filter(Objects::nonNull)
                                .map(Folder::getOriginUser)
                )
                .flatMap(s -> s)
                .filter(Objects::nonNull)
                .toList();

        Set<String> referencedUserIds = referencedUsers.stream()
                .map(UserRepresentation::getId)
                .filter(StringUtils::isNotEmpty)
                .collect(toSet());

        // Fetch the full models

        Map<String, UserRepresentation> userNamesMap = authService.getUsersByIds(referencedUserIds).stream()
                .map(user -> modelMapper.map(user, UserRepresentation.class))
                .collect(toMap(
                        UserRepresentation::getId,
                        user -> user
                ));

        // Replace in place

        referencedUsers.stream()
                .filter(user -> userNamesMap.containsKey(user.getId()))
                .forEach(user -> modelMapper.map(userNamesMap.get(user.getId()), user));
    }


    public void updateTenantDeskInnerValues(@NotNull String tenantId, @Nullable UserDto userDto) {

        if (userDto == null) {
            return;
        }

        List<DeskRepresentation> administeredDesks = permissionService.getAdministeredDesks(userDto.getId(), tenantId);
        List<DeskRepresentation> associatedDesks = new ArrayList<>(authService.getDesksFromUser(userDto.getId(), unpaged(), null).getContent());
        List<DeskRepresentation> delegationManagedDesks = permissionService.getDelegationManagedDesks(userDto.getId(), tenantId);
        List<DeskRepresentation> supervisedDesks = permissionService.getSupervisedDesks(userDto.getId(), tenantId);
        associatedDesks.removeIf(desk -> !StringUtils.equals(desk.getTenantId(), tenantId));

        userDto.setAdministeredDeskIds(administeredDesks.stream().map(DeskRepresentation::getId).toList());
        userDto.setAssociatedDeskIds(associatedDesks.stream().map(DeskRepresentation::getId).toList());
        userDto.setDelegationManagedDeskIds(delegationManagedDesks.stream().map(DeskRepresentation::getId).toList());
        userDto.setSupervisedDeskIds(supervisedDesks.stream().map(DeskRepresentation::getId).toList());
    }


    /**
     * Update the privileges on given user list
     *
     * @param users    the collection to update
     * @param tenantId if set, adds the context, for the functional admins
     */
    public void updatePrivileges(@NotNull Collection<User> users, @Nullable String tenantId) {

        Set<String> userIds = users.stream().map(User::getId).collect(toSet());
        Set<String> admins = authService.filterUsersWithPrivilege(userIds, SUPER_ADMIN, tenantId);
        Set<String> tenantAdmins = authService.filterUsersWithPrivilege(userIds, TENANT_ADMIN, tenantId);
        Set<String> functionalAdmins = authService.filterUsersWithPrivilege(userIds, FUNCTIONAL_ADMIN, tenantId);

        users.forEach(u -> u.setPrivilege(NONE));

        users.stream()
                .filter(u -> functionalAdmins.contains(u.getId()))
                .forEach(u -> u.setPrivilege(FUNCTIONAL_ADMIN));

        users.stream()
                .filter(u -> tenantAdmins.contains(u.getId()))
                .forEach(u -> u.setPrivilege(TENANT_ADMIN));

        users.stream()
                .filter(u -> admins.contains(u.getId()))
                .forEach(u -> u.setPrivilege(SUPER_ADMIN));
    }


    public void computeGlobalLevelDifferencesAndUpdateUserRolesAndPermissions(User existingUser, UserDto requestUserDto) {

        String userId = existingUser.getId();

        // Integrity checks

        boolean isSuperAdmin = requestUserDto.getPrivilege() == SUPER_ADMIN;
        boolean isTenantLess = isEmpty(requestUserDto.getAssociatedTenantIds()) && isEmpty(requestUserDto.getAdministeredTenantIds());
        if (isTenantLess && !isSuperAdmin) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.a_non_super_admin_should_be_associated_with_at_least_one_tenant");
        }

        // Refresh every user sub-permissions, to ease the following diff computing.

        UserDto existingUserDto = modelMapper.map(existingUser, UserDto.class);
        updateGlobalInnerValues(existingUserDto);

        // Compute differences

        Collection<String> administeredTenantIdsToAdd = subtract(requestUserDto.getAdministeredTenantIds(), existingUserDto.getAdministeredTenantIds());
        Collection<String> administeredTenantIdsToRemove = subtract(existingUserDto.getAdministeredTenantIds(), requestUserDto.getAdministeredTenantIds());

        log.trace("updateUser requestUserDto.getAdministeredTenantIds:{}", requestUserDto.getAdministeredTenantIds());
        log.trace("updateUser existingUserDto.getAdministeredTenantIds:{}", existingUserDto.getAdministeredTenantIds());
        log.trace("updateUser administeredTenantIdsToAdd:{}", administeredTenantIdsToAdd);
        log.trace("updateUser administeredTenantIdsToRemove:{}", administeredTenantIdsToRemove);

        administeredTenantIdsToAdd.forEach(tenantId -> authService.addUserToTenant(existingUser, tenantId, TENANT_ADMIN));
        administeredTenantIdsToRemove.forEach(tenantId -> authService.addUserToTenant(existingUser, tenantId, NONE));

        Collection<String> tenantIdsToAdd = subtract(requestUserDto.getAssociatedTenantIds(), existingUserDto.getAssociatedTenantIds());
        Collection<String> tenantIdsToRemove = subtract(existingUserDto.getAssociatedTenantIds(), requestUserDto.getAssociatedTenantIds());

        log.trace("updateUser requestUserDto.getAssociatedTenantIds:{}", requestUserDto.getAssociatedTenantIds());
        log.trace("updateUser existingUserDto.getAssociatedTenantIds:{}", existingUserDto.getAssociatedTenantIds());
        log.trace("updateUser tenantIdsToAdd:{}", tenantIdsToAdd);
        log.trace("updateUser tenantIdsToRemove:{}", tenantIdsToRemove);

        tenantIdsToAdd.forEach(tenantId -> authService.addUserToTenant(existingUser, tenantId, NONE));
        tenantIdsToRemove.forEach(tenantId -> authService.removeUserFromTenant(userId, tenantId));
    }


    public void computeTenantLevelDifferencesAndUpdateUserRolesAndPermissions(@NotNull String tenantId, User existingUser, UserDto request) {

        if (request.getSupervisedDeskIds() != null) {

            List<String> originalSupervisedDeskIds = permissionService.getSupervisedDesks(existingUser.getId(), tenantId).stream()
                    .map(DeskRepresentation::getId)
                    .toList();

            originalSupervisedDeskIds.stream()
                    .filter(origId -> request.getSupervisedDeskIds().stream().noneMatch(requestId -> StringUtils.equals(origId, requestId)))
                    .forEach(deskId -> permissionService.removeSupervisorsFromDesk(tenantId, deskId, List.of(existingUser.getId())));

            request.getSupervisedDeskIds().stream()
                    .filter(requestId -> originalSupervisedDeskIds.stream().noneMatch(origId -> StringUtils.equals(requestId, origId)))
                    .forEach(deskId -> permissionService.addSupervisorsToDesk(tenantId, deskId, List.of(existingUser.getId())));
        }

        if (request.getPrivilege() != FUNCTIONAL_ADMIN) {
            log.debug("remove all administered desks");
            permissionService.removeAllAdministeredDesks(tenantId, existingUser.getId());
        } else if (request.getAdministeredDeskIds() != null) {

            List<String> originalAdministeredDeskIds = permissionService.getAdministeredDesks(existingUser.getId(), tenantId).stream()
                    .map(DeskRepresentation::getId)
                    .toList();

            originalAdministeredDeskIds.stream()
                    .filter(origId -> request.getAdministeredDeskIds().stream().noneMatch(requestId -> StringUtils.equals(origId, requestId)))
                    .forEach(deskId -> permissionService.removeFunctionalAdmin(tenantId, deskId, existingUser.getId()));

            request.getAdministeredDeskIds().stream()
                    .filter(requestId -> originalAdministeredDeskIds.stream().noneMatch(origId -> StringUtils.equals(requestId, origId)))
                    .forEach(deskId -> permissionService.setFunctionalAdmin(tenantId, deskId, existingUser.getId()));
        }

        if (request.getDelegationManagedDeskIds() != null) {

            List<String> originalDelegationManagerIds = permissionService.getDelegationManagedDesks(existingUser.getId(), tenantId).stream()
                    .map(DeskRepresentation::getId)
                    .toList();

            originalDelegationManagerIds.stream()
                    .filter(origId -> request.getDelegationManagedDeskIds().stream().noneMatch(requestId -> StringUtils.equals(origId, requestId)))
                    .forEach(deskId -> permissionService.removeDelegationManagersFromDesk(tenantId, deskId, List.of(existingUser.getId())));

            request.getDelegationManagedDeskIds().stream()
                    .filter(requestId -> originalDelegationManagerIds.stream().noneMatch(origId -> StringUtils.equals(requestId, origId)))
                    .forEach(deskId -> permissionService.addDelegationManagersToDesk(tenantId, deskId, List.of(existingUser.getId())));
        }

        if (request.getAssociatedDeskIds() != null) {

            List<String> originalAssociatedDeskIds = authService.getDesksFromUser(existingUser.getId(), unpaged(), null)
                    .filter(desk -> StringUtils.equals(tenantId, desk.getTenantId()))
                    .map(DeskRepresentation::getId)
                    .toList();

            originalAssociatedDeskIds.stream()
                    .filter(origId -> request.getAssociatedDeskIds().stream().noneMatch(requestId -> StringUtils.equals(origId, requestId)))
                    .forEach(deskId -> authService.removeUsersFromDesk(deskId, List.of(existingUser.getId())));

            request.getAssociatedDeskIds().stream()
                    .filter(requestId -> originalAssociatedDeskIds.stream().noneMatch(origId -> StringUtils.equals(requestId, origId)))
                    .forEach(deskId -> authService.addUsersToDesk(deskId, List.of(existingUser.getId())));
        }
    }


    public void deleteFilterForUser(String userId, FolderFilter folderFilter) {
        removeFilterRefFromUserPrefIfNeeded(userId, folderFilter);
        folderFilterRepository.delete(folderFilter);
    }


    private void removeFilterRefFromUserPrefIfNeeded(String userId, FolderFilter folderFilter) {
        Optional<UserPreferences> userPrefOpt = userPreferencesRepository.getByUserId(userId);
        userPrefOpt.filter(userPref -> userPref.getCurrentFilter() != null)
                .filter(userPref -> userPref.getCurrentFilter().equals(folderFilter))
                .ifPresent(userPref -> {
                    userPref.setCurrentFilter(null);
                    userPreferencesRepository.save(userPref);
                });
    }

}
