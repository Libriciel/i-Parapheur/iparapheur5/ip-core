/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.business.typology;

import coop.libriciel.ipcore.business.auth.UserBusinessService;
import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.SubtypeMetadata;
import coop.libriciel.ipcore.model.database.Type;
import coop.libriciel.ipcore.model.database.TypologyEntity;
import coop.libriciel.ipcore.model.database.requests.SubtypeRepresentation;
import coop.libriciel.ipcore.model.database.requests.TypeRepresentation;
import coop.libriciel.ipcore.model.database.userPreferences.FolderFilter;
import coop.libriciel.ipcore.model.permission.DelegationDto;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.services.content.ContentServiceProperties;
import coop.libriciel.ipcore.services.database.FolderFilterRepository;
import coop.libriciel.ipcore.services.database.SubtypeRepository;
import coop.libriciel.ipcore.services.database.TypeRepository;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.RequestUtils;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Supplier;

import static coop.libriciel.ipcore.model.crypto.SignatureFormat.AUTO;
import static coop.libriciel.ipcore.model.database.SubtypeMetadata.SUBTYPE_METADATA_COMPARATOR;
import static coop.libriciel.ipcore.utils.RequestUtils.returnRedactedResponse;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;
import static org.springframework.data.domain.Pageable.unpaged;
import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE;
import static org.springframework.http.HttpStatus.NOT_FOUND;


/**
 * Utility class to factorize typology-related logic, using repositories.
 */
@Service
@Log4j2
public class TypologyBusinessService {


    // <editor-fold desc="Beans">


    private final ContentServiceProperties contentServiceProperties;
    private final FolderFilterRepository folderFilterRepository;
    private final ModelMapper modelMapper;
    private final PermissionServiceInterface permissionService;
    private final SubtypeRepository subtypeRepository;
    private final TypeRepository typeRepository;
    private final UserBusinessService userBusinessService;


    @Autowired
    public TypologyBusinessService(ContentServiceProperties contentServiceProperties,
                                   FolderFilterRepository folderFilterRepository,
                                   ModelMapper modelMapper,
                                   PermissionServiceInterface permissionService,
                                   SubtypeRepository subtypeRepository,
                                   TypeRepository typeRepository,
                                   UserBusinessService userBusinessService) {
        this.contentServiceProperties = contentServiceProperties;
        this.folderFilterRepository = folderFilterRepository;
        this.modelMapper = modelMapper;
        this.permissionService = permissionService;
        this.subtypeRepository = subtypeRepository;
        this.typeRepository = typeRepository;
        this.userBusinessService = userBusinessService;
    }


    // </editor-fold desc="Beans">


    public void deleteSubtype(String id) {
        List<FolderFilter> subtypeFilters = folderFilterRepository.findAllBySubtypeIdIsIn(List.of(id));
        subtypeFilters.forEach(filter -> userBusinessService.deleteFilterForUser(filter.getUserId(), filter));

        permissionService.deletePermission(id);
        subtypeRepository.deleteById(id);
    }


    public void deleteType(String tenantId, String typeId) {
        List<FolderFilter> typeFilters = folderFilterRepository.findAllByTypeId(typeId);
        typeFilters.forEach(filter -> userBusinessService.deleteFilterForUser(filter.getUserId(), filter));

        Page<Subtype> subtypes = subtypeRepository.findAllByTenant_IdAndParentType_Id(tenantId, typeId, unpaged());
        subtypes.forEach(subtype -> {
            try {
                deleteSubtype(subtype.getId());
            } catch (Exception e) {
                log.error("An error occurred while deleting the subtype {}", subtype.getId(), e);
            }
        });

        typeRepository.deleteById(typeId);
    }


    public void checkTypeReplacementCompatibility(@NotNull Type oldType, @NotNull Type newType) {

        if ((oldType.getSignatureFormat() == AUTO) || (newType.getSignatureFormat()) == AUTO) {
            throw new RuntimeException("The signature format should have been evaluated before the checkTypeReplacementCompatibility call");
        }

        boolean isDifferentFormat = oldType.getSignatureFormat() != newType.getSignatureFormat();
        boolean isNewFormatEnveloped = newType.getSignatureFormat().isEnvelopedSignature();
        if (isDifferentFormat && isNewFormatEnveloped) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.folder_incompatible_with_an_enveloped_signature_typology");
        }
    }


    public void checkSubtypeReplacementCompatibility(@NotNull Subtype newSubType, @NotNull List<Document> documents) {

        long mainDocumentsSize = documents.stream()
                .filter(Document::isMainDocument)
                .count();

        int maxDocument = newSubType.isMultiDocuments() ? contentServiceProperties.getMaxMainFiles() : 1;
        if (mainDocumentsSize > maxDocument) {
            log.error("checkSubtypeReplacementCompatibility - maxDocuments:{} < existingDocuments:{}, abort", maxDocument, mainDocumentsSize);
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.the_amount_of_existing_documents_is_incompatible_with_the_chosen_subtype");
        }
    }


    private void updateInnerValues(@NotNull String tenantId,
                                   @NotNull Map<Supplier<String>, Consumer<Type>> typeAccessors,
                                   @NotNull Map<Supplier<String>, Consumer<Subtype>> subtypeAccessors,
                                   boolean checkIntegrity) {

        Set<String> typeIds = typeAccessors.keySet().stream()
                .filter(Objects::nonNull)
                .map(Supplier::get)
                .filter(StringUtils::isNotEmpty)
                .collect(toSet());

        Set<String> subtypeIds = subtypeAccessors.keySet().stream()
                .filter(Objects::nonNull)
                .map(Supplier::get)
                .filter(StringUtils::isNotEmpty)
                .collect(toSet());

        Map<String, Type> typesMap = typeRepository.findAllByTenant_IdAndIdIn(tenantId, typeIds, unpaged()).getContent().stream()
                .collect(toMap(
                        Type::getId,
                        type -> type
                ));

        Map<String, Subtype> subtypesMap = subtypeRepository.findAllByTenant_IdAndIdIn(tenantId, subtypeIds, unpaged()).getContent().stream()
                .collect(toMap(
                        Subtype::getId,
                        subtype -> subtype
                ));

        // Integrity

        if (checkIntegrity) {

            if (typeAccessors.keySet().stream()
                    .map(Supplier::get)
                    .filter(StringUtils::isNotEmpty)
                    .anyMatch(id -> !typesMap.containsKey(id))) {
                throw new LocalizedStatusException(NOT_FOUND, "message.unknown_type_id");
            }

            if (subtypeAccessors.keySet().stream()
                    .map(Supplier::get)
                    .filter(StringUtils::isNotEmpty)
                    .anyMatch(id -> !subtypesMap.containsKey(id))) {
                throw new LocalizedStatusException(NOT_FOUND, "message.unknown_subtype_id");
            }
        }

        // Metadata sorts

        subtypesMap.values().stream()
                .filter(Objects::nonNull)
                .map(Subtype::getSubtypeMetadataList)
                .filter(CollectionUtils::isNotEmpty)
                .forEach(subtypeMetadataList -> subtypeMetadataList.sort(SUBTYPE_METADATA_COMPARATOR));

        // Write in place

        typeAccessors.entrySet().stream()
                .filter(e -> e.getKey() != null)
                .filter(e -> StringUtils.isNotEmpty(e.getKey().get()))
                .filter(e -> typesMap.containsKey(e.getKey().get()))
                .forEach(e -> e.getValue().accept(typesMap.get(e.getKey().get())));

        subtypeAccessors.entrySet().stream()
                .filter(e -> e.getKey() != null)
                .filter(e -> StringUtils.isNotEmpty(e.getKey().get()))
                .filter(e -> subtypesMap.containsKey(e.getKey().get()))
                .forEach(e -> e.getValue().accept(subtypesMap.get(e.getKey().get())));
    }


    public void updateInnerValues(@NotNull String tenantId, @NotNull List<DelegationDto> delegationList) {
        updateInnerValues(
                tenantId,
                delegationList.stream()
                        .collect(toMap(
                                delegationDto -> delegationDto::getTypeId,
                                delegationDto -> type -> Optional.ofNullable(type)
                                        .map(t -> returnRedactedResponse(modelMapper, t, TypeRepresentation::new))
                                        .ifPresent(delegationDto::setType)
                        )),
                delegationList.stream()
                        .collect(toMap(
                                delegationDto -> delegationDto::getSubtypeId,
                                delegationDto -> subtype -> Optional.ofNullable(subtype)
                                        .map(st -> returnRedactedResponse(modelMapper, st, SubtypeRepresentation::new))
                                        .ifPresent(delegationDto::setSubtype)
                        )),
                false
        );
    }


    public void updateTypology(@NotNull String tenantId, @NotNull List<? extends Folder> folders, boolean checkIntegrity) {

        updateInnerValues(
                tenantId,
                folders.stream()
                        .collect(toMap(
                                folder -> (Supplier<String>) () -> Optional.ofNullable(folder.getType())
                                        .map(Type::getId)
                                        .orElse(null),
                                folder -> (Consumer<Type>) type -> Optional.ofNullable(type)
                                        .ifPresent(folder::setType)
                        )),
                folders.stream()
                        .collect(toMap(
                                folder -> (Supplier<String>) () -> Optional.ofNullable(folder.getSubtype())
                                        .map(Subtype::getId)
                                        .orElse(null),
                                folder -> (Consumer<Subtype>) subtype -> Optional.ofNullable(subtype)
                                        .ifPresent(folder::setSubtype)
                        )),
                checkIntegrity
        );
    }


    public void verifyFolderMetadataForValidationWorkflow(@NotNull String tenantId, @NotNull Folder folder) {

        Subtype fullSubtype = subtypeRepository.findByIdAndTenant_Id(folder.getSubtype().getId(), tenantId)
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_subtype_id"));

        List<String> missingMandatoryMetadata = getMissingMandatoryMetadata(folder.getMetadata(), fullSubtype.getSubtypeMetadataList(), false);
        if (CollectionUtils.isNotEmpty(missingMandatoryMetadata)) {
            log.debug("verifyFolderMetadata - Some mandatory metadata were not filled, abort: {}", missingMandatoryMetadata);
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.missing_mandatory_metadata", missingMandatoryMetadata.get(0));
        }
    }


    /**
     * Compute the missing mandatory metadata missing from the input.
     *
     * @param inputMetadata       a (metadataKey:metadataValue) map. NOT AN (ID:VALUE) MAP !!
     * @param subtypeMetadataList
     * @param allowAllEditable
     * @return
     */
    public static List<String> getMissingMandatoryMetadata(@NotNull Map<String, String> inputMetadata,
                                                           @NotNull List<SubtypeMetadata> subtypeMetadataList,
                                                           boolean allowAllEditable) {
        return subtypeMetadataList.stream()
                .filter(SubtypeMetadata::isMandatory)
                .filter(meta -> StringUtils.isEmpty(meta.getDefaultValue()))
                .filter(meta -> !(meta.isEditable() && allowAllEditable))
                .map(subtypeMetadata -> subtypeMetadata.getMetadata().getKey())
                .filter(e -> !inputMetadata.containsKey(e))
                .toList();
    }


}
