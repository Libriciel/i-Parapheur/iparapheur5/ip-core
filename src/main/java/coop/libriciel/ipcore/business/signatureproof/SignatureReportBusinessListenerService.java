/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.business.signatureproof;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Map;


@Log4j2
@Service
public class SignatureReportBusinessListenerService implements MessageListener {


    private final MessageProcessingService messageProcessingService;
    private final ObjectMapper objectMapper;


    public SignatureReportBusinessListenerService(MessageProcessingService messageProcessingService,
                                                  ObjectMapper objectMapper) {
        this.messageProcessingService = messageProcessingService;
        this.objectMapper = objectMapper;
    }


    @Override
    public void onMessage(@NotNull Message message, byte[] pattern) {
        try {

            TypeReference<Map<String, String>> typeRef = new TypeReference<>() {};
            Map<String, String> map = objectMapper.readValue(message.toString(), typeRef);

            messageProcessingService.processMessage(map);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
