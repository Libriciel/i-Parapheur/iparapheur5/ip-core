/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.business.signatureproof;

import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.exceptions.MissingTagException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import static coop.libriciel.ipcore.configuration.RedisConfigurer.*;

import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import static coop.libriciel.ipcore.utils.TextUtils.MESSAGE_BUNDLE;


@Log4j2
@Service
public class MessageProcessingService {


    private final ResourceBundle message = ResourceBundle.getBundle(MESSAGE_BUNDLE, Locale.getDefault());


    // <editor-fold desc="Beans">


    private final SignatureReportBusinessService signatureReportBusinessService;
    private final WorkflowServiceInterface workflowService;


    @Autowired
    public MessageProcessingService(SignatureReportBusinessService signatureReportBusinessService,
                                    WorkflowServiceInterface workflowService) {
        this.signatureReportBusinessService = signatureReportBusinessService;
        this.workflowService = workflowService;
    }


    // </editor-fold desc="Beans">


    /**
     * Careful here, there can be only one "retryable" method per service.
     * Do not copy-paste this in the same class.
     */
    @Retryable(
            retryFor = MissingTagException.class,
            maxAttempts = MissingTagException.RETRY_ATTEMPTS,
            backoff = @Backoff(delay = MissingTagException.RETRY_DELAY)
    )
    public void processMessage(Map<String, String> map) throws MissingTagException {

        if (!signatureReportBusinessService.isSignatureValidationServiceActivated()) {
            return;
        }

        Folder folder = workflowService.getFolder(map.get(FOLDER_ID_KEY), map.get(TENANT_ID_KEY), false);

        if (!signatureReportBusinessService.qualifyForSignatureReport(folder)) {
            String errorMessage = message.getString("message.waiting_for_alfresco_service");
            signatureReportBusinessService.saveError(folder, errorMessage);
            throw new MissingTagException(errorMessage);
        }

        signatureReportBusinessService.generateAllSignaturesReportAutomatic(folder);
    }


}
