/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.business.signatureproof;

import com.fasterxml.jackson.core.JsonProcessingException;
import coop.libriciel.ipcore.business.content.DocumentBusinessService;
import coop.libriciel.ipcore.model.content.*;
import coop.libriciel.ipcore.model.crypto.SignatureFormat;
import coop.libriciel.ipcore.model.database.Type;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.crypto.CryptoServiceInterface;
import coop.libriciel.ipcore.services.database.TypeRepository;
import coop.libriciel.ipcore.services.secret.SecretServiceInterface;
import coop.libriciel.ipcore.services.signatureValidation.SignatureValidationServiceInterface;
import coop.libriciel.ipcore.utils.FileUtils;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.exceptions.ExternalException;
import io.micrometer.common.util.StringUtils;
import jakarta.annotation.PostConstruct;
import lombok.extern.log4j.Log4j2;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.IntStream;

import static coop.libriciel.ipcore.services.signatureValidation.ChorusProAbstractService.*;
import static coop.libriciel.ipcore.utils.LocalizedStatusException.getMessageForLocale;
import static coop.libriciel.ipcore.utils.TextUtils.MESSAGE_BUNDLE;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_PDF;


@Log4j2
@Service
public class SignatureReportBusinessService {


    public static final String CLAIMED_DATE_ELEMENT = "claimedDate";
    public static final String EIDAS_LEVEL_ELEMENT = "eIDASLevel";
    public static final String ERRORS_NODE = "errors";
    public static final String ERROR = "error";
    public static final String IDENTIFIER_ELEMENT = "identifier";
    public static final String SIGNATURE_ERRORS_NODE = "signatureErrors";
    public static final String SIGNATURE_ID = "signatureId";
    public static final String SIGNATURE_REPORTS_NODE = "signatureReports";
    public static final String SIGNATURE_REPORT_NODE = "signatureReport";
    public static final String STATUS_ELEMENT = "status";
    public static final String SIGNER_CERTIFICATE_ELEMENT = "signerCertificate";
    public static final String SUBJECT_DN_ELEMENT = "subjectDN";
    public static final String ISSUER_DN_ELEMENT = "issuerDN";
    public static final String NOT_BEFORE_ELEMENT = "notBefore";
    public static final String NOT_AFTER_ELEMENT = "notAfter";
    public static final String PDF_EXTENSION = ".pdf";

    private final ResourceBundle resourceBundle = ResourceBundle.getBundle(MESSAGE_BUNDLE, Locale.getDefault());
    private final String reportName = resourceBundle.getString("message.iparapheur_signature_report");


    private boolean signatureValidationServiceIsActivated;


    // <editor-fold desc="Beans">


    private final ContentServiceInterface contentService;
    private final CryptoServiceInterface cryptoService;
    private final DocumentBusinessService documentBusinessService;
    private final SecretServiceInterface secretServiceInterface;
    private final SignatureValidationServiceInterface signatureValidationService;
    private final TypeRepository typeRepository;


    @Autowired
    public SignatureReportBusinessService(ContentServiceInterface contentService,
                                          CryptoServiceInterface cryptoService,
                                          DocumentBusinessService documentBusinessService,
                                          SecretServiceInterface secretServiceInterface,
                                          SignatureValidationServiceInterface signatureValidationServiceInterface,
                                          TypeRepository typeRepository) {
        this.contentService = contentService;
        this.cryptoService = cryptoService;
        this.documentBusinessService = documentBusinessService;
        this.secretServiceInterface = secretServiceInterface;
        this.signatureValidationService = signatureValidationServiceInterface;
        this.typeRepository = typeRepository;
    }


    // </editor-fold desc="Beans">


    @PostConstruct
    public void onSetup() {
        signatureValidationServiceIsActivated = secretServiceInterface.getValidationServiceConfiguration() != null;
    }


    public boolean isSignatureValidationServiceActivated() {
        return signatureValidationServiceIsActivated;
    }


    public boolean qualifyForSignatureReport(Folder folder) {
        contentService.populateFolderWithAllDocumentTypes(folder);

        List<Document> mainDocumentList = folder.getDocumentList().stream().filter(Document::isMainDocument).toList();

        boolean containsEmbeddedSignature = mainDocumentList.stream().anyMatch(d -> !CollectionUtils.isEmpty(d.getEmbeddedSignatureInfos()));
        boolean containsDetachedSignature = mainDocumentList.stream().anyMatch(d -> !CollectionUtils.isEmpty(d.getDetachedSignatures()));
        if (!containsEmbeddedSignature && !containsDetachedSignature) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.do_not_qualify_for_signature_report");
        }

        Type type = typeRepository.findById(folder.getType().getId())
                .orElseThrow(() -> {
                    log.error("Error on qualifyForSignatureReport - find type - folderId:{}", folder.getId());
                    return new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.signature_format_not_fount");
                });

        SignatureFormat signatureFormat = cryptoService.getSignatureFormat(type, folder.getDocumentList());

        return switch (signatureFormat) {
            case AUTO -> {
                log.error("Error on qualifyForSignatureReport - case AUTO - folderId:{}", folder.getId());
                throw new RuntimeException("The signature format should have been evaluated before the qualifyForSignatureReport call");
            }
            case PADES -> containsEmbeddedSignature;
            case XADES_DETACHED -> containsDetachedSignature;
            // Todo: Update when pkcs7 and pes_v2 qualify for signature verification
            case PKCS7, PES_V2 -> throw new LocalizedStatusException(BAD_REQUEST, "message.do_not_qualify_for_signature_report");
        };
    }


    private void deleteExistingReport(List<String> signatureReportsId) {
        signatureReportsId.forEach(contentService::deleteDocument);
    }


    public SignatureProof generateAllSignaturesReport(Folder folder) {
        if (!isSignatureValidationServiceActivated()) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.error_service_deactivated");
        }

        if (!qualifyForSignatureReport(folder)) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.do_not_qualify_for_signature_report");
        }

        try {
            return getSignaturesProof(folder);
        } catch (Exception e) {
            log.error("generateAllSignaturesReport Error: " + e.getMessage());
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.error_signature_report_generation", e.getMessage());
        }
    }


    /**
     * Careful here, there can be only one "retryable" method per service.
     * Do not copy-paste this in the same class.
     */
    @Retryable(
            retryFor = ExternalException.class,
            maxAttempts = ExternalException.RETRY_ATTEMPTS,
            backoff = @Backoff(delay = ExternalException.RETRY_DELAY)
    )
    public void generateAllSignaturesReportAutomatic(Folder folder) throws ExternalException {
        try {
            getSignaturesProof(folder);
        } catch (IOException | ParserConfigurationException | SAXException e) {
            saveError(folder, e.getMessage());
        } catch (ExternalException e) {
            saveError(folder, e.getMessage());
            throw e;
        }
    }


    public void saveError(Folder folder, String error) {
        log.error("generateAllSignaturesReport - attempt for signature proof generation: " + error);

        List<String> generatedSignatureReportList = getSignatureReportsId(folder);

        MediatypeFileResource emptyDocument = new MediatypeFileResource(
                new ByteArrayInputStream(new byte[0]),
                null,
                MediaType.APPLICATION_PDF,
                0,
                reportName + PDF_EXTENSION);

        try {
            File emptyFile = FileUtils.createTempFile(emptyDocument.getInputStream(), emptyDocument.getName(), PDF_EXTENSION);
            DocumentBuffer emptyReportBuffer = FileUtils.getDocumentBufferFromFile(emptyFile, reportName + PDF_EXTENSION, true);
            contentService.createSignatureProof(
                    folder.getContentId(),
                    emptyReportBuffer,
                    true,
                    getMessageForLocale("message.error_signature_report_generation", Locale.getDefault(), error),
                    null);
        } catch (Exception e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e.getMessage());
        }

        if (!generatedSignatureReportList.isEmpty()) {
            deleteExistingReport(generatedSignatureReportList);

            folder.getDocumentList()
                    .stream()
                    .filter(Document::isMainDocument)
                    .forEach(document -> {
                        if (!CollectionUtils.isEmpty(document.getDetachedSignatures())) {
                            document.getDetachedSignatures().forEach(detachedSignature -> {
                                ValidatedSignatureInformation signatureInfo = detachedSignature.getSignatureInfo();
                                resetSignatureInfo(signatureInfo);
                                try {
                                    contentService.updateDetachedSignatureInformation(detachedSignature.getId(), signatureInfo);
                                } catch (JsonProcessingException e) {
                                    throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e.getMessage());
                                }
                            });
                        } else if (!CollectionUtils.isEmpty(document.getEmbeddedSignatureInfos())) {
                            document.getEmbeddedSignatureInfos().forEach(SignatureReportBusinessService::resetSignatureInfo);
                            try {
                                contentService.updateEmbeddedSignatureInformation(document.getId(), document.getEmbeddedSignatureInfos());
                            } catch (JsonProcessingException e) {
                                throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e.getMessage());
                            }
                        }
                    });
        }
    }


    private static void resetSignatureInfo(ValidatedSignatureInformation signatureInfo) {
        signatureInfo.setIsSignatureValid(null);
        signatureInfo.setError(null);
        signatureInfo.setEidasLevel(null);
        signatureInfo.setCertificateBeginDate(null);
        signatureInfo.setCertificateEndDate(null);
    }


    private SignatureProof getSignaturesProof(Folder folder) throws IOException, ParserConfigurationException, SAXException, ExternalException {

        List<String> oldReportsIdList = getSignatureReportsId(folder);
        List<File> fileList = new ArrayList<>();
        List<Document> mainDocumentList = folder.getDocumentList().stream().filter(Document::isMainDocument).toList();

        for (Document document : mainDocumentList) {
            DocumentBuffer docContent = contentService.retrieveContent(document.getId());
            // FIXME : This is a workaround, the retrieve content shall populate this value, but does not.
            docContent.setName(document.getName());
            List<DetachedSignature> detachedSignatures = document.getDetachedSignatures();

            if (!detachedSignatures.isEmpty()) {
                Collection<DocumentBuffer> originalFiles = List.of(docContent);

                for (DetachedSignature detachedSignature : detachedSignatures) {
                    DocumentBuffer signContent = contentService.retrieveContent(detachedSignature.getId());
                    signContent.setName(detachedSignature.getName());

                    Map<String, String> reportMap = signatureValidationService.getSignatureValidation(signContent, originalFiles);
                    fileList.add(getFile(reportMap));

                    updateSignatureInfosWithXmlData(reportMap, List.of(detachedSignature.getSignatureInfo()));
                    contentService.updateDetachedSignatureInformation(detachedSignature.getId(), detachedSignature.getSignatureInfo());
                }
            } else {
                Map<String, String> reportMap = signatureValidationService.getSignatureValidation(docContent, null);
                fileList.add(getFile(reportMap));

                updateSignatureInfosWithXmlData(reportMap, document.getEmbeddedSignatureInfos());
                contentService.updateEmbeddedSignatureInformation(document.getId(), document.getEmbeddedSignatureInfos());
            }
        }

        File singleReport = (fileList.size() > 1) ? documentBusinessService.mergeStreamToPdf(fileList) : fileList.get(0);

        DocumentBuffer singleReportBuffer = FileUtils.getDocumentBufferFromFile(singleReport, reportName + PDF_EXTENSION, true);
        String signatureProofId = contentService.createSignatureProof(folder.getContentId(), singleReportBuffer, true, null, null);
        SignatureProof signatureProof = contentService.getSignatureProof(signatureProofId);

        deleteExistingReport(oldReportsIdList);

        return signatureProof;
    }


    private static void updateSignatureInfosWithXmlData(Map<String, String> reportMap,
                                                        List<ValidatedSignatureInformation> signatureInfos
    ) throws ParserConfigurationException, IOException, SAXException {
        String xmlString = new String(Base64.getDecoder().decode(reportMap.get(XML)));

        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        org.w3c.dom.Document doc = builder.parse(new ByteArrayInputStream(xmlString.getBytes()));

        Map<String, String> errorsMap = extractXmlErrors(doc);

        extractXmlDataAndUpdateSignatureInfos(doc, errorsMap, signatureInfos);
    }


    static @NotNull Map<String, String> extractXmlErrors(org.w3c.dom.Document doc) {
        Map<String, String> errorsMap = new HashMap<>();
        NodeList errorsNodeList = doc.getElementsByTagName(ERRORS_NODE);

        if (errorsNodeList.getLength() > 0) {
            Node node = errorsNodeList.item(0);
            NodeList child = node.getChildNodes();
            for (int i = 0; i < child.getLength(); i++) {
                Node item = child.item(i).getNextSibling();
                if (item != null && item.getNodeName().equals(SIGNATURE_ERRORS_NODE)) {
                    String signatureId = item.getAttributes().getNamedItem(SIGNATURE_ID).getNodeValue();

                    NodeList errorlist = item.getChildNodes();
                    StringBuilder allErrors = new StringBuilder();
                    for (int j = 0; j < errorlist.getLength(); j++) {
                        Node error = errorlist.item(j);
                        if (error != null && error.getNodeName().equals(ERROR)) {
                            if (!allErrors.isEmpty()) {
                                allErrors.append(", ");
                            }
                            allErrors.append(error.getFirstChild().getNodeValue());
                        }
                    }
                    errorsMap.put(signatureId, allErrors.toString());
                }
            }
        }
        return errorsMap;
    }


    static void extractXmlDataAndUpdateSignatureInfos(org.w3c.dom.Document doc, Map<String, String> errorsMap, List<ValidatedSignatureInformation> signatureInfos) {
        NodeList reportsNode = doc.getElementsByTagName(SIGNATURE_REPORTS_NODE);

        if (reportsNode.getLength() > 0) {
            Node node = reportsNode.item(0);
            NodeList child = node.getChildNodes();

            IntStream.range(0, child.getLength())
                    .mapToObj(child::item)
                    .map(Node::getNextSibling)
                    .filter(Objects::nonNull)
                    .filter(reportItem -> SIGNATURE_REPORT_NODE.equals(reportItem.getNodeName()))
                    .filter(reportItem -> reportItem.getNodeType() == Node.ELEMENT_NODE)
                    .map(Element.class::cast)
                    .forEach(signatureReportsElement -> {
                        Element el = (Element) signatureReportsElement.getElementsByTagName(SIGNER_CERTIFICATE_ELEMENT).item(0);
                        String subjectDN = el.getElementsByTagName(SUBJECT_DN_ELEMENT).item(0).getFirstChild().getNodeValue();
                        String commonName = getRdnValue(new X500Name(subjectDN), BCStyle.CN);

                        String issuerDN = el.getElementsByTagName(ISSUER_DN_ELEMENT).item(0).getFirstChild().getNodeValue();
                        String issuerName = getRdnValue(new X500Name(issuerDN), BCStyle.CN);

                        String beginDate = el.getElementsByTagName(NOT_BEFORE_ELEMENT).item(0).getFirstChild().getNodeValue();
                        String endDate = el.getElementsByTagName(NOT_AFTER_ELEMENT).item(0).getFirstChild().getNodeValue();

                        String identifier = signatureReportsElement.getElementsByTagName(IDENTIFIER_ELEMENT).item(0).getFirstChild().getNodeValue();
                        String claimedDate = signatureReportsElement.getElementsByTagName(CLAIMED_DATE_ELEMENT).item(0) != null ?
                                             signatureReportsElement.getElementsByTagName(CLAIMED_DATE_ELEMENT).item(0).getFirstChild().getNodeValue() :
                                             null;
                        String status = signatureReportsElement.getElementsByTagName(STATUS_ELEMENT).item(0).getFirstChild().getNodeValue();
                        Boolean isValid = switch (status) {
                            case "NO_ERROR_FOUND" -> true;
                            case "ERROR_FOUND" -> false;
                            default -> null;
                        };
                        String eidasLevel = signatureReportsElement.getElementsByTagName(EIDAS_LEVEL_ELEMENT).item(0).getFirstChild().getNodeValue();

                        signatureInfos.stream()
                                .filter(info -> {
                                    if (claimedDate == null) {
                                        return false;
                                    }
                                    if (info.getSignatureDateTime() == null) {
                                        info.setSignatureDateTime(Instant.parse(claimedDate).getEpochSecond() * 1000);
                                    }
                                    return info.getSignatureDateTime() == Instant.parse(claimedDate).getEpochSecond() * 1000;
                                })
                                .forEach(info -> {
                                    info.setIsSignatureValid(isValid);
                                    info.setPrincipalIssuer(commonName);
                                    info.setPrincipalSubjectIssuer(issuerName);
                                    info.setCertificateBeginDate(Instant.parse(beginDate).getEpochSecond() * 1000);
                                    info.setCertificateEndDate(Instant.parse(endDate).getEpochSecond() * 1000);
                                    info.setEidasLevel(eidasLevel);
                                    info.setError(errorsMap.getOrDefault(identifier, null));
                                });
                    });
        } else {
            throw new ExternalException("extractXmlInfo() xml report is empty");
        }

    }


    private static String getRdnValue(X500Name x500Name, ASN1ObjectIdentifier identifier) {
        RDN[] rdns = x500Name.getRDNs(identifier);
        return rdns.length > 0 ? rdns[0].getFirst().getValue().toString() : null;
    }


    private static @NotNull File getFile(Map<String, String> signatureReportContent) throws IOException {
        byte[] decodedBytes = Base64.getDecoder().decode(signatureReportContent.get(PDF));
        ByteArrayInputStream byteArray = new ByteArrayInputStream(decodedBytes);
        return FileUtils.createTempFile(byteArray, TMP_SIGNATURE_PROOF_PREFIX, TMP_SIGNATURE_PROOF_PDF_SUFFIX);
    }


    private List<String> getSignatureReportsId(Folder folder) {
        return folder.getSignatureProofList().stream()
                .filter(Predicate.not(Document::isMainDocument))
                .filter(d -> d.getName().startsWith(reportName))
                .map(d -> d.getMediaType().isCompatibleWith(APPLICATION_PDF) ? d.getId() : d.getPdfVisualId())
                .filter(StringUtils::isNotEmpty)
                .toList();
    }


    public void addSignatureProofAsAnnexe(@NotNull Folder folder) {
        ArrayList<Document> documentList = new ArrayList<>(folder.getDocumentList());

        for (SignatureProof proof : folder.getSignatureProofList()) {
            if (proof.getErrorMessage() == null) {
                documentList.add(proof);
            }
        }
        folder.setDocumentList(documentList);
    }


}
