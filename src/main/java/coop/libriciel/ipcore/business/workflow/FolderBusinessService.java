/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.business.workflow;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import coop.libriciel.ipcore.business.content.DocumentBusinessService;
import coop.libriciel.ipcore.business.typology.TypologyBusinessService;
import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.content.CreateFolderRequest;
import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.crypto.SignatureFormat;
import coop.libriciel.ipcore.model.database.*;
import coop.libriciel.ipcore.model.ipng.IpngFolderProofs;
import coop.libriciel.ipcore.model.ipng.IpngProofWrap;
import coop.libriciel.ipcore.model.pdfstamp.Stamp;
import coop.libriciel.ipcore.model.workflow.*;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.content.ContentServiceProperties;
import coop.libriciel.ipcore.services.crypto.CryptoServiceInterface;
import coop.libriciel.ipcore.services.database.MetadataRepository;
import coop.libriciel.ipcore.services.database.SubtypeLayerRepository;
import coop.libriciel.ipcore.services.groovy.GroovyResultCatcher;
import coop.libriciel.ipcore.services.groovy.GroovyService;
import coop.libriciel.ipcore.services.ipng.IpngServiceInterface;
import coop.libriciel.ipcore.services.pdfstamp.PdfStampServiceInterface;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.services.transformation.TransformationServiceInterface;
import coop.libriciel.ipcore.services.workflow.LegacyIdRepository;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.*;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import jakarta.servlet.http.HttpServletResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.util.StreamUtils;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;

import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static coop.libriciel.ipcore.business.content.DocumentBusinessService.TMP_PRINT_DOC_PREFIX;
import static coop.libriciel.ipcore.business.content.DocumentBusinessService.TMP_PRINT_DOC_SUFFIX;
import static coop.libriciel.ipcore.controller.DocumentController.MAX_DOCUMENTS;
import static coop.libriciel.ipcore.model.crypto.SignatureFormat.AUTO;
import static coop.libriciel.ipcore.model.database.InternalMetadata.Constants.*;
import static coop.libriciel.ipcore.model.database.SubtypeLayerAssociation.*;
import static coop.libriciel.ipcore.model.database.TemplateType.DOCKET;
import static coop.libriciel.ipcore.model.pdfstamp.StampType.METADATA;
import static coop.libriciel.ipcore.model.workflow.Action.*;
import static coop.libriciel.ipcore.services.content.AlfrescoService.PREMIS_NODE_FILE_NAME;
import static coop.libriciel.ipcore.services.content.ContentServiceInterface.FILE_TRANSFER_BUFFER_SIZE;
import static coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface.*;
import static coop.libriciel.ipcore.utils.ApiUtils.MAX_DOCUMENT_SIZE;
import static coop.libriciel.ipcore.utils.ApiUtils.MAX_FOLDERS_COUNT;
import static coop.libriciel.ipcore.utils.CollectionUtils.stream;
import static coop.libriciel.ipcore.utils.CollectionUtils.toMutableList;
import static coop.libriciel.ipcore.utils.FileUtils.safeCreateTempFile;
import static coop.libriciel.ipcore.utils.FolderUtils.CREATION_WORKFLOW_INDEX;
import static coop.libriciel.ipcore.utils.FolderUtils.VALIDATION_WORKFLOW_SMALLEST_INDEX;
import static coop.libriciel.ipcore.utils.PaginatedList.MAX_PAGE_SIZE;
import static coop.libriciel.ipcore.utils.TextUtils.*;
import static java.lang.System.currentTimeMillis;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Arrays.asList;
import static java.util.Collections.*;
import static java.util.Comparator.*;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toMap;
import static javax.xml.XMLConstants.*;
import static org.apache.commons.io.FilenameUtils.removeExtension;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.springframework.core.io.buffer.DataBufferUtils.readInputStream;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM_VALUE;
import static org.springframework.http.MediaType.APPLICATION_PDF;
import static org.springframework.util.ResourceUtils.CLASSPATH_URL_PREFIX;


@Log4j2
@Service
public class FolderBusinessService {

    public static final String TMP_IPNG_PROOFS_PREFIX = "ip_ipng_proofs_";
    public static final String TMP_IPNG_PROOFS_SUFFIX = ".json";

    private static final String INTERNAL_FOLDER_FIELD = INTERNAL_PREFIX + "folder";
    private static final String INTERNAL_RESOURCE_BUNDLE_FIELD = INTERNAL_PREFIX + "resource_bundle";
    private static final String INTERNAL_RESOURCE_CRYPTO_UTILS_FIELD = INTERNAL_PREFIX + "crypto_utils";
    private static final String INTERNAL_RESOURCE_TEXT_UTILS_FIELD = INTERNAL_PREFIX + "text_utils";


    @Value(CLASSPATH_URL_PREFIX + "templates/mail/docket.ftl")
    private @Getter Resource defaultDocketResource;

    private @Setter ConverterProperties htmlToPdfConverterProperties;

    @Value(CLASSPATH_URL_PREFIX + "premis3.0.xsd")
    private Resource premisXsd;


    // <editor-fold desc="Beans">


    private final AuthServiceInterface authService;
    private final Configuration configuration;
    private final ContentServiceInterface contentService;
    private final ContentServiceProperties contentServiceProperties;
    private final CryptoServiceInterface cryptoService;
    private final DocumentBusinessService documentBusinessService;
    private final GroovyService groovyService;
    private final IpngServiceInterface ipngService;
    private final LegacyIdRepository legacyIdRepository;
    private final MetadataRepository metadataRepository;
    private final PdfStampServiceInterface pdfStampService;
    private final PermissionServiceInterface permissionService;
    private final StatsServiceInterface statsService;
    private final SubtypeLayerRepository subtypeLayerRepository;
    private final TransformationServiceInterface transformationService;
    private final TypologyBusinessService typologyBusinessService;
    private final WorkflowBusinessService workflowBusinessService;
    private final WorkflowServiceInterface workflowService;


    @Autowired
    public FolderBusinessService(AuthServiceInterface authService,
                                 Configuration configuration,
                                 ContentServiceInterface contentService,
                                 ContentServiceProperties contentServiceProperties,
                                 CryptoServiceInterface cryptoService,
                                 DocumentBusinessService documentBusinessService,
                                 GroovyService groovyService,
                                 IpngServiceInterface ipngService,
                                 LegacyIdRepository legacyIdRepository,
                                 MetadataRepository metadataRepository,
                                 PdfStampServiceInterface pdfStampService,
                                 PermissionServiceInterface permissionService,
                                 StatsServiceInterface statsService,
                                 SubtypeLayerRepository subtypeLayerRepository,
                                 TransformationServiceInterface transformationService,
                                 TypologyBusinessService typologyBusinessService,
                                 WorkflowBusinessService workflowBusinessService,
                                 WorkflowServiceInterface workflowService) {
        this.authService = authService;
        this.configuration = configuration;
        this.contentService = contentService;
        this.contentServiceProperties = contentServiceProperties;
        this.cryptoService = cryptoService;
        this.documentBusinessService = documentBusinessService;
        this.groovyService = groovyService;
        this.ipngService = ipngService;
        this.legacyIdRepository = legacyIdRepository;
        this.metadataRepository = metadataRepository;
        this.pdfStampService = pdfStampService;
        this.permissionService = permissionService;
        this.statsService = statsService;
        this.subtypeLayerRepository = subtypeLayerRepository;
        this.transformationService = transformationService;
        this.typologyBusinessService = typologyBusinessService;
        this.workflowBusinessService = workflowBusinessService;
        this.workflowService = workflowService;

        htmlToPdfConverterProperties = new ConverterProperties();
        htmlToPdfConverterProperties.setBaseUri("classpath:/");
    }


    // </editor-fold desc="Beans">


    public Folder createDraftFolder(@NotNull Tenant tenant,
                                    @NotNull Desk originDesk,
                                    @NotNull List<MultipartFile> mainFiles,
                                    @Nullable List<MultipartFile> annexeFiles,
                                    @Nullable List<MultipartFile> detachedSignatures,
                                    @NotNull CreateFolderRequest createFolderRequest) {

        log.debug("createDraftFolder deskId:{} name:{} typeId:{} subtypeId:{}",
                originDesk.getId(), createFolderRequest.getName(), createFolderRequest.getTypeId(), createFolderRequest.getSubtypeId());

        // Permission check

        boolean deskHasCreationRights = permissionService.currentUserHasFolderCreationRightOnSomeDeskIn(tenant.getId(), Set.of(originDesk.getId()));
        if (!deskHasCreationRights) {
            log.info("createDraftFolder - desk {} does not have folder creation right", originDesk.getId());
            throw new LocalizedStatusException(FORBIDDEN, "message.you_can_t_create_folder_on_this_desk");
        }

        // Basic checks

        if (CollectionUtils.isEmpty(mainFiles)) {
            log.info("createDraftFolder - missing main file, abort");
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.cannot_create_folder_without_main_document");
        }

        String folderName = firstNotEmpty(createFolderRequest.getName(), removeExtension(mainFiles.get(0).getOriginalFilename()));
        folderName = TextUtils.substituteForbiddenChar(folderName);

        Map<Integer, String> variableDesksIdsMap = new HashMap<>();
        if (Objects.nonNull(createFolderRequest.getVariableDesksIds())) {
            createFolderRequest.getVariableDesksIds().forEach((key, value) -> variableDesksIdsMap.put(Integer.parseInt(key), value));
        }

        Folder result = workflowBusinessService.prepareDraftFromParams(tenant, originDesk, createFolderRequest, folderName, variableDesksIdsMap);

        if ((CollectionUtils.size(mainFiles) + CollectionUtils.size(annexeFiles)) >= MAX_DOCUMENTS) {
            log.info("createDraftFolder - too many documents, abort");
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE, "message.documents_count_reached_the_limit_of_n", MAX_DOCUMENTS);
        }

        int maxMainDocuments = ofNullable(result.getSubtype())
                .filter(Subtype::isMultiDocuments)
                .map(s -> contentServiceProperties.getMaxMainFiles())
                .orElse(1);

        if (CollectionUtils.size(mainFiles) > maxMainDocuments) {
            log.info("createDraftFolder - too many main documents, abort");
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE, "message.main_documents_count_reached_the_limit_of_n", maxMainDocuments);
        }

        if (result.getType() != null) {

            List<Document> dummyMainFiles = IntStream
                    .range(0, CollectionUtils.size(mainFiles))
                    .mapToObj(i -> new Document(mainFiles.get(i), true, i))
                    .toList();

            List<Document> dummyAnnexes = ofNullable(annexeFiles)
                    .filter(CollectionUtils::isNotEmpty)
                    .map(a -> IntStream.range(0, CollectionUtils.size(a))
                            .mapToObj(i -> new Document(annexeFiles.get(i), false, i))
                            .toList()
                    )
                    .orElse(emptyList());

            result.setDocumentList(new ArrayList<>());
            result.getDocumentList().addAll(dummyMainFiles);
            result.getDocumentList().addAll(dummyAnnexes);

            if (!result.getType().getSignatureFormat().equals(AUTO)) {
                log.info("createDraftFolder - checking signature format...");
                CryptoUtils.checkSignatureFormat(result.getType().getSignatureFormat(), result.getDocumentList(), detachedSignatures);
                log.info("createDraftFolder - signature format OK");
            }
        }

        Stream.concat(mainFiles.stream(), stream(annexeFiles))
                .filter(document -> document.getSize() > MAX_DOCUMENT_SIZE)
                .findAny()
                .ifPresent(document -> {
                    log.error("Error at draft creation, at least one document is above the maximum size, abort.");
                    throw new LocalizedStatusException(INSUFFICIENT_STORAGE, "message.some_document_exceeds_size_limit", MAX_DOCUMENT_SIZE / (1024 * 1024));
                });

        // Creating Folder

        String contentId = contentService.createFolder(tenant);
        result.setContentId(contentId);

        try {

            contentService.storeMultipartList(tenant.getId(), result, mainFiles, true, null, transformationService::toPdf);
            contentService.storeMultipartList(tenant.getId(), result, annexeFiles, false, null, transformationService::toPdf);

            if (MapUtils.isNotEmpty(createFolderRequest.getDetachedSignaturesMapping())) {

                // TODO user createDocument to get documents ids.
                List<Document> documents = contentService.getDocumentList(contentId);

                createFolderRequest
                        .getDetachedSignaturesMapping()
                        .forEach((documentName, detachedSignatureNames) -> {

                            Document document = documents.stream()
                                    .filter(d -> d.getName().equals(documentName))
                                    .findFirst()
                                    .orElseThrow(() -> new LocalizedStatusException(BAD_REQUEST, "message.document_mapping_error"));

                            List<MultipartFile> detachedSignaturesToCreate = getDetachedSignatureListForDocument(detachedSignatureNames, detachedSignatures);

                            detachedSignaturesToCreate.forEach(detachedSignatureToCreate -> createDetachedSignatureForDocument(
                                    contentId,
                                    document,
                                    detachedSignatureToCreate));
                        });
            }

            // Upload to Flowable

            log.info("type:{} subtype:{} creationW:{} validationW:{}", result.getType(), result.getSubtype(),
                    result.getCreationWorkflowDefinitionKey(), result.getValidationWorkflowDefinitionKey());

            String createdFolderId = workflowService
                    .createDraftWorkflow(tenant.getId(), result, variableDesksIdsMap)
                    .getId();


            if (createFolderRequest.getLegacyId() != null) {
                log.debug("createDraftFolder - request has a 'legacyId' :{}", createFolderRequest.getLegacyId());
                Optional<LegacyIdRecord> existingLegacyIdRecordOpt = legacyIdRepository.findById(createFolderRequest.getLegacyId());
                if (existingLegacyIdRecordOpt.isPresent()) {
                    log.error("CreateFolder folder legacy id:{} already exists, abort creation", createFolderRequest.getLegacyId());
                    throw new LocalizedStatusException(CONFLICT, "message.legacy_id_already_exists");
                }
                
                LegacyIdRecord legacyIdRecord = new LegacyIdRecord(createFolderRequest.getLegacyId(), createdFolderId);
                legacyIdRepository.save(legacyIdRecord);
            }

            result.setId(createdFolderId);

        } catch (Exception e) {
            // 'Compensation' mechanism
            log.error(
                    "An error occurred at draft creation while storing files or instantiating workflow, the incomplete content ({}) will be deleted. Error: {}",
                    contentId,
                    e.getMessage());
            contentService.deleteFolder(result);
            throw e;
        }

        // Sending back result

        log.debug("CreateFolder folder:{}", result);
        statsService.registerFolderAction(tenant, CREATE, result, originDesk, null);

        result.getMetadata().entrySet()
                .removeIf(entry -> StringUtils.startsWith(entry.getKey(), INTERNAL_PREFIX));

        return result;
    }


    private List<MultipartFile> getDetachedSignatureListForDocument(List<String> detachedSignatureNames, List<MultipartFile> detachedSignatures) {
        checkDetachedSignatureNames(detachedSignatureNames, detachedSignatures);

        return detachedSignatures.stream()
                .filter(detachedSignature -> detachedSignature.getOriginalFilename() != null
                        && detachedSignatureNames.contains(detachedSignature.getOriginalFilename()))
                .toList();
    }


    private void checkDetachedSignatureNames(List<String> detachedSignatureNames, List<MultipartFile> detachedSignatures) {
        Map<String, MultipartFile> multipartFileNames = detachedSignatures
                .stream()
                .collect(toMap(MultipartFile::getOriginalFilename, m -> m));

        if (detachedSignatureNames.stream().anyMatch(name -> !multipartFileNames.containsKey(name))) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.detached_signature_mapping_error");
        }
    }


    private void createDetachedSignatureForDocument(String folderContentId, Document document, MultipartFile detachedSignature) {
        DocumentBuffer documentBuffer = new DocumentBuffer(detachedSignature, false, -1);

        documentBuffer.setContentFlux(readInputStream(detachedSignature::getInputStream, new DefaultDataBufferFactory(), FILE_TRANSFER_BUFFER_SIZE));

        contentService.createDetachedSignature(folderContentId, document, documentBuffer, null);
    }


    public void downloadFolderAsZip(HttpServletResponse response, Tenant tenant, Folder folder, String premisContent) {

        response.setContentType(APPLICATION_OCTET_STREAM_VALUE);

        String safeFolderName = FileUtils.sanitizeFileName(folder.getName());
        response.setHeader(CONTENT_DISPOSITION, String.format("attachment;filename=%s_%s.zip", safeFolderName, folder.getContentId()));

        // To uncomment if we want to use the full print doc
        //   folderWithHistory.setDocumentList(folder.getDocumentList());
        //   Set<String> annexeIds = folder.getDocumentList().stream()
        //        .filter(doc -> !doc.isMainDocument())
        //        .map(Document::getId)
        //        .collect(Collectors.toSet());

        folder.getMetadata().remove("action", "i_Parapheur_internal_archive");
        Path printDocTempFile = generatePrintDocAsTempFile(tenant, folder, true, emptySet());

        // Zip everything up

        Set<String> existingFileNames = new HashSet<>();

        try (ZipOutputStream zippedOut = new ZipOutputStream(response.getOutputStream());
             InputStream premisInputStreamAutoCloseableInput = new ByteArrayInputStream(premisContent.getBytes(UTF_8))) {

            folder.getDocumentList()
                    .forEach(document -> {
                        log.debug("getArchiveAsZip documentName:{} nodeId:{}", document.getName(), document.getId());

                        String safeDocumentName = Optional.of(document.getName())
                                .map(FileUtils::sanitizeFileName)
                                .map(name -> FileUtils.enumerateOnDuplicates(name, existingFileNames))
                                .get();

                        existingFileNames.add(safeDocumentName);
                        String innerSubFolder = document.isMainDocument() ? "Documents principaux/" : "Annexes/";
                        String innerFolderPath = String.format("%s%s", innerSubFolder, safeDocumentName);
                        addZipEntry(zippedOut, innerFolderPath, document.getId());
                    });

            folder.getDocumentList().stream()
                    .map(Document::getDetachedSignatures)
                    .flatMap(Collection::stream)
                    .forEach(detachedSignature -> {
                        log.debug("getArchiveAsZip detachedSignature:{} nodeId:{}", detachedSignature.getName(), detachedSignature.getId());

                        String safeDetachedSignatureName = Optional.of(detachedSignature.getName())
                                .map(FileUtils::sanitizeFileName)
                                .map(name -> FileUtils.enumerateOnDuplicates(name, existingFileNames))
                                .get();

                        existingFileNames.add(safeDetachedSignatureName);
                        String innerFolderPath = String.format("%s%s", "Documents principaux/", safeDetachedSignatureName);
                        addZipEntry(zippedOut, innerFolderPath, detachedSignature.getId());
                    });

            // Premis file

            ZipEntry premisEntry = new ZipEntry(PREMIS_NODE_FILE_NAME);
            premisEntry.setTime(currentTimeMillis());
            zippedOut.putNextEntry(premisEntry);
            StreamUtils.copy(premisInputStreamAutoCloseableInput, zippedOut);
            zippedOut.closeEntry();

            // Print doc

            ResourceBundle messageResourceBundle = ResourceBundle.getBundle(MESSAGE_BUNDLE, Locale.getDefault());
            // to replace by 'printable_pdf_name' if in the end we want the full print doc
            String printDocFileNameTemplate = messageResourceBundle.getString("message.pdf_docket_name");
            addZipEntry(zippedOut, premisEntry, printDocTempFile, printDocFileNameTemplate, safeFolderName);

            // IPNG proofs

            if (isNotEmpty(folder.getMetadata().get(META_IPNG_PROOF_SENT_ID))) {
                Path ipngProofTempFile = createIpngProofFileForFolder(folder);
                String ipngProofFileNameTemplate = messageResourceBundle.getString("message.ipng_proof_name");
                addZipEntry(zippedOut, premisEntry, ipngProofTempFile, ipngProofFileNameTemplate, safeFolderName);
            }

            zippedOut.finish();

        } catch (IOException e) {
            log.error("Error while building folder zip : {}", e.getMessage());
            log.debug("Error details : ", e);
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_building_zip_file");
        }
    }


    private void addZipEntry(ZipOutputStream zippedOut, String zipEntryName, String documentId) {
        ZipEntry entry = new ZipEntry(zipEntryName);
        entry.setTime(currentTimeMillis());
        try (InputStream documentinputStream = contentService.retrievePipedDocument(documentId)) {
            zippedOut.putNextEntry(entry);
            StreamUtils.copy(documentinputStream, zippedOut);
            zippedOut.closeEntry();
        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_building_zip_file");
        }
    }


    private void addZipEntry(ZipOutputStream zippedOut, ZipEntry entry, Path path, String template, String folderName) {
        String printDocFileName = MessageFormat.format(template, folderName);

        ZipEntry printDocEntry = new ZipEntry(printDocFileName);
        entry.setTime(currentTimeMillis());

        try {
            zippedOut.putNextEntry(printDocEntry);
            InputStream printDocInputStream = Files.newInputStream(path);
            StreamUtils.copy(printDocInputStream, zippedOut);
            printDocInputStream.close();
            zippedOut.closeEntry();
        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_building_zip_file");
        }
    }


    public Folder updateFolder(@NotNull Tenant tenant,
                               @NotNull Folder existingFolder,
                               @NotNull Folder updateRequest) {

        // Refresh values

        typologyBusinessService.updateTypology(tenant.getId(), singletonList(existingFolder), false);
        typologyBusinessService.updateTypology(tenant.getId(), singletonList(updateRequest), true);
        updateRequest.setOriginDesk(existingFolder.getOriginDesk());
        contentService.populateFolderWithAllDocumentTypes(existingFolder);

        // Integrity check

        if (existingFolder.getType() != null) {
            SignatureFormat oldSignatureFormat = cryptoService.getSignatureFormat(existingFolder.getType(), existingFolder.getDocumentList());
            existingFolder.getType().setSignatureFormat(oldSignatureFormat);
        }

        SignatureFormat newSignatureFormat = cryptoService.getSignatureFormat(updateRequest.getType(), existingFolder.getDocumentList());
        updateRequest.getType().setSignatureFormat(newSignatureFormat);

        CryptoUtils.checkSignatureFormat(newSignatureFormat, existingFolder.getDocumentList(), null);

        if (existingFolder.getType() != null) {
            typologyBusinessService.checkTypeReplacementCompatibility(existingFolder.getType(), updateRequest.getType());
        }

        typologyBusinessService.checkSubtypeReplacementCompatibility(updateRequest.getSubtype(), existingFolder.getDocumentList());

        // Compute diffs

        updateRequest.setName(TextUtils.substituteForbiddenChar(updateRequest.getName()));

        DeskRepresentation finalDesk = existingFolder.getFinalDesk();
        String creationWorkflowDefinitionKey = existingFolder.getCreationWorkflowDefinitionKey();
        String validationWorkflowDefinitionKey = existingFolder.getValidationWorkflowDefinitionKey();

        // We should allow the replacement of an existing value with a null one, here.
        // The Optional is not a good fit, since it will fallback on the first non-null value
        if (updateRequest.getSubtype() != null) {
            creationWorkflowDefinitionKey = updateRequest.getSubtype().getCreationWorkflowId();
            validationWorkflowDefinitionKey = updateRequest.getSubtype().getValidationWorkflowId();
        }

        String requestOriginDeskId = updateRequest.getOriginDesk().getId();
        WorkflowDefinition newWorkflowDefinition = null;

        boolean hasASelectionScript = Optional.ofNullable(updateRequest.getSubtype())
                .map(Subtype::getWorkflowSelectionScript)
                .map(StringUtils::isNotEmpty)
                .orElse(false);

        boolean hasEveryDraftAndCreationStepHasBeenCompleted = existingFolder.getStepList().stream()
                .filter(t -> t.getWorkflowIndex() < VALIDATION_WORKFLOW_SMALLEST_INDEX)
                .allMatch(t -> t.getDate() != null);

        boolean shouldExecuteTheSelectionScript = !hasEveryDraftAndCreationStepHasBeenCompleted && hasASelectionScript;

        if (shouldExecuteTheSelectionScript) {
            // TODO all this should probably go into a dedicated method on workflowbusinessService to be shared
            GroovyResultCatcher scriptResult = groovyService
                    .computeSelectionScriptForFolder(tenant.getId(), updateRequest)
                    .orElseThrow(() -> new LocalizedStatusException(UNPROCESSABLE_ENTITY, "message.no_result_from_workflow_selection_script"));

            newWorkflowDefinition = workflowService
                    .getWorkflowDefinitionByKey(tenant.getId(), scriptResult.getWorkflowDefinitionKey())
                    .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_workflow_definition_key"));

            Map<Integer, String> variableDeskIds = new HashMap<>();
            groovyService.updateVariableDeskMapFromScriptResult(tenant.getId(), scriptResult, newWorkflowDefinition, variableDeskIds);

            Map<String, String> placeholderDesksMetadata = workflowService.computePlaceholderDesksConcreteValues(
                    newWorkflowDefinition,
                    requestOriginDeskId,
                    variableDeskIds
            );

            updateRequest.getMetadata().putAll(placeholderDesksMetadata);
            updateRequest.setFinalDesk(Optional.ofNullable(newWorkflowDefinition.getFinalDesk()).orElse(updateRequest.getOriginDesk()));
            workflowService.mapIndexedPlaceholdersToActualDesk(updateRequest.getFinalDesk(), requestOriginDeskId, placeholderDesksMetadata);

        } else if (!hasEveryDraftAndCreationStepHasBeenCompleted) {
            newWorkflowDefinition = workflowService
                    .getWorkflowDefinitionByKeyAndMapPlaceholders(tenant.getId(), validationWorkflowDefinitionKey, requestOriginDeskId)
                    .orElseThrow(() -> new LocalizedStatusException(UNPROCESSABLE_ENTITY, "message.unknown_workflow_definition_key"));
        }

        if (newWorkflowDefinition != null) {
            validationWorkflowDefinitionKey = newWorkflowDefinition.getKey();
            finalDesk = newWorkflowDefinition.getFinalDesk();
        }

        updateRequest.setFinalDesk(finalDesk);
        updateRequest.setCreationWorkflowDefinitionKey(creationWorkflowDefinitionKey);
        updateRequest.setValidationWorkflowDefinitionKey(validationWorkflowDefinitionKey);

        workflowService.editFolder(tenant.getId(), existingFolder.getId(), updateRequest);
        return updateRequest;
    }


    public void deleteFolder(@NotNull String tenantId, @NotNull Folder folder, boolean fromTrashbin) {
        log.info("deleteFolder: {}", folder.getId());
        try {
            workflowBusinessService.cleanupAllSecureMailData(tenantId, folder);
        } catch (Exception e) {
            log.error("An error occurred while trying to cancel external tasks upon folder deletion: {}", e.getMessage());
            log.debug("error detail : ", e);
        }

        if (folder.getLegacyId() != null) {
            try {
                legacyIdRepository.deleteById(folder.getLegacyId());
            } catch (Exception e) {
                log.error("An error occurred while trying to cleanup legacyId table upon folder deletion: {}", e.getMessage());
                log.debug("error detail : ", e);
            }
        }

        try {
            contentService.deleteFolder(folder);
        } catch (Exception e) {
            log.error("An error occurred while trying to delete the folder on the content service: {}", e.getMessage());
            log.debug("error detail : ", e);
        }

        Optional.ofNullable(folder.getId())
                .ifPresent(id -> workflowService.deleteInstance(id, fromTrashbin));
    }


    public void checkCreateFolderLimitations(@NotNull Tenant tenant) {

        int limit = tenant.getFolderLimit() != null ? tenant.getFolderLimit() : MAX_FOLDERS_COUNT;
        Pageable pageable = PageRequest.of(0, 1);
        Page<? extends Folder> folderList = workflowService.listFolders(tenant.getId(), pageable, null, null, null, null, null);
        long foldersCount = Optional.of(folderList)
                .map(Page::getTotalElements)
                .orElse(0L);

        if (foldersCount >= limit) {
            throw new LocalizedStatusException(INSUFFICIENT_STORAGE, "message.already_n_folders_maximum_reached", foldersCount);
        }
    }


    public void prepareChecksumDataForPremisFile(List<Document> documents) {
        documents
                .stream()
                .filter(d -> StringUtils.isEmpty(d.getChecksumValue()))
                // Retrieving the full document checksum
                .forEach(d -> {
                    try (InputStream ios = contentService.retrievePipedDocument(d.getId())) {
                        d.setChecksumValue(DigestUtils.sha256Hex(ios).toUpperCase());
                    } catch (IOException e) {
                        throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_computing_document_checksum");
                    }
                });
    }


    public String generatePremisContent(Folder folder) {

        try {
            // TODO: Stream this, instead of putting it in memory.
            // Low priority, though. Since it should never be a very large String.
            // We should manage to get the HTTP outputStream from the WebClient, and write in it directly.
            String premis = XmlUtils.folderToPremisXml(folder, contentService::retrievePipedDocument);
            premis = XmlUtils.prettyPrint(premis);

            // Validation

            SchemaFactory schemaFactory = SchemaFactory.newInstance(W3C_XML_SCHEMA_NS_URI);
            schemaFactory.setProperty(ACCESS_EXTERNAL_DTD, EMPTY);
            schemaFactory.setProperty(ACCESS_EXTERNAL_SCHEMA, EMPTY);

            try (InputStream premisXsdInputStream = premisXsd.getInputStream();
                 StringReader reader = new StringReader(premis)) {

                javax.xml.validation.Schema schema = schemaFactory.newSchema(new StreamSource(premisXsdInputStream));
                Validator validator = schema.newValidator();
                validator.validate(new StreamSource(reader));
            }

            return premis;

        } catch (IOException | TransformerException | SAXException e) {
            log.warn("Error generating Premis file", e);
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_generating_premis_file");
        }
    }


    public @NotNull Path generatePrintDocAsTempFile(@NotNull Tenant tenant, Folder folder, boolean includeDocket, Set<String> finalAnnexesIds) {

        boolean signatureHasOccurred = folder.getStepList()
                .stream()
                .map(Task::getPerformedAction)
                .anyMatch(action -> action == SIGNATURE || action == EXTERNAL_SIGNATURE);
        // Retrieving target document Ids

        List<String> printableMainDocIds = folder.getDocumentList().stream()
                .filter(Document::isMainDocument)
                .sorted(comparing(Document::getIndex, nullsLast(naturalOrder())))
                .map(d -> d.getMediaType().isCompatibleWith(APPLICATION_PDF) ? d.getId() : d.getPdfVisualId())
                .filter(StringUtils::isNotEmpty)
                .toList();

        List<String> printableAnnexesDocIds = folder.getDocumentList().stream()
                .filter(d -> !d.isMainDocument())
                .filter(d -> finalAnnexesIds.contains(d.getId()))
                .sorted(comparing(Document::getIndex, nullsLast(naturalOrder())))
                .map(d -> d.getMediaType().isCompatibleWith(APPLICATION_PDF) ? d.getId() : d.getPdfVisualId())
                .filter(StringUtils::isNotEmpty)
                .toList();

        // Stamp retrieving

        List<SubtypeLayer> layers = subtypeLayerRepository
                .findBySubtype_Id(folder.getSubtype().getId(), PageRequest.of(0, MAX_PAGE_SIZE))
                .getContent();

        List<Stamp> mainDocStampList = layers.stream()
                .filter(sl -> asList(ALL, MAIN_DOCUMENT).contains(sl.getAssociation()))
                .flatMap(sl -> sl.getLayer().getStampList().stream())
                .filter(stamp -> signatureHasOccurred || !stamp.isAfterSignature())
                .toList();

        List<Stamp> annexeStampList = layers.stream()
                .filter(sl -> asList(ALL, ANNEXE).contains(sl.getAssociation()))
                .flatMap(sl -> sl.getLayer().getStampList().stream())
                .filter(stamp -> signatureHasOccurred || !stamp.isAfterSignature())
                .toList();

        Stream.concat(mainDocStampList.stream(), annexeStampList.stream())
                .forEach(stamp -> computeStampValue(tenant.getId(), folder, stamp));

        // Building result

        List<File> tempFilesToMerge = Stream.concat(printableMainDocIds.stream(), printableAnnexesDocIds.stream())
                .map(id -> {
                    DocumentBuffer docBuff = contentService.retrieveContent(id);
                    if (printableMainDocIds.contains(id)) {docBuff.setMainDocument(true);}
                    return docBuff;
                })
                .map(document -> {
                    if (document.isMainDocument() && CollectionUtils.isNotEmpty(mainDocStampList)) {
                        return pdfStampService.createStamp(document, mainDocStampList, contentService::retrieveContent);
                    } else if (!document.isMainDocument() && CollectionUtils.isNotEmpty(annexeStampList)) {
                        return pdfStampService.createStamp(document, annexeStampList, contentService::retrieveContent);
                    } else {
                        return document;
                    }
                })
                .map(DocumentBusinessService::safeContentServiceBufferToInputStream)
                .map(is -> safeCreateTempFile(is, TMP_PRINT_DOC_PREFIX, TMP_PRINT_DOC_SUFFIX))
                .collect(toMutableList());

        if (includeDocket) {
            ByteArrayInputStream docketPdfInputStream = generateDocketFromFolder(tenant, folder);
            File tempDocketFile = safeCreateTempFile(docketPdfInputStream, TMP_PRINT_DOC_PREFIX, TMP_PRINT_DOC_SUFFIX);
            tempFilesToMerge.add(0, tempDocketFile);
        }

        try {
            return documentBusinessService.mergeStreamToPdf(tempFilesToMerge).toPath();
        } catch (IOException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.error_merging_pdf", e);
        }
    }


    public @NotNull ByteArrayInputStream generateDocketFromFolder(Tenant tenant, Folder folder) {

        authService.updateFolderReferencedDeskNames(singletonList(folder));
        authService.updateFolderReferencedUserNames(singletonList(folder));
        typologyBusinessService.updateTypology(tenant.getId(), singletonList(folder), false);
        folder.getStepList().removeIf(task -> Action.isSecondary(task.getAction()));

        // FIXME: Those 2 should have been cleaned earlier
        folder.getMetadata().entrySet().removeIf(entry -> StringUtils.startsWith(entry.getKey(), INTERNAL_PREFIX));
        folder.getMetadata().entrySet().removeIf(entry -> StringUtils.startsWith(entry.getKey(), "workflow_internal_"));

        try (InputStream docketTemplateInputStream = tenant.getCustomTemplates().containsKey(DOCKET)
                                                     ? RequestUtils.bufferToInputStream(contentService.getCustomTemplate(tenant, DOCKET))
                                                     : defaultDocketResource.getInputStream()) {
            return generateDocketPdfInputStream(folder, docketTemplateInputStream);
        } catch (IOException exception) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, exception, "message.error_creating_docket");
        }
    }


    private void computeStampValue(String tenantId, Folder folder, Stamp stamp) {
        log.debug("computeStampValue type:{}", stamp.getType());

        // Default cases

        if (stamp.getComputedValue() != null) {
            return;
        }

        if (stamp.getType() != METADATA) {
            return;
        }

        // Compute the internal metadata values

        String metadataId = stamp.getValue();
        log.debug("metadataId:{}", metadataId);

        Metadata metadataDefinition = metadataRepository
                .findByIdAndTenant_IdOrTenantLess(metadataId, tenantId)
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.unknown_metadata_id"));

        Optional<Task> lastVisa = Optional.ofNullable(
                FolderUtils.getLastPerformedTask(folder, Set.of(START, VISA, SEAL, SECOND_OPINION, SIGNATURE, EXTERNAL_SIGNATURE))
        );
        Optional<Task> lastSignature = Optional.ofNullable(FolderUtils.getLastPerformedTask(folder, Set.of(SIGNATURE, EXTERNAL_SIGNATURE)));
        Optional<Folder> optionalFolder = Optional.ofNullable(folder);
        Optional<String> computedValue = switch (metadataDefinition.getKey()) {
            case FOLDER_NAME_KEY -> optionalFolder.map(Folder::getName);
            case FOLDER_TYPE_KEY -> optionalFolder.map(Folder::getType).map(Type::getName);
            case FOLDER_SUBTYPE_KEY -> optionalFolder.map(Folder::getSubtype).map(Subtype::getName);
            case LAST_VISA_DESK_KEY -> lastVisa.map(Task::getDesks).orElse(emptyList()).stream().findFirst().map(DeskRepresentation::getName);
            case LAST_VISA_USER_KEY -> lastVisa.map(Task::getUser).map(UserUtils::prettyPrint);
            case LAST_VISA_DATE_KEY -> lastVisa.map(Task::getDate).map(TextUtils::prettyPrintDateTime);
            case LAST_SIGNATURE_DESK_KEY -> lastSignature.map(Task::getDesks).orElse(emptyList()).stream().findFirst().map(DeskRepresentation::getName);
            case LAST_SIGNATURE_USER_KEY -> lastSignature.map(Task::getUser).map(UserUtils::prettyPrint);
            case LAST_SIGNATURE_DATE_KEY -> lastSignature.map(Task::getDate).map(TextUtils::prettyPrintDateTime);
            case LAST_SIGNATURE_DELEGATED_BY_KEY -> lastSignature.map(Task::getDelegatedByDesk).map(DeskRepresentation::getName);
            case LAST_SIGNATURE_HASH_KEY -> Optional.of(EMPTY); // FIXME
            default -> optionalFolder.map(Folder::getMetadata).map(metaList -> metaList.get(metadataDefinition.getKey()));
        };

        stamp.setComputedValue(computedValue.orElse(EMPTY));
    }


    public ByteArrayInputStream generateDocketPdfInputStream(@NotNull Folder folder, @NotNull InputStream docketResourceInputStream) {

        Map<String, Object> fields = new HashMap<>();

        // Filter creation workflow steps
        List<Task> filteredTaskList = folder
                .getStepList()
                .stream()
                .filter(task -> task.getWorkflowIndex() != null)
                // This is the feature: we only want the validation workflow...
                // And the Start one too, because it does exists in v4.
                .filter(task -> task.getWorkflowIndex() != CREATION_WORKFLOW_INDEX)
                .toList();

        folder.setStepList(filteredTaskList);
        fields.put(INTERNAL_FOLDER_FIELD, folder);
        fields.put(INTERNAL_RESOURCE_BUNDLE_FIELD, ResourceBundle.getBundle(MESSAGE_BUNDLE, Locale.getDefault()));
        // We can only pass objects in here. Not classes.
        // Either we create a lambda for every method, or we instantiate the class...
        // The second one makes less code.
        //noinspection InstantiationOfUtilityClass
        fields.put(INTERNAL_RESOURCE_CRYPTO_UTILS_FIELD, new CryptoUtils());
        //noinspection InstantiationOfUtilityClass
        fields.put(INTERNAL_RESOURCE_TEXT_UTILS_FIELD, new TextUtils());

        try (ByteArrayOutputStream fos = new ByteArrayOutputStream();
             InputStreamReader reader = new InputStreamReader(docketResourceInputStream)) {

            Template templateBody = new Template("Docket template", reader, configuration);
            String docket = FreeMarkerTemplateUtils.processTemplateIntoString(templateBody, fields);
            HtmlConverter.convertToPdf(docket, fos, htmlToPdfConverterProperties);
            return new ByteArrayInputStream(fos.toByteArray());

        } catch (IOException | TemplateException e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, e, "message.error_creating_docket");
        }
    }


    @NotNull
    public List<IpngFolderProofs> gatherIpngProofsForFolder(Folder folder) {
        Map<String, String> metaMap = folder.getMetadata();
        if (StringUtils.isEmpty(metaMap.get(META_IPNG_PROOF_SENT_ID))) {
            throw new LocalizedStatusException(NOT_FOUND, "message.no_ipng_proof_on_folder");
        }

        // TODO handle multiple IPNG tasks - we don't handle with the current metadata system
        List<IpngFolderProofs> result = new ArrayList<>();

        IpngFolderProofs proofs = IpngFolderProofs.builder()
                .receiverDbxId(metaMap.get(META_IPNG_RECIPIENT_DBX_ID))
                .receiverEntityId(metaMap.get(META_IPNG_RECIPIENT_ENTITY_ID))
                .proofSentId(metaMap.get(META_IPNG_PROOF_SENT_ID))
                .proofReceiptId(metaMap.get(META_IPNG_PROOF_RECEIPT_ID))
                .proofResponseId(metaMap.get(META_IPNG_RESPONSE_ID))
                .build();


        try {
            IpngProofWrap sentProof = ipngService.getProof(proofs.getProofSentId());
            proofs.setProofSent(sentProof);
        } catch (IpInternalException e) {
            log.debug("Error retrieving sent proof for the folder : ", e);
            throw new LocalizedStatusException(NOT_FOUND, "message.no_ipng_send_proof_on_folder");
        }

        try {
            IpngProofWrap proofReceipt = ipngService.getProof(proofs.getProofReceiptId());
            proofs.setProofReceipt(proofReceipt);
        } catch (IpInternalException e) {
            log.debug("Error retrieving proof receipt for the folder : ", e);
            throw new LocalizedStatusException(NOT_FOUND, "message.no_ipng_receipt_proof_on_folder");
        }

        try {
            IpngProofWrap responseProof = ipngService.getProof(proofs.getProofResponseId());
            proofs.setProofResponse(responseProof);
        } catch (IpInternalException e) {
            log.debug("Error retrieving response proof for folder : ", e);
            throw new LocalizedStatusException(NOT_FOUND, "message.no_ipng_response_proof_on_folder");
        }

        result.add(proofs);
        return result;
    }


    @NotNull
    public Path createIpngProofFileForFolder(Folder folder) throws IOException {

        List<IpngFolderProofs> proofList = this.gatherIpngProofsForFolder(folder);
        Path ipngProofsTempFile = Files.createTempFile(TMP_IPNG_PROOFS_PREFIX, TMP_IPNG_PROOFS_SUFFIX);
        try (OutputStream os = Files.newOutputStream(ipngProofsTempFile)) {
            ObjectMapper om = new ObjectMapper();
            om.writeValue(os, proofList);
        }
        return ipngProofsTempFile;
    }


}
