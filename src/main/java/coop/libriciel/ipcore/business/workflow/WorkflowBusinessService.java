/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.business.workflow;

import coop.libriciel.crypto.api.model.DataToSign;
import coop.libriciel.crypto.api.model.DataToSignHolder;
import coop.libriciel.ipcore.business.template.TemplateBusinessService;
import coop.libriciel.ipcore.business.typology.TypologyBusinessService;
import coop.libriciel.ipcore.controller.DeskController;
import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.auth.requests.UserRepresentation;
import coop.libriciel.ipcore.model.content.CreateFolderRequest;
import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.crypto.DocumentDataToSignHolder;
import coop.libriciel.ipcore.model.crypto.PdfSignaturePosition;
import coop.libriciel.ipcore.model.crypto.SealCertificate;
import coop.libriciel.ipcore.model.crypto.SignatureFormat;
import coop.libriciel.ipcore.model.database.*;
import coop.libriciel.ipcore.model.database.userPreferences.UserPreferences;
import coop.libriciel.ipcore.model.externalsignature.ExternalSignatureParams;
import coop.libriciel.ipcore.model.ipng.IpngMetadata;
import coop.libriciel.ipcore.model.ipng.IpngMetadataList;
import coop.libriciel.ipcore.model.ipng.IpngProof;
import coop.libriciel.ipcore.model.ipng.IpngTypology;
import coop.libriciel.ipcore.model.workflow.*;
import coop.libriciel.ipcore.model.workflow.requests.SealTaskParams;
import coop.libriciel.ipcore.model.workflow.requests.SignatureTaskParams;
import coop.libriciel.ipcore.model.workflow.requests.SimpleTaskParams;
import coop.libriciel.ipcore.model.workflow.requests.StartWorkflowResponse;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.crypto.CryptoServiceInterface;
import coop.libriciel.ipcore.services.database.SubtypeRepository;
import coop.libriciel.ipcore.services.database.TypeRepository;
import coop.libriciel.ipcore.services.database.UserPreferencesRepository;
import coop.libriciel.ipcore.services.externalsignature.ExternalSignatureInterface;
import coop.libriciel.ipcore.services.filetransfer.FileTransferServiceInterface;
import coop.libriciel.ipcore.services.groovy.GroovyResultCatcher;
import coop.libriciel.ipcore.services.groovy.GroovyService;
import coop.libriciel.ipcore.services.ipng.IpngServiceInterface;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.services.secret.SecretServiceInterface;
import coop.libriciel.ipcore.services.securemail.SecureMailServiceInterface;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.services.transformation.TransformationServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.CryptoUtils;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.UserUtils;
import coop.libriciel.ipcore.utils.exceptions.MissingTagException;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.modelmapper.ModelMapper;
import org.springframework.core.io.Resource;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.util.*;
import java.util.regex.Matcher;
import java.util.stream.Stream;

import static coop.libriciel.ipcore.model.crypto.SignatureFormat.*;
import static coop.libriciel.ipcore.model.database.ExternalSignatureProvider.DOCAGE;
import static coop.libriciel.ipcore.model.database.ExternalSignatureProvider.OODRIVE_SIGN;
import static coop.libriciel.ipcore.model.database.TemplateType.*;
import static coop.libriciel.ipcore.model.workflow.Action.*;
import static coop.libriciel.ipcore.model.workflow.State.CURRENT;
import static coop.libriciel.ipcore.model.workflow.State.UPCOMING;
import static coop.libriciel.ipcore.model.workflow.WorkflowDefinition.*;
import static coop.libriciel.ipcore.services.workflow.IpWorkflowService.*;
import static coop.libriciel.ipcore.utils.CryptoUtils.SIGNATURE_PLACEHOLDER_CUSTOM_SIGNATURE_FIELD;
import static coop.libriciel.ipcore.utils.CryptoUtils.sha256Sign;
import static coop.libriciel.ipcore.utils.FolderUtils.*;
import static coop.libriciel.ipcore.utils.RequestUtils.writeFluxToOutputStream;
import static coop.libriciel.ipcore.utils.TextUtils.RESERVED_PREFIX;
import static coop.libriciel.ipcore.utils.UserUtils.AUTOMATIC_TASK_USER_ID;
import static java.util.Arrays.asList;
import static java.util.Collections.*;
import static java.util.Optional.ofNullable;
import static java.util.UUID.randomUUID;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.*;


@Log4j2
@Service
public class WorkflowBusinessService {


    // <editor-fold desc="Beans">


    private final AuthServiceInterface authService;
    private final ContentServiceInterface contentService;
    private final CryptoServiceInterface cryptoService;
    private final ExternalSignatureInterface externalSignatureService;
    private final FileTransferServiceInterface fileTransferService;
    private final GroovyService groovyService;
    private final IpngServiceInterface ipngService;
    private final PermissionServiceInterface permissionService;
    private final StatsServiceInterface statsService;
    private final SecretServiceInterface secretService;
    private final SecureMailServiceInterface secureMailService;
    private final TemplateBusinessService templateBusinessService;
    private final TransformationServiceInterface transformationService;
    private final TypeRepository typeRepository;
    private final TypologyBusinessService typologyBusinessService;
    private final UserPreferencesRepository userPreferencesRepository;
    private final WorkflowServiceInterface workflowService;
    private final ModelMapper modelMapper;
    private final SubtypeRepository subtypeRepository;


    public WorkflowBusinessService(AuthServiceInterface authService,
                                   ContentServiceInterface contentService,
                                   CryptoServiceInterface cryptoService,
                                   ExternalSignatureInterface externalSignatureService,
                                   FileTransferServiceInterface fileTransferService,
                                   GroovyService groovyService,
                                   IpngServiceInterface ipngService,
                                   PermissionServiceInterface permissionService,
                                   StatsServiceInterface statsService,
                                   SecretServiceInterface secretService,
                                   SecureMailServiceInterface secureMailService,
                                   TemplateBusinessService templateBusinessService,
                                   TransformationServiceInterface transformationService,
                                   TypeRepository typeRepository,
                                   TypologyBusinessService typologyBusinessService,
                                   UserPreferencesRepository userPreferencesRepository,
                                   WorkflowServiceInterface workflowService,
                                   ModelMapper modelMapper, SubtypeRepository subtypeRepository) {
        this.authService = authService;
        this.contentService = contentService;
        this.cryptoService = cryptoService;
        this.externalSignatureService = externalSignatureService;
        this.fileTransferService = fileTransferService;
        this.ipngService = ipngService;
        this.permissionService = permissionService;
        this.secretService = secretService;
        this.secureMailService = secureMailService;
        this.templateBusinessService = templateBusinessService;
        this.transformationService = transformationService;
        this.typeRepository = typeRepository;
        this.groovyService = groovyService;
        this.statsService = statsService;
        this.typologyBusinessService = typologyBusinessService;
        this.userPreferencesRepository = userPreferencesRepository;
        this.workflowService = workflowService;
        this.modelMapper = modelMapper;
        this.subtypeRepository = subtypeRepository;
    }


    // </editor-fold desc="Beans">


    /**
     * @throws LocalizedStatusException if all the actor desks of the next step have been deleted, or do not have any owner.
     */
    public void checkForNullOrEmptyDesksInNextStep(String tenantId, Folder folder) {
        log.debug("checkForNullDesksInNextStep");

        List<Desk> populatedDesks = new ArrayList<>();
        Task nextTask = folder.getStepList()
                .stream()
                .filter(step -> step.getState().equals(UPCOMING))
                .findFirst()
                .orElseThrow(() -> {
                    log.error("Error -  no upcoming task in folder {}", folder.getName());
                    return new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_fetch_next_step_desks");
                });

        nextTask.getDesks().stream()
                .map(desk -> authService.findDeskByIdNoException(tenantId, desk.getId()))
                .filter(Objects::nonNull)
                .forEach(populatedDesks::add);

        // TODO if ParallelType == AND && one null or more -> throw exception
        if (CollectionUtils.isEmpty(populatedDesks)) {
            log.error("Error - no valid desk in upcoming task for folder: {}", folder.getName());
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.cannot_fetch_next_step_desks");
        }

        boolean noUserInAnyNextDesk = populatedDesks.stream()
                .map(desk -> authService.listUsersFromDesk(tenantId, desk.getId(), 0, 1))
                .peek(log::debug)
                .allMatch(usersInDesk -> usersInDesk.getTotal() == 0);

        if (noUserInAnyNextDesk) {
            log.error("No user in any desk of upcoming task for folder: {}", folder.getName());
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.no_user_in_next_step_desks");
        }
    }


    public @NotNull StartWorkflowResponse doStartWorkflow(String tenantId,
                                                          Folder folder,
                                                          String deskId,
                                                          SimpleTaskParams simpleTaskParams) {
        log.debug("doStartWorkflow");

        if (asList(XADES_DETACHED, PKCS7).contains(folder.getType().getSignatureFormat())) {

            long signatureStepsCount = folder.getStepList().stream().filter(s -> asList(SIGNATURE, EXTERNAL_SIGNATURE, SEAL).contains(s.getAction())).count();
            long documentCount = folder.getDocumentList().stream().filter(Document::isMainDocument).count();
            long signaturesCount = signatureStepsCount * documentCount;

            if (signaturesCount > DeskController.MAX_DETACHED_SIGNATURES_GENERATED_PER_FOLDER) {
                throw new LocalizedStatusException(
                        INSUFFICIENT_STORAGE,
                        "message.this_workflow_will_generates_n_detached_signatures_for_a_limit_of_m",
                        signaturesCount, DeskController.MAX_DETACHED_SIGNATURES_GENERATED_PER_FOLDER
                );
            }
        }

        // Start

        Task startTask = folder.getStepList()
                .stream()
                .filter(t -> t.getAction() == START)
                .filter(t -> t.getState() == CURRENT)
                .filter(t -> t.getDate() == null)
                .findFirst()
                .orElseThrow(() -> new LocalizedStatusException(CONFLICT, "message.cannot_start_this_folder"));

        checkForNullOrEmptyDesksInNextStep(tenantId, folder);

        // TODO this should be either calculated at Folder construction time, or in a shared utils, not ad-hoc code
        boolean hasCreationWorkflow = folder.getStepList().stream()
                .filter(t -> t.getAction() != START)
                .filter(t -> t.getState() == UPCOMING)
                .filter(t -> t.getDate() == null)
                .findFirst()
                .map(Task::getWorkflowIndex)
                .filter(workflowIdx -> workflowIdx < VALIDATION_WORKFLOW_SMALLEST_INDEX).isPresent();

        if (!hasCreationWorkflow) {
            typologyBusinessService.verifyFolderMetadataForValidationWorkflow(tenantId, folder);
            convertMainDocsToPdfIfNeeded(tenantId, folder);
        }

        // Pre-start checks

        // FIXME : Evaluate an overridden workflow definition id ?

        // Starting workflow

        workflowService.performTask(START, startTask, deskId, folder, simpleTaskParams);
        // FIXME : statsService.registerSimpleAction(tenant, START, deskId, folderId);

        log.debug("Folder started {}", folder);
        return new StartWorkflowResponse(folder.getId());
    }


    public WorkflowDefinition computeWorkflowDefinitionFromScriptResult(@NotNull GroovyResultCatcher scriptResult,
                                                                        @NotNull String tenantId,
                                                                        @NotNull String deskId) {

        Map<Integer, String> variableDesksIdsMap = new HashMap<>();

        if (StringUtils.isEmpty(scriptResult.getWorkflowDefinitionKey())) {
            return null;
        }

        WorkflowDefinition workflowDefinition = workflowService
                .getWorkflowDefinitionByKey(tenantId, scriptResult.getWorkflowDefinitionKey())
                .orElseThrow(() -> new LocalizedStatusException(NOT_FOUND, "message.unknown_workflow_definition_key"));

        log.debug("evaluateWorkflowSelectionScript overriddenWorkflow:{}", workflowDefinition.getName());
        groovyService.updateVariableDeskMapFromScriptResult(tenantId, scriptResult, workflowDefinition, variableDesksIdsMap);

        // TODO: Maybe all this, but like, in WorkflowService?
        workflowService.substitutePlaceholdersToIndexedPlaceholders(workflowDefinition);
        Map<String, String> placeholdersMapping = workflowService.computePlaceholderDesksConcreteValues(
                workflowDefinition,
                deskId,
                variableDesksIdsMap
        );

        streamInternalReferencedDesks(workflowDefinition)
                .forEach(d -> workflowService.mapIndexedPlaceholdersToActualDesk(d, deskId, placeholdersMapping));

        populateDesksAndValidators(workflowDefinition);
        return workflowDefinition;
    }


    public @NotNull Folder prepareDraftFromParams(Tenant tenant,
                                                  Desk desk,
                                                  CreateFolderRequest draftFolderParams,
                                                  String folderName,
                                                  @NotNull Map<Integer, String> variableDeskIds) {

        Folder result = new Folder();
        result.setName(folderName);
        result.setLegacyId(draftFolderParams.getLegacyId());
        result.setDueDate(draftFolderParams.getDueDate());
        result.setVisibility(draftFolderParams.getVisibility());
        result.setOriginDesk(new DeskRepresentation(desk.getId(), desk.getName()));
        result.setType(Optional.ofNullable(draftFolderParams.getTypeId()).map(i -> Type.builder().id(i).build()).orElse(null));
        result.setSubtype(Optional.ofNullable(draftFolderParams.getSubtypeId()).map(i -> Subtype.builder().id(i).build()).orElse(null));
        result.setMetadata(new HashMap<>());

        // Setup Typology

        typologyBusinessService.updateTypology(tenant.getId(), singletonList(result), false);

        if (result.getSubtype() != null) {
            result.setCreationWorkflowDefinitionKey(result.getSubtype().getCreationWorkflowId());
            result.setValidationWorkflowDefinitionKey(result.getSubtype().getValidationWorkflowId());
        }

        // Integrity checks

        log.info("prepareDraftFromParams - checking metadata...");
        checkAndCopyMetadataFromParams(draftFolderParams, result);
        log.info("prepareDraftFromParams - check metadata OK");

        groovyService.updateWorkflowDataFromScript(tenant.getId(), result, variableDeskIds);
        return result;
    }


    public void checkAndCopyMetadataFromParams(CreateFolderRequest draftFolderParams, Folder result) {

        // Prepare some useful collections

        List<SubtypeMetadata> subtypeMetadata = result.getSubtype().getSubtypeMetadataList();

        Set<String> metadataKeys = subtypeMetadata.stream()
                .map(SubtypeMetadata::getMetadata)
                .map(Metadata::getKey)
                .collect(toSet());

        Map<String, Metadata> idMetadataMap = subtypeMetadata.stream()
                .map(SubtypeMetadata::getMetadata)
                .collect(toMap(Metadata::getId, m -> m));

        // Filter out the subtype association, and force the metadata Key as map Key
        Map<String, String> checkedMetadata = new HashMap<>();
        Map<String, String> metadataMap = draftFolderParams.getMetadata();

        if (metadataMap != null) {
            Map<String, String> parsedMetadataReferencedByKeys = metadataMap.entrySet().stream()
                    .filter(entry -> !entry.getKey().startsWith(RESERVED_PREFIX))
                    .filter(entry -> metadataKeys.contains(entry.getKey()))
                    .filter(entry -> entry.getValue() != null)
                    .collect(toMap(Map.Entry::getKey, Map.Entry::getValue));

            Map<String, String> parsedMetadataReferencedByIds = metadataMap.entrySet().stream()
                    .filter(entry -> !entry.getKey().startsWith(RESERVED_PREFIX))
                    .filter(entry -> idMetadataMap.containsKey(entry.getKey()))
                    .collect(toMap(
                            entry -> idMetadataMap.get(entry.getKey()).getKey(),
                            Map.Entry::getValue)
                    );

            Map<String, String> reservedMetadata = metadataMap.entrySet().stream()
                    .filter(entry -> entry.getKey().startsWith(RESERVED_PREFIX))
                    .collect(toMap(Map.Entry::getKey, Map.Entry::getValue));

            checkedMetadata.putAll(reservedMetadata);
            checkedMetadata.putAll(parsedMetadataReferencedByKeys);
            checkedMetadata.putAll(parsedMetadataReferencedByIds);

            // Logs the removed metadata.
            // Note that it is a v4 behaviour: clients can throw any metadata, and the iparapheur has to clean everything up.
            // This is bad, and painful, but we have to stick to it.

            metadataMap.keySet().stream()
                    .filter(key -> !key.startsWith(RESERVED_PREFIX))
                    .filter(key -> !metadataKeys.contains(key))
                    .filter(key -> !idMetadataMap.containsKey(key))
                    .forEach(key -> log.warn("createDraftFolder - A sent metadata does not belong to the given subtype, ignored: {}", key));

        }

        // Check subtype mandatory metadata

        List<String> missingMandatoryMetadata = TypologyBusinessService.getMissingMandatoryMetadata(checkedMetadata, subtypeMetadata, true);
        if (CollectionUtils.isNotEmpty(missingMandatoryMetadata)) {
            log.debug("verifyFolderMetadata - Some mandatory metadata were not filled, abort: {}", missingMandatoryMetadata);
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.missing_mandatory_metadata", missingMandatoryMetadata.get(0));
        }

        result.setMetadata(checkedMetadata);
    }


    public Folder createDraftFolderFromInternalTask(@NotNull String deskId,
                                                    @NotNull String folderName,
                                                    @NotNull Tenant tenant,
                                                    @NotNull List<DocumentBuffer> documentList,
                                                    @NotNull CreateFolderRequest draftFolderParams) {


        Desk desk = Desk.builder().id(deskId).build();

        Map<Integer, String> variableDesksIdsMap = new HashMap<>();
        if (draftFolderParams.getVariableDesksIds() != null) {
            draftFolderParams.getVariableDesksIds().forEach((key, value) -> variableDesksIdsMap.put(Integer.parseInt(key), value));
        }

        Folder result = prepareDraftFromParams(tenant, desk, draftFolderParams, folderName, variableDesksIdsMap);

        // Creating Folder

        String contentId = contentService.createFolder(tenant);
        result.setContentId(contentId);

        for (DocumentBuffer doc : documentList) {
            String documentId = contentService.createDocument(tenant.getId(), result, doc);
            doc.setId(documentId);
            result.getDocumentList().add(doc);
        }

        // Upload to Flowable

        log.info("type:{} subtype:{} creationW:{} validationW:{}", result.getType(), result.getSubtype(),
                result.getCreationWorkflowDefinitionKey(), result.getValidationWorkflowDefinitionKey());

        String createdFolderId = workflowService.createDraftWorkflow(tenant.getId(), result, variableDesksIdsMap).getId();
        result.setId(createdFolderId);

        // Sending back result

        log.debug("CreateFolder folder:{}", result);
        statsService.registerFolderAction(tenant, CREATE, result, result.getOriginDesk(), null);
        return result;
    }


    /**
     * Loops through every desks, actors and notified.
     *
     * @param workflowDefinition
     * @return A null-filtered Stream of desks
     */
    private static Stream<DeskRepresentation> streamInternalReferencedDesks(WorkflowDefinition workflowDefinition) {
        return workflowDefinition.getSteps().stream()
                .flatMap(d -> Stream.concat(
                        Stream.concat(
                                Optional.ofNullable(d.getValidatingDesks()).orElse(emptyList()).stream(),
                                Stream.of(workflowDefinition.getFinalDesk())
                        ),
                        Stream.concat(
                                Optional.ofNullable(d.getNotifiedDesks()).orElse(emptyList()).stream(),
                                workflowDefinition.getFinalNotifiedDesks().stream()
                        )
                ))
                .filter(Objects::nonNull);
    }


    /**
     * Update the referenced desks names,
     * and swap the inner placeholders to the public ones.
     *
     * @param workflowDefinition
     */
    public void populateDesksAndValidators(WorkflowDefinition workflowDefinition) {

        Set<String> referencedDeskIds = streamInternalReferencedDesks(workflowDefinition)
                .map(DeskRepresentation::getId)
                .filter(StringUtils::isNotEmpty)
                .filter(id -> !isVariableDeskIdPlaceholder(id))
                .collect(toSet());

        log.trace("populateDesksAndValidators referencedDeskIds:{}", referencedDeskIds);
        Map<String, String> referencedDeskInfos = authService.getDeskNames(referencedDeskIds);
        log.trace("populateDesksAndValidators referencedDeskInfos:{}", referencedDeskInfos);

        streamInternalReferencedDesks(workflowDefinition)
                .filter(entity -> StringUtils.isNotEmpty(entity.getId()))
                .filter(entity -> !replaceVariablePlaceholdersByInternalConstant(entity))
                .forEach(entity -> entity.setName(referencedDeskInfos.get(entity.getId())));
    }


    /**
     * The internal WorkflowDefinition has a special structure, with a split final step.
     * We have to make the API (= the DTO) cleaner.
     * <p>
     * Since the conversion is slightly too complex, we can't simply use the ModelMapper.
     *
     * @param workflowDefinition
     * @return a proper WorkflowDefinitionDto
     */
    public WorkflowDefinitionDto convertToDto(WorkflowDefinition workflowDefinition) {

        WorkflowDefinitionDto workflowDefinitionDto = modelMapper.map(workflowDefinition, WorkflowDefinitionDto.class);

        // Repack the final step

        StepDefinitionDto finalStepDefinitionDto = new StepDefinitionDto();
        finalStepDefinitionDto.setType(StepDefinitionType.ARCHIVE);
        finalStepDefinitionDto.setValidatingDesks(singletonList(workflowDefinition.getFinalDesk()));
        finalStepDefinitionDto.setNotifiedDesks(workflowDefinition.getFinalNotifiedDesks());

        workflowDefinitionDto.getSteps().add(finalStepDefinitionDto);

        // Convert the public placeholder ids

        workflowDefinitionDto.getSteps().stream()
                .map(StepDefinitionDto::getValidatingDesks)
                .flatMap(Collection::stream)
                .forEach(desk -> desk.setId(GENERIC_VALIDATORS_INTERNAL_TO_PUBLIC_IDS_CONVERTER.apply(desk.getId())));

        workflowDefinitionDto.getSteps().stream()
                .map(StepDefinitionDto::getNotifiedDesks)
                .flatMap(Collection::stream)
                .forEach(desk -> desk.setId(GENERIC_VALIDATORS_INTERNAL_TO_PUBLIC_IDS_CONVERTER.apply(desk.getId())));

        return workflowDefinitionDto;
    }


    /**
     * The internal WorkflowDefinition has a special structure, with a split final step.
     * We have to make the API (= the DTO) cleaner.
     * <p>
     * Since the conversion is slightly too complex, we can't simply use the ModelMapper.
     *
     * @param workflowDefinitionDto
     * @return a proper WorkflowDefinition
     */
    public WorkflowDefinition convertToInternal(WorkflowDefinitionDto workflowDefinitionDto) {

        WorkflowDefinition workflowDefinition = modelMapper.map(workflowDefinitionDto, WorkflowDefinition.class);

        // Unpack the final step

        StepDefinition finalStepDefinition = workflowDefinition.getSteps().stream()
                .filter(step -> step.getType() == ARCHIVE)
                .reduce((first, second) -> second)
                .orElseThrow(() -> new RuntimeException("No final task available. This should not happen."));

        workflowDefinition.setFinalDesk(finalStepDefinition.getValidatingDesks().stream()
                .findFirst()
                .orElse(null));

        workflowDefinition.setFinalNotifiedDesks(finalStepDefinition.getNotifiedDesks());

        workflowDefinition.getSteps().removeIf(step -> step.getType() == ARCHIVE);

        // Convert the internal placeholder ids

        streamInternalReferencedDesks(workflowDefinition)
                .forEach(desk -> desk.setId(GENERIC_VALIDATORS_PUBLIC_TO_INTERNAL_IDS_CONVERTER.apply(desk.getId())));

        return workflowDefinition;
    }


    private boolean replaceVariablePlaceholdersByInternalConstant(DeskRepresentation e) {

        Matcher matcher = META_EMITTER_PATTERN.matcher(e.getId());
        if (matcher.matches()) {
            e.setId(EMITTER_ID_PLACEHOLDER);
            return true;
        }

        matcher = META_BOSSOF_PATTERN.matcher(e.getId());
        if (matcher.matches()) {
            e.setId(BOSS_OF_ID_PLACEHOLDER);
            return true;
        }

        matcher = META_VARIABLEDESK_PATTERN.matcher(e.getId());
        if (matcher.matches()) {
            e.setId(VARIABLE_DESK_ID_PLACEHOLDER);
            return true;
        }

        return false;
    }


    private boolean isVariableDeskIdPlaceholder(String id) {

        if (META_EMITTER_PATTERN.matcher(id).matches()) {
            return true;
        }

        if (META_BOSSOF_PATTERN.matcher(id).matches()) {
            return true;
        }

        return META_VARIABLEDESK_PATTERN.matcher(id).matches();
    }


    @Retryable(
            retryFor = MissingTagException.class,
            maxAttempts = MissingTagException.RETRY_ATTEMPTS,
            backoff = @Backoff(delay = MissingTagException.RETRY_DELAY)
    )
    public void performRetryableAutoSeal(@NotNull Tenant tenant, @NotNull Desk desk, @NotNull Folder folder, @NotNull Task task) throws MissingTagException {
        log.info("Trying automatic seal, desk:{} folder:{}", desk.getId(), folder.getId());

        // Obviously, we have to refresh the documents in here, on every retry
        contentService.populateFolderWithAllDocumentTypes(folder);

        if (folder.getDocumentList().stream()
                .filter(Document::isMainDocument)
                .filter(document -> document.getMediaType().isCompatibleWith(APPLICATION_PDF))
                .anyMatch(document -> document.getSealTags() == null)) {
            log.error("Seal tags are missing, the automatic seal task may retry later");
            throw new MissingTagException("Seal tags are missing, cancel the automatic seal task");
        }

        performSeal(tenant, desk, folder, task, UserUtils.getAutomaticUser(), null);
    }


    public void performSeal(@NotNull Tenant tenant,
                            @NotNull Desk desk,
                            @NotNull Folder folder,
                            @NotNull Task task,
                            @NotNull User currentUser,
                            @Nullable SealTaskParams sealTaskParams) {

        checkForNullOrEmptyDesksInNextStep(tenant.getId(), folder);

        log.info("performSeal folderId:{}", folder.getId());

        // Integrity check

        folder.setType(typeRepository.findById(folder.getType().getId())
                .orElseThrow(() -> {
                    log.error("Error - type not found for folder : {}", folder.getName());
                    return new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.unknown_type_id");
                }));

        // The document may already be populated on auto-tasks
        if (CollectionUtils.isEmpty(folder.getDocumentList())) {
            contentService.populateFolderWithAllDocumentTypes(folder);
        }

        checkActionAllowedWithDetachedSignatures(folder, SEAL);

        typologyBusinessService.updateTypology(tenant.getId(), singletonList(folder), false);

        SealCertificate sealCert = ofNullable(folder.getSubtype().getSealCertificateId())
                .map(id -> secretService.getSealCertificate(tenant.getId(), id))
                .orElseThrow(() -> {
                    log.error("Error - seal certificate not found for folder : {}", folder.getName());
                    return new LocalizedStatusException(NOT_FOUND, "message.unknown_seal_certificate_id");
                });

        String signatureImageBase64 = ofNullable(sealCert.getSignatureImageContentId())
                .filter(StringUtils::isNotEmpty)
                .map(contentService::retrieveContentAsBase64)
                .orElse(null);

        PrivateKey privateKey = CryptoUtils.getPrivateKey(sealCert);
        if (sealCert.getExpirationDate().before(new Date())) {
            throw new LocalizedStatusException(NOT_ACCEPTABLE, "message.expired_seal_certificate");
        }

        Desk delegatingDesk = getDelegatingDesk(tenant, desk, folder);

        // Actual Seal

        long signatureDateTime = new Date().getTime();
        SignatureFormat signatureFormat = cryptoService.getSignatureFormat(folder.getType(), folder.getDocumentList());
        TemplateType forcedSealTemplate = StringUtils.equals(AUTOMATIC_TASK_USER_ID, currentUser.getId()) ? SEAL_AUTOMATIC : null;
        UserPreferences currentUserPreferences = Optional.ofNullable(currentUser.getId())
                .flatMap(userPreferencesRepository::getByUserId)
                .orElse(new UserPreferences());

        folder.getDocumentList().stream()
                .filter(Document::isMainDocument)
                .forEach(d -> {

                    DocumentBuffer documentBuffer = contentService.retrieveContent(d.getId());

                    documentBuffer.setName(d.getName());
                    documentBuffer.setId(d.getId());
                    PdfSignaturePosition pdfSignaturePosition = CryptoUtils.getPosition(folder, d, currentUserPreferences, SEAL);
                    TemplateType templateType = firstNonNull(forcedSealTemplate, pdfSignaturePosition.getTemplateType(), SEAL_MEDIUM);
                    Resource templateResource = templateBusinessService.getTemplateResource(tenant, templateType);

                    DataToSignHolder dataToSignHolder = cryptoService.getDataToSign(
                            tenant,
                            desk,
                            folder,
                            currentUser,
                            documentBuffer,
                            signatureFormat,
                            pdfSignaturePosition,
                            sealCert.getPublicCertificateBase64(),
                            sealCert.getName(),
                            signatureDateTime,
                            true,
                            templateResource,
                            signatureImageBase64,
                            Optional.ofNullable(sealTaskParams).map(SealTaskParams::getCustomSignatureField).orElse(null),
                            delegatingDesk);

                    long originalDocSize = documentBuffer.getContentLength();

                    for (DataToSign dataToSign : dataToSignHolder.getDataToSignList()) {
                        String stringToSignBase64 = dataToSign.getDataToSignBase64();
                        byte[] bytesToSign = Base64.getDecoder().decode(stringToSignBase64);
                        dataToSign.setSignatureValue(sha256Sign(bytesToSign, privateKey));
                    }

                    // We have to reset the streams here,
                    // the documents has to be parsed once for the digest, and a second time for the signature injection.
                    // Special case : PKCS7 doesn't need the document on signature generation.
                    documentBuffer = (signatureFormat != PKCS7) ? contentService.retrieveContent(d.getId()) : null;
                    templateResource = templateBusinessService.getTemplateResource(tenant, templateType);

                    DocumentBuffer signedDocumentBuffer = cryptoService.signDocument(
                            tenant,
                            desk,
                            folder,
                            currentUser,
                            dataToSignHolder,
                            signatureFormat,
                            pdfSignaturePosition,
                            sealCert.getPublicCertificateBase64(),
                            sealCert.getName(),
                            true,
                            templateResource,
                            documentBuffer,
                            signatureImageBase64,
                            ofNullable(sealTaskParams).map(SealTaskParams::getCustomSignatureField).orElse(null),
                            delegatingDesk
                    );

                    signedDocumentBuffer.setId(d.getId());
                    signedDocumentBuffer.setName(d.getName());
                    downloadThenSaveSignedDocument(task, folder, currentUser, signatureFormat, d, signedDocumentBuffer);
                });

        workflowService.performTask(task, SEAL, folder, desk.getId(), sealTaskParams, null, sealCert.getPublicCertificateBase64());
        contentService.removeFirstSignaturePlacementAnnotation(folder.getDocumentList());
        Long timeToCompleteInHours = statsService.computeTimeToCompleteInHours(task);
        statsService.registerFolderAction(tenant, SEAL, folder, desk, timeToCompleteInHours);

        log.info("PerformAction taskId:{}", task.getId());
    }


    /**
     * In the case where the acting desk is not part of the current step's validators, retrieve the
     * desk that has given delegation to the acting desk, otherwise, return null.
     *
     * @return The delegating desk, or null if not applicable
     */
    @Nullable
    public Desk getDelegatingDesk(@NotNull Tenant tenant, @NotNull Desk desk, @NotNull Folder folder) {
        String actingDeskId = desk.getId();
        List<String> currentStepDeskIds = folder.getStepList()
                .stream()
                .filter(step -> isActive(step.getState()))
                .flatMap(step -> step.getDesks().stream())
                .map(DeskRepresentation::getId)
                .toList();

        Desk delegatingDesk = null;
        if (!currentStepDeskIds.contains(actingDeskId)) {
            // It's a delegation, retieve the probable delegating desk
            Set<String> deskHavingDelegToActingDesk = permissionService.getActiveDelegationsForDesks(tenant.getId(), singleton(actingDeskId))
                    .get(actingDeskId)
                    .stream()
                    .map(DelegationRule::getDelegatingGroup)
                    .collect(toSet());

            delegatingDesk = currentStepDeskIds.stream()
                    .filter(deskHavingDelegToActingDesk::contains)
                    .findAny()
                    .map(id -> authService.findDeskByIdNoException(tenant.getId(), id))
                    .orElse(null);
        }

        return delegatingDesk;
    }


    // @PreAuthorize("hasRole(#task.candidateGroup)")
    public void performSignature(@NotNull Tenant tenant,
                                 @NotNull Desk desk,
                                 @NotNull Task task,
                                 @NotNull Folder folder,
                                 @NotNull User user,
                                 SignatureTaskParams signatureTaskParams) {

        checkForNullOrEmptyDesksInNextStep(tenant.getId(), folder);
        assertCurrentUserCanPerformTask(tenant.getId(), task, user, folder.getType().getId(), folder.getSubtype().getId(), desk.getId());

        Desk delegatingDesk = getDelegatingDesk(tenant, desk, folder);

        if (StringUtils.isNotEmpty(signatureTaskParams.getCustomSignatureField())) {
            List<String> keysToRemove = folder.getMetadata()
                    .keySet()
                    .stream()
                    // FIXME all metadata with the workflow_internal prefix should not be visible here
                    .filter(key -> key.contains("workflow_internal_"))
                    .toList();

            keysToRemove.forEach(key -> folder.getMetadata().remove(key));
            folder.getMetadata().put(SIGNATURE_PLACEHOLDER_CUSTOM_SIGNATURE_FIELD, signatureTaskParams.getCustomSignatureField());
            workflowService.editFolder(tenant.getId(), folder.getId(), folder);
        }

        List<DocumentDataToSignHolder> dataToSignHolderList = signatureTaskParams.getDataToSignHolderList();
        String certBase64 = signatureTaskParams.getCertificateBase64();

        List<Document> mainDocumentList = folder.getDocumentList().stream().filter(Document::isMainDocument).toList();

        String userSignatureBase64 = ofNullable(user.getSignatureImageContentId())
                .filter(StringUtils::isNotEmpty)
                .map(contentService::retrieveContentAsBase64)
                .orElse(null);

        // We don't want to partially sign a folder.
        // We'll check for every document signature, first.
        Set<String> mainDocumentIdSet = mainDocumentList.stream().map(Document::getId).collect(toSet());
        Set<String> signedDocumentIdSet = dataToSignHolderList.stream().map(DocumentDataToSignHolder::getDocumentId).collect(toSet());
        if (!CollectionUtils.isEqualCollection(mainDocumentIdSet, signedDocumentIdSet)) {
            log.warn("The number of signatures does not match the number of documents");
            throw new LocalizedStatusException(BAD_REQUEST, "message.error_missing_signature");
        }

        folder.setType(this.typeRepository.findById(folder.getType().getId())
                .orElseThrow(() -> {
                    log.error("Error - type not found for folder : {}", folder.getName());
                    return new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.unknown_type_id");
                }));

        SignatureFormat signatureFormat = cryptoService.getSignatureFormat(folder.getType(), folder.getDocumentList());

        mainDocumentList.forEach(d -> {
            DataToSignHolder dataToSignHolder = dataToSignHolderList.stream()
                    .filter(p -> StringUtils.equals(p.getDocumentId(), d.getId()))
                    .findFirst()
                    .orElseThrow(() -> new LocalizedStatusException(BAD_REQUEST, "message.error_missing_signature"));

            // We have to reset the stream here, the document has to be parsed once for the digest,
            // and a second time for the signature injection. Large document streams aren't (and should not) be resettable.
            // Special case : PKCS7 doesn't need the document on signature generation.
            DocumentBuffer docBuffer = (signatureFormat != PKCS7) ? contentService.retrieveContent(d.getId()) : null;

            UserPreferences currentUserPreferences = userPreferencesRepository.getByUserId(user.getId()).orElse(new UserPreferences());
            PdfSignaturePosition pdfSignaturePosition = CryptoUtils.getPosition(folder, d, currentUserPreferences, SIGNATURE);
            TemplateType templateType = firstNonNull(pdfSignaturePosition.getTemplateType(), SIGNATURE_MEDIUM); // TODO: Link the user's default template
            Resource templateResource = templateBusinessService.getTemplateResource(tenant, templateType);

            DocumentBuffer signedDocumentBuffer = cryptoService.signDocument(
                    tenant,
                    desk,
                    folder,
                    user,
                    dataToSignHolder,
                    signatureFormat,
                    pdfSignaturePosition,
                    certBase64,
                    user.getUserName(),
                    false,
                    templateResource,
                    docBuffer,
                    userSignatureBase64,
                    signatureTaskParams.getCustomSignatureField(),
                    delegatingDesk
            );

            signedDocumentBuffer.setId(d.getId());
            signedDocumentBuffer.setName(d.getName());
            downloadThenSaveSignedDocument(task, folder, user, signatureFormat, d, signedDocumentBuffer);
        });

        contentService.removeFirstSignaturePlacementAnnotation(mainDocumentList);
        workflowService.performTask(task, SIGNATURE, folder, desk.getId(), signatureTaskParams, null, certBase64);
        Long timeToCompleteInHours = statsService.computeTimeToCompleteInHours(task);
        statsService.registerFolderAction(tenant, SIGNATURE, folder, desk, timeToCompleteInHours);

        log.info("PerformAction taskId:{}", task.getId());
        triggerPostTaskActions(tenant.getId(), desk, folder, SIGNATURE);
    }


    /**
     * Use a blocking call to retrieve entirely the signedDocumentBuffer content into a temp storage
     * (current impl uses a byte array), then send it to contentService.
     * This is necessary because if we don't block and an error occurs on crypto, the exception is raised too late and we store an empty file on contentService.
     * <p>
     * TODO we have the doc size on th e calling contexts, so we could implement a special treatment using temp files instead of byte arrays, for big documents
     */
    public void downloadThenSaveSignedDocument(@NotNull Task task,
                                               @NotNull Folder folder,
                                               @NotNull User user,
                                               SignatureFormat signatureFormat,
                                               Document document,
                                               DocumentBuffer signedDocumentBuffer) {

        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            writeFluxToOutputStream(signedDocumentBuffer.getContentFlux(), baos);
            int signedDocSize = baos.size();
            byte[] signedDocBytes = baos.toByteArray();

            try (ByteArrayInputStream bais = new ByteArrayInputStream(signedDocBytes)) {
                Flux<DataBuffer> newSignedContentFlux = DataBufferUtils.readInputStream(() -> bais, new DefaultDataBufferFactory(), signedDocSize);
                signedDocumentBuffer.setContentFlux(newSignedContentFlux);

                if (signatureFormat.isEnvelopedSignature()) {
                    contentService.updateDocument(document.getId(), signedDocumentBuffer, false);
                } else {
                    task.setUser(modelMapper.map(user, UserRepresentation.class));
                    // Hardcoded fix.
                    // TODO : Parse it from Crypto
                    signedDocumentBuffer.setMediaType(signatureFormat == XADES_DETACHED ? APPLICATION_XML : APPLICATION_OCTET_STREAM);
                    contentService.createDetachedSignature(folder.getContentId(), document, signedDocumentBuffer, task);
                }
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    @Retryable(
            retryFor = MissingTagException.class,
            maxAttempts = MissingTagException.RETRY_ATTEMPTS,
            backoff = @Backoff(delay = MissingTagException.RETRY_DELAY)
    )
    public void performRetryableAutoExternalSignature(@NotNull Tenant tenant,
                                                      @NotNull Desk desk,
                                                      @NotNull Folder folder,
                                                      @NotNull Task task,
                                                      @NotNull ExternalSignatureParams externalSignatureParams) throws MissingTagException {
        log.info("Trying external-signature, desk:{} folder:{}", desk.getId(), folder.getId());

        // Obviously, we have to refresh the documents in here, on every retry
        contentService.populateFolderWithAllDocumentTypes(folder);

        if (folder.getDocumentList().stream()
                .filter(Document::isMainDocument)
                .filter(document -> document.getMediaType().isCompatibleWith(APPLICATION_PDF))
                .anyMatch(document -> document.getSignatureTags() == null)) {
            log.error("Signature tags are missing, the automatic external-signature task may retry later");
            throw new MissingTagException("Signature tags are missing, cancel the automatic external-signature task");
        }

        performExternalSignature(tenant, desk, folder, task, UserUtils.getAutomaticUser(), externalSignatureParams);
    }


    public void performExternalSignature(@NotNull Tenant tenant,
                                         @NotNull Desk desk,
                                         @NotNull Folder folder,
                                         @NotNull Task task,
                                         @NotNull User user,
                                         @NotNull ExternalSignatureParams externalSignatureParams) {

        checkForNullOrEmptyDesksInNextStep(tenant.getId(), folder);

        if (StringUtils.isEmpty(folder.getSubtype().getId())) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.unknown_subtype_id");
        }

        // The document may already be already populated on auto-tasks
        if (CollectionUtils.isEmpty(folder.getDocumentList())) {
            contentService.populateFolderWithAllDocumentTypes(folder);
        }

        Subtype subtype = subtypeRepository.findById(folder.getSubtype().getId())
                .orElseThrow(() -> new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.unknown_subtype_id"));

        boolean signatureProviderRefusesCoSignature = subtype.getExternalSignatureConfig().getServiceName().equalsOneOf(DOCAGE, OODRIVE_SIGN);
        boolean containsSignature = folder.getDocumentList()
                .stream()
                .filter(Document::isMainDocument)
                .map(Document::getEmbeddedSignatureInfos)
                .filter(CollectionUtils::isNotEmpty)
                .flatMap(List::stream)
                .anyMatch(Objects::nonNull);
        boolean hasAnyPasswordProtected = folder.getDocumentList()
                .stream()
                .filter(Document::isMainDocument)
                .filter(document -> document.getPasswordProtected() != null)
                .anyMatch(Document::getPasswordProtected);

        if (signatureProviderRefusesCoSignature && containsSignature) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.external_signature_provider_cannot_sign_an_already_signed_document");
        }
        if (signatureProviderRefusesCoSignature && hasAnyPasswordProtected) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.external_signature_provider_cannot_sign_a_password_protected_document");
        }

        checkActionAllowedWithDetachedSignatures(folder, EXTERNAL_SIGNATURE);

        List<Document> pdfDocumentList = contentService.getDocumentList(folder.getContentId()).stream()
                .filter(Document::isMainDocument)
                .filter(document -> document.getMediaType().equals(APPLICATION_PDF))
                .toList();

        boolean isOodrive = subtype.getExternalSignatureConfig().getServiceName() == OODRIVE_SIGN;
        if (isOodrive && pdfDocumentList.size() > 1) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.oodrive_sign_does_not_support_multi_doc");
        }

        String procedureId = externalSignatureService.createProcedure(folder, externalSignatureParams, pdfDocumentList, folder.getSubtype().getId());
        workflowService.performExternalSignature(task, user, folder, externalSignatureParams, procedureId);

        Long timeToCompleteInHours = statsService.computeTimeToCompleteInHours(task);
        statsService.registerFolderAction(tenant, EXTERNAL_SIGNATURE, folder, desk, timeToCompleteInHours);
    }


    public void checkActionAllowedWithDetachedSignatures(@NotNull Folder folder, @NotNull Action action) {

        boolean containsDetachedSignatures = folder.getDocumentList()
                .stream()
                .filter(Document::isMainDocument)
                .map(Document::getDetachedSignatures)
                .anyMatch(CollectionUtils::isNotEmpty);

        // Default case, nothing else to check

        if (!containsDetachedSignatures) {
            return;
        }

        // Most external signature services won't do anything but PAdES.
        // So we'll prevent signature invalidation, and forbid that action for now.

        if (action == EXTERNAL_SIGNATURE) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.you_can_t_perform_this_action_with_detached_signatures");
        }

        // Regular case check

        boolean isCryptographicSignatureAction = action.equalsOneOf(SEAL, SIGNATURE);
        boolean isEnvelopedSignature = cryptoService
                .getSignatureFormat(folder.getType(), folder.getDocumentList())
                .isEnvelopedSignature();

        if (isEnvelopedSignature && isCryptographicSignatureAction) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.you_can_t_perform_this_action_with_detached_signatures");
        }
    }


    public void assertCurrentUserCanPerformTask(@NotNull String tenantId,
                                                @NotNull Task task,
                                                @Nullable User user,
                                                @Nullable String typeId,
                                                @Nullable String subtypeId,
                                                @Nullable String deskId) {

        Set<String> taskDeskIds = task.getDesks().stream().map(DeskRepresentation::getId).collect(toSet());
        boolean userHasDelegation = permissionService.currentUserHasDelegationOnSomeDeskIn(tenantId, taskDeskIds, typeId, subtypeId);

        if (!taskDeskIds.contains(deskId) && !userHasDelegation) {
            throw new LocalizedStatusException(BAD_REQUEST, "message.current_desk_not_consistent_with_task_desk_or_delegation");
        }

        if (user != null && user.getId().equals(AUTOMATIC_TASK_USER_ID)) {
            log.info("Automatic user, bypassing permission check.");
            return;
        }

        boolean canPerformActionOnFolderAsDesk = permissionService.currentUserHasAdminRightsOnTenant(tenantId) ||
                permissionService.currentUserHasFolderActionRightOnSomeDeskIn(tenantId, taskDeskIds, typeId, subtypeId);

        if (!canPerformActionOnFolderAsDesk) {
            log.info("assertCurrentUserCanPerformTask : failed to perform task {}, insufficient rights", task);
            throw new LocalizedStatusException(FORBIDDEN, "message.you_can_t_perform_an_action_on_this_folder");
        }
    }


    public void triggerPostTaskActions(String tenantId, Desk desk, Folder folder, @NotNull Action performedAction) {
        triggerIpngPostAction(tenantId, desk, folder);
        if (!performedAction.equals(READ)) {
            disableDeleteCapabilityOnCurrentDocuments(folder);
        }
    }


    /**
     * Detects if this folder is between creation and validation workflows.
     * If it is, may trigger automatic conversions to PDF depending on typology and doc types
     */
    public void convertToPdfOnWorkflowSwitch(@NotNull String tenantId, Folder folder, Task currentTask, Action actionBeingPerformed) {
        log.debug("convertToPdfOnWorkflowSwitch");

        // Stop if the current action is not a visa (eg reject, second opinion...)
        if (actionBeingPerformed != VISA) {
            return;
        }

        // Stop there if current workflow is not a creation workflow
        if (currentTask.getWorkflowIndex() != CREATION_WORKFLOW_INDEX) {
            return;
        }

        log.debug("convertToPdfOnWorkflowSwitch - current task is a visa, part of a creation workflow");

        Optional<Task> nextTask = folder.getStepList().stream()
                .filter(task -> task.getAction() != READ)
                .filter(task -> task.getState() == UPCOMING)
                .findFirst();

        // trigger conversion if next step is on validation workflow
        nextTask
                .filter(task -> task.getWorkflowIndex() > CREATION_WORKFLOW_INDEX)
                .ifPresent(t -> {
                    log.debug("convertToPdfOnWorkflowSwitch - next step is on validation workflow, start conversion process");
                    convertMainDocsToPdfIfNeeded(tenantId, folder);
                });
    }


    public void convertMainDocsToPdfIfNeeded(@NotNull String tenantId, @NotNull Folder folder) {
        log.debug("convertMainDocsToPdfIfNeeded");

        typologyBusinessService.updateTypology(tenantId, singletonList(folder), false);

        if (folder.getType().getSignatureFormat() != PADES) {
            return;
        }

        List<Document> documentList = Optional.of(contentService.getDocumentList(folder.getContentId()))
                .filter(CollectionUtils::isNotEmpty)
                .orElseThrow(() -> {
                    log.error("No document found for Folder: {}", folder.getName());
                    return new LocalizedStatusException(NOT_FOUND, "message.unknown_document_id");
                });

        log.debug("convertMainDocsToPdfIfNeeded - doclist size : {}", documentList.size());

        documentList.stream()
                .filter(Document::isMainDocument)
                .filter(document -> !APPLICATION_PDF.equalsTypeAndSubtype(document.getMediaType()))
                .forEach(document -> {
                    log.debug("convertMainDocsToPdfIfNeeded - converting doc {} to pdf", document.getName());
                    DocumentBuffer docBuffer = contentService.retrieveContent(document.getId());
                    docBuffer.setMediaType(document.getMediaType());
                    docBuffer.setName(document.getName());

                    DocumentBuffer transformedDocument = transformationService.toPdf(docBuffer);
                    transformedDocument.setName(FilenameUtils.removeExtension(document.getName()) + ".pdf");
                    transformedDocument.setIndex(document.getIndex());
                    transformedDocument.setMediaType(APPLICATION_PDF);
                    transformedDocument.setMainDocument(true);

                    contentService.updateDocument(document.getId(), transformedDocument, true);
                });

    }


    public void sendIpngProof(String token, Folder folder, IpngProof proof, String targetName) throws NoSuchAlgorithmException, IOException {
        String folderHash = this.fileTransferService.postFolder(folder, targetName);

        proof.setHash(folderHash);
        // Send data to IPNG
        this.ipngService.postProof(proof, token);
        log.info("Proof sent to IPNG network API, waiting for result");
    }


    public void disableDeleteCapabilityOnCurrentDocuments(Folder folder) {
        contentService.getDocumentList(folder.getContentId())
                .stream()
                .filter(document -> !document.isMainDocument())
                .forEach(document -> {
                    document.setDeletable(false);
                    contentService.updateDocumentInternalProperties(document);
                });
    }


    private void populateIpngResponseProofFromFolder(String token, String tenantId, Folder folder, IpngProof proof) {
        IpngTypology ipngFullTypo = this.ipngService.getLatestTypology(token);
        IpngMetadataList ipngFullMeta = this.ipngService.getLatestMetadataList(token);

        String allIpngMetadata = folder.getMetadata().get(METADATA_KEY_IPNG_RETURN_METADATA_IDS);
        List<IpngMetadata> proofMetadataList = new ArrayList<>();
        if (!StringUtils.isEmpty(allIpngMetadata)) {
            List<String> metadataKeys = asList(allIpngMetadata.split(IPNG_METADATA_KEYS_SEPARATOR));

            metadataKeys.forEach(key -> {
                ipngFullMeta.getMetadatas().stream()
                        .filter(ipngMetadata -> ipngMetadata.getKey().equals(key))
                        .findFirst()
                        .ifPresent(proofMetadataList::add);
            });
        }
        proof.setMetadatas(proofMetadataList);

        String ipngTypeKey = folder.getMetadata().getOrDefault(METADATA_KEY_IPNG_RETURN_TYPE_ID, "000-default");
        proof.setType(ipngFullTypo.getTypes()
                .stream()
                .filter(ipngType -> StringUtils.equals(ipngType.getKey(), ipngTypeKey))
                .findAny()
                .orElseThrow(() -> new ResponseStatusException(INTERNAL_SERVER_ERROR, "Error retrieving IPNG type"))
        );
    }


    private void prepareAndSendIpngResponse(String tenantId, String deskId, Folder folder) {
        log.info("sending back folder to original sender through ipng");
        IpngProof proof = new IpngProof();
        String uuid = randomUUID().toString();
        proof.setId(this.ipngService.buildProofId(uuid, folder.getName().trim()));

        proof.setBusinessId(folder.getMetadata().get(METADATA_KEY_IPNG_RETURN_BUSINESS_ID));
        proof.setReceiverDeskboxId(folder.getMetadata().get(METADATA_KEY_IPNG_RETURN_DESKBOX_ID));
        proof.setSenderDeskboxId(folder.getMetadata().get(METADATA_KEY_IPNG_SOURCE_DESKBOX_ID));
        proof.setProofType(IpngProof.ProofType.RESPONSE);

        // TODO Maybe it is here that we should find the appropriate credentials to use with IPNG.
        String token = "";
        populateIpngResponseProofFromFolder(token, tenantId, folder, proof);

        try {
            this.sendIpngProof(token, folder, proof, proof.getId());
        } catch (NoSuchAlgorithmException | IOException e) {
            log.error("could not send proof " + proof + " back to ipng", e);
        }
    }


    private void triggerIpngPostAction(String tenantId, Desk desk, Folder folder) {
        folder = workflowService.getFolder(folder.getId(), tenantId, true);

        // We filter first on whether an IPNG metadata is present, as this should be both less frequent and faster to test
        if (folder.getMetadata().containsKey(METADATA_KEY_IPNG_RETURN_DESKBOX_ID)
                && folder.getMetadata().containsKey(METADATA_KEY_IPNG_RETURN_BUSINESS_ID)) {
            // calculate it is over

            List<Action> endActions = List.of(ARCHIVE, CHAIN, UNDO);
            List<Task> remainingNonEndSteps = folder.getStepList().stream()
                    .filter(t -> t.getState() == UPCOMING)
                    .filter(t -> !endActions.contains(t.getAction()))
                    .toList();

            if (remainingNonEndSteps.isEmpty()) {
                // send back trough IPNG
                prepareAndSendIpngResponse(tenantId, desk.getId(), folder);
            }
        }
    }


    public void handleExternalTasksRejection(Folder folder, Task task) {

        if (folder.getStepList().stream().map(Task::getAction).anyMatch(action -> action == SECURE_MAIL)) {
            secureMailService.rejectExternalProcedure(folder, task);
            task.getMetadata().put(METADATA_STATUS, "");
            task.getMetadata().put(METADATA_PASTELL_DOCUMENT_ID, "");
        }

        if (folder.getStepList().stream().map(Task::getAction).anyMatch(action -> action == EXTERNAL_SIGNATURE)) {
            externalSignatureService.rejectExternalProcedure(folder, task);
            task.getMetadata().put(METADATA_STATUS, "");
            task.getMetadata().put(METADATA_TRANSACTION_ID, "");
        }
    }

    public void cleanupAllSecureMailData(String tenantId, Folder folder) {
        Folder folderWithHistory = workflowService.getFolder(folder.getId(), tenantId, true);

        folderWithHistory.getStepList().stream()
                .filter(task -> task.getAction() != null)
                .filter(task -> task.getAction() == SECURE_MAIL)
                .forEach(task -> {
                    secureMailService.rejectExternalProcedure(folder, task);
                });
    }
}
