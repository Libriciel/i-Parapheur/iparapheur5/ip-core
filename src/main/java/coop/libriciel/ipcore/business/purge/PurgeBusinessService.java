/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.business.purge;

import coop.libriciel.ipcore.business.workflow.FolderBusinessService;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.services.database.TenantRepository;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.PaginatedList;
import jakarta.validation.Valid;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static org.springframework.data.domain.Pageable.unpaged;


@Log4j2
@Service
public class PurgeBusinessService {

    public static final int MAX_LOOP = 100;
    public static final int PURGE_PAGE_SIZE = 200;

    private final FolderBusinessService folderBusinessService;
    private final PurgeProperties properties;
    private final TenantRepository tenantRepository;
    private final WorkflowServiceInterface workflowService;


    @Autowired
    public PurgeBusinessService(FolderBusinessService folderBusinessService,
                                @Valid PurgeProperties properties,
                                TenantRepository tenantRepository,
                                WorkflowServiceInterface workflowService) {
        this.folderBusinessService = folderBusinessService;
        this.properties = properties;
        this.tenantRepository = tenantRepository;
        this.workflowService = workflowService;
    }


    @Scheduled(cron = "${services.business.purge.frequency:-}")
    public void purge() {
        log.info("Starting global purge of {} or more days old archived folders.", properties.getDaysBeforeDelete());

        // FIXME unpaged() here is a bad idea, we must paginate. 1000 seems a good max per page.
        Page<Tenant> tenantPage = this.tenantRepository.findAll(unpaged());

        tenantPage.stream().forEach(this::collectAndDeleteOldArchives);
    }


    private void collectAndDeleteOldArchives(Tenant tenant) {
        PaginatedList<? extends Folder> foldersToDelete = new PaginatedList<>(Collections.emptyList(), 1000, 1, () -> 1000);

        int nbLoop = 0;
        long purgeUpperLimitDate = LocalDateTime.now().minusDays(properties.getDaysBeforeDelete()).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        for (int pageIdx = 0; pageIdx < foldersToDelete.getTotal() / foldersToDelete.getPageSize() && nbLoop < MAX_LOOP; nbLoop++) {
            try {
                foldersToDelete = this.workflowService.getArchives(tenant.getId(), Pageable.ofSize(PURGE_PAGE_SIZE).withPage(pageIdx), purgeUpperLimitDate);

                log.info(
                        "Found {} old folders (on a total of {}) of {} or more days in the trashbin on tenant {}.",
                        foldersToDelete.getData().size(),
                        foldersToDelete.getTotal(),
                        properties.getDaysBeforeDelete(),
                        tenant.getId()
                );

                deleteFolders(tenant.getId(), foldersToDelete.getData());
            } catch (Exception e) {
                log.error("An error occurred during the fetching of old trashbin folders for purge, on tenant '{}' : {}", tenant.getId(), e.getMessage());
                log.debug("Error details : ", e);
            }
        }
    }


    private void deleteFolders(@NotNull String tenantId, List<? extends Folder> foldersToDelete) {
        foldersToDelete.forEach(folder -> {
            try {
                folderBusinessService.deleteFolder(tenantId, folder, true);
            } catch (Exception e) {
                log.error("An error occurred during the purge of the folder {}, error detail : {}", folder.getId(), e.getMessage());
                log.debug("Error details : ", e);
            }
        });
    }

}
