/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.business.purge;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.validation.annotation.Validated;


@Data
@Log4j2
@Validated
@AllArgsConstructor
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "services.business.purge")
public class PurgeProperties {

    // TODO: Add a Validator here, somehow,
    //  and throw an error if it is a voluntary broken frequency,
    //  or a frequency that does not trigger at least once a... month ?
    @NotBlank
    private String frequency;

    @Min(value = 7, message = "Cannot have less than 7 days before a purge")
    @Max(value = 365, message = "Cannot have more than 365 days before a purge")
    private int daysBeforeDelete;

}
