/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.business.template;

import coop.libriciel.crypto.api.model.DataToSign;
import coop.libriciel.crypto.api.model.PadesParameters;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.crypto.DocumentDataToSignHolder;
import coop.libriciel.ipcore.model.database.TemplateType;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.templates.TemplateInfo;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.crypto.CryptoServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.RequestUtils;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.MapUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.stereotype.Service;
import org.yaml.snakeyaml.Yaml;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.text.DateFormat;
import java.util.*;

import static coop.libriciel.crypto.api.model.CertificationPermission.CHANGES_PERMITTED;
import static coop.libriciel.crypto.api.model.DigestAlgorithm.SHA256;
import static coop.libriciel.ipcore.configuration.CacheConfig.TEMPLATES_CACHE_KEY;
import static coop.libriciel.ipcore.configuration.CacheConfig.TEMPLATES_DIMENSIONS_CACHE_KEY_GENERATOR_KEY;
import static coop.libriciel.ipcore.model.database.TemplateType.*;
import static coop.libriciel.ipcore.services.content.ContentServiceInterface.FILE_TRANSFER_BUFFER_SIZE;
import static coop.libriciel.ipcore.utils.CryptoUtils.*;
import static coop.libriciel.ipcore.utils.FileUtils.*;
import static java.util.Collections.emptyList;
import static java.util.List.of;
import static org.apache.commons.collections4.MapUtils.getObject;
import static org.springframework.core.io.buffer.DataBufferUtils.readInputStream;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;


@Log4j2
@Service
public class TemplateBusinessService {

    public static char[] DUMMY_SEAL_CERTIFICATE_PASSWORD = "dummySealCertificate".toCharArray();
    public static String DUMMY_SEAL_CERTIFICATE_ALIAS = "prenom nom - exemple";

    private @Value(MAIL_TEMPLATE_PATH + "mail_notification_single.ftl") Resource mailNotificationSingleTemplateResource;
    private @Value(MAIL_TEMPLATE_PATH + "mail_notification_digest.ftl") Resource mailNotificationDigestTemplateResource;
    private @Value(MAIL_TEMPLATE_PATH + "mail_action_send.ftl") Resource mailActionSendTemplateResource;
    private @Value(MAIL_TEMPLATE_PATH + "docket.ftl") Resource docketTemplateResource;

    private @Value(TEMPLATE_PATH + "A4_1_page.pdf") Resource a4BlankPdfResource;

    private @Value(SIGNATURE_TEMPLATE_PATH + "dummy_seal_certificate.p12") Resource dummySealCertificateResource;

    private @Value(SIGNATURE_TEMPLATE_PATH + "default_small_signature_template.yml") Resource defaultSmallSignatureTemplateResource;
    private @Value(SIGNATURE_TEMPLATE_PATH + "default_medium_signature_template.yml") Resource defaultMediumSignatureTemplateResource;
    private @Value(SIGNATURE_TEMPLATE_PATH + "default_large_signature_template.yml") Resource defaultLargeSignatureTemplateResource;
    private @Value(SIGNATURE_TEMPLATE_PATH + "default_alternate_signature_1_template.yml") Resource defaultAlternateSignature1TemplateResource;
    private @Value(SIGNATURE_TEMPLATE_PATH + "default_alternate_signature_2_template.yml") Resource defaultAlternateSignature2TemplateResource;
    private @Value(SIGNATURE_TEMPLATE_PATH + "default_automatic_seal_template.yml") Resource defaultAutomaticSealTemplateResource;
    private @Value(SIGNATURE_TEMPLATE_PATH + "default_alternate_seal_template.yml") Resource defaultAlternateSealTemplateResource;

    public static final List<TemplateType> SEAL_AND_SIGNATURE_TEMPLATES = of(
            SEAL_AUTOMATIC,
            SEAL_MEDIUM,
            SEAL_LARGE,
            SEAL_ALTERNATE,
            SIGNATURE_SMALL,
            SIGNATURE_MEDIUM,
            SIGNATURE_LARGE,
            SIGNATURE_ALTERNATE_1,
            SIGNATURE_ALTERNATE_2
    );

    private final CryptoServiceInterface cryptoService;
    private final ContentServiceInterface contentService;
    private final ModelMapper modelMapper;


    public TemplateBusinessService(CryptoServiceInterface cryptoService, ContentServiceInterface contentService, ModelMapper modelMapper) {
        this.cryptoService = cryptoService;
        this.contentService = contentService;
        this.modelMapper = modelMapper;
    }


    public DocumentBuffer fakeSeal(@NotNull Resource templateResource, @NotNull String signatureImageBase64, @Nullable TemplateInfo templateInfo) {

        // Load the dummy seal certificate

        String publicCertBase64;
        PrivateKey privateKey;
        try {
            KeyStore keystore = KeyStore.getInstance("PKCS12");
            keystore.load(dummySealCertificateResource.getInputStream(), DUMMY_SEAL_CERTIFICATE_PASSWORD);
            Certificate cert = keystore.getCertificate(DUMMY_SEAL_CERTIFICATE_ALIAS);
            publicCertBase64 = Base64.getEncoder().encodeToString(cert.getEncoded());
            privateKey = (PrivateKey) keystore.getKey(DUMMY_SEAL_CERTIFICATE_ALIAS, DUMMY_SEAL_CERTIFICATE_PASSWORD);
        } catch (KeyStoreException | CertificateException | IOException | NoSuchAlgorithmException | UnrecoverableKeyException e) {
            log.error("Error reading the dummy seal certificate", e);
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.error_reading_dummy_seal_certificate");
        }

        // Build the dummy signature params

        long signatureDate = new Date().getTime();

        Map<String, String> dummyMapping = new HashMap<>();
        dummyMapping.put(SIGNATURE_PLACEHOLDER_SIGNATURE_IMAGE_BASE64, signatureImageBase64);
        dummyMapping.put(SIGNATURE_PLACEHOLDER_USER_NAME, "Nom Prénom");
        dummyMapping.put(SIGNATURE_PLACEHOLDER_DESK_NAME, "Nom du bureau");
        dummyMapping.put(SIGNATURE_PLACEHOLDER_DESK_SHORT_NAME, "Nom court du bureau");
        dummyMapping.put(SIGNATURE_PLACEHOLDER_TENANT_NAME, "Nom de l'entité");
        dummyMapping.put(SIGNATURE_PLACEHOLDER_SIGNATURE_DATE, DateFormat.getDateInstance(DateFormat.MEDIUM).format(signatureDate));
        dummyMapping.put(SIGNATURE_PLACEHOLDER_SIGNATURE_TIME, DateFormat.getTimeInstance(DateFormat.MEDIUM).format(signatureDate));
        dummyMapping.put(SIGNATURE_PLACEHOLDER_CUSTOM_SIGNATURE_FIELD, "Champ de signature personnalisé");
        dummyMapping.put(SIGNATURE_PLACEHOLDER_DELEGATION_FROM, "En l'absence de Bureau absent");
        dummyMapping.put(SIGNATURE_PLACEHOLDER_COUNTRY_NAME, "Pays");
        dummyMapping.put(SIGNATURE_PLACEHOLDER_CITY_NAME, "Ville");
        dummyMapping.put(SIGNATURE_PLACEHOLDER_ZIPCODE_NAME, "code postal");

        PadesParameters params = new PadesParameters();
        params.setDataToSignList(new ArrayList<>());
        params.setPublicCertificateBase64(publicCertBase64);
        params.setSignatureDateTime(signatureDate);
        params.setDigestAlgorithm(SHA256);
        params.setCertificationPermission(CHANGES_PERMITTED);
        params.setCity("Ville");
        params.setCountry("Pays");
        params.setZipCode("code postal");
        params.setStamp(cryptoService.getPdfSignatureStamp(dummyMapping, templateResource));

        // Sign a dummy PDF

        DocumentBuffer documentBuffer = new DocumentBuffer();
        documentBuffer.setContentFlux(readInputStream(() -> openDummyPdf(templateInfo), new DefaultDataBufferFactory(), FILE_TRANSFER_BUFFER_SIZE));

        DocumentDataToSignHolder documentDataToSignHolder = cryptoService.getDataToSign(documentBuffer, params);

        if (documentDataToSignHolder.getDataToSignList() == null) {
            log.error("Null data to sign list on fake seal.");
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.empty_data_to_sign_list");
        }

        for (DataToSign dataToSign : documentDataToSignHolder.getDataToSignList()) {
            String stringToSignBase64 = dataToSign.getDataToSignBase64();
            byte[] bytesToSign = Base64.getDecoder().decode(stringToSignBase64);
            dataToSign.setSignatureValue(sha256Sign(bytesToSign, privateKey));
        }
        params.setDataToSignList(documentDataToSignHolder.getDataToSignList());

        if (templateInfo != null && params.getStamp() != null) {
            params.getStamp().setX(0f);
            params.getStamp().setY(0f);
        }

        return cryptoService.signDocument(documentBuffer, params);
    }


    /**
     * This can hardly be done in a single line without opening both.
     * A dedicated method is the cleanest way to go.
     *
     * @param templateInfo contains the expected dimensions, will use an A4 if null
     * @return the opened PDF resource stream
     */
    private InputStream openDummyPdf(@Nullable TemplateInfo templateInfo) throws IOException {

        if (templateInfo == null) {
            return a4BlankPdfResource.getInputStream();
        }

        try (PDDocument document = new PDDocument();
             ByteArrayOutputStream out = new ByteArrayOutputStream()) {

            document.setVersion(1.6f);
            PDPage page = new PDPage();
            page.setMediaBox(new PDRectangle(templateInfo.getWidth(), templateInfo.getHeight()));
            page.setCropBox(new PDRectangle(templateInfo.getWidth(), templateInfo.getHeight()));
            document.addPage(page);
            document.save(out);
            return new ByteArrayInputStream(out.toByteArray());
        } catch (Exception e) {
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.could_not_read_template_content", e);
        }
    }


    public Resource getDefaultTemplateResource(TemplateType templateType) {
        return switch (templateType) {
            case MAIL_NOTIFICATION_SINGLE -> mailNotificationSingleTemplateResource;
            case MAIL_NOTIFICATION_DIGEST -> mailNotificationDigestTemplateResource;
            case MAIL_ACTION_SEND -> mailActionSendTemplateResource;
            case DOCKET -> docketTemplateResource;
            case SIGNATURE_SMALL -> defaultSmallSignatureTemplateResource;
            case SIGNATURE_MEDIUM, SEAL_MEDIUM -> defaultMediumSignatureTemplateResource;
            case SIGNATURE_LARGE, SEAL_LARGE -> defaultLargeSignatureTemplateResource;
            case SIGNATURE_ALTERNATE_1 -> defaultAlternateSignature1TemplateResource;
            case SIGNATURE_ALTERNATE_2 -> defaultAlternateSignature2TemplateResource;
            case SEAL_AUTOMATIC -> defaultAutomaticSealTemplateResource;
            case SEAL_ALTERNATE -> defaultAlternateSealTemplateResource;
        };
    }


    public @Nullable DocumentBuffer getCustomTemplate(Tenant tenant, TemplateType templateType) {
        Map<TemplateType, String> templates = tenant.getCustomTemplates();
        if (MapUtils.isNotEmpty(templates) && templates.containsKey(templateType)) {
            return contentService.getCustomTemplate(tenant, templateType);
        }
        return null;
    }


    public @Nullable Resource getCustomTemplateResource(Tenant tenant, TemplateType templateType) {
        DocumentBuffer customTemplateDocumentBuffer = getCustomTemplate(tenant, templateType);
        if (customTemplateDocumentBuffer != null) {
            try {
                return new InputStreamResource(RequestUtils.bufferToInputStream(customTemplateDocumentBuffer));
            } catch (IOException e) {
                log.error("Error while creating an InputStreamResource from a custom template.");
            }
        }
        return null;
    }


    public @NotNull Resource getTemplateResource(@NotNull Tenant tenant, @NotNull TemplateType templateType) {
        return Optional.ofNullable(getCustomTemplateResource(tenant, templateType))
                .orElseGet(() -> getDefaultTemplateResource(templateType));
    }


    @Cacheable(value = TEMPLATES_CACHE_KEY, keyGenerator = TEMPLATES_DIMENSIONS_CACHE_KEY_GENERATOR_KEY)
    public TemplateInfo getTemplateInfo(String tenantId, Tenant tenant, TemplateType templateType) {
        log.debug("Get template info. Tenant : {}, template type : {}", tenantId, templateType);

        try (InputStream templateInputStream = this.getTemplateResource(tenant, templateType).getInputStream()) {
            Map<String, Object> template = new Yaml().load(templateInputStream);
            TemplateInfo templateInfo = modelMapper.map(template, TemplateInfo.class);

            List<Map<String, Object>> elements = (List<Map<String, Object>>) getObject(template, "elements", emptyList());
            elements.stream()
                    .filter(value -> value.toString().contains(SIGNATURE_PLACEHOLDER_CUSTOM_SIGNATURE_FIELD))
                    .findFirst()
                    .ifPresent(value -> templateInfo.setContainsCustomSignatureField(true));

            return templateInfo;
        } catch (Exception e) {
            log.error("An error occurred when retrieving the current active template of type {} on tenant {}", templateType.name(), tenant.getName());
            log.debug("Error detail : ", e);
            throw new LocalizedStatusException(INTERNAL_SERVER_ERROR, "message.missing_template");
        }
    }

}
