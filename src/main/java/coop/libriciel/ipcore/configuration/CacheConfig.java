/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.configuration;

import coop.libriciel.ipcore.model.database.TemplateType;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.lang.reflect.Method;


@Log4j2
@Configuration
@EnableCaching
public class CacheConfig {

    public static final String TEMPLATES_CACHE_KEY = "templates";
    public static final String TEMPLATES_DIMENSIONS_CACHE_KEY_GENERATOR_KEY = "TemplateDimensionsCacheKeyGenerator";


    @Bean
    public CacheManager cacheManager() {
        return new ConcurrentMapCacheManager(TEMPLATES_CACHE_KEY);
    }


    @Bean(TEMPLATES_DIMENSIONS_CACHE_KEY_GENERATOR_KEY)
    public KeyGenerator keyGenerator() {
        return new TemplateDimensionsCacheKeyGenerator();
    }


    public static class TemplateDimensionsCacheKeyGenerator implements KeyGenerator {

        /**
         * @param target the target instance | unused
         * @param method the method being called | unused
         * Warning : this method shall only be used to store template dimensions of methods using following arguments:
         * [0] : tenantId
         * [2] : template Type
         * @param params the method parameters. An object array that contains every argument used in the called method.
         * ex. if we call the method :
         * ----------------------------------------------------------------------------------------------------------------
         * getTemplateDimensions(@Parameter(description = Tenant.API_DOC_ID_VALUE)
         *                       @PathVariable(name = Tenant.API_PATH) String tenantId,
         *                       @TenantResolved Tenant tenant,
         *                       @PathVariable TemplateType templateType)
         * ----------------------------------------------------------------------------------------------------------------
         * "params" will contain [tenantId, tenant, templateType]
         * In this case, we will parse the first parameter as the "tenantId" and the last as the "templateType".
         * If the parsing fails, we will throw an exception since it means the parameters are not the one we expected.
         * @return the key used to store cache.
         */
        public @NotNull Object generate(@NotNull Object target, @NotNull Method method, Object @NotNull ... params) {
            try {
                String tenantKey = (String) params[0];
                TemplateType templateType = (TemplateType) params[2];
                return "%s_size_%s_%s".formatted(TEMPLATES_CACHE_KEY, tenantKey, templateType.toString().toLowerCase());
            } catch (Exception e) {
                throw new RuntimeException("Could not generate cache key", e);
            }
        }
    }
}
