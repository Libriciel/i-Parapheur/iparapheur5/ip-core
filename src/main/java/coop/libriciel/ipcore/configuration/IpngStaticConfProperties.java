/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.configuration;


import coop.libriciel.ipcore.services.ipng.IpngBindingsInterface;
import lombok.Data;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Map;


@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "ipng")
@ConditionalOnProperty(name = IpngBindingsInterface.PREFERENCES_PROVIDER_KEY, havingValue = "static")
@Data
public class IpngStaticConfProperties {

    private String lastRedisMessage = "0";


    @Data
    public static class TenantConfiguration {

        Map<String, String> deskToDeskbox = Collections.emptyMap();
        Map<String, String> innerTypesToOutgoingIpngTypes = Collections.emptyMap();
        Map<String, String> incomingIpngTypesToInnerTypes = Collections.emptyMap();
    }


    Map<String, String> tenantToEntities = Collections.emptyMap();
    Map<String, TenantConfiguration> tenantConfigurations = Collections.emptyMap();


}
