/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.configuration;

import io.swagger.v3.core.converter.AnnotatedType;
import io.swagger.v3.core.converter.ModelConverter;
import io.swagger.v3.core.converter.ModelConverterContext;
import io.swagger.v3.oas.models.media.Schema;
import org.springdoc.core.converters.SchemaPropertyDeprecatingConverter;
import org.springframework.stereotype.Component;

import java.util.Iterator;

import static java.util.Collections.singletonList;


/**
 * This configuration fixes a bug on the OpenAPI generation.
 * The blog post linked down here says it all.
 * <p>
 * TLDR: If we set a {@link Deprecated} annotation on an enum field, it deprecates the entire enum.
 * It seems to be a known bug, and the developer is not keen to fix it whatsoever.
 * The following class is a workaround.
 * <p>
 * TODO: Remove this in 5.4.0+,
 *  once the UserRepresentation's deprecated fields are set to be deleted.
 *  (Check for deprecated non-primitive fields before cleaning it)
 *
 * @see <a href="https://codesoapbox.dev/how-to-fix-springdoc-openapi-ignoring-deprecated-on-non-primitive-fields">The source blog post</a>}
 */
@Component
public class CustomSchemaPropertyDeprecatingConverter extends SchemaPropertyDeprecatingConverter {


    @Override
    public Schema<?> resolve(AnnotatedType type, ModelConverterContext context, Iterator<ModelConverter> chain) {
        if (chain.hasNext()) {
            Schema<?> resolvedSchema = chain.next().resolve(type, context, chain);
            if (type.isSchemaProperty() && containsDeprecatedAnnotation(type.getCtxAnnotations())) {
                if (resolvedSchema.get$ref() != null) {
                    // Sibling values alongside $ref are ignored in OpenAPI versions lower than 3.1. See:
                    // https://swagger.io/docs/specification/using-ref/#sibling
                    // To add properties to a $ref, it must be wrapped in allOf.
                    resolvedSchema = wrapInAllOf(resolvedSchema);
                }
                resolvedSchema.setDeprecated(true);
            }
            return resolvedSchema;
        }
        return null;
    }


    private Schema<?> wrapInAllOf(Schema<?> resolvedSchema) {
        Schema<?> wrapperSchema = new Schema<>();
        wrapperSchema.allOf(singletonList(resolvedSchema));
        return wrapperSchema;
    }


}
