/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
package coop.libriciel.ipcore.configuration;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import java.util.Properties;
import java.util.stream.Stream;


@Log4j2
@Configuration
public class SchedulerConfigurer {

    private static final String[] QUARTZ_PROPERTIES = new String[]{
            "org.quartz.jobStore.class", "org.quartz.jobStore.tablePrefix", "org.quartz.jobStore.driverDelegateClass",
            "org.quartz.jobStore.dataSource", "org.quartz.dataSource.quartzDS.driver", "org.quartz.dataSource.quartzDS.URL",
            "org.quartz.dataSource.quartzDS.user", "org.quartz.dataSource.quartzDS.password", "org.quartz.threadPool.class",
            "org.quartz.scheduler.skipUpdateCheck", "org.quartz.scheduler.idleWaitTime", "org.quartz.threadPool.threadCount",
            "org.quartz.scheduler.instanceId", "org.quartz.jobStore.misfireThreshold", "org.quartz.scheduler.instanceName",
            "org.quartz.dataSource.quartzDS.maxConnections"
    };

    private final ApplicationContext applicationContext;


    @Autowired
    public SchedulerConfigurer(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }


    @Bean
    public SchedulerFactoryBean schedulerFactoryBean() {
        log.info("Initializing Quartz.......");

        SchedulerJobFactory jobFactory = new SchedulerJobFactory();
        jobFactory.setApplicationContext(applicationContext);

        SchedulerFactoryBean schedulerFactory = new SchedulerFactoryBean();
        schedulerFactory.setApplicationContext(applicationContext);
        schedulerFactory.setQuartzProperties(quartzProperties());
        schedulerFactory.setWaitForJobsToCompleteOnShutdown(true);
        schedulerFactory.setJobFactory(jobFactory);

        return schedulerFactory;
    }


    @Bean
    public Properties quartzProperties() {

        Environment env = applicationContext.getEnvironment();
        String prefix = "spring.quartz.properties.";

        Properties props = new Properties();
        Stream.of(QUARTZ_PROPERTIES)
                .filter(k -> env.containsProperty(prefix + k))
                .forEach(key -> {
                    log.debug("Quartz property '{}: {}'", key, env.getProperty(prefix + key));
                    props.setProperty(key, env.getProperty(prefix + key));
                });

        return props;
    }


}
