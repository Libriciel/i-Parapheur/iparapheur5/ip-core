/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.configuration;

import coop.libriciel.ipcore.business.signatureproof.SignatureReportBusinessListenerService;
import coop.libriciel.ipcore.business.workflow.WorkflowBusinessService;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.database.SubtypeRepository;
import coop.libriciel.ipcore.services.externalsignature.ExternalSignatureInterface;
import coop.libriciel.ipcore.services.ipng.IpngServiceInterface;
import coop.libriciel.ipcore.services.mail.NotificationServiceInterface;
import coop.libriciel.ipcore.services.mail.PostponedNotificationCacheRepository;
import coop.libriciel.ipcore.services.redis.RedisMessageListenerService;
import coop.libriciel.ipcore.services.securemail.SecureMailServiceInterface;
import coop.libriciel.ipcore.services.websocket.WebsocketService;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import io.lettuce.core.RedisClient;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisStreamCommands;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.transaction.PlatformTransactionManager;

import static coop.libriciel.ipcore.utils.TextUtils.INTERNAL_PREFIX;


@Log4j2
@Configuration
@EnableRedisRepositories(basePackageClasses = {PostponedNotificationCacheRepository.class})
@ConditionalOnProperty(name = "spring.cache.type", havingValue = "REDIS")
public class RedisConfigurer {


    public static final String TASK_HOOK_QUEUE = INTERNAL_PREFIX + "task_hook_trigger";

    public static final String EXTERNAL_SIGNATURE_CONNECTOR_NOTIFICATION_QUEUE = INTERNAL_PREFIX + "external_signature_connector_notification_queue";
    public static final String PASTELL_CONNECTOR_NOTIFICATION_QUEUE = INTERNAL_PREFIX + "pastell_connector_notification_queue";
    public static final String SIGNATURE_PROOF_GENERATION_QUEUE = INTERNAL_PREFIX + "signature_proof_generation_queue";

    public static final String TENANT_ID_KEY = "TenantId";
    public static final String FOLDER_ID_KEY = "FolderId";


    private @Value("${spring.data.redis.host}") String hostName;


    // <editor-fold desc="Beans">


    private final AuthServiceInterface authService;
    private final ContentServiceInterface contentService;
    private final ExternalSignatureInterface externalSignatureService;
    private final PlatformTransactionManager platformTransactionManager;
    private final SecureMailServiceInterface secureMailService;
    private final SignatureReportBusinessListenerService signatureReportBusinessListenerService;
    private final WebsocketService websocketService;
    private final WorkflowBusinessService workflowBusinessService;


    @Autowired
    public RedisConfigurer(AuthServiceInterface authService,
                           ContentServiceInterface contentService,
                           ExternalSignatureInterface externalSignatureService,
                           PlatformTransactionManager platformTransactionManager,
                           SecureMailServiceInterface secureMailService,
                           SignatureReportBusinessListenerService signatureReportBusinessListenerService,
                           WebsocketService websocketService,
                           WorkflowBusinessService workflowBusinessService) {
        this.authService = authService;
        this.contentService = contentService;
        this.externalSignatureService = externalSignatureService;
        this.platformTransactionManager = platformTransactionManager;
        this.secureMailService = secureMailService;
        this.signatureReportBusinessListenerService = signatureReportBusinessListenerService;
        this.websocketService = websocketService;
        this.workflowBusinessService = workflowBusinessService;
    }


    // </editor-fold desc="Beans">


    @Bean
    LettuceConnectionFactory redisConnectionFactory() {
        RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration();

        redisStandaloneConfiguration.setHostName(this.hostName);

        return new LettuceConnectionFactory(redisStandaloneConfiguration);
    }


    @Bean
    public RedisTemplate<Object, Object> redisTemplate() {
        RedisTemplate<Object, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(redisConnectionFactory());
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        return redisTemplate;
    }


    @Bean
    RedisMessageListenerContainer redisContainer(RedisConnectionFactory redisConnectionFactory,
                                                 @Autowired NotificationServiceInterface notificationController,
                                                 @Autowired SubtypeRepository subtypeRepository,
                                                 @Autowired WorkflowServiceInterface workflowService) {

        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(redisConnectionFactory);

        container.addMessageListener(
                new MessageListenerAdapter(
                        new RedisMessageListenerService(
                                authService,
                                contentService,
                                notificationController,
                                platformTransactionManager,
                                subtypeRepository,
                                websocketService,
                                workflowBusinessService,
                                workflowService
                        )
                ),
                new ChannelTopic(TASK_HOOK_QUEUE)
        );

        container.addMessageListener(
                new MessageListenerAdapter(externalSignatureService),
                new ChannelTopic(EXTERNAL_SIGNATURE_CONNECTOR_NOTIFICATION_QUEUE)
        );

        container.addMessageListener(
                new MessageListenerAdapter(secureMailService),
                new ChannelTopic(PASTELL_CONNECTOR_NOTIFICATION_QUEUE)
        );

        container.addMessageListener(
                new MessageListenerAdapter(signatureReportBusinessListenerService),
                new ChannelTopic(SIGNATURE_PROOF_GENERATION_QUEUE)
        );

        return container;
    }


    @Bean
    @ConditionalOnProperty(name = IpngServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = "ipng")
    RedisStreamCommands<String, String> redisSyncCommand() {
        RedisClient client = RedisClient.create("redis://" + this.hostName);
        StatefulRedisConnection<String, String> connection = client.connect();
        return connection.sync();
    }

}
