/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.configuration;


import coop.libriciel.ipcore.services.auth.AuthServiceProperties;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;

import static org.springframework.messaging.simp.stomp.StompCommand.CONNECT;
import static org.springframework.security.config.Elements.HTTP;


@Log4j2
@Configuration
@EnableWebSocket
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    private final AuthServiceProperties authServiceProperties;
    private final GlobalApplicationProperties applicationProperties;


    public WebSocketConfig(AuthServiceProperties authServiceProperties, GlobalApplicationProperties applicationProperties) {
        this.authServiceProperties = authServiceProperties;
        this.applicationProperties = applicationProperties;
    }


    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.setApplicationDestinationPrefixes("");
        registry.enableSimpleBroker("/websocket");
    }


    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        String protocol = applicationProperties.getProtocol();
        String host = applicationProperties.getHost();
        String allowedOriginPattern = protocol + "://" + host + ":[*]";

        registry.addEndpoint("/websocket")
                .setAllowedOriginPatterns(allowedOriginPattern);
        registry.addEndpoint("/websocket")
                .setAllowedOriginPatterns(allowedOriginPattern)
                .withSockJS();
    }


    @Override
    public void configureClientInboundChannel(ChannelRegistration registration) {
        registration.interceptors(new ChannelInterceptor() {
            @Override
            public Message<?> preSend(@NotNull Message<?> message, @NotNull MessageChannel channel) {
                StompHeaderAccessor accessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);

                if (accessor == null || accessor.getCommand() == null || !accessor.getCommand().equals(CONNECT)) {
                    log.trace("intercept client inbound message (websocket) - not a connect message");
                    return message;
                }

                LinkedMultiValueMap<String, String> headers = (LinkedMultiValueMap<String, String>) message.getHeaders().get("nativeHeaders");

                if (headers == null || !headers.containsKey("Authorization")) {
                    log.error("intercept client inbound message (websocket) - missing auth header in connect message");
                    throw new LocalizedStatusException(HttpStatus.UNAUTHORIZED, "message.websocket_handshake_no_authorization_header_found");
                }

                List<?> authHeaders = headers.get("Authorization");

                if (authHeaders == null) {
                    log.error("intercept client inbound message (websocket) - bad auth header in connect message");
                    throw new LocalizedStatusException(HttpStatus.UNAUTHORIZED, "message.websocket_handshake_empty_authorization_header");
                }

                // TODO check JWT manually
                URI requestUri = UriComponentsBuilder.newInstance()
                        .scheme(HTTP)
                        .host(authServiceProperties.getHost())
                        .port(authServiceProperties.getPort())
                        .path("/auth/realms/" + authServiceProperties.getRealm() + "/protocol/openid-connect/userinfo")
                        .build()
                        .normalize()
                        .toUri();

                WebClient.builder()
                        .build()
                        .get()
                        .uri(requestUri)
                        .header(HttpHeaders.AUTHORIZATION, "" + authHeaders.get(0))
                        .retrieve()
                        .bodyToMono(String.class)
                        .blockOptional()
                        .orElseThrow(() -> {
                            log.error("intercept client inbound message (websocket) - authentication failed");
                            return new LocalizedStatusException(HttpStatus.FORBIDDEN, "message.websocket_handshake_error_while_verifying_user_token");
                        });

                return message;
            }
        });
    }
}
