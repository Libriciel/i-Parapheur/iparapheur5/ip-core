/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.configuration;

import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.database.*;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.services.resolvers.*;
import coop.libriciel.ipcore.services.secret.SecretServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;


@Configuration
@EnableSpringDataWebSupport
public class WebConfig implements WebMvcConfigurer {


    // <editor-fold desc="Beans">

    private final AuthServiceInterface authService;
    private final ExternalSignatureConfigRepository externalSignatureConfigRepository;
    private final LayerRepository layerRepository;
    private final MetadataRepository metadataRepository;
    private final PermissionServiceInterface permissionService;
    private final SecretServiceInterface secretService;
    private final StampRepository stampRepository;
    private final SubtypeRepository subtypeRepository;
    private final TenantRepository tenantRepository;
    private final TypeRepository typeRepository;
    private final WorkflowServiceInterface workflowService;


    @Autowired
    public WebConfig(AuthServiceInterface authService,
                     ExternalSignatureConfigRepository externalSignatureConfigRepository,
                     LayerRepository layerRepository,
                     MetadataRepository metadataRepository,
                     PermissionServiceInterface permissionService,
                     SecretServiceInterface secretService,
                     StampRepository stampRepository,
                     SubtypeRepository subtypeRepository,
                     TenantRepository tenantRepository,
                     TypeRepository typeRepository,
                     WorkflowServiceInterface workflowService) {
        this.authService = authService;
        this.externalSignatureConfigRepository = externalSignatureConfigRepository;
        this.layerRepository = layerRepository;
        this.metadataRepository = metadataRepository;
        this.permissionService = permissionService;
        this.secretService = secretService;
        this.stampRepository = stampRepository;
        this.subtypeRepository = subtypeRepository;
        this.tenantRepository = tenantRepository;
        this.typeRepository = typeRepository;
        this.workflowService = workflowService;
    }


    // </editor-fold desc="Beans">


    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(new CurrentUserResolver(authService));
        argumentResolvers.add(new ExternalSignatureConfigResolver(externalSignatureConfigRepository));
        argumentResolvers.add(new DelegationResolver(permissionService));
        argumentResolvers.add(new DeskResolver(authService));
        argumentResolvers.add(new TaskResolver(workflowService));
        argumentResolvers.add(new FolderResolver(permissionService, workflowService));
        argumentResolvers.add(new TenantResolver(tenantRepository));
        argumentResolvers.add(new TypeResolver(typeRepository));
        argumentResolvers.add(new SubtypeResolver(subtypeRepository));
        argumentResolvers.add(new LayerResolver(layerRepository));
        argumentResolvers.add(new StampResolver(stampRepository));
        argumentResolvers.add(new SealCertificateResolver(secretService));
        argumentResolvers.add(new MetadataResolver(metadataRepository));
        argumentResolvers.add(new UserResolver(authService));
        argumentResolvers.add(new WorkflowDefinitionResolver(workflowService));
    }


}
