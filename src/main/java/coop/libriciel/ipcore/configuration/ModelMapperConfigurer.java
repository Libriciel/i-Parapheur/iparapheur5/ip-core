/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.configuration;

import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.auth.requests.DeskDto;
import coop.libriciel.ipcore.model.auth.requests.UserRepresentation;
import coop.libriciel.ipcore.model.crypto.CertificateInformations;
import coop.libriciel.ipcore.model.crypto.SealCertificate;
import coop.libriciel.ipcore.model.crypto.SealCertificateDto;
import coop.libriciel.ipcore.model.database.*;
import coop.libriciel.ipcore.model.database.requests.SubtypeDto;
import coop.libriciel.ipcore.model.database.requests.SubtypeLayerDto;
import coop.libriciel.ipcore.model.database.requests.SubtypeMetadataDto;
import coop.libriciel.ipcore.model.database.userPreferences.*;
import coop.libriciel.ipcore.model.pdfstamp.Layer;
import coop.libriciel.ipcore.model.permission.Delegation;
import coop.libriciel.ipcore.model.permission.DelegationDto;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.FolderDto;
import coop.libriciel.ipcore.model.workflow.StepDefinition;
import coop.libriciel.ipcore.model.workflow.StepDefinitionDto;
import coop.libriciel.ipcore.services.content.ContentServiceProperties;
import coop.libriciel.ipcore.utils.CryptoUtils;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.jetbrains.annotations.NotNull;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Optional;

import static coop.libriciel.ipcore.utils.CollectionUtils.toMutableList;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static org.modelmapper.convention.MatchingStrategies.STRICT;


/**
 * DTO schemas should have id lists in write-mode, and representation-objects in read-mode.
 * So the DTOs have both variables, one for the input, one for the output.
 * <p>
 * We shall never use the DTOs in the code. Those objects are made for data transfer only.
 * They contain 2 similar lists, an de-sync between the 2 would be bad.
 * <p>
 * We have to use the real objects, with single lists.
 * <p>
 * These converters automatically map ids to (dummy, un-named) objects with the right id set.
 * It is often enough to request sub-elements.
 */
@Configuration
public class ModelMapperConfigurer {


    private static final Converter<List<DeskRepresentation>, List<String>> DESK_REPRESENTATION_TO_ID_LIST_CONVERTER =
            context -> Optional.ofNullable(context.getSource())
                    .orElse(emptyList())
                    .stream()
                    .map(DeskRepresentation::getId)
                    .toList();

    private static final Converter<List<String>, List<DeskRepresentation>> ID_TO_DESK_REPRESENTATION_LIST_CONVERTER =
            context -> Optional.ofNullable(context.getSource())
                    .orElse(emptyList())
                    .stream()
                    .map(i -> new DeskRepresentation(i, null))
                    .collect(toMutableList());

    public static final Converter<List<String>, List<DeskRepresentation>> ID_TO_DESK_REPRESENTATION_NULLABLE_LIST_CONVERTER =
            context -> Optional.ofNullable(context.getSource())
                    .map(source -> source.stream().map(DeskRepresentation::new).collect(toMutableList()))
                    .orElse(null);

    public static final Converter<List<DeskRepresentation>, List<String>> DESK_REPRESENTATION_TO_ID_NULLABLE_LIST_CONVERTER =
            context -> Optional.ofNullable(context.getSource())
                    .map(source -> source.stream().map(DeskRepresentation::getId).collect(toMutableList()))
                    .orElse(null);

    public static final Converter<List<String>, List<Metadata>> ID_TO_METADATA_NULLABLE_LIST_CONVERTER =
            context -> Optional.ofNullable(context.getSource())
                    .map(source -> source.stream().map(Metadata::new).collect(toMutableList()))
                    .orElse(null);

    public static final Converter<List<Metadata>, List<String>> METADATA_TO_ID_NULLABLE_LIST_CONVERTER =
            context -> Optional.ofNullable(context.getSource())
                    .map(source -> source.stream().map(Metadata::getId).collect(toMutableList()))
                    .orElse(null);

    public static final Converter<DeskRepresentation, String> DESK_REPRESENTATION_TO_ID_CONVERTER =
            context -> Optional.ofNullable(context.getSource())
                    .map(DeskRepresentation::getId)
                    .orElse(null);

    public static final Converter<String, DeskRepresentation> ID_TO_DESK_REPRESENTATION_CONVERTER =
            context -> Optional.ofNullable(context.getSource())
                    .filter(StringUtils::isNotEmpty)
                    .map(i -> new DeskRepresentation(i, null))
                    .orElse(null);

    private static final Converter<ExternalSignatureConfig, String> EXTERNAL_SIGNATURE_CONFIG_TO_ID_CONVERTER =
            context -> Optional.ofNullable(context.getSource())
                    .map(ExternalSignatureConfig::getId)
                    .orElse(null);

    private static final Converter<String, ExternalSignatureConfig> ID_TO_EXTERNAL_SIGNATURE_CONFIG_CONVERTER =
            context -> Optional.ofNullable(context.getSource())
                    .filter(StringUtils::isNotEmpty)
                    .map(i -> ExternalSignatureConfig.builder().id(i).build())
                    .orElse(null);

    public static final Converter<Type, String> TYPE_TO_ID_CONVERTER =
            context -> Optional.ofNullable(context.getSource())
                    .map(TypologyEntity::getId)
                    .orElse(null);

    public static final Converter<String, Type> ID_TO_TYPE_CONVERTER =
            context -> Optional.ofNullable(context.getSource())
                    .filter(StringUtils::isNotEmpty)
                    .map(i -> Type.builder().id(i).build())
                    .orElse(null);

    public static final Converter<Subtype, String> SUBTYPE_TO_ID_CONVERTER =
            context -> Optional.ofNullable(context.getSource())
                    .map(TypologyEntity::getId)
                    .orElse(null);

    public static final Converter<String, Subtype> ID_TO_SUBTYPE_CONVERTER =
            context -> Optional.ofNullable(context.getSource())
                    .filter(StringUtils::isNotEmpty)
                    .map(i -> Subtype.builder().id(i).build())
                    .orElse(null);

    private static final Converter<Layer, String> LAYER_TO_ID_CONVERTER =
            context -> Optional.ofNullable(context.getSource())
                    .map(Layer::getId)
                    .orElse(null);

    private static final Converter<String, Layer> ID_TO_LAYER_CONVERTER =
            context -> Optional.ofNullable(context.getSource())
                    .filter(StringUtils::isNotEmpty)
                    .map(Layer::new)
                    .orElse(null);

    private static final Converter<Metadata, String> METADATA_TO_ID_CONVERTER =
            context -> Optional.ofNullable(context.getSource())
                    .map(Metadata::getId)
                    .orElse(null);

    private static final Converter<String, Metadata> ID_TO_METADATA_CONVERTER =
            context -> Optional.ofNullable(context.getSource())
                    .filter(StringUtils::isNotEmpty)
                    .map(i -> Metadata.builder().id(i).build())
                    .orElse(null);

    public static final Converter<User, String> USER_TO_ID_CONVERTER =
            context -> Optional.ofNullable(context.getSource())
                    .map(User::getId)
                    .orElse(null);

    public static final Converter<UserRepresentation, String> USER_REP_TO_ID_CONVERTER =
            context -> Optional.ofNullable(context.getSource())
                    .map(UserRepresentation::getId)
                    .orElse(null);

    public static final Converter<String, User> ID_TO_USER_CONVERTER =
            context -> Optional.ofNullable(context.getSource())
                    .filter(StringUtils::isNotEmpty)
                    .map(User::new)
                    .orElse(null);

    public static final Converter<String, UserRepresentation> ID_TO_USER_REP_CONVERTER =
            context -> Optional.ofNullable(context.getSource())
                    .filter(StringUtils::isNotEmpty)
                    .map(UserRepresentation::new)
                    .orElse(null);

    private static final Converter<List<Metadata>, List<String>> METADATA_TO_ID_LIST_CONVERTER =
            context -> Optional.ofNullable(context.getSource())
                    .orElse(emptyList())
                    .stream()
                    .map(Metadata::getId)
                    .toList();

    private static final Converter<List<String>, List<Metadata>> ID_TO_METADATA_LIST_CONVERTER =
            context -> Optional.ofNullable(context.getSource())
                    .orElse(emptyList())
                    .stream()
                    .map(Metadata::new)
                    .toList();

    private static final Converter<List<String>, List<LabelledColumn>> ID_TO_LABELLED_COLUMN_LIST_CONVERTER =
            context -> Optional.ofNullable(context.getSource())
                    .orElse(emptyList())
                    .stream()
                    .map(LabelledColumn::new)
                    .collect(toList());

    private static final Converter<String, CertificateInformations> X_500_SUBJECT_INFORMATIONS_CONVERTER =
            context -> Optional.ofNullable(context.getSource())
                    .map(CryptoUtils::parseX509Certificate)
                    .map(JcaX509CertificateHolder::getSubject)
                    .map(CryptoUtils::parseInnerX500Values)
                    .orElse(null);

    private static final Converter<String, CertificateInformations> X_500_ISSUER_INFORMATIONS_CONVERTER =
            context -> Optional.ofNullable(context.getSource())
                    .map(CryptoUtils::parseX509Certificate)
                    .map(JcaX509CertificateHolder::getIssuer)
                    .map(CryptoUtils::parseInnerX500Values)
                    .orElse(null);


    // <editor-fold desc="Beans">


    private final ContentServiceProperties contentServiceProperties;


    public ModelMapperConfigurer(ContentServiceProperties contentServiceProperties) {
        this.contentServiceProperties = contentServiceProperties;
    }


    // </editor-fold desc="Beans">


    @Bean
    public ModelMapper modelMapper() {

        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(STRICT);

        setupUserPreferencesDto(modelMapper);
        setupTableLayoutDto(modelMapper);
        setupFolderFilterDto(modelMapper);
        setupFolderFilterParam(modelMapper);
        setupDeskDto(modelMapper);
        setupSubtypeDto(modelMapper);
        setupSubtypeLayerDto(modelMapper);
        setupSubtypeMetadataDto(modelMapper);
        setupFolderDto(modelMapper);
        setupStepDefinitionDto(modelMapper);
        setupSealCertificateDto(modelMapper);
        setupDelegationDto(modelMapper);

        return modelMapper;
    }


    private void setupUserPreferencesDto(@NotNull ModelMapper modelMapper) {

        modelMapper.typeMap(UserPreferencesDto.class, UserPreferences.class)
                .addMappings(m -> m.map(UserPreferencesDto::getFavoriteDeskIds, UserPreferences::setFavoriteDeskIdList));

        modelMapper.emptyTypeMap(UserPreferences.class, UserPreferencesDto.class)
                .addMappings(m -> m.using(ID_TO_DESK_REPRESENTATION_LIST_CONVERTER)
                        .map(UserPreferences::getFavoriteDeskIdList, UserPreferencesDto::setFavoriteDesks))
                .addMappings(m -> m.map(UserPreferences::getFavoriteDeskIdList, UserPreferencesDto::setFavoriteDeskIds))
                .implicitMappings();
    }


    private void setupTableLayoutDto(@NotNull ModelMapper modelMapper) {
        modelMapper.emptyTypeMap(TableLayout.class, TableLayoutDto.class)
                .addMappings(m -> m.using(ID_TO_LABELLED_COLUMN_LIST_CONVERTER)
                        .map(TableLayout::getColumnList, TableLayoutDto::setLabelledColumnList))
                .implicitMappings();
    }


    private void setupFolderFilterDto(@NotNull ModelMapper modelMapper) {

        modelMapper.emptyTypeMap(FolderFilterDto.class, FolderFilter.class)
                .addMappings(m -> m.using(ID_TO_TYPE_CONVERTER)
                        .map(FolderFilterDto::getTypeId, FolderFilter::setType))
                .addMappings(m -> m.using(ID_TO_SUBTYPE_CONVERTER)
                        .map(FolderFilterDto::getSubtypeId, FolderFilter::setSubtype))
                .implicitMappings();

        modelMapper.emptyTypeMap(FolderFilter.class, FolderFilterDto.class)
                .addMappings(m -> m.map(FolderFilter::getType, FolderFilterDto::setType))
                .addMappings(m -> m.map(FolderFilter::getSubtype, FolderFilterDto::setSubtype))
                .implicitMappings();
    }


    private void setupFolderFilterParam(@NotNull ModelMapper modelMapper) {

        modelMapper.typeMap(FolderFilter.class, FolderFilterParam.class)
                .addMappings(m -> m.using(TYPE_TO_ID_CONVERTER)
                        .map(FolderFilter::getType, FolderFilterParam::setTypeId))
                .addMappings(m -> m.using(SUBTYPE_TO_ID_CONVERTER)
                        .map(FolderFilter::getSubtype, FolderFilterParam::setSubtypeId));
    }


    private void setupDeskDto(@NotNull ModelMapper modelMapper) {

        modelMapper.typeMap(DeskDto.class, Desk.class)
                .addMappings(m -> m.using(ID_TO_DESK_REPRESENTATION_CONVERTER)
                        .map(DeskDto::getParentDeskId, Desk::setParentDesk));

        modelMapper.typeMap(Desk.class, DeskDto.class)
                .addMappings(m -> m.using(DESK_REPRESENTATION_TO_ID_CONVERTER)
                        .map(Desk::getParentDesk, DeskDto::setParentDeskId));
    }


    private void setupSubtypeDto(@NotNull ModelMapper modelMapper) {

        modelMapper.emptyTypeMap(SubtypeDto.class, Subtype.class)
                .addMappings(m -> m.using(ID_TO_EXTERNAL_SIGNATURE_CONFIG_CONVERTER)
                        .map(SubtypeDto::getExternalSignatureConfigId, Subtype::setExternalSignatureConfig))
                .implicitMappings();

        Converter<Boolean, Integer> booleanMultiDocToSettingsValue = mappingContext
                -> mappingContext.getSource()
                   ? contentServiceProperties.getMaxMainFiles()
                   : 1;

        modelMapper.typeMap(Subtype.class, SubtypeDto.class)
                .addMappings(m -> m.using(EXTERNAL_SIGNATURE_CONFIG_TO_ID_CONVERTER)
                        .map(Subtype::getExternalSignatureConfig, SubtypeDto::setExternalSignatureConfigId))
                .addMappings(m -> m.using(ID_TO_DESK_REPRESENTATION_NULLABLE_LIST_CONVERTER)
                        .map(Subtype::getCreationPermittedDeskIds, SubtypeDto::setCreationPermittedDesks))
                .addMappings(m -> m.using(ID_TO_DESK_REPRESENTATION_NULLABLE_LIST_CONVERTER)
                        .map(Subtype::getFilterableByDeskIds, SubtypeDto::setFilterableByDesks))
                .addMappings(m -> m.using(booleanMultiDocToSettingsValue)
                        .map(Subtype::isMultiDocuments, SubtypeDto::setMaxMainDocuments));
    }


    private void setupSubtypeLayerDto(@NotNull ModelMapper modelMapper) {

        modelMapper.emptyTypeMap(SubtypeLayerDto.class, SubtypeLayer.class)
                .addMappings(m -> m.using(ID_TO_SUBTYPE_CONVERTER)
                        .map(SubtypeLayerDto::getSubtypeId, SubtypeLayer::setSubtype))
                .addMappings(m -> m.using(ID_TO_LAYER_CONVERTER)
                        .map(SubtypeLayerDto::getLayerId, SubtypeLayer::setLayer))
                .addMappings(m -> m.map(SubtypeLayerDto::getCompositeId, SubtypeLayer::setId))
                .implicitMappings();

        modelMapper.typeMap(SubtypeLayer.class, SubtypeLayerDto.class)
                .addMappings(m -> m.using(SUBTYPE_TO_ID_CONVERTER)
                        .map(SubtypeLayer::getSubtype, SubtypeLayerDto::setSubtypeId))
                .addMappings(m -> m.using(LAYER_TO_ID_CONVERTER)
                        .map(SubtypeLayer::getLayer, SubtypeLayerDto::setLayerId));

    }


    private void setupSubtypeMetadataDto(@NotNull ModelMapper modelMapper) {

        modelMapper.emptyTypeMap(SubtypeMetadataDto.class, SubtypeMetadata.class)
                .addMappings(m -> m.using(ID_TO_SUBTYPE_CONVERTER)
                        .map(SubtypeMetadataDto::getSubtypeId, SubtypeMetadata::setSubtype))
                .addMappings(m -> m.using(ID_TO_METADATA_CONVERTER)
                        .map(SubtypeMetadataDto::getMetadataId, SubtypeMetadata::setMetadata))
                .addMappings(m -> m.map(SubtypeMetadataDto::getCompositeId, SubtypeMetadata::setId))
                .implicitMappings();

        modelMapper.typeMap(SubtypeMetadata.class, SubtypeMetadataDto.class)
                .addMappings(m -> m.using(SUBTYPE_TO_ID_CONVERTER)
                        .map(SubtypeMetadata::getSubtype, SubtypeMetadataDto::setSubtypeId))
                .addMappings(m -> m.using(METADATA_TO_ID_CONVERTER)
                        .map(SubtypeMetadata::getMetadata, SubtypeMetadataDto::setMetadataId));
    }


    private void setupFolderDto(@NotNull ModelMapper modelMapper) {

        modelMapper.emptyTypeMap(FolderDto.class, Folder.class)
                .addMappings(m -> m.using(ID_TO_TYPE_CONVERTER)
                        .map(FolderDto::getTypeId, Folder::setType))
                .addMappings(m -> m.using(ID_TO_SUBTYPE_CONVERTER)
                        .map(FolderDto::getSubtypeId, Folder::setSubtype))
                .implicitMappings();

        modelMapper.typeMap(Folder.class, FolderDto.class)
                .addMappings(m -> m.using(TYPE_TO_ID_CONVERTER)
                        .map(Folder::getType, FolderDto::setTypeId))
                .addMappings(m -> m.using(SUBTYPE_TO_ID_CONVERTER)
                        .map(Folder::getSubtype, FolderDto::setSubtypeId));
    }


    private void setupStepDefinitionDto(@NotNull ModelMapper modelMapper) {

        modelMapper.typeMap(StepDefinitionDto.class, StepDefinition.class)
                .addMappings(m -> m.using(ID_TO_DESK_REPRESENTATION_LIST_CONVERTER)
                        .map(StepDefinitionDto::getValidatingDeskIds, StepDefinition::setValidatingDesks))
                .addMappings(m -> m.using(ID_TO_DESK_REPRESENTATION_LIST_CONVERTER)
                        .map(StepDefinitionDto::getNotifiedDeskIds, StepDefinition::setNotifiedDesks))
                .addMappings(m -> m.using(ID_TO_METADATA_LIST_CONVERTER)
                        .map(StepDefinitionDto::getMandatoryValidationMetadataIds, StepDefinition::setMandatoryValidationMetadata))
                .addMappings(m -> m.using(ID_TO_METADATA_LIST_CONVERTER)
                        .map(StepDefinitionDto::getMandatoryRejectionMetadataIds, StepDefinition::setMandatoryRejectionMetadata));

        modelMapper.typeMap(StepDefinition.class, StepDefinitionDto.class)
                .addMappings(m -> m.using(DESK_REPRESENTATION_TO_ID_LIST_CONVERTER)
                        .map(StepDefinition::getValidatingDesks, StepDefinitionDto::setValidatingDeskIds))
                .addMappings(m -> m.using(DESK_REPRESENTATION_TO_ID_LIST_CONVERTER)
                        .map(StepDefinition::getNotifiedDesks, StepDefinitionDto::setNotifiedDeskIds))
                .addMappings(m -> m.using(METADATA_TO_ID_LIST_CONVERTER)
                        .map(StepDefinition::getMandatoryValidationMetadata, StepDefinitionDto::setMandatoryValidationMetadataIds))
                .addMappings(m -> m.using(METADATA_TO_ID_LIST_CONVERTER)
                        .map(StepDefinition::getMandatoryRejectionMetadata, StepDefinitionDto::setMandatoryRejectionMetadataIds));
    }


    private void setupSealCertificateDto(@NotNull ModelMapper modelMapper) {

        modelMapper.typeMap(SealCertificate.class, SealCertificateDto.class)
                .addMappings(m -> m.using(X_500_ISSUER_INFORMATIONS_CONVERTER)
                        .map(SealCertificate::getPublicCertificateBase64, SealCertificateDto::setIssuer))
                .addMappings(m -> m.using(X_500_SUBJECT_INFORMATIONS_CONVERTER)
                        .map(SealCertificate::getPublicCertificateBase64, SealCertificateDto::setSubject));
    }


    private void setupDelegationDto(@NotNull ModelMapper modelMapper) {

        modelMapper.typeMap(Delegation.class, DelegationDto.class)
                .addMappings(m -> m.using(DESK_REPRESENTATION_TO_ID_CONVERTER)
                        .map(Delegation::getDelegatingDesk, DelegationDto::setDelegatingDeskId))
                .addMappings(m -> m.using(DESK_REPRESENTATION_TO_ID_CONVERTER)
                        .map(Delegation::getSubstituteDesk, DelegationDto::setSubstituteDeskId))
                .addMappings(m -> m.using(TYPE_TO_ID_CONVERTER)
                        .map(Delegation::getType, DelegationDto::setTypeId))
                .addMappings(m -> m.using(SUBTYPE_TO_ID_CONVERTER)
                        .map(Delegation::getSubtype, DelegationDto::setSubtypeId));

        modelMapper.emptyTypeMap(DelegationDto.class, Delegation.class)
                .addMappings(m -> m.using(ID_TO_DESK_REPRESENTATION_CONVERTER)
                        .map(DelegationDto::getDelegatingDeskId, Delegation::setDelegatingDesk))
                .addMappings(m -> m.using(ID_TO_DESK_REPRESENTATION_CONVERTER)
                        .map(DelegationDto::getSubstituteDeskId, Delegation::setSubstituteDesk))
                .addMappings(m -> m.using(ID_TO_TYPE_CONVERTER)
                        .map(DelegationDto::getTypeId, Delegation::setType))
                .addMappings(m -> m.using(ID_TO_SUBTYPE_CONVERTER)
                        .map(DelegationDto::getSubtypeId, Delegation::setSubtype))
                .implicitMappings();
    }


}
