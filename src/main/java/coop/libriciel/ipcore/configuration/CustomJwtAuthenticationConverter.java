/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.configuration;

import coop.libriciel.ipcore.utils.KeycloakSecurityUtils;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;


@Log4j2
@Component
public class CustomJwtAuthenticationConverter implements Converter<Jwt, AbstractAuthenticationToken> {


    public static final String REALM_ACCESS_CLAIM_KEY = "realm_access";
    public static final String ROLES_CLAIM_KEY = "roles";


    public CustomJwtAuthenticationConverter() {
        log.debug("CustomJwtAuthenticationConverter()");
    }


    @Bean
    public JwtAuthenticationConverter jwtAuthenticationConverterForKeycloak() {
        Converter<Jwt, Collection<GrantedAuthority>> jwtGrantedAuthoritiesConverter = this::extractAuthorities;

        JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter();
        jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(jwtGrantedAuthoritiesConverter);
        return jwtAuthenticationConverter;
    }


    @Override
    public AbstractAuthenticationToken convert(@NotNull Jwt jwt) {
        log.debug("CustomJwtAuthenticationConverter - convert");
        Collection<GrantedAuthority> authorities = extractAuthorities(jwt);

        return new JwtAuthenticationToken(jwt, authorities);
    }


    private Collection<GrantedAuthority> extractAuthorities(Jwt jwt) {
        //noinspection unchecked
        return Optional.ofNullable(jwt)
                .map(jwtToken -> jwtToken.getClaim(REALM_ACCESS_CLAIM_KEY))
                .filter(realmAccessObj -> realmAccessObj instanceof Map)
                .map(realmAccessObj -> (Map<String, Object>) realmAccessObj)
                .map(realmAccessMap -> realmAccessMap.get(ROLES_CLAIM_KEY))
                .filter(rolesObj -> rolesObj instanceof ArrayList)
                .map(rolesObj -> (ArrayList<String>) rolesObj)
                .stream()
                .flatMap(Collection::stream)
                .map(role -> new SimpleGrantedAuthority(KeycloakSecurityUtils.ROLE_PREFIX_FOR_SPRING_AUTH + role))
                .collect(toList());
    }


}
