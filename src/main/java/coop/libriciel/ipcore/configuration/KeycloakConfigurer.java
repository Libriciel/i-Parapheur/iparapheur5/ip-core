/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.configuration;

import coop.libriciel.ipcore.services.auth.AuthServiceProperties;
import lombok.extern.log4j.Log4j2;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.authorization.client.AuthzClient;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

import static org.keycloak.OAuth2Constants.CLIENT_CREDENTIALS;


@Configuration
@Log4j2
@EnableWebSecurity
@ConditionalOnProperty(name = "keycloak.enabled", havingValue = "true", matchIfMissing = true)
public class KeycloakConfigurer {

    private static final String CLIENT_ID_ADMIN_CLI = "admin-cli";

    private Keycloak keycloakClient;

    private final AuthServiceProperties authServiceProperties;
    private final KeycloakSpringBootProperties kcProperties;

    public KeycloakConfigurer(AuthServiceProperties authServiceProperties, KeycloakSpringBootProperties kcProperties) {
        this.authServiceProperties = authServiceProperties;
        this.kcProperties = kcProperties;
    }

    @Bean
    public KeycloakBuilder builder() {
        return KeycloakBuilder.builder()
                .realm(kcProperties.getRealm())
                .serverUrl(kcProperties.getBackchannelServerUrl())
                .clientId(kcProperties.getResource())
                .clientSecret((String) kcProperties.getCredentials().get("secret"))
                .grantType(CLIENT_CREDENTIALS);
    }


    @Bean
    public Keycloak getKeycloakClient() {
        String keycloakBackendUrl = String.format("http://%s:%d/auth", authServiceProperties.getHost(), authServiceProperties.getPort());

        this.keycloakClient = KeycloakBuilder.builder()
                .serverUrl(keycloakBackendUrl)
                .realm(authServiceProperties.getRealm())
                .username(authServiceProperties.getUsername())
                .password(authServiceProperties.getPassword())
                .clientId(CLIENT_ID_ADMIN_CLI)
                .grantType(OAuth2Constants.PASSWORD)
                .resteasyClient(
                        ((ResteasyClientBuilder) ResteasyClientBuilder.newBuilder())
                        .connectionPoolSize(authServiceProperties.getPoolSize())
                        .build()
                )
                .build();

        return this.keycloakClient;
    }


    @Scheduled(fixedDelayString = "${services.auth.ticket-delay}")
    protected void refreshClientToken() {
        keycloakClient.tokenManager().refreshToken();
    }



    /**
     * The default {@link AuthzClient#create()} asks a `keycloak.json` file, that doesn't really exist on SpringBoot.
     * This is a (not-so-bad) workaround to make it SpringBoot-friendly.
     * <p>
     * Cheers to <a href="https://gist.github.com/dasniko/2c64393da0bca89434670908141914c4">this post</a>
     *
     * @param kcProperties
     * @return
     */
    @Bean
    public AuthzClient authzClient(KeycloakSpringBootProperties kcProperties) {

        org.keycloak.authorization.client.Configuration configuration = new org.keycloak.authorization.client.Configuration(
                kcProperties.getBackchannelServerUrl(),
                kcProperties.getRealm(),
                kcProperties.getResource(),
                kcProperties.getCredentials(),
                null
        );

        return AuthzClient.create(configuration);
    }

}
