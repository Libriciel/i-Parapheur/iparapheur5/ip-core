/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller;

import coop.libriciel.ipcore.business.workflow.WorkflowBusinessService;
import coop.libriciel.ipcore.model.content.DetachedSignature;
import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.database.Type;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.UUID;

import static coop.libriciel.ipcore.model.crypto.SignatureFormat.PADES;
import static coop.libriciel.ipcore.model.crypto.SignatureFormat.PKCS7;
import static coop.libriciel.ipcore.model.workflow.Action.*;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;


@WithMockUser
@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
class WorkflowControllerTest {


    @Autowired private WorkflowController workflowController;
    @Autowired private WorkflowBusinessService workflowBusinessService;


    @Test
    void checkActionAllowedWithDetachedSignatures() {

        DetachedSignature dummyDetachedSignature = DetachedSignature.builder()
                .id(UUID.randomUUID().toString())
                .build();

        Document unsignedDocument = Document.builder()
                .id(UUID.randomUUID().toString())
                .isMainDocument(true)
                .build();

        Document signedDocument = Document.builder()
                .id(UUID.randomUUID().toString())
                .isMainDocument(true)
                .detachedSignatures(List.of(dummyDetachedSignature))
                .build();

        Type type = new Type();

        Folder folder = new Folder();
        folder.setType(type);

        // Detached signature protocol

        type.setSignatureFormat(PKCS7);

        folder.setDocumentList(List.of(unsignedDocument, unsignedDocument));
        assertDoesNotThrow(() -> workflowBusinessService.checkActionAllowedWithDetachedSignatures(folder, VISA));
        assertDoesNotThrow(() -> workflowBusinessService.checkActionAllowedWithDetachedSignatures(folder, SEAL));
        assertDoesNotThrow(() -> workflowBusinessService.checkActionAllowedWithDetachedSignatures(folder, EXTERNAL_SIGNATURE
        ));

        folder.setDocumentList(List.of(signedDocument));
        assertDoesNotThrow(() -> workflowBusinessService.checkActionAllowedWithDetachedSignatures(folder, VISA));
        assertDoesNotThrow(() -> workflowBusinessService.checkActionAllowedWithDetachedSignatures(folder, SEAL));
        assertThrows(
                LocalizedStatusException.class,
                () -> workflowBusinessService.checkActionAllowedWithDetachedSignatures(folder, EXTERNAL_SIGNATURE)
        );

        // Enveloped signature protocol

        type.setSignatureFormat(PADES);

        folder.setDocumentList(List.of(unsignedDocument, unsignedDocument));
        assertDoesNotThrow(() -> workflowBusinessService.checkActionAllowedWithDetachedSignatures(folder, VISA));
        assertDoesNotThrow(() -> workflowBusinessService.checkActionAllowedWithDetachedSignatures(folder, SEAL));
        assertDoesNotThrow(() -> workflowBusinessService.checkActionAllowedWithDetachedSignatures(folder, EXTERNAL_SIGNATURE));

        folder.setDocumentList(List.of(signedDocument));
        assertDoesNotThrow(() -> workflowBusinessService.checkActionAllowedWithDetachedSignatures(folder, VISA));
        assertThrows(LocalizedStatusException.class, () -> workflowBusinessService.checkActionAllowedWithDetachedSignatures(folder, SEAL));
        assertThrows(
                LocalizedStatusException.class,
                () -> workflowBusinessService.checkActionAllowedWithDetachedSignatures(folder, EXTERNAL_SIGNATURE)
        );
    }


}
