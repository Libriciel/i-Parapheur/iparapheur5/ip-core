/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import coop.libriciel.ipcore.services.gdpr.GdprInformationDetailsDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static com.fasterxml.jackson.databind.SerializationFeature.INDENT_OUTPUT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest
@ExtendWith(SpringExtension.class)
class ServerInfoControllerTest {


    private @Autowired ServerInfoController serverInfoController;
    private final ObjectMapper objectMapper = JsonMapper.builder()
            .enable(INDENT_OUTPUT)
            .build();


    @WithMockUser()
    @Test
    void getGdprInformationDetailsDto() throws JsonProcessingException {

        String expected = """
                          {
                            "declaringEntity" : {
                              "name" : "Mairie de Montpellier",
                              "address" : "1 Place Georges Frêche, 34000 Montpellier",
                              "siret" : "213 401 722 01787",
                              "apeCode" : "8411Z",
                              "phoneNumber" : "04 67 34 70 00",
                              "mail" : "contact-mairie@dom.local",
                              "dpo" : {
                                "name" : "Nom du DPO",
                                "mail" : "dpo@dom.local"
                              },
                              "responsible" : {
                                "name" : "Nom du maire",
                                "title" : "Maire"
                              }
                            },
                            "hostingEntityComments" : "Libriciel SCOP, assure l'hébergement de vos données chez plusieurs sous-traitants, uniquement sur le territoire français, en dehors des zones soumises au PATRIOT Act.",
                            "hostingEntity" : {
                              "name" : "Libriciel SCOP",
                              "address" : "140 Rue Aglaonice de Thessalie, 34170 Castelnau-le-Lez",
                              "siret" : "491 011 698 00025"
                            },
                            "maintenanceEntity" : {
                              "name" : "Libriciel SCOP",
                              "address" : "140 Rue Aglaonice de Thessalie, 34170 Castelnau-le-Lez",
                              "siret" : "491 011 698 00025"
                            },
                            "application" : {
                              "name" : "iparapheur",
                              "cookieSessionDuration" : "180 minutes",
                              "mandatoryCookies" : [ "Cookie de localisation" ],
                              "preservedDataAfterDeletion" : [ "Historique des transactions", "Commentaires" ],
                              "optionalCookies" : [ {
                                "name" : "Cookies de personnalisation de l'application",
                                "elements" : [ "un cookie de personnalisation" ],
                                "description" : "Permet de conserver vos préférences."
                              } ],
                              "noCookies" : false,
                              "editor" : {
                                "name" : "Libriciel Scop",
                                "address" : "140 Rue Aglaonice de Thessalie, 34170 Castelnau-le-Lez",
                                "siret" : "491 011 698 00025"
                              },
                              "noDataProcessed" : false,
                              "noDataCollected" : false,
                              "dataProcesses" : [ {
                                "name" : "Affichage de vos données personnelles",
                                "elements" : [ "Liste des utilisateurs : votre identifiant de connexion, apparait dans la liste." ]
                              } ],
                              "collectedDataSet" : [ {
                                "name" : "Création d'un utilisateur",
                                "mandatoryElements" : [ "Civilité de l'utilisateur", "Nom de l'utilisateur" ],
                                "optionalElements" : [ "Numéro de téléphone" ]
                              } ]
                            }
                          }\
                          """;

        GdprInformationDetailsDto gdprInformationDetails = serverInfoController.getServerInfo().getGdprInformationDetailsDto();
        assertNotNull(gdprInformationDetails);
        String gdprPropertiesString = objectMapper.writeValueAsString(gdprInformationDetails);
        assertEquals(expected, gdprPropertiesString);
    }


}
