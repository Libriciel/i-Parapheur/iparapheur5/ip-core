/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller.admin;

import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.pdfstamp.LayerDto;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.database.*;
import coop.libriciel.ipcore.services.pdfstamp.PdfStampServiceInterface;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.Locale;
import java.util.UUID;

import static coop.libriciel.ipcore.utils.LocalizedStatusException.getMessageForLocale;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;


class AdminLayerControllerTest {

    @Test
    void createLayerFailWhenTenantLimitReached() {

        LayerRepository layerRepositoryMock = mock(LayerRepository.class);

        AdminLayerController adminLayerController = new AdminLayerController(
                new ModelMapper(),
                mock(StatsServiceInterface.class),
                layerRepositoryMock,
                mock(StampRepository.class),
                mock(MetadataRepository.class),
                mock(PdfStampServiceInterface.class),
                mock(ContentServiceInterface.class),
                mock(PlatformTransactionManager.class)
        );

        Tenant dummyTenant = Tenant.builder().id(UUID.randomUUID().toString()).build();
        dummyTenant.setLayerLimit(0);

        Mockito.when(layerRepositoryMock.countAllByTenant_Id(anyString()))
                .thenReturn(0L);

        LocalizedStatusException exception = assertThrows(
                LocalizedStatusException.class,
                () -> adminLayerController.createLayer(dummyTenant.getId(), dummyTenant, new LayerDto())
        );
        assertEquals(
                HttpStatus.INSUFFICIENT_STORAGE + " \""
                        + getMessageForLocale("message.already_n_layers_maximum_reached", Locale.getDefault(), dummyTenant.getLayerLimit())
                        + "\"",
                exception.getMessage()
        );
    }
}
