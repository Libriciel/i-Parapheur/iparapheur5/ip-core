/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.ipcore.controller.admin;

import coop.libriciel.ipcore.model.database.*;
import coop.libriciel.ipcore.services.database.ExternalSignatureConfigRepository;
import coop.libriciel.ipcore.services.database.SubtypeRepository;
import coop.libriciel.ipcore.services.database.TenantRepository;
import coop.libriciel.ipcore.services.database.TypeRepository;
import coop.libriciel.ipcore.services.externalsignature.ExternalSignatureService;
import coop.libriciel.ipcore.services.stats.NoneStatsService;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.IntStream;

import static coop.libriciel.ipcore.controller.admin.AdminExternalSignatureController.MAX_EXTERNAL_SIGNATURE_CONFIG_PER_TENANT_COUNT;
import static coop.libriciel.ipcore.model.database.ExternalSignatureProvider.DOCAGE;

import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
class AdminExternalSignatureControllerTest {

    private static final Tenant EXAMPLE_TENANT = Tenant.builder().id(UUID.randomUUID().toString()).build();
    private static final Type EXAMPLE_TYPE = Type.builder().id(UUID.randomUUID().toString()).tenant(EXAMPLE_TENANT).build();
    private static final Subtype EXAMPLE_SUBTYPE = Subtype.builder().id(UUID.randomUUID().toString()).tenant(EXAMPLE_TENANT).parentType(EXAMPLE_TYPE).build();
    private static ExternalSignatureConfig exampleConfig;

    private AdminExternalSignatureController adminExternalSignatureController;

    private @Autowired ExternalSignatureConfigRepository externalSignatureConfigRepository;
    private @Autowired TenantRepository tenantRepository;
    private @Autowired TypeRepository typeRepository;
    private @Autowired SubtypeRepository subtypeRepository;
    private @Autowired ModelMapper modelMapper;
    private @Mock ExternalSignatureService externalSignatureServiceMock;


    // <editor-fold desc="Init/Cleanup">


    @BeforeAll
    public static void init() {
        exampleConfig = new ExternalSignatureConfig();
        exampleConfig.setTenant(EXAMPLE_TENANT);
        exampleConfig.setId(UUID.randomUUID().toString());
        exampleConfig.setName(RandomStringUtils.insecure().nextAlphanumeric(32));
        exampleConfig.setServiceName(DOCAGE);
        exampleConfig.setLogin(RandomStringUtils.insecure().nextAlphanumeric(32));
        exampleConfig.setPassword(RandomStringUtils.insecure().nextAlphanumeric(32));
    }


    @BeforeEach
    void setUp() {
        tenantRepository.save(EXAMPLE_TENANT);
        typeRepository.save(EXAMPLE_TYPE);
        subtypeRepository.save(EXAMPLE_SUBTYPE);
    }


    @BeforeEach
    @AfterEach
    void cleanup() {

        adminExternalSignatureController = new AdminExternalSignatureController(
                externalSignatureConfigRepository,
                externalSignatureServiceMock,
                modelMapper,
                new NoneStatsService()
        );

        externalSignatureConfigRepository.findAll().stream()
                .map(ExternalSignatureConfig::getId)
                .forEach(externalSignatureConfigRepository::deleteById);
    }


    @AfterEach
    void tearDown() {
        subtypeRepository.deleteAll();
        typeRepository.deleteAll();
        tenantRepository.deleteAll();
    }


    // </editor-fold desc="Init/Cleanup">


    @Nested
    class createExternalSignatureConfig {


        @Test
        void regularCreation() {

            exampleConfig.setTransactionIds(emptySet());
            exampleConfig.setSubtypes(emptyList());

            ExternalSignatureConfigDto result = adminExternalSignatureController.createExternalSignatureConfig(
                    EXAMPLE_TENANT.getId(),
                    EXAMPLE_TENANT,
                    modelMapper.map(exampleConfig, ExternalSignatureConfigDto.class)
            );

            assertNotNull(result.getId());
            assertEquals(exampleConfig.getName(), result.getName());
            assertEquals(1L, externalSignatureConfigRepository.countAllByTenantId(EXAMPLE_TENANT.getId()));
        }


        @Test
        void overflowError() {

            exampleConfig.setTransactionIds(emptySet());
            exampleConfig.setSubtypes(emptyList());

            IntStream.range(0, MAX_EXTERNAL_SIGNATURE_CONFIG_PER_TENANT_COUNT)
                    .mapToObj(i -> modelMapper.map(exampleConfig, ExternalSignatureConfig.class))
                    .peek(externalSignatureConfig -> externalSignatureConfig.setId(UUID.randomUUID().toString()))
                    .forEach(externalSignatureConfigRepository::save);

            assertThrows(
                    LocalizedStatusException.class,
                    () -> adminExternalSignatureController.createExternalSignatureConfig(
                            EXAMPLE_TENANT.getId(),
                            EXAMPLE_TENANT,
                            modelMapper.map(exampleConfig, ExternalSignatureConfigDto.class)
                    )
            );
        }


    }


    @Nested
    class deleteExternalSignatureConfig {


        @Test
        void regularDeletion() {

            exampleConfig.setTransactionIds(emptySet());
            exampleConfig.setSubtypes(emptyList());
            externalSignatureConfigRepository.save(exampleConfig);

            assertDoesNotThrow(() -> adminExternalSignatureController.deleteExternalSignatureConfig(
                    EXAMPLE_TENANT.getId(),
                    EXAMPLE_TENANT,
                    exampleConfig.getId(),
                    exampleConfig
            ));
        }


        @Test
        void usedByFoldersError() {

            exampleConfig.setTransactionIds(Set.of(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
            exampleConfig.setSubtypes(emptyList());
            externalSignatureConfigRepository.save(exampleConfig);

            assertThrows(
                    LocalizedStatusException.class,
                    () -> adminExternalSignatureController.deleteExternalSignatureConfig(
                            EXAMPLE_TENANT.getId(),
                            EXAMPLE_TENANT,
                            exampleConfig.getId(),
                            exampleConfig
                    ));
        }


        @Test
        void usedBySubtypeError() {

            exampleConfig.setTransactionIds(emptySet());
            exampleConfig.setSubtypes(List.of(EXAMPLE_SUBTYPE));
            externalSignatureConfigRepository.save(exampleConfig);

            assertThrows(
                    LocalizedStatusException.class,
                    () -> adminExternalSignatureController.deleteExternalSignatureConfig(
                            EXAMPLE_TENANT.getId(),
                            EXAMPLE_TENANT,
                            exampleConfig.getId(),
                            exampleConfig
                    ));
        }


    }

}
