/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller.admin;

import coop.libriciel.ipcore.business.auth.DeskBusinessService;
import coop.libriciel.ipcore.business.auth.UserBusinessService;
import coop.libriciel.ipcore.business.database.MetadataBusinessService;
import coop.libriciel.ipcore.business.typology.TypologyBusinessService;
import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.WorkflowDefinition;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Locale;
import java.util.UUID;

import static coop.libriciel.ipcore.services.auth.KeycloakService.SUPER_ADMIN_ROLE_NAME;
import static coop.libriciel.ipcore.utils.LocalizedStatusException.getMessageForLocale;
import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE;


@SpringBootTest
@ExtendWith(SpringExtension.class)
@WithMockUser(username = "admin", roles = {SUPER_ADMIN_ROLE_NAME})
class AdminDeskControllerTest {


    private AdminDeskController adminDeskController;
    private WorkflowServiceInterface workflowServiceMock;
    private AuthServiceInterface authServiceMock;


    @BeforeEach
    void setup() {

        workflowServiceMock = Mockito.mock(WorkflowServiceInterface.class);
        authServiceMock = Mockito.mock(AuthServiceInterface.class);

        adminDeskController = new AdminDeskController(
                authServiceMock,
                mock(DeskBusinessService.class),
                mock(MetadataBusinessService.class),
                mock(ModelMapper.class),
                mock(PermissionServiceInterface.class),
                mock(StatsServiceInterface.class),
                mock(TypologyBusinessService.class),
                mock(UserBusinessService.class),
                workflowServiceMock
        );
    }


    @Test
    void testDeleteDeskFailWhenUsedByWorkflows() throws Exception {

        Tenant dummyTenant = Tenant.builder().id(UUID.randomUUID().toString()).build();

        Desk dummyDesk = new Desk();
        dummyDesk.setId(UUID.randomUUID().toString());
        dummyDesk.setTenantId(dummyTenant.getId());
        dummyDesk.setName("DummyDeskName");
        dummyDesk.setDirectChildrenCount(0);

        Mockito.when(authServiceMock.searchDesks(anyString(), any(Pageable.class), anyList(), anyString(), eq(false)))
                .thenReturn(new PageImpl<>(List.of(dummyDesk)));

        WorkflowDefinition dummyWorkflowDefinition = new WorkflowDefinition();
        dummyWorkflowDefinition.setId(UUID.randomUUID().toString());
        dummyWorkflowDefinition.setName("Dummy desk");

        Page<WorkflowDefinition> page = new PageImpl<>(List.of(dummyWorkflowDefinition));

        Mockito.when(workflowServiceMock.getWorkflowsUsingDeskId(anyString(), anyString()))
                .thenReturn(page);

        Exception exception = assertThrows(
                Exception.class,
                () -> adminDeskController.deleteDesk(dummyTenant.getId(), dummyTenant, dummyDesk.getId(), dummyDesk)
        );
        assertEquals(
                NOT_ACCEPTABLE + " \""
                        + getMessageForLocale("message.cannot_delete_a_desk_used_by_workflows", Locale.getDefault(), 1, "[Dummy desk]")
                        + "\"",
                exception.getMessage()
        );
    }


    @Test
    void testDeleteDeskFailWhenUsedByInstances() throws Exception {

        Tenant dummyTenant = Tenant.builder().id(UUID.randomUUID().toString()).build();

        Desk dummyDesk = new Desk();
        dummyDesk.setId(UUID.randomUUID().toString());
        dummyDesk.setTenantId(dummyTenant.getId());
        dummyDesk.setName("DummyDeskName");
        dummyDesk.setDirectChildrenCount(0);

        Mockito.when(authServiceMock.searchDesks(anyString(), any(Pageable.class), anyList(), anyString(), eq(false)))
                .thenReturn(new PageImpl<>(List.of(dummyDesk)));

        Mockito.when(workflowServiceMock.getWorkflowsUsingDeskId(anyString(), anyString()))
                .thenReturn(new PageImpl<>(emptyList()));

        Folder dummyFolder = new Folder();
        dummyFolder.setName("Folder Test");

        Page<Folder> page = new PageImpl<>(List.of(dummyFolder));

        Mockito.when(workflowServiceMock.getInstancesUsingOldWorkflowsByDeskId(anyString(), anyString()))
                .thenReturn(page);

        LocalizedStatusException exception = assertThrows(
                LocalizedStatusException.class,
                () -> adminDeskController.deleteDesk(dummyTenant.getId(), dummyTenant, dummyDesk.getId(), dummyDesk)
        );
        assertEquals(
                NOT_ACCEPTABLE + " \""
                        + getMessageForLocale("message.cannot_delete_a_desk_with_folders", Locale.getDefault(), 1)
                        + "\"",
                exception.getMessage()
        );
    }

    @Test
    void testDeleteDeskFailWhenDeskHasChildren() throws Exception {

        Tenant dummyTenant = Tenant.builder().id(UUID.randomUUID().toString()).build();

        Desk dummyDesk = new Desk();
        dummyDesk.setId(UUID.randomUUID().toString());
        dummyDesk.setTenantId(dummyTenant.getId());
        dummyDesk.setName("DummyDeskName");
        dummyDesk.setDirectChildrenCount(2);

        Mockito.when(authServiceMock.searchDesks(anyString(), any(Pageable.class), anyList(), anyString(), eq(false)))
                .thenReturn(new PageImpl<>(List.of(dummyDesk)));

        Mockito.when(workflowServiceMock.getWorkflowsUsingDeskId(anyString(), anyString()))
                .thenReturn(new PageImpl<>(emptyList()));

        Mockito.when(workflowServiceMock.getInstancesUsingOldWorkflowsByDeskId(anyString(), anyString()))
                .thenReturn(new PageImpl<>(emptyList()));

        LocalizedStatusException exception = assertThrows(
                LocalizedStatusException.class,
                () -> adminDeskController.deleteDesk(dummyTenant.getId(), dummyTenant, dummyDesk.getId(), dummyDesk)
        );
        assertEquals(
                NOT_ACCEPTABLE + " \""
                        + getMessageForLocale("message.cannot_delete_a_desk_with_children", Locale.getDefault(), 1)
                        + "\"",
                exception.getMessage()
        );
    }



    @Test
    void testDeleteDeskSuccess() throws Exception {

        Tenant dummyTenant = Tenant.builder().id(UUID.randomUUID().toString()).build();

        Desk dummyDesk = new Desk();
        dummyDesk.setId(UUID.randomUUID().toString());
        dummyDesk.setTenantId(dummyTenant.getId());
        dummyDesk.setName("DummyDeskName");
        dummyDesk.setDirectChildrenCount(0);

        Mockito.when(authServiceMock.searchDesks(anyString(), any(Pageable.class), anyList(), anyString(), eq(false)))
                .thenReturn(new PageImpl<>(List.of(dummyDesk)));

        Mockito.when(workflowServiceMock.getWorkflowsUsingDeskId(anyString(), anyString()))
                .thenReturn(new PageImpl<>(emptyList()));

        Mockito.when(workflowServiceMock.getInstancesUsingOldWorkflowsByDeskId(anyString(), anyString()))
                .thenReturn(new PageImpl<>(emptyList()));

        assertDoesNotThrow(() -> adminDeskController.deleteDesk(dummyTenant.getId(), dummyTenant, dummyDesk.getId(), dummyDesk));
    }

}
