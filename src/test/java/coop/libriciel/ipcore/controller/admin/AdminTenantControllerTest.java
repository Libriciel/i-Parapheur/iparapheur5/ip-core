/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller.admin;

import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.database.TenantRepresentation;
import coop.libriciel.ipcore.model.database.requests.TenantDto;
import coop.libriciel.ipcore.services.database.DatabaseServiceInterface;
import coop.libriciel.ipcore.services.database.TenantRepository;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.*;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static coop.libriciel.ipcore.model.database.TenantSortBy.Constants.NAME_VALUE;
import static coop.libriciel.ipcore.utils.TextUtils.MESSAGE_BUNDLE;
import static java.lang.Integer.MAX_VALUE;
import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.*;


@Log4j2
@SpringBootTest
@ExtendWith(SpringExtension.class)
@WithMockUser(
        username = "Jean",
        roles = {"admin"}
)
class AdminTenantControllerTest {


    @Autowired AdminTenantController adminTenantController;
    @Autowired TenantRepository tenantRepository;


    @BeforeEach
    @AfterEach
    void cleanup() {

        tenantRepository.findAll().forEach(tenantRepository::delete);

        ResourceBundle messageResourceBundle = ResourceBundle.getBundle(MESSAGE_BUNDLE, Locale.getDefault());
        String defaultTenantName = messageResourceBundle.getString("message.initial_tenant");
        tenantRepository.save(
                Tenant.builder()
                        .id(UUID.randomUUID().toString())
                        .name(defaultTenantName)
                        .index(0L)
                        .contentId(UUID.randomUUID().toString())
                        .statsId(UUID.randomUUID().toString())
                        .build()
        );
    }


    @Test
    void initTest() {

        String defaultTenantId = tenantRepository.findAll().stream().findFirst()
                .map(Tenant::getId)
                .orElseThrow(() -> new AssertionError("No default tenant"));

        PageRequest pageRequest = PageRequest.of(0, MAX_VALUE, Sort.by(NAME_VALUE));
        List<TenantRepresentation> tenantList = adminTenantController.listTenants(pageRequest, null).getContent();
        assertEquals(1, tenantList.size());

        TenantRepresentation defaultTenant = tenantList.get(0);
        assertNotNull(defaultTenant);
        assertEquals(defaultTenantId, defaultTenant.getId());
    }


    @Test
    void createTenant() {
        TenantDto tenantDto01 = new TenantDto("Tenant 01", "10000", 100, 100, 100, 100, 100, 100, 0, 0);
        TenantDto tenantDto02 = new TenantDto("Tenant 02", "20000", 200, 200, 200, 200, 200, 200, 0, 0);

        TenantDto tenant01 = adminTenantController.createTenant(tenantDto01);
        TenantDto tenant02 = adminTenantController.createTenant(tenantDto02);

        assertNotNull(tenant01);
        assertNotNull(tenant02);
        assertFalse(StringUtils.isEmpty(tenant01.getId()));
        assertFalse(StringUtils.isEmpty(tenant02.getId()));
        assertEquals("Tenant 01", tenant01.getName());
        assertEquals("10000", tenant01.getZipCode());
        assertEquals(100, tenant01.getDeskLimit());
        assertEquals(100, tenant01.getUserLimit());
        assertEquals(100, tenant01.getWorkflowLimit());
        assertEquals(100, tenant01.getTypeLimit());
        assertEquals(100, tenant01.getSubtypeLimit());
        assertEquals(100, tenant01.getFolderLimit());
        assertEquals(0, tenant01.getMetadataLimit());
        assertEquals(0, tenant01.getLayerLimit());
        assertEquals("Tenant 02", tenant02.getName());
        assertEquals("20000", tenant02.getZipCode());
        assertEquals(200, tenant02.getDeskLimit());
        assertEquals(200, tenant02.getUserLimit());
        assertEquals(200, tenant02.getWorkflowLimit());
        assertEquals(200, tenant02.getTypeLimit());
        assertEquals(200, tenant02.getSubtypeLimit());
        assertEquals(200, tenant02.getFolderLimit());
        assertEquals(0, tenant02.getMetadataLimit());
        assertEquals(0, tenant02.getLayerLimit());
    }


    @Test
    void editTenant() {

        TenantDto tenantDto = new TenantDto("Tenant 01");
        tenantDto = adminTenantController.createTenant(tenantDto);

        Tenant tenant = tenantRepository.findById(tenantDto.getId())
                .orElseGet(Assertions::fail);

        assertEquals("Tenant 01", tenant.getName());
        assertNull(tenant.getZipCode());
        assertNull(tenant.getDeskLimit());
        assertNull(tenant.getUserLimit());
        assertNull(tenant.getWorkflowLimit());
        assertNull(tenant.getTypeLimit());
        assertNull(tenant.getSubtypeLimit());
        assertNull(tenant.getFolderLimit());
        assertNull(tenant.getMetadataLimit());
        assertNull(tenant.getLayerLimit());

        TenantDto modifiedDto = new TenantDto("Tenant 01 with new name", "20000", 2, 3, 4, null, null, null, 0, 0);
        modifiedDto.setId(tenant.getId());
        adminTenantController.updateTenant(tenant.getId(), tenant, modifiedDto);

        tenant = tenantRepository.findById(tenantDto.getId())
                .orElseGet(Assertions::fail);

        assertEquals("Tenant 01 with new name", tenant.getName());
        assertEquals("20000", tenant.getZipCode());
        assertEquals(2, tenant.getDeskLimit());
        assertEquals(3, tenant.getUserLimit());
        assertEquals(4, tenant.getWorkflowLimit());
        assertNull(tenant.getTypeLimit());
        assertNull(tenant.getSubtypeLimit());
        assertNull(tenant.getFolderLimit());
        assertEquals(0, tenant.getMetadataLimit());
        assertEquals(0, tenant.getLayerLimit());
    }


}
