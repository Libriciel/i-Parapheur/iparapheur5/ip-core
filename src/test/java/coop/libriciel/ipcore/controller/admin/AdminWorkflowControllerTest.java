/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller.admin;

import coop.libriciel.ipcore.business.workflow.WorkflowBusinessService;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.workflow.*;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.database.SubtypeRepository;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.InputStream;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import static coop.libriciel.ipcore.controller.admin.AdminWorkflowDefinitionController.checkDefinitionIntegrity;
import static coop.libriciel.ipcore.model.workflow.StepDefinitionParallelType.OR;
import static coop.libriciel.ipcore.model.workflow.WorkflowDefinition.BOSS_OF_ID_PLACEHOLDER;
import static coop.libriciel.ipcore.utils.LocalizedStatusException.getMessageForLocale;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;


@Log4j2
@SpringBootTest
@ExtendWith(SpringExtension.class)
class AdminWorkflowControllerTest {


    @Test
    void patchBpmn_bpmnIo() throws Exception {

        InputStream simpleWorkflowInputStream = getClass()
                .getClassLoader()
                .getResourceAsStream("bpmn/http.bpmn.io_visaDRH_visaDGS_signPresident.bpmn20.xml");

        String result = AdminWorkflowDefinitionController.patchBpmn(getClass().getClassLoader(), simpleWorkflowInputStream);
        String expected =
                """
                <bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:flowable="http://flowable.org/bpmn" \
                exporter="bpmn-js (https://demo.bpmn.io)" exporterVersion="3.2.1" id="Definitions_1osu5pz" targetNamespace="http://bpmn.io/schema/bpmn">
                  <bpmn:process id="Process_1lzk2p2" isExecutable="true" name="Process_1lzk2p2">
                    <bpmn:startEvent id="StartEvent_0gowkhg">
                      <bpmn:outgoing>SequenceFlow_0jia701</bpmn:outgoing>
                    </bpmn:startEvent>
                    <bpmn:sequenceFlow id="SequenceFlow_0jia701" sourceRef="StartEvent_0gowkhg" targetRef="Task_0tu55t0"></bpmn:sequenceFlow>
                    <bpmn:sequenceFlow id="SequenceFlow_0889tye" sourceRef="Task_0tu55t0" targetRef="Task_0ii92vr"></bpmn:sequenceFlow>
                    <bpmn:sequenceFlow id="SequenceFlow_0sgwjie" sourceRef="Task_0ii92vr" targetRef="Task_08nvn6g"></bpmn:sequenceFlow>
                    <bpmn:endEvent id="EndEvent_0cokryq">
                      <bpmn:incoming>SequenceFlow_0iuof41</bpmn:incoming>
                    </bpmn:endEvent>
                    <bpmn:sequenceFlow id="SequenceFlow_0iuof41" sourceRef="Task_08nvn6g" targetRef="EndEvent_0cokryq"></bpmn:sequenceFlow>
                    <bpmn:subProcess id="Task_0tu55t0" name="visa DRH">
                      <bpmn:incoming>SequenceFlow_0jia701</bpmn:incoming>
                      <bpmn:outgoing>SequenceFlow_0889tye</bpmn:outgoing>
                    </bpmn:subProcess>
                    <bpmn:subProcess id="Task_0ii92vr" name="visa DGS">
                      <bpmn:incoming>SequenceFlow_0889tye</bpmn:incoming>
                      <bpmn:outgoing>SequenceFlow_0sgwjie</bpmn:outgoing>
                    </bpmn:subProcess>
                    <bpmn:subProcess id="Task_08nvn6g" name="signature Président">
                      <bpmn:incoming>SequenceFlow_0sgwjie</bpmn:incoming>
                      <bpmn:outgoing>SequenceFlow_0iuof41</bpmn:outgoing>
                    </bpmn:subProcess>
                  </bpmn:process>
                </bpmn:definitions>
                """;

        assertEquals(expected.trim(), result.trim());
    }


    @Test
    void patchBpmn_flowableModeler() throws Exception {

        InputStream simpleWorkflowInputStream = getClass().getClassLoader().getResourceAsStream("bpmn/flowableModeler_simple_workflow.bpmn20.xml");
        String result = AdminWorkflowDefinitionController.patchBpmn(getClass().getClassLoader(), simpleWorkflowInputStream);
        String expected =
                """
                <bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:flowable="http://flowable.org/bpmn" expressionLanguage="http://www.w3.org/1999/XPath" targetNamespace="http://www.flowable.org/processdef" typeLanguage="http://www.w3.org/2001/XMLSchema">
                  <process xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:omgdc="http://www.omg.org/spec/DD/20100524/DC" xmlns:omgdi="http://www.omg.org/spec/DD/20100524/DI" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" id="simple_workflow" isExecutable="true" name="Simple workflow">
                    <documentation>Straighforward visa-signature workflow</documentation>
                    <startEvent id="startEvent1"></startEvent>
                    <callActivity calledElement="visa" id="visa_DRH" name="visa DRH" flowable:inheritBusinessKey="true" flowable:inheritVariables="false">
                      <bpmn:extensionElements>
                        <flowable:in sourceExpression="DRH" target="current_candidate_groups"></flowable:in>
                        <flowable:in sourceExpression="admins" target="previous_candidate_groups"></flowable:in>
                        <flowable:in sourceExpression="unknown" target="previous_action"></flowable:in>
                      </bpmn:extensionElements>
                    </callActivity>
                    <callActivity calledElement="signature" id="signature_President" name="signature Président" flowable:inheritBusinessKey="true" flowable:inheritVariables="false">
                      <bpmn:extensionElements>
                        <flowable:in sourceExpression="Président" target="current_candidate_groups"></flowable:in>
                        <flowable:in sourceExpression="DRH" target="previous_candidate_groups"></flowable:in>
                        <flowable:in sourceExpression="visa" target="previous_action"></flowable:in>
                      </bpmn:extensionElements>
                    </callActivity>
                    <bpmn:boundaryEvent attachedToRef="signature_President" id="signature_President_undo_catch">
                      <bpmn:errorEventDefinition errorRef="undo"></bpmn:errorEventDefinition>
                    </bpmn:boundaryEvent>
                    <bpmn:sequenceFlow id="signature_President_undo_catch_back_sequenceflow" sourceRef="signature_President_undo_catch" targetRef="visa_DRH"></bpmn:sequenceFlow>
                    <endEvent id="sid-60A14468-0498-4BAA-9FCE-03AE43CF47F7"></endEvent>
                    <sequenceFlow id="sid-24850A4C-275C-4A43-9608-CF356255317F" sourceRef="signature_President" targetRef="sid-60A14468-0498-4BAA-9FCE-03AE43CF47F7"></sequenceFlow>
                    <sequenceFlow id="sid-E81E772C-1D9E-433F-8AAE-7F5F3B5C5925" sourceRef="startEvent1" targetRef="visa_DRH"></sequenceFlow>
                    <sequenceFlow id="sid-D05D909D-956E-4A7F-BA5D-6008F4DC6541" sourceRef="visa_DRH" targetRef="signature_President"></sequenceFlow>
                  </process>
                </bpmn:definitions>
                """;

        assertEquals(expected.trim(), result.trim());
    }


    @Test
    void checkIntegrity_nullityError() {

        StepDefinitionDto stepDefinitionDto = new StepDefinitionDto(
                StepDefinitionType.VISA, emptyList(), emptyList(), emptyList(), emptyList(), emptyList(), emptyList(), emptyList(), emptyList(), OR
        );
        WorkflowDefinitionDto workflowDefinition = new WorkflowDefinitionDto();
        workflowDefinition.setId(UUID.randomUUID().toString());

        // Null steps

        workflowDefinition.setSteps(null);
        assertThrows(LocalizedStatusException.class, () -> checkDefinitionIntegrity(workflowDefinition));

        workflowDefinition.setSteps(singletonList(null));
        assertThrows(LocalizedStatusException.class, () -> checkDefinitionIntegrity(workflowDefinition));

        // Null validators

        workflowDefinition.setSteps(List.of(stepDefinitionDto));

        stepDefinitionDto.setValidatingDeskIds(null);
        assertThrows(LocalizedStatusException.class, () -> checkDefinitionIntegrity(workflowDefinition));

        stepDefinitionDto.setValidatingDeskIds(singletonList(null));
        assertThrows(LocalizedStatusException.class, () -> checkDefinitionIntegrity(workflowDefinition));

        // Valid

        stepDefinitionDto.setValidatingDeskIds(singletonList(UUID.randomUUID().toString()));
        stepDefinitionDto.setType(StepDefinitionType.ARCHIVE);
        assertDoesNotThrow(() -> checkDefinitionIntegrity(workflowDefinition));
    }


    @Test
    void checkIntegrity_parallelStepGenericDeskError() {

        WorkflowDefinitionDto workflowDefinition = new WorkflowDefinitionDto();
        workflowDefinition.setId(UUID.randomUUID().toString());
        workflowDefinition.setSteps(List.of(
                new StepDefinitionDto(
                        StepDefinitionType.VISA,
                        asList(UUID.randomUUID().toString(), BOSS_OF_ID_PLACEHOLDER),
                        emptyList(), emptyList(), emptyList(), emptyList(), emptyList(), emptyList(), emptyList(), OR
                )
        ));

        assertThrows(LocalizedStatusException.class, () -> checkDefinitionIntegrity(workflowDefinition));
    }


    @Test
    void checkIntegrity_bossOfAfterParallelStepError() {

        WorkflowDefinitionDto workflowDefinition = new WorkflowDefinitionDto();
        workflowDefinition.setId(UUID.randomUUID().toString());
        workflowDefinition.setSteps(List.of(
                new StepDefinitionDto(
                        StepDefinitionType.VISA,
                        asList(UUID.randomUUID().toString(), UUID.randomUUID().toString()),
                        emptyList(), emptyList(), emptyList(), emptyList(), emptyList(), emptyList(), emptyList(), OR
                ),
                new StepDefinitionDto(
                        StepDefinitionType.VISA,
                        singletonList(BOSS_OF_ID_PLACEHOLDER),
                        emptyList(), emptyList(), emptyList(), emptyList(), emptyList(), emptyList(), emptyList(), OR
                )
        ));

        assertThrows(LocalizedStatusException.class, () -> checkDefinitionIntegrity(workflowDefinition));
    }


    @Test
    void createWorkflowDefinitionFailWhenTenantLimitReached() {

        WorkflowBusinessService workflowBusinessServiceMock = mock(WorkflowBusinessService.class);
        WorkflowServiceInterface workflowServiceMock = mock(WorkflowServiceInterface.class);

        AdminWorkflowDefinitionController adminWorkflowDefinitionController = new AdminWorkflowDefinitionController(
                mock(AuthServiceInterface.class),
                new ModelMapper(),
                mock(StatsServiceInterface.class),
                mock(SubtypeRepository.class),
                workflowServiceMock,
                workflowBusinessServiceMock
        );

        Tenant dummyTenant = Tenant.builder().id(UUID.randomUUID().toString()).build();
        dummyTenant.setWorkflowLimit(1);

        WorkflowDefinitionDto workflowDefinition = new WorkflowDefinitionDto();
        workflowDefinition.setId(UUID.randomUUID().toString());
        workflowDefinition.setSteps(List.of(
                new StepDefinitionDto(
                        StepDefinitionType.VISA,
                        asList(UUID.randomUUID().toString(), UUID.randomUUID().toString()),
                        emptyList(), emptyList(), emptyList(), emptyList(), emptyList(), emptyList(), emptyList(), OR
                ),
                new StepDefinitionDto(
                        StepDefinitionType.ARCHIVE,
                        singletonList(BOSS_OF_ID_PLACEHOLDER),
                        emptyList(), emptyList(), emptyList(), emptyList(), emptyList(), emptyList(), emptyList(), OR
                )
        ));

        Mockito.when(workflowServiceMock.listWorkflowDefinitions(anyString(), any(), eq(null)))
                .thenReturn(new PageImpl<>(List.of(workflowDefinition)));

        LocalizedStatusException exception = assertThrows(
                LocalizedStatusException.class,
                () -> adminWorkflowDefinitionController.createWorkflowDefinition(dummyTenant.getId(), dummyTenant, workflowDefinition)
        );
        assertEquals(
                HttpStatus.INSUFFICIENT_STORAGE + " \""
                        + getMessageForLocale("message.already_n_workflow_maximum_reached", Locale.getDefault(), dummyTenant.getWorkflowLimit())
                        + "\"",
                exception.getMessage()
        );
    }


}
