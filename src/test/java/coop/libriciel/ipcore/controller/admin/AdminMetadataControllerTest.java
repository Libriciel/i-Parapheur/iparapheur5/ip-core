/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller.admin;

import coop.libriciel.ipcore.model.database.*;
import coop.libriciel.ipcore.services.database.*;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.server.ResponseStatusException;

import java.util.Locale;
import java.util.UUID;
import java.util.stream.StreamSupport;

import static coop.libriciel.ipcore.model.database.MetadataSortBy.Constants.NAME_VALUE;
import static coop.libriciel.ipcore.model.database.MetadataType.TEXT;
import static coop.libriciel.ipcore.services.auth.KeycloakService.SUPER_ADMIN_ROLE_NAME;
import static coop.libriciel.ipcore.utils.LocalizedStatusException.getMessageForLocale;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.springframework.http.HttpStatus.*;


@Log4j2
@SpringBootTest
@ExtendWith(SpringExtension.class)
@WithMockUser(username = "admin", roles = {SUPER_ADMIN_ROLE_NAME})
class AdminMetadataControllerTest {


    @Autowired AdminMetadataController adminMetadataController;
    @Autowired TenantRepository tenantRepository;
    @Autowired TypeRepository typeRepository;
    @Autowired SubtypeRepository subtypeRepository;
    @Autowired MetadataRepository metadataRepository;
    @Autowired ModelMapper modelMapper;


    @BeforeEach
    void init() {
        this.cleanup();

        Tenant tenant = tenantRepository.save(Tenant.builder().id(UUID.randomUUID().toString()).name("Tenant").build());

        assertNotNull(tenant);

        typeRepository.save(Type.builder().tenant(tenant).id("type01").name("Type 01").build());
        subtypeRepository.save(Subtype.builder().tenant(tenant).id("type01").name("Type 01").build());
    }


    @AfterEach
    void cleanup() {
        subtypeRepository.deleteAll();
        typeRepository.deleteAll();
        metadataRepository.deleteAll();
        tenantRepository.deleteAll();
    }


    @Test
    void createTenant() {

        Subtype subtype = subtypeRepository.findAll().stream().findFirst().orElse(null);
        assertNotNull(subtype);
        Tenant tenant = subtype.getTenant();
        assertNotNull(tenant);

        MetadataDto metadataDto = MetadataDto.builder()
                .key("m01").name("Metadata 01").type(TEXT)
                .restrictedValues(asList("Value 01", "Value 02", "Value 03"))
                .build();

        metadataDto = adminMetadataController.createMetadata(subtype.getTenant().getId(), tenant, metadataDto);

        assertNotNull(metadataDto);
        assertNotNull(metadataDto.getId());
        assertEquals("m01", metadataDto.getKey());
        assertEquals("Metadata 01", metadataDto.getName());
        assertEquals(asList("Value 01", "Value 02", "Value 03"), metadataDto.getRestrictedValues());
    }


    @Test
    void createMetadata_keyTenantUniqueConstraint() {

        Subtype subtype = subtypeRepository.findAll().stream().findFirst().orElse(null);
        assertNotNull(subtype);
        Tenant tenant = subtype.getTenant();
        assertNotNull(tenant);

        MetadataDto metadataDto = MetadataDto.builder().key("1234").name("Meta 01").index(2).type(TEXT).restrictedValues(emptyList()).build();
        adminMetadataController.createMetadata(subtype.getTenant().getId(), subtype.getTenant(), metadataDto);

        try {
            adminMetadataController.createMetadata(subtype.getTenant().getId(), subtype.getTenant(), metadataDto);
            fail();
        } catch (ResponseStatusException e) {
            assertEquals(CONFLICT, e.getStatusCode());
        }
    }


    @Test
    void createMetadata_duplicateValues_lengthLimit() {

        Subtype subtype = subtypeRepository.findAll().stream().findFirst().orElse(null);
        assertNotNull(subtype);
        Tenant tenant = subtype.getTenant();
        assertNotNull(tenant);

        MetadataDto metadataDto = MetadataDto.builder()
                .key("1234").name("Meta 01").index(2)
                .type(TEXT).restrictedValues(singletonList(RandomStringUtils.insecure().nextAlphanumeric(40000)))
                .build();
        try {
            adminMetadataController.createMetadata(subtype.getTenant().getId(), subtype.getTenant(), metadataDto);
            fail();
        } catch (ResponseStatusException e) {
            assertEquals(INSUFFICIENT_STORAGE, e.getStatusCode());
        }
    }


    @Test
    void createMetadata_duplicateValues_duplicates() {

        Subtype subtype = subtypeRepository.findAll().stream().findFirst().orElse(null);
        assertNotNull(subtype);
        Tenant tenant = subtype.getTenant();
        assertNotNull(tenant);

        MetadataDto metadataDto = MetadataDto.builder()
                .key("1234").name("Meta 01").index(2).type(TEXT)
                .restrictedValues(asList("01", "02", "03", "02"))
                .build();

        try {
            adminMetadataController.createMetadata(subtype.getTenant().getId(), subtype.getTenant(), metadataDto);
            fail();
        } catch (ResponseStatusException e) {
            assertEquals(BAD_REQUEST, e.getStatusCode());
        }
    }


    @Test
    void createMetadata_duplicateValues_containingNull() {

        Subtype subtype = subtypeRepository.findAll().stream().findFirst().orElse(null);
        assertNotNull(subtype);
        Tenant tenant = subtype.getTenant();
        assertNotNull(tenant);

        MetadataDto metadataDto = MetadataDto.builder()
                .key("1234").name("Meta 01").index(2).type(TEXT)
                .restrictedValues(singletonList(null))
                .build();

        try {
            adminMetadataController.createMetadata(subtype.getTenant().getId(), subtype.getTenant(), metadataDto);
            fail();
        } catch (ResponseStatusException e) {
            assertEquals(BAD_REQUEST, e.getStatusCode());
        }
    }


    @Test
    void listMetadata() {

        // Prepare

        Subtype subtype = subtypeRepository.findAll().stream().findFirst().orElse(null);
        assertNotNull(subtype);
        Tenant tenant = subtype.getTenant();
        assertNotNull(tenant);

        MetadataDto metadataDto = MetadataDto.builder()
                .key("m01").name("Meta 01").index(2).type(TEXT)
                .restrictedValues(asList("Value 01", "Value 02", "Value 03"))
                .build();

        adminMetadataController.createMetadata(tenant.getId(), tenant, metadataDto);

        // Retrieve and test

        Pageable pageable = PageRequest.of(0, 50, Sort.Direction.ASC, NAME_VALUE);
        Page<MetadataRepresentation> metadataList = adminMetadataController.listMetadata(tenant.getId(), tenant, false, null, pageable);
        assertNotNull(metadataList);
        assertEquals(1, metadataList.getTotalElements());

        MetadataRepresentation metadataRepresentation = metadataList.getContent().get(0);
        assertNotNull(metadataRepresentation.getId());
        assertNotNull(metadataRepresentation.getName());
        assertNotNull(metadataRepresentation.getIndex());
    }


    @Test
    void listMetadataWithSearchTerms() {

        // Prepare

        Subtype subtype = StreamSupport.stream(subtypeRepository.findAll().spliterator(), false).findFirst().orElse(null);
        assertNotNull(subtype);
        Tenant tenant = subtype.getTenant();
        assertNotNull(tenant);

        MetadataDto metadataDto1 = MetadataDto.builder()
                .key("m01").name("Meta 01").index(2).type(TEXT)
                .restrictedValues(asList("Value 01", "Value 02", "Value 03"))
                .build();

        adminMetadataController.createMetadata(tenant.getId(), tenant, metadataDto1);

        MetadataDto metadataDto2 = MetadataDto.builder()
                .key("m02").name("Meta 02").index(2).type(TEXT)
                .restrictedValues(asList("Value 01", "Value 02", "Value 03"))
                .build();

        adminMetadataController.createMetadata(tenant.getId(), tenant, metadataDto2);

        // Retrieve and test

        Pageable pageable = PageRequest.of(0, 50, Sort.Direction.ASC, NAME_VALUE);
        Page<MetadataRepresentation> metadataList = adminMetadataController.listMetadata(tenant.getId(), tenant, false, "Meta 02", pageable);
        assertNotNull(metadataList);
        assertEquals(1, metadataList.getTotalElements());

        MetadataRepresentation metadataRepresentation = metadataList.getContent().get(0);
        assertNotNull(metadataRepresentation.getId());
        assertEquals("Meta 02", metadataRepresentation.getName());
        assertEquals("m02", metadataRepresentation.getKey());
        assertEquals(2, metadataRepresentation.getIndex());
    }


    @Test
    void editMetadata() {

        // Prepare

        Subtype subtype = subtypeRepository.findAll().stream().findFirst().orElse(null);
        assertNotNull(subtype);
        Tenant tenant = subtype.getTenant();
        assertNotNull(tenant);

        MetadataDto existingMetadata = MetadataDto.builder()
                .name("Metadata bad name")
                .key("m01")
                .type(TEXT)
                .restrictedValues(asList("Value 01", "Value 02", "Value 03"))
                .build();

        existingMetadata = adminMetadataController.createMetadata(tenant.getId(), tenant, existingMetadata);
        Metadata existingMetadataEntity = modelMapper.map(existingMetadata, Metadata.class);
        existingMetadataEntity.setSubtypeMetadataList(emptyList());

        // Update and test

        MetadataDto updatedMetadata = MetadataDto.builder()
                .name("Metadata good name")
                .key("m01")
                .type(TEXT)
                .restrictedValues(asList("Value 03", "Value 02"))
                .build();

        adminMetadataController.updateMetadata(tenant.getId(), existingMetadata.getId(), existingMetadataEntity, updatedMetadata);
    }


    @Test
    void createMetadataFailWhenTenantLimitReached() {

        MetadataRepository metadataRepositoryMock = mock(MetadataRepository.class);

        AdminMetadataController adminMetadataController = new AdminMetadataController(
                new ModelMapper(),
                mock(StatsServiceInterface.class),
                metadataRepositoryMock,
                mock(TableLayoutRepository.class),
                mock(PermissionServiceInterface.class),
                mock(SubtypeMetadataRepository.class),
                mock(WorkflowServiceInterface.class),
                mock(SubtypeRepository.class)
        );

        Tenant dummyTenant = Tenant.builder().id(UUID.randomUUID().toString()).build();
        dummyTenant.setMetadataLimit(0);



        Mockito.when(metadataRepositoryMock.countAllByTenant_Id(anyString()))
                .thenReturn(0L);

        LocalizedStatusException exception = assertThrows(
                LocalizedStatusException.class,
                () -> adminMetadataController.createMetadata(dummyTenant.getId(), dummyTenant, new MetadataDto())
        );
        assertEquals(
                HttpStatus.INSUFFICIENT_STORAGE + " \""
                        + getMessageForLocale("message.already_n_metadata_maximum_reached", Locale.getDefault(), dummyTenant.getMetadataLimit())
                        + "\"",
                exception.getMessage()
        );
    }


}
