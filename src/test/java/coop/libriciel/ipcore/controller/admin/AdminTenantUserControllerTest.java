/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller.admin;

import coop.libriciel.ipcore.business.auth.DeskBusinessService;
import coop.libriciel.ipcore.business.auth.UserBusinessService;
import coop.libriciel.ipcore.business.database.TenantBusinessService;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.auth.requests.UserDto;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.auth.KeycloakService;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.database.DatabaseServiceInterface;
import coop.libriciel.ipcore.services.database.UserPreferencesRepository;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.Locale;
import java.util.UUID;

import static coop.libriciel.ipcore.utils.LocalizedStatusException.getMessageForLocale;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;


class AdminTenantUserControllerTest {

    private AdminTenantUserController adminTenantUserController;

    private @Mock AuthServiceInterface authServiceMock = mock(KeycloakService.class);

    @BeforeEach
    void setup() {
        adminTenantUserController = new AdminTenantUserController(
                authServiceMock,
                mock(ContentServiceInterface.class),
                mock(DatabaseServiceInterface.class),
                mock(DeskBusinessService.class),
                new ModelMapper(),
                mock(PermissionServiceInterface.class),
                mock(StatsServiceInterface.class),
                mock(TenantBusinessService.class),
                mock(UserBusinessService.class),
                mock(UserPreferencesRepository.class)
        );
    }


    @Test
    void testCreateUserFailWhenTenantLimitReached() {

        Tenant dummyTenant = Tenant.builder().id(UUID.randomUUID().toString()).build();
        dummyTenant.setUserLimit(1);

        User dummyUser = new User();

        Mockito.when(authServiceMock.listTenantUsers(any(), any(), any()))
                .thenReturn(new PageImpl<>(List.of(dummyUser)));

        LocalizedStatusException exception = assertThrows(
                LocalizedStatusException.class,
                () -> this.adminTenantUserController.createUser(dummyTenant.getId(), dummyTenant, new UserDto(), dummyUser)
        );
        assertEquals(
                HttpStatus.INSUFFICIENT_STORAGE + " \""
                        + getMessageForLocale("message.already_n_users_maximum_reached", Locale.getDefault(), dummyTenant.getUserLimit())
                        + "\"",
                exception.getMessage()
        );
    }

}
