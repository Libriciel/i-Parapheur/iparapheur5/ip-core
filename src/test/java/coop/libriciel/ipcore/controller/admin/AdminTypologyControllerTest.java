/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller.admin;

import coop.libriciel.ipcore.business.auth.DeskBusinessService;
import coop.libriciel.ipcore.business.typology.SubtypeProperties;
import coop.libriciel.ipcore.business.typology.TypologyBusinessService;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.database.Type;
import coop.libriciel.ipcore.model.database.TypologyRepresentation;
import coop.libriciel.ipcore.model.database.requests.SubtypeDto;
import coop.libriciel.ipcore.model.database.requests.TypeDto;
import coop.libriciel.ipcore.services.database.*;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.services.secret.SecretServiceInterface;
import coop.libriciel.ipcore.services.securemail.SecureMailServiceInterface;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.stream.IntStream;

import static coop.libriciel.ipcore.model.crypto.SignatureFormat.PADES;
import static coop.libriciel.ipcore.model.crypto.SignatureFormat.PES_V2;
import static coop.libriciel.ipcore.services.auth.KeycloakService.SUPER_ADMIN_ROLE_NAME;
import static coop.libriciel.ipcore.utils.LocalizedStatusException.getMessageForLocale;
import static java.lang.Integer.MAX_VALUE;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.springframework.data.domain.PageRequest.of;


@Log4j2
@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@WithMockUser(username = "admin", roles = {SUPER_ADMIN_ROLE_NAME})
class AdminTypologyControllerTest {


    @Autowired AdminTypologyController adminTypologyController;
    @Autowired TypeRepository typeRepository;
    @Autowired TenantRepository tenantRepository;
    @Autowired SubtypeRepository subtypeRepository;
    @Autowired ModelMapper modelMapper;


    // <editor-fold desc="Utils">


    private static TypeDto dummyType(String name) {
        return new TypeDto() {{
            setName(name);
            setSignatureFormat(PES_V2);
        }};
    }


    private static SubtypeDto dummySubtype(Pair<Type, Integer> index, String name) {
        return new SubtypeDto() {{
            setName(index.getKey().getName() + "." + name + "_" + index.getValue());
            setDescription("Description subtype " + index.getKey().getId() + "." + index.getValue());
            setCreationPermittedDeskIds(emptyList());
            setCreationPermittedDesks(emptyList());
            setFilterableByDeskIds(emptyList());
            setFilterableByDesks(emptyList());
            setSubtypeMetadataList(emptyList());
            setSubtypeLayers(emptyList());
        }};
    }


    // </editor-fold desc="Utils">


    @BeforeEach
    void setup() {

        cleanup();

        Tenant tenant = tenantRepository.save(Tenant.builder().id("tenant01").build());

        // Creating 10 types and 10 subtypes for each
        IntStream.range(0, 10)
                .mapToObj(i -> adminTypologyController.createType(tenant.getId(), tenant, dummyType("type_" . concat(String.valueOf(i)))))
                .map(typeDto -> modelMapper.map(typeDto, Type.class))
                .peek(type -> type.setTenant(tenant))
                .flatMap(t -> IntStream.range(0, 10).mapToObj(i -> Pair.of(t, i)))
                .forEach(p -> adminTypologyController.createSubtype(tenant.getId(), p.getKey().getId(), p.getKey(), dummySubtype(p, "subtype")));

        assertEquals(10, typeRepository.count());
        assertEquals(100, subtypeRepository.count());
    }


    @AfterEach
    void cleanup() {
        subtypeRepository.deleteAll();
        typeRepository.deleteAll();
        tenantRepository.deleteAll();
    }


    @Test
    void createType() {

        Tenant defaultTenant = getDefaultTenant();

        TypeDto typeDto = adminTypologyController.createType(defaultTenant.getId(), defaultTenant, dummyType("type_9999"));
        assertNotNull(typeDto);
    }


    @Test
    void list() {

        Tenant defaultTenant = getDefaultTenant();

        Page<TypologyRepresentation> list = adminTypologyController.getTypologyHierarchy(
                defaultTenant.getId(),
                defaultTenant,
                false,
                emptySet(),
                of(0, MAX_VALUE),
                null
        );

        assertEquals(110, list.getContent().size());
        assertEquals(110, list.getTotalElements());

        assertTrue(list.getContent().stream().map(TypologyRepresentation::getId).allMatch(StringUtils::isNotEmpty));
        assertTrue(list.getContent().stream().map(TypologyRepresentation::getName).allMatch(StringUtils::isNotEmpty));

        assertEquals("type_0", list.getContent().get(0).getName());
        assertTrue(list.getContent().stream()
                .skip(1).limit(10)
                .map(TypologyRepresentation::getName)
                .allMatch(s -> StringUtils.startsWith(s, "type_0.subtype_"))
        );
        assertEquals("type_9", list.getContent().get(99).getName());
        assertTrue(list.getContent().stream()
                .skip(100).limit(10)
                .map(TypologyRepresentation::getName)
                .allMatch(s -> StringUtils.startsWith(s, "type_9.subtype_"))
        );
    }


    private Tenant getDefaultTenant() {
        return tenantRepository.findAll().stream()
                .findFirst()
                .orElseThrow(() -> new AssertionError("No default tenant"));
    }


    @Test
    void createTypeWithSameNameInsensitiveCaseFail() {
        Tenant defaultTenant = getDefaultTenant();

        LocalizedStatusException exception = assertThrows(
                LocalizedStatusException.class,
                () -> adminTypologyController.createType(defaultTenant.getId(), defaultTenant, dummyType("TYPE_1"))
        );

        String expectedMessage = LocalizedStatusException.getMessageForLocale("message.already_existing_type_name", Locale.getDefault());
        assertEquals(
                HttpStatus.CONFLICT + " \"" + expectedMessage + "\"",
                exception.getLocalizedMessage()
        );
    }


    @Test
    void createSubTypeWithSameNameFail() {
        Tenant defaultTenant = getDefaultTenant();

        Type type = typeRepository.findAll().stream()
                .findFirst()
                .orElseThrow(() -> new AssertionError("No type found"));

        String name = "subtype";

        LocalizedStatusException exception = assertThrows(
                LocalizedStatusException.class,
                () -> adminTypologyController.createSubtype(defaultTenant.getId(), type.getId(), type, dummySubtype(Pair.of(type, 1), name))
        );

        String expectedMessage = LocalizedStatusException.getMessageForLocale("message.already_existing_subtype_name", Locale.getDefault());
        assertEquals(
                HttpStatus.CONFLICT + " \"" + expectedMessage + "\"",
                exception.getLocalizedMessage()
        );
    }

    @Test
    void createSubTypeFailWhenTenantLimitReached() {
        SubtypeRepository subtypeRepositoryMock = mock(SubtypeRepository.class);

        AdminTypologyController adminTypologyController = new AdminTypologyController(
                mock(DeskBusinessService.class),
                mock(DatabaseServiceInterface.class),
                mock(ExternalSignatureConfigRepository.class),
                modelMapper,
                mock(PermissionServiceInterface.class),
                mock(SecretServiceInterface.class),
                mock(SecureMailServiceInterface.class),
                mock(StatsServiceInterface.class),
                mock(SubtypeLayerRepository.class),
                mock(SubtypeMetadataRepository.class),
                mock(SubtypeProperties.class),
                subtypeRepositoryMock,
                mock(TypeRepository.class),
                mock(TypologyBusinessService.class),
                mock(WorkflowServiceInterface.class)
        );

        Tenant dummyTenant = Tenant.builder().id(UUID.randomUUID().toString()).build();
        dummyTenant.setSubtypeLimit(1);

        Type dummyType = new Type();
        dummyType.setId(UUID.randomUUID().toString());
        dummyType.setTenant(dummyTenant);
        dummyType.setSignatureFormat(PADES);

        Subtype dummySubtype = new Subtype();
        dummySubtype.setId(UUID.randomUUID().toString());

        SubtypeDto newSubtype = new SubtypeDto();
        newSubtype.setId(UUID.randomUUID().toString());
        newSubtype.setName("New");
        newSubtype.setSubtypeMetadataList(emptyList());
        newSubtype.setSubtypeLayers(emptyList());

        Mockito.when(subtypeRepositoryMock.findAllByTenant_Id(any(), any()))
                .thenReturn(new PageImpl<>(List.of(dummySubtype)));

        LocalizedStatusException exception = assertThrows(
                LocalizedStatusException.class,
                () -> adminTypologyController.createSubtype(dummyTenant.getId(), dummyType.getId(), dummyType, newSubtype)
        );
        assertEquals(
                HttpStatus.INSUFFICIENT_STORAGE + " \""
                        + getMessageForLocale("message.already_n_subtypes_maximum_reached", Locale.getDefault(), dummyTenant.getSubtypeLimit())
                        + "\"",
                exception.getMessage()
        );
    }
}
