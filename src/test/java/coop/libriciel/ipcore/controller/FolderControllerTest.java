/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller;

import coop.libriciel.ipcore.business.auth.DeskBusinessService;
import coop.libriciel.ipcore.business.auth.UserBusinessService;
import coop.libriciel.ipcore.business.signatureproof.SignatureReportBusinessService;
import coop.libriciel.ipcore.business.template.TemplateBusinessService;
import coop.libriciel.ipcore.business.typology.TypologyBusinessService;
import coop.libriciel.ipcore.business.workflow.FolderBusinessService;
import coop.libriciel.ipcore.business.workflow.WorkflowBusinessService;
import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.database.Type;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.ipWorkflow.IpWorkflowInstance;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.crypto.CryptoServiceInterface;
import coop.libriciel.ipcore.services.database.*;
import coop.libriciel.ipcore.services.externalsignature.ExternalSignatureService;
import coop.libriciel.ipcore.services.ipng.PendingIpngFolderRepository;
import coop.libriciel.ipcore.services.mail.NotificationServiceInterface;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.services.securemail.SecureMailServiceInterface;
import coop.libriciel.ipcore.services.stats.NoneStatsService;
import coop.libriciel.ipcore.services.stats.StatsServiceInterface;
import coop.libriciel.ipcore.services.workflow.IpWorkflowService;
import coop.libriciel.ipcore.services.workflow.LegacyIdRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM_VALUE;


class FolderControllerTest {


    private FolderController folderController;

    private final ModelMapper modelMapper = new ModelMapper();
    private final ClassLoader classLoader = getClass().getClassLoader();
    private final StatsServiceInterface statsService = new NoneStatsService();

    private @Mock AuthServiceInterface authServiceMock = mock(AuthServiceInterface.class);
    private @Mock ContentServiceInterface contentServiceMock = mock(ContentServiceInterface.class);
    private @Mock CryptoServiceInterface cryptoServiceMock = mock(CryptoServiceInterface.class);
    private @Mock DatabaseServiceInterface dbServiceMock = mock(DatabaseServiceInterface.class);
    private @Mock DeskBusinessService deskBusinessServiceMock = mock(DeskBusinessService.class);
    private @Mock ExternalSignatureService externalSignatureServiceMock = mock(ExternalSignatureService.class);
    private @Mock FolderBusinessService folderBusinessServiceMock = mock(FolderBusinessService.class);
    private @Mock LegacyIdRepository legacyIdRepositoryMock = mock(LegacyIdRepository.class);
    private @Mock MetadataRepository metadataRepository = mock(MetadataRepository.class);
    private @Mock NotificationServiceInterface notificationServiceMock = mock(NotificationServiceInterface.class);
    private @Mock PendingIpngFolderRepository pendingIpngFolderRepository = mock(PendingIpngFolderRepository.class);
    private @Mock PermissionServiceInterface permissionServiceMock = mock(PermissionServiceInterface.class);
    private @Mock SecureMailServiceInterface secureMailServiceMock = mock(SecureMailServiceInterface.class);
    private @Mock SignatureReportBusinessService signatureReportBusinessServiceMock = mock(SignatureReportBusinessService.class);
    private @Mock SubtypeRepository subtypeRepositoryMock = mock(SubtypeRepository.class);
    private @Mock TemplateBusinessService templateBusinessServiceMock = mock(TemplateBusinessService.class);
    private @Mock TypeRepository typeRepositoryMock = mock(TypeRepository.class);
    private @Mock TypologyBusinessService typologyBusinessServiceMock = mock(TypologyBusinessService.class);
    private @Mock UserBusinessService userBusinessService = mock(UserBusinessService.class);
    private @Mock UserPreferencesRepository userPreferencesRepositoryMock = mock(UserPreferencesRepository.class);
    private @Mock WorkflowBusinessService workflowBusinessServiceMock = mock(WorkflowBusinessService.class);
    private @Mock IpWorkflowService workflowServiceMock = mock(IpWorkflowService.class);


    @BeforeEach
    void setup() {
        folderController = new FolderController(
                authServiceMock,
                contentServiceMock,
                cryptoServiceMock,
                dbServiceMock,
                deskBusinessServiceMock,
                externalSignatureServiceMock,
                folderBusinessServiceMock,
                legacyIdRepositoryMock,
                metadataRepository,
                modelMapper,
                notificationServiceMock,
                pendingIpngFolderRepository,
                permissionServiceMock,
                secureMailServiceMock,
                signatureReportBusinessServiceMock,
                statsService,
                subtypeRepositoryMock,
                templateBusinessServiceMock,
                typeRepositoryMock,
                typologyBusinessServiceMock,
                userBusinessService,
                userPreferencesRepositoryMock,
                workflowBusinessServiceMock,
                workflowServiceMock
        );
    }


    @Nested
    class CreateFolderWithPremis {


        @Test
        void minimal() throws IOException {

            Tenant tenant = new Tenant();
            tenant.setId(UUID.randomUUID().toString());
            tenant.setName(RandomStringUtils.insecure().nextAlphanumeric(36));

            Desk originDesk = new Desk();
            originDesk.setId(UUID.randomUUID().toString());
            originDesk.setName(RandomStringUtils.insecure().nextAlphanumeric(36));

            IpWorkflowInstance dummyFolder = new IpWorkflowInstance();
            dummyFolder.setId(UUID.randomUUID().toString());
            dummyFolder.setName(RandomStringUtils.insecure().nextAlphanumeric(36));

            MockMultipartFile premisFile = new MockMultipartFile(
                    "premis.xml",
                    """
                    <?xml version="1.0" encoding="UTF-8"?>
                    <premis xmlns="http://www.loc.gov/premis/v3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="3.0"
                            xsi:schemaLocation="http://www.loc.gov/premis/v3 http://www.loc.gov/standards/premis/v3/premis.xsd">
                      <object xsi:type="intellectualEntity">
                        <objectIdentifier>
                          <objectIdentifierType>folderId</objectIdentifierType>
                          <objectIdentifierValue>0ffde83f-fbd8-11ee-993f-0242ac120014</objectIdentifierValue>
                        </objectIdentifier>
                        <significantProperties>
                          <significantPropertiesType>i_Parapheur_reserved_type</significantPropertiesType>
                          <significantPropertiesValue>CAdES</significantPropertiesValue>
                        </significantProperties>
                        <significantProperties>
                          <significantPropertiesType>i_Parapheur_reserved_subtype</significantPropertiesType>
                          <significantPropertiesValue>Autoseal</significantPropertiesValue>
                        </significantProperties>
                        <originalName>cad1</originalName>
                      </object>
                      <object xsi:type="file">
                        <significantProperties>
                          <significantPropertiesType>i_Parapheur_reserved_mainDocument</significantPropertiesType>
                          <significantPropertiesValue>true</significantPropertiesValue>
                        </significantProperties>
                        <originalName>test.pdf</originalName>
                      </object>
                    </premis>
                    """.getBytes(UTF_8)
            );

            when(folderBusinessServiceMock.createDraftFolder(eq(tenant), eq(originDesk), anyList(), anyList(), any(), any()))
                    .thenReturn(new Folder());

            when(typeRepositoryMock.findByIdAndTenant_Id(any(), any()))
                    .thenReturn(Optional.of(new Type(UUID.randomUUID().toString())));

            when(subtypeRepositoryMock.findByIdAndParentType_IdAndTenant_Id(any(), any(), any()))
                    .thenReturn(Optional.of(new Subtype(UUID.randomUUID().toString())));

            when(workflowServiceMock.listFolders(anyString(), eq(PageRequest.of(0, 1)), eq(null), eq(null), eq(null), eq(null), eq(null)))
                    .thenReturn(new PageImpl<>(List.of(dummyFolder), PageRequest.of(0, 1), 1L));

            try (InputStream fileStream = classLoader.getResourceAsStream("pdf/signature_tag.pdf")) {

                MultipartFile testMultipartFile = new MockMultipartFile("test.pdf", fileStream);
                assertNotNull(testMultipartFile);
                MultipartFile[] testMultipartFileList = List.of(testMultipartFile).toArray(MultipartFile[]::new);

                folderController.createFolder(tenant.getId(), tenant, originDesk.getId(), originDesk, false, premisFile, testMultipartFileList);

                verify(folderBusinessServiceMock, times(1))
                        .createDraftFolder(
                                eq(tenant),
                                eq(originDesk),
                                argThat(mainDocumentList -> mainDocumentList.size() == 1),
                                argThat(List::isEmpty),
                                argThat(List::isEmpty),
                                any()
                        );
            }
        }


        @Test
        void withDetachedSignature() throws IOException {

            Tenant tenant = new Tenant();
            tenant.setId(UUID.randomUUID().toString());
            tenant.setName(RandomStringUtils.insecure().nextAlphanumeric(36));

            Desk originDesk = new Desk();
            originDesk.setId(UUID.randomUUID().toString());
            originDesk.setName(RandomStringUtils.insecure().nextAlphanumeric(36));

            IpWorkflowInstance dummyFolder = new IpWorkflowInstance();
            dummyFolder.setId(UUID.randomUUID().toString());
            dummyFolder.setName(RandomStringUtils.insecure().nextAlphanumeric(36));

            String cadesSignature = """
                                    -----BEGIN PKCS7-----
                                    MIIJHAYJKoZIhvcNAQcCoIIJDTCCCQkCAQExDzANBglghkgBZQMEAgEFADALBgkq
                                    hkiG9w0BBwGgggXSMIIFzjCCA7agAwIBAgIIIccK1MoOECswDQYJKoZIhvcNAQEL
                                    BQAwRTELMAkGA1UEBhMCRlIxEjAQBgNVBAoMCUxpYnJpY2llbDEiMCAGA1UEAwwZ
                                    QUMgTGlicmljaWVsIFBlcnNvbm5lbCBHMjAeFw0yMzExMDkwOTIyNTdaFw0yNjEx
                                    MDgwOTIyNTdaMIHAMQswCQYDVQQGEwJGUjEVMBMGA1UECAwMMzQgLSBIZXJhdWx0
                                    MRQwEgYDVQQHDAtNb250cGVsbGllcjEXMBUGA1UECgwOTGlicmljaWVsIFNDT1Ax
                                    FTATBgNVBAsMDFRlc3QgaW50ZXJuZTEmMCQGA1UEAwwdQWRyaWVuIEJyaWNjaGkg
                                    LSBUZXN0IGludGVybmUxLDAqBgkqhkiG9w0BCQEWHWFkcmllbi5icmljY2hpQGxp
                                    YnJpY2llbC5jb29wMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoMqi
                                    s0P03We38wfXrt2kODPK8Eh7ISl9YtbmATTc+x/YAqKmFHBkkMbVorAnwT6KTFYK
                                    eugxtYL5RoSjIYC/aCttl1/3vsRH/8GKhRAyhza6irMpxeULnHoz0QEixLxeq1C/
                                    Xyh4MJ8K2QRSJK856ZZzA6a5UZCk8SzOmVTKANtZXjecr9sofypG+3oIthVBzeDH
                                    R35NoEzAgEPTfOrRsRjZCnaj0rpjV73tkvveP1rsvHMjV77w0boI4p2AhOlK/1yo
                                    GKh7AknRYMYBWhXYFlZM/30Gt5BzmuB7tSr9W1UBu07G2MR1m9gccxVWO8/6qqcL
                                    Wrr2jsQMXYQSMDI4CwIDAQABo4IBRDCCAUAwDAYDVR0TAQH/BAIwADAfBgNVHSME
                                    GDAWgBTVdZIN8GVa/vG7Ug2y5pmdV79zwDA4BggrBgEFBQcBAQQsMCowKAYIKwYB
                                    BQUHMAGGHGh0dHA6Ly9wa2kubGlicmljaWVsLmZyL29jc3AwKAYDVR0RBCEwH4Ed
                                    YWRyaWVuLmJyaWNjaGlAbGlicmljaWVsLmNvb3AwNAYDVR0lBC0wKwYIKwYBBQUH
                                    AwIGCCsGAQUFBwMEBgorBgEEAYI3CgMMBgkqhkiG9y8BAQUwRgYDVR0fBD8wPTA7
                                    oDmgN4Y1aHR0cDovL3BraS5saWJyaWNpZWwuZnIvYWMtbGlicmljaWVsLXBlcnNv
                                    bm5lbC1nMi5jcmwwHQYDVR0OBBYEFBspHEyo5dTWQdgLKodkkZtVsktXMA4GA1Ud
                                    DwEB/wQEAwIF4DANBgkqhkiG9w0BAQsFAAOCAgEAGjN1YNC/fjNlPBrClznPZJwM
                                    jCmpxMkJS62MKtiBJ9Snx64gmElYlYXvM/DtunByaA3ElgFYzDfXBa1rNOm19orN
                                    pw5RZkOCdNScBKAn6+sIMmz3cRARm5Av06FIumFOVmXXpNMLVNQIdPijEoG7ybOI
                                    9uKWOfPx87l7MJTY/Sjf+L8C9cMfUakekD6WIvlrr1gKHwaEP5e3ppoSY0aqOHtr
                                    0x+lKQrYh88RS1BGqUpMEgmyFnVQ4ymsUY8fwusCdz4NpxGRI6yPaP/KFb23blzF
                                    iE70BOt1DWSRhgy5S+1kA16qbh3Wn/L4juJX3Bt2adFIzy/v14v/KSu1sb+Xn6Bn
                                    q0WQOFd1CjXUFYXmFjomwh1/DFyXdPfUqjXGfNCGRPJ9UsO8VzetbvfZruQKIInD
                                    rN4ZsiQvQ85aEGpskDBwRUpZagLbOsCR8UktOROhtJ0juJjgqZwdgY90uLueOsu6
                                    AX9Zx41JnY0BijiKnWDsqaTYF9wgdPOeVmDjy5W9EsnMLiP7Xtt7qBx6VSrYaH9L
                                    Ezyxgzh4WdTskna2xJ9Npe651if9J31pa/GdwthsFBgFG/u8he0fyZFLxNMQpB7k
                                    Sf1i2hJy18yE7K0k2IIgPXm8n9wsOqzhmi8d1Z1afbBYZ/Wf4l2Tljd85q4ddoYf
                                    aTScEAq4CPvbewDjVOsxggMOMIIDCgIBATBRMEUxCzAJBgNVBAYTAkZSMRIwEAYD
                                    VQQKDAlMaWJyaWNpZWwxIjAgBgNVBAMMGUFDIExpYnJpY2llbCBQZXJzb25uZWwg
                                    RzICCCHHCtTKDhArMA0GCWCGSAFlAwQCAQUAoIIBjjAYBgkqhkiG9w0BCQMxCwYJ
                                    KoZIhvcNAQcBMBwGCSqGSIb3DQEJBTEPFw0yNDA0MTYwOTU5NTdaMC0GCSqGSIb3
                                    DQEJNDEgMB4wDQYJYIZIAWUDBAIBBQChDQYJKoZIhvcNAQELBQAwLgYLKoZIhvcN
                                    AQkQAhExHzAdoAgMBk1leGljb6ENDAtaaWh1YXRhbmVqb6ICMAAwLwYJKoZIhvcN
                                    AQkEMSIEIPNWs4YWpiF+umz+S1fhNaZP59kgqEIfR2xQLmjyqvagMDMGBwQAgZUy
                                    AQExKDAmoCQwIjAgBgNVBEgxGQwXQ2hlZiBkZSBQw7RsZSBTaWduYXR1cmUwgY4G
                                    CyqGSIb3DQEJEAIvMX8wfTB7MHkEIMoaVSpHBDSWouIfKOTePPZ3/SqOfoqufzIY
                                    FgnpXZwtMFUwSaRHMEUxCzAJBgNVBAYTAkZSMRIwEAYDVQQKDAlMaWJyaWNpZWwx
                                    IjAgBgNVBAMMGUFDIExpYnJpY2llbCBQZXJzb25uZWwgRzICCCHHCtTKDhArMA0G
                                    CSqGSIb3DQEBCwUABIIBABVo8NVBtwTE3hBths+GZpwjYpuOjw8umH7E6QEM0IT7
                                    GDafn23rd09LyP48Ws+8Lto7JKJqTG/QMJ2YhatXJEM3LWHFx02KXqpge4W+Zis/
                                    06pkUTLl5yBN6cTmF6wI9TJmJrD21edhDFuGKMpFhBTFhEHEHcCsOVqja/Fky4pc
                                    9HObP/Zn19jGg4vUPCm4G9Q99qS/NZVaEByLWn1MfMWchQKZnR3jnjuUqJmrQM1m
                                    5FVGGgrivbcOqzQiaS0VV+5AptuKkLpHrBUKcixau72Ve0p0ogwp0cA20tqaOaNJ
                                    9D1uyJvnfSb3efkZkwXuHDnPOX8mFv6RN0vaDw7YX9o=
                                    -----END PKCS7-----\
                                    """;

            MockMultipartFile premisFile = new MockMultipartFile(
                    "premis.xml",
                    """
                    <?xml version="1.0" encoding="UTF-8"?>
                    <premis xmlns="http://www.loc.gov/premis/v3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="3.0"
                            xsi:schemaLocation="http://www.loc.gov/premis/v3 http://www.loc.gov/standards/premis/v3/premis.xsd">
                      <object xsi:type="intellectualEntity">
                        <objectIdentifier>
                          <objectIdentifierType>folderId</objectIdentifierType>
                          <objectIdentifierValue>0ffde83f-fbd8-11ee-993f-0242ac120014</objectIdentifierValue>
                        </objectIdentifier>
                        <significantProperties>
                          <significantPropertiesType>i_Parapheur_reserved_type</significantPropertiesType>
                          <significantPropertiesValue>CAdES</significantPropertiesValue>
                        </significantProperties>
                        <significantProperties>
                          <significantPropertiesType>i_Parapheur_reserved_subtype</significantPropertiesType>
                          <significantPropertiesValue>Autoseal</significantPropertiesValue>
                        </significantProperties>
                        <originalName>cad1</originalName>
                      </object>
                      <object xsi:type="file">
                        <significantProperties>
                          <significantPropertiesType>i_Parapheur_reserved_mainDocument</significantPropertiesType>
                          <significantPropertiesValue>true</significantPropertiesValue>
                        </significantProperties>
                        <originalName>signature_tag.pdf</originalName>
                        <signatureInformation>
                          <signature>
                            <signatureEncoding>UTF-8</signatureEncoding>
                            <signatureMethod>CAdES_BASELINE_B</signatureMethod>
                            <signatureValue>%s</signatureValue>
                            <signatureValidationRules>CAdES</signatureValidationRules>
                          </signature>
                        </signatureInformation>
                      </object>
                    </premis>
                    """.formatted(cadesSignature)
                            .getBytes(UTF_8)
            );

            when(folderBusinessServiceMock.createDraftFolder(eq(tenant), eq(originDesk), anyList(), anyList(), any(), any()))
                    .thenReturn(new Folder());

            when(typeRepositoryMock.findByIdAndTenant_Id(any(), any()))
                    .thenReturn(Optional.of(new Type(UUID.randomUUID().toString())));

            when(subtypeRepositoryMock.findByIdAndParentType_IdAndTenant_Id(any(), any(), any()))
                    .thenReturn(Optional.of(new Subtype(UUID.randomUUID().toString())));

            when(workflowServiceMock.listFolders(anyString(), eq(PageRequest.of(0, 1)), eq(null), eq(null), eq(null), eq(null), eq(null)))
                    .thenReturn(new PageImpl<>(List.of(dummyFolder), PageRequest.of(0, 1), 1L));

            try (InputStream fileStream = classLoader.getResourceAsStream("pdf/signature_tag.pdf")) {

                MultipartFile testMultipartFile = new MockMultipartFile("signature_tag.pdf", fileStream);
                assertNotNull(testMultipartFile);
                MultipartFile[] testMultipartFileList = List.of(testMultipartFile).toArray(MultipartFile[]::new);

                folderController.createFolder(tenant.getId(), tenant, originDesk.getId(), originDesk, false, premisFile, testMultipartFileList);

                verify(folderBusinessServiceMock, times(1))
                        .createDraftFolder(
                                eq(tenant),
                                eq(originDesk),
                                argThat(mainDocumentList -> {
                                    assertEquals(1, mainDocumentList.size());
                                    return true;
                                }),
                                argThat(List::isEmpty),
                                argThat(signatureList -> {
                                    assertEquals(1, signatureList.size());

                                    MultipartFile signature = signatureList.get(0);
                                    assertNotNull(signature);
                                    assertNotNull(signature.getName());
                                    assertEquals(APPLICATION_OCTET_STREAM_VALUE, signature.getContentType());

                                    try {
                                        assertEquals(cadesSignature, new String(signature.getBytes(), UTF_8));
                                        // This block is called twice, the inputStream has to be reset
                                        signature.getInputStream().reset();
                                    } catch (IOException e) {
                                        throw new RuntimeException(e);
                                    }
                                    return true;
                                }),
                                argThat(request -> {
                                    assertNotNull(request.getDetachedSignaturesMapping());
                                    List<String> firstDocumentSignatures = request.getDetachedSignaturesMapping().get("signature_tag.pdf");
                                    assertNotNull(firstDocumentSignatures);
                                    assertEquals(1, firstDocumentSignatures.size());
                                    assertTrue(StringUtils.isNotEmpty(firstDocumentSignatures.get(0)));
                                    return true;
                                })
                        );
            }
        }


    }


}
