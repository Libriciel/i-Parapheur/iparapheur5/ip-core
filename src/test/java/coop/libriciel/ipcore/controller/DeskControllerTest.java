/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.controller;

import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.database.Type;
import coop.libriciel.ipcore.services.database.SubtypeRepository;
import coop.libriciel.ipcore.services.database.TenantRepository;
import coop.libriciel.ipcore.services.database.TypeRepository;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.stream.IntStream;

import static coop.libriciel.ipcore.model.crypto.SignatureFormat.PES_V2;
import static coop.libriciel.ipcore.services.auth.KeycloakService.DESK_ROLE_PREFIX;
import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@WithMockUser(
        username = "Jean",
        roles = {DESK_ROLE_PREFIX + "desk0", DESK_ROLE_PREFIX + "desk1"}
)
public class DeskControllerTest {


    @Autowired TypeRepository typeRepository;
    @Autowired TenantRepository tenantRepository;
    @Autowired SubtypeRepository subtypeRepository;


    // <editor-fold desc="Utils">


    private static Type dummyType(int id) {
        return Type.builder()
                .tenant(Tenant.builder().id("tenant01").build())
                .id("type_" + id)
                .name("Type " + id)
                .signatureFormat(PES_V2)
                .build();
    }


    private static Subtype dummySubtype(Pair<Type, Integer> index) {
        return Subtype.builder()
                .id("subtype_" + index.getKey().getId() + "." + index.getValue())
                .name("Subtype " + index.getKey().getId() + "." + index.getValue())
                .parentType(index.getKey())
                .build();
    }


    // </editor-fold desc="Utils">


    @BeforeEach
    void setup() {
        this.cleanup();

        tenantRepository.save(Tenant.builder().id("tenant01").build());

        // Creating 10 types and 10 subtypes for each

        IntStream.range(0, 10)
                .mapToObj(i -> typeRepository.save(dummyType(i)))
                .flatMap(t -> IntStream.range(0, 10).mapToObj(i -> Pair.of(t, i)))
                .forEach(p -> subtypeRepository.save(dummySubtype(p)));

        assertEquals(10, typeRepository.count());
        assertEquals(100, subtypeRepository.count());
    }


    @AfterEach
    void cleanup() {
        subtypeRepository.deleteAll();
        typeRepository.deleteAll();
        tenantRepository.deleteAll();
    }


//    @Test
//    void getTypes_permissionEveryone() {
//        assertEquals(10, deskController.getTypes("tenant01", "desk0", 0, 50).getData().size());
//        assertEquals(10, deskController.getTypes("tenant01", "desk1", 0, 50).getData().size());
//    }


//    @Test
//    void getTypes_pagination() {
//        assertEquals(5, deskController.getTypes("tenant01", "desk0", 1, 5).getData().size());
//        assertEquals(0, deskController.getTypes("tenant01", "desk0", 2, 5).getData().size());
//        assertEquals(10, deskController.getTypes("tenant01", "desk0", 2, 5).getTotal());
//    }


//    @Test
//    void getTypes_permissionReduced() {
//
//        List<Subtype> allSubtypes = StreamSupport.stream(subtypeRepository.findAll().spliterator(), false).toList();
//        allSubtypes.stream().limit(15).forEach(s -> s.getCreationPermittedRoles().add("desk0"));
//        subtypeRepository.saveAll(allSubtypes);
//
//        assertEquals(10, deskController.getTypes("tenant01", "desk0", 0, 50).getData().size());
//        assertEquals(9, deskController.getTypes("tenant01", "desk1", 0, 50).getData().size());
//        assertEquals("type_0", deskController.getTypes("tenant01", "desk0", 0, 50).getData().get(0).getId());
//        assertEquals("type_1", deskController.getTypes("tenant01", "desk0", 0, 50).getData().get(1).getId());
//
//        allSubtypes.stream().skip(5).limit(25).forEach(s -> s.getCreationPermittedRoles().add("desk1"));
//        subtypeRepository.saveAll(allSubtypes);
//
//        assertEquals(9, deskController.getTypes("tenant01", "desk0", 0, 50).getData().size());
//        assertEquals(10, deskController.getTypes("tenant01", "desk1", 0, 50).getData().size());
//        assertEquals("type_0", deskController.getTypes("tenant01", "desk1", 0, 50).getData().get(0).getId());
//        assertEquals("type_1", deskController.getTypes("tenant01", "desk1", 0, 50).getData().get(1).getId());
//    }


//    @Test
//    void getSubtypes_permissionEveryone() {
//        assertEquals(10, deskController.getSubtypes("tenant01", "desk0", "type_0", 0, 50).getData().size());
//        assertEquals(10, deskController.getSubtypes("tenant01", "desk0", "type_1", 0, 50).getData().size());
//        assertEquals(10, deskController.getSubtypes("tenant01", "desk1", "type_0", 0, 50).getData().size());
//        assertEquals(10, deskController.getSubtypes("tenant01", "desk1", "type_1", 0, 50).getData().size());
//    }
//
//
//    @Test
//    void getSubtypes_pagination() {
//        assertEquals(5, deskController.getSubtypes("tenant01", "desk0", "type_0", 1, 5).getData().size());
//        assertEquals(0, deskController.getSubtypes("tenant01", "desk0", "type_0", 2, 5).getData().size());
//        assertEquals(10, deskController.getSubtypes("tenant01", "desk0", "type_0", 2, 5).getTotal());
//    }


//    @Test
//    void getSubtypes_permissionReduced() {
//
//        List<Subtype> allSubtypes = StreamSupport.stream(subtypeRepository.findAll().spliterator(), false).toList();
//        allSubtypes.stream().limit(15).forEach(s -> s.getCreationPermittedRoles().add("desk_0"));
//        subtypeRepository.saveAll(allSubtypes);
//
//        assertEquals(10, deskController.getSubtypes("tenant01", "desk_0", "type_0", 0, 50).getData().size());
//        assertEquals(10, deskController.getSubtypes("tenant01", "desk_0", "type_1", 0, 50).getData().size());
//        assertEquals(0, deskController.getSubtypes("tenant01", "desk_1", "type_0", 0, 50).getData().size());
//        assertEquals(5, deskController.getSubtypes("tenant01", "desk_1", "type_1", 0, 50).getData().size());
//
//        allSubtypes.stream().skip(5).limit(15).forEach(s -> s.getCreationPermittedRoles().add("desk_1"));
//        subtypeRepository.saveAll(allSubtypes);
//
//        assertEquals(20, deskController.getSubtypes("tenant01", "desk_0", "type_0", 0, 50).getData().size());
//        assertEquals(10, deskController.getSubtypes("tenant01", "desk_0", "type_1", 0, 50).getData().size());
//        assertEquals(5, deskController.getSubtypes("tenant01", "desk_1", "type_0", 0, 50).getData().size());
//        assertEquals(10, deskController.getSubtypes("tenant01", "desk_1", "type_1", 0, 50).getData().size());
//    }


//    @Test
//    void getSubtypes_error404() {
//        try {
//            deskController.getSubtypes("tenant01", "desk_0", "bad_id", 0, 50);
//            fail();
//        } catch (ResponseStatusException e) {
//            assertEquals(NOT_FOUND, e.getStatus());
//        }
//    }

}
