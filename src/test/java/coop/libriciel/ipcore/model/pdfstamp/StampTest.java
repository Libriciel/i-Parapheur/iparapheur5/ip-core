/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.pdfstamp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import static coop.libriciel.ipcore.model.pdfstamp.StampTextColor.RED;
import static coop.libriciel.ipcore.model.pdfstamp.StampType.METADATA;
import static org.junit.jupiter.api.Assertions.*;


class StampTest {


    @Test
    void deserialize() throws JsonProcessingException {

        String json =
                """
                {
                  "id": "1234",
                  "page": 1,
                  "x": 2,
                  "y": 3,
                  "signatureRank": 4,
                  "afterSignature": true,
                  "type": "METADATA",
                  "value": "9876",
                  "fontSize": 11,
                  "textColor": "RED"
                }
                """;

        Stamp stamp = new ObjectMapper().readValue(json, Stamp.class);

        assertNotNull(stamp);
        assertEquals("1234", stamp.getId());
        assertEquals(1, stamp.getPage());
        assertEquals(2, stamp.getX());
        assertEquals(3, stamp.getY());
        assertEquals(4, stamp.getSignatureRank());
        assertTrue(stamp.isAfterSignature());
        assertEquals("9876", stamp.getValue());
        assertEquals(11, stamp.getFontSize());
        assertEquals(METADATA, stamp.getType());
        assertEquals(RED, stamp.getTextColor());
    }

}
