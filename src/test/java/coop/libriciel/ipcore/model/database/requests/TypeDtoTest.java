/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.database.requests;

import coop.libriciel.ipcore.model.crypto.PdfSignaturePosition;
import coop.libriciel.ipcore.model.database.Type;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.UUID;

import static coop.libriciel.ipcore.model.crypto.SignatureFormat.PES_V2;
import static coop.libriciel.ipcore.model.database.Type.SignatureProtocol.HELIOS;
import static coop.libriciel.ipcore.model.pdfstamp.Origin.BOTTOM_LEFT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest
class TypeDtoTest {


    private @Autowired ModelMapper modelMapper;


    private static void assertDeepEquals(Type entity, TypeDto dto) {
        assertEquals(entity.getId(), dto.getId());
        assertEquals(entity.getName(), dto.getName());
        assertEquals(entity.getSignatureFormat(), dto.getSignatureFormat());
        assertEquals(entity.getProtocol(), dto.getProtocol());
        assertEquals(entity.isSignatureVisible(), dto.isSignatureVisible());
        assertEquals(entity.getSignaturePosition(), dto.getSignaturePosition());
        assertEquals(entity.getSignatureLocation(), dto.getSignatureLocation());
        assertEquals(entity.getSignatureZipCode(), dto.getSignatureZipCode());
    }


    @Test
    void convertEntityToDto_full() {

        Type entity = new Type();
        entity.setId(UUID.randomUUID().toString());
        entity.setName(RandomStringUtils.insecure().nextAlphanumeric(30));
        entity.setDescription(RandomStringUtils.insecure().nextAlphanumeric(30));
        entity.setSignatureFormat(PES_V2);
        entity.setProtocol(HELIOS);
        entity.setSignatureVisible(true);
        entity.setSignaturePosition(new PdfSignaturePosition(1, 2, 3, BOTTOM_LEFT, null));
        entity.setSignatureLocation("Montpellier");
        entity.setSignatureZipCode("34000");

        TypeDto dto = modelMapper.map(entity, TypeDto.class);
        assertNotNull(dto);
        assertDeepEquals(entity, dto);
    }


    @Test
    void convertEntityToDto_empty() {

        Type entity = new Type();

        TypeDto dto = modelMapper.map(entity, TypeDto.class);
        assertNotNull(dto);
        assertDeepEquals(entity, dto);
    }


    @Test
    void convertDtoToEntity_full() {

        TypeDto dto = new TypeDto();
        dto.setId(UUID.randomUUID().toString());
        dto.setName(RandomStringUtils.insecure().nextAlphanumeric(30));
        dto.setDescription(RandomStringUtils.insecure().nextAlphanumeric(30));
        dto.setSignatureFormat(PES_V2);
        dto.setProtocol(HELIOS);
        dto.setSignatureVisible(true);
        dto.setSignaturePosition(new PdfSignaturePosition(1, 2, 3, BOTTOM_LEFT, null));
        dto.setSignatureLocation("Montpellier");
        dto.setSignatureZipCode("34000");

        Type entity = modelMapper.map(dto, Type.class);
        assertNotNull(entity);
        assertDeepEquals(entity, dto);
    }


    @Test
    void convertDtoToEntity_empty() {

        TypeDto dto = new TypeDto();

        Type entity = modelMapper.map(dto, Type.class);
        assertNotNull(entity);
        assertDeepEquals(entity, dto);
    }


}
