/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.database;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Random;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest
class TypologyRepresentationTest {


    private @Autowired ModelMapper modelMapper;


    private static void assertDeepEquals(TypologyEntity entity, TypologyRepresentation dto) {
        assertEquals(entity.getId(), dto.getId());
        assertEquals(entity.getName(), dto.getName());
    }


    @Test
    void convertEntityToDto_full() {

        TypologyEntity entity = new TypologyEntity();
        entity.setId(UUID.randomUUID().toString());
        entity.setName(RandomStringUtils.insecure().nextAlphanumeric(30));

        TypologyRepresentation dto = modelMapper.map(entity, TypologyRepresentation.class);
        assertNotNull(dto);
        assertDeepEquals(entity, dto);
    }


    @Test
    void convertEntityToDto_empty() {

        TypologyEntity entity = new TypologyEntity();

        TypologyRepresentation dto = modelMapper.map(entity, TypologyRepresentation.class);
        assertNotNull(dto);
        assertDeepEquals(entity, dto);
    }


    @Test
    void convertDtoToEntity_full() {

        TypologyRepresentation dto = new TypologyRepresentation();
        dto.setId(UUID.randomUUID().toString());
        dto.setName(RandomStringUtils.insecure().nextAlphanumeric(30));
        dto.setParentId(UUID.randomUUID().toString());
        dto.setChildrenCount(new Random().nextInt(99));

        TypologyEntity entity = modelMapper.map(dto, TypologyEntity.class);
        assertNotNull(entity);
        assertDeepEquals(entity, dto);
    }


    @Test
    void convertDtoToEntity_empty() {

        TypologyRepresentation dto = new TypologyRepresentation();

        TypologyEntity entity = modelMapper.map(dto, TypologyEntity.class);
        assertNotNull(entity);
        assertDeepEquals(entity, dto);
    }


}
