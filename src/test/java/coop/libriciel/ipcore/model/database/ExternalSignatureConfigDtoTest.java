/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.database;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static coop.libriciel.ipcore.model.database.ExternalSignatureProvider.YOUSIGN_V2;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest
class ExternalSignatureConfigDtoTest {


    private @Autowired ModelMapper modelMapper;


    private static void assertDeepEquals(ExternalSignatureConfig entity, ExternalSignatureConfigDto dto) {
        assertEquals(entity.getName(), dto.getName());
        assertEquals(entity.getServiceName(), dto.getServiceName());
        assertEquals(entity.getUrl(), dto.getUrl());
        assertEquals(entity.getToken(), dto.getToken());
        assertEquals(entity.getPassword(), dto.getPassword());
        assertEquals(entity.getLogin(), dto.getLogin());
    }


    @Test
    void convertEntityToDto_full() {

        ExternalSignatureConfig entity = new ExternalSignatureConfig();
        entity.setName(RandomStringUtils.insecure().nextAlphanumeric(30));
        entity.setServiceName(YOUSIGN_V2);
        entity.setUrl(RandomStringUtils.insecure().nextAlphanumeric(30));
        entity.setToken(RandomStringUtils.insecure().nextAlphanumeric(30));
        entity.setPassword(RandomStringUtils.insecure().nextAlphanumeric(30));
        entity.setLogin(RandomStringUtils.insecure().nextAlphanumeric(30));

        ExternalSignatureConfigDto dto = modelMapper.map(entity, ExternalSignatureConfigDto.class);
        assertNotNull(dto);
        assertDeepEquals(entity, dto);
    }


    @Test
    void convertEntityToDto_empty() {

        ExternalSignatureConfig entity = new ExternalSignatureConfig();

        ExternalSignatureConfigDto dto = modelMapper.map(entity, ExternalSignatureConfigDto.class);
        assertNotNull(dto);
        assertDeepEquals(entity, dto);
    }


    @Test
    void convertDtoToEntity_full() {

        ExternalSignatureConfigDto dto = new ExternalSignatureConfigDto();
        dto.setName(RandomStringUtils.insecure().nextAlphanumeric(30));
        dto.setServiceName(YOUSIGN_V2);
        dto.setUrl(RandomStringUtils.insecure().nextAlphanumeric(30));
        dto.setToken(RandomStringUtils.insecure().nextAlphanumeric(30));
        dto.setPassword(RandomStringUtils.insecure().nextAlphanumeric(30));
        dto.setLogin(RandomStringUtils.insecure().nextAlphanumeric(30));

        ExternalSignatureConfig entity = modelMapper.map(dto, ExternalSignatureConfig.class);
        assertNotNull(entity);
        assertDeepEquals(entity, dto);
    }


    @Test
    void convertDtoToEntity_empty() {

        ExternalSignatureConfigDto dto = new ExternalSignatureConfigDto();

        ExternalSignatureConfig entity = modelMapper.map(dto, ExternalSignatureConfig.class);
        assertNotNull(entity);
        assertDeepEquals(entity, dto);
    }


}
