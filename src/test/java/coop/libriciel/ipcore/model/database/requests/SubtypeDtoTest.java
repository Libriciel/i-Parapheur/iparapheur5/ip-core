/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.database.requests;

import coop.libriciel.ipcore.model.database.ExternalSignatureConfig;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.services.content.ContentServiceProperties;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;
import java.util.Random;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest
class SubtypeDtoTest {


    private @Autowired ModelMapper modelMapper;
    private @Autowired ContentServiceProperties contentServiceProperties;


    private void assertDeepEquals(Subtype entity, SubtypeDto dto) {
        assertEquals(entity.getId(), dto.getId());
        assertEquals(entity.getName(), dto.getName());
        assertEquals(entity.getDescription(), dto.getDescription());
        assertEquals(entity.getCreationWorkflowId(), dto.getCreationWorkflowId());
        assertEquals(entity.getValidationWorkflowId(), dto.getValidationWorkflowId());
        assertEquals(entity.getWorkflowSelectionScript(), dto.getWorkflowSelectionScript());
        assertEquals(entity.isReadingMandatory(), dto.isReadingMandatory());
        assertEquals(entity.isDigitalSignatureMandatory(), dto.isDigitalSignatureMandatory());
        assertEquals(entity.isAnnexeIncluded(), dto.isAnnexeIncluded());
        assertEquals(entity.isAnnotationsAllowed(), dto.isAnnotationsAllowed());
        assertEquals(entity.getSecureMailServerId(), dto.getSecureMailServerId());
        assertEquals(entity.getSealCertificateId(), dto.getSealCertificateId());
        assertEquals(entity.isSealAutomatic(), dto.isSealAutomatic());
        assertEquals(
                Optional.ofNullable(entity.getExternalSignatureConfig()).map(ExternalSignatureConfig::getId).orElse(null),
                dto.getExternalSignatureConfigId()
        );
        assertEquals(entity.isExternalSignatureAutomatic(), dto.isExternalSignatureAutomatic());

        assertEquals(entity.isMultiDocuments(), dto.isMultiDocuments());
        assertEquals(!entity.isMultiDocuments(), dto.getMaxMainDocuments() == 1);
        assertEquals(entity.isMultiDocuments(), dto.getMaxMainDocuments() == contentServiceProperties.getMaxMainFiles());
    }


    @Test
    void convertEntityToDto_full() {

        Subtype entity = new Subtype();
        entity.setId(UUID.randomUUID().toString());
        entity.setName(RandomStringUtils.insecure().nextAlphanumeric(30));
        entity.setDescription(RandomStringUtils.insecure().nextAlphanumeric(30));
        entity.setCreationWorkflowId(UUID.randomUUID().toString());
        entity.setValidationWorkflowId(UUID.randomUUID().toString());
        entity.setWorkflowSelectionScript(RandomStringUtils.insecure().nextAlphanumeric(30));
        entity.setReadingMandatory(true);
        entity.setDigitalSignatureMandatory(true);
        entity.setAnnexeIncluded(true);
        entity.setMultiDocuments(true);
        entity.setAnnotationsAllowed(true);
        entity.setSecureMailServerId(new Random().nextLong());
        entity.setSealCertificateId(UUID.randomUUID().toString());
        entity.setSealAutomatic(true);
        entity.setExternalSignatureConfig(ExternalSignatureConfig.builder().id(UUID.randomUUID().toString()).build());
        entity.setExternalSignatureAutomatic(true);

        SubtypeDto dto = modelMapper.map(entity, SubtypeDto.class);
        assertNotNull(dto);
        assertDeepEquals(entity, dto);
    }


    @Test
    void convertEntityToDto_empty() {

        Subtype entity = new Subtype();

        SubtypeDto dto = modelMapper.map(entity, SubtypeDto.class);
        assertNotNull(dto);
        assertDeepEquals(entity, dto);
    }


    @Test
    void convertDtoToEntity_full() {

        SubtypeDto dto = new SubtypeDto();
        dto.setId(UUID.randomUUID().toString());
        dto.setName(RandomStringUtils.insecure().nextAlphanumeric(30));
        dto.setDescription(RandomStringUtils.insecure().nextAlphanumeric(30));
        dto.setCreationWorkflowId(UUID.randomUUID().toString());
        dto.setValidationWorkflowId(UUID.randomUUID().toString());
        dto.setWorkflowSelectionScript(RandomStringUtils.insecure().nextAlphanumeric(30));
        dto.setReadingMandatory(true);
        dto.setDigitalSignatureMandatory(true);
        dto.setAnnexeIncluded(true);
        dto.setMultiDocuments(true);
        dto.setAnnotationsAllowed(true);
        dto.setSecureMailServerId(new Random().nextLong());
        dto.setSealCertificateId(UUID.randomUUID().toString());
        dto.setSealAutomatic(true);
        dto.setExternalSignatureConfigId(UUID.randomUUID().toString());
        dto.setExternalSignatureAutomatic(true);

        // This is a read-only value, we force it here to simplify the upcoming comparison
        dto.setMaxMainDocuments(contentServiceProperties.getMaxMainFiles());

        Subtype entity = modelMapper.map(dto, Subtype.class);
        assertNotNull(entity);
        assertDeepEquals(entity, dto);
    }


    @Test
    void convertDtoToEntity_empty() {

        SubtypeDto dto = new SubtypeDto();

        // This is a read-only value, we force it here to simplify the upcoming comparison
        dto.setMaxMainDocuments(1);

        Subtype entity = modelMapper.map(dto, Subtype.class);
        assertNotNull(entity);
        assertDeepEquals(entity, dto);
    }


}
