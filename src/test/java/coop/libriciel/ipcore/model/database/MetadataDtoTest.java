/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.database;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Random;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest
class MetadataDtoTest {


    private @Autowired ModelMapper modelMapper;


    private static void assertDeepEquals(Metadata entity, MetadataDto dto) {
        assertEquals(entity.getId(), dto.getId());
        assertEquals(entity.getKey(), dto.getKey());
        assertEquals(entity.getName(), dto.getName());
        assertEquals(entity.getIndex(), dto.getIndex());
        assertEquals(entity.getType(), dto.getType());
        assertEquals(entity.getRestrictedValues(), dto.getRestrictedValues());
    }


    @Test
    void convertEntityToDto_full() {

        Metadata entity = new Metadata();
        entity.setId(UUID.randomUUID().toString());
        entity.setKey(RandomStringUtils.insecure().nextAlphanumeric(30));
        entity.setName(RandomStringUtils.insecure().nextAlphanumeric(30));
        entity.setIndex(new Random().nextInt(99));
        entity.setType(MetadataType.DATE);
        entity.setRestrictedValues(List.of(
                RandomStringUtils.insecure().nextAlphanumeric(30),
                RandomStringUtils.insecure().nextAlphanumeric(30))
        );

        MetadataDto dto = modelMapper.map(entity, MetadataDto.class);
        assertNotNull(dto);
        assertDeepEquals(entity, dto);
    }


    @Test
    void convertEntityToDto_empty() {

        Metadata entity = new Metadata();

        MetadataDto dto = modelMapper.map(entity, MetadataDto.class);
        assertNotNull(dto);
        assertDeepEquals(entity, dto);
    }


    @Test
    void convertDtoToEntity_full() {

        MetadataDto dto = new MetadataDto();
        dto.setId(UUID.randomUUID().toString());
        dto.setKey(RandomStringUtils.insecure().nextAlphanumeric(30));
        dto.setName(RandomStringUtils.insecure().nextAlphanumeric(30));
        dto.setIndex(new Random().nextInt(99));
        dto.setType(MetadataType.DATE);
        dto.setRestrictedValues(List.of(
                RandomStringUtils.insecure().nextAlphanumeric(30),
                RandomStringUtils.insecure().nextAlphanumeric(30))
        );

        Metadata entity = modelMapper.map(dto, Metadata.class);
        assertNotNull(entity);
        assertDeepEquals(entity, dto);
    }


    @Test
    void convertDtoToEntity_empty() {

        MetadataDto dto = new MetadataDto();

        Metadata entity = modelMapper.map(dto, Metadata.class);
        assertNotNull(entity);
        assertDeepEquals(entity, dto);
    }


}
