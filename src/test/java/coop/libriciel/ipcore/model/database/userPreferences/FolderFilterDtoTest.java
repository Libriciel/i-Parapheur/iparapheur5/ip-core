/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.database.userPreferences;

import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Type;
import coop.libriciel.ipcore.model.database.TypologyRepresentation;
import coop.libriciel.ipcore.model.workflow.State;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
class FolderFilterDtoTest {

    private @Autowired ModelMapper modelMapper;


    static void assertTypeMatchesRep(Type type, TypologyRepresentation typoRep) {
        if (type == null) {
            assertNull(typoRep);
            return;
        }

        assertEquals(type.getId(), typoRep.getId());
        assertEquals(type.getName(), typoRep.getName());
    }


    static void assertSubtypeMatchesRep(Subtype subtype, TypologyRepresentation typoRep) {
        if (subtype == null) {
            assertNull(typoRep);
            return;
        }

        assertEquals(subtype.getId(), typoRep.getId());
        assertEquals(subtype.getName(), typoRep.getName());
    }


    static void assertTypeMatchesId(Type type, String id) {
        if (id == null) {
            assertNull(type);
            return;
        }

        assertEquals(id, type.getId());
    }


    static void assertSubtypeMatchesId(Subtype subtype, String id) {
        if (id == null) {
            assertNull(subtype);
            return;
        }

        assertEquals(id, subtype.getId());
    }


    private static void assertDeepEqualsCommonFields(FolderFilter filter, FolderFilterDto dto) {
        assertEquals(filter.getFilterName(), dto.getFilterName());
        assertEquals(filter.getSearchData(), dto.getSearchData());
        //noinspection deprecation
        assertEquals(filter.getLegacyId(), dto.getLegacyId());
        assertEquals(filter.getState(), dto.getState());

    }


    private static void assertDeepEqualsFromDbToDto(FolderFilter filter, FolderFilterDto dto) {
        assertDeepEqualsCommonFields(filter, dto);
        assertEquals(filter.getId(), dto.getId());
        assertEquals(filter.getUserId(), dto.getUserId());
        assertTypeMatchesRep(filter.getType(), dto.getType());
        assertSubtypeMatchesRep(filter.getSubtype(), dto.getSubtype());
        assertEquals(filter.getCreatedAfter(), dto.getCreatedAfter());
        assertEquals(filter.getCreatedBefore(), dto.getCreatedBefore());

    }


    private static void assertDeepEqualsFromDtoToDb(FolderFilter filter, FolderFilterDto dto) {
        assertDeepEqualsCommonFields(filter, dto);
        assertTypeMatchesId(filter.getType(), dto.getTypeId());
        assertSubtypeMatchesId(filter.getSubtype(), dto.getSubtypeId());
    }


    @Test
    void convertFilterToDto_full() {

        FolderFilter filter = new FolderFilter();
        filter.setId(UUID.randomUUID().toString());
        filter.setUserId(UUID.randomUUID().toString());

        filter.setLegacyId(UUID.randomUUID().toString());

        filter.setFilterName(RandomStringUtils.insecure().nextAlphanumeric(30));
        filter.setSearchData(RandomStringUtils.insecure().nextAlphanumeric(30));
        filter.setState(State.CURRENT);

        Date d1 = new Date();
        Date d2 = new Date();
        d2.setTime(d2.getTime() + 84000);
        filter.setCreatedAfter(d1);
        filter.setCreatedBefore(d2);

        filter.setType(Type.builder().id(UUID.randomUUID().toString()).build());
        filter.setSubtype(Subtype.builder().id(UUID.randomUUID().toString()).build());


        FolderFilterDto dto = modelMapper.map(filter, FolderFilterDto.class);
        assertNotNull(dto);
        assertDeepEqualsFromDbToDto(filter, dto);
    }


    @Test
    void convertFilterToDto_empty() {

        FolderFilter filter = new FolderFilter();

        FolderFilterDto dto = modelMapper.map(filter, FolderFilterDto.class);
        assertNotNull(dto);
        assertDeepEqualsFromDbToDto(filter, dto);
    }


    @Test
    void convertDtoToFilter_full() {

        FolderFilterDto dto = new FolderFilterDto();
        dto.setId(UUID.randomUUID().toString());
        //noinspection deprecation
        dto.setLegacyId(UUID.randomUUID().toString());

        dto.setFilterName(RandomStringUtils.insecure().nextAlphanumeric(30));
        dto.setSearchData(RandomStringUtils.insecure().nextAlphanumeric(30));
        dto.setState(State.CURRENT);

        Date d1 = new Date();
        Date d2 = new Date();
        d2.setTime(d2.getTime() + 84000);
        dto.setCreatedAfter(d1);
        dto.setCreatedBefore(d2);

        dto.setTypeId(UUID.randomUUID().toString());
        dto.setSubtypeId(UUID.randomUUID().toString());

        FolderFilter filter = modelMapper.map(dto, FolderFilter.class);
        assertNotNull(filter);
        assertDeepEqualsFromDtoToDb(filter, dto);
    }


    @Test
    void convertDtoToFilter_empty() {

        FolderFilterDto dto = new FolderFilterDto();

        FolderFilter entity = modelMapper.map(dto, FolderFilter.class);
        assertNotNull(entity);
        assertDeepEqualsFromDtoToDb(entity, dto);
    }

}
