/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.database.userPreferences;

import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Random;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest
class UserPreferencesDtoTest {


    private @Autowired ModelMapper modelMapper;

    private final String favoriteDeskUuid1 = UUID.randomUUID().toString();
    private final String favoriteDeskUuid2 = UUID.randomUUID().toString();


    private static void assertDeepEquals(UserPreferences entity, UserPreferencesDto dto) {
        assertEquals(entity.getShowDelegations(), dto.isShowDelegations());
        assertEquals(entity.getTaskViewDefaultPageSize(), dto.getTaskViewDefaultPageSize());
        assertEquals(entity.getArchiveViewDefaultPageSize(), dto.getArchiveViewDefaultPageSize());
        assertEquals(entity.getTaskViewDefaultSortBy(), dto.getTaskViewDefaultSortBy());
        assertEquals(entity.getShowAdminIds(), dto.isShowAdminIds());
        assertEquals(entity.getTaskViewDefaultAsc(), dto.isTaskViewDefaultAsc());
        assertEquals(entity.getFolderViewBlockList(), dto.getFolderViewBlockList());
        assertEquals(entity.getTaskViewColumnList(), dto.getTaskViewColumnList());
        assertEquals(entity.getArchiveViewColumnList(), dto.getArchiveViewColumnList());
        assertEquals(entity.getFavoriteDeskIdList(), dto.getFavoriteDesks().stream().map(DeskRepresentation::getId).toList());
    }


    @Test
    void convertEntityToDto_full() {

        UserPreferences entity = new UserPreferences();
        entity.setUserId(UUID.randomUUID().toString());
        entity.setShowDelegations(true);
        entity.setTaskViewDefaultPageSize(new Random().nextInt(99));
        entity.setArchiveViewDefaultPageSize(new Random().nextInt(99));
        entity.setTaskViewDefaultSortBy(TaskViewColumn.FOLDER_ID);
        entity.setShowAdminIds(true);
        entity.setTaskViewDefaultAsc(true);
        entity.setFolderViewBlockList(List.of(FolderViewBlock.METADATA, FolderViewBlock.ANNEXE_DOCUMENTS));
        entity.setTaskViewColumnList(List.of(TaskViewColumn.CREATION_DATE, TaskViewColumn.TYPE));
        entity.setArchiveViewColumnList(List.of(ArchiveViewColumn.ORIGIN_DESK, ArchiveViewColumn.CREATION_DATE));
        entity.setFavoriteDeskIdList(List.of(favoriteDeskUuid1, favoriteDeskUuid2));

        UserPreferencesDto dto = modelMapper.map(entity, UserPreferencesDto.class);
        assertNotNull(dto);
        assertDeepEquals(entity, dto);
    }


    @Test
    void convertEntityToDto_empty() {

        UserPreferences entity = new UserPreferences();

        UserPreferencesDto dto = modelMapper.map(entity, UserPreferencesDto.class);
        assertNotNull(dto);
        assertDeepEquals(entity, dto);
    }


    @Test
    void convertDtoToEntity_full() {

        UserPreferencesDto dto = new UserPreferencesDto();
        dto.setShowDelegations(true);
        dto.setTaskViewDefaultPageSize(new Random().nextInt(99));
        dto.setArchiveViewDefaultPageSize(new Random().nextInt(99));
        dto.setTaskViewDefaultSortBy(TaskViewColumn.FOLDER_ID);
        dto.setShowAdminIds(true);
        dto.setTaskViewDefaultAsc(true);
        dto.setFolderViewBlockList(List.of(FolderViewBlock.METADATA, FolderViewBlock.ANNEXE_DOCUMENTS));
        dto.setTaskViewColumnList(List.of(TaskViewColumn.CREATION_DATE, TaskViewColumn.TYPE));
        dto.setArchiveViewColumnList(List.of(ArchiveViewColumn.ORIGIN_DESK, ArchiveViewColumn.CREATION_DATE));
        dto.setFavoriteDeskIds(List.of(favoriteDeskUuid1, favoriteDeskUuid2));
        dto.setFavoriteDesks(List.of(
                new DeskRepresentation(favoriteDeskUuid1, null),
                new DeskRepresentation(favoriteDeskUuid2, null)
        ));
        dto.setSignatureImageContentId(UUID.randomUUID().toString());

        UserPreferences entity = modelMapper.map(dto, UserPreferences.class);
        assertNotNull(entity);
        assertDeepEquals(entity, dto);
    }


    @Test
    void convertDtoToEntity_empty() {

        UserPreferences entity = new UserPreferences();

        UserPreferencesDto dto = modelMapper.map(entity, UserPreferencesDto.class);
        assertNotNull(dto);
        assertDeepEquals(entity, dto);
    }


}
