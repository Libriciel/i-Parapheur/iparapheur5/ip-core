/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.database;

import coop.libriciel.ipcore.model.crypto.PdfSignaturePosition;
import org.junit.jupiter.api.Test;

import static coop.libriciel.ipcore.model.pdfstamp.Origin.BOTTOM_LEFT;
import static org.junit.jupiter.api.Assertions.*;


class PdfSignaturePositionConverterTest {


    private final String serializedUIDv1 = "{\"x\":5.1,\"y\":10.2,\"page\":15}";


    @Test
    void convertToDatabaseColumn() {

        PdfSignaturePosition position = new PdfSignaturePosition(5.1F, 10.2F, 15, BOTTOM_LEFT, null);

        assertEquals(serializedUIDv1, new PdfSignaturePositionConverter().convertToDatabaseColumn(position));
        assertNull(new PdfSignaturePositionConverter().convertToDatabaseColumn(null));
    }


    @Test
    void convertToEntityAttribute() {

        PdfSignaturePosition position = new PdfSignaturePositionConverter().convertToEntityAttribute(serializedUIDv1);

        assertNotNull(position);
        assertEquals(5.1F, position.getX());
        assertEquals(10.2F, position.getY());
        assertEquals(15, position.getPage());
        assertNull(new PdfSignaturePositionConverter().convertToEntityAttribute(null));
    }


}
