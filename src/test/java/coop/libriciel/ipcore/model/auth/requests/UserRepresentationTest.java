/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.auth.requests;

import coop.libriciel.ipcore.model.auth.User;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.UUID;

import static coop.libriciel.ipcore.model.auth.UserPrivilege.FUNCTIONAL_ADMIN;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest
@SuppressWarnings("deprecation")
class UserRepresentationTest {


    private @Autowired ModelMapper modelMapper;


    private static void assertDeepEquals(User entity, UserRepresentation representation) {
        assertEquals(entity.getUserName(), representation.getUserName());
        assertEquals(entity.getFirstName(), representation.getFirstName());
        assertEquals(entity.getLastName(), representation.getLastName());
        assertEquals(entity.getEmail(), representation.getEmail());
        assertEquals(entity.getPrivilege(), representation.getPrivilege());
        assertEquals(entity.getIsLdapSynchronized(), representation.getIsLdapSynchronized());
    }


    @Nested
    class ConvertEntityToRepresentation {

        @Test
        void full() {

            User entity = new User();
            entity.setId(UUID.randomUUID().toString());
            entity.setUserName(RandomStringUtils.insecure().nextAlphanumeric(30));
            entity.setFirstName(RandomStringUtils.insecure().nextAlphanumeric(30));
            entity.setLastName(RandomStringUtils.insecure().nextAlphanumeric(30));
            entity.setEmail(RandomStringUtils.insecure().nextAlphanumeric(30) + "@dom.local");
            entity.setPrivilege(FUNCTIONAL_ADMIN);
            entity.setIsLdapSynchronized(true);

            UserRepresentation representation = modelMapper.map(entity, UserRepresentation.class);
            assertNotNull(representation);
            assertDeepEquals(entity, representation);
        }


        @Test
        void empty() {

            User entity = new User();

            UserRepresentation representation = modelMapper.map(entity, UserRepresentation.class);
            assertNotNull(representation);
            assertDeepEquals(entity, representation);
        }

    }


    @Nested
    class ConvertRepresentationToEntity {

        @Test
        void convertRepresentationToEntity_full() {

            UserRepresentation representation = new UserRepresentation();
            representation.setUserName(RandomStringUtils.insecure().nextAlphanumeric(30));
            representation.setFirstName(RandomStringUtils.insecure().nextAlphanumeric(30));
            representation.setLastName(RandomStringUtils.insecure().nextAlphanumeric(30));
            representation.setEmail(RandomStringUtils.insecure().nextAlphanumeric(30) + "@dom.local");
            representation.setPrivilege(FUNCTIONAL_ADMIN);
            representation.setIsLdapSynchronized(true);

            User entity = modelMapper.map(representation, User.class);
            assertNotNull(entity);
            assertDeepEquals(entity, representation);
        }


        @Test
        void convertRepresentationToEntity_empty() {

            UserRepresentation representation = new UserRepresentation();

            User entity = modelMapper.map(representation, User.class);
            assertNotNull(entity);
            assertDeepEquals(entity, representation);
        }


    }


}
