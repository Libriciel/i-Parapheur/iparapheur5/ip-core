/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.auth;

import org.junit.jupiter.api.Test;

import static coop.libriciel.ipcore.model.auth.PasswordPolicies.KnownRegexes.ENTROPY_80;
import static org.junit.jupiter.api.Assertions.*;


class PasswordPoliciesTest {


    @Test
    void entropy80ValueTest() {

        String passwordRegex = "^(?:(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^a-zA-Z0-9]).{12,})|(?:(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{14,})|(?:(?=.*"
                               + "[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9]).{13,})|(?:(?=.*[a-z])(?=.*[0-9])(?=.*[^a-zA-Z0-9]).{13,})|(?:(?=.*[A-Z])(?=.*"
                               + "[0-9])(?=.*[^a-zA-Z0-9]).{13,})|(?:(?=.*[a-z])(?=.*[A-Z]).{14,})|(?:(?=.*[a-z])(?=.*[0-9]).{16,})|(?:(?=.*[a-z])("
                               + "?=.*[^a-zA-Z0-9]).{14,})|(?:(?=.*[A-Z])(?=.*[0-9]).{16,})|(?:(?=.*[A-Z])(?=.*[^a-zA-Z0-9]).{14,})|(?:(?=.*[0-9])("
                               + "?=.*[^a-zA-Z0-9]).{15,})|(?:[a-z]{17,})|(?:[A-Z]{17,})|(?:[0-9]{24,})|(?:[^a-zA-Z0-9]{16,})$";

        assertEquals(passwordRegex, ENTROPY_80.getRegex());
    }


    @Test
    @SuppressWarnings("SpellCheckingInspection")
    void entropy80RegexTest() {

        // 1-alphabet rule
        assertFalse("aaaaaaaaaaaaaaaa".matches(ENTROPY_80.getRegex()));
        assertTrue("aaaaaaaaaaaaaaaaa".matches(ENTROPY_80.getRegex()));
        assertFalse("AAAAAAAAAAAAAAAA".matches(ENTROPY_80.getRegex()));
        assertTrue("AAAAAAAAAAAAAAAAA".matches(ENTROPY_80.getRegex()));
        assertFalse("11111111111111111111111".matches(ENTROPY_80.getRegex()));
        assertTrue("111111111111111111111111".matches(ENTROPY_80.getRegex()));
        assertFalse("###############".matches(ENTROPY_80.getRegex()));
        assertTrue("################".matches(ENTROPY_80.getRegex()));

        // 2-alphabet rule
        assertFalse("aaaaaaAAAAAAA".matches(ENTROPY_80.getRegex()));
        assertTrue("aaaaaaAAAAAAAA".matches(ENTROPY_80.getRegex()));
        assertFalse("aaaaaa111111111".matches(ENTROPY_80.getRegex()));
        assertTrue("aaaaaa1111111111".matches(ENTROPY_80.getRegex()));
        assertFalse("aaaaa########".matches(ENTROPY_80.getRegex()));
        assertTrue("aaaaa#########".matches(ENTROPY_80.getRegex()));
        assertFalse("AAAAAA111111111".matches(ENTROPY_80.getRegex()));
        assertTrue("AAAAAA1111111111".matches(ENTROPY_80.getRegex()));
        assertFalse("AAAAAA#######".matches(ENTROPY_80.getRegex()));
        assertTrue("AAAAAA########".matches(ENTROPY_80.getRegex()));
        assertFalse("1111111#######".matches(ENTROPY_80.getRegex()));
        assertTrue("1111111########".matches(ENTROPY_80.getRegex()));

        // 3-alphabet rule
        assertFalse("aaaaAAAA11111".matches(ENTROPY_80.getRegex()));
        assertTrue("aaaaAAAA111111".matches(ENTROPY_80.getRegex()));
        assertFalse("aaaaAAAA####".matches(ENTROPY_80.getRegex()));
        assertTrue("aaaaAAAA#####".matches(ENTROPY_80.getRegex()));
        assertFalse("aaaaa1111###".matches(ENTROPY_80.getRegex()));
        assertTrue("aaaaa1111####".matches(ENTROPY_80.getRegex()));
        assertFalse("AAAAA1111###".matches(ENTROPY_80.getRegex()));
        assertTrue("AAAAA1111####".matches(ENTROPY_80.getRegex()));

        // 4-alphabet rule
        assertFalse("aaaaAAAA11#".matches(ENTROPY_80.getRegex()));
        assertTrue("aaaaAAAA11##".matches(ENTROPY_80.getRegex()));
    }


}
