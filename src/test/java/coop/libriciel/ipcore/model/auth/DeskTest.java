/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.auth;

import org.junit.jupiter.api.Test;
import org.keycloak.representations.idm.RoleRepresentation;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


class DeskTest {


    @Test
    void buildDeskFromRole() {

        String id = "someID";
        String name = "someName";

        RoleRepresentation role = new RoleRepresentation("tenant_%s_desk_%s".formatted(UUID.randomUUID(), id), name, false);
        role.setId(id);

        Desk d = new Desk(role);

        assertNotNull(d);
        assertEquals(name, d.getName());
        assertNotNull(d.getDelegationRules());
    }


}
