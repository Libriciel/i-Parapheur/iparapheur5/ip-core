/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.content.alfresco;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest
class AlfrescoEntryTest {


    private @Autowired ObjectMapper mapper;

    private static final String test =
            """
            {
              "entry": {
                "createdAt": "2020-03-23T16:59:36.798+0000",
                "isFolder": false,
                "isFile": true,
                "createdByUser": {
                  "id": "admin",
                  "displayName": "Administrator"
                },
                "modifiedAt": "2020-03-23T16:59:36.849+0000",
                "modifiedByUser": {
                  "id": "admin",
                  "displayName": "Administrator"
                },
                "name": "file",
                "id": "664f32ad-ce91-4482-a487-2d7dd40296bf",
                "nodeType": "cm:content",
                "content": {
                  "mimeType": "application/pdf",
                  "mimeTypeName": "Adobe PDF Document",
                  "sizeInBytes": 13264,
                  "encoding": "UTF-8"
                },
                "parentId": "de140262-95a0-4896-8a4a-1a6b7f35a6dd"
              }
            }
            """.indent(1);


    @Test
    void testParse() throws JsonProcessingException {
        AlfrescoEntry<AlfrescoNode> parsed = mapper.readValue(test, new TypeReference<>() {});

        assertEquals("664f32ad-ce91-4482-a487-2d7dd40296bf", parsed.getEntry().getId());
        assertEquals("file", parsed.getEntry().getName());
        assertEquals(13264, parsed.getEntry().getContent().getSizeInBytes());
        assertEquals("application/pdf", parsed.getEntry().getContent().getMimeType());
    }

}
