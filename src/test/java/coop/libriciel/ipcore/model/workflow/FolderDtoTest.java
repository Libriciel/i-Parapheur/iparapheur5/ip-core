/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.workflow;

import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Type;
import coop.libriciel.ipcore.model.database.TypologyEntity;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.FolderDto;
import org.apache.commons.lang3.RandomStringUtils;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest
class FolderDtoTest {


    private @Autowired ModelMapper modelMapper;


    private static void assertDeepEquals(@NotNull Folder entity, @NotNull FolderDto dto) {
        assertEquals(entity.getName(), dto.getName());
        assertEquals(entity.getMetadata(), dto.getMetadata());
        assertEquals(Optional.ofNullable(entity.getType()).map(TypologyEntity::getId).orElse(null), dto.getTypeId());
        assertEquals(Optional.ofNullable(entity.getSubtype()).map(TypologyEntity::getId).orElse(null), dto.getSubtypeId());
    }


    @Test
    void convertEntityToDto_full() {

        String typeId = UUID.randomUUID().toString();
        String subtypeId = UUID.randomUUID().toString();

        Folder entity = new Folder();
        entity.setId(UUID.randomUUID().toString());
        entity.setName(RandomStringUtils.insecure().nextAlphanumeric(30));
        entity.setType(Type.builder().id(typeId).build());
        entity.setSubtype(Subtype.builder().id(subtypeId).build());
        entity.setMetadata(Map.of(
                RandomStringUtils.insecure().nextAlphanumeric(30), RandomStringUtils.insecure().nextAlphanumeric(30),
                RandomStringUtils.insecure().nextAlphanumeric(30), RandomStringUtils.insecure().nextAlphanumeric(30)
        ));

        FolderDto dto = modelMapper.map(entity, FolderDto.class);
        assertNotNull(dto);
        assertDeepEquals(entity, dto);
    }


    @Test
    void convertEntityToDto_empty() {

        Folder entity = new Folder();

        FolderDto dto = modelMapper.map(entity, FolderDto.class);
        assertNotNull(dto);
        assertDeepEquals(entity, dto);
    }


    @Test
    void convertDtoToEntity_full() {

        FolderDto dto = new FolderDto();
        dto.setName(RandomStringUtils.insecure().nextAlphanumeric(30));
        dto.setTypeId(UUID.randomUUID().toString());
        dto.setSubtypeId(UUID.randomUUID().toString());
        dto.setMetadata(Map.of(
                RandomStringUtils.insecure().nextAlphanumeric(30), RandomStringUtils.insecure().nextAlphanumeric(30),
                RandomStringUtils.insecure().nextAlphanumeric(30), RandomStringUtils.insecure().nextAlphanumeric(30)
        ));

        Folder entity = modelMapper.map(dto, Folder.class);
        assertNotNull(entity);
        assertDeepEquals(entity, dto);
    }


    @Test
    void convertDtoToEntity_empty() {

        FolderDto dto = new FolderDto();

        Folder entity = modelMapper.map(dto, Folder.class);
        assertNotNull(entity);
        assertDeepEquals(entity, dto);
    }


}
