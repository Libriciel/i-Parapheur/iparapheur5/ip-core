/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.workflow;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static coop.libriciel.ipcore.model.workflow.WorkflowDefinition.*;
import static org.junit.jupiter.api.Assertions.assertEquals;


class WorkflowDefinitionTest {


    @Test
    void genericValidatorsPublicToInternalIdsConverter() {
        List<String> dirty = Arrays.asList(RESERVED_EMITTER_ID_PLACEHOLDER, "1234", null, VARIABLE_DESK_ID_PLACEHOLDER);
        List<String> cleaned = Arrays.asList(EMITTER_ID_PLACEHOLDER, "1234", null, VARIABLE_DESK_ID_PLACEHOLDER);
        assertEquals(cleaned, dirty.stream().map(GENERIC_VALIDATORS_PUBLIC_TO_INTERNAL_IDS_CONVERTER).toList());
    }

    @Test
    void genericValidatorsInternalToPublicIdsConverter() {
        List<String> dirty = Arrays.asList(EMITTER_ID_PLACEHOLDER, "1234", null, RESERVED_VARIABLE_DESK_ID_PLACEHOLDER);
        List<String> cleaned = Arrays.asList(RESERVED_EMITTER_ID_PLACEHOLDER, "1234", null, RESERVED_VARIABLE_DESK_ID_PLACEHOLDER);
        assertEquals(cleaned, dirty.stream().map(GENERIC_VALIDATORS_INTERNAL_TO_PUBLIC_IDS_CONVERTER).toList());
    }


}
