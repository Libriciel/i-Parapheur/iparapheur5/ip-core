/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.workflow.ipWorkflow;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import org.junit.jupiter.api.Test;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;
import static com.fasterxml.jackson.databind.DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_USING_DEFAULT_VALUE;
import static com.fasterxml.jackson.databind.MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


class IpWorkflowInstanceTest {

    private static final ObjectMapper mapper = JsonMapper.builder()
            .enable(ACCEPT_CASE_INSENSITIVE_ENUMS)
            .enable(READ_UNKNOWN_ENUM_VALUES_USING_DEFAULT_VALUE)
            .configure(FAIL_ON_UNKNOWN_PROPERTIES, false)
            .build();

    private static final String test =
            """
            {
              "id": "16",
              "name": "Test 03",
              "businessKey": "164ed517-97ba-4bec-bec0-bfafa9a7fecf",
              "startTime": "2020-05-13T12:01:52.784+0000",
              "dueDate": null,
              "endTime": null,
              "variables": {
                "i_Parapheur_internal_public_annotation": "null",
                "approved": "true",
                "workflow_internal_steps": "null",
                "i_Parapheur_internal_private_annotation": "null",
                "workflow_internal_creation_workflow_id": "simple_workflow_01",
                "action": "i_Parapheur_internal_start",
                "workflow_internal_validation_start_date": "2020-05-14T11:28:05.177+0200",
                "workflow_internal_validation_workflow_id": "simple_workflow_01"
              },
              "state": null,
              "taskList": [
                {
                  "id": "workflow_internal_draft",
                  "instanceId": null,
                  "instanceName": null,
                  "expectedAction": "START",
                  "performedAction": null,
                  "beginDate": null,
                  "dueDate": null,
                  "date": null,
                  "candidateGroup": null,
                  "pending": false,
                  "variables": {
                    "i_Parapheur_internal_public_annotation": "null",
                    "approved": "true",
                    "workflow_internal_steps": "null",
                    "i_Parapheur_internal_private_annotation": "null",
                    "workflow_internal_creation_workflow_id": "simple_workflow_01",
                    "action": "i_Parapheur_internal_start",
                    "workflow_internal_validation_start_date": "2020-05-14T11:28:05.177+0200",
                    "workflow_internal_validation_workflow_id": "simple_workflow_01"
                  }
                },
                {
                  "id": "5031",
                  "instanceId": null,
                  "instanceName": null,
                  "expectedAction": "VISA",
                  "performedAction": null,
                  "beginDate": "2020-05-14T09:28:07.499+0000",
                  "dueDate": null,
                  "date": null,
                  "candidateGroup": "abd71e45-00d2-4763-9d2d-0ec7efcf61b4",
                  "pending": true,
                  "variables": {
                    "workflow_internal_current_candidate_groups": "abd71e45-00d2-4763-9d2d-0ec7efcf61b4",
                    "workflow_internal_previous_action": "i_Parapheur_internal_start",
                    "workflow_internal_previous_candidate_groups": "unknown",
                    "workflow_internal_workflow_index": "1",
                    "workflow_internal_task_id": "0",
                    "workflow_internal_step_index": "0"
                  }
                },
                {
                  "id": "1",
                  "instanceId": null,
                  "instanceName": null,
                  "expectedAction": "VISA",
                  "performedAction": null,
                  "beginDate": null,
                  "dueDate": null,
                  "date": null,
                  "candidateGroup": "f53436ae-8a65-47ea-8d44-296ceb345409",
                  "pending": false,
                  "variables": {}
                }
              ],
              "deploymentKey": null,
              "processDefinitionId": "i_Parapheur_internal_folder:1:15",
              "originGroup": "desk00",
              "finalGroup": "desk99",
              "creationWorkflowId": null,
              "validationWorkflowId": null
            }
            """.indent(1);


    @Test
    void parse() throws JsonProcessingException {
        IpWorkflowInstance parsed = mapper.readValue(test, new TypeReference<>() {});
        assertNotNull(parsed);

        assertEquals("16", parsed.getId());
        assertEquals("Test 03", parsed.getName());
        assertEquals("desk00", parsed.getOriginDesk().getId());
        assertEquals("desk99", parsed.getFinalDesk().getId());
        assertEquals(3, parsed.getStepList().size());
    }


//    @Test public void serialize() throws JsonProcessingException {
//
//        Folder toTest = Folder.builder()
//                .id("_id")
//                .name("Folder 01")
//                .state(PENDING)
//                .originDesk(new Entity("odid", null))
//                .finalDesk(new Entity("fdid", null))
//                .creationWorkflowDefinitionKey("cwdid")
//                .validationWorkflowDefinitionKey("vwdid")
//                .contentId("cid")
//                .build();
//
//        // Serialize
//
//        ObjectMapper mapper = new ObjectMapper();
//        mapper.enable(ACCEPT_CASE_INSENSITIVE_ENUMS);
//        mapper.enable(SORT_PROPERTIES_ALPHABETICALLY);
//        mapper.enable(READ_UNKNOWN_ENUM_VALUES_USING_DEFAULT_VALUE);
//        mapper.enable(INDENT_OUTPUT);
//
//        String serialized = mapper.writeValueAsString(toTest).replaceAll("\n", EMPTY);
//
//        // Test
//
//        String expected = "{" +
//                          "  \"beginDate\" : null, " +
//                          "  \"documentList\" : [ ], " +
//                          "  \"endTime\" : null, " +
//                          "  \"finalDesk\" : {" +
//                          "    \"id\" : \"fdid\", " +
//                          "    \"name\" : null" +
//                          "  }," +
//                          "  \"id\" : \"_id\", " +
//                          "  \"metadata\" : { }, " +
//                          "  \"name\" : \"Folder 01\", " +
//                          "  \"originDesk\" : {" +
//                          "    \"id\" : \"odid\", " +
//                          "    \"name\" : null" +
//                          "  }," +
//                          "  \"startTime\" : null, " +
//                          "  \"state\" : \"PENDING\", " +
//                          "  \"stepList\" : [ ]" +
//                          "}";
//
//        Assert.assertEquals(
//                expected.trim().replaceAll("\\s+", " "),
//                serialized.trim().replaceAll("\\s+", " ")
//        );
//    }

}
