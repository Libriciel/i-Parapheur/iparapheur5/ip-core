/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.workflow.ipWorkflow;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import coop.libriciel.ipcore.model.workflow.Task;
import org.junit.jupiter.api.Test;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;
import static com.fasterxml.jackson.databind.DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_USING_DEFAULT_VALUE;
import static com.fasterxml.jackson.databind.MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS;
import static coop.libriciel.ipcore.model.workflow.Action.*;
import static coop.libriciel.ipcore.model.workflow.State.*;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.junit.jupiter.api.Assertions.*;


class IpWorkflowTaskTest {


    private static final ObjectMapper mapper = JsonMapper.builder()
            .enable(ACCEPT_CASE_INSENSITIVE_ENUMS)
            .enable(READ_UNKNOWN_ENUM_VALUES_USING_DEFAULT_VALUE)
            .configure(FAIL_ON_UNKNOWN_PROPERTIES, false)
            .build();

    private static final String test =
            """
            {
              "id": "workflow_internal_draft",
              "instanceId": "desk01",
              "instanceName": "Desk 01",
              "expectedAction": "START",
              "performedAction": "REJECT",
              "beginDate": "2020-05-14T12:00:00.000+0200",
              "dueDate": "2021-05-14T12:00:00.000+0200",
              "date": "2022-05-14T12:00:00.000+0200",
              "candidateGroup": null,
              "pending": false,
              "state": "REJECTED",
              "variables": {
                "i_Parapheur_internal_public_annotation": "Public Annotation",
                "i_Parapheur_internal_start_public_annotation": "Public Annotation",
                "approved": "true",
                "workflow_internal_steps": "null",
                "i_Parapheur_internal_private_annotation": "Private Annotation",
                "i_Parapheur_internal_start_private_annotation": "Private Annotation",
                "workflow_internal_creation_workflow_id": "simple_workflow_01",
                "workflow_internal_validation_start_date": "2020-05-14T11:28:05.177+0200",
                "workflow_internal_validation_workflow_id": "simple_workflow_01"
              },
              "readByUserIds": ["user01"]
            }
            """.indent(1);


    @Test
    void noArgsConstructor() {
        Task task = new IpWorkflowTask();
        assertEquals(VISA, task.getAction());
        assertEquals(UPCOMING, task.getState());
    }


    @Test
    void builder() {
        Task task = IpWorkflowTask.builder().build();
        assertEquals(VISA, task.getAction());
        assertEquals(PENDING, task.getState());
    }


    @Test
    void parse() throws JsonProcessingException {
        IpWorkflowTask parsed = mapper.readValue(test, new TypeReference<>() {});
        assertNotNull(parsed);

        assertEquals("workflow_internal_draft", parsed.getId());
        assertEquals(START, parsed.getAction());
        assertEquals(REJECTED, parsed.getState());
        assertEquals(1589450400000L, parsed.getBeginDate().getTime());
        assertEquals(1652522400000L, parsed.getDate().getTime());
        assertEquals(1620986400000L, parsed.getFolderDueDate().getTime());
        assertEquals("Public Annotation", parsed.getPublicAnnotation());
        assertEquals("Private Annotation", parsed.getPrivateAnnotation());
        assertTrue(isNotEmpty(parsed.getReadByUserIds()));
        assertTrue(parsed.getReadByUserIds().contains("user01"));
    }


    @Test
    void computeState() {

        new IpWorkflowTask() {{
            computeState();
            assertEquals(UPCOMING, getState());
        }};

        new IpWorkflowTask() {{
            setAction(VISA);
            setPerformedAction(VISA);
            setIsPending(false);
            computeState();
            assertEquals(VALIDATED, getState());
        }};

        new IpWorkflowTask() {{
            setAction(VISA);
            setPerformedAction(TRANSFER);
            setIsPending(false);
            computeState();
            assertEquals(TRANSFERRED, getState());
        }};

        new IpWorkflowTask() {{
            setAction(VISA);
            setPerformedAction(ASK_SECOND_OPINION);
            setIsPending(false);
            computeState();
            assertEquals(SECONDED, getState());
        }};

        new IpWorkflowTask() {{
            setAction(VISA);
            setPerformedAction(REJECT);
            setIsPending(false);
            computeState();
            assertEquals(REJECTED, getState());
        }};

        new IpWorkflowTask() {{
            setAction(VISA);
            setPerformedAction(null);
            setIsPending(true);
            computeState();
            assertEquals(PENDING, getState());
        }};

        new IpWorkflowTask() {{
            setAction(VISA);
            setPerformedAction(null);
            setIsPending(false);
            computeState();
            assertEquals(UPCOMING, getState());
        }};
    }


}
