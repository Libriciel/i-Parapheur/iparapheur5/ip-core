/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.workflow;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import static coop.libriciel.ipcore.model.workflow.Action.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


class ActionTest {


    @Test
    void equalsOneOf() {
        assertFalse(VISA.equalsOneOf());
        assertTrue(VISA.equalsOneOf(VISA));
        assertFalse(VISA.equalsOneOf(TRANSFER, SECOND_OPINION, UNDO));
        assertTrue(VISA.equalsOneOf(SIGNATURE, SECOND_OPINION, VISA));
    }


    /**
     * Just testing if the values aren't null nor empty.
     * For code coverage increase.
     */
    @Test
    void getPrettyPrintString() {
        for (Action action : Action.values()) {
            assertFalse(StringUtils.isEmpty(action.getMessageKey()));
        }
    }


}
