/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.workflow;

import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.Metadata;
import coop.libriciel.ipcore.model.database.MetadataDto;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.SubtypeMetadata;
import coop.libriciel.ipcore.model.database.requests.SubtypeMetadataDto;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
class StepDefinitionDtoTest {


    private @Autowired ModelMapper modelMapper;


    private static void assertDeepEquals(StepDefinition entity, StepDefinitionDto dto) {

        assertEquals(entity.getType() != null, dto.getType() != null);
        if (dto.getType() != null) {
            assertEquals(entity.getType(), dto.getType().toAction());
        }

        assertEquals(entity.getParallelType(), dto.getParallelType());

        assertEquals(CollectionUtils.isEmpty(entity.getValidatingDesks()), CollectionUtils.isEmpty(dto.getValidatingDeskIds()));
        if (CollectionUtils.isNotEmpty(entity.getValidatingDesks()) || CollectionUtils.isNotEmpty(dto.getValidatingDeskIds())) {
            assertEquals(
                    entity.getValidatingDesks().stream().map(DeskRepresentation::getId).toList(),
                    dto.getValidatingDeskIds()
            );
            // Prevents an NPE on DTO to Entity conversion, where these 2 fields are not mapped anyway
            if (CollectionUtils.isNotEmpty(dto.getValidatingDesks())) {
                assertEquals(
                        entity.getValidatingDesks().stream().map(DeskRepresentation::getId).toList(),
                        dto.getValidatingDesks().stream().map(DeskRepresentation::getId).toList()
                );
            }
        }

        assertEquals(CollectionUtils.isEmpty(entity.getNotifiedDesks()), CollectionUtils.isEmpty(dto.getNotifiedDeskIds()));
        if (CollectionUtils.isNotEmpty(entity.getNotifiedDesks()) || CollectionUtils.isNotEmpty(dto.getNotifiedDeskIds())) {
            assertEquals(
                    entity.getNotifiedDesks().stream().map(DeskRepresentation::getId).toList(),
                    dto.getNotifiedDeskIds()
            );
            // Prevents an NPE on DTO to Entity conversion, where these 2 fields are not mapped anyway
            if (CollectionUtils.isNotEmpty(dto.getNotifiedDesks())) {
                assertEquals(
                        entity.getNotifiedDesks().stream().map(DeskRepresentation::getId).toList(),
                        dto.getNotifiedDesks().stream().map(DeskRepresentation::getId).toList()
                );
            }
        }

        assertEquals(CollectionUtils.isEmpty(entity.getMandatoryValidationMetadata()), CollectionUtils.isEmpty(dto.getMandatoryValidationMetadataIds()));
        if (CollectionUtils.isNotEmpty(entity.getNotifiedDesks()) || CollectionUtils.isNotEmpty(dto.getMandatoryValidationMetadataIds())) {
            assertEquals(
                    entity.getMandatoryValidationMetadata().stream().map(Metadata::getId).toList(),
                    dto.getMandatoryValidationMetadataIds()
            );
            // Prevents an NPE on DTO to Entity conversion, where these 2 fields are not mapped anyway
            if (CollectionUtils.isNotEmpty(dto.getMandatoryValidationMetadata())) {
                assertEquals(
                        entity.getMandatoryValidationMetadata().stream().map(Metadata::getId).toList(),
                        dto.getMandatoryValidationMetadata().stream().map(MetadataDto::getId).toList()
                );
            }
        }

        assertEquals(CollectionUtils.isEmpty(entity.getMandatoryRejectionMetadata()), CollectionUtils.isEmpty(dto.getMandatoryRejectionMetadataIds()));
        if (CollectionUtils.isNotEmpty(entity.getNotifiedDesks()) || CollectionUtils.isNotEmpty(dto.getMandatoryRejectionMetadataIds())) {
            assertEquals(
                    entity.getMandatoryRejectionMetadata().stream().map(Metadata::getId).toList(),
                    dto.getMandatoryRejectionMetadataIds()
            );
            // Prevents an NPE on DTO to Entity conversion, where these 2 fields are not mapped anyway 
            if (CollectionUtils.isNotEmpty(dto.getMandatoryRejectionMetadata())) {
                assertEquals(
                        entity.getMandatoryRejectionMetadata().stream().map(Metadata::getId).toList(),
                        dto.getMandatoryRejectionMetadata().stream().map(MetadataDto::getId).toList()
                );
            }
        }
    }


    @Nested
    class EntityToDtoTest {


        @Test
        void full() {

            StepDefinition entity = new StepDefinition();
            entity.setType(Action.SIGNATURE);
            entity.setParallelType(StepDefinitionParallelType.AND);
            entity.setMandatoryValidationMetadata(List.of(
                    Metadata.builder().id(UUID.randomUUID().toString()).name(RandomStringUtils.insecure().nextAlphanumeric(36)).build(),
                    Metadata.builder().id(UUID.randomUUID().toString()).name(RandomStringUtils.insecure().nextAlphanumeric(36)).build()
            ));
            entity.setMandatoryRejectionMetadata(List.of(
                    Metadata.builder().id(UUID.randomUUID().toString()).name(RandomStringUtils.insecure().nextAlphanumeric(36)).build(),
                    Metadata.builder().id(UUID.randomUUID().toString()).name(RandomStringUtils.insecure().nextAlphanumeric(36)).build()
            ));
            entity.setValidatingDesks(List.of(
                    new DeskRepresentation(UUID.randomUUID().toString(), RandomStringUtils.insecure().nextAlphanumeric(36)),
                    new DeskRepresentation(UUID.randomUUID().toString(), RandomStringUtils.insecure().nextAlphanumeric(36))
            ));
            entity.setNotifiedDesks(List.of(
                    new DeskRepresentation(UUID.randomUUID().toString(), RandomStringUtils.insecure().nextAlphanumeric(36)),
                    new DeskRepresentation(UUID.randomUUID().toString(), RandomStringUtils.insecure().nextAlphanumeric(36))
            ));

            StepDefinitionDto dto = modelMapper.map(entity, StepDefinitionDto.class);
            assertNotNull(dto);
            assertDeepEquals(entity, dto);
        }


        @Test
        void empty() {

            StepDefinition entity = new StepDefinition();

            StepDefinitionDto dto = modelMapper.map(entity, StepDefinitionDto.class);
            assertNotNull(dto);
            assertDeepEquals(entity, dto);
        }


    }


    @Nested
    class DtoToEntityTest {


        @Test
        void full() {

            StepDefinitionDto dto = new StepDefinitionDto();
            dto.setType(StepDefinitionType.SIGNATURE);
            dto.setParallelType(StepDefinitionParallelType.AND);
            dto.setMandatoryValidationMetadataIds(List.of(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
            dto.setMandatoryRejectionMetadataIds(List.of(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
            dto.setValidatingDeskIds(List.of(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
            dto.setNotifiedDeskIds(List.of(UUID.randomUUID().toString(), UUID.randomUUID().toString()));

            StepDefinition entity = modelMapper.map(dto, StepDefinition.class);
            assertNotNull(entity);
            assertDeepEquals(entity, dto);
        }


        @Test
        void empty() {

            StepDefinitionDto dto = new StepDefinitionDto();

            StepDefinition entity = modelMapper.map(dto, StepDefinition.class);
            assertNotNull(entity);
            assertDeepEquals(entity, dto);
        }


    }


}
