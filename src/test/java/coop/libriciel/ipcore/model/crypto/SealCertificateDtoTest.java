/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.crypto;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.Random;
import java.util.UUID;

import static java.lang.Long.MAX_VALUE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest
class SealCertificateDtoTest {


    private @Autowired ModelMapper modelMapper;


    private static void assertDeepEquals(SealCertificate entity, SealCertificateDto dto) {
        assertEquals(entity.getId(), dto.getId());
        assertEquals(entity.getName(), dto.getName());
        assertEquals(entity.getExpirationDate(), dto.getExpirationDate());
        assertEquals(entity.getSignatureImageContentId(), dto.getSignatureImageContentId());
        assertEquals(entity.getUsageCount(), dto.getUsageCount());
    }


    @Test
    void convertEntityToDto_full() {

        SealCertificate entity = new SealCertificate();
        entity.setId(UUID.randomUUID().toString());
        entity.setName(RandomStringUtils.insecure().nextAlphanumeric(30));
        entity.setExpirationDate(new Date(new Random().nextLong(MAX_VALUE)));
        entity.setSignatureImageContentId(UUID.randomUUID().toString());
        entity.setUsageCount(new Random().nextLong(MAX_VALUE));

        SealCertificateDto dto = modelMapper.map(entity, SealCertificateDto.class);
        assertNotNull(dto);
        assertDeepEquals(entity, dto);
    }


    @Test
    void convertEntityToDto_empty() {

        SealCertificate entity = new SealCertificate();

        SealCertificateDto dto = modelMapper.map(entity, SealCertificateDto.class);
        assertNotNull(dto);
        assertDeepEquals(entity, dto);
    }


    @Test
    void convertDtoToEntity_full() {

        SealCertificateDto dto = new SealCertificateDto();
        dto.setId(UUID.randomUUID().toString());
        dto.setName(RandomStringUtils.insecure().nextAlphanumeric(30));
        dto.setExpirationDate(new Date(new Random().nextLong(MAX_VALUE)));
        dto.setSignatureImageContentId(UUID.randomUUID().toString());
        dto.setUsageCount(new Random().nextLong(MAX_VALUE));

        SealCertificate entity = modelMapper.map(dto, SealCertificate.class);
        assertNotNull(entity);
        assertDeepEquals(entity, dto);
    }


    @Test
    void convertDtoToEntity_empty() {

        SealCertificateDto dto = new SealCertificateDto();

        SealCertificate entity = modelMapper.map(dto, SealCertificate.class);
        assertNotNull(entity);
        assertDeepEquals(entity, dto);
    }


}
