/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.permission.request;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest
class KeycloakResourcePoliciesEvaluationTest {


    private @Autowired ObjectMapper objectMapper;


    @Test
    void parse() throws Exception {

        String stringToParse =
                """
                {
                   "resource":{
                      "name":"tenant_c6d3ad73-ddcc-419b-8dbb-6ad5750573ca_desk_7c83a02e-03cf-483f-9074-29d030211263 with scopes [urn:ipcore:scopes:view]",
                      "_id":"7c83a02e-03cf-483f-9074-29d030211263"
                   },
                   "scopes":[
                      {
                         "id":"07d33def-3e6c-4f2c-a914-cffa9a734f8a",
                         "name":"urn:ipcore:scopes:view"
                      }
                   ],
                   "policies":[],
                   "status":"PERMIT",
                   "allowedScopes":[
                      {
                         "id":"07d33def-3e6c-4f2c-a914-cffa9a734f8a",
                         "name":"urn:ipcore:scopes:view"
                      }
                   ]
                }
                """;

        KeycloakResourcePoliciesEvaluation result = objectMapper.readValue(stringToParse, KeycloakResourcePoliciesEvaluation.class);
        assertNotNull(result);

        assertNotNull(result.getResource());
        assertEquals("tenant_c6d3ad73-ddcc-419b-8dbb-6ad5750573ca_desk_7c83a02e-03cf-483f-9074-29d030211263 with scopes [urn:ipcore:scopes:view]", result.getResource().getName());
        assertEquals("7c83a02e-03cf-483f-9074-29d030211263", result.getResource().getId());

        assertNotNull(result.getScopes());
        assertEquals(1, result.getScopes().size());
        assertEquals("07d33def-3e6c-4f2c-a914-cffa9a734f8a", result.getScopes().get(0).getId());
        assertEquals("urn:ipcore:scopes:view", result.getScopes().get(0).getName());

        assertNotNull(result.getPolicies());
        assertEquals(0, result.getPolicies().size());

        assertEquals("PERMIT", result.getStatus());

        assertNotNull(result.getAllowedScopes());
        assertEquals(1, result.getAllowedScopes().size());
        assertEquals("07d33def-3e6c-4f2c-a914-cffa9a734f8a", result.getAllowedScopes().get(0).getId());
        assertEquals("urn:ipcore:scopes:view", result.getAllowedScopes().get(0).getName());
    }


}
