/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.permission.request;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
class KeycloakResourcePolicyEvaluationTest {


    private @Autowired ObjectMapper objectMapper;


    @Test
    void parse() throws Exception {

        String stringToParse =
                """
                {
                   "policy": null,
                   "status":"DENY",
                   "associatedPolicies":[
                      {
                         "policy": null,
                         "status":"DENY",
                         "associatedPolicies":[],
                         "scopes":["urn:ipcore:scopes:view"]
                      }
                   ],
                   "scopes":["urn:ipcore:scopes:association"]
                }
                """;

        KeycloakResourcePolicyEvaluation result = objectMapper.readValue(stringToParse, KeycloakResourcePolicyEvaluation.class);
        assertNotNull(result);
        assertNull(result.getPolicy());
        assertEquals("DENY", result.getStatus());
        assertNotNull(result.getScopes());
        assertEquals(1, result.getScopes().size());
        assertEquals("urn:ipcore:scopes:association", result.getScopes().get(0));
        assertNotNull(result.getAssociatedPolicies());
        assertEquals(1, result.getAssociatedPolicies().size());

        KeycloakResourcePolicyEvaluation subResult = result.getAssociatedPolicies().get(0);
        assertNull(subResult.getPolicy());
        assertEquals("DENY", subResult.getStatus());
        assertNotNull(subResult.getAssociatedPolicies());
        assertEquals(0, subResult.getAssociatedPolicies().size());
        assertEquals(1, subResult.getScopes().size());
        assertEquals("urn:ipcore:scopes:view", subResult.getScopes().get(0));
    }

}
