/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.permission.request;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest
class KeycloakResourcePolicyTest {


    private @Autowired ObjectMapper objectMapper;


    @Test
    void parse() throws Exception {

        String stringToTest =
                """
                {
                  "id":"b952c588-642a-42f2-8898-d53663ecd240",
                  "name":"tenant_c6d3ad73-ddcc-419b-8dbb-6ad5750573ca_desk_7c83a02e-03cf-483f-9074-29d030211263_association",
                  "type":"scope",
                  "resources":["tenant_c6d3ad73-ddcc-419b-8dbb-6ad5750573ca_desk_7c83a02e-03cf-483f-9074-29d030211263"],
                  "scopes":["urn:ipcore:scopes:association"],
                  "logic":"POSITIVE",
                  "decisionStrategy":"AFFIRMATIVE",
                  "config":{}
                }
                """;

        KeycloakResourcePolicy result = objectMapper.readValue(stringToTest, KeycloakResourcePolicy.class);
        assertNotNull(result);

        assertEquals("b952c588-642a-42f2-8898-d53663ecd240", result.getId());
        assertEquals("tenant_c6d3ad73-ddcc-419b-8dbb-6ad5750573ca_desk_7c83a02e-03cf-483f-9074-29d030211263_association", result.getName());
        assertEquals("scope", result.getType());

        assertNotNull(result.getResources());
        assertEquals(1, result.getResources().size());
        assertEquals("tenant_c6d3ad73-ddcc-419b-8dbb-6ad5750573ca_desk_7c83a02e-03cf-483f-9074-29d030211263", result.getResources().get(0));

        assertNotNull(result.getScopes());
        assertEquals(1, result.getScopes().size());
        assertEquals("urn:ipcore:scopes:association", result.getScopes().get(0));

        assertEquals("POSITIVE", result.getLogic());
        assertEquals("AFFIRMATIVE", result.getDecisionStrategy());
    }


}
