/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.model.permission;

import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Type;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest
class DelegationDtoTest {


    private @Autowired ModelMapper modelMapper;


    private void assertDeepEquals(Delegation entity, DelegationDto dto) {
        assertEquals(entity.getId(), dto.getId());

        assertEquals(Optional.ofNullable(entity.getSubstituteDesk()).map(DeskRepresentation::getId).orElse(null), dto.getSubstituteDeskId());
        assertEquals(Optional.ofNullable(entity.getDelegatingDesk()).map(DeskRepresentation::getId).orElse(null), dto.getDelegatingDeskId());

        assertEquals(Optional.ofNullable(entity.getType()).map(Type::getId).orElse(null), dto.getTypeId());
        assertEquals(Optional.ofNullable(entity.getSubtype()).map(Subtype::getId).orElse(null), dto.getSubtypeId());

        assertEquals(entity.getStart(), dto.getStart());
        assertEquals(entity.getEnd(), dto.getEnd());
    }


    @Nested
    class ConvertEntityToDto {


        @Test
        void full() {

            Delegation entity = new Delegation();
            entity.setId(UUID.randomUUID().toString());
            entity.setSubstituteDesk(new DeskRepresentation(UUID.randomUUID().toString(), null));
            entity.setDelegatingDesk(new DeskRepresentation(UUID.randomUUID().toString(), null));
            entity.setType(new Type(UUID.randomUUID().toString()));
            entity.setSubtype(new Subtype(UUID.randomUUID().toString()));
            entity.setStart(new Date(new Random().nextLong()));
            entity.setEnd(new Date(new Random().nextLong()));

            DelegationDto dto = modelMapper.map(entity, DelegationDto.class);
            assertNotNull(dto);
            assertDeepEquals(entity, dto);
        }


        @Test
        void empty() {

            Delegation entity = new Delegation();

            DelegationDto dto = modelMapper.map(entity, DelegationDto.class);
            assertNotNull(dto);
            assertDeepEquals(entity, dto);
        }


    }


    @Nested
    class ConvertDtoToEntity {


        @Test
        void full() {

            DelegationDto dto = new DelegationDto();
            dto.setId(UUID.randomUUID().toString());
            dto.setSubstituteDeskId(UUID.randomUUID().toString());
            dto.setDelegatingDeskId(UUID.randomUUID().toString());
            dto.setTypeId(UUID.randomUUID().toString());
            dto.setSubtypeId(UUID.randomUUID().toString());
            dto.setStart(new Date(new Random().nextLong()));
            dto.setEnd(new Date(new Random().nextLong()));

            Delegation entity = modelMapper.map(dto, Delegation.class);
            assertNotNull(entity);
            assertDeepEquals(entity, dto);
        }


        @Test
        void empty() {

            DelegationDto dto = new DelegationDto();

            Delegation entity = modelMapper.map(dto, Delegation.class);
            assertNotNull(entity);
            assertDeepEquals(entity, dto);
        }


    }


}
