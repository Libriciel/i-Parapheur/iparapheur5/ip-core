/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.utils;

import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.requests.UserRepresentation;
import coop.libriciel.ipcore.model.content.DetachedSignature;
import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.content.ValidatedSignatureInformation;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Type;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import gov.loc.premis.v3.File;
import gov.loc.premis.v3.IntellectualEntity;
import gov.loc.premis.v3.PremisComplexType;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import javax.xml.transform.TransformerException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.UUID;

import static coop.libriciel.ipcore.model.crypto.SignatureFormat.XADES_DETACHED;
import static coop.libriciel.ipcore.model.workflow.Action.SIGNATURE;
import static coop.libriciel.ipcore.model.workflow.Action.VISA;
import static coop.libriciel.ipcore.model.workflow.State.VALIDATED;
import static coop.libriciel.ipcore.utils.XmlUtils.*;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.http.MediaType.APPLICATION_PDF;
import static org.springframework.http.MediaType.TEXT_XML;


class XmlUtilsTest {


    private static final String PREMIS_EXAMPLE =
            """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
            <premis version="3.0" xmlns="http://www.loc.gov/premis/v3">
                <object xsi:type="intellectualEntity" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                    <objectIdentifier>
                        <objectIdentifierType>folderId</objectIdentifierType>
                        <objectIdentifierValue>ba36f98b-62bd-11ee-bef3-be6c84118738</objectIdentifierValue>
                    </objectIdentifier>
                    <significantProperties>
                        <significantPropertiesType>i_Parapheur_reserved_type</significantPropertiesType>
                        <significantPropertiesValue>PADES</significantPropertiesValue>
                    </significantProperties>
                    <significantProperties>
                        <significantPropertiesType>i_Parapheur_reserved_subtype</significantPropertiesType>
                        <significantPropertiesValue>Auto-visa</significantPropertiesValue>
                    </significantProperties>
                    <significantProperties>
                        <significantPropertiesType>i_Parapheur_reserved_dueDate</significantPropertiesType>
                        <significantPropertiesValue>2000-12-25</significantPropertiesValue>
                    </significantProperties>
                    <significantProperties>
                        <significantPropertiesType>meta_id_01</significantPropertiesType>
                        <significantPropertiesValue>meta 01 value</significantPropertiesValue>
                    </significantProperties>
                    <significantProperties>
                        <significantPropertiesType>meta_key_02</significantPropertiesType>
                        <significantPropertiesValue>meta 02 value</significantPropertiesValue>
                    </significantProperties>
                    <originalName>test ssss</originalName>
                </object>
                <object xsi:type="file" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                    <objectIdentifier>
                        <objectIdentifierType>documentId</objectIdentifierType>
                        <objectIdentifierValue>c7c311f9-ec65-4cbb-90f3-ba20a390ceab</objectIdentifierValue>
                    </objectIdentifier>
                    <significantProperties>
                        <significantPropertiesType>i_Parapheur_reserved_mainDocument</significantPropertiesType>
                        <significantPropertiesValue>true</significantPropertiesValue>
                    </significantProperties>
                    <originalName>copie-d-examen-simple-ou-double-1.pdf</originalName>
                    <significantProperties xmlns="" xmlns:premis="http://www.loc.gov/premis/v3">
                        <significantPropertiesType>i_Parapheur_reserved_mainDocument</significantPropertiesType>
                        <significantPropertiesValue>true</significantPropertiesValue>
                    </significantProperties>
                </object>
            </premis>
            """;


    @Test
    void canonicalize() throws Exception {
        String poorXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>   <definitions ></definitions   >   ";
        String canonicalizedXml = XmlUtils.canonicalize(poorXml.getBytes());

        assertEquals("<definitions></definitions>", canonicalizedXml);
    }


    @Test
    void canonicalize_error() {
        String badXml = "<<<<<";
        assertThrows(TransformerException.class, () -> XmlUtils.canonicalize(badXml.getBytes()));
    }


    @Nested
    class patchPremisTrashBin {


        @Test
        void keepTheRegularPremis() throws TransformerException, IOException {
            String patched;
            try (InputStream bais = new ByteArrayInputStream(PREMIS_EXAMPLE.getBytes())) {
                patched = XmlUtils.patchTrashBinPremis(getClass().getClassLoader(), bais);
            }
            assertEquals(
                    XmlUtils.canonicalize(PREMIS_EXAMPLE.getBytes()),
                    XmlUtils.canonicalize(patched.getBytes())
            );
        }


        @Test
        void fixityToSignificantProperty() throws TransformerException, IOException {

            String brokenExample =
                    """
                    <?xml version="1.0" encoding="UTF-8"?>
                    <premis xmlns="http://www.loc.gov/premis/v3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="3.0" \
                    xsi:schemaLocation="http://www.loc.gov/premis/v3 http://www.loc.gov/standards/premis/v3/premis.xsd">
                      <!-- Files -->
                      <object xsi:type="file">
                        <objectIdentifier>
                          <objectIdentifierType>documentId</objectIdentifierType>
                          <objectIdentifierValue>77bd7ea0-29c3-447e-bdfd-b6bb30d77e9d</objectIdentifierValue>
                        </objectIdentifier>
                        <preservationLevel>
                          <preservationLevelValue>mainDocument</preservationLevelValue>
                        </preservationLevel>
                        <objectCharacteristics>
                          <fixity>
                            <messageDigestAlgorithm>SHA-256</messageDigestAlgorithm>
                            <messageDigest>B03D0710D8648314A5FF7D69DBDF3217F9D7775132D2993EBF6C2C896D9B5A81</messageDigest>
                          </fixity>
                          <size>1234</size>
                          <format>
                            <formatDesignation>
                              <formatName>application/pdf</formatName>
                              <formatVersion>1.6</formatVersion>
                            </formatDesignation>
                          </format>
                        </objectCharacteristics>
                        <originalName>Document_foo.pdf</originalName>
                        <signatureInformation>
                          <signature>
                            <signatureEncoding>UTF-8</signatureEncoding>
                            <signatureMethod>XAdES_BASELINE_B</signatureMethod>
                            <signatureValue>signature_1_value</signatureValue>
                            <signatureValidationRules>XAdES-1.2.2</signatureValidationRules>
                          </signature>
                        </signatureInformation>
                        <signatureInformation>
                          <signature>
                            <signatureEncoding>UTF-8</signatureEncoding>
                            <signatureMethod>XAdES_BASELINE_B</signatureMethod>
                            <signatureValue>signature_2_value</signatureValue>
                            <signatureValidationRules>XAdES-1.2.2</signatureValidationRules>
                          </signature>
                        </signatureInformation>
                      </object>
                      <object xsi:type="file">
                        <objectIdentifier>
                          <objectIdentifierType>documentId</objectIdentifierType>
                          <objectIdentifierValue>2d6684cc-1c9b-42f3-8a91-fb76bb617598</objectIdentifierValue>
                        </objectIdentifier>
                        <preservationLevel>
                          <preservationLevelValue>annex</preservationLevelValue>
                        </preservationLevel>
                        <objectCharacteristics>
                          <fixity>
                            <messageDigestAlgorithm>SHA-256</messageDigestAlgorithm>
                            <messageDigest>2072C9B544878DE7DE0711441A8C61070B0872FA0A4F2A243B45BC11C43729F9</messageDigest>
                          </fixity>
                          <size>4321</size>
                          <format>
                            <formatDesignation>
                              <formatName>application/pdf</formatName>
                              <formatVersion>1.5</formatVersion>
                            </formatDesignation>
                          </format>
                        </objectCharacteristics>
                        <originalName>Annexe.pdf</originalName>
                      </object>
                    </premis>\
                    """;

            String expected =
                    """
                    <?xml version="1.0" encoding="UTF-8"?>
                    <premis xmlns="http://www.loc.gov/premis/v3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="3.0" \
                    xsi:schemaLocation="http://www.loc.gov/premis/v3 http://www.loc.gov/standards/premis/v3/premis.xsd">
                        <!-- Files -->
                        <object xsi:type="file">
                            <objectIdentifier>
                                <objectIdentifierType>documentId</objectIdentifierType>
                                <objectIdentifierValue>77bd7ea0-29c3-447e-bdfd-b6bb30d77e9d</objectIdentifierValue>
                            </objectIdentifier>
                            <objectCharacteristics>
                                <fixity>
                                    <messageDigestAlgorithm>SHA-256</messageDigestAlgorithm>
                                    <messageDigest>B03D0710D8648314A5FF7D69DBDF3217F9D7775132D2993EBF6C2C896D9B5A81</messageDigest>
                                </fixity>
                                <size>1234</size>
                                <format>
                                    <formatDesignation>
                                        <formatName>application/pdf</formatName>
                                        <formatVersion>1.6</formatVersion>
                                    </formatDesignation>
                                </format>
                            </objectCharacteristics>
                            <originalName>Document_foo.pdf</originalName>
                            <signatureInformation>
                                <signature>
                                    <signatureEncoding>UTF-8</signatureEncoding>
                                    <signatureMethod>XAdES_BASELINE_B</signatureMethod>
                                    <signatureValue>signature_1_value</signatureValue>
                                    <signatureValidationRules>XAdES-1.2.2</signatureValidationRules>
                                </signature>
                            </signatureInformation>
                            <signatureInformation>
                                <signature>
                                    <signatureEncoding>UTF-8</signatureEncoding>
                                    <signatureMethod>XAdES_BASELINE_B</signatureMethod>
                                    <signatureValue>signature_2_value</signatureValue>
                                    <signatureValidationRules>XAdES-1.2.2</signatureValidationRules>
                                </signature>
                            </signatureInformation>
                            <significantProperties xmlns="" xmlns:premis="http://www.loc.gov/premis/v3">
                                <significantPropertiesType>i_Parapheur_reserved_mainDocument</significantPropertiesType>
                                <significantPropertiesValue>false</significantPropertiesValue>
                            </significantProperties>
                        </object>
                        <object xsi:type="file">
                            <objectIdentifier>
                                <objectIdentifierType>documentId</objectIdentifierType>
                                <objectIdentifierValue>2d6684cc-1c9b-42f3-8a91-fb76bb617598</objectIdentifierValue>
                            </objectIdentifier>
                            <objectCharacteristics>
                                <fixity>
                                    <messageDigestAlgorithm>SHA-256</messageDigestAlgorithm>
                                    <messageDigest>2072C9B544878DE7DE0711441A8C61070B0872FA0A4F2A243B45BC11C43729F9</messageDigest>
                                </fixity>
                                <size>4321</size>
                                <format>
                                    <formatDesignation>
                                        <formatName>application/pdf</formatName>
                                        <formatVersion>1.5</formatVersion>
                                    </formatDesignation>
                                </format>
                            </objectCharacteristics>
                            <originalName>Annexe.pdf</originalName>
                            <significantProperties xmlns="" xmlns:premis="http://www.loc.gov/premis/v3">
                                <significantPropertiesType>i_Parapheur_reserved_mainDocument</significantPropertiesType>
                                <significantPropertiesValue>false</significantPropertiesValue>
                            </significantProperties>
                        </object>
                    </premis>\
                    """;

            String patched;
            try (InputStream bais = new ByteArrayInputStream(brokenExample.getBytes())) {
                patched = XmlUtils.patchTrashBinPremis(getClass().getClassLoader(), bais);
            }
            assertEquals(
                    XmlUtils.canonicalize(expected.getBytes()),
                    XmlUtils.canonicalize(patched.getBytes())
            );
        }


        @Test
        void emptyToSignificantProperty() throws TransformerException, IOException {

            String brokenExample =
                    """
                    <?xml version="1.0" encoding="UTF-8"?>
                    <premis xmlns="http://www.loc.gov/premis/v3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="3.0" \
                    xsi:schemaLocation="http://www.loc.gov/premis/v3 http://www.loc.gov/standards/premis/v3/premis.xsd">
                      <!-- Files -->
                      <object xsi:type="file">
                        <objectIdentifier>
                          <objectIdentifierType>documentId</objectIdentifierType>
                          <objectIdentifierValue>77bd7ea0-29c3-447e-bdfd-b6bb30d77e9d</objectIdentifierValue>
                        </objectIdentifier>
                        <preservationLevel>
                          <preservationLevelValue>mainDocument</preservationLevelValue>
                        </preservationLevel>
                        <objectCharacteristics>
                          <size>1234</size>
                          <format>
                            <formatDesignation>
                              <formatName>application/pdf</formatName>
                              <formatVersion>1.6</formatVersion>
                            </formatDesignation>
                          </format>
                        </objectCharacteristics>
                        <originalName>Document_foo.pdf</originalName>
                      </object>
                    </premis>\
                    """;

            String expected =
                    """
                    <?xml version="1.0" encoding="UTF-8"?>
                    <premis xmlns="http://www.loc.gov/premis/v3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="3.0" \
                    xsi:schemaLocation="http://www.loc.gov/premis/v3 http://www.loc.gov/standards/premis/v3/premis.xsd">
                        <!-- Files -->
                        <object xsi:type="file">
                            <objectIdentifier>
                                <objectIdentifierType>documentId</objectIdentifierType>
                                <objectIdentifierValue>77bd7ea0-29c3-447e-bdfd-b6bb30d77e9d</objectIdentifierValue>
                            </objectIdentifier>
                            <objectCharacteristics>
                                <size>1234</size>
                                <format>
                                    <formatDesignation>
                                        <formatName>application/pdf</formatName>
                                        <formatVersion>1.6</formatVersion>
                                    </formatDesignation>
                                </format>
                            </objectCharacteristics>
                            <originalName>Document_foo.pdf</originalName>
                            <significantProperties xmlns="" xmlns:premis="http://www.loc.gov/premis/v3">
                                <significantPropertiesType>i_Parapheur_reserved_mainDocument</significantPropertiesType>
                                <significantPropertiesValue>false</significantPropertiesValue>
                            </significantProperties>
                        </object>
                    </premis>\
                    """;

            String patched;
            try (InputStream bais = new ByteArrayInputStream(brokenExample.getBytes())) {
                patched = XmlUtils.patchTrashBinPremis(getClass().getClassLoader(), bais);
            }
            assertEquals(
                    XmlUtils.canonicalize(expected.getBytes()),
                    XmlUtils.canonicalize(patched.getBytes())
            );
        }
    }


    @Test
    void parsePremisString() throws IOException {

        PremisComplexType premisComplexType;
        try (InputStream exempleInputStream = new ByteArrayInputStream(PREMIS_EXAMPLE.getBytes())) {
            premisComplexType = XmlUtils.parsePremisString(exempleInputStream);
        }

        assertNotNull(premisComplexType);

        IntellectualEntity intellectualEntity = premisComplexType.getObject().stream()
                .filter(IntellectualEntity.class::isInstance)
                .map(IntellectualEntity.class::cast)
                .findFirst()
                .orElse(null);

        assertNotNull(intellectualEntity);
        assertNotNull(intellectualEntity.getOriginalName());
        assertEquals("test ssss", intellectualEntity.getOriginalName().getValue());
        assertEquals("PADES", getSignificantPropertyValue(intellectualEntity.getSignificantProperties(), SIGNIFICANT_PROPERTY_TYPE_KEY));
        assertEquals("Auto-visa", getSignificantPropertyValue(intellectualEntity.getSignificantProperties(), SIGNIFICANT_PROPERTY_SUBTYPE_KEY));
        assertEquals("2000-12-25", getSignificantPropertyValue(intellectualEntity.getSignificantProperties(), SIGNIFICANT_PROPERTY_DUE_DATE));

        File mainDocument = premisComplexType.getObject().stream()
                .filter(File.class::isInstance)
                .map(File.class::cast)
                .findFirst()
                .orElse(null);

        assertNotNull(mainDocument);
        assertNotNull(mainDocument.getOriginalName());
        assertEquals("copie-d-examen-simple-ou-double-1.pdf", mainDocument.getOriginalName().getValue());
        assertTrue(getSignificantBooleanPropertyValue(mainDocument.getSignificantProperties(), SIGNIFICANT_PROPERTY_MAIN_DOCUMENT_KEY));
    }


    @Test
    void folderToPremisXml() throws IOException, TransformerException {

        String expected =
                """
                <?xml version="1.0" encoding="UTF-8"?>
                <premis xmlns="http://www.loc.gov/premis/v3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="3.0" \
                xsi:schemaLocation="http://www.loc.gov/premis/v3 http://www.loc.gov/standards/premis/v3/premis.xsd">
                  <object xsi:type="intellectualEntity">
                    <objectIdentifier>
                      <objectIdentifierType>folderId</objectIdentifierType>
                      <objectIdentifierValue>12536f34-ce6c-444a-aa28-b9318e0b94f5</objectIdentifierValue>
                    </objectIdentifier>
                    <significantProperties>
                      <significantPropertiesType>i_Parapheur_reserved_type</significantPropertiesType>
                      <significantPropertiesValue>PES</significantPropertiesValue>
                    </significantProperties>
                    <significantProperties>
                      <significantPropertiesType>i_Parapheur_reserved_subtype</significantPropertiesType>
                      <significantPropertiesValue>Default</significantPropertiesValue>
                    </significantProperties>
                    <significantProperties>
                      <significantPropertiesType>i_Parapheur_reserved_dueDate</significantPropertiesType>
                      <significantPropertiesValue>2000-12-25</significantPropertiesValue>
                    </significantProperties>
                    <originalName>Folder 001</originalName>
                  </object>
                  <object xsi:type="file">
                    <objectIdentifier>
                      <objectIdentifierType>documentId</objectIdentifierType>
                      <objectIdentifierValue>77bd7ea0-29c3-447e-bdfd-b6bb30d77e9d</objectIdentifierValue>
                    </objectIdentifier>
                    <significantProperties>
                      <significantPropertiesType>i_Parapheur_reserved_mainDocument</significantPropertiesType>
                      <significantPropertiesValue>true</significantPropertiesValue>
                    </significantProperties>
                    <objectCharacteristics>
                      <fixity>
                        <messageDigestAlgorithm>SHA-256</messageDigestAlgorithm>
                        <messageDigest>B03D0710D8648314A5FF7D69DBDF3217F9D7775132D2993EBF6C2C896D9B5A81</messageDigest>
                      </fixity>
                      <size>1234</size>
                      <format>
                        <formatDesignation>
                          <formatName>application/pdf</formatName>
                          <formatVersion>1.6</formatVersion>
                        </formatDesignation>
                      </format>
                    </objectCharacteristics>
                    <originalName>Document_foo.pdf</originalName>
                    <signatureInformation>
                      <signature>
                        <signatureEncoding>UTF-8</signatureEncoding>
                        <signatureMethod>CAdES_BASELINE_B</signatureMethod>
                        <signatureValue>signature_1_value</signatureValue>
                        <signatureValidationRules>CAdES</signatureValidationRules>
                      </signature>
                    </signatureInformation>
                    <signatureInformation>
                      <signature>
                        <signatureEncoding>UTF-8</signatureEncoding>
                        <signatureMethod>CAdES_BASELINE_B</signatureMethod>
                        <signatureValue>signature_2_value</signatureValue>
                        <signatureValidationRules>CAdES</signatureValidationRules>
                      </signature>
                    </signatureInformation>
                  </object>
                  <object xsi:type="file">
                    <objectIdentifier>
                      <objectIdentifierType>documentId</objectIdentifierType>
                      <objectIdentifierValue>2d6684cc-1c9b-42f3-8a91-fb76bb617598</objectIdentifierValue>
                    </objectIdentifier>
                    <significantProperties>
                      <significantPropertiesType>i_Parapheur_reserved_mainDocument</significantPropertiesType>
                      <significantPropertiesValue>false</significantPropertiesValue>
                    </significantProperties>
                    <objectCharacteristics>
                      <fixity>
                        <messageDigestAlgorithm>SHA-256</messageDigestAlgorithm>
                        <messageDigest>2072C9B544878DE7DE0711441A8C61070B0872FA0A4F2A243B45BC11C43729F9</messageDigest>
                      </fixity>
                      <size>4321</size>
                      <format>
                        <formatDesignation>
                          <formatName>application/pdf</formatName>
                          <formatVersion>1.5</formatVersion>
                        </formatDesignation>
                      </format>
                    </objectCharacteristics>
                    <originalName>Annexe.pdf</originalName>
                  </object>
                  <event>
                    <eventIdentifier>
                      <eventIdentifierType>taskId</eventIdentifierType>
                      <eventIdentifierValue>e7daa1a7-fcdd-4f7e-81b7-1440c7a941a4</eventIdentifierValue>
                    </eventIdentifier>
                    <eventType>VISA</eventType>
                    <eventDateTime>1970-01-01T01:00:00.000+0100</eventDateTime>
                    <eventOutcomeInformation>
                      <eventOutcomeDetail>
                        <eventOutcomeDetailNote>Annotation Publique</eventOutcomeDetailNote>
                      </eventOutcomeDetail>
                    </eventOutcomeInformation>
                    <linkingAgentIdentifier>
                      <linkingAgentIdentifierType>userId</linkingAgentIdentifierType>
                      <linkingAgentIdentifierValue>458b3c8e-af55-4e3a-bd4f-2705c4b5b741</linkingAgentIdentifierValue>
                      <linkingAgentRole>Bureau de Machin</linkingAgentRole>
                    </linkingAgentIdentifier>
                  </event>
                  <event>
                    <eventIdentifier>
                      <eventIdentifierType>taskId</eventIdentifierType>
                      <eventIdentifierValue>f8ed5dd9-910f-4c33-bab7-854de49f0ea3</eventIdentifierValue>
                    </eventIdentifier>
                    <eventType>SIGNATURE</eventType>
                    <eventDateTime>1970-01-01T01:00:09.999+0100</eventDateTime>
                    <linkingAgentIdentifier>
                      <linkingAgentIdentifierType>userId</linkingAgentIdentifierType>
                      <linkingAgentIdentifierValue>f31b53a2-ffd8-416d-b24e-778bc69d3b8c</linkingAgentIdentifierValue>
                      <linkingAgentRole>Bureau de Truc</linkingAgentRole>
                    </linkingAgentIdentifier>
                  </event>
                  <agent>
                    <agentIdentifier>
                      <agentIdentifierType>userId</agentIdentifierType>
                      <agentIdentifierValue>458b3c8e-af55-4e3a-bd4f-2705c4b5b741</agentIdentifierValue>
                    </agentIdentifier>
                    <agentName>Gérard Plip</agentName>
                  </agent>
                  <agent>
                    <agentIdentifier>
                      <agentIdentifierType>userId</agentIdentifierType>
                      <agentIdentifierValue>f31b53a2-ffd8-416d-b24e-778bc69d3b8c</agentIdentifierValue>
                    </agentIdentifier>
                    <agentName>Robert Plop</agentName>
                  </agent>
                </premis>
                """;

        // Building example

        DeskRepresentation desk1 = new DeskRepresentation("deskId_01", "Bureau de Machin");
        DeskRepresentation desk2 = new DeskRepresentation("deskId_02", "Bureau de Truc");

        UserRepresentation user1 = new UserRepresentation("458b3c8e-af55-4e3a-bd4f-2705c4b5b741");
        user1.setFirstName("Gérard");
        user1.setLastName("Plip");

        UserRepresentation user2 = new UserRepresentation("f31b53a2-ffd8-416d-b24e-778bc69d3b8c");
        user2.setFirstName("Robert");
        user2.setLastName("Plop");

        Task task1 = Task.builder()
                .action(VISA)
                .performedAction(VISA)
                .state(VALIDATED)
                .date(new Date(0L))
                .desks(singletonList(desk1))
                .user(user1)
                .publicAnnotation("Annotation Publique")
                .build();

        Task task2 = Task.builder()
                .action(SIGNATURE)
                .performedAction(SIGNATURE)
                .state(VALIDATED)
                .date(new Date(9999L))
                .desks(singletonList(desk2))
                .user(user2)
                .build();

        task1.setId("e7daa1a7-fcdd-4f7e-81b7-1440c7a941a4");
        task2.setId("f8ed5dd9-910f-4c33-bab7-854de49f0ea3");

        Document document1 = Document.builder()
                .id("77bd7ea0-29c3-447e-bdfd-b6bb30d77e9d")
                .name("Document_foo.pdf")
                .contentLength(1234)
                .mediaType(APPLICATION_PDF)
                .mediaVersion(1.6F)
                .checksumValue("B03D0710D8648314A5FF7D69DBDF3217F9D7775132D2993EBF6C2C896D9B5A81")
                .isMainDocument(true)
                .detachedSignatures(asList(
                        new DetachedSignature("detachedSig_1",
                                "myFile1.p7s",
                                1234L,
                                TEXT_XML,
                                "77bd7ea0-29c3-447e-bdfd-b6bb30d77e9d",
                                UUID.randomUUID().toString(),
                                new ValidatedSignatureInformation()
                        ),
                        new DetachedSignature("detachedSig_2",
                                "myFile2.p7s",
                                1234L,
                                TEXT_XML,
                                "77bd7ea0-29c3-447e-bdfd-b6bb30d77e9d",
                                UUID.randomUUID().toString(),
                                new ValidatedSignatureInformation()
                        )
                ))
                .build();

        Document document2 = Document.builder()
                .id("2d6684cc-1c9b-42f3-8a91-fb76bb617598")
                .name("Annexe.pdf").contentLength(4321)
                .mediaType(APPLICATION_PDF)
                .mediaVersion(1.5F)
                .checksumValue("2072C9B544878DE7DE0711441A8C61070B0872FA0A4F2A243B45BC11C43729F9")
                .isMainDocument(false)
                .detachedSignatures(emptyList())
                .build();

        Folder folder = new Folder();
        folder.setId("12536f34-ce6c-444a-aa28-b9318e0b94f5");
        folder.setName("Folder 001");
        folder.setType(Type.builder().id("8ee47ea0-13b4-444f-a2a8-8c62dd53ba7c").signatureFormat(XADES_DETACHED).name("PES").build());
        folder.setSubtype(Subtype.builder().id("a023cbf0-6605-4913-92c7-5b75bcc6af9b").name("Default").build());
        folder.setDueDate(new Date(977745600000L));
        folder.setStepList(asList(task1, task2));
        folder.setDocumentList(asList(document1, document2));

        // Actual tests

        try (InputStream signature1InputStream = new ByteArrayInputStream("signature_1_value".getBytes(UTF_8));
             InputStream signature2InputStream = new ByteArrayInputStream("signature_2_value".getBytes(UTF_8))) {

            String result = XmlUtils.folderToPremisXml(
                    folder,
                    s -> switch (s) {
                        case "detachedSig_1" -> signature1InputStream;
                        case "detachedSig_2" -> signature2InputStream;
                        default -> null;
                    }
            );

            assertNotNull(result);
            // TODO: Fix the locale in the CI
//            assertEquals(
//                    expected.trim(),
//                    XmlUtils.prettyPrint(result).trim()
//            );
        }
    }


}
