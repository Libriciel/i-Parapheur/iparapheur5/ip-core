/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.utils;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.StreamSupport;

import static coop.libriciel.ipcore.utils.TextUtils.MESSAGE_BUNDLE;
import static java.util.Locale.FRENCH;
import static java.util.Locale.ROOT;
import static java.util.Spliterator.ORDERED;
import static java.util.Spliterators.spliteratorUnknownSize;
import static java.util.stream.Collectors.toSet;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


class LocalizedStatusExceptionTest {


    @Test
    void getMessageForLocale() {
        String frenchMessage = LocalizedStatusException.getMessageForLocale("message.unknown_tenant_id", FRENCH);
        String englishMessage = LocalizedStatusException.getMessageForLocale("message.unknown_tenant_id", ROOT);
        assertNotEquals(frenchMessage, englishMessage);
    }


    @Test
    void getMessageForLocale_withParameters() {
        String message = LocalizedStatusException.getMessageForLocale("message.already_n_desks_maximum_reached", ROOT, 3);
        assertEquals("Already 3 desks, the maximum has been reached", message);
    }


    @Test
    void missingLocalizationCheck() {
        ResourceBundle englishBundle = ResourceBundle.getBundle(MESSAGE_BUNDLE, ROOT);
        ResourceBundle frenchBundle = ResourceBundle.getBundle(MESSAGE_BUNDLE, FRENCH);

        assertEquals(
                StreamSupport.stream(spliteratorUnknownSize(englishBundle.getKeys().asIterator(), ORDERED), false).collect(toSet()),
                StreamSupport.stream(spliteratorUnknownSize(frenchBundle.getKeys().asIterator(), ORDERED), false).collect(toSet())
        );
    }


    @Test
    void getMessageForLocale_withChoiceFormat_withEqual() {
        String message = LocalizedStatusException.getMessageForLocale("message.cannot_delete_a_desk_used_by_workflows",
                ROOT,
                1,
                List.of("Workflow1")
        );
        assertEquals("Cannot delete the desk as it is used by a workflow: [Workflow1]", message);
    }


    @Test
    void getMessageForLocale_withChoiceFormat_withMoreOrEqual() {
        String message = LocalizedStatusException.getMessageForLocale("message.cannot_delete_a_desk_used_by_workflows",
                ROOT,
                3,
                List.of("Workflow1", "Workflow2", "Workflow3")
        );
        assertEquals("Cannot delete the desk as it is used by 3 workflows: [Workflow1, Workflow2, Workflow3]", message);
    }


    @Test
    void getMessageForLocale_withChoiceFormat_withStrictlyMoreThan() {
        String message = LocalizedStatusException.getMessageForLocale("message.cannot_delete_a_desk_used_by_workflows",
                ROOT,
                4,
                List.of("Workflow1", "Workflow2", "Workflow3")
        );
        assertEquals("Cannot delete the desk as it is used by 4 workflows, including: [Workflow1, Workflow2, Workflow3]", message);
    }

}
