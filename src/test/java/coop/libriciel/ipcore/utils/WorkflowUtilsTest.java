/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class WorkflowUtilsTest {


    @Test
    void nameToKey() {
        assertEquals("abc_1", WorkflowUtils.nameToKey("abc_1"));
        assertEquals("___", WorkflowUtils.nameToKey("   "));
        assertEquals("abc", WorkflowUtils.nameToKey("ABC"));
        assertEquals("_abc_", WorkflowUtils.nameToKey(" ABC "));
        assertEquals("_1abc", WorkflowUtils.nameToKey("1ABC"));
        assertEquals("_5abc", WorkflowUtils.nameToKey("5ABC"));
        assertEquals("a_b_c-d", WorkflowUtils.nameToKey("a:b(c-d"));
    }


}
