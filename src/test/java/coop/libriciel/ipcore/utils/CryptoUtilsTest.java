/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.utils;

import coop.libriciel.ipcore.model.externalsignature.SignRequestMember;
import org.apache.commons.lang3.RandomStringUtils;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static coop.libriciel.ipcore.model.crypto.SignatureFormat.*;
import static coop.libriciel.ipcore.model.externalsignature.ExternalSignatureParams.*;
import static coop.libriciel.ipcore.model.externalsignature.ExternalSignatureParams.METADATA_EXTERNAL_SIGNATURE_KEY_PHONE;
import static coop.libriciel.ipcore.utils.CryptoUtils.EXAMPLE_PUBLIC_CERTIFICATE_BASE64;
import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.*;


class CryptoUtilsTest {


    @Test
    void parseX509Certificate() {

        JcaX509CertificateHolder jcaX509CertificateHolder = CryptoUtils.parseX509Certificate(EXAMPLE_PUBLIC_CERTIFICATE_BASE64);
        assertNotNull(jcaX509CertificateHolder);

        assertEquals(1689501897000L, jcaX509CertificateHolder.getNotAfter().getTime());
        assertEquals(1626429897000L, jcaX509CertificateHolder.getNotBefore().getTime());
    }


    @Test
    void getCnStringValue() {

        JcaX509CertificateHolder jcaX509CertificateHolder = CryptoUtils.parseX509Certificate(EXAMPLE_PUBLIC_CERTIFICATE_BASE64);
        assertNotNull(jcaX509CertificateHolder);

        X500Name issuerX500Name = jcaX509CertificateHolder.getIssuer();
        assertNotNull(issuerX500Name);
        assertEquals("AC_ADULLACT_Utilisateurs_G3", CryptoUtils.parseInnerX500Values(issuerX500Name).getCommonName());

        X500Name subjectX500Name = jcaX509CertificateHolder.getSubject();
        assertNotNull(subjectX500Name);
        assertEquals("Test_Tilo", CryptoUtils.parseInnerX500Values(subjectX500Name).getCommonName());
    }


    @Test
    void checkDetachedSignatures() {
        assertDoesNotThrow(() -> CryptoUtils.checkIfDetachedSignatureAllowed(PKCS7, true));

        assertDoesNotThrow(() -> CryptoUtils.checkIfDetachedSignatureAllowed(XADES_DETACHED, true));

        assertDoesNotThrow(() -> CryptoUtils.checkIfDetachedSignatureAllowed(PES_V2, false));

        assertThrows(
                RuntimeException.class,
                () -> CryptoUtils.checkIfDetachedSignatureAllowed(AUTO, true)
        );

        assertThrows(
                LocalizedStatusException.class,
                () -> CryptoUtils.checkIfDetachedSignatureAllowed(PES_V2, true)
        );

        assertThrows(
                LocalizedStatusException.class,
                () -> CryptoUtils.checkIfDetachedSignatureAllowed(PADES, true)
        );
    }


    @Nested
    class GetExternalSignatureMetadataMap {


        @Test
        void empty() {
            Map<String, String> metadataMap = new HashMap<>();
            Map<Long, SignRequestMember> result = CryptoUtils.getExternalSignatureMetadataMap(metadataMap);
            assertEquals(0, result.size());
        }


        @Test
        void incompleteMetadata() {
            Map<String, String> metadataMap = Map.of(
                    METADATA_EXTERNAL_SIGNATURE_KEY_FIRSTNAME, RandomStringUtils.insecure().nextAlphabetic(36),
                    METADATA_EXTERNAL_SIGNATURE_KEY_LASTNAME, RandomStringUtils.insecure().nextAlphabetic(36)
            );
            Map<Long, SignRequestMember> result = CryptoUtils.getExternalSignatureMetadataMap(metadataMap);
            assertEquals(1, result.size());
            assertTrue(result.containsKey(0L));
            assertFalse(CryptoUtils.isConfigurationComplete(result.get(0L)));
        }


        @Test
        void defaultMetadata() {
            Map<String, String> metadataMap = Map.of(
                    METADATA_EXTERNAL_SIGNATURE_KEY_FIRSTNAME, RandomStringUtils.insecure().nextAlphabetic(36),
                    METADATA_EXTERNAL_SIGNATURE_KEY_LASTNAME, RandomStringUtils.insecure().nextAlphabetic(36),
                    METADATA_EXTERNAL_SIGNATURE_KEY_MAIL, "%s@invalid".formatted(RandomStringUtils.insecure().nextAlphabetic(36)),
                    METADATA_EXTERNAL_SIGNATURE_KEY_PHONE, RandomStringUtils.insecure().nextNumeric(10)
            );
            Map<Long, SignRequestMember> result = CryptoUtils.getExternalSignatureMetadataMap(metadataMap);
            assertEquals(1, result.size());
            assertTrue(result.containsKey(0L));
            assertTrue(CryptoUtils.isConfigurationComplete(result.get(0L)));
        }


        @Test
        void numberedMetadata() {
            String numberSuffix = "5";
            Map<String, String> metadataMap = Map.of(
                    METADATA_EXTERNAL_SIGNATURE_KEY_FIRSTNAME + numberSuffix, RandomStringUtils.insecure().nextAlphabetic(36),
                    METADATA_EXTERNAL_SIGNATURE_KEY_LASTNAME + numberSuffix, RandomStringUtils.insecure().nextAlphabetic(36),
                    METADATA_EXTERNAL_SIGNATURE_KEY_MAIL + numberSuffix, "%s@invalid".formatted(RandomStringUtils.insecure().nextAlphabetic(36)),
                    METADATA_EXTERNAL_SIGNATURE_KEY_PHONE + numberSuffix, RandomStringUtils.insecure().nextNumeric(10)
            );
            Map<Long, SignRequestMember> result = CryptoUtils.getExternalSignatureMetadataMap(metadataMap);
            assertEquals(1, result.size());
            assertTrue(result.containsKey(5L));
            assertTrue(CryptoUtils.isConfigurationComplete(result.get(5L)));
        }


    }


}
