/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.utils;

import lombok.Data;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.LongSupplier;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


class CollectionUtilsTest {


    @Nested
    class ComputeTotal {


        @Test
        void noData() {

            LongSupplier totalSupplier = Mockito.mock(LongSupplier.class);
            Mockito.doReturn(0L).when(totalSupplier).getAsLong();
            long result = CollectionUtils.computeTotal(emptyList(), 0, 4, totalSupplier);

            assertEquals(0L, result);
            verify(totalSupplier, times(0)).getAsLong();
        }


        @Test
        void middlePage() {

            LongSupplier totalSupplier = Mockito.mock(LongSupplier.class);
            Mockito.doReturn(999L).when(totalSupplier).getAsLong();
            long result = CollectionUtils.computeTotal(List.of("A", "B", "C", "D"), 4, 4, totalSupplier);

            assertEquals(999L, result);
            verify(totalSupplier, times(1)).getAsLong();
        }


        @Test
        void partialLastPage() {

            LongSupplier totalSupplier = Mockito.mock(LongSupplier.class);
            Mockito.doReturn(18L).when(totalSupplier).getAsLong();
            long result = CollectionUtils.computeTotal(List.of("A", "B"), 4, 4, totalSupplier);

            assertEquals(18L, result);
            verify(totalSupplier, times(0)).getAsLong();
        }


        @Test
        void fullLastPage() {

            LongSupplier totalSupplier = Mockito.mock(LongSupplier.class);
            Mockito.doReturn(20L).when(totalSupplier).getAsLong();
            long result = CollectionUtils.computeTotal(List.of("A", "B", "C", "D"), 4, 4, totalSupplier);

            assertEquals(20L, result);
            verify(totalSupplier, times(1)).getAsLong();
        }


        @Test
        void brokenTotal() {

            LongSupplier totalSupplier = Mockito.mock(LongSupplier.class);
            Mockito.doReturn(-1L).when(totalSupplier).getAsLong();
            long result = CollectionUtils.computeTotal(List.of("A", "B", "C", "D"), 4, 4, totalSupplier);

            assertEquals(-1L, result);
            verify(totalSupplier, times(1)).getAsLong();
        }


        @Test
        void overflowPage() {

            LongSupplier totalSupplier = Mockito.mock(LongSupplier.class);
            Mockito.doReturn(22L).when(totalSupplier).getAsLong();
            long result = CollectionUtils.computeTotal(emptyList(), 9999, 4, totalSupplier);

            assertEquals(22L, result);
            verify(totalSupplier, times(1)).getAsLong();
        }


    }


    @Test
    void toMutableList() {
        List<Integer> integerList = Stream.of(1, 2, 3).collect(CollectionUtils.toMutableList());
        assertEquals(3, integerList.size());
        assertDoesNotThrow(() -> integerList.add(4));
    }


    @Test
    void toMutableSet() {
        Set<Integer> integerSet = Stream.of(1, 2, 3).collect(CollectionUtils.toMutableSet());
        assertEquals(3, integerSet.size());
        assertDoesNotThrow(() -> integerSet.add(4));
    }


    @Test
    void parseMetadata() {
        String[][] dataTest = {{"testKey1", "testValue1"}, {"testKey2", "testValue2"}};
        Map<String, String> parsed = CollectionUtils.parseMetadata(dataTest);

        assertEquals("testValue1", parsed.get("testKey1"));
        assertEquals("testValue2", parsed.get("testKey2"));
        assertNull(parsed.get("testKey99"));

        assertNotNull(CollectionUtils.parseMetadata(null));
    }


    @Test
    void isSequential() {

        assertTrue(CollectionUtils.isSequential(emptyList()));
        assertTrue(CollectionUtils.isSequential(List.of(1L)));
        assertTrue(CollectionUtils.isSequential(List.of(1L, 2L)));
        assertTrue(CollectionUtils.isSequential(List.of(-3L, -2L, -1L)));
        assertTrue(CollectionUtils.isSequential(List.of(7L, 4L, 6L, 5L)));
        assertTrue(CollectionUtils.isSequential(List.of(-2L, -1L, 0L, 1L, 2L)));

        assertFalse(CollectionUtils.isSequential(List.of(1L, 3L)));
        assertFalse(CollectionUtils.isSequential(List.of(1L, 2L, 2L, 4L)));
    }


    @Nested
    class StreamWithPaginatedRequest {


        /**
         * Mockito cannot spy a simple dummy Function<> retrieval.
         * Assert.equals cannot compare spy-ed lists and regular lists neither.
         * So it is easier to create a dummy class, to count how many times the getPage has been called:
         * We can naively compare everything, and spy the getPage method.
         */
        @SuppressWarnings("LombokGetterMayBeUsed")
        private static class TestIntegerList {


            private final List<Integer> list;


            TestIntegerList(int size) {
                this.list = new ArrayList<>();
                IntStream.range(0, size).forEach(list::add);
            }


            public List<Integer> getList() {
                return list;
            }


            public Page<Integer> getPage(@NotNull Pageable pageable) {
                return new PageImpl<>(
                        list.stream().skip(pageable.getOffset()).limit(pageable.getPageSize()).toList(),
                        pageable,
                        list.size()
                );
            }


        }


        @Test
        void singlePage() {

            TestIntegerList sourceList = spy(new TestIntegerList(10));

            List<Integer> retrievedList = CollectionUtils
                    .streamWithPaginatedRequest(sourceList::getPage, 100)
                    .toList();

            assertEquals(sourceList.getList(), retrievedList);
            verify(sourceList, times(1)).getPage(any());
        }


        @Test
        void multiplePagesWithPartialLast() {

            TestIntegerList sourceList = spy(new TestIntegerList(10));

            List<Integer> retrievedList = CollectionUtils
                    .streamWithPaginatedRequest(sourceList::getPage, 4)
                    .toList();

            assertEquals(sourceList.getList(), retrievedList);
            verify(sourceList, times(3)).getPage(any());
        }


        @Test
        void multiplePagesWithCompleteLast() {

            TestIntegerList sourceList = spy(new TestIntegerList(20));

            List<Integer> retrievedList = CollectionUtils
                    .streamWithPaginatedRequest(sourceList::getPage, 10)
                    .toList();

            assertEquals(sourceList.getList(), retrievedList);
            verify(sourceList, times(2)).getPage(any());
        }

    }


}
