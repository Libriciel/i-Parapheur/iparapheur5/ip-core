/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.utils;

import coop.libriciel.ipcore.model.crypto.PdfSignaturePosition;
import org.apache.pdfbox.Loader;
import org.apache.pdfbox.io.RandomAccessReadBuffer;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.junit.jupiter.api.Test;

import java.io.InputStream;

import static org.apache.pdfbox.io.IOUtils.createMemoryOnlyStreamCache;
import static org.apache.pdfbox.io.IOUtils.createTempFileOnlyStreamCache;
import static org.apache.pdfbox.io.MemoryUsageSetting.setupTempFileOnly;
import static org.junit.jupiter.api.Assertions.assertEquals;


class PdfTextLocationStripperTest {


    private final ClassLoader classLoader = getClass().getClassLoader();


    @Test
    void notFound() throws Exception {

        try (InputStream pdfInputStream = classLoader.getResourceAsStream("pdf/signature_tag.pdf");
             PDDocument pdDocument = Loader.loadPDF(new RandomAccessReadBuffer(pdfInputStream), createMemoryOnlyStreamCache())) {

            PdfTextLocationStripper stripper = new PdfTextLocationStripper("@not_the_right_tag@", null);
            stripper.search(pdDocument);

            assertEquals(0, stripper.getSignatureTagsFound().size());
        }
    }


    @Test
    void signatureTag() throws Exception {

        try (InputStream pdfInputStream = classLoader.getResourceAsStream("pdf/signature_tag.pdf");
             PDDocument pdDocument = Loader.loadPDF(new RandomAccessReadBuffer(pdfInputStream), createMemoryOnlyStreamCache())) {

            PdfTextLocationStripper stripper = new PdfTextLocationStripper(null, null);
            stripper.search(pdDocument);

            assertEquals(1, stripper.getSignatureTagsFound().size());

            PdfSignaturePosition result1 = stripper.getSignatureTagsFound().get(0);
            assertEquals(1, result1.getPage());
            assertEquals(245.1f, result1.getX(), 0.1f);
            assertEquals(733.0f, result1.getY(), 0.1f);
        }
    }


    @Test
    void signatureIndexedTags() throws Exception {

        try (InputStream pdfInputStream = classLoader.getResourceAsStream("pdf/signature_indexed_tags.pdf");
             PDDocument pdDocument = Loader.loadPDF(new RandomAccessReadBuffer(pdfInputStream), createMemoryOnlyStreamCache())) {

            PdfTextLocationStripper stripper = new PdfTextLocationStripper("#signature_%n#", null);
            stripper.search(pdDocument);

            assertEquals(4, stripper.getSignatureTagsFound().size());

            PdfSignaturePosition result1 = stripper.getSignatureTagsFound().get(1);
            assertEquals(1, result1.getPage());
            assertEquals(266.1f, result1.getX(), 0.1f);
            assertEquals(733.0f, result1.getY(), 0.1f);

            PdfSignaturePosition result2 = stripper.getSignatureTagsFound().get(2);
            assertEquals(1, result2.getPage());
            assertEquals(434.1f, result2.getX(), 0.1f);
            assertEquals(567.4f, result2.getY(), 0.1f);

            PdfSignaturePosition result3 = stripper.getSignatureTagsFound().get(3);
            assertEquals(1, result3.getPage());
            assertEquals(161.1f, result3.getX(), 0.1f);
            assertEquals(622.6f, result3.getY(), 0.1f);

            PdfSignaturePosition result7 = stripper.getSignatureTagsFound().get(7);
            assertEquals(1, result7.getPage());
            assertEquals(485.1f, result7.getX(), 0.1f);
            assertEquals(677.8f, result7.getY(), 0.1f);
        }
    }


    @Test
    void signatureDefaultAndIndexedTags() throws Exception {

        try (InputStream pdfInputStream = classLoader.getResourceAsStream("pdf/default_and_indexed_tags.pdf");
             PDDocument pdDocument = Loader.loadPDF(new RandomAccessReadBuffer(pdfInputStream), createMemoryOnlyStreamCache())) {

            PdfTextLocationStripper stripper = new PdfTextLocationStripper("#signature%n#", null);
            stripper.search(pdDocument);

            assertEquals(3, stripper.getSignatureTagsFound().size());

            PdfSignaturePosition result0 = stripper.getSignatureTagsFound().get(0);
            assertEquals(1, result0.getPage());
            assertEquals(111.8f, result0.getX(), 0.1f);
            assertEquals(733.0f, result0.getY(), 0.1f);

            PdfSignaturePosition result1 = stripper.getSignatureTagsFound().get(1);
            assertEquals(1, result1.getPage());
            assertEquals(141.8f, result1.getX(), 0.1f);
            assertEquals(691.7f, result1.getY(), 0.1f);

            PdfSignaturePosition result2 = stripper.getSignatureTagsFound().get(2);
            assertEquals(1, result2.getPage());
            assertEquals(165.8f, result2.getX(), 0.1f);
            assertEquals(650.3f, result2.getY(), 0.1f);
        }
    }


    @Test
    void sealIndexedTags() throws Exception {

        try (InputStream pdfInputStream = classLoader.getResourceAsStream("pdf/seal_indexed_tags.pdf");
             PDDocument pdDocument = Loader.loadPDF(new RandomAccessReadBuffer(pdfInputStream), createMemoryOnlyStreamCache())) {

            PdfTextLocationStripper stripper = new PdfTextLocationStripper(null, "#cachet_%n#");
            stripper.search(pdDocument);

            assertEquals(4, stripper.getSealTagsFound().size());

            PdfSignaturePosition result1 = stripper.getSealTagsFound().get(1);
            assertEquals(1, result1.getPage());
            assertEquals(132.8f, result1.getX(), 0.1f);
            assertEquals(760.6f, result1.getY(), 0.1f);

            PdfSignaturePosition result2 = stripper.getSealTagsFound().get(2);
            assertEquals(1, result2.getPage());
            assertEquals(492.8f, result2.getX(), 0.1f);
            assertEquals(746.8f, result2.getY(), 0.1f);

            PdfSignaturePosition result3 = stripper.getSealTagsFound().get(3);
            assertEquals(1, result3.getPage());
            assertEquals(132.8f, result3.getX(), 0.1f);
            assertEquals(125.8f, result3.getY(), 0.1f);

            PdfSignaturePosition result4 = stripper.getSealTagsFound().get(4);
            assertEquals(1, result4.getPage());
            assertEquals(492.8f, result4.getX(), 0.1f);
            assertEquals(112.0f, result4.getY(), 0.1f);
        }
    }


    @Test
    void multiTags() throws Exception {

        try (InputStream pdfInputStream = classLoader.getResourceAsStream("pdf/multi_tags.pdf");
             PDDocument pdDocument = Loader.loadPDF(new RandomAccessReadBuffer(pdfInputStream), createMemoryOnlyStreamCache())) {

            PdfTextLocationStripper stripper = new PdfTextLocationStripper("#signature#", "#cachet#");
            stripper.search(pdDocument);

            assertEquals(1, stripper.getSignatureTagsFound().size());
            PdfSignaturePosition signaturePosition = stripper.getSignatureTagsFound().get(0);
            assertEquals(1, signaturePosition.getPage());
            assertEquals(170.1f, signaturePosition.getX(), 0.1f);
            assertEquals(719.2f, signaturePosition.getY(), 0.1f);

            assertEquals(1, stripper.getSealTagsFound().size());
            PdfSignaturePosition sealPosition = stripper.getSealTagsFound().get(0);
            assertEquals(1, sealPosition.getPage());
            assertEquals(300.8f, sealPosition.getX(), 0.1f);
            assertEquals(595.0f, sealPosition.getY(), 0.1f);
        }
    }


}
