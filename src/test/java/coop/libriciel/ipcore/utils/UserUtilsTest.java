/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.utils;

import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.auth.requests.UserRepresentation;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.junit.jupiter.api.Assertions.*;


class UserUtilsTest {


    @Nested
    class PrettyPrint {


        @Test
        void user() {

            assertEquals(EMPTY, UserUtils.prettyPrint((User) null));

            User user = new User();
            assertEquals(EMPTY, UserUtils.prettyPrint(user));

            user.setId(UUID.randomUUID().toString());
            assertEquals(user.getId(), UserUtils.prettyPrint(user));

            user.setUserName(RandomStringUtils.insecure().nextAlphanumeric(64));
            assertEquals(user.getUserName(), UserUtils.prettyPrint(user));

            user.setUserName(RandomStringUtils.insecure().nextAlphanumeric(64));
            assertEquals(user.getUserName(), UserUtils.prettyPrint(user));

            user.setFirstName(RandomStringUtils.insecure().nextAlphanumeric(64));
            assertEquals(user.getFirstName(), UserUtils.prettyPrint(user));

            user.setLastName(RandomStringUtils.insecure().nextAlphanumeric(64));
            assertEquals("%s %s".formatted(user.getFirstName(), user.getLastName()), UserUtils.prettyPrint(user));
        }


        @Test
        void userRepresentation() {

            assertEquals(EMPTY, UserUtils.prettyPrint((UserRepresentation) null));

            UserRepresentation user = new UserRepresentation();
            assertEquals(EMPTY, UserUtils.prettyPrint(user));

            user.setId(UUID.randomUUID().toString());
            assertEquals(user.getId(), UserUtils.prettyPrint(user));

            user.setUserName(RandomStringUtils.insecure().nextAlphanumeric(64));
            assertEquals(user.getUserName(), UserUtils.prettyPrint(user));

            user.setUserName(RandomStringUtils.insecure().nextAlphanumeric(64));
            assertEquals(user.getUserName(), UserUtils.prettyPrint(user));

            user.setFirstName(RandomStringUtils.insecure().nextAlphanumeric(64));
            assertEquals(user.getFirstName(), UserUtils.prettyPrint(user));

            user.setLastName(RandomStringUtils.insecure().nextAlphanumeric(64));
            assertEquals("%s %s".formatted(user.getFirstName(), user.getLastName()), UserUtils.prettyPrint(user));
        }


    }


}
