/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.utils;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.http.MediaType.*;


class FileUtilsTest {


    @Test
    void enumerateOnDuplicates() {
        assertEquals("test", FileUtils.enumerateOnDuplicates("test", Set.of()));
        assertEquals("test.pdf", FileUtils.enumerateOnDuplicates("test.pdf", Set.of()));
        assertEquals("test.pdf", FileUtils.enumerateOnDuplicates("test.pdf", Set.of("some other file.pdf")));
        assertEquals("test (2).pdf", FileUtils.enumerateOnDuplicates("test.pdf", Set.of("test.pdf")));
        assertEquals("test (4).pdf", FileUtils.enumerateOnDuplicates("test.pdf", Set.of("test.pdf", "test (2).pdf", "test (3).pdf")));
        assertEquals("test (2) (2).pdf", FileUtils.enumerateOnDuplicates("test (2).pdf", Set.of("test (2).pdf")));
    }


    @Test
    void sanitizeFileName() {

        assertEquals("test_test", FileUtils.sanitizeFileName("test?test"));
        assertEquals("test_test", FileUtils.sanitizeFileName("test|test"));
        assertEquals("test_test", FileUtils.sanitizeFileName("test\\test"));
        assertEquals("test_test", FileUtils.sanitizeFileName("test*test"));
        assertEquals("test_test", FileUtils.sanitizeFileName("test/test"));
        assertEquals("test-test", FileUtils.sanitizeFileName("test:test"));
        assertEquals("_test_.pdf", FileUtils.sanitizeFileName("<test>.pdf"));

        assertEquals(255, "%s (99).docx".formatted(FileUtils.sanitizeFileName(RandomStringUtils.insecure().nextAlphanumeric(300))).length());
    }


    @Test
    void parseFileNameFromAttachmentTest() {
        assertEquals("test 1.pdf", FileUtils.parseFileNameFromAttachment("attachment; filename=\"test 1.pdf\"; filename*=UTF-8''..."));
        assertEquals("(2).pdf", FileUtils.parseFileNameFromAttachment("attachment; filename=\"(2).pdf\"; filename*=UTF-8''..."));
        assertNull(FileUtils.parseFileNameFromAttachment("not_a_real_header"));
        assertNull(FileUtils.parseFileNameFromAttachment(null));
    }


    @Test
    void isXml() {
        assertTrue(FileUtils.isXml(TEXT_XML));
        assertTrue(FileUtils.isXml(APPLICATION_XML));
        assertFalse(FileUtils.isXml(TEXT_PLAIN));
        assertFalse(FileUtils.isXml(null));
    }


}
