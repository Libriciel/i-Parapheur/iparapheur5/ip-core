/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.utils;

import org.junit.jupiter.api.Test;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;


class PaginatedListTest {


    // <editor-fold desc="Utils">


    static long failWithLongResult() {
        fail();
        return 999;
    }


    // </editor-fold desc="Utils">


    @Test
    void constructor_fullPage() {
        PaginatedList<String> result = new PaginatedList<>(asList("1", "2", "3", "4", "5"), 0, 5, () -> 7L);
        assertEquals(7, result.getTotal());
    }


    @Test
    void constructor_lastPage() {
        PaginatedList<String> result = new PaginatedList<>(asList("6", "7"), 1, 5, PaginatedListTest::failWithLongResult);
        assertEquals(7, result.getTotal());
    }


    @Test
    void constructor_emptyList() {
        PaginatedList<String> result = new PaginatedList<>(emptyList(), 0, 10, PaginatedListTest::failWithLongResult);
        assertEquals(0, result.getTotal());
    }


    @Test
    void constructor_overheadRequest() {
        PaginatedList<String> result = new PaginatedList<>(emptyList(), 99, 10, () -> 7L);
        assertEquals(7, result.getTotal());
    }

}
