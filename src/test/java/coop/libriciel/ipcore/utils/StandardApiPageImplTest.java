/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import coop.libriciel.ipcore.model.common.StandardApiPageImpl;
import coop.libriciel.ipcore.model.database.requests.TypeRepresentation;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest
public class StandardApiPageImplTest {

    private @Autowired ObjectMapper objectMapper;


    String serializedPageImplStd = """
                                   {
                                     "content": [
                                       {
                                         "id": "a23cf174-79c4-4295-8ef8-8090d59c9ab6",
                                         "name": "auto",
                                         "description": "auto"
                                       },
                                       {
                                         "id": "b56b2f07-b02f-4007-8bef-67be320b91a7",
                                         "name": "detached-cades",
                                         "description": "detached1"
                                       },
                                       {
                                         "id": "6183e313-d4f3-46f3-b6a7-00f5d43bef8d",
                                         "name": "detached-xades",
                                         "description": "xades-détaché"
                                       }
                                     ],
                                     "empty": false,
                                     "first": false,
                                     "last": true,
                                     "number": 1,
                                     "numberOfElements": 3,
                                     "size": 5,
                                     "totalElements": 8,
                                     "totalPages": 2,
                                     "sort": {
                                       "empty": false,
                                       "sorted": true,
                                       "unsorted": false
                                     },
                                     "pageable": {
                                       "offset": 5,
                                       "pageNumber": 1,
                                       "pageSize": 5,
                                       "paged": true,
                                       "unpaged": false,
                                       "sort": {
                                         "empty": false,
                                         "sorted": true,
                                         "unsorted": false
                                       }
                                     }
                                   }
                                   """;

    String serializedPageImplEmpty = """
                                     {
                                       "content": [],
                                       "empty": true,
                                       "first": false,
                                       "last": true,
                                       "number": 1,
                                       "numberOfElements": 0,
                                       "size": 5,
                                       "totalElements": 5,
                                       "totalPages": 1,
                                       "sort": {
                                         "empty": false,
                                         "sorted": true,
                                         "unsorted": false
                                       },
                                       "pageable": {
                                         "offset": 5,
                                         "pageNumber": 1,
                                         "pageSize": 5,
                                         "paged": true,
                                         "unpaged": false,
                                         "sort": {
                                           "empty": false,
                                           "sorted": true,
                                           "unsorted": false
                                         }
                                       }
                                     }
                                     """;

    String serializedPageImplFullPage = """
                                        {
                                          "content": [
                                            {
                                              "id": "a23cf174-79c4-4295-8ef8-8090d59c9ab6",
                                              "name": "auto",
                                              "description": "auto"
                                            },
                                            {
                                              "id": "b56b2f07-b02f-4007-8bef-67be320b91a7",
                                              "name": "detached-cades",
                                              "description": "detached1"
                                            },
                                            {
                                              "id": "6183e313-d4f3-46f3-b6a7-00f5d43bef8d",
                                              "name": "detached-xades",
                                              "description": "xades-détaché"
                                            }
                                          ],
                                          "empty": false,
                                          "first": true,
                                          "last": true,
                                          "number": 0,
                                          "numberOfElements": 3,
                                          "size": 3,
                                          "totalElements": 3,
                                          "totalPages": 1,
                                          "sort": {
                                            "empty": false,
                                            "sorted": true,
                                            "unsorted": false
                                          },
                                          "pageable": {
                                            "offset": 0,
                                            "pageNumber": 0,
                                            "pageSize": 3,
                                            "paged": true,
                                            "unpaged": false,
                                            "sort": {
                                              "empty": false,
                                              "sorted": true,
                                              "unsorted": false
                                            }
                                          }
                                        }
                                        """;


    @Test
    void deserialization_reserialization() throws JsonProcessingException {
        StandardApiPageImpl<TypeRepresentation> parsed = objectMapper.readValue(serializedPageImplStd, new TypeReference<>() {});

        assertEquals(3, parsed.getContent().size());
        assertEquals(1, parsed.getNumber());
        assertEquals(5, parsed.getSize());
        assertEquals(8, parsed.getTotalElements());
        assertEquals(2, parsed.getTotalPages());
        assertEquals(3, parsed.getNumberOfElements());

        assertEquals("a23cf174-79c4-4295-8ef8-8090d59c9ab6", parsed.getContent().get(0).getId());

        String reserialized = objectMapper.writeValueAsString(parsed);
        System.out.println(reserialized);
        assertEquals(serializedPageImplStd.replaceAll("[\\s\n]", ""), reserialized);
    }


    @Test
    void serialization_std_case() throws JsonProcessingException {

        TypeRepresentation t1 = new TypeRepresentation("a23cf174-79c4-4295-8ef8-8090d59c9ab6", "auto", "auto");
        TypeRepresentation t2 = new TypeRepresentation("b56b2f07-b02f-4007-8bef-67be320b91a7", "detached-cades", "detached1");
        TypeRepresentation t3 = new TypeRepresentation("6183e313-d4f3-46f3-b6a7-00f5d43bef8d", "detached-xades", "xades-détaché");

        List<TypeRepresentation> content = Arrays.asList(t1, t2, t3);
        Pageable pageable = PageRequest.of(1, 5, Sort.by("name"));

        StandardApiPageImpl<TypeRepresentation> page = new StandardApiPageImpl<>(content, pageable, 8);

        assertEquals(3, page.getContent().size());
        assertEquals(1, page.getNumber());
        assertEquals(5, page.getSize());
        assertEquals(8, page.getTotalElements());
        assertEquals(2, page.getTotalPages());
        assertEquals(3, page.getNumberOfElements());

        assertEquals("a23cf174-79c4-4295-8ef8-8090d59c9ab6", page.getContent().get(0).getId());

        String serialized = objectMapper.writeValueAsString(page);
        System.out.println(serialized);
        assertEquals(serializedPageImplStd.replaceAll("[\\s\n]", ""), serialized);
    }


    @Test
    void serialization_empty() throws JsonProcessingException {


        List<TypeRepresentation> content = new ArrayList<>();
        Pageable pageable = PageRequest.of(1, 5, Sort.by("name"));

        StandardApiPageImpl<TypeRepresentation> page = new StandardApiPageImpl<>(content, pageable, 5);

        assertEquals(0, page.getContent().size());
        assertEquals(1, page.getNumber());
        assertEquals(5, page.getSize());
        assertEquals(5, page.getTotalElements());
        assertEquals(1, page.getTotalPages());
        assertEquals(0, page.getNumberOfElements());

        String serialized = objectMapper.writeValueAsString(page);
        System.out.println(serialized);
        assertEquals(serializedPageImplEmpty.replaceAll("[\\s\n]", ""), serialized);
    }


    @Test
    void serialization_fullPage() throws JsonProcessingException {

        TypeRepresentation t1 = new TypeRepresentation("a23cf174-79c4-4295-8ef8-8090d59c9ab6", "auto", "auto");
        TypeRepresentation t2 = new TypeRepresentation("b56b2f07-b02f-4007-8bef-67be320b91a7", "detached-cades", "detached1");
        TypeRepresentation t3 = new TypeRepresentation("6183e313-d4f3-46f3-b6a7-00f5d43bef8d", "detached-xades", "xades-détaché");

        List<TypeRepresentation> content = Arrays.asList(t1, t2, t3);
        Pageable pageable = PageRequest.of(0, 3, Sort.by("name"));

        StandardApiPageImpl<TypeRepresentation> page = new StandardApiPageImpl<>(content, pageable, 3);

        assertEquals(3, page.getContent().size());
        assertEquals(0, page.getNumber());
        assertEquals(3, page.getSize());
        assertEquals(3, page.getTotalElements());
        assertEquals(1, page.getTotalPages());
        assertEquals(3, page.getNumberOfElements());

        String serialized = objectMapper.writeValueAsString(page);
        System.out.println(serialized);
        assertEquals(serializedPageImplFullPage.replaceAll("[\\s\n]", ""), serialized);
    }

}
