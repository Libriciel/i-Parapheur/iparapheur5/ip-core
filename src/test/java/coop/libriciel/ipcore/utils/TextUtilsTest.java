/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.utils;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.text.MessageFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

import static coop.libriciel.ipcore.model.database.MetadataType.*;
import static coop.libriciel.ipcore.utils.TextUtils.*;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.http.HttpStatus.INSUFFICIENT_STORAGE;


public class TextUtilsTest {


    public static final String DUMMY_SERVICE = "dummy";


    @Test
    void substituteForbiddenChars() {
        String name = """
                       line with
                      linebreak \
                      and also a \n see \r now?
                      """;

        String substitutedName = TextUtils.substituteForbiddenChar(name);

        assertNotEquals(name, substitutedName);
        assertEquals(" line with linebreak and also a   see   now? ", substitutedName);
    }


    @Test
    void firstNotEmpty() {
        assertNull(TextUtils.firstNotEmpty());
        assertNull(TextUtils.firstNotEmpty(null, null, null));
        assertEquals("", TextUtils.firstNotEmpty(null, "", null));
        assertEquals("plop", TextUtils.firstNotEmpty("", "plop", null));
        assertEquals("plop", TextUtils.firstNotEmpty("", null, "plop"));
    }


    @Test
    void prettyPrintDateTime() {
        assertNotNull(TextUtils.prettyPrintDateTime(new Date()));
    }


    @Test
    void getNewAttributeIfChanged() {

        assertEquals("1", TextUtils.getNewAttributeIfChanged(Pair.of("0", "0"), Pair.of("1", "0"), Pair::getLeft));
        assertEquals("1", TextUtils.getNewAttributeIfChanged(Pair.of("0", "0"), Pair.of("0", "1"), Pair::getRight));

        assertNull(TextUtils.getNewAttributeIfChanged(Pair.of("0", "0"), Pair.of("1", "0"), Pair::getRight));
        assertNull(TextUtils.getNewAttributeIfChanged(Pair.of("0", "0"), Pair.of("0", "1"), Pair::getLeft));
    }


    @Test
    void getNewAttributeIfChangedBoolean() {

        assertEquals("true", TextUtils.getNewAttributeIfChangedBoolean(Pair.of(false, false), Pair.of(true, false), Pair::getLeft));
        assertEquals("true", TextUtils.getNewAttributeIfChangedBoolean(Pair.of(false, false), Pair.of(false, true), Pair::getRight));

        assertNull(TextUtils.getNewAttributeIfChangedBoolean(Pair.of(false, false), Pair.of(true, false), Pair::getRight));
        assertNull(TextUtils.getNewAttributeIfChangedBoolean(Pair.of(false, false), Pair.of(false, true), Pair::getLeft));
    }


    @Test
    void isNewAttributeChanged() {

        assertTrue(TextUtils.hasAttributeChanged(Pair.of("0", "0"), Pair.of("1", "0"), Pair::getLeft));
        assertTrue(TextUtils.hasAttributeChanged(Pair.of("0", "0"), Pair.of("0", "1"), Pair::getRight));

        assertFalse(TextUtils.hasAttributeChanged(Pair.of("0", "0"), Pair.of("1", "0"), Pair::getRight));
        assertFalse(TextUtils.hasAttributeChanged(Pair.of("0", "0"), Pair.of("0", "1"), Pair::getLeft));
    }


    @Test
    void isNewAttributeChangedBoolean() {

        assertTrue(TextUtils.hasAttributeChangedBoolean(Pair.of(false, false), Pair.of(true, false), Pair::getLeft));
        assertTrue(TextUtils.hasAttributeChangedBoolean(Pair.of(false, false), Pair.of(false, true), Pair::getRight));

        assertFalse(TextUtils.hasAttributeChangedBoolean(Pair.of(false, false), Pair.of(true, false), Pair::getRight));
        assertFalse(TextUtils.hasAttributeChangedBoolean(Pair.of(false, false), Pair.of(false, true), Pair::getLeft));
    }


    @Test
    void escapeMessageFormatSpecialChars() {
        //noinspection ConstantValue
        assertNull(TextUtils.escapeMessageFormatSpecialChars(null));
        assertEquals("", TextUtils.escapeMessageFormatSpecialChars(""));
        assertEquals("test", TextUtils.escapeMessageFormatSpecialChars("test"));
        assertEquals("test''test", TextUtils.escapeMessageFormatSpecialChars("test'test"));
    }


    @Nested
    class CheckValues {


        @Test
        void withText() {

            TextUtils.checkValues(TEXT, null);
            TextUtils.checkValues(TEXT, emptyList());
            TextUtils.checkValues(TEXT, asList("Test", "1234", "\"\"\"\""));

            // Null check

            try {
                TextUtils.checkValues(TEXT, asList("Test", null, "Test"));
                fail();
            } catch (LocalizedStatusException e) {
                String expectedErrorMessage = ResourceBundle
                        .getBundle(MESSAGE_BUNDLE, Locale.getDefault())
                        .getString("message.metadata_incorrect_null_value");
                assertEquals(expectedErrorMessage, e.getReason());
            }
        }


        @Test
        void withDate() {

            TextUtils.checkValues(DATE, null);
            TextUtils.checkValues(DATE, emptyList());
            TextUtils.checkValues(DATE, asList("1977-04-22T06:00:00.000+0100", "2020-12-25T06:00:00.000+0100"));

            // Null check

            try {
                TextUtils.checkValues(DATE, asList("1977-04-22T06:00:00.000+0100", null));
                fail();
            } catch (LocalizedStatusException e) {
                String expectedErrorMessage = ResourceBundle
                        .getBundle(MESSAGE_BUNDLE, Locale.getDefault())
                        .getString("message.metadata_incorrect_null_value");
                assertEquals(expectedErrorMessage, e.getReason());
            }

            // Bad value check

            try {
                TextUtils.checkValues(DATE, asList("1977-04-22T06:00:00.000+0100", "potato"));
                fail();
            } catch (LocalizedStatusException e) {
                String expectedErrorMessageTemplate = ResourceBundle
                        .getBundle(MESSAGE_BUNDLE, Locale.getDefault())
                        .getString("message.metadata_incorrect_s_value_for_iso8601_date");

                expectedErrorMessageTemplate = TextUtils.escapeMessageFormatSpecialChars(expectedErrorMessageTemplate);
                assertEquals(MessageFormat.format(expectedErrorMessageTemplate, "potato"), e.getReason());
            }
        }


        @Test
        void withInteger() {

            TextUtils.checkValues(INTEGER, null);
            TextUtils.checkValues(INTEGER, emptyList());
            TextUtils.checkValues(INTEGER, asList("1", "-1234", "0"));

            ResourceBundle bundle = ResourceBundle.getBundle(MESSAGE_BUNDLE, Locale.getDefault());

            // Null check

            try {
                TextUtils.checkValues(INTEGER, asList("1", null, "2"));
                fail();
            } catch (LocalizedStatusException e) {
                String expectedErrorMessage = bundle.getString("message.metadata_incorrect_null_value");
                assertEquals(expectedErrorMessage, e.getReason());
            }

            // Bad value check

            try {
                TextUtils.checkValues(INTEGER, asList("1", "1.4"));
                fail();
            } catch (LocalizedStatusException e) {
                String expectedErrorMessageTemplate = bundle.getString("message.metadata_incorrect_s_value_for_integer");
                expectedErrorMessageTemplate = TextUtils.escapeMessageFormatSpecialChars(expectedErrorMessageTemplate);
                assertEquals(MessageFormat.format(expectedErrorMessageTemplate, "1.4"), e.getReason());
            }

            // Max size value check

            try {
                TextUtils.checkValues(INTEGER, IntStream.range(0, 999).mapToObj(String::valueOf).toList());
                fail();
            } catch (LocalizedStatusException e) {
                assertEquals(INSUFFICIENT_STORAGE, e.getStatusCode());
            }
        }


        @Test
        void withFloat() {

            TextUtils.checkValues(FLOAT, null);
            TextUtils.checkValues(FLOAT, emptyList());
            TextUtils.checkValues(FLOAT, asList("1", "-1234", "-16.65356"));

            // Null check

            try {
                TextUtils.checkValues(FLOAT, asList("1", null, "2"));
                fail();
            } catch (LocalizedStatusException e) {
                String expectedErrorMessage = ResourceBundle
                        .getBundle(MESSAGE_BUNDLE, Locale.getDefault())
                        .getString("message.metadata_incorrect_null_value");
                assertEquals(expectedErrorMessage, e.getReason());
            }

            // Bad value check

            try {
                TextUtils.checkValues(FLOAT, asList("1", "1.4", "potato"));
                fail();
            } catch (LocalizedStatusException e) {
                String expectedErrorMessageTemplate = ResourceBundle
                        .getBundle(MESSAGE_BUNDLE, Locale.getDefault())
                        .getString("message.metadata_incorrect_s_value_for_float");
                expectedErrorMessageTemplate = TextUtils.escapeMessageFormatSpecialChars(expectedErrorMessageTemplate);
                assertEquals(MessageFormat.format(expectedErrorMessageTemplate, "potato"), e.getReason());
            }
        }


        @Test
        void withBoolean() {

            TextUtils.checkValues(BOOLEAN, null);
            TextUtils.checkValues(BOOLEAN, emptyList());

            // Restricted values test

            try {
                TextUtils.checkValues(BOOLEAN, asList("true", "potato"));
                fail();
            } catch (LocalizedStatusException e) {
                String expectedErrorMessage = ResourceBundle
                        .getBundle(MESSAGE_BUNDLE, Locale.getDefault())
                        .getString("message.metadata_restricted_values_are_not_applicables_to_boolean_type");
                assertEquals(expectedErrorMessage, e.getReason());
            }
        }


        @Test
        void withUrl() {

            TextUtils.checkValues(URL, null);
            TextUtils.checkValues(URL, emptyList());
            TextUtils.checkValues(URL, asList("https://libriciel.fr", "http://www.gnu.org:80/"));

            ResourceBundle bundle = ResourceBundle.getBundle(MESSAGE_BUNDLE, Locale.getDefault());

            // Null check

            try {
                TextUtils.checkValues(URL, asList("https://libriciel.fr", null));
                fail();
            } catch (LocalizedStatusException e) {
                assertEquals(bundle.getString("message.metadata_incorrect_null_value"), e.getReason());
            }

            // Bad value check

            try {
                TextUtils.checkValues(URL, asList("https://libriciel.fr", ""));
                fail();
            } catch (LocalizedStatusException e) {
                assertEquals(bundle.getString("message.metadata_incorrect_empty_value"), e.getReason());
            }

            try {
                TextUtils.checkValues(URL, asList("https://libriciel.fr", "potato"));
                fail();
            } catch (LocalizedStatusException e) {
                String expectedErrorMessageTemplate = bundle.getString("message.metadata_incorrect_s_value_for_rfc3986_url");
                expectedErrorMessageTemplate = TextUtils.escapeMessageFormatSpecialChars(expectedErrorMessageTemplate);
                assertEquals(MessageFormat.format(expectedErrorMessageTemplate, "potato"), e.getReason());
            }
        }


    }


    @Test
    void parseDate() {
        Pattern datePattern = Pattern.compile(TIME_PERMISSION_INTERNAL_NAME_REGEX);

        String fullDate = "time_2022-01-10_to_2022-01-28";
        Matcher fullDateMatcher = datePattern.matcher(fullDate);
        assertTrue(fullDateMatcher.matches());
        assertEquals("2022-01-10", fullDateMatcher.group("start"));
        assertEquals("2022-01-28", fullDateMatcher.group("end"));

        String startDate = "time_2022-01-10_to_null";
        Matcher startDateMatcher = datePattern.matcher(startDate);
        assertTrue(startDateMatcher.matches());
        assertEquals("2022-01-10", startDateMatcher.group("start"));
        assertEquals("null", startDateMatcher.group("end"));

        String endDate = "time_null_to_2022-01-28";
        Matcher endDateMatcher = datePattern.matcher(endDate);
        assertTrue(endDateMatcher.matches());
        assertEquals("null", endDateMatcher.group("start"));
        assertEquals("2022-01-28", endDateMatcher.group("end"));
    }


    @Test
    void parseDesk() {
        String deskName = "tenant_67d576ee-fa08-4d51-a075-3097e0fee633_desk_918e7ea6-940d-4ee6-a4bc-6ee98580fbd3_owner";
        Matcher deskMatcher = DESK_OWNER_INTERNAL_NAME_PATTERN.matcher(deskName);
        assertTrue(deskMatcher.matches());
        assertEquals("67d576ee-fa08-4d51-a075-3097e0fee633", deskMatcher.group("tenantId"));
        assertEquals("918e7ea6-940d-4ee6-a4bc-6ee98580fbd3", deskMatcher.group("deskId"));
    }


    @Test
    void parseLocalizedDouble() {
        assertEquals(0F, TextUtils.parseLocalizedDouble("0"));

        assertEquals(0.5F, TextUtils.parseLocalizedDouble("0,5"));
        assertEquals(0.5F, TextUtils.parseLocalizedDouble("0.5"));
        assertEquals(0.5F, TextUtils.parseLocalizedDouble(",5"));
        assertEquals(0.5F, TextUtils.parseLocalizedDouble(".5"));

        assertEquals(0.5F, TextUtils.parseLocalizedDouble(" 0,5"));
        assertEquals(10000.5F, TextUtils.parseLocalizedDouble("10 000,5"));

        assertThrows(NumberFormatException.class, () -> TextUtils.parseLocalizedDouble("1.000,5"));
        assertThrows(NumberFormatException.class, () -> TextUtils.parseLocalizedDouble(""));
        assertThrows(NumberFormatException.class, () -> TextUtils.parseLocalizedDouble("nope"));
        assertThrows(NumberFormatException.class, () -> TextUtils.parseLocalizedDouble(null));
    }


    @Test
    void containsForbiddenChar() {
        Pattern pattern = Pattern.compile(NO_FORBIDDEN_CHARACTERS_REGEXP);

        String noForbiddenChar1 = "random normal string 1";
        String noForbiddenChar2 = "ééè¨fkk $ù* caractères chelou. :!,,*¤}{[])(&&é\"# haha";

        // directly with regex
        assertTrue(pattern.matcher(noForbiddenChar1).matches());
        assertTrue(pattern.matcher(noForbiddenChar2).matches());

        // with static method
        assertFalse(TextUtils.containsForbiddenChar(noForbiddenChar1));
        assertFalse(TextUtils.containsForbiddenChar(noForbiddenChar2));

        String containsLinebreak = "line \n break";
        String containsCarriageReturn = "carriage \r return";
        String containsUnbreakableSpace = "unbreakable \u00A0 space";

        // directly with regex
        assertFalse(pattern.matcher(containsLinebreak).matches());
        assertFalse(pattern.matcher(containsCarriageReturn).matches());
        assertFalse(pattern.matcher(containsUnbreakableSpace).matches());

        // with static method

        assertTrue(TextUtils.containsForbiddenChar(containsLinebreak));
        assertTrue(TextUtils.containsForbiddenChar(containsCarriageReturn));
        assertTrue(TextUtils.containsForbiddenChar(containsUnbreakableSpace));
    }


    @Test
    void isUuid() {

        assertFalse(TextUtils.isUuid(null));
        assertFalse(TextUtils.isUuid(""));
        assertFalse(TextUtils.isUuid("test"));
        assertFalse(TextUtils.isUuid("vvvvvvvv-wwww-xxxx-yyyy-zzzzzzzzzzzz"));
        assertFalse(TextUtils.isUuid("eaa1c5f5-cfa4-43aa-9bf6-14ef4759d3a6-9bf6"));

        assertTrue(TextUtils.isUuid(UUID.randomUUID().toString()));
    }


    @Test
    void iso8601DateTimeFormat() {
        long originTimeStamp = 1619712000000L;
        String serializedDate = TextUtils.serializeDate(new Date(originTimeStamp), ISO8601_DATE_TIME_FORMAT);
        Date deserializedDate = TextUtils.deserializeDate(serializedDate, ISO8601_DATE_TIME_FORMAT);
        assertNotNull(deserializedDate);
        assertEquals(originTimeStamp, deserializedDate.getTime());
    }


}
