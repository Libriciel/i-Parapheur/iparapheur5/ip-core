/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.utils;

import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.workflow.FolderSortBy;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.*;

import java.util.List;

import static coop.libriciel.ipcore.model.workflow.FolderSortBy.SUBTYPE_NAME;
import static coop.libriciel.ipcore.utils.PaginatedList.MAX_PAGE_SIZE;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.data.domain.Sort.Direction.DESC;


class RequestUtilsTest {


    @Test
    void getPageNumber() {
        assertEquals(0, RequestUtils.getPageNumber(Pageable.unpaged()));
        assertEquals(0, RequestUtils.getPageNumber(PageRequest.of(0, 100)));
        assertEquals(10, RequestUtils.getPageNumber(PageRequest.of(10, 100)));
    }


    @Test
    void getPageSize() {
        assertEquals(MAX_PAGE_SIZE, RequestUtils.getPageSize(Pageable.unpaged()));
        assertEquals(100, RequestUtils.getPageSize(PageRequest.of(20, 100)));
        assertThrows(IllegalArgumentException.class, () -> RequestUtils.getPageSize(PageRequest.of(10, 0)));
    }


    @Test
    void hasChangesBetween() {
        DeskRepresentation o1 = new DeskRepresentation("id01", "same_name");
        DeskRepresentation o2 = new DeskRepresentation("id02", "same_name");
        assertTrue(RequestUtils.hasChangesBetween(o1, o2, DeskRepresentation::getId));
        assertFalse(RequestUtils.hasChangesBetween(o1, o2, DeskRepresentation::getName));
    }


    @Test
    void toPaginatedList() {

        PaginatedList<String> paginated = new PaginatedList<>(List.of("1", "2", "3"), 3, 15,75);
        Page<String> result = RequestUtils.toPage(paginated, SUBTYPE_NAME, false);

        assertNotNull(result.getContent());
        assertEquals(3, result.getContent().size());

        assertNotNull(result.getPageable());
        assertEquals(SUBTYPE_NAME, RequestUtils.getFirstOderSort(result.getPageable(), FolderSortBy.class, null));
        assertFalse(RequestUtils.isFirstOrderAsc(result.getPageable()));
        assertEquals(3, result.getPageable().getPageNumber());
        assertEquals(15, result.getPageable().getPageSize());
        assertEquals(75, result.getTotalElements());
    }


    @Test
    void toPage() {

        Sort sort = Sort.by(DESC, String.valueOf(SUBTYPE_NAME));
        Pageable pageable = PageRequest.of(15, 3, sort);
        PageImpl<String> paginated = new PageImpl<>(List.of("1", "2", "3"), pageable, 75);

        PaginatedList<String> result = RequestUtils.toPaginatedList(paginated);

        assertNotNull(result.getData());
        assertEquals(3, result.getData().size());
        assertEquals(3, result.getPageSize());
        assertEquals(15, result.getPage());
        assertEquals(75, result.getTotal());
    }


}
