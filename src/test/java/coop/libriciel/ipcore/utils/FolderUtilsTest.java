/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.utils;

import coop.libriciel.ipcore.model.workflow.Task;
import org.junit.Assert;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.MissingResourceException;

import static coop.libriciel.ipcore.model.workflow.Action.*;
import static coop.libriciel.ipcore.model.workflow.Task.ExternalState.*;
import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertEquals;


class FolderUtilsTest {


    @Test
    void testMapTaskStatusBadArgument() {
        Assert.assertThrows(NullPointerException.class, () -> FolderUtils.mapTaskStatus(null));
        Assert.assertThrows(MissingResourceException.class, () -> FolderUtils.mapTaskStatus("toto"));
    }


    @Test
    void testMapTaskStatusOK() {
        assertEquals(ACTIVE, FolderUtils.mapTaskStatus("ACTIVE"));
        assertEquals(SIGNED, FolderUtils.mapTaskStatus("SIGNED"));
        assertEquals(REFUSED, FolderUtils.mapTaskStatus("REFUSED"));
        assertEquals(EXPIRED, FolderUtils.mapTaskStatus("EXPIRED"));
        assertEquals(CREATED, FolderUtils.mapTaskStatus("creation"));
        assertEquals(IN_REDACTION, FolderUtils.mapTaskStatus("modification"));
        assertEquals(DELETED, FolderUtils.mapTaskStatus("supression"));
        assertEquals(SENT, FolderUtils.mapTaskStatus("envoi"));
        assertEquals(SENT_AGAIN, FolderUtils.mapTaskStatus("renvoi"));
        assertEquals(RECEIVED_PARTIALLY, FolderUtils.mapTaskStatus("reception-partielle"));
        assertEquals(RECEIVED, FolderUtils.mapTaskStatus("reception"));
        assertEquals(NOT_RECEIVED, FolderUtils.mapTaskStatus("non-recu"));
        assertEquals(FORM, FolderUtils.mapTaskStatus("FORM"));
        assertEquals(ERROR, FolderUtils.mapTaskStatus("ERROR"));
    }


    @Nested
    class GetCurrentExternalSignatureMetadataIndex {


        @Test
        void empty() {
            List<Task> taskList = emptyList();
            assertEquals(1L, FolderUtils.getCurrentExternalSignatureIndex(taskList));
        }


        @Test
        void firstCase() {

            List<Task> taskList = List.of(
                    Task.builder().action(START).performedAction(START).build(),
                    Task.builder().action(EXTERNAL_SIGNATURE).performedAction(null).build()
            );

            assertEquals(1L, FolderUtils.getCurrentExternalSignatureIndex(taskList));
        }


        @Test
        void farCase() {

            List<Task> taskList = List.of(
                    Task.builder().action(START).performedAction(START).build(),
                    Task.builder().action(EXTERNAL_SIGNATURE).performedAction(EXTERNAL_SIGNATURE).build(),
                    Task.builder().action(EXTERNAL_SIGNATURE).performedAction(EXTERNAL_SIGNATURE).build(),
                    Task.builder().action(EXTERNAL_SIGNATURE).performedAction(EXTERNAL_SIGNATURE).build(),
                    Task.builder().action(EXTERNAL_SIGNATURE).performedAction(EXTERNAL_SIGNATURE).build(),
                    Task.builder().action(EXTERNAL_SIGNATURE).performedAction(null).build()
            );

            assertEquals(5L, FolderUtils.getCurrentExternalSignatureIndex(taskList));
        }


        @Test
        void withPollutedSecondOpinion() {

            List<Task> taskList = List.of(
                    Task.builder().action(START).performedAction(START).build(),
                    Task.builder().action(EXTERNAL_SIGNATURE).performedAction(EXTERNAL_SIGNATURE).build(),
                    Task.builder().action(EXTERNAL_SIGNATURE).performedAction(ASK_SECOND_OPINION).build(),
                    Task.builder().action(SECOND_OPINION).performedAction(SECOND_OPINION).build(),
                    Task.builder().action(EXTERNAL_SIGNATURE).performedAction(ASK_SECOND_OPINION).build(),
                    Task.builder().action(SECOND_OPINION).performedAction(SECOND_OPINION).build(),
                    Task.builder().action(EXTERNAL_SIGNATURE).performedAction(null).build()
            );

            assertEquals(2L, FolderUtils.getCurrentExternalSignatureIndex(taskList));
        }


    }


    @Nested
    class CountStepsWithAction {


        @Test
        void empty() {

            List<Task> taskList = List.of(
                    Task.builder().action(START).performedAction(START).build(),
                    Task.builder().action(EXTERNAL_SIGNATURE).performedAction(null).build()
            );

            assertEquals(0, FolderUtils.countStepsWithAction(taskList, VISA));
        }


        @Test
        void simpleCase() {

            List<Task> taskList = List.of(
                    Task.builder().action(START).performedAction(START).build(),
                    Task.builder().action(EXTERNAL_SIGNATURE).performedAction(null).build()
            );

            assertEquals(1, FolderUtils.countStepsWithAction(taskList, EXTERNAL_SIGNATURE));
        }


        @Test
        void withSecondOpinions() {

            List<Task> taskList = List.of(
                    Task.builder().action(START).performedAction(START).build(),
                    Task.builder().action(EXTERNAL_SIGNATURE).performedAction(EXTERNAL_SIGNATURE).build(),
                    Task.builder().action(EXTERNAL_SIGNATURE).performedAction(ASK_SECOND_OPINION).build(),
                    Task.builder().action(SECOND_OPINION).performedAction(SECOND_OPINION).build(),
                    Task.builder().action(EXTERNAL_SIGNATURE).performedAction(EXTERNAL_SIGNATURE).build(),
                    Task.builder().action(EXTERNAL_SIGNATURE).performedAction(null).build()
            );

            assertEquals(3, FolderUtils.countStepsWithAction(taskList, EXTERNAL_SIGNATURE));
        }


    }


//    @Test
//    void testSetStateLateAllValues() {
//        Date before = Date.from(Instant.now().minus(30, ChronoUnit.DAYS));
//        Date now = Date.from(Instant.now());
//        Date after = Date.from(Instant.now().plus(30, ChronoUnit.DAYS));
//
//        Assertions.assertEquals(PENDING, StateUtils.computeLateState(PENDING, null));
//        Assertions.assertEquals(LATE, StateUtils.computeLateState(PENDING, before));
//        Assertions.assertEquals(LATE, StateUtils.computeLateState(PENDING, now));
//        Assertions.assertEquals(PENDING, StateUtils.computeLateState(PENDING, after));
//
//        Assertions.assertEquals(CURRENT, StateUtils.computeLateState(CURRENT, null));
//        Assertions.assertEquals(LATE, StateUtils.computeLateState(CURRENT, before));
//        Assertions.assertEquals(LATE, StateUtils.computeLateState(CURRENT, now));
//        Assertions.assertEquals(CURRENT, StateUtils.computeLateState(CURRENT, after));
//    }


}
