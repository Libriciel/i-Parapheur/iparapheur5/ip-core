/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.securemail;

import com.fasterxml.jackson.core.JsonProcessingException;
import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.securemail.MailParams;
import coop.libriciel.ipcore.model.securemail.SecureMailDocument;
import coop.libriciel.ipcore.model.securemail.SecureMailEntity;
import coop.libriciel.ipcore.model.securemail.SecureMailServer;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import org.jetbrains.annotations.NotNull;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.util.List;

import static coop.libriciel.ipcore.utils.TextUtilsTest.DUMMY_SERVICE;
import static java.util.Collections.emptyList;


@Service(SecureMailServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = SecureMailServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = DUMMY_SERVICE)
public class DummySecureMailService implements SecureMailServiceInterface {


    @Override
    public @NotNull SecureMailServer createServer(@NotNull SecureMailServer secureMailServer) {
        return new SecureMailServer();
    }


    @Override
    public boolean testServer(@NotNull SecureMailServer secureMailServer) throws JsonProcessingException {
        return false;
    }


    @Override
    public @NotNull List<SecureMailServer> findAllServers(@NotNull String tenantId) {
        return emptyList();
    }


    @Override
    public @NotNull SecureMailServer findServer(
            @NotNull String tenantId,
            @NotNull Long id) {
        return new SecureMailServer();
    }


    @Override
    public void deleteServer(@NotNull String tenantId, @NotNull Long id) {}


    @Override
    public @NotNull SecureMailServer updateServerObject(
            @NotNull String tenantId,
            @NotNull SecureMailServer secureMailServer) {
        return new SecureMailServer();
    }


    @Override
    public @NotNull SecureMailServer updateServer(
            @NotNull String tenantId,
            @NotNull Long id,
            String url,
            String login,
            String password,
            String sslPublicKey,
            int entity,
            String type) {
        return new SecureMailServer();
    }


    @Override
    public SecureMailDocument findDocument(@NotNull String secureMailServerId, @NotNull String id) {
        return null;
    }


    @Override
    public List<SecureMailEntity> findAllPastellEntities(@NotNull SecureMailServer secureMailServer) throws JsonProcessingException {
        return null;
    }


    @Override
    public void rejectExternalProcedure(@NotNull Folder folder, @NotNull Task task) {

    }


    @Override
    public @NotNull SecureMailDocument createAndSendSecureMail(
            @NotNull Tenant tenant,
            @NotNull Folder folder,
            @NotNull MailParams mailParams,
            @NotNull List<Document> documents,
            Flux<DataBuffer> docketFlux) {
        return new SecureMailDocument();
    }


}
