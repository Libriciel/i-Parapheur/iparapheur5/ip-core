/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.database;

import coop.libriciel.ipcore.model.database.serverInfo.ServerInfo;
import coop.libriciel.ipcore.services.gdpr.GdprInformationDetailsDto;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;


class ServerInfoServiceTest {
    @Test
    void testSerializeAndDeserialize() {
        GdprInformationDetailsDto start = new GdprInformationDetailsDto();
        start.getDeclaringEntity().setName(RandomStringUtils.insecure().nextAlphanumeric(30));
        start.getDeclaringEntity().setAddress(RandomStringUtils.insecure().nextAlphanumeric(30));
        start.getDeclaringEntity().setSiret(RandomStringUtils.insecure().nextAlphanumeric(30));
        start.getDeclaringEntity().setApeCode(RandomStringUtils.insecure().nextAlphanumeric(30));
        start.getDeclaringEntity().setPhoneNumber(RandomStringUtils.insecure().nextAlphanumeric(30));
        start.getDeclaringEntity().setMail(RandomStringUtils.insecure().nextAlphanumeric(30));
        start.getDeclaringEntity().getResponsible().setName(RandomStringUtils.insecure().nextAlphanumeric(30));
        start.getDeclaringEntity().getResponsible().setTitle(RandomStringUtils.insecure().nextAlphanumeric(30));
        start.getDeclaringEntity().getDpo().setName(RandomStringUtils.insecure().nextAlphanumeric(30));
        start.getDeclaringEntity().getDpo().setMail(RandomStringUtils.insecure().nextAlphanumeric(30));
        start.getHostingEntity().setName(RandomStringUtils.insecure().nextAlphanumeric(30));
        start.getHostingEntity().setAddress(RandomStringUtils.insecure().nextAlphanumeric(30));
        start.getHostingEntity().setSiret(RandomStringUtils.insecure().nextAlphanumeric(30));
        start.setHostingEntityComments(RandomStringUtils.insecure().nextAlphanumeric(30));
        start.getMaintenanceEntity().setName(RandomStringUtils.insecure().nextAlphanumeric(30));
        start.getMaintenanceEntity().setAddress(RandomStringUtils.insecure().nextAlphanumeric(30));
        start.getMaintenanceEntity().setSiret(RandomStringUtils.insecure().nextAlphanumeric(30));

        Map<ServerInfo.Key, String> tempMap = ServerInfoService.serializeGdpr(start);
        GdprInformationDetailsDto end = ServerInfoService.deserializeGdpr(tempMap);
        assertEquals(start.getDeclaringEntity().getName(), end.getDeclaringEntity().getName());
        assertEquals(start.getDeclaringEntity().getAddress(), end.getDeclaringEntity().getAddress());
        assertEquals(start.getDeclaringEntity().getSiret(), end.getDeclaringEntity().getSiret());
        assertEquals(start.getDeclaringEntity().getApeCode(), end.getDeclaringEntity().getApeCode());
        assertEquals(start.getDeclaringEntity().getPhoneNumber(), end.getDeclaringEntity().getPhoneNumber());
        assertEquals(start.getDeclaringEntity().getMail(), end.getDeclaringEntity().getMail());
        assertEquals(start.getDeclaringEntity().getResponsible().getName(), end.getDeclaringEntity().getResponsible().getName());
        assertEquals(start.getDeclaringEntity().getResponsible().getTitle(), end.getDeclaringEntity().getResponsible().getTitle());
        assertEquals(start.getDeclaringEntity().getDpo().getName(), end.getDeclaringEntity().getDpo().getName());
        assertEquals(start.getDeclaringEntity().getDpo().getMail(), end.getDeclaringEntity().getDpo().getMail());
        assertEquals(start.getHostingEntity().getName(), end.getHostingEntity().getName());
        assertEquals(start.getHostingEntity().getAddress(), end.getHostingEntity().getAddress());
        assertEquals(start.getHostingEntity().getSiret(), end.getHostingEntity().getSiret());
        assertEquals(start.getHostingEntityComments(), end.getHostingEntityComments());
        assertEquals(start.getMaintenanceEntity().getName(), end.getMaintenanceEntity().getName());
        assertEquals(start.getMaintenanceEntity().getAddress(), end.getMaintenanceEntity().getAddress());
        assertEquals(start.getMaintenanceEntity().getSiret(), end.getMaintenanceEntity().getSiret());
    }
}
