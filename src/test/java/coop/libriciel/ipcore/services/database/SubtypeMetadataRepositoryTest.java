/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.database;

import coop.libriciel.ipcore.model.database.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.UUID;

import static coop.libriciel.ipcore.model.crypto.SignatureFormat.PES_V2;
import static coop.libriciel.ipcore.model.database.MetadataType.TEXT;
import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;


@SpringBootTest
@ExtendWith(SpringExtension.class)
@AutoConfigureTestDatabase(replace = NONE)
class SubtypeMetadataRepositoryTest {


    private @Autowired TypeRepository typeRepository;
    private @Autowired TenantRepository tenantRepository;
    private @Autowired SubtypeRepository subtypeRepository;
    private @Autowired MetadataRepository metadataRepository;
    private @Autowired SubtypeMetadataRepository subtypeMetadataRepository;


    @BeforeEach
    void setup() {

        this.cleanup();

        Tenant tenant = tenantRepository.save(
                Tenant.builder()
                        .id("tenantId")
                        .name("Tenant name")
                        .index(1L)
                        .contentId("content1234")
                        .statsId("stats1234")
                        .build()
        );

        Type type = typeRepository.save(Type.builder()
                .id("type_01").name("Type 01").description("First type")
                .subtypes(emptyList()).signatureFormat(PES_V2).tenant(tenant)
                .build());

        subtypeRepository.save(Subtype.builder().id("st01").tenant(tenant).parentType(type).build());
    }


    @AfterEach
    void cleanup() {
        subtypeMetadataRepository.deleteAll();
        subtypeRepository.deleteAll();
        typeRepository.deleteAll();
        metadataRepository.deleteAll();
        tenantRepository.deleteAll();
    }


    @Test
    void deleteCascade_fromSubtype() {

        Tenant tenant = tenantRepository.findAll().stream().findFirst().orElse(null);
        assertNotNull(tenant);
        Type type = typeRepository.findAll().stream().findFirst().orElse(null);
        assertNotNull(type);
        Subtype subtype = subtypeRepository.findAll().stream().findFirst().orElse(null);
        assertNotNull(subtype);

        Metadata metadata01 = metadataRepository.save(new Metadata(UUID.randomUUID().toString(), "m01", "m1", 1, TEXT, null, tenant, emptyList()));
        metadata01 = metadataRepository.findById(metadata01.getId()).orElse(null);
        assertNotNull(metadata01);
        Metadata metadata02 = metadataRepository.save(new Metadata(UUID.randomUUID().toString(), "m02", "m2", 2, TEXT, null, tenant, emptyList()));
        metadata02 = metadataRepository.findById(metadata02.getId()).orElse(null);
        assertNotNull(metadata02);

        subtypeMetadataRepository.save(new SubtypeMetadata(subtype, metadata01, null, true, true));
        subtypeMetadataRepository.save(new SubtypeMetadata(subtype, metadata02, null, true, false));

        subtype = subtypeRepository.findById(subtype.getId()).orElse(null);
        assertNotNull(subtype);
        metadata01 = metadataRepository.findById(metadata01.getId()).orElse(null);
        assertNotNull(metadata01);
        metadata02 = metadataRepository.findById(metadata02.getId()).orElse(null);
        assertNotNull(metadata02);

        // Delete cascade

        assertEquals(1, subtypeRepository.findAll().size());
        assertEquals(2, metadataRepository.findAll().size());
        assertEquals(2, subtypeMetadataRepository.findAll().size());
        subtypeRepository.delete(subtype);
        assertEquals(0, subtypeRepository.findAll().size());
        assertEquals(2, metadataRepository.findAll().size());
        assertEquals(0, subtypeMetadataRepository.findAll().size());
    }


    @Test
    void deleteCascade_fromMetadata() {

        Tenant tenant = tenantRepository.findAll().stream().findFirst().orElse(null);
        assertNotNull(tenant);
        Type type = typeRepository.findAll().stream().findFirst().orElse(null);
        assertNotNull(type);
        Subtype subtype = subtypeRepository.findAll().stream().findFirst().orElse(null);
        assertNotNull(subtype);

        Metadata metadata01 = metadataRepository.save(new Metadata(UUID.randomUUID().toString(), "m01", "m1", 1, TEXT, null, tenant, emptyList()));
        metadata01 = metadataRepository.findById(metadata01.getId()).orElse(null);
        assertNotNull(metadata01);
        Metadata metadata02 = metadataRepository.save(new Metadata(UUID.randomUUID().toString(), "m02", "m2", 2, TEXT, null, tenant, emptyList()));
        metadata02 = metadataRepository.findById(metadata02.getId()).orElse(null);
        assertNotNull(metadata02);

        subtypeMetadataRepository.save(new SubtypeMetadata(subtype, metadata01, null, true, true));
        subtypeMetadataRepository.save(new SubtypeMetadata(subtype, metadata02, null, true, false));

        subtype = subtypeRepository.findById(subtype.getId()).orElse(null);
        assertNotNull(subtype);
        metadata01 = metadataRepository.findById(metadata01.getId()).orElse(null);
        assertNotNull(metadata01);
        metadata02 = metadataRepository.findById(metadata02.getId()).orElse(null);
        assertNotNull(metadata02);

        // Delete cascade

        assertEquals(1, subtypeRepository.findAll().size());
        assertEquals(2, metadataRepository.findAll().size());
        assertEquals(2, subtypeMetadataRepository.findAll().size());
        metadataRepository.delete(metadata01);
        assertEquals(1, subtypeRepository.findAll().size());
        assertEquals(1, metadataRepository.findAll().size());
        assertEquals(1, subtypeMetadataRepository.findAll().size());
        metadataRepository.delete(metadata02);
        assertEquals(1, subtypeRepository.findAll().size());
        assertEquals(0, metadataRepository.findAll().size());
        assertEquals(0, subtypeMetadataRepository.findAll().size());
    }


}
