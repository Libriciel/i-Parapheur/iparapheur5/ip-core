/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.database;

import coop.libriciel.ipcore.model.database.ExternalSignatureConfig;
import coop.libriciel.ipcore.model.database.Metadata;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.database.Type;
import coop.libriciel.ipcore.model.pdfstamp.Layer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.UUID;

import static coop.libriciel.ipcore.model.database.MetadataType.TEXT;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;


@SpringBootTest
@ExtendWith(SpringExtension.class)
@AutoConfigureTestDatabase(replace = NONE)
class TenantRepositoryTest {


    private @Autowired TypeRepository typeRepository;
    private @Autowired TenantRepository tenantRepository;
    private @Autowired MetadataRepository metadataRepository;
    private @Autowired LayerRepository layerRepository;
    private @Autowired ExternalSignatureConfigRepository externalSignatureConfigRepository;


    @BeforeEach
    void init() {
        cleanup();

        tenantRepository.save(Tenant.builder().id("tenant01").name("Tenant name 01").index(1L).contentId("content1").statsId("stats1").build());
        tenantRepository.save(Tenant.builder().id("tenant02").name("Tenant name 02").index(2L).contentId("content2").statsId("stats2").build());
        tenantRepository.save(Tenant.builder().id("tenant03").name("Tenant name 03").index(3L).contentId("content3").statsId("stats3").build());
        tenantRepository.save(Tenant.builder().id("tenant04").name("Tenant name 99").index(4L).contentId("content4").statsId("stats4").build());
    }


    @AfterEach
    void cleanup() {
        typeRepository.deleteAll();
        tenantRepository.deleteAll();
        metadataRepository.deleteAll();
        layerRepository.deleteAll();
    }


    @Test
    void deleteCascade_types() {

        Tenant tenant = tenantRepository.findAll().stream().findFirst().orElse(null);
        assertNotNull(tenant);

        typeRepository.save(Type.builder().id("t11").name("Type 01").tenant(tenant).build());
        typeRepository.save(Type.builder().id("t12").name("Type 02").tenant(tenant).build());

        tenant = tenantRepository.findById(tenant.getId()).orElse(null);
        assertNotNull(tenant);

        // Delete cascade

        assertEquals(2, typeRepository.findAll().size());
        tenantRepository.delete(tenant);
        assertEquals(0, typeRepository.findAll().size());
    }


    @Test
    void deleteCascade_externalSignatureConfigs() {

        Tenant tenant = tenantRepository.findAll().stream().findFirst().orElse(null);
        assertNotNull(tenant);

        externalSignatureConfigRepository.save(new ExternalSignatureConfig("id11", null, null, null, null, null, null, null, emptySet(), emptyList(), tenant));
        externalSignatureConfigRepository.save(new ExternalSignatureConfig("id12", null, null, null, null, null, null, null, emptySet(), emptyList(), tenant));

        tenant = tenantRepository.findById(tenant.getId()).orElse(null);
        assertNotNull(tenant);

        // Delete cascade

        assertEquals(2, externalSignatureConfigRepository.findAll().size());
        tenantRepository.delete(tenant);
        assertEquals(0, externalSignatureConfigRepository.findAll().size());
    }


    @Test
    void deleteCascade_metadata() {

        Tenant tenant = tenantRepository.findAll().stream().findFirst().orElse(null);
        assertNotNull(tenant);

        metadataRepository.save(new Metadata(UUID.randomUUID().toString(), "id01", "name 01", 1, TEXT, emptyList(), tenant, null));
        metadataRepository.save(new Metadata(UUID.randomUUID().toString(), "id02", "name 02", 2, TEXT, emptyList(), tenant, null));

        tenant = tenantRepository.findById(tenant.getId()).orElse(null);
        assertNotNull(tenant);

        // Delete cascade

        assertEquals(2, metadataRepository.findAll().size());
        tenantRepository.delete(tenant);
        assertEquals(0, metadataRepository.findAll().size());
    }


    @Test
    void deleteCascade_layer() {

        Tenant tenant = tenantRepository.findAll().stream().findFirst().orElse(null);
        assertNotNull(tenant);

        layerRepository.save(new Layer(UUID.randomUUID().toString(), "name 01", tenant, emptyList(), emptyList()));
        layerRepository.save(new Layer(UUID.randomUUID().toString(), "name 02", tenant, emptyList(), emptyList()));

        tenant = tenantRepository.findById(tenant.getId()).orElse(null);
        assertNotNull(tenant);

        // Delete cascade

        assertEquals(2, layerRepository.findAll().size());
        tenantRepository.delete(tenant);
        assertEquals(0, layerRepository.findAll().size());
    }


    @Test
    void search_singleResult() {
        Page<Tenant> tenantListPage = tenantRepository.findAllWithSearchTerm("%99%", PageRequest.of(0, 50));
        assertNotNull(tenantListPage);
        assertEquals(1, tenantListPage.getTotalElements());
        assertEquals(1, tenantListPage.get().count());
        assertEquals("tenant04", tenantListPage.get().findFirst().map(Tenant::getId).orElse(null));
    }


    @Test
    void search_paginatedResult() {
        Page<Tenant> tenantListPage = tenantRepository.findAllWithSearchTerm("%name 0%", PageRequest.of(1, 1));
        assertNotNull(tenantListPage);
        assertEquals(3, tenantListPage.getTotalElements());
        assertEquals(1, tenantListPage.get().count());
        assertEquals("tenant02", tenantListPage.get().findFirst().map(Tenant::getId).orElse(null));
    }


    @Test
    void search_emptyResult() {
        Page<Tenant> tenantListPage = tenantRepository.findAllWithSearchTerm("%NOTHING_TO_FIND%", PageRequest.of(0, 50));
        assertNotNull(tenantListPage);
        assertEquals(0, tenantListPage.getTotalElements());
        assertEquals(0, tenantListPage.get().count());
    }


}
