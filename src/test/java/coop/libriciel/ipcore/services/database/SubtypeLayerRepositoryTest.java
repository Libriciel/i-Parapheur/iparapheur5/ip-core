/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.database;

import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.SubtypeLayer;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.database.Type;
import coop.libriciel.ipcore.model.pdfstamp.Layer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.UUID;

import static coop.libriciel.ipcore.model.crypto.SignatureFormat.PES_V2;
import static coop.libriciel.ipcore.model.database.SubtypeLayerAssociation.ANNEXE;
import static coop.libriciel.ipcore.model.database.SubtypeLayerAssociation.MAIN_DOCUMENT;
import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;


@SpringBootTest
@ExtendWith(SpringExtension.class)
@AutoConfigureTestDatabase(replace = NONE)
class SubtypeLayerRepositoryTest {


    private @Autowired TypeRepository typeRepository;
    private @Autowired TenantRepository tenantRepository;
    private @Autowired SubtypeRepository subtypeRepository;
    private @Autowired LayerRepository layerRepository;
    private @Autowired SubtypeLayerRepository subtypeLayerRepository;
    private @Autowired MetadataRepository metadataRepository;
    private @Autowired SubtypeMetadataRepository subtypeMetadataRepository;


    @BeforeEach
    void setup() {

        this.cleanup();

        Tenant tenant = tenantRepository.save(
                Tenant.builder()
                        .id("tenantId")
                        .name("Tenant name")
                        .index(1L)
                        .contentId("content1234")
                        .statsId("stats1234")
                        .build()
        );

        Type type = typeRepository.save(Type.builder()
                .id("type_01").name("Type 01").description("First type")
                .subtypes(emptyList()).signatureFormat(PES_V2).tenant(tenant)
                .build());

        subtypeRepository.save(Subtype.builder().id("st01").tenant(tenant).parentType(type).build());
    }


    @AfterEach
    void cleanup() {
        subtypeLayerRepository.deleteAll();
        subtypeRepository.deleteAll();
        typeRepository.deleteAll();
        layerRepository.deleteAll();
        tenantRepository.deleteAll();
        metadataRepository.deleteAll();
        subtypeMetadataRepository.deleteAll();
    }


    @Test
    void deleteCascade_fromSubtype() {

        Tenant tenant = tenantRepository.findAll().stream().findFirst().orElse(null);
        assertNotNull(tenant);
        Type type = typeRepository.findAll().stream().findFirst().orElse(null);
        assertNotNull(type);
        Subtype subtype = subtypeRepository.findAll().stream().findFirst().orElse(null);
        assertNotNull(subtype);

        Layer layer01 = layerRepository.save(new Layer(UUID.randomUUID().toString(), "l01", tenant, emptyList(), emptyList()));
        layer01 = layerRepository.findById(layer01.getId()).orElse(null);
        assertNotNull(layer01);
        Layer layer02 = layerRepository.save(new Layer(UUID.randomUUID().toString(), "l02", tenant, emptyList(), emptyList()));
        layer02 = layerRepository.findById(layer02.getId()).orElse(null);
        assertNotNull(layer02);

        subtypeLayerRepository.save(new SubtypeLayer(subtype, layer01, MAIN_DOCUMENT));
        subtypeLayerRepository.save(new SubtypeLayer(subtype, layer02, ANNEXE));

        subtype = subtypeRepository.findById(subtype.getId()).orElse(null);
        assertNotNull(subtype);
        layer01 = layerRepository.findById(layer01.getId()).orElse(null);
        assertNotNull(layer01);
        layer02 = layerRepository.findById(layer02.getId()).orElse(null);
        assertNotNull(layer02);

        // Delete cascade

        assertEquals(1, subtypeRepository.findAll().size());
        assertEquals(2, layerRepository.findAll().size());
        assertEquals(2, subtypeLayerRepository.findAll().size());
        subtypeRepository.delete(subtype);
        assertEquals(0, subtypeRepository.findAll().size());
        assertEquals(2, layerRepository.findAll().size());
        assertEquals(0, subtypeLayerRepository.findAll().size());
    }


    @Test
    void deleteCascade_fromLayer() {

        Tenant tenant = tenantRepository.findAll().stream().findFirst().orElse(null);
        assertNotNull(tenant);
        Type type = typeRepository.findAll().stream().findFirst().orElse(null);
        assertNotNull(type);
        Subtype subtype = subtypeRepository.findAll().stream().findFirst().orElse(null);
        assertNotNull(subtype);

        Layer layer01 = layerRepository.save(new Layer(UUID.randomUUID().toString(), "l01", tenant, emptyList(), emptyList()));
        layer01 = layerRepository.findById(layer01.getId()).orElse(null);
        assertNotNull(layer01);
        Layer layer02 = layerRepository.save(new Layer(UUID.randomUUID().toString(), "l02", tenant, emptyList(), emptyList()));
        layer02 = layerRepository.findById(layer02.getId()).orElse(null);
        assertNotNull(layer02);

        subtypeLayerRepository.save(new SubtypeLayer(subtype, layer01, MAIN_DOCUMENT));
        subtypeLayerRepository.save(new SubtypeLayer(subtype, layer02, ANNEXE));

        subtype = subtypeRepository.findById(subtype.getId()).orElse(null);
        assertNotNull(subtype);
        layer01 = layerRepository.findById(layer01.getId()).orElse(null);
        assertNotNull(layer01);
        layer02 = layerRepository.findById(layer02.getId()).orElse(null);
        assertNotNull(layer02);

        // Delete cascade

        assertEquals(1, subtypeRepository.findAll().size());
        assertEquals(2, layerRepository.findAll().size());
        assertEquals(2, subtypeLayerRepository.findAll().size());
        layerRepository.delete(layer01);
        assertEquals(1, subtypeRepository.findAll().size());
        assertEquals(1, layerRepository.findAll().size());
        assertEquals(1, subtypeLayerRepository.findAll().size());
        layerRepository.delete(layer02);
        assertEquals(1, subtypeRepository.findAll().size());
        assertEquals(0, layerRepository.findAll().size());
        assertEquals(0, subtypeLayerRepository.findAll().size());
    }


}
