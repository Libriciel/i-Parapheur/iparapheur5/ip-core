/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.database;

import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.database.MetadataRepresentation;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.workflow.DelegationRule;
import coop.libriciel.ipcore.model.workflow.requests.ColumnedTaskListRequest;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Set;

import static coop.libriciel.ipcore.model.database.MetadataType.TEXT;
import static coop.libriciel.ipcore.model.database.MetadataType.URL;
import static coop.libriciel.ipcore.model.workflow.FolderSortBy.Constants.*;
import static coop.libriciel.ipcore.model.workflow.FolderSortBy.TYPE_NAME;
import static coop.libriciel.ipcore.model.workflow.State.*;
import static coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface.META_SUBTYPE_ID;
import static coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface.META_TYPE_ID;
import static java.util.Collections.*;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.jooq.conf.ParamType.INLINED;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.data.domain.Sort.Direction.DESC;


class PostgresServiceTest {


    @Test
    void buildTenantIndexAvailableRequest() {
        String expected =
                """
                SELECT
                  possibleIndex
                FROM generate_series(0, 10000) AS possibleIndex
                WHERE
                  possibleIndex NOT IN (
                    SELECT index FROM tenant
                  )
                FETCH NEXT 1 ROWS ONLY
                """.indent(1);

        String result = PostgresService
                .buildTenantIndexAvailableRequest()
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildUserDataGroupIndexAvailableRequest() {
        String expected =
                """
                SELECT
                  possibleIndex
                FROM generate_series(0, 50000) AS possibleIndex
                WHERE
                  possibleIndex NOT IN (
                    SELECT CAST(value AS int)
                    FROM user_attribute
                    WHERE name = 'i_Parapheur_internal_content_group_index'
                  )
                FETCH NEXT 1 ROWS ONLY
                """.indent(1);

        String result = PostgresService
                .buildUserDataGroupIndexAvailableRequest()
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildListTenantsForUser() {

        String expected =
                """
                SELECT
                  t.id AS id,
                  t.name AS name,
                  urmu.role_id AS user_link,
                  urmu.user_id AS user_id
                FROM
                  tenant AS t
                JOIN
                  keycloak_role AS kru
                  ON
                    kru.name = ('tenant_' || t.id)
                LEFT OUTER JOIN
                  user_role_mapping AS urmu
                  ON (
                    urmu.role_id = kru.id
                    AND urmu.user_id = 'user1234'
                  )
                WHERE (
                  upper(t.name) LIKE upper('%search%')
                  AND urmu.role_id IS NULL
                ) ORDER BY t.id DESC
                OFFSET 150 ROWS
                FETCH NEXT 50 ROWS ONLY
                """.indent(1);

        String result = PostgresService
                .buildListTenantsForUserRequest("user1234", PageRequest.of(3, 50, DESC, Tenant.COLUMN_ID), "search", true)
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildCountTenantsForUser() {

        String expected =
                """
                SELECT
                  count(*)
                FROM
                  tenant AS t
                JOIN
                  keycloak_role AS kru
                  ON
                    kru.name = ('tenant_' || t.id)
                LEFT OUTER JOIN
                  user_role_mapping AS urmu
                  ON (
                    urmu.role_id = kru.id
                    AND urmu.user_id = 'user1234'
                  )
                WHERE (
                  t.name LIKE '%search%'
                  AND urmu.role_id IS NOT NULL
                )
                """.indent(1);

        String result = PostgresService
                .buildCountTenantsForUser("user1234", "search", false)
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildListAdminUsersIdForTenant() {

        String expected =
                """
                SELECT
                  urm.user_id AS user_id,
                  krta.name
                FROM user_role_mapping AS urm
                JOIN
                  keycloak_role AS krta
                  ON (
                    krta.name = 'tenant_tenant01_admin'
                    AND urm.role_id = krta.id
                  )
                UNION ALL
                SELECT
                  urma.user_id AS user_id,
                  kru.name
                FROM user_role_mapping AS urma
                JOIN
                  keycloak_role AS krsa
                  ON (
                    krsa.name = 'admin'
                    AND urma.role_id = krsa.id
                  )
                JOIN user_role_mapping AS urmta
                ON urma.user_id = urmta.user_id
                JOIN
                  keycloak_role AS kru
                  ON (
                    kru.name = 'tenant_tenant01'
                    AND urmta.role_id = kru.id
                  )
                """.indent(1);

        String result = PostgresService
                .buildListAdminUsersIdForTenant("tenant01")
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildTypologyEntityListSqlRequest() {

        String expected =
                """
                SELECT
                    t.id AS id,
                    t.name AS name,
                    t.description AS description,
                    t.name AS order1_name,
                    NULL AS order2_name,
                    NULL AS parent_id,
                    count(st.id) AS children_count
                FROM type AS t
                LEFT OUTER JOIN subtype AS st
                  ON st.parent_id = t.id
                WHERE (
                  t.tenant_id = 'tenant01'
                  AND (
                    t.name ILIKE '%search_word%'
                    OR st.name ILIKE '%search_word%'
                  )
                )
                GROUP BY t.id, t.name, t.description, parent_id, order1_name, order2_name
                UNION ALL
                SELECT
                    st.id AS id,
                    st.name AS name,
                    st.description AS description,
                    t.name AS order1_name,
                    st.name AS order2_name,
                    t.id AS parent_id,
                    0 AS children_count
                FROM subtype AS st
                JOIN type AS t
                  ON st.parent_id = t.id
                WHERE (
                  st.tenant_id = 'tenant01'
                  AND t.id NOT IN ('type_15870')
                  AND st.name ILIKE '%search_word%'
                )
                ORDER BY order1_name ASC, order2_name ASC NULLS FIRST
                OFFSET 150 ROWS
                FETCH NEXT 50 ROWS ONLY
                """.indent(1);

        Pageable pageable = PageRequest.of(3, 50);
        String result = PostgresService
                .buildTypologyEntityListSqlRequest("tenant01", pageable, false, Set.of("type_15870"), "search_word")
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildTypologyEntityListSqlRequest_collapseAll() {

        String expected =
                """
                SELECT
                    t.id AS id,
                    t.name AS name,
                    t.description AS description,
                    t.name AS order1_name,
                    NULL AS order2_name,
                    NULL AS parent_id,
                    count(st.id) AS children_count
                FROM type AS t
                LEFT OUTER JOIN subtype AS st
                  ON st.parent_id = t.id
                WHERE (   t.tenant_id = 'tenant01'
                  AND 1 = 1
                )
                GROUP BY t.id, t.name, t.description, parent_id, order1_name, order2_name
                UNION ALL
                SELECT
                    st.id AS id,
                    st.name AS name,
                    st.description AS description,
                    t.name AS order1_name,
                    st.name AS order2_name,
                    t.id AS parent_id,
                    0 AS children_count
                FROM subtype AS st
                JOIN type AS t
                  ON st.parent_id = t.id
                WHERE (
                  st.tenant_id = 'tenant01'
                  AND t.id IN ('type_15870')
                  AND 1 = 1
                )
                ORDER BY order1_name ASC, order2_name ASC NULLS FIRST
                OFFSET 150 ROWS
                FETCH NEXT 50 ROWS ONLY
                """.indent(1);

        Pageable pageable = PageRequest.of(3, 50);
        String result = PostgresService
                .buildTypologyEntityListSqlRequest("tenant01", pageable, true, Set.of("type_15870"), null)
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildTypologyEntityTotalSqlRequest() {

        String expected =
                """
                SELECT count(*)
                FROM (
                  SELECT  DISTINCT  t.id AS id
                  FROM type AS t
                  LEFT OUTER JOIN subtype AS st
                    ON st.parent_id = t.id
                  WHERE (
                    t.tenant_id = 'tenant01'
                    AND 1 = 1
                  )
                  UNION ALL
                  SELECT
                    st.id AS id
                  FROM subtype AS st
                  JOIN type AS t
                    ON st.parent_id = t.id
                  WHERE (
                    st.tenant_id = 'tenant01'
                    AND t.id NOT IN ('type_15870')
                    AND 1 = 1
                  )
                )
                """;

        String result = PostgresService
                .buildTypologyEntityTotalSqlRequest("tenant01", false, Set.of("type_15870"), null)
                .getSQL(INLINED)
                .replaceAll(" AS alias_.*?$", "");

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildTypologyEntityTotalSqlRequest_collapseAll() {

        String expected =
                """
                SELECT count(*)
                FROM (
                  SELECT DISTINCT
                    t.id AS id
                  FROM type AS t
                  LEFT OUTER JOIN subtype AS st
                    ON st.parent_id = t.id
                  WHERE (
                    t.tenant_id = 'tenant01'
                    AND 1 = 1
                  )
                  UNION ALL
                  SELECT
                    st.id AS id
                  FROM subtype AS st
                  JOIN type AS t
                    ON st.parent_id = t.id
                  WHERE (
                    st.tenant_id = 'tenant01'
                    AND t.id IN ('type_15870')
                    AND 1 = 1
                  )
                )
                """;

        String result = PostgresService
                .buildTypologyEntityTotalSqlRequest("tenant01", true, Set.of("type_15870"), null)
                .getSQL(INLINED)
                .replaceAll(" AS alias_.*?$", "");

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildTasksSqlRequest() {
        String expected =
                """
                SELECT
                  ACT_RU_EXECUTION.ROOT_PROC_INST_ID_ AS FOLDER_ID,
                  act_ru_exec_root.NAME_ AS FOLDER_NAME,
                  ACT_RU_IDENTITYLINK.group_id_ AS CURRENT_DESK_ID,
                  ACT_RU_TASK.id_ AS TASK_ID,
                  ACT_RU_TASK.NAME_ AS task_name,
                  ACT_RU_TASK.due_date_ AS LATE_DATE,
                  ACT_RU_EXECUTION.start_time_ AS START_DATE,
                  NULL AS end_date,
                  array_agg(DISTINCT ARRAY[
                    act_ru_variable_md.NAME_,
                    act_ru_variable_md.text_
                  ]) AS variables,
                  typology.name AS SORT_BY
                FROM ACT_RU_IDENTITYLINK
                  JOIN ACT_RU_TASK
                    ON ACT_RU_IDENTITYLINK.task_id_ = ACT_RU_TASK.id_
                  JOIN ACT_RU_EXECUTION
                    ON ACT_RU_EXECUTION.id_ = ACT_RU_TASK.proc_inst_id_
                  JOIN ACT_RU_EXECUTION AS act_ru_exec_root
                    ON act_ru_exec_root.id_ = ACT_RU_EXECUTION.ROOT_PROC_INST_ID_
                  JOIN act_ru_variable AS act_ru_variable_md
                    ON ACT_RU_EXECUTION.ROOT_PROC_INST_ID_ = act_ru_variable_md.proc_inst_id_
                  LEFT OUTER JOIN act_ru_variable AS act_ru_variables_sb
                    ON (
                      ACT_RU_EXECUTION.ROOT_PROC_INST_ID_ = act_ru_variables_sb.proc_inst_id_
                      AND act_ru_variables_sb.name_ = 'i_Parapheur_internal_type_id'
                    )
                  LEFT OUTER JOIN type AS typology
                    ON act_ru_variables_sb.text_ = typology.id
                  LEFT OUTER JOIN act_ru_variable AS act_ru_variables_db
                    ON ACT_RU_EXECUTION.ROOT_PROC_INST_ID_ = act_ru_variables_db.proc_inst_id_
                WHERE (
                  ACT_RU_IDENTITYLINK.type_ = 'candidate'
                  AND ACT_RU_IDENTITYLINK.group_id_ IN ('27551ec8-38b3-4cb9-9d57-6ccb615a3f26')
                  AND ACT_RU_TASK.NAME_ <> 'undo'
                  AND ACT_RU_TASK.NAME_ <> 'workflow_internal_draft'
                  AND ACT_RU_TASK.NAME_ <> 'workflow_internal_rejected'  AND ACT_RU_TASK.NAME_ <> 'workflow_internal_finished'
                )
                GROUP BY
                  FOLDER_ID,
                  FOLDER_NAME,
                  CURRENT_DESK_ID,
                  TASK_ID,
                  task_name,
                  LATE_DATE,
                  START_DATE,
                  SORT_BY
                ORDER BY
                  SORT_BY ASC,
                  START_DATE ASC
                OFFSET 50 ROWS
                FETCH NEXT 50 ROWS ONLY
                """;

        Desk testDesk = Desk.builder().id("27551ec8-38b3-4cb9-9d57-6ccb615a3f26").build();
        String result = PostgresService
                .buildTasksSqlRequest(testDesk, PENDING, TYPE_NAME, true, 1, 50)
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Nested
    class BuildColumnedTaskListRequest {


        @Test
        void pending() {

            String expected =
                    """
                    SELECT
                      act_ru_execution.ROOT_PROC_INST_ID_ AS FOLDER_ID,
                      act_ru_exec_root.NAME_ AS FOLDER_NAME,
                      act_hi_identitylink.GROUP_ID_ AS CURRENT_DESK_ID,
                      act_ru_task.ID_ AS TASK_ID,
                      act_ru_task.NAME_ AS task_name,
                      act_ru_task.DUE_DATE_ AS LATE_DATE,
                      act_ru_exec_root.START_TIME_ AS CREATION_DATE,
                      NULL AS end_date,
                      task_start.end_time_ AS START_DATE,
                      task_start.assignee_ AS EMITTER_USER_ID,
                      start_identity.GROUP_ID_ AS EMITTER_DESK_ID,
                      (task_read.id_ IS NOT NULL) AS IS_READ,
                      type_metadata.text_ AS TYPE_ID,
                      subtype_metadata.text_ AS SUBTYPE_ID,
                      NULL AS READ_TASK_CURRENT_DESK_ID,
                      ((start_user.first_name || ' ') || start_user.last_name) AS SORT_BY,
                      metadata_service.text_ AS metadata_service,
                      metadata_url.text_ AS metadata_url
                    FROM act_hi_identitylink
                    JOIN act_ru_task
                      ON (
                        act_hi_identitylink.TASK_ID_ = act_ru_task.ID_
                        AND act_hi_identitylink.TYPE_ = 'candidate'
                      )
                    JOIN act_ru_execution
                      ON act_ru_execution.ID_ = act_ru_task.PROC_INST_ID_
                    JOIN act_ru_execution AS act_ru_exec_root
                      ON (
                        act_ru_exec_root.ID_ = act_ru_execution.ROOT_PROC_INST_ID_
                        AND act_ru_exec_root.tenant_id_ = '6c53cbb2-37f5-452a-8620-d771ad05297d'
                      )
                    JOIN act_hi_taskinst AS task_start
                      ON (
                        task_start.name_ = 'main_start'
                        AND task_start.tenant_id_ = '6c53cbb2-37f5-452a-8620-d771ad05297d'
                      )
                    JOIN act_hi_procinst AS task_start_proc
                      ON (
                        task_start_proc.ID_ = task_start.PROC_INST_ID_
                        AND task_start_proc.business_key_ = act_ru_exec_root.business_key_
                      )
                    JOIN act_hi_identitylink AS start_identity
                      ON (
                        start_identity.TASK_ID_ = task_start.ID_
                        AND start_identity.TYPE_ = 'candidate'
                      )
                    LEFT OUTER JOIN act_hi_taskinst AS task_read
                      ON (
                        task_read.PROC_INST_ID_ = act_ru_task.PROC_INST_ID_
                        AND task_read.NAME_ = 'read'
                        AND task_read.end_time_ IS NOT NULL
                        AND task_read.assignee_ = 'e9d5e78e-c63f-4c83-aa92-198769457e21'
                      )
                    LEFT OUTER JOIN ACT_RU_VARIABLE AS type_metadata
                      ON (
                        act_ru_execution.ROOT_PROC_INST_ID_ = type_metadata.PROC_INST_ID_
                        AND type_metadata.NAME_ = 'i_Parapheur_internal_type_id'
                      )
                    LEFT OUTER JOIN ACT_RU_VARIABLE AS subtype_metadata
                      ON (
                        act_ru_execution.ROOT_PROC_INST_ID_ = subtype_metadata.PROC_INST_ID_
                        AND subtype_metadata.NAME_ = 'i_Parapheur_internal_subtype_id'
                      )
                    LEFT OUTER JOIN user_entity AS start_user
                      ON (
                        start_user.id = task_start.assignee_
                        AND start_user.realm_id = 'i-Parapheur'
                      )
                    LEFT OUTER JOIN ACT_RU_VARIABLE AS metadata_service
                      ON (
                        act_ru_execution.ROOT_PROC_INST_ID_ = metadata_service.PROC_INST_ID_
                        AND metadata_service.name_ = 'service'
                      )
                    LEFT OUTER JOIN ACT_RU_VARIABLE AS metadata_url
                      ON (
                        act_ru_execution.ROOT_PROC_INST_ID_ = metadata_url.PROC_INST_ID_
                        AND metadata_url.name_ = 'url'
                      )
                    WHERE (
                      act_ru_task.NAME_ NOT IN (
                        'workflow_internal_draft', 'main_start'
                      )
                      AND act_ru_task.NAME_ NOT IN (
                        'workflow_internal_rejected', 'main_delete'
                      )
                      AND act_ru_task.NAME_ NOT IN (
                        'workflow_internal_finished', 'main_archive'
                      )
                      AND act_ru_task.NAME_ <> 'undo'
                      AND act_ru_task.NAME_ <> 'read'
                      AND act_hi_identitylink.GROUP_ID_ IN ('21cc32a7-95fd-4e45-a935-90e11e1136f2')
                    )
                    GROUP BY
                      FOLDER_ID,
                      FOLDER_NAME,
                      CURRENT_DESK_ID,
                      TASK_ID,
                      task_name,
                      LATE_DATE,
                      CREATION_DATE,
                      IS_READ,
                      SORT_BY,
                      START_DATE,
                      EMITTER_USER_ID,
                      EMITTER_DESK_ID,
                      TYPE_ID,
                      SUBTYPE_ID,
                      READ_TASK_CURRENT_DESK_ID,
                      metadata_service,
                      metadata_url
                    ORDER BY
                      SORT_BY ASC NULLS FIRST,
                      CREATION_DATE ASC
                    OFFSET 150 ROWS
                    FETCH NEXT 30 ROWS ONLY
                    """.indent(1);

            Pageable pageable = PageRequest.of(5, 30, Sort.by(EMITTER_USER_NAME_VALUE));
            ColumnedTaskListRequest instanceColumnRequest = new ColumnedTaskListRequest(
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    List.of("url", "service"),
                    emptyMap(),
                    PENDING
            );
            List<MetadataRepresentation> includedMetadata = List.of(
                    new MetadataRepresentation("url", EMPTY, "url", null, URL),
                    new MetadataRepresentation("service", EMPTY, "service", null, TEXT)
            );

            String result = PostgresService
                    .buildColumnedTaskListRequest(
                            "6c53cbb2-37f5-452a-8620-d771ad05297d",
                            singletonList("21cc32a7-95fd-4e45-a935-90e11e1136f2"),
                            emptyList(),
                            "e9d5e78e-c63f-4c83-aa92-198769457e21",
                            instanceColumnRequest,
                            includedMetadata,
                            pageable
                    )
                    .getSQL(INLINED);

            assertEquals(
                    expected.trim().replaceAll("\\s+", " "),
                    result.trim().replaceAll("\\s+", " ")
            );
        }


        @Test
        void undo() {

            String expected =
                    """
                    SELECT
                      act_ru_execution.ROOT_PROC_INST_ID_ AS FOLDER_ID,
                      act_ru_exec_root.NAME_ AS FOLDER_NAME,
                      act_hi_identitylink.GROUP_ID_ AS CURRENT_DESK_ID,
                      act_ru_task.ID_ AS TASK_ID,
                      act_ru_task.NAME_ AS task_name,
                      act_ru_task.DUE_DATE_ AS LATE_DATE,
                      act_ru_exec_root.START_TIME_ AS CREATION_DATE,
                      NULL AS end_date,
                      task_start.end_time_ AS START_DATE,
                      task_start.assignee_ AS EMITTER_USER_ID,
                      start_identity.GROUP_ID_ AS EMITTER_DESK_ID,
                      (task_read.id_ IS NOT NULL) AS IS_READ,
                      type_metadata.text_ AS TYPE_ID,
                      subtype_metadata.text_ AS SUBTYPE_ID,
                      act_hi_identitylink_read_desk.group_id_ AS READ_TASK_CURRENT_DESK_ID,
                      upper(current_desk.description) AS SORT_BY,
                      metadata_service.text_ AS metadata_service,
                      metadata_url.text_ AS metadata_url
                    FROM act_hi_identitylink
                    JOIN act_ru_task
                    ON (
                      act_hi_identitylink.TASK_ID_ = act_ru_task.ID_
                      AND act_hi_identitylink.TYPE_ = 'candidate'
                    )
                    JOIN act_ru_task AS act_ru_task_read_desk
                    ON (
                      act_ru_task.PROC_INST_ID_ = act_ru_task_read_desk.PROC_INST_ID_
                      AND act_ru_task_read_desk.NAME_ = 'read'
                    )
                    JOIN act_hi_identitylink AS act_hi_identitylink_read_desk
                    ON (
                      act_hi_identitylink_read_desk.task_id_ = act_ru_task_read_desk.ID_
                      AND act_hi_identitylink_read_desk.type_ = 'candidate'
                    )
                    JOIN act_ru_execution
                    ON act_ru_execution.ID_ = act_ru_task.PROC_INST_ID_
                    JOIN act_ru_execution AS act_ru_exec_root
                    ON (
                      act_ru_exec_root.ID_ = act_ru_execution.ROOT_PROC_INST_ID_
                      AND act_ru_exec_root.tenant_id_ = '29f9b247-431d-42aa-b00b-7df7507e83f7'
                    )
                    JOIN act_hi_taskinst AS task_start
                    ON (
                      task_start.name_ = 'main_start'
                      AND task_start.tenant_id_ = '29f9b247-431d-42aa-b00b-7df7507e83f7'
                    )
                    JOIN act_hi_procinst AS task_start_proc
                    ON (
                      task_start_proc.ID_ = task_start.PROC_INST_ID_
                      AND task_start_proc.business_key_ = act_ru_exec_root.business_key_
                    )
                    JOIN act_hi_identitylink AS start_identity
                    ON (
                      start_identity.TASK_ID_ = task_start.ID_
                      AND start_identity.TYPE_ = 'candidate'
                    )
                    LEFT OUTER JOIN act_hi_taskinst AS task_read
                    ON (
                      task_read.PROC_INST_ID_ = act_ru_task.PROC_INST_ID_
                      AND task_read.NAME_ = 'read'
                      AND task_read.end_time_ IS NOT NULL
                      AND task_read.assignee_ = 'e9d5e78e-c63f-4c83-aa92-198769457e21'
                    )
                    LEFT OUTER JOIN ACT_RU_VARIABLE AS type_metadata
                    ON (
                      act_ru_execution.ROOT_PROC_INST_ID_ = type_metadata.PROC_INST_ID_
                      AND type_metadata.NAME_ = 'i_Parapheur_internal_type_id'
                    )
                    LEFT OUTER JOIN ACT_RU_VARIABLE AS subtype_metadata
                    ON (
                      act_ru_execution.ROOT_PROC_INST_ID_ = subtype_metadata.PROC_INST_ID_
                      AND subtype_metadata.NAME_ = 'i_Parapheur_internal_subtype_id'
                    )
                    JOIN keycloak_role AS current_desk
                    ON (
                      act_hi_identitylink.GROUP_ID_ = current_desk.id
                      AND current_desk.realm_id = 'i-Parapheur'
                    )
                    LEFT OUTER JOIN ACT_RU_VARIABLE AS metadata_service
                    ON (
                      act_ru_execution.ROOT_PROC_INST_ID_ = metadata_service.PROC_INST_ID_
                      AND metadata_service.name_ = 'service'
                    )
                    LEFT OUTER JOIN ACT_RU_VARIABLE AS metadata_url
                    ON (
                      act_ru_execution.ROOT_PROC_INST_ID_ = metadata_url.PROC_INST_ID_
                      AND metadata_url.name_ = 'url'
                    )
                    WHERE (
                      act_ru_task.NAME_ = 'undo'
                      AND act_ru_task.NAME_ <> 'read'
                      AND act_hi_identitylink.GROUP_ID_ IN ('f641e6e1-fa60-42fc-88bb-4c4f8e12b97a')
                    )
                    GROUP BY
                      FOLDER_ID,
                      FOLDER_NAME,
                      CURRENT_DESK_ID,
                      TASK_ID,
                      task_name,
                      LATE_DATE,
                      CREATION_DATE,
                      IS_READ,
                      SORT_BY,
                      START_DATE,
                      EMITTER_USER_ID,
                      EMITTER_DESK_ID,
                      TYPE_ID,
                      SUBTYPE_ID,
                      READ_TASK_CURRENT_DESK_ID,
                      metadata_service,
                      metadata_url
                    ORDER BY
                      SORT_BY ASC NULLS FIRST,
                      CREATION_DATE ASC
                    OFFSET 150 ROWS
                    FETCH NEXT 30 ROWS ONLY
                    """.indent(1);

            Pageable pageable = PageRequest.of(5, 30, Sort.by(CURRENT_DESK_NAME_VALUE));
            ColumnedTaskListRequest instanceColumnRequest = new ColumnedTaskListRequest(
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    List.of("url", "service"),
                    emptyMap(),
                    RETRIEVABLE
            );
            List<MetadataRepresentation> includedMetadata = List.of(
                    new MetadataRepresentation("url", EMPTY, "url", null, URL),
                    new MetadataRepresentation("service", EMPTY, "service", null, TEXT)
            );

            String result = PostgresService
                    .buildColumnedTaskListRequest(
                            "29f9b247-431d-42aa-b00b-7df7507e83f7",
                            singletonList("f641e6e1-fa60-42fc-88bb-4c4f8e12b97a"),
                            emptyList(),
                            "e9d5e78e-c63f-4c83-aa92-198769457e21",
                            instanceColumnRequest,
                            includedMetadata,
                            pageable
                    )
                    .getSQL(INLINED);

            assertEquals(
                    expected.trim().replaceAll("\\s+", " "),
                    result.trim().replaceAll("\\s+", " ")
            );
        }


        @Test
        void downstream() {

            String expected =
                    """
                    SELECT
                      act_ru_execution.ROOT_PROC_INST_ID_ AS FOLDER_ID,
                      act_ru_exec_root.NAME_ AS FOLDER_NAME,
                      act_hi_identitylink.GROUP_ID_ AS CURRENT_DESK_ID,
                      act_ru_task.ID_ AS TASK_ID,
                      act_ru_task.NAME_ AS task_name,
                      act_ru_task.DUE_DATE_ AS LATE_DATE,
                      act_ru_exec_root.START_TIME_ AS CREATION_DATE,
                      NULL AS end_date,
                      task_start.end_time_ AS START_DATE,
                      task_start.assignee_ AS EMITTER_USER_ID,
                      start_identity.GROUP_ID_ AS EMITTER_DESK_ID,
                      (task_read.id_ IS NOT NULL) AS IS_READ,
                      type_metadata.text_ AS TYPE_ID,
                      subtype_metadata.text_ AS SUBTYPE_ID,
                      NULL AS READ_TASK_CURRENT_DESK_ID,
                      upper(subtype.name) AS SORT_BY,
                      metadata_service.text_ AS metadata_service,
                      metadata_url.text_ AS metadata_url
                    FROM act_hi_identitylink
                    JOIN act_ru_task
                    ON (
                      act_hi_identitylink.TASK_ID_ = act_ru_task.ID_
                      AND act_hi_identitylink.TYPE_ = 'candidate'
                    )
                    JOIN act_ru_execution
                    ON act_ru_execution.ID_ = act_ru_task.PROC_INST_ID_
                    JOIN act_ru_execution AS act_ru_exec_root
                    ON (
                      act_ru_exec_root.ID_ = act_ru_execution.ROOT_PROC_INST_ID_
                      AND act_ru_exec_root.tenant_id_ = '29f9b247-431d-42aa-b00b-7df7507e83f7'
                    )
                    JOIN act_hi_taskinst AS task_start
                    ON (
                      task_start.name_ = 'main_start'
                      AND task_start.tenant_id_ = '29f9b247-431d-42aa-b00b-7df7507e83f7'
                    )
                    JOIN act_hi_procinst AS task_start_proc
                    ON (
                      task_start_proc.ID_ = task_start.PROC_INST_ID_
                      AND task_start_proc.business_key_ = act_ru_exec_root.business_key_
                    )
                    JOIN act_hi_identitylink AS start_identity
                    ON (
                      start_identity.TASK_ID_ = task_start.ID_
                      AND start_identity.TYPE_ = 'candidate'
                    )
                    LEFT OUTER JOIN act_hi_taskinst AS task_read
                    ON (
                      task_read.PROC_INST_ID_ = act_ru_task.PROC_INST_ID_
                      AND task_read.NAME_ = 'read'
                      AND task_read.end_time_ IS NOT NULL
                      AND task_read.assignee_ = 'e9d5e78e-c63f-4c83-aa92-198769457e21'
                    )
                    LEFT OUTER JOIN ACT_RU_VARIABLE AS type_metadata
                    ON (
                      act_ru_execution.ROOT_PROC_INST_ID_ = type_metadata.PROC_INST_ID_
                      AND type_metadata.NAME_ = 'i_Parapheur_internal_type_id'
                    )
                    LEFT OUTER JOIN ACT_RU_VARIABLE AS subtype_metadata
                    ON (
                      act_ru_execution.ROOT_PROC_INST_ID_ = subtype_metadata.PROC_INST_ID_
                      AND subtype_metadata.NAME_ = 'i_Parapheur_internal_subtype_id'
                    )
                    LEFT OUTER JOIN subtype AS subtype
                    ON (
                      subtype.id = subtype_metadata.text_
                      AND subtype.tenant_id = '29f9b247-431d-42aa-b00b-7df7507e83f7'
                    )
                    JOIN act_hi_procinst AS act_hi_procinst_upstream
                      ON act_ru_execution.business_key_ = act_hi_procinst_upstream.business_key_
                    JOIN act_hi_taskinst AS act_hi_taskinst_upstream
                      ON (
                        act_hi_procinst_upstream.ID_ = act_hi_taskinst_upstream.PROC_INST_ID_
                        AND act_hi_taskinst_upstream.tenant_id_ = '29f9b247-431d-42aa-b00b-7df7507e83f7'
                        AND act_hi_taskinst_upstream.end_time_ IS NOT NULL
                        AND act_hi_taskinst_upstream.name_ NOT IN ( 'read', 'undo' )
                      )
                    JOIN act_hi_identitylink AS act_hi_identitylink_upstream
                      ON (
                        act_hi_taskinst_upstream.ID_ = act_hi_identitylink_upstream.task_id_
                        AND act_hi_identitylink_upstream.TYPE_ = 'candidate'
                      )
                    LEFT OUTER JOIN ACT_RU_VARIABLE AS metadata_service
                    ON (
                      act_ru_execution.ROOT_PROC_INST_ID_ = metadata_service.PROC_INST_ID_
                      AND metadata_service.name_ = 'service'
                    )
                    LEFT OUTER JOIN ACT_RU_VARIABLE AS metadata_url
                    ON (
                      act_ru_execution.ROOT_PROC_INST_ID_ = metadata_url.PROC_INST_ID_
                      AND metadata_url.name_ = 'url'
                    )
                    WHERE (
                      act_ru_task.NAME_ NOT IN ( 'workflow_internal_draft', 'main_start', 'undo' )
                      AND act_ru_task.NAME_ <> 'read'
                      AND act_hi_identitylink_upstream.GROUP_ID_ IN ('f641e6e1-fa60-42fc-88bb-4c4f8e12b97a')
                    )
                    GROUP BY
                      FOLDER_ID,
                      FOLDER_NAME,
                      CURRENT_DESK_ID,
                      TASK_ID,
                      task_name,
                      LATE_DATE,
                      CREATION_DATE,
                      IS_READ,
                      SORT_BY,
                      START_DATE,
                      EMITTER_USER_ID,
                      EMITTER_DESK_ID,
                      TYPE_ID,
                      SUBTYPE_ID,
                      READ_TASK_CURRENT_DESK_ID,
                      metadata_service,
                      metadata_url
                    ORDER BY
                      SORT_BY ASC NULLS FIRST,
                      CREATION_DATE ASC
                    OFFSET 150 ROWS
                    FETCH NEXT 30 ROWS ONLY
                    """.indent(1);

            Pageable pageable = PageRequest.of(5, 30, Sort.by(SUBTYPE_NAME_VALUE));
            ColumnedTaskListRequest instanceColumnRequest = new ColumnedTaskListRequest(
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    List.of("url", "service"),
                    emptyMap(),
                    DOWNSTREAM
            );
            List<MetadataRepresentation> includedMetadata = List.of(
                    new MetadataRepresentation("url", EMPTY, "url", null, URL),
                    new MetadataRepresentation("service", EMPTY, "service", null, TEXT)
            );

            String result = PostgresService
                    .buildColumnedTaskListRequest(
                            "29f9b247-431d-42aa-b00b-7df7507e83f7",
                            singletonList("f641e6e1-fa60-42fc-88bb-4c4f8e12b97a"),
                            emptyList(),
                            "e9d5e78e-c63f-4c83-aa92-198769457e21",
                            instanceColumnRequest,
                            includedMetadata,
                            pageable
                    )
                    .getSQL(INLINED);

            assertEquals(
                    expected.trim().replaceAll("\\s+", " "),
                    result.trim().replaceAll("\\s+", " ")
            );
        }


        @Test
        void delegated() {

            String expected =
                    """
                    SELECT
                      act_ru_execution.ROOT_PROC_INST_ID_ AS FOLDER_ID,
                      act_ru_exec_root.NAME_ AS FOLDER_NAME,
                      act_hi_identitylink.GROUP_ID_ AS CURRENT_DESK_ID,
                      act_ru_task.ID_ AS TASK_ID,
                      act_ru_task.NAME_ AS task_name,
                      act_ru_task.DUE_DATE_ AS LATE_DATE,
                      act_ru_exec_root.START_TIME_ AS CREATION_DATE,
                      NULL AS end_date,
                      task_start.end_time_ AS START_DATE,
                      task_start.assignee_ AS EMITTER_USER_ID,
                      start_identity.GROUP_ID_ AS EMITTER_DESK_ID,
                      (task_read.id_ IS NOT NULL) AS IS_READ,
                      type_metadata.text_ AS TYPE_ID,
                      subtype_metadata.text_ AS SUBTYPE_ID,
                      NULL AS READ_TASK_CURRENT_DESK_ID,
                      upper(start_desk.description) AS SORT_BY,
                      metadata_service.text_ AS metadata_service,
                      metadata_url.text_ AS metadata_url
                    FROM act_hi_identitylink
                    JOIN act_ru_task
                    ON (
                      act_hi_identitylink.TASK_ID_ = act_ru_task.ID_
                      AND act_hi_identitylink.TYPE_ = 'candidate'
                    )
                    JOIN act_ru_execution
                    ON act_ru_execution.ID_ = act_ru_task.PROC_INST_ID_
                    JOIN act_ru_execution AS act_ru_exec_root
                    ON (
                      act_ru_exec_root.ID_ = act_ru_execution.ROOT_PROC_INST_ID_
                      AND act_ru_exec_root.tenant_id_ = '29f9b247-431d-42aa-b00b-7df7507e83f7'
                    )
                    JOIN act_hi_taskinst AS task_start
                    ON (
                      task_start.name_ = 'main_start'
                      AND task_start.tenant_id_ = '29f9b247-431d-42aa-b00b-7df7507e83f7'
                    )
                    JOIN act_hi_procinst AS task_start_proc
                    ON (
                      task_start_proc.ID_ = task_start.PROC_INST_ID_
                      AND task_start_proc.business_key_ = act_ru_exec_root.business_key_
                    )
                    JOIN act_hi_identitylink AS start_identity
                    ON (
                      start_identity.TASK_ID_ = task_start.ID_
                      AND start_identity.TYPE_ = 'candidate'
                    )
                    LEFT OUTER JOIN act_hi_taskinst AS task_read
                    ON (
                      task_read.PROC_INST_ID_ = act_ru_task.PROC_INST_ID_
                      AND task_read.NAME_ = 'read'
                      AND task_read.end_time_ IS NOT NULL
                      AND task_read.assignee_ = 'e9d5e78e-c63f-4c83-aa92-198769457e21'
                    )
                    LEFT OUTER JOIN ACT_RU_VARIABLE AS type_metadata
                    ON (
                      act_ru_execution.ROOT_PROC_INST_ID_ = type_metadata.PROC_INST_ID_
                      AND type_metadata.NAME_ = 'i_Parapheur_internal_type_id'
                    )
                    LEFT OUTER JOIN ACT_RU_VARIABLE AS subtype_metadata
                    ON (
                      act_ru_execution.ROOT_PROC_INST_ID_ = subtype_metadata.PROC_INST_ID_
                      AND subtype_metadata.NAME_ = 'i_Parapheur_internal_subtype_id'
                    )
                    LEFT OUTER JOIN keycloak_role AS start_desk
                    ON (
                      start_identity.GROUP_ID_ = start_desk.id
                      AND start_desk.realm_id = 'i-Parapheur'
                    )
                    LEFT OUTER JOIN ACT_RU_VARIABLE AS metadata_service
                    ON (
                      act_ru_execution.ROOT_PROC_INST_ID_ = metadata_service.PROC_INST_ID_
                      AND metadata_service.name_ = 'service'
                    )
                    LEFT OUTER JOIN ACT_RU_VARIABLE AS metadata_url
                    ON (
                      act_ru_execution.ROOT_PROC_INST_ID_ = metadata_url.PROC_INST_ID_
                      AND metadata_url.name_ = 'url'
                    )
                    WHERE (
                      act_ru_task.NAME_ NOT IN ( 'workflow_internal_draft', 'main_start' )
                      AND act_ru_task.NAME_ NOT IN ( 'workflow_internal_rejected', 'main_delete' )
                      AND act_ru_task.NAME_ NOT IN ( 'workflow_internal_finished', 'main_archive' )
                      AND act_ru_task.NAME_ <> 'undo'
                      AND act_ru_task.NAME_ <> 'read'
                      AND (
                        (
                          act_hi_identitylink.GROUP_ID_ = '981ad5ea-8986-4966-b012-35f2412d07f6'
                          AND type_metadata.text_ = '6e12ada6-f583-4ff9-b7d7-a378f2962174'
                        )
                        OR (
                          act_hi_identitylink.GROUP_ID_ = '851fe20d-5555-4800-a927-94e25b747670'
                          AND subtype_metadata.text_ = 'f577d708-6cb4-4add-9960-4b4868395d22'
                        )
                        OR act_hi_identitylink.GROUP_ID_ = '51cd3f14-c7a8-4d8a-9a74-66d50bc3ea97'
                      )
                    )
                    GROUP BY
                      FOLDER_ID,
                      FOLDER_NAME,
                      CURRENT_DESK_ID,
                      TASK_ID,
                      task_name,
                      LATE_DATE,
                      CREATION_DATE,
                      IS_READ,
                      SORT_BY,
                      START_DATE,
                      EMITTER_USER_ID,
                      EMITTER_DESK_ID,
                      TYPE_ID,
                      SUBTYPE_ID,
                      READ_TASK_CURRENT_DESK_ID,
                      metadata_service,
                      metadata_url
                    ORDER BY
                      SORT_BY ASC NULLS FIRST,
                      CREATION_DATE ASC
                    OFFSET 150 ROWS
                    FETCH NEXT 30 ROWS ONLY
                    """.indent(1);

            Pageable pageable = PageRequest.of(5, 30, Sort.by(EMITTER_DESK_NAME_VALUE));
            ColumnedTaskListRequest instanceColumnRequest = new ColumnedTaskListRequest(
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    List.of("url", "service"),
                    emptyMap(),
                    DELEGATED
            );
            List<MetadataRepresentation> includedMetadata = List.of(
                    new MetadataRepresentation("url", EMPTY, "url", null, URL),
                    new MetadataRepresentation("service", EMPTY, "service", null, TEXT)
            );

            String result = PostgresService
                    .buildColumnedTaskListRequest(
                            "29f9b247-431d-42aa-b00b-7df7507e83f7",
                            singletonList("f641e6e1-fa60-42fc-88bb-4c4f8e12b97a"),
                            List.of(
                                    new DelegationRule(
                                            "981ad5ea-8986-4966-b012-35f2412d07f6",
                                            CURRENT,
                                            META_TYPE_ID,
                                            "6e12ada6-f583-4ff9-b7d7-a378f2962174"
                                    ),
                                    new DelegationRule(
                                            "851fe20d-5555-4800-a927-94e25b747670",
                                            CURRENT, META_SUBTYPE_ID,
                                            "f577d708-6cb4-4add-9960-4b4868395d22"
                                    ),
                                    new DelegationRule(
                                            "51cd3f14-c7a8-4d8a-9a74-66d50bc3ea97",
                                            CURRENT,
                                            null,
                                            null
                                    )
                            ),
                            "e9d5e78e-c63f-4c83-aa92-198769457e21",
                            instanceColumnRequest,
                            includedMetadata,
                            pageable
                    )
                    .getSQL(INLINED);

            assertEquals(
                    expected.trim().replaceAll("\\s+", " "),
                    result.trim().replaceAll("\\s+", " ")
            );
        }


        @Test
        void stateNull() {

            String expected =
                    """
                    SELECT
                      act_ru_execution.ROOT_PROC_INST_ID_ AS FOLDER_ID,
                      act_ru_exec_root.NAME_ AS FOLDER_NAME,
                      act_hi_identitylink.GROUP_ID_ AS CURRENT_DESK_ID,
                      act_ru_task.ID_ AS TASK_ID,
                      act_ru_task.NAME_ AS task_name,
                      act_ru_task.DUE_DATE_ AS LATE_DATE,
                      act_ru_exec_root.START_TIME_ AS CREATION_DATE,
                      NULL AS end_date,
                      task_start.end_time_ AS START_DATE,
                      task_start.assignee_ AS EMITTER_USER_ID,
                      start_identity.GROUP_ID_ AS EMITTER_DESK_ID,
                      (task_read.id_ IS NOT NULL) AS IS_READ,
                      type_metadata.text_ AS TYPE_ID,
                      subtype_metadata.text_ AS SUBTYPE_ID,
                      NULL AS READ_TASK_CURRENT_DESK_ID,
                      upper(start_desk.description) AS SORT_BY,
                      metadata_service.text_ AS metadata_service,
                      metadata_url.text_ AS metadata_url
                    FROM act_hi_identitylink
                    JOIN act_ru_task
                    ON (
                      act_hi_identitylink.TASK_ID_ = act_ru_task.ID_
                      AND act_hi_identitylink.TYPE_ = 'candidate'
                    )
                    JOIN act_ru_execution
                    ON act_ru_execution.ID_ = act_ru_task.PROC_INST_ID_
                    JOIN act_ru_execution AS act_ru_exec_root
                    ON (
                      act_ru_exec_root.ID_ = act_ru_execution.ROOT_PROC_INST_ID_
                      AND act_ru_exec_root.tenant_id_ = '29f9b247-431d-42aa-b00b-7df7507e83f7'
                    )
                    JOIN act_hi_taskinst AS task_start
                    ON (
                      task_start.name_ = 'main_start'
                      AND task_start.tenant_id_ = '29f9b247-431d-42aa-b00b-7df7507e83f7'
                    )
                    JOIN act_hi_procinst AS task_start_proc
                    ON (
                      task_start_proc.ID_ = task_start.PROC_INST_ID_
                      AND task_start_proc.business_key_ = act_ru_exec_root.business_key_
                    )
                    JOIN act_hi_identitylink AS start_identity
                    ON (
                      start_identity.TASK_ID_ = task_start.ID_
                      AND start_identity.TYPE_ = 'candidate'
                    )
                    LEFT OUTER JOIN act_hi_taskinst AS task_read
                    ON (
                      task_read.PROC_INST_ID_ = act_ru_task.PROC_INST_ID_
                      AND task_read.NAME_ = 'read'
                      AND task_read.end_time_ IS NOT NULL
                      AND task_read.assignee_ = 'e9d5e78e-c63f-4c83-aa92-198769457e21'
                    )
                    LEFT OUTER JOIN ACT_RU_VARIABLE AS type_metadata
                    ON (
                      act_ru_execution.ROOT_PROC_INST_ID_ = type_metadata.PROC_INST_ID_
                      AND type_metadata.NAME_ = 'i_Parapheur_internal_type_id'
                    )
                    LEFT OUTER JOIN ACT_RU_VARIABLE AS subtype_metadata
                    ON (
                      act_ru_execution.ROOT_PROC_INST_ID_ = subtype_metadata.PROC_INST_ID_
                      AND subtype_metadata.NAME_ = 'i_Parapheur_internal_subtype_id'
                    )
                    LEFT OUTER JOIN keycloak_role AS start_desk
                    ON (
                      start_identity.GROUP_ID_ = start_desk.id
                      AND start_desk.realm_id = 'i-Parapheur'
                    )
                    LEFT OUTER JOIN ACT_RU_VARIABLE AS metadata_service
                    ON (
                      act_ru_execution.ROOT_PROC_INST_ID_ = metadata_service.PROC_INST_ID_
                      AND metadata_service.name_ = 'service'
                    )
                    LEFT OUTER JOIN ACT_RU_VARIABLE AS metadata_url
                    ON (
                      act_ru_execution.ROOT_PROC_INST_ID_ = metadata_url.PROC_INST_ID_
                      AND metadata_url.name_ = 'url'
                    )
                    WHERE (
                      act_ru_task.NAME_ <> 'read'
                      AND act_hi_identitylink.GROUP_ID_ IN ('f641e6e1-fa60-42fc-88bb-4c4f8e12b97a')
                    )
                    GROUP BY
                      FOLDER_ID,
                      FOLDER_NAME,
                      CURRENT_DESK_ID,
                      TASK_ID,
                      task_name,
                      LATE_DATE,
                      CREATION_DATE,
                      IS_READ,
                      SORT_BY,
                      START_DATE,
                      EMITTER_USER_ID,
                      EMITTER_DESK_ID,
                      TYPE_ID,
                      SUBTYPE_ID,
                      READ_TASK_CURRENT_DESK_ID,
                      metadata_service,
                      metadata_url
                    ORDER BY
                      SORT_BY ASC NULLS FIRST,
                      CREATION_DATE ASC
                    OFFSET 150 ROWS
                    FETCH NEXT 30 ROWS ONLY
                    """.indent(1);

            Pageable pageable = PageRequest.of(5, 30, Sort.by(EMITTER_DESK_NAME_VALUE));
            ColumnedTaskListRequest instanceColumnRequest = new ColumnedTaskListRequest(
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    List.of("url", "service"),
                    emptyMap(),
                    null
            );
            List<MetadataRepresentation> includedMetadata = List.of(
                    new MetadataRepresentation("url", EMPTY, "url", null, URL),
                    new MetadataRepresentation("service", EMPTY, "service", null, TEXT)
            );

            String result = PostgresService
                    .buildColumnedTaskListRequest(
                            "29f9b247-431d-42aa-b00b-7df7507e83f7",
                            singletonList("f641e6e1-fa60-42fc-88bb-4c4f8e12b97a"),
                            List.of(
                                    new DelegationRule(
                                            "981ad5ea-8986-4966-b012-35f2412d07f6",
                                            CURRENT,
                                            META_TYPE_ID,
                                            "6e12ada6-f583-4ff9-b7d7-a378f2962174"
                                    ),
                                    new DelegationRule(
                                            "851fe20d-5555-4800-a927-94e25b747670",
                                            CURRENT, META_SUBTYPE_ID,
                                            "f577d708-6cb4-4add-9960-4b4868395d22"
                                    ),
                                    new DelegationRule(
                                            "51cd3f14-c7a8-4d8a-9a74-66d50bc3ea97",
                                            CURRENT,
                                            null,
                                            null
                                    )
                            ),
                            "e9d5e78e-c63f-4c83-aa92-198769457e21",
                            instanceColumnRequest,
                            includedMetadata,
                            pageable
                    )
                    .getSQL(INLINED);

            assertEquals(
                    expected.trim().replaceAll("\\s+", " "),
                    result.trim().replaceAll("\\s+", " ")
            );
        }

    }


}
