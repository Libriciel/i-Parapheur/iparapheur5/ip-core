/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.database;

import coop.libriciel.ipcore.model.crypto.PdfSignaturePosition;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Type;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static coop.libriciel.ipcore.model.crypto.SignatureFormat.PES_V2;
import static coop.libriciel.ipcore.model.database.Type.SignatureProtocol.HELIOS;
import static coop.libriciel.ipcore.model.pdfstamp.Origin.BOTTOM_LEFT;
import static java.util.Collections.emptyList;
import static java.util.stream.StreamSupport.stream;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;


@SpringBootTest
@ExtendWith(SpringExtension.class)
@AutoConfigureTestDatabase(replace = NONE)
class TypeRepositoryTest {


    private @Autowired TypeRepository typeRepository;
    private @Autowired SubtypeRepository subtypeRepository;


    @BeforeEach
    void setup() {

        Type type = Type.builder()
                .id("type_01").name("Type 01").description("First type")
                .subtypes(emptyList())
                .signatureFormat(PES_V2).protocol(HELIOS)
                .signatureLocation("Montpellier").signatureZipCode("34000")
                .signaturePosition(new PdfSignaturePosition(50f, 75f, -1, BOTTOM_LEFT, null))
                .build();

        type = typeRepository.save(type);
        assertNotNull(type);
        assertEquals("type_01", type.getId());
        assertEquals("Type 01", type.getName());
        assertEquals("First type", type.getDescription());
        assertNotNull(type.getSubtypes());
        assertEquals(PES_V2, type.getSignatureFormat());
        assertEquals(HELIOS, type.getProtocol());
        assertEquals("Montpellier", type.getSignatureLocation());
        assertEquals("34000", type.getSignatureZipCode());
        assertTrue(type.isSignatureVisible());
        assertNotNull(type.getSignaturePosition());
        assertEquals(50f, type.getSignaturePosition().getX(), 1f);
        assertEquals(75f, type.getSignaturePosition().getY(), 1f);
        assertEquals(-1, type.getSignaturePosition().getPage());
    }


    @AfterEach
    void cleanup() {
        typeRepository.deleteAll();
        subtypeRepository.deleteAll();
    }


    @Test
    void deserialize() {

        Type type = typeRepository.findAll().stream().findFirst().orElse(null);
        assertNotNull(type);

        type = typeRepository.findById(type.getId()).orElse(null);

        assertNotNull(type);
        assertEquals("type_01", type.getId());
        assertEquals("Type 01", type.getName());
        assertEquals("First type", type.getDescription());
        assertNotNull(type.getSubtypes());
        assertEquals(PES_V2, type.getSignatureFormat());
        assertEquals(HELIOS, type.getProtocol());
        assertEquals("Montpellier", type.getSignatureLocation());
        assertEquals("34000", type.getSignatureZipCode());
        assertTrue(type.isSignatureVisible());
        assertNotNull(type.getSignaturePosition());
        assertEquals(50f, type.getSignaturePosition().getX(), 1f);
        assertEquals(75f, type.getSignaturePosition().getY(), 1f);
        assertEquals(-1, type.getSignaturePosition().getPage());
    }


    @Test
    void deleteCascade() {

        Type type = stream(typeRepository.findAll().spliterator(), false).findFirst().orElse(null);
        assertNotNull(type);

        subtypeRepository.save(Subtype.builder().id("st11").name("Subtype 01").parentType(type).description("first subtype").build());
        subtypeRepository.save(Subtype.builder().id("st12").name("Subtype 02").parentType(type).description("second subtype").build());

        type = typeRepository.findById(type.getId()).orElse(null);
        assertNotNull(type);

        // Delete cascade

        typeRepository.delete(type);
        assertEquals(0, stream(subtypeRepository.findAll().spliterator(), false).count());
    }


}
