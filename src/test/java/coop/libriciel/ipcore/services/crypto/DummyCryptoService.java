/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.crypto;

import coop.libriciel.crypto.api.model.CadesParameters;
import coop.libriciel.crypto.api.model.DataToSign;
import coop.libriciel.crypto.api.model.PadesParameters;
import coop.libriciel.crypto.api.model.XadesParameters;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.crypto.DocumentDataToSignHolder;
import coop.libriciel.ipcore.services.crypto.CryptoServiceProperties.AutoFormatDefaultValue;
import coop.libriciel.ipcore.services.crypto.CryptoServiceProperties.SignatureLocation;
import org.jetbrains.annotations.NotNull;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.UUID;

import static coop.libriciel.ipcore.model.crypto.SignatureFormat.PKCS7;
import static coop.libriciel.ipcore.utils.TextUtilsTest.DUMMY_SERVICE;
import static io.netty.util.NetUtil.LOCALHOST4;
import static java.util.Collections.singletonList;


@Service(CryptoServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = CryptoServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = DUMMY_SERVICE)
public class DummyCryptoService implements CryptoServiceInterface {


    @Override
    public @NotNull CryptoServiceProperties getProperties() {
        return new CryptoServiceProperties(
                DUMMY_SERVICE,
                LOCALHOST4.getHostName(),
                80,
                new AutoFormatDefaultValue(PKCS7),
                new SignatureLocation("France", "Montpellier", "34000")
        );
    }


    @Override
    public DocumentDataToSignHolder getDataToSign(@NotNull DocumentBuffer documentBuffer, @NotNull CadesParameters params) {
        return null;
    }


    @Override
    public DocumentDataToSignHolder getDataToSign(@NotNull DocumentBuffer documentBuffer, @NotNull XadesParameters params) {

        DataToSign dataToSign = new DataToSign();
        dataToSign.setId(UUID.randomUUID().toString());
        dataToSign.setDataToSignBase64(Base64.getEncoder().encodeToString("DATA_TO_SIGN".getBytes()));

        DocumentDataToSignHolder dataToSignHolder = new DocumentDataToSignHolder();
        dataToSignHolder.setDataToSignList(singletonList(dataToSign));
        dataToSignHolder.setDocumentId(documentBuffer.getId());
        dataToSignHolder.setSignatureDateTime(15524092460L);

        return dataToSignHolder;
    }


    @Override
    public DocumentDataToSignHolder getDataToSign(@NotNull DocumentBuffer documentBuffer, @NotNull PadesParameters params) {

        DataToSign dataToSign = new DataToSign();
        dataToSign.setId(UUID.randomUUID().toString());
        dataToSign.setDataToSignBase64(Base64.getEncoder().encodeToString("DATA_TO_SIGN".getBytes()));

        DocumentDataToSignHolder dataToSignHolder = new DocumentDataToSignHolder();
        dataToSignHolder.setDataToSignList(singletonList(dataToSign));
        dataToSignHolder.setDocumentId(documentBuffer.getId());
        dataToSignHolder.setSignatureDateTime(15524092460L);

        return dataToSignHolder;
    }


    @Override
    public DocumentBuffer signDocument(@NotNull CadesParameters params) {
        return null;
    }


    @Override
    public DocumentBuffer signDocument(@NotNull DocumentBuffer documentBuffer, @NotNull XadesParameters params) {
        return null;
    }


    @Override
    public DocumentBuffer signDocument(@NotNull DocumentBuffer documentBuffer, @NotNull PadesParameters params) {
        return null;
    }


}
