/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.crypto;

import coop.libriciel.crypto.api.model.PdfSignatureStamp;
import coop.libriciel.crypto.api.model.XadesParameters;
import coop.libriciel.ipcore.business.template.TemplateBusinessService;
import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.database.TemplateType;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.database.Type;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.regex.Matcher;

import static coop.libriciel.crypto.api.model.SignatureStampElementType.IMAGE;
import static coop.libriciel.ipcore.services.crypto.CryptoServiceInterface.*;
import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
@ExtendWith(SpringExtension.class)
class CryptoServiceInterfaceTest {


    private final CryptoServiceInterface cryptoService = new DummyCryptoService();
    private @Autowired TemplateBusinessService templateBusinessService;


    @Nested
    class Regexes {


        @Test
        void complementaryTitleCity() {

            assertFalse(COMPLEMENTARY_INFO_TITLE_PATTERN.matcher("").find());
            assertFalse(COMPLEMENTARY_INFO_TITLE_PATTERN.matcher("nope").find());

            String test = "VILLE=\"Montpellier\" , TITRE=\"Maître Docteur, M.D.\",CODEPOSTAL=\"34000\"";
            Matcher matcher = COMPLEMENTARY_INFO_TITLE_PATTERN.matcher(test);
            assertTrue(matcher.find());
            assertEquals("Maître Docteur, M.D.", matcher.group(1));
        }


        @Test
        void complementaryInfoCity() {

            assertFalse(COMPLEMENTARY_INFO_CITY_PATTERN.matcher("").find());
            assertFalse(COMPLEMENTARY_INFO_CITY_PATTERN.matcher("nope").find());

            String test = "VILLE=\"Montpellier\", TITRE=\"Maître Docteur, M.D.\",CODEPOSTAL=\"34000\"";
            Matcher matcher = COMPLEMENTARY_INFO_CITY_PATTERN.matcher(test);
            assertTrue(matcher.find());
            assertEquals("Montpellier", matcher.group(1));
        }


        @Test
        void complementaryInfoZipCode() {

            assertFalse(COMPLEMENTARY_INFO_ZIP_CODE_PATTERN.matcher("").find());
            assertFalse(COMPLEMENTARY_INFO_ZIP_CODE_PATTERN.matcher("nope").find());

            String testNoSpace = "VILLE=\"Montpellier\", TITRE=\"Maître Docteur, M.D.\",CODEPOSTAL=\"34000\"";
            Matcher matcherNoSpace = COMPLEMENTARY_INFO_ZIP_CODE_PATTERN.matcher(testNoSpace);
            assertTrue(matcherNoSpace.find());
            assertEquals("34000", matcherNoSpace.group(1));

            String testSnakeCase = "VILLE=\"Montpellier\", TITRE=\"Maître Docteur, M.D.\",CODE_POSTAL=\"44000\"";
            Matcher matcherSnakeCase = COMPLEMENTARY_INFO_ZIP_CODE_PATTERN.matcher(testSnakeCase);
            assertTrue(matcherSnakeCase.find());
            assertEquals("44000", matcherSnakeCase.group(1));
        }


    }


    @Test
    void getPdfSignatureStamp() {

        Resource mediumTemplateResource = templateBusinessService.getDefaultTemplateResource(TemplateType.SIGNATURE_MEDIUM);
        PdfSignatureStamp stamp = cryptoService.getPdfSignatureStamp(emptyMap(), mediumTemplateResource);

        assertNotNull(stamp.getHeight());
        assertNotNull(stamp.getWidth());
        assertNotNull(stamp.getElements());

        assertEquals(70, stamp.getHeight(), 1F);
        assertEquals(200, stamp.getWidth(), 1F);
        assertEquals(4, stamp.getElements().size());

        stamp.getElements().stream()
                .filter(e -> e.getType() == IMAGE)
                .forEach(e -> {
                    assertTrue(StringUtils.isNotEmpty(e.getValue()));
                    assertFalse(e.getValue().contains(" "));
                    assertFalse(e.getValue().contains("\n"));
                });
    }


    @Test
    void overrideParamsWithComplementaryInfo_allParams_success() {

        String complementaryInfo = "TITRE=\"Overridden title\", VILLE=\"Overridden City\",CODEPOSTAL=\"56789\"";
        User user = User.builder()
                .userName("user")
                .complementaryField(complementaryInfo)
                .build();

        XadesParameters signatureParams = new XadesParameters();
        signatureParams.setClaimedRoles(singletonList("baseRole"));
        signatureParams.setCity("baseCity");
        signatureParams.setZipCode("12345");

        cryptoService.overrideParamsWithComplementaryInfo(
                user,
                new Desk(),
                new Type(),
                new Tenant(),
                signatureParams::setClaimedRoles,
                signatureParams::setCountry,
                signatureParams::setCity,
                signatureParams::setZipCode
        );

        assertTrue(CollectionUtils.isNotEmpty(signatureParams.getClaimedRoles()));
        assertEquals("Overridden title", signatureParams.getClaimedRoles().get(0));
        assertEquals("Overridden City", signatureParams.getCity());
        assertEquals("56789", signatureParams.getZipCode());
    }


}
