/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.groovy;

import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.workflow.StepDefinition;
import coop.libriciel.ipcore.model.workflow.WorkflowDefinition;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static coop.libriciel.ipcore.model.workflow.Action.VISA;
import static coop.libriciel.ipcore.model.workflow.StepDefinitionParallelType.OR;
import static coop.libriciel.ipcore.utils.TextUtils.INTERNAL_PREFIX;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@SpringBootTest
@ExtendWith(SpringExtension.class)
class GroovyServiceTest {


    private @Autowired GroovyService groovyService;
    private @Mock AuthServiceInterface authServiceMock = mock(AuthServiceInterface.class);


    @Nested
    class ExecuteSelectionScript {


        @Test
        void simpleScript() {

            String script =
                    """
                    circuit "test1234";
                    """;

            GroovyResultCatcher result = groovyService.executeSelectionScript("tenant01", script, emptyMap());

            assertNotNull(result);
            assertEquals("test1234", result.getWorkflowDefinitionKey());
        }


        @Test
        void metadataPlaceholder() {

            String script =
                    """
                    deskId = "test1234"
                    circuit "test", ["${deskId}"];
                    """;

            GroovyResultCatcher result = groovyService.executeSelectionScript("tenant01", script, emptyMap());

            assertNotNull(result);
            assertEquals("test", result.getWorkflowDefinitionKey());
            assertNotNull(result.getVariableDesks());
            assertEquals(1, result.getVariableDesks().size());
            assertEquals("test1234", result.getVariableDesks().get(0));
        }


        @Test
        void legacyScript() {

            String script =
                    """
                    circuit '5 Key With Uppercase And Spaces';
                    """;

            GroovyResultCatcher result = groovyService.executeSelectionScript("tenant01", script, emptyMap());

            assertNotNull(result);
            assertEquals("_5_key_with_uppercase_and_spaces", result.getWorkflowDefinitionKey());
        }


        @Test
        void wrapper() {

            String script =
                    """
                    selectionCircuit {
                      circuit "test4321";
                    }
                    """;

            GroovyResultCatcher result = groovyService.executeSelectionScript("tenant01", script, emptyMap());

            assertNotNull(result);
            assertEquals("test4321", result.getWorkflowDefinitionKey());
        }


        @Test
        void metadataInput() {

            String script =
                    """
                    if (meta01 == '001') {
                      circuit "entered001";
                    }
                    
                    if (meta01 == '002') {
                      circuit "entered002";
                    }
                    
                    circuit "didNotEnterIfs";
                    """;

            GroovyResultCatcher result = groovyService.executeSelectionScript("tenant01", script, Map.of("meta01", "002"));
            assertNotNull(result);
            assertEquals("entered002", result.getWorkflowDefinitionKey());
        }


        @Test
        void metadataContext() {

            String script =
                    """
                    if (meta01 == "value01") {
                      circuit "meta01_exists";
                    }
                    
                    circuit "default";
                    """;

            GroovyResultCatcher result = groovyService.executeSelectionScript("tenant01", script, Map.of("meta01", "value01"));
            assertNotNull(result);
            assertEquals("meta01_exists", result.getWorkflowDefinitionKey());

            // Test if the previous meta01 value is properly cleaned from context,
            // and does not exist anymore on a second call.
            assertThrows(
                    LocalizedStatusException.class,
                    () -> groovyService.executeSelectionScript("tenant01", script, emptyMap())
            );
        }


        @Test
        void deskSelector() {

            String script =
                    """
                    circuit "signature_helios", ["Bleu"];
                    """;

            GroovyResultCatcher result = groovyService.executeSelectionScript("tenant01", script, Map.of("meta01", "002"));

            assertNotNull(result);
            assertEquals("signature_helios", result.getWorkflowDefinitionKey());
            assertEquals(1, result.getVariableDesks().size());
            assertEquals("Bleu", result.getVariableDesks().get(0));
        }


    }


    @Test
    void updateVariableDeskMapFromScriptResult() {

        // Prepare variables

        String tenantId = "tenantId01";
        String workflowDefinitionId = "workflowDefinitionId01";
        List<Object> variableDesks = List.of("desk01", "desk02", "desk03");

        GroovyResultCatcher groovyResultCatcher = new GroovyResultCatcher();
        groovyResultCatcher.setResult(workflowDefinitionId, variableDesks);

        WorkflowDefinition workflowDefinition = new WorkflowDefinition();
        workflowDefinition.setSteps(List.of(
                new StepDefinition(
                        VISA, OR,
                        List.of(new DeskRepresentation(UUID.randomUUID().toString())),
                        emptyList(), emptyList(), emptyList()
                ),
                new StepDefinition(
                        VISA, OR,
                        List.of(new DeskRepresentation("${" + INTERNAL_PREFIX + "validation_variable_desk_id_1}")),
                        emptyList(), emptyList(), emptyList()
                ),
                new StepDefinition(
                        VISA, OR,
                        List.of(new DeskRepresentation(UUID.randomUUID().toString())),
                        emptyList(), emptyList(), emptyList()
                ),
                new StepDefinition(
                        VISA, OR,
                        List.of(new DeskRepresentation("${" + INTERNAL_PREFIX + "validation_variable_desk_id_3}")),
                        emptyList(), emptyList(), emptyList()
                )
        ));

        // Prepare mockups

        GroovyService groovyServiceTest = new GroovyService(authServiceMock, null, null);

        when(authServiceMock.findDeskByIdOrShortName(any(), any()))
                .thenReturn(new Desk(UUID.randomUUID().toString()));

        // Actual tests

        HashMap<Integer, String> result = new HashMap<>();
        groovyServiceTest.updateVariableDeskMapFromScriptResult(tenantId, groovyResultCatcher, workflowDefinition, result);

        assertEquals(2, result.size());
        assertTrue(StringUtils.isNotEmpty(result.get(1)));
        assertTrue(StringUtils.isNotEmpty(result.get(3)));
    }


}
