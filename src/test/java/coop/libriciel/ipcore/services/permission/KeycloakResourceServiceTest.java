/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.permission;

import org.junit.jupiter.api.Test;

import java.util.Set;

import static coop.libriciel.ipcore.services.permission.PermissionServiceInterface.RESOURCE_TYPE_DESK;
import static coop.libriciel.ipcore.services.permission.PermissionServiceInterface.SCOPE_VIEW;
import static org.jooq.conf.ParamType.INLINED;
import static org.junit.jupiter.api.Assertions.assertEquals;


class KeycloakResourceServiceTest {


    @Test
    void buildPrefetchSqlRequest() {
        String expected =
                """
                SELECT
                  resource.id AS targetResourceId,
                  resource.name AS targetResourceName,
                  policy.name AS sourceDeskName

                FROM resource_server_policy AS permission
                
                JOIN associated_policy
                  ON associated_policy.policy_id = permission.id

                JOIN resource_server_policy AS policy
                  ON policy.id = associated_policy.associated_policy_id

                JOIN policy_config AS policy_config_role
                  ON (
                    policy_config_role.policy_id = policy.id
                    AND (
                      ( policy_config_role.name = 'roles' AND policy_config_role.value IN ('[{"id":"desk01","required":true}]') )
                      OR
                      ( policy_config_role.name = 'users' AND policy_config_role.value = '["user01"]' )
                    )
                  )

                JOIN scope_policy
                  ON scope_policy.policy_id = permission.id

                JOIN resource_server_scope AS scope
                  ON scope.id = scope_policy.scope_id

                JOIN resource_policy
                  ON resource_policy.policy_id = permission.id

                JOIN resource_server_resource AS resource
                  ON resource.id = resource_policy.resource_id

                WHERE (
                  permission.type = 'scope'
                  AND scope.name LIKE 'urn:ipcore:scopes:view'
                  AND resource.type = 'urn:ipcore:resources:desk'
                )
                """.indent(1);

        String result = KeycloakResourceService
                .buildPrefetchSqlRequest(Set.of("desk01"), "user01", RESOURCE_TYPE_DESK, SCOPE_VIEW)
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


}
