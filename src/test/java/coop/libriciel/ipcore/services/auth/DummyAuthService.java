/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.auth;

import coop.libriciel.ipcore.model.auth.*;
import coop.libriciel.ipcore.model.auth.requests.UserDto;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.utils.PaginatedList;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;

import static coop.libriciel.ipcore.utils.PaginatedList.emptyPaginatedList;
import static coop.libriciel.ipcore.utils.TextUtilsTest.DUMMY_SERVICE;
import static java.util.Collections.*;
import static org.springframework.data.domain.Page.empty;


@Service(AuthServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = AuthServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = DUMMY_SERVICE)
public class DummyAuthService implements AuthServiceInterface {


    @Override
    public boolean isInternalUser(@NotNull User user) {return false;}


    // <editor-fold desc="Tenants CRUDL">


    @Override
    public @NotNull String createTenant(@NotNull String tenantKey) {return UUID.randomUUID().toString();}


    @Override
    public void deleteTenant(@NotNull String tenantKey) {}


    // </editor-fold desc="Tenants CRUDL">


    // <editor-fold desc="Users CRUDL">


    @Override
    public @NotNull String createUser(@NotNull UserDto request, @Nullable String currentTenantId) {
        return UUID.randomUUID().toString();
    }


    @Override
    public @Nullable User findUserById(@NotNull String id) {
        return new User(id);
    }


    @Override
    public void updateUser(@NotNull String userId,
                           @Nullable String firstName,
                           @Nullable String lastName,
                           @Nullable String email,
                           @Nullable String complementaryField) {}


    @Override
    public void updateUserPreferences(@NotNull String userId,
                                      @Nullable String notificationsRedirectionMail,
                                      @Nullable String isNotifiedOnConfidentialFolders,
                                      @Nullable String isNotifiedOnFollowedFolders,
                                      @Nullable String isNotifiedOnLateFolders,
                                      @Nullable String notificationsCronFrequency) {}


    @Override
    public void updateUserInternalMetadata(@NotNull String userId,
                                           @Nullable Integer contentGroupIndex,
                                           @Nullable String contentNodeId,
                                           @Nullable String signatureImageContentId) {}


    @Override
    public void updateUserGlobalPrivileges(@NotNull User user, @NotNull UserDto updatedUser, String specificTenantId) {}


    @Override
    public void resetUserPassword(@NotNull String userId, @NotNull String password) {}


    @Override
    public void deleteUser(@NotNull String userId) {}


    @Override
    public int countLoggedInUsers() {
        return 0;
    }


    @Override
    public @NotNull Set<String> listTenantsForUser(String userId) {
        return emptySet();
    }


    @Override
    public @NotNull Page<User> listUsers(@NotNull Pageable pageable, @Nullable String searchTerm) {
        return empty();
    }


    @Override
    public @NotNull Page<User> listTenantUsers(@NotNull String tenantId,
                                               @NotNull Pageable pageable,
                                               @Nullable String searchTerm) {
        return empty();
    }


    // </editor-fold desc="Users CRUDL">


    @Override
    public void removeUserFromTenant(@NotNull String userId, @NotNull String tenantId) {}


    @Override
    public @NotNull Set<String> filterUsersWithPrivilege(@NotNull Set<String> userIds, @NotNull UserPrivilege privilege, @Nullable String targetTenantId) {
        return emptySet();
    }


    @Override
    public @NotNull List<User> getSuperAdminsList(int maxResults) {return emptyList();}


    @Override
    public void addUserToTenant(@NotNull User user, @NotNull String tenantId, @NotNull UserPrivilege privilege) {}


    @Override
    public void addDefaultUserOnTenant(Tenant tenant) {}


    @Override
    public @NotNull String createDesk(@NotNull String tenantId, @NotNull String name, @NotNull String shortName, @Nullable String description,
                                      @Nullable String parentDeskId) {
        return UUID.randomUUID().toString();
    }


    @Override
    public void editDesk(@NotNull String tenantId, @NotNull Desk previousDeskVersion, @NotNull String name, @NotNull String shortName,
                         @Nullable String description, @Nullable String directParentId) {}


    @Override
    public @NotNull Page<Desk> listDesks(@NotNull String tenantKey, @NotNull Pageable pageable, @NotNull List<String> reverseIdList, boolean collapseAll) {
        return empty();
    }


    @Override
    public @NotNull Page<Desk> searchDesks(@NotNull String tenantKey, @NotNull Pageable pageable, @NotNull List<String> reverseIdList,
                                           @Nullable String searchTerm, boolean collapseAll) {
        return empty();
    }


    @Override
    public @NotNull Page<DeskRepresentation> findDeskByShortName(@NotNull String tenantId, @NotNull String shortName) {
        return new PageImpl<>(singletonList(new DeskRepresentation(UUID.randomUUID().toString(), shortName)));
    }


    @Override
    public @Nullable Desk findDeskById(@Nullable String tenantId, @NotNull String id) {return Desk.builder().id(id).build();}


    @Override
    public @Nullable Desk findDeskByIdNoException(@Nullable String tenantId, @NotNull String deskId) {
        return null;
    }


    @Override
    public @Nullable User findTenantUserById(@Nullable String tenantId, @NotNull String id) {return null;}


    @Override
    public void updateUserPrivileges(@NotNull String tenantId, @NotNull User user, @NotNull UserPrivilege newPrivilege) {}


    @Override
    public @NotNull List<User> getUsersByIds(@NotNull Set<String> userIds) {return emptyList();}


    @Override
    public void deleteDesk(@NotNull String deskId) {}


    @Override
    public void addUsersToDesk(@NotNull String deskId, @NotNull Collection<String> userIds) {}


    @Override
    public @NotNull PaginatedList<User> listTenantAdminUsers(@NotNull String tenantId, int page, int pageSize) {return emptyPaginatedList();}


    @Override
    public @NotNull PaginatedList<User> listUsersFromDesk(String tenantId, @NotNull String deskId, int page, int pageSize) {return emptyPaginatedList();}


    @Override
    public void refreshUsersDeskStatus(@NotNull List<User> userList, @NotNull String deskId) {}


    @Override
    public void populateDeskNamesAndTenantIds(@NotNull Map<String, Desk> deskMap) {

    }


    @Override
    public String getServiceAccountUserId() {
        return "service-account-user-id";
    }


    @Override
    public @NotNull PasswordPolicies getPasswordPolicies() {
        return new PasswordPolicies();
    }


    @Override
    public @NotNull Page<DeskRepresentation> getDesksFromUser(@NotNull String userId, Pageable pageable, @Nullable String searchTerm) {
        return empty();
    }


    @Override
    public void removeUsersFromDesk(@NotNull String deskId, @NotNull Collection<String> userIds) {}


    @Override
    public @NotNull Map<String, String> getDeskNames(@NotNull Set<String> deskIds) {return emptyMap();}


}
