/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.auth;

import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.PasswordPolicies;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.services.mail.NotificationServiceProperties;
import coop.libriciel.ipcore.services.permission.KeycloakResourceService;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.*;
import org.keycloak.authorization.client.AuthzClient;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.keycloak.representations.idm.authorization.ResourceRepresentation;
import org.keycloak.representations.idm.authorization.RolePolicyRepresentation;
import org.keycloak.representations.idm.authorization.ScopePermissionRepresentation;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

import static coop.libriciel.ipcore.model.auth.UserPrivilege.SUPER_ADMIN;
import static coop.libriciel.ipcore.model.auth.requests.UserSortBy.USERNAME;
import static coop.libriciel.ipcore.utils.TextUtilsTest.DUMMY_SERVICE;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.jooq.conf.ParamType.INLINED;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;


@SpringBootTest
class KeycloakServiceTest {


    private KeycloakService keycloakService;

    private KeycloakResourceService keycloakResourceServerService;
    private Keycloak keycloakMock;
    private RealmResource keycloakRealmMock;
    private AuthzClient authzClient;
    private ClientResource clientResource;
    private ResourcesResource resources;
    private ModelMapper modelMapper;
    private UsersResource usersResourceClient;


    @BeforeEach
    void setUp() {

        authzClient = Mockito.mock(AuthzClient.class);
        keycloakRealmMock = Mockito.mock(RealmResource.class);
        keycloakMock = Mockito.mock(Keycloak.class);
        clientResource = Mockito.mock(ClientResource.class);
        resources = Mockito.mock(ResourcesResource.class);
        modelMapper = Mockito.mock(ModelMapper.class);
        usersResourceClient = Mockito.mock(UsersResource.class);

        AuthorizationResource authorizationResource = Mockito.mock(AuthorizationResource.class);
        Mockito.doReturn(authorizationResource).when(clientResource).authorization();
        Mockito.doReturn(new UserRepresentation()).when(clientResource).getServiceAccountUser();
        Mockito.doReturn(resources).when(authorizationResource).resources();
        Mockito.doReturn(keycloakRealmMock).when(keycloakMock).realm(anyString());

        AuthServiceProperties authenticationProperties = new AuthServiceProperties(
                "keycloak", "localhost", 8080,
                60000, "i-Parapheur", "user", "pass",
                new AuthServiceProperties.DataBase("localhost", 4567, "test", "user", "pass"),
                16
        );

        NotificationServiceProperties notificationProperties = new NotificationServiceProperties(
                DUMMY_SERVICE,
                "noreply@dom.local", 1025,
                "dummy", "pass", true,
                true, true,
                "dummy@dom.invalid",
                "https://iparapheur.dom.local",
                "Footer éè", "[iparapheur éè]"
        );

        keycloakService = new KeycloakService(authenticationProperties, keycloakMock, modelMapper, notificationProperties);
        keycloakService.realmResource = keycloakRealmMock;
        keycloakService.clientResource = clientResource;
        keycloakService.usersResourceClient = usersResourceClient;

        keycloakResourceServerService = new KeycloakResourceService(authzClient, authenticationProperties, keycloakMock);
        keycloakResourceServerService.setClientResource(clientResource);
    }


    @Nested
    class ParsePasswordPolicies {


        @Test
        void full() {

            PasswordPolicies passwordPolicy = KeycloakService.parsePasswordPolicies(
                    "length(12)"
                    + " and notUsername(undefined)"
                    + " and specialChars(1)"
                    + " and notEmail(undefined)"
                    + " and upperCase(1)"
                    + " and lowerCase(1)"
                    + " and digits(1)"
                    + " and maxLength(64)"
                    + " and regexPattern(^(?:(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=[^a-zA-Z/d]).*{12,})$)"
                    + " and hashAlgorithm(pbkdf2-sha256)"
                    + " and forceExpiredPasswordChange(365)"
                    + " and passwordHistory(3)"
                    + " and maxAuthAge(300)"
            );

            assertNotNull(passwordPolicy);
            assertEquals(12, passwordPolicy.getMinLength());
            assertEquals(64, passwordPolicy.getMaxLength());
            assertTrue(passwordPolicy.isNotUsername());
            assertTrue(passwordPolicy.isNotEmail());
            assertEquals(1, passwordPolicy.getSpecialCharsMinCount());
            assertEquals(1, passwordPolicy.getUppercaseCharsMinCount());
            assertEquals(1, passwordPolicy.getLowercaseCharsMinCount());
            assertEquals(1, passwordPolicy.getDigitsMinCount());
            assertEquals("^(?:(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=[^a-zA-Z/d]).*{12,})$", passwordPolicy.getRegexPattern());
            assertEquals(365, passwordPolicy.getForceExpiredPasswordChange());
            assertEquals(3, passwordPolicy.getPasswordHistory());
            assertEquals("pbkdf2-sha256", passwordPolicy.getHashAlgorithm());
            assertEquals(300, passwordPolicy.getMaxAuthAge());
        }


        @Test
        void minimal() {

            PasswordPolicies smallPasswordPolicy = KeycloakService.parsePasswordPolicies("length(12)");

            assertNotNull(smallPasswordPolicy);
            assertEquals(12, smallPasswordPolicy.getMinLength());
            assertEquals(0, smallPasswordPolicy.getMaxLength());
            assertFalse(smallPasswordPolicy.isNotUsername());
            assertFalse(smallPasswordPolicy.isNotEmail());
            assertEquals(0, smallPasswordPolicy.getSpecialCharsMinCount());
            assertEquals(0, smallPasswordPolicy.getUppercaseCharsMinCount());
            assertEquals(0, smallPasswordPolicy.getLowercaseCharsMinCount());
            assertEquals(0, smallPasswordPolicy.getDigitsMinCount());
            assertNull(smallPasswordPolicy.getRegexPattern());
            assertEquals(0, smallPasswordPolicy.getForceExpiredPasswordChange());
            assertEquals(0, smallPasswordPolicy.getPasswordHistory());
            assertNull(smallPasswordPolicy.getHashAlgorithm());
            assertEquals(0, smallPasswordPolicy.getMaxAuthAge());
        }


        @Test
        void empty() {

            PasswordPolicies emptyPasswordPolicy = KeycloakService.parsePasswordPolicies(EMPTY);

            assertNotNull(emptyPasswordPolicy);
            assertEquals(0, emptyPasswordPolicy.getMinLength());
            assertEquals(0, emptyPasswordPolicy.getMaxLength());
            assertFalse(emptyPasswordPolicy.isNotUsername());
            assertFalse(emptyPasswordPolicy.isNotEmail());
            assertEquals(0, emptyPasswordPolicy.getSpecialCharsMinCount());
            assertEquals(0, emptyPasswordPolicy.getUppercaseCharsMinCount());
            assertEquals(0, emptyPasswordPolicy.getLowercaseCharsMinCount());
            assertEquals(0, emptyPasswordPolicy.getDigitsMinCount());
            assertNull(emptyPasswordPolicy.getRegexPattern());
            assertEquals(0, emptyPasswordPolicy.getForceExpiredPasswordChange());
            assertEquals(0, emptyPasswordPolicy.getPasswordHistory());
            assertNull(emptyPasswordPolicy.getHashAlgorithm());
            assertEquals(0, emptyPasswordPolicy.getMaxAuthAge());

            PasswordPolicies nullPasswordPolicy = KeycloakService.parsePasswordPolicies(null);

            assertNotNull(nullPasswordPolicy);
            assertEquals(0, nullPasswordPolicy.getMinLength());
            assertEquals(0, nullPasswordPolicy.getMaxLength());
            assertFalse(nullPasswordPolicy.isNotUsername());
            assertFalse(nullPasswordPolicy.isNotEmail());
            assertEquals(0, nullPasswordPolicy.getSpecialCharsMinCount());
            assertEquals(0, nullPasswordPolicy.getUppercaseCharsMinCount());
            assertEquals(0, nullPasswordPolicy.getLowercaseCharsMinCount());
            assertEquals(0, nullPasswordPolicy.getDigitsMinCount());
            assertNull(nullPasswordPolicy.getRegexPattern());
            assertEquals(0, nullPasswordPolicy.getForceExpiredPasswordChange());
            assertEquals(0, nullPasswordPolicy.getPasswordHistory());
            assertNull(nullPasswordPolicy.getHashAlgorithm());
            assertEquals(0, nullPasswordPolicy.getMaxAuthAge());
        }


    }


    @Nested
    class SerializePasswordPolicies {


        @Test
        void full() {

            PasswordPolicies passwordPolicies = new PasswordPolicies();
            passwordPolicies.setMinLength(12);
            passwordPolicies.setNotUsername(true);
            passwordPolicies.setSpecialCharsMinCount(1);
            passwordPolicies.setNotEmail(true);
            passwordPolicies.setUppercaseCharsMinCount(1);
            passwordPolicies.setLowercaseCharsMinCount(1);
            passwordPolicies.setDigitsMinCount(1);
            passwordPolicies.setMaxLength(64);
            passwordPolicies.setRegexPattern("^(?:(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=[^a-zA-Z/d]).*{12,})$)");
            passwordPolicies.setForceExpiredPasswordChange(365);
            passwordPolicies.setPasswordHistory(3);
            passwordPolicies.setHashAlgorithm("pbkdf2-sha256");
            passwordPolicies.setMaxAuthAge(300);

            String result = KeycloakService.serializePasswordPolicies(passwordPolicies);

            String expected = "length(12) "
                              + "and maxLength(64) "
                              + "and notUsername(undefined) "
                              + "and notEmail(undefined) "
                              + "and specialChars(1) "
                              + "and upperCase(1) "
                              + "and lowerCase(1) "
                              + "and regexPattern(^(?:(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=[^a-zA-Z/d]).*{12,})$)) "
                              + "and digits(1) "
                              + "and hashAlgorithm(pbkdf2-sha256) "
                              + "and forceExpiredPasswordChange(365) "
                              + "and passwordHistory(3) "
                              + "and maxAuthAge(300)";

            assertNotNull(result);
            assertEquals(expected, result);
        }


        @Test
        void minimal() {

            PasswordPolicies passwordPolicies = new PasswordPolicies();
            passwordPolicies.setMinLength(12);

            String result = KeycloakService.serializePasswordPolicies(passwordPolicies);
            String expected = "length(12)";

            assertNotNull(result);
            assertEquals(expected, result);
        }


        @Test
        void empty() {

            PasswordPolicies passwordPolicies = new PasswordPolicies();

            String result = KeycloakService.serializePasswordPolicies(passwordPolicies);
            String expected = "";

            assertNotNull(result);
            assertEquals(expected, result);
        }


    }


    @Test
    void getAttribute() {

        assertEquals("0", KeycloakService.getAttribute(Map.of("key", asList("0", "1", "2")), "key"));
        assertEquals("0", KeycloakService.getAttribute(Map.of("key", singletonList("0")), "key"));

        assertNull(KeycloakService.getAttribute(Map.of("key", asList("0", "1", "2")), "badKey"));
        assertNull(KeycloakService.getAttribute(Map.of("key", emptyList()), "key"));
        assertNull(KeycloakService.getAttribute(null, "key"));
    }


    @Test
    void createListDesksSqlRequest() {

        String expected =
                """
                WITH RECURSIVE rel_tree AS (
                     SELECT
                       kr.id AS id,
                       0 AS level,
                       CAST(ARRAY[((kr.description || ' :: ') || kr.id)] AS TEXT[]) AS path_info_names,
                       CAST(ARRAY[kr.id] AS TEXT[]) AS path_info_ids,
                       kr.description AS description
                     FROM keycloak_role AS kr
                     LEFT OUTER JOIN role_attribute AS ra
                       ON ( kr.id = ra.role_id
                      AND ra.name = 'i_Parapheur_internal_parent_desk_id' )
                     WHERE ( ra.value IS NULL
                       AND CAST(kr.name AS varchar) LIKE 'tenant_tenant01_desk_%' )
                   UNION ALL
                     SELECT
                       ra.role_id AS id,
                       (p.level + 1) AS level,
                       array_append( p.path_info_names, CAST(((kr.description || ' :: ') || ra.role_id) AS TEXT) ) AS path_info_names,
                       array_append( p.path_info_ids, CAST(ra.role_id AS TEXT) ) AS path_info_ids,
                       kr.description AS description
                     FROM role_attribute AS ra
                     JOIN rel_tree AS p
                       ON p.id = ra.value
                     JOIN keycloak_role AS kr
                       ON kr.id = ra.role_id
                     WHERE ( ra.name = 'i_Parapheur_internal_parent_desk_id'
                       AND ra.value NOT IN ('1234')
                       AND CAST(kr.name AS varchar) LIKE 'tenant_tenant01_desk_%' )
                )
                SELECT
                  rt.id AS id,
                  rt.description AS description,
                  rt.level AS level,
                  count(ra.id) AS direct_children_count,
                  rt.path_info_names AS path_info_names,
                  rt.path_info_ids AS path_info_ids
                FROM rel_tree AS rt
                LEFT OUTER JOIN role_attribute AS ra
                  ON ( ra.value = rt.id
                 AND ra.name = 'i_Parapheur_internal_parent_desk_id' )
                LEFT OUTER JOIN role_attribute AS ra_shortname
                  ON ( rt.id = ra_shortname.role_id
                 AND ra_shortname.name = 'i_Parapheur_internal_short_name' )
                WHERE (
                  ( description ILIKE '%namelike%'
                    OR CAST(ra_shortname.value AS varchar) ILIKE '%namelike%'
                  )
                  AND 1 = 1
                )
                GROUP BY rt.id, description, level, path_info_names, path_info_ids
                ORDER BY path_info_names
                ASC OFFSET 100 ROWS
                FETCH NEXT 50 ROWS ONLY
                """.indent(1);

        String result = keycloakService
                .buildListDesksSqlRequest("tenant01", 2, 50,
                        false, singletonList("1234"), "namelike", null, true)
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildGetUserAdminPrivileges() {

        String expected =
                """
                SELECT
                  urm.user_id AS id
                FROM keycloak_role AS kr
                JOIN user_role_mapping AS urm
                  ON kr.id = urm.role_id
                JOIN realm AS r
                  ON (
                   kr.realm_id = r.id
                   AND r.name = 'i-Parapheur'
                  )
                WHERE (
                  kr.name LIKE 'admin'
                  AND urm.user_id IN ( 'userId01', 'userId02' )
                )
                """.indent(1);

        String result = keycloakService
                .buildFilterUsersWithPrivilege(asList("userId01", "userId02"), SUPER_ADMIN, null)
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildGetDeskNames() {
        String expected =
                """
                SELECT
                  kr.id AS id,
                  kr.description AS description
                FROM keycloak_role AS kr
                JOIN realm AS r
                  ON kr.realm_id = r.id
                WHERE (
                  r.name = 'i-Parapheur'
                  AND kr.id IN ( 'id01', 'id02' )
                )
                """.indent(1);

        String result = keycloakService
                .buildGetDeskNames(asList("id01", "id02"))
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildListUsersByIds() {
        String expected =
                """
                SELECT
                   ue.id AS id,
                   ue.username AS userName,
                   ue.first_name AS firstName,
                   ue.last_name AS lastName,
                   ue.email AS email,
                   ue.federation_link AS federation,
                   sso_table.identity_provider AS identityProvider
                 FROM
                   user_entity AS ue
                   LEFT OUTER JOIN federated_identity AS sso_table ON ue.id = sso_table.user_id
                 WHERE
                   (
                     ue.realm_id = 'i-Parapheur'
                     AND ue.id IN ( 'id01', 'id02' )
                   )
                """.indent(1);

        String result = keycloakService
                .buildListUsersByIds(asList("id01", "id02"))
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildListUsersRequest() {
        String expected =
                """
                SELECT
                  ue.id AS id,
                  ue.username AS userName,
                  ue.first_name AS firstName,
                  ue.last_name AS lastName,
                  ue.email AS email,
                  ue.federation_link AS federation,
                  sso_table.identity_provider AS identityProvider
                FROM
                  user_entity AS ue
                  LEFT OUTER JOIN federated_identity AS sso_table ON ue.id = sso_table.user_id
                WHERE (
                  (
                    upper(ue.username) LIKE upper('%searchTerm%')
                    OR upper(ue.first_name) LIKE upper('%searchTerm%')
                    OR upper(ue.last_name) LIKE upper('%searchTerm%')
                    OR upper(ue.email) LIKE upper('%searchTerm%')
                  )
                  AND ue.username NOT IN ( 'service-account-ipcore-api', 'soapui', 'rest-admin' )
                  AND ue.realm_id = 'i-Parapheur'
                )
                ORDER BY username ASC
                OFFSET 150 ROWS
                FETCH NEXT 50 ROWS ONLY
                """.indent(1);

        String result = keycloakService
                .buildListUsersRequest(USERNAME, true, "searchTerm", 3, 50)
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildSearchUserWithinGroupRequest() {
        String expected =
                """
                SELECT
                  ue.id AS id,
                  ue.username AS userName,
                  ue.first_name AS firstName,
                  ue.last_name AS lastName,
                  ue.email AS email,
                  ue.federation_link AS federation,
                  sso_table.identity_provider AS identityProvider,
                  max(CASE
                      WHEN ( kr.name = 'tenant_tenant01_admin' OR cf.value = 'tenant_tenant01_admin' ) THEN 3
                      WHEN ( kr.name = 'tenant_tenant01_functional_admin' OR cf.value = 'tenant_tenant01_functional_admin' ) THEN 2
                      WHEN ( kr.name = 'tenant_tenant01' OR cf.value = 'tenant_tenant01' ) THEN 1
                      ELSE 0
                    END) AS privilegeIndex
                FROM
                  user_entity AS ue
                  LEFT OUTER JOIN federated_identity AS sso_table ON ue.id = sso_table.user_id
                  LEFT OUTER JOIN (
                           user_role_mapping AS urm
                           JOIN keycloak_role AS kr
                           ON (
                             urm.role_id = kr.id
                             AND (
                               kr.name = 'tenant_tenant01'
                               OR kr.name = 'tenant_tenant01_admin'
                               OR kr.name = 'tenant_tenant01_functional_admin'
                             )
                           )
                  )
                  ON urm.user_id = ue.id
                LEFT OUTER JOIN (
                           component AS c
                           JOIN component_config AS cf
                           ON (
                             c.id = cf.component_id
                             AND cf.name = 'role'
                             AND cf.value = 'tenant_tenant01'
                           )
                  )
                  ON c.parent_id = ue.federation_link
                WHERE (
                  (
                    upper(ue.username) LIKE upper('%searchTerm%')
                    OR upper(ue.first_name) LIKE upper('%searchTerm%')
                    OR upper(ue.last_name) LIKE upper('%searchTerm%')
                    OR upper(ue.email) LIKE upper('%searchTerm%')
                  )
                  AND ue.username NOT IN ( 'service-account-ipcore-api', 'soapui', 'rest-admin' )
                  AND ue.realm_id = 'i-Parapheur'
                )
                GROUP BY ue.id, ue.username, ue.first_name, ue.last_name, ue.email, ue.federation_link, sso_table.identity_provider
                HAVING (
                  'tenant_tenant01' = ANY ( array_agg(kr.name) )
                  OR 'tenant_tenant01' = ANY ( array_agg(cf.value) )
                  OR 'tenant_tenant01_admin' = ANY ( array_agg(kr.name) )
                  OR 'tenant_tenant01_admin' = ANY ( array_agg(cf.value) )
                  OR 'tenant_tenant01_functional_admin' = ANY ( array_agg(kr.name) )
                  OR 'tenant_tenant01_functional_admin' = ANY ( array_agg(cf.value) )
                )
                ORDER BY username ASC
                OFFSET 150 ROWS
                FETCH NEXT 50 ROWS ONLY
                """.indent(1);

        String result = keycloakService
                .buildSearchUserWithinGroupRequest("tenant01", USERNAME, true, "searchTerm", 3, 50)
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildListUsersTotalRequest() {
        String expected =
                """
                SELECT
                  count(*)
                FROM user_entity AS ue
                WHERE (
                  upper(ue.username) LIKE upper('%searchTerm%')
                  OR upper(ue.first_name) LIKE upper('%searchTerm%')
                  OR upper(ue.last_name) LIKE upper('%searchTerm%')
                  OR upper(ue.email) LIKE upper('%searchTerm%')
                )
                """.indent(1);

        String result = keycloakService
                .buildListUsersTotalRequest("searchTerm")
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildSearchUserWithinGroupTotalRequest() {
        String expected =
                """
                SELECT
                  count(DISTINCT ue.id)
                FROM user_entity AS ue
                LEFT OUTER JOIN (
                           user_role_mapping AS urm
                           JOIN keycloak_role AS kr
                           ON (
                             urm.role_id = kr.id
                             AND (
                               kr.name = 'tenant_tenant01'
                               OR kr.name = 'tenant_tenant01_admin'
                               OR kr.name = 'tenant_tenant01_functional_admin'
                             )
                           )
                  )
                  ON urm.user_id = ue.id
                LEFT OUTER JOIN (
                           component AS c
                           JOIN component_config AS cf
                           ON (
                             c.id = cf.component_id
                             AND cf.name = 'role'
                             AND cf.value = 'tenant_tenant01'
                           )
                  )
                  ON c.parent_id = ue.federation_link
                WHERE (
                  (
                    upper(ue.username) LIKE upper('%searchTerm%')
                    OR upper(ue.first_name) LIKE upper('%searchTerm%')
                    OR upper(ue.last_name) LIKE upper('%searchTerm%')
                    OR upper(ue.email) LIKE upper('%searchTerm%')
                  )
                  AND (
                    'tenant_tenant01' = kr.name
                    OR 'tenant_tenant01' = cf.value
                    OR 'tenant_tenant01_admin' = kr.name
                    OR 'tenant_tenant01_admin' = cf.value
                    OR 'tenant_tenant01_functional_admin' = kr.name
                    OR 'tenant_tenant01_functional_admin' = cf.value
                  )
                )
                """.indent(1);

        String result = keycloakService
                .buildSearchUserWithinGroupTotalRequest("tenant01", "searchTerm")
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildTotalDesksSqlRequest() {
        String expected =
                """
                WITH RECURSIVE rel_tree AS (
                     SELECT
                       kr.id AS id,
                       kr.description AS description
                     FROM keycloak_role AS kr
                     LEFT OUTER JOIN role_attribute AS ra
                       ON ( kr.id = ra.role_id
                      AND ra.name = 'i_Parapheur_internal_parent_desk_id' )
                     WHERE ( ra.value IS NULL
                       AND CAST(kr.name AS varchar) LIKE 'tenant_tenant01_desk_%' )
                   UNION ALL
                     SELECT
                       ra.role_id AS id,
                       kr.description AS description
                     FROM role_attribute AS ra
                     JOIN rel_tree AS p
                       ON p.id = ra.value
                     JOIN keycloak_role AS kr
                       ON kr.id = ra.role_id
                     WHERE ( ra.name = 'i_Parapheur_internal_parent_desk_id'
                       AND ra.value NOT IN ('1234')
                       AND CAST(kr.name AS varchar) LIKE 'tenant_tenant01_desk_%' )
                )
                SELECT
                  count(*)
                FROM rel_tree AS rt
                WHERE description ILIKE '%nameLike%'
                """.indent(1);

        String result = keycloakService
                .buildTotalDesksSqlRequest("tenant01", false, singletonList("1234"), "nameLike")
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void buildListDeskAndDelegates() {

        String expected =
                """
                SELECT
                  keycloak_role_parent.id AS id,
                  keycloak_role_parent.description AS description,
                  keycloak_role_parent.name AS name,
                  keycloak_role_child.id AS delegating_id,
                  role_attribute_parent.value AS parent_delegation_by_metadata,
                  substring( keycloak_role_parent.name, 7, 36 ) AS tenantId
                FROM user_role_mapping
                LEFT OUTER JOIN composite_role
                  ON composite_role.composite = user_role_mapping.role_id
                JOIN keycloak_role AS keycloak_role_parent
                  ON user_role_mapping.role_id = keycloak_role_parent.id
                LEFT OUTER JOIN keycloak_role AS keycloak_role_child
                  ON composite_role.child_role = keycloak_role_child.id
                LEFT OUTER JOIN role_attribute AS role_attribute_parent
                  ON ( keycloak_role_parent.id = role_attribute_parent.role_id
                 AND role_attribute_parent.name = 'i_Parapheur_internal_typology_delegations' )
                WHERE (
                  user_role_mapping.user_id = 'user1234'
                  AND keycloak_role_parent.realm_id = 'i-Parapheur'
                  AND keycloak_role_parent.name LIKE 'tenant_%_desk_%'
                  AND 1 = 1
                )
                ORDER BY tenantId
                OFFSET 150 ROWS
                FETCH NEXT 50 ROWS ONLY
                """.indent(1);

        String result = keycloakService
                .buildListDeskAndDelegates("user1234", 3, 50, null)
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void findDeskByIdTest_withParentDesk() {
        String testParentId = "testParentId";
        String testParentName = "testParentName";

        RoleRepresentation roleMock = Mockito.mock(RoleRepresentation.class);
        RoleByIdResource roleByIdResourceMock = Mockito.mock(RoleByIdResource.class);
        Mockito.when(roleMock.getAttributes()).thenReturn(Collections.singletonMap(Desk.ATTRIBUTE_PARENT_DESK_ID, Lists.list(testParentId)));
        Mockito.when(roleMock.getName()).thenReturn("testRoleName");
        Mockito.when(roleMock.getId()).thenReturn("testRoleId");
        Mockito.when(keycloakRealmMock.rolesById()).thenReturn(roleByIdResourceMock);
        Mockito.when(roleByIdResourceMock.getRole(anyString())).thenReturn(roleMock);

        KeycloakService keycloakServiceSpy = Mockito.spy(keycloakService);
        Mockito.doReturn(Collections.singletonMap(testParentId, testParentName)).when(keycloakServiceSpy).getDeskNames(anySet());
        Mockito.doReturn("testTenantId").when(keycloakServiceSpy).getTenantIdForRoleName(anyString(), anyString());

        Desk d = keycloakServiceSpy.findDeskById(null, "cc9f4c57-173a-4c89-b53f-e465a023ae62");
        assertNotNull(d);
        assertNotNull(d.getParentDesk());
        assertEquals(d.getParentDesk().getId(), testParentId);
        assertEquals(d.getParentDesk().getName(), testParentName);

    }


    @Test
    void saveDeskWithShortName() {
        String testId = "testId";
        String testName = "testName";
        String testShortName = "testShortName";

        Tenant tenant = Tenant.builder().id("testTenantId").name("testTenantName").build();

        KeycloakService keycloakServiceSpy = Mockito.spy(keycloakService);
        KeycloakResourceService keycloakResourceServerServiceSpy = Mockito.spy(keycloakResourceServerService);

        RoleRepresentation roleMock = Mockito.mock(RoleRepresentation.class);
        Mockito.when(roleMock.getId()).thenReturn(testId);

        RoleResource roleResource = Mockito.mock(RoleResource.class);
        Mockito.when(roleResource.toRepresentation()).thenReturn(roleMock);

        RolesResource rolesResourceListMock = Mockito.mock(RolesResource.class);
        Mockito.when(rolesResourceListMock.get(Mockito.any())).thenReturn(roleResource);
        Mockito.doNothing().when(rolesResourceListMock).create(any());

        RoleByIdResource roleByIdResourceMock = Mockito.mock(RoleByIdResource.class);
        Mockito.when(keycloakRealmMock.rolesById()).thenReturn(roleByIdResourceMock);
        Mockito.when(keycloakRealmMock.roles()).thenReturn(rolesResourceListMock);
        keycloakServiceSpy.rolesResourceClient = rolesResourceListMock;

        ResourceRepresentation mockResourceRepresentation = new ResourceRepresentation();
        RolePolicyRepresentation mockRolePolicyRepresentation = new RolePolicyRepresentation();
        ScopePermissionRepresentation scopePermissionRepresentation = new ScopePermissionRepresentation();
        mockResourceRepresentation.setId(UUID.randomUUID().toString());
        mockRolePolicyRepresentation.setId(UUID.randomUUID().toString());
        scopePermissionRepresentation.setId(UUID.randomUUID().toString());
        Mockito.doNothing().when(keycloakServiceSpy).addUsersToDesk(anyString(), anyList());
        Mockito.doReturn(EMPTY).when(keycloakResourceServerServiceSpy).getServiceAccountAccessToken(anyString());
        Mockito.doReturn(scopePermissionRepresentation).when(keycloakResourceServerServiceSpy).createDeskPermission(
                anyString(), anyString(), anySet(), eq(mockResourceRepresentation), eq(mockRolePolicyRepresentation)
        );

        keycloakServiceSpy.createDesk(tenant.getId(), testName, testShortName, null, null);

        Mockito.verify(rolesResourceListMock, Mockito.times(1)).create(Mockito.any());
        Mockito.verify(roleMock, Mockito.times(1)).singleAttribute(Desk.ATTRIBUTE_SHORT_NAME, testShortName);
        Mockito.verify(roleByIdResourceMock, Mockito.times(1)).updateRole(Mockito.any(), eq(roleMock));
    }


    @Test
    void buildListDesksByShortNameSqlRequest() {

        String expected =
                """
                SELECT
                  kr.id AS id,
                  kr.description AS description
                FROM keycloak_role AS kr
                LEFT OUTER JOIN role_attribute AS ra
                  ON ( kr.id = ra.role_id AND ra.name = 'i_Parapheur_internal_short_name' )
                WHERE (
                  kr.name LIKE 'tenant_tenant01_desk_%'
                  AND lower(ra.value) = lower('shortName01')
                )
                """.indent(1);

        String result = keycloakService
                .buildListDesksByShortNameSqlRequest("tenant01", "shortName01")
                .getSQL(INLINED);

        assertEquals(
                expected.trim().replaceAll("\\s+", " "),
                result.trim().replaceAll("\\s+", " ")
        );
    }


    @Test
    void doesUserExistOnTenant() {

        // Prepare the tests

        UserRepresentation bobUser = new UserRepresentation();
        bobUser.setId(UUID.randomUUID().toString());
        bobUser.setUsername("bob");
        UserRepresentation bobbyUser = new UserRepresentation();
        bobbyUser.setId(UUID.randomUUID().toString());
        bobbyUser.setUsername("bobby");

        Mockito.when(usersResourceClient.searchByUsername(eq(bobUser.getUsername()), any()))
                .thenReturn(List.of(bobUser, bobbyUser));
        Mockito.when(usersResourceClient.searchByUsername(eq(bobbyUser.getUsername()), any()))
                .thenReturn(List.of(bobbyUser));

        String tenantId1 = UUID.randomUUID().toString();
        String tenantId2 = UUID.randomUUID().toString();

        KeycloakService keycloakServiceSpy = Mockito.spy(keycloakService);

        Mockito.doReturn(Set.of(tenantId1, tenantId2))
                .when(keycloakServiceSpy).listTenantsForUser(eq(bobUser.getId()));
        Mockito.doReturn(Set.of(tenantId1))
                .when(keycloakServiceSpy).listTenantsForUser(eq(bobbyUser.getId()));

        // Actual tests

        assertTrue(keycloakServiceSpy.doesUserExistOnTenant(bobUser.getUsername(), tenantId1));
        assertTrue(keycloakServiceSpy.doesUserExistOnTenant(bobUser.getUsername(), tenantId2));

        assertTrue(keycloakServiceSpy.doesUserExistOnTenant(bobbyUser.getUsername(), tenantId1));
        assertFalse(keycloakServiceSpy.doesUserExistOnTenant(bobbyUser.getUsername(), tenantId2));

        assertFalse(keycloakServiceSpy.doesUserExistOnTenant(bobUser.getUsername(), UUID.randomUUID().toString()));
        assertFalse(keycloakServiceSpy.doesUserExistOnTenant(bobUser.getUsername(), null));
        assertFalse(keycloakServiceSpy.doesUserExistOnTenant(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
    }


}
