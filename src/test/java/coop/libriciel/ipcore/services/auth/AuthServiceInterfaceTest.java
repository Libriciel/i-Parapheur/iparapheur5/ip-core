/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.auth;

import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.data.domain.PageImpl;

import java.util.List;

import static java.util.Collections.emptyList;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


class AuthServiceInterfaceTest {


    private static final String EXAMPLE_TENANT_ID = "tenant_01";

    private @Mock AuthServiceInterface authServiceMock = mock(AuthServiceInterface.class);


    @Test
    void findDeskByIdOrShortName() {

        // Build test cases

        when(authServiceMock.findDeskByIdOrShortName(any(), any()))
                .thenCallRealMethod();

        when(authServiceMock.findDeskByIdNoException(eq(EXAMPLE_TENANT_ID), any()))
                .thenReturn(null);

        when(authServiceMock.findDeskByIdNoException(EXAMPLE_TENANT_ID, "desk_01"))
                .thenReturn(Desk.builder().id("desk_01").shortName("d01").build());

        when(authServiceMock.findDeskByIdNoException(EXAMPLE_TENANT_ID, "desk_011"))
                .thenReturn(Desk.builder().id("desk_011").shortName("d011").build());

        when(authServiceMock.findDeskByShortName(eq(EXAMPLE_TENANT_ID), any()))
                .thenReturn(new PageImpl<>(emptyList()));

        when(authServiceMock.findDeskByShortName(EXAMPLE_TENANT_ID, "d01"))
                .thenReturn(new PageImpl<>(List.of(
                        new DeskRepresentation("desk_011", "Desk 011, with overlapping attributes"),
                        new DeskRepresentation("desk_01", "Desk 01, the expected one")
                )));

        // Tests

        assertNotNull(authServiceMock.findDeskByIdOrShortName(EXAMPLE_TENANT_ID, "desk_01"));
        assertNotNull(authServiceMock.findDeskByIdOrShortName(EXAMPLE_TENANT_ID, "d01"));

        assertNull(authServiceMock.findDeskByIdOrShortName(EXAMPLE_TENANT_ID, "desk_non_existing_key_or_short_name"));
        assertNull(authServiceMock.findDeskByIdOrShortName(EXAMPLE_TENANT_ID, null));
        assertNull(authServiceMock.findDeskByIdOrShortName(EXAMPLE_TENANT_ID, EMPTY));
        assertNull(authServiceMock.findDeskByIdOrShortName(EMPTY, EMPTY));
    }


}
