/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.workflow;

import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.requests.UserRepresentation;
import coop.libriciel.ipcore.model.database.*;
import coop.libriciel.ipcore.model.workflow.*;
import coop.libriciel.ipcore.model.workflow.Task.ExternalState;
import coop.libriciel.ipcore.utils.TextUtils;
import coop.libriciel.workflow.api.model.Instance;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;

import java.time.Instant;
import java.util.*;
import java.util.regex.Matcher;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static coop.libriciel.ipcore.services.workflow.IpWorkflowService.*;
import static coop.libriciel.ipcore.utils.TextUtils.ISO8601_DATE_TIME_FORMAT;
import static java.time.ZoneId.systemDefault;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.*;


class IpWorkflowServiceTest {


    private final ModelMapper modelMapper = IpWorkflowService.initializeInternalModelMapper();


    @Test
    void matchBossOfMetadataPatterns() {

        String bossOfMetadataKey = "i_Parapheur_internal_validation_boss_of_desk_id_1";
        String bossOfIdPlaceHolder = String.format("${%s}", bossOfMetadataKey);
        Matcher matcher = META_BOSSOF_PATTERN.matcher(bossOfIdPlaceHolder);
        assertTrue(matcher.matches());
        assertEquals(bossOfMetadataKey, matcher.group(1));
    }


    @Test
    void matchVariableDeskMetadataPatterns() {

        String variableDeskMetadataKey = "i_Parapheur_internal_validation_variable_desk_id_1";
        String variableDeskIdPlaceHolder = String.format("${%s}", variableDeskMetadataKey);
        Matcher matcher = META_VARIABLEDESK_PATTERN.matcher(variableDeskIdPlaceHolder);
        assertTrue(matcher.matches());
        assertEquals(variableDeskMetadataKey, matcher.group(1));
    }


    @Nested
    class FolderToWorkflowInstanceTest {


        private static void assertDeepEquals(Instance libEntity, Folder entity) {
            // assertEquals(entity.getId(), entity.getId()); // This is stuck, due to a readonly parameter, but it is mapped anyway
            assertEquals(IpWorkflowService.toDate(libEntity.getDueDate()), entity.getDueDate());
            assertEquals(libEntity.getName(), entity.getName());
            assertEquals(libEntity.getBusinessKey(), entity.getContentId());

            assertEquals(CollectionUtils.size(libEntity.getTaskList()), CollectionUtils.size(entity.getStepList()));
            if (libEntity.getTaskList() != null) {
                assertNotNull(entity.getStepList());
                IntStream.range(0, CollectionUtils.size(libEntity.getTaskList()))
                        .forEach(index -> TaskToLibTaskTest.assertDeepEquals(libEntity.getTaskList().get(index), entity.getStepList().get(index)));
            }

            assertEquals(
                    Optional.ofNullable(libEntity.getTaskList())
                            .map(list -> list.stream().map(coop.libriciel.workflow.api.model.Task::getId).toList())
                            .orElse(null),
                    Optional.ofNullable(entity.getStepList())
                            .map(list -> list.stream().map(Task::getId).toList())
                            .orElse(null)
            );
            assertEquals(
                    Optional.ofNullable(libEntity.getTaskList())
                            .map(list -> list.stream()
                                    .map(coop.libriciel.workflow.api.model.Task::getExpectedAction)
                                    .map(IpWorkflowService::toCoreValue)
                                    .toList())
                            .orElse(null),
                    Optional.ofNullable(entity.getStepList())
                            .map(list -> list.stream().map(Task::getAction).toList())
                            .orElse(null)
            );
            assertEquals(libEntity.getOriginGroup(), Optional.ofNullable(entity.getOriginDesk()).map(DeskRepresentation::getId).orElse(null));
            assertEquals(libEntity.getFinalGroup(), Optional.ofNullable(entity.getFinalDesk()).map(DeskRepresentation::getId).orElse(null));
            assertEquals(MapUtils.getString(libEntity.getVariables(), META_PREMIS_NODE_ID), entity.getPremisNodeId());
            assertEquals(Optional.ofNullable(entity.getType()).map(TypologyEntity::getTenant).map(Tenant::getId).orElse(null), libEntity.getTenantId());
            // assertEquals(IpWorkflowService.toCoreValue(libEntity.getState()), entity.getState());  This one is actually computed
            assertEquals(
                    Optional.ofNullable(entity.getType()).map(Type::getId).orElse(null),
                    MapUtils.getString(libEntity.getVariables(), META_TYPE_ID)
            );
            assertEquals(entity.getCreationWorkflowDefinitionKey(), libEntity.getCreationWorkflowKey());
            assertEquals(entity.getValidationWorkflowDefinitionKey(), libEntity.getValidationWorkflowKey());
            assertEquals(
                    Optional.ofNullable(entity.getSubtype()).map(Subtype::getId).orElse(null),
                    MapUtils.getString(libEntity.getVariables(), META_SUBTYPE_ID)
            );
            assertEquals(
                    libEntity.getVariables().entrySet().stream()
                            .filter(e -> !StringUtils.equals(e.getKey(), META_TYPE_ID))
                            .filter(e -> !StringUtils.equals(e.getKey(), META_SUBTYPE_ID))
                            .filter(e -> !StringUtils.equals(e.getKey(), META_PREMIS_NODE_ID))
                            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)),
                    entity.getMetadata().entrySet().stream()
                            .filter(e -> !StringUtils.equals(e.getKey(), META_TYPE_ID))
                            .filter(e -> !StringUtils.equals(e.getKey(), META_SUBTYPE_ID))
                            .filter(e -> !StringUtils.equals(e.getKey(), META_PREMIS_NODE_ID))
                            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue))
            );
        }


        @Test
        void convertLibEntityToEntity_full() {

            var innerTask1 = new coop.libriciel.workflow.api.model.Task();
            innerTask1.setId(UUID.randomUUID().toString());
            innerTask1.setExpectedAction(coop.libriciel.workflow.api.model.Action.START);
            innerTask1.setPerformedAction(coop.libriciel.workflow.api.model.Action.START);
            innerTask1.setDate(IpWorkflowService.toOffsetDateTime(new Date(new Random().nextLong(999999999))));

            var innerTask2 = new coop.libriciel.workflow.api.model.Task();
            innerTask2.setId(UUID.randomUUID().toString());
            innerTask2.setExpectedAction(coop.libriciel.workflow.api.model.Action.EXTERNAL_SIGNATURE);
            innerTask2.setPerformedAction(coop.libriciel.workflow.api.model.Action.REJECT);
            innerTask2.setDate(IpWorkflowService.toOffsetDateTime(new Date(new Random().nextLong(999999999))));

            Instance libEntity = new Instance(UUID.randomUUID().toString(), null, null, null, List.of(innerTask1, innerTask2), null);
            libEntity.setName(RandomStringUtils.insecure().nextAlphanumeric(30));
            libEntity.setBusinessKey(UUID.randomUUID().toString());
            libEntity.setDueDate(IpWorkflowService.toOffsetDateTime(new Date(new Random().nextLong(999999999))));
            libEntity.setOriginGroup(UUID.randomUUID().toString());
            libEntity.setFinalGroup(UUID.randomUUID().toString());
            libEntity.setCreationWorkflowKey(RandomStringUtils.insecure().nextAlphabetic(64));
            libEntity.setValidationWorkflowKey(RandomStringUtils.insecure().nextAlphabetic(64));
            libEntity.setState(coop.libriciel.workflow.api.model.State.FINISHED);
            libEntity.setVariables(Map.of(
                    META_TYPE_ID, UUID.randomUUID().toString(),
                    META_SUBTYPE_ID, UUID.randomUUID().toString(),
                    META_PREMIS_NODE_ID, UUID.randomUUID().toString(),
                    UUID.randomUUID().toString(), RandomStringUtils.insecure().nextAlphanumeric(30),
                    UUID.randomUUID().toString(), RandomStringUtils.insecure().nextAlphanumeric(30)
            ));

            Folder entity = modelMapper.map(libEntity, Folder.class);
            assertNotNull(entity);
            assertDeepEquals(libEntity, entity);
        }


        @Test
        void convertLibEntityToEntity_empty() {
            Instance libEntity = new Instance();
            Folder entity = modelMapper.map(libEntity, Folder.class);
            assertNotNull(entity);
            assertDeepEquals(libEntity, entity);
        }


        @Test
        void convertEntityToLibEntity_full() {

            Tenant innerTenant = new Tenant();
            innerTenant.setId(UUID.randomUUID().toString());

            Type innerType = new Type();
            innerType.setTenant(innerTenant);
            innerType.setId(UUID.randomUUID().toString());

            Subtype innerSubtype = new Subtype();
            innerSubtype.setTenant(innerTenant);
            innerSubtype.setId(UUID.randomUUID().toString());
            innerSubtype.setParentType(innerType);
            innerSubtype.setCreationWorkflowId(UUID.randomUUID().toString());
            innerSubtype.setValidationWorkflowId(UUID.randomUUID().toString());

            Folder instance = new Folder();
            instance.setId(UUID.randomUUID().toString());
            instance.setName(RandomStringUtils.insecure().nextAlphanumeric(30));
            instance.setContentId(UUID.randomUUID().toString());
            instance.setPremisNodeId(UUID.randomUUID().toString());
            instance.setDueDate(new Date(new Random().nextLong(999999999)));
            instance.setOriginDesk(new DeskRepresentation(UUID.randomUUID().toString()));
            instance.setFinalDesk(new DeskRepresentation(UUID.randomUUID().toString()));
            instance.setType(innerType);
            instance.setSubtype(innerSubtype);
            instance.setState(State.FINISHED);
            instance.setMetadata(Map.of(
                    UUID.randomUUID().toString(), RandomStringUtils.insecure().nextAlphanumeric(30),
                    UUID.randomUUID().toString(), RandomStringUtils.insecure().nextAlphanumeric(30)
            ));

            Instance libEntity = modelMapper.map(instance, Instance.class);
            assertNotNull(libEntity);
            assertDeepEquals(libEntity, instance);
        }


        @Test
        void convertEntityToLibEntity_empty() {
            Folder entity = new Folder();
            Instance libEntity = modelMapper.map(entity, Instance.class);
            assertNotNull(libEntity);
            assertDeepEquals(libEntity, entity);
        }


    }


    @Nested
    class StepDefinitionToLibStepDefinitionTest {


        private static void assertDeepEquals(coop.libriciel.workflow.api.model.StepDefinition libEntity, StepDefinition coreObject) {
            assertEquals(libEntity.getType(), IpWorkflowService.toIpWorkflowValue(coreObject.getType()));
            assertEquals(libEntity.getParallelType(), IpWorkflowService.toIpWorkflowValue(coreObject.getParallelType()));
            assertEquals(
                    libEntity.getValidatingDeskIds(),
                    Optional.ofNullable(coreObject.getValidatingDesks())
                            .map(list -> list.stream().map(DeskRepresentation::getId).toList())
                            .orElse(null)
            );
            assertEquals(
                    libEntity.getNotifiedDeskIds(),
                    Optional.ofNullable(coreObject.getNotifiedDesks())
                            .map(list -> list.stream().map(DeskRepresentation::getId).toList())
                            .orElse(null)
            );
            assertEquals(
                    libEntity.getMandatoryValidationMetadataIds(),
                    Optional.ofNullable(coreObject.getMandatoryValidationMetadata())
                            .map(list -> list.stream().map(Metadata::getId).toList())
                            .orElse(null)
            );
            assertEquals(
                    libEntity.getMandatoryRejectionMetadataIds(),
                    Optional.ofNullable(coreObject.getMandatoryRejectionMetadata())
                            .map(list -> list.stream().map(Metadata::getId).toList())
                            .orElse(null)
            );
        }


        @Test
        void convertLibEntityToEntity_full() {

            var libEntity = new coop.libriciel.workflow.api.model.StepDefinition();

            libEntity.setType(coop.libriciel.workflow.api.model.Action.SIGNATURE);
            libEntity.setParallelType(coop.libriciel.workflow.api.model.ParallelType.AND);
            libEntity.setValidatingDeskIds(singletonList(UUID.randomUUID().toString()));
            libEntity.setNotifiedDeskIds(singletonList(UUID.randomUUID().toString()));
            libEntity.setMandatoryValidationMetadataIds(singletonList(UUID.randomUUID().toString()));
            libEntity.setMandatoryRejectionMetadataIds(singletonList(UUID.randomUUID().toString()));

            StepDefinition entity = modelMapper.map(libEntity, StepDefinition.class);
            assertNotNull(entity);
            assertDeepEquals(libEntity, entity);
        }


        @Test
        void convertLibEntityToEntity_empty() {

            var libEntity = new coop.libriciel.workflow.api.model.StepDefinition();
            StepDefinition entity = modelMapper.map(libEntity, StepDefinition.class);
            assertNotNull(entity);
            assertDeepEquals(libEntity, entity);
        }


        @Test
        void convertEntityToLibEntity_full() {

            StepDefinition entity = new StepDefinition();
            entity.setType(Action.SIGNATURE);
            entity.setParallelType(StepDefinitionParallelType.AND);
            entity.setValidatingDesks(singletonList(new DeskRepresentation(UUID.randomUUID().toString())));
            entity.setNotifiedDesks(singletonList(new DeskRepresentation(UUID.randomUUID().toString())));
            entity.setMandatoryValidationMetadata(singletonList(new Metadata(UUID.randomUUID().toString())));
            entity.setMandatoryRejectionMetadata(singletonList(new Metadata(UUID.randomUUID().toString())));

            var libEntity = modelMapper.map(entity, coop.libriciel.workflow.api.model.StepDefinition.class);
            assertNotNull(libEntity);
            assertDeepEquals(libEntity, entity);
        }


        @Test
        void convertEntityToLibEntity_empty() {

            StepDefinition entity = new StepDefinition();

            var libEntity = modelMapper.map(entity, coop.libriciel.workflow.api.model.StepDefinition.class);
            assertNotNull(libEntity);
            assertDeepEquals(libEntity, entity);
        }


    }


    @Nested
    class WorkflowDefinitionToLibWorkflowDefinitionTest {


        private static void assertDeepEquals(coop.libriciel.workflow.api.model.WorkflowDefinition libEntity, WorkflowDefinition coreObject) {

            assertEquals(libEntity.getId(), coreObject.getId());
            assertEquals(libEntity.getName(), coreObject.getName());
            assertEquals(CollectionUtils.size(libEntity.getSteps()), CollectionUtils.size(coreObject.getSteps()));
            assertEquals(libEntity.getFinalDeskId(), Optional.ofNullable(coreObject.getFinalDesk()).map(DeskRepresentation::getId).orElse(null));
            assertEquals(
                    libEntity.getFinalNotifiedDeskIds(),
                    Optional.ofNullable(coreObject.getFinalNotifiedDesks())
                            .map(list -> list.stream().map(DeskRepresentation::getId).toList())
                            .orElse(null)
            );
        }


        @Test
        void convertLibEntityToEntity_full() {

            var libEntity = new coop.libriciel.workflow.api.model.WorkflowDefinition();
            libEntity.setId(UUID.randomUUID().toString());
            libEntity.setKey(RandomStringUtils.insecure().nextAlphanumeric(30));
            libEntity.setName(RandomStringUtils.insecure().nextAlphanumeric(30));
            libEntity.setSteps(emptyList());
            libEntity.setFinalDeskId(UUID.randomUUID().toString());
            libEntity.setFinalNotifiedDeskIds(singletonList(UUID.randomUUID().toString()));

            WorkflowDefinition entity = modelMapper.map(libEntity, WorkflowDefinition.class);
            assertNotNull(entity);
            assertDeepEquals(libEntity, entity);
        }


        @Test
        void convertLibEntityToEntity_empty() {

            var libEntity = new coop.libriciel.workflow.api.model.WorkflowDefinition();
            WorkflowDefinition entity = modelMapper.map(libEntity, WorkflowDefinition.class);
            assertNotNull(entity);
            assertDeepEquals(libEntity, entity);
        }


        @Test
        void convertEntityToLibEntity_full() {

            WorkflowDefinition entity = new WorkflowDefinition();
            entity.setId(UUID.randomUUID().toString());
            entity.setKey(RandomStringUtils.insecure().nextAlphanumeric(30));
            entity.setName(RandomStringUtils.insecure().nextAlphanumeric(30));
            entity.setSteps(emptyList());
            entity.setFinalDesk(new DeskRepresentation(UUID.randomUUID().toString()));
            entity.setFinalNotifiedDesks(singletonList(new DeskRepresentation(UUID.randomUUID().toString())));

            var libEntity = modelMapper.map(entity, coop.libriciel.workflow.api.model.WorkflowDefinition.class);
            assertNotNull(libEntity);
            assertDeepEquals(libEntity, entity);
        }


        @Test
        void convertEntityToLibEntity_empty() {

            WorkflowDefinition entity = new WorkflowDefinition();

            var libEntity = modelMapper.map(entity, coop.libriciel.workflow.api.model.WorkflowDefinition.class);
            assertNotNull(libEntity);
            assertDeepEquals(libEntity, entity);
        }


    }


    @Nested
    class TaskToLibTaskTest {


        static void assertDeepEquals(coop.libriciel.workflow.api.model.Task libEntity, Task coreObject) {

            assertEquals(libEntity.getId(), coreObject.getId());
            assertEquals(IpWorkflowService.toCoreValue(libEntity.getExpectedAction()), coreObject.getAction());
            assertEquals(IpWorkflowService.toCoreValue(libEntity.getPerformedAction()), coreObject.getPerformedAction());
            // assertEquals(IpWorkflowService.toCoreValue(libEntity.getState()), coreObject.getState()); This one is actually computed
            assertEquals(IpWorkflowService.toCoreValue(libEntity.getExternalState()), coreObject.getExternalState());
            assertEquals(
                    libEntity.getCandidateGroups(),
                    Optional.ofNullable(coreObject.getDesks())
                            .map(list -> list.stream().map(DeskRepresentation::getId).toList())
                            .orElse(null)
            );
            assertEquals(libEntity.getDelegatedByGroupId(), Optional.ofNullable(coreObject.getDelegatedByDesk()).map(DeskRepresentation::getId).orElse(null));
            assertEquals(libEntity.getAssignee(), Optional.ofNullable(coreObject.getUser()).map(UserRepresentation::getId).orElse(null));
            assertEquals(libEntity.getReadByUserIds(), coreObject.getReadByUserIds());
            assertEquals(MapUtils.getString(libEntity.getVariables(), METADATA_PUBLIC_CERTIFICATE_BASE64), coreObject.getPublicCertificateBase64());
            assertEquals(MapUtils.getString(libEntity.getVariables(), METADATA_TRANSACTION_ID), coreObject.getExternalSignatureProcedureId());
            assertEquals(MapUtils.getString(libEntity.getVariables(), METADATA_PUBLIC_ANNOTATION), coreObject.getPublicAnnotation());
            assertEquals(MapUtils.getString(libEntity.getVariables(), METADATA_PRIVATE_ANNOTATION), coreObject.getPrivateAnnotation());
            assertEquals(IpWorkflowService.toDate(libEntity.getBeginDate()), coreObject.getBeginDate());
            assertEquals(IpWorkflowService.toDate(libEntity.getDate()), coreObject.getDate());
            assertEquals(
                    Optional.ofNullable(libEntity.getVariables())
                            .map(map -> map.get(METADATA_DRAFT_CREATION_DATE))
                            .map(stringDate -> TextUtils.deserializeDate(stringDate, ISO8601_DATE_TIME_FORMAT))
                            .map(Date::getTime)
                            .orElse(null),
                    Optional.ofNullable(coreObject.getDraftCreationDate())
                            .map(Date::getTime)
                            .orElse(null)
            );
            assertEquals(
                    libEntity.getNotifiedGroups(),
                    Optional.ofNullable(coreObject.getNotifiedDesks())
                            .map(list -> list.stream().map(DeskRepresentation::getId).toList())
                            .orElse(null)
            );
            assertEquals(libEntity.getWorkflowIndex(), coreObject.getWorkflowIndex());
            assertEquals(libEntity.getStepIndex(), coreObject.getStepIndex());
            assertEquals(libEntity.getMandatoryValidationMetadata(), coreObject.getMandatoryValidationMetadata());
            assertEquals(libEntity.getMandatoryRejectionMetadata(), coreObject.getMandatoryRejectionMetadata());
        }


        @Test
        void convertLibEntityToEntity_full() {

            var libEntity = new coop.libriciel.workflow.api.model.Task();
            libEntity.setExpectedAction(coop.libriciel.workflow.api.model.Action.EXTERNAL_SIGNATURE);
            libEntity.setPerformedAction(coop.libriciel.workflow.api.model.Action.REJECT);
            libEntity.setState(coop.libriciel.workflow.api.model.State.REJECTED);
            libEntity.setExternalState(coop.libriciel.workflow.api.model.ExternalState.REFUSED);
            libEntity.setCandidateGroups(List.of(UUID.randomUUID().toString()));
            libEntity.setDelegatedByGroupId(UUID.randomUUID().toString());
            libEntity.setAssignee(UUID.randomUUID().toString());
            libEntity.setReadByUserIds(Set.of(UUID.randomUUID().toString()));
            libEntity.setBeginDate(Instant.ofEpochMilli(new Random().nextLong(999999)).atZone(systemDefault()).toOffsetDateTime());
            libEntity.setDate(IpWorkflowService.toOffsetDateTime(new Date(new Random().nextLong(999999))));
            libEntity.setVariables(Map.of(
                    METADATA_PUBLIC_CERTIFICATE_BASE64, RandomStringUtils.insecure().nextAlphanumeric(500),
                    METADATA_TRANSACTION_ID, UUID.randomUUID().toString(),
                    METADATA_DRAFT_CREATION_DATE, TextUtils.serializeDate(new Date(new Random().nextLong(999999)), ISO8601_DATE_TIME_FORMAT),
                    METADATA_PUBLIC_ANNOTATION, RandomStringUtils.insecure().nextAlphanumeric(60),
                    METADATA_PRIVATE_ANNOTATION, RandomStringUtils.insecure().nextAlphanumeric(60),
                    UUID.randomUUID().toString(), RandomStringUtils.insecure().nextAlphanumeric(60),
                    UUID.randomUUID().toString(), RandomStringUtils.insecure().nextAlphanumeric(60)
            ));
            libEntity.setNotifiedGroups(List.of(UUID.randomUUID().toString()));
            libEntity.setWorkflowIndex(new Random().nextLong());
            libEntity.setStepIndex(new Random().nextLong());
            libEntity.setMandatoryValidationMetadata(List.of(UUID.randomUUID().toString()));
            libEntity.setMandatoryRejectionMetadata(List.of(UUID.randomUUID().toString()));

            Task entity = modelMapper.map(libEntity, Task.class);
            assertNotNull(entity);
            assertDeepEquals(libEntity, entity);
        }


        @Test
        void convertLibEntityToEntity_empty() {

            var libEntity = new coop.libriciel.workflow.api.model.Task();
            libEntity.setVariables(null);
            Task entity = modelMapper.map(libEntity, Task.class);
            assertNotNull(entity);
            assertDeepEquals(libEntity, entity);
        }


        @Test
        void convertEntityToLibEntity_full() {

            Task entity = new Task();
            entity.setAction(Action.EXTERNAL_SIGNATURE);
            entity.setPerformedAction(Action.REJECT);
            entity.setState(State.REJECTED);
            entity.setExternalState(ExternalState.REFUSED);
            entity.setDesks(List.of(new DeskRepresentation(UUID.randomUUID().toString())));
            entity.setDelegatedByDesk(new DeskRepresentation(UUID.randomUUID().toString()));
            entity.setUser(new UserRepresentation(UUID.randomUUID().toString()));
            entity.setReadByUserIds(Set.of(UUID.randomUUID().toString()));
            entity.setBeginDate(new Date(new Random().nextLong(999999)));
            entity.setDate(new Date(new Random().nextLong(999999)));
            entity.setDraftCreationDate(new Date(new Random().nextLong(999999)));
            entity.setMetadata(Map.of(
                    UUID.randomUUID().toString(), RandomStringUtils.insecure().nextAlphanumeric(60),
                    UUID.randomUUID().toString(), RandomStringUtils.insecure().nextAlphanumeric(60)
            ));
            entity.setExternalSignatureProcedureId(UUID.randomUUID().toString());
            entity.setPublicCertificateBase64(RandomStringUtils.insecure().nextAlphanumeric(500));
            entity.setPublicAnnotation(RandomStringUtils.insecure().nextAlphanumeric(60));
            entity.setPrivateAnnotation(RandomStringUtils.insecure().nextAlphanumeric(60));
            entity.setNotifiedDesks(List.of(new DeskRepresentation(UUID.randomUUID().toString())));
            entity.setWorkflowIndex(new Random().nextLong());
            entity.setStepIndex(new Random().nextLong());
            entity.setMandatoryValidationMetadata(List.of(UUID.randomUUID().toString()));
            entity.setMandatoryRejectionMetadata(List.of(UUID.randomUUID().toString()));

            var libEntity = modelMapper.map(entity, coop.libriciel.workflow.api.model.Task.class);
            assertNotNull(libEntity);
            assertDeepEquals(libEntity, entity);
        }


        @Test
        void convertEntityToLibEntity_empty() {

            Task entity = new Task();
            entity.setState(null);

            var libEntity = modelMapper.map(entity, coop.libriciel.workflow.api.model.Task.class);
            assertNotNull(libEntity);
            assertDeepEquals(libEntity, entity);
        }


    }


}
