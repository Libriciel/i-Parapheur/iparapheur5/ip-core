/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.filetransfer;

import coop.libriciel.ipcore.model.filetransfer.FolderUnpackedContent;
import coop.libriciel.ipcore.model.workflow.Folder;
import org.jetbrains.annotations.NotNull;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.io.IOException;

import static coop.libriciel.ipcore.utils.TextUtilsTest.DUMMY_SERVICE;
import static org.apache.commons.lang3.StringUtils.EMPTY;


@Service(FileTransferServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = FileTransferServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = DUMMY_SERVICE, matchIfMissing = true)
public class DummyFileTransferService implements FileTransferServiceInterface {

    @Override
    public @NotNull String buildRetrievalUrl(String baseUrl, String fileName) {
        return EMPTY;
    }


    @Override
    public String postFolder(@NotNull Folder folder, String targetName) {
        return "";
    }


    @Override
    public FolderUnpackedContent getFolderFilesFromUrl(@NotNull String url) throws IOException {
        return null;
    }

}
