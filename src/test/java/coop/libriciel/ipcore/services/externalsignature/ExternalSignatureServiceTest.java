/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.externalsignature;

import coop.libriciel.external.signature.connector.api.model.FileWrapper;
import coop.libriciel.external.signature.connector.api.model.PdfVersion;
import coop.libriciel.iparapheur.customalfrescotengines.model.crypto.PdfSignaturePosition;
import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.content.ValidatedSignatureInformation;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;

import java.util.*;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;


class ExternalSignatureServiceTest {


    @Test
    void convertToLibPdfVersion() {

        assertEquals(PdfVersion.V1_0, ExternalSignatureService.convertToLibPdfVersion(1.0f));
        assertEquals(PdfVersion.V1_1, ExternalSignatureService.convertToLibPdfVersion(1.1f));
        assertEquals(PdfVersion.V1_2, ExternalSignatureService.convertToLibPdfVersion(1.2f));
        assertEquals(PdfVersion.V1_3, ExternalSignatureService.convertToLibPdfVersion(1.3f));
        assertEquals(PdfVersion.V1_4, ExternalSignatureService.convertToLibPdfVersion(1.4f));
        assertEquals(PdfVersion.V1_5, ExternalSignatureService.convertToLibPdfVersion(1.5f));
        assertEquals(PdfVersion.V1_6, ExternalSignatureService.convertToLibPdfVersion(1.6f));
        assertEquals(PdfVersion.V1_7, ExternalSignatureService.convertToLibPdfVersion(1.7f));
        assertEquals(PdfVersion.V2_0, ExternalSignatureService.convertToLibPdfVersion(2.0f));

        assertNull(ExternalSignatureService.convertToLibPdfVersion(null));
        assertNull(ExternalSignatureService.convertToLibPdfVersion(1.8f));
        assertNull(ExternalSignatureService.convertToLibPdfVersion(3.0f));
        assertNull(ExternalSignatureService.convertToLibPdfVersion(-1345648f));
    }


    @Nested
    class DocumentToFileWrapper {


        private static void assertDeepEquals(FileWrapper libEntity, Document coreObject) {

            assertEquals(coreObject.getId(), libEntity.getId());
            assertEquals(coreObject.getName(), libEntity.getName());
            assertEquals(Objects.nonNull(coreObject.getMediaVersion()), Objects.nonNull(libEntity.getPdfVersion()));
            assertEquals(Objects.nonNull(coreObject.getEmbeddedSignatureInfos()), Objects.nonNull(libEntity.getEmbeddedSignatures()));

            if (coreObject.getEmbeddedSignatureInfos() != null) {
                assertNotNull(libEntity.getEmbeddedSignatures());
                assertEquals(coreObject.getEmbeddedSignatureInfos().size(), libEntity.getEmbeddedSignatures().size());

                IntStream
                        .range(0, coreObject.getEmbeddedSignatureInfos().size())
                        .forEach(index -> {
                            ValidatedSignatureInformation coreSignature = coreObject.getEmbeddedSignatureInfos().get(index);
                            coop.libriciel.external.signature.connector.api.model.SignatureInfo libSignature = libEntity.getEmbeddedSignatures().get(index);
                            assertEquals(coreSignature.getPrincipalIssuer(), libSignature.getName());
                            assertEquals(coreSignature.getPrincipalSubjectIssuer(), libSignature.getIssuerName());
                        });
            }
        }


        @Test
        void convertEntityToLibEntity_full() {
            ModelMapper modelMapper = ExternalSignatureService.initializeInternalModelMapper();

            Document document = new Document();
            document.setId(UUID.randomUUID().toString());
            document.setName(RandomStringUtils.insecure().nextAlphanumeric(30));
            document.setMediaVersion(1.7f);
            document.setEmbeddedSignatureInfos(List.of(
                    new ValidatedSignatureInformation(
                            new Random().nextLong(),
                            new PdfSignaturePosition(),
                            RandomStringUtils.insecure().nextAlphanumeric(30),
                            RandomStringUtils.insecure().nextAlphanumeric(30),
                            true,
                            new Random().nextLong(),
                            new Random().nextLong(),
                            RandomStringUtils.insecure().nextAlphanumeric(30),
                            RandomStringUtils.insecure().nextAlphanumeric(30)
                    ),
                    new ValidatedSignatureInformation(
                            new Random().nextLong(),
                            new PdfSignaturePosition(),
                            RandomStringUtils.insecure().nextAlphanumeric(30),
                            RandomStringUtils.insecure().nextAlphanumeric(30),
                            true,
                            new Random().nextLong(),
                            new Random().nextLong(),
                            RandomStringUtils.insecure().nextAlphanumeric(30),
                            RandomStringUtils.insecure().nextAlphanumeric(30)
                    )
            ));

            FileWrapper fileWrapper = modelMapper.map(document, FileWrapper.class);
            assertDeepEquals(fileWrapper, document);
        }


        @Test
        void convertEntityToLibEntity_empty() {

            ModelMapper modelMapper = ExternalSignatureService.initializeInternalModelMapper();
            Document document = new Document();

            FileWrapper fileWrapper = modelMapper.map(document, FileWrapper.class);
            assertDeepEquals(fileWrapper, document);
        }


    }


}
