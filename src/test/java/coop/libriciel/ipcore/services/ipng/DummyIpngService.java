/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.ipng;

import coop.libriciel.ipcore.model.ipng.*;
import org.jetbrains.annotations.NotNull;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import static coop.libriciel.ipcore.utils.TextUtilsTest.DUMMY_SERVICE;


@Service(IpngServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = IpngServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = DUMMY_SERVICE)
public class DummyIpngService implements IpngServiceInterface {


    @Override
    public @NotNull List<IpngEntity> listAllEntities() {
        return new ArrayList<>();
    }


    @Override
    public @NotNull IpngEntity getEntity(@NotNull String entityId) {
        return new IpngEntity("id", new HashMap<>(), "urlInstance", "affiliation", true, "publicKey", new ArrayList<>());
    }


    @Override
    public @NotNull Deskbox getDeskBox(@NotNull String entityId, @NotNull String id) {
        return new Deskbox("id",
                new HashMap<>(),
                "entityId",
                new HashMap<>(),
                "urlInstance",
                "mail",
                false);
    }


    @Override
    public @NotNull IpngMetadataList getLatestMetadataList(String token) {
        return new IpngMetadataList();
    }


    @Override
    public void postProof(@NotNull IpngProof ipngProof, String token, Consumer<IpngProofResult> successCallback) {

    }


    @Override
    public @NotNull IpngProofResult postProof(@NotNull IpngProof ipngProof, String token) {
        return new IpngProofResult("id",
                "receiverEncryptedFolder",
                "receiverEntityId",
                "senderEncryptedFolder",
                "senderEntityId");
    }


    @Override
    public @NotNull IpngProofWrap getProof(String proofId) {
        return new IpngProofWrap();
    }


    @Override
    public @NotNull IpngTypology getLatestTypology(String token) {
        return new IpngTypology();
    }


    @Override
    public void handleRedisMessageMap(Map<String, String> msgContent) {

    }


}
