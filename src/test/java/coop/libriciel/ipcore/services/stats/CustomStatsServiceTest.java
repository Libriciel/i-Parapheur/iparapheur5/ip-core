/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.stats;

import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.database.Type;
import coop.libriciel.ipcore.model.workflow.Folder;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.util.Map;
import java.util.regex.Pattern;

import static coop.libriciel.ipcore.model.stats.StatsCategory.USER;
import static coop.libriciel.ipcore.model.workflow.Action.*;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
@ExtendWith(SpringExtension.class)
@ActiveProfiles("customStats")
public class CustomStatsServiceTest {


    private @Autowired StatsServiceInterface statsService;
    private static MockWebServer mockBackEnd;


    @BeforeEach
    void initialize() throws IOException {

        mockBackEnd = new MockWebServer();
        mockBackEnd.start();

        CustomStatsService customStatsService = (CustomStatsService) statsService;
        customStatsService.getProperties().setUrl("localhost");
        customStatsService.getProperties().setPort(mockBackEnd.getPort());
    }


    @AfterEach
    void tearDown() throws Exception {
        mockBackEnd.shutdown();
    }


    @Test
    void registerFolderAction() throws Exception {

        // Build up test case

        Tenant tenant = new Tenant();
        tenant.setId("t01");
        tenant.setName("Tenant 01");

        Folder folder = new Folder();
        folder.setId("f01");
        folder.setName("Folder 01");
        folder.setType(Type.builder().tenant(tenant).id("type01").name("Type 01").build());
        folder.setSubtype(Subtype.builder()
                .tenant(tenant).id("subtype01").name("Subtype 01")
                .isDigitalSignatureMandatory(true).isReadingMandatory(true).isMultiDocuments(true).build());
        folder.setMetadata(Map.of("my_metadata", "my_value"));

        // Mock server and testing request sent

        MockResponse mockedResponse = new MockResponse();
        mockedResponse.setResponseCode(200);
        mockBackEnd.enqueue(mockedResponse);

        statsService.registerFolderAction(tenant, START, folder, new DeskRepresentation("d01", "Desk 01"), 12345L);

        RecordedRequest recordedRequest = mockBackEnd.takeRequest(1, SECONDS);
        assertNotNull(recordedRequest);
        assertNotNull(recordedRequest.getPath());
        assertEquals("GET", recordedRequest.getMethod());
        assertTrue(recordedRequest.getPath().startsWith("/matomo.php?"));
        assertTrue(recordedRequest.getPath().contains("apiv1=1"));
        assertTrue(recordedRequest.getPath().contains("e_a=D%C3%A9marrage") || recordedRequest.getPath().contains("e_a=Start"));
        assertTrue(recordedRequest.getPath().contains("e_v=12345"));
        assertTrue(recordedRequest.getPath().contains("idsite=1"));
        assertTrue(recordedRequest.getPath().contains("rec=1"));

        mockBackEnd.enqueue(mockedResponse);
    }


    @Test
    void registerAdminAction() throws Exception {

        // Build up test case

        Tenant tenant = new Tenant();
        tenant.setId("t01");
        tenant.setName("Tenant 01");

        // Mock server and testing request sent

        MockResponse mockedResponse = new MockResponse();
        mockedResponse.setResponseCode(200);
        mockBackEnd.enqueue(mockedResponse);

        statsService.registerAdminAction(tenant, USER, UPDATE, "u01");
        RecordedRequest recordedRequest = mockBackEnd.takeRequest(1, SECONDS);
        System.out.println(recordedRequest);

        assertNotNull(recordedRequest);
        assertNotNull(recordedRequest.getPath());
        assertEquals("GET", recordedRequest.getMethod());
        assertTrue(recordedRequest.getPath().startsWith("/matomo.php?"));
        assertTrue(recordedRequest.getPath().contains("apiv1=1"));
        assertTrue(Pattern.compile(".*?rand=\\d+.*?").matcher(recordedRequest.getPath()).matches());
        assertTrue(recordedRequest.getPath().contains("e_v=u01"));
        assertTrue(recordedRequest.getPath().contains("e_c=USER"));
        assertTrue(recordedRequest.getPath().contains("e_a=UPDATE"));
        assertTrue(recordedRequest.getPath().contains("rec=1"));
    }


}
