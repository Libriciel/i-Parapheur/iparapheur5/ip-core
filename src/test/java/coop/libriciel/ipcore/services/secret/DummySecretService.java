/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.secret;

import coop.libriciel.ipcore.model.signatureValidation.ValidationServiceConfiguration;
import coop.libriciel.ipcore.model.crypto.SealCertificate;
import coop.libriciel.ipcore.model.crypto.SealCertificateRepresentation;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import static coop.libriciel.ipcore.utils.TextUtilsTest.DUMMY_SERVICE;


@Log4j2
@Service(SecretServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = SecretServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = DUMMY_SERVICE)
public class DummySecretService implements SecretServiceInterface {


    // <editor-fold desc="Seal certificate CRUDL">


    @Override
    public void storeSealCertificate(@NotNull String tenantId, @NotNull SealCertificate sealCertificate) {}


    @Override
    public @NotNull SealCertificate getSealCertificate(@NotNull String tenantId, @NotNull String sealCertificateId) {
        SealCertificate result = new SealCertificate();
        result.setId(sealCertificateId);
        return result;
    }


    @Override
    public void deleteSealCertificate(@NotNull String tenantId, @NotNull String sealCertificateId) {

    }


    @Override
    public @NotNull Page<SealCertificateRepresentation> getSealCertificateList(@NotNull String tenantId, @NotNull Pageable pageable) {
        return Page.empty();
    }


    // </editor-fold desc="Seal certificate CRUDL">


    @Override
    public void deleteTenant(@NotNull String tenantId) {

    }


    @Override
    public void storeValidationServiceConfiguration(@NotNull String clientId, @NotNull String clientSecret) {

    }


    @Override
    public @Nullable ValidationServiceConfiguration getValidationServiceConfiguration() {
        return null;
    }


    @Override
    public void deleteValidationServiceConfiguration() {

    }


}
