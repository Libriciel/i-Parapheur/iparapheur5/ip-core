/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.pdfstamp;

import com.fasterxml.jackson.databind.ObjectMapper;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.pdfstamp.StickyNote;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static coop.libriciel.ipcore.services.auth.KeycloakService.DESK_ROLE_PREFIX;
import static io.netty.util.NetUtil.LOCALHOST4;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.springframework.core.io.buffer.DataBufferUtils.readInputStream;


@SpringBootTest
@ExtendWith(SpringExtension.class)
@WithMockUser(
        username = "Jean",
        roles = {DESK_ROLE_PREFIX + "desk_0", DESK_ROLE_PREFIX + "desk_1"}
)
public class PdfStampServiceTest {

    private @Autowired ObjectMapper objectMapper;
    public static MockWebServer mockBackEnd;
    private PdfStampService pdfStampService;

    private StickyNote mockedAnnotation;
    private DocumentBuffer mockedDocumentBuffer;


    // <editor-fold desc="Tests LifeCycle">


    @BeforeAll
    static void setUp() throws IOException {
        mockBackEnd = new MockWebServer();
        mockBackEnd.start();
    }


    @BeforeEach
    void initialize() {

        pdfStampService = new PdfStampService(
                objectMapper,
                new PdfStampServiceProperties("none", LOCALHOST4.getHostName(), mockBackEnd.getPort())
        );

        mockedAnnotation = new StickyNote();
        mockedAnnotation.setX(0);
        mockedAnnotation.setY(0);

        mockedDocumentBuffer = new DocumentBuffer();
        InputStream body = new ByteArrayInputStream("test".getBytes(UTF_8));
        mockedDocumentBuffer.setContentFlux(readInputStream(() -> body, new DefaultDataBufferFactory(), 8192));
    }


    @AfterAll
    static void tearDown() throws IOException {
        mockBackEnd.shutdown();
    }


    // </editor-fold desc="Tests LifeCycle">


    // FIXME repair the tests (or remove obsolete ones) befor merging to develop

//    @Test
//    void createAnnotation_ok() {
//
//        mockBackEnd.enqueue(new MockResponse()
//                .setResponseCode(200)
//                .addHeader("Content-Type", APPLICATION_OCTET_STREAM.toString())
//                .setBody("123456789"));
//
//        DocumentBuffer result = pdfStampService.createAnnotation(mockedDocumentBuffer, mockedAnnotation);
//
//        assertEquals(9, result.getContentLength());
//        assertNotNull(result.getContentFlux());
//    }


//    @Test
//    void createAnnotation_subService4xx() {
//
//        mockBackEnd.enqueue(new MockResponse()
//                .setResponseCode(404)
//                .addHeader("Content-Type", APPLICATION_OCTET_STREAM.toString()));
//
//        try {
//            pdfStampService.createAnnotation(mockedDocumentBuffer, mockedAnnotation);
//            fail();
//        } catch (ResponseStatusException e) {
//            assertEquals(INTERNAL_SERVER_ERROR, e.getStatus());
//            assertNotNull(e.getReason());
//            assertNotNull(e.getMessage());
//        }
//    }


//    @Test
//    void createAnnotation_subService5xx() {
//
//        mockBackEnd.enqueue(new MockResponse()
//                .setResponseCode(503)
//                .addHeader("Content-Type", APPLICATION_OCTET_STREAM.toString()));
//
//        try {
//            pdfStampService.createAnnotation(mockedDocumentBuffer, mockedAnnotation);
//            fail();
//        } catch (ResponseStatusException e) {
//            assertEquals(INTERNAL_SERVER_ERROR, e.getStatus());
//            assertNotNull(e.getReason());
//            assertNotNull(e.getMessage());
//        }
//    }


//    @Test
//    void deleteAnnotation_ok() {
//
//        mockBackEnd.enqueue(new MockResponse()
//                .setResponseCode(200)
//                .addHeader("Content-Type", APPLICATION_OCTET_STREAM_VALUE)
//                .setBody("123456789"));
//
//        DocumentBuffer result = pdfStampService.deleteComment(mockedDocumentBuffer, "id_01");
//
//        assertEquals(9, result.getContentLength());
//        assertNotNull(result.getContentFlux());
//    }


//    @Test
//    void deleteAnnotation_subService4xx() {
//
//        mockBackEnd.enqueue(new MockResponse()
//                .setResponseCode(404)
//                .addHeader("Content-Type", APPLICATION_OCTET_STREAM.toString()));
//
//        try {
//            pdfStampService.deleteComment(mockedDocumentBuffer, "id_01");
//            fail();
//        } catch (ResponseStatusException e) {
//            assertEquals(INTERNAL_SERVER_ERROR, e.getStatus());
//            assertNotNull(e.getReason());
//            assertNotNull(e.getMessage());
//        }
//    }


}
