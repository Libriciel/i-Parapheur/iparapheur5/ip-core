/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.pdfstamp;

import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.pdfstamp.Stamp;
import coop.libriciel.pdf.stamp.api.model.Comment;
import org.jetbrains.annotations.NotNull;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Function;

import static coop.libriciel.ipcore.utils.TextUtilsTest.DUMMY_SERVICE;


@Service(PdfStampServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = PdfStampServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = DUMMY_SERVICE)
public class DummyPdfStampService implements PdfStampServiceInterface {


    @Override
    public @NotNull DocumentBuffer createStamp(@NotNull DocumentBuffer documentBuffer,
                                               @NotNull List<Stamp> stampList,
                                               @NotNull Function<String, DocumentBuffer> imageSupplier) {
        return documentBuffer;
    }


    @Override
    public @NotNull DocumentBuffer addComment(@NotNull DocumentBuffer documentBuffer, @NotNull Comment comment) {
        return documentBuffer;
    }


    @Override
    public @NotNull DocumentBuffer deleteComment(@NotNull DocumentBuffer documentBuffer, @NotNull String annotationId) {
        return documentBuffer;
    }


}
