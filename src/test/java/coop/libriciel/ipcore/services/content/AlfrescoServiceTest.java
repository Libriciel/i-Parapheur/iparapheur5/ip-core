/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.content;

import com.fasterxml.jackson.databind.ObjectMapper;
import coop.libriciel.alfresco.core.model.ContentInfo;
import coop.libriciel.alfresco.core.model.Node;
import coop.libriciel.alfresco.core.model.NodeChildAssociation;
import coop.libriciel.iparapheur.customalfrescotengines.model.crypto.SignatureInformation;
import coop.libriciel.iparapheur.customalfrescotengines.model.crypto.PdfSignaturePosition;
import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.content.ValidatedSignatureInformation;
import coop.libriciel.ipcore.model.content.alfresco.AlfrescoNode;
import coop.libriciel.ipcore.model.database.Tenant;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.*;

import static coop.libriciel.iparapheur.customalfrescotengines.model.content.PdfVersion.V1_4;
import static coop.libriciel.ipcore.model.content.alfresco.AlfrescoProperties.*;
import static coop.libriciel.ipcore.services.content.AlfrescoService.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.http.MediaType.APPLICATION_XML_VALUE;


@SpringBootTest
@ExtendWith(SpringExtension.class)
class AlfrescoServiceTest {


    private static final String SERIALIZED_TAGS_EXAMPLE =
            """
            {\
              "0":{\
                "x":111.796,\
                "y":733.089,\
                "page":1\
              },\
              "1":{\
                "x":141.79599,\
                "y":691.689,\
                "page":1\
              }\
            }\
            """;
    private static final String SERIALIZED_DOCUMENT_SIGNATURES_EXAMPLE =
            """
            [\
              {\
                "position":{"x":50.0,"y":50.0,"page":0,"pageHeight":842,"pageWidth":596},\
                "principalIssuer":"MEAxCzAJBgNVBAYTAkZSMRIwEAYDVQQKEwlDcnlwdG9sb2cxHTAbBgNVBAMTFFRlc3QgVW5pdmVyc2lnbiBSb290",\
                "principalSubjectIssuer":"MEMxCzAJBgNVBAYTAkZSMRIwEAYDVQQKEwlDcnlwdG9sb2cxIDAeBgNVBAMTF1Rlc3QgVW5pdmVyc2lnbiBDQSAyMDE4"\
              },\
              {\
                "position":{"x":0.0,"y":0.0,"page":0,"pageHeight":842,"pageWidth":596},\
                "principalIssuer":"MEMxCzAJBgNVBAYTAkZSMRIwEAYDVQQKEwlDcnlwdG9sb2cxIDAeBgNVBAMTF1Rlc3QgVW5pdmVyc2lnbiBDQSAyMDE4",\
                "principalSubjectIssuer":"MDsxCzAJBgNVBAYTAkZSMRIwEAYDVQQKEwlDcnlwdG9sb2cxGDAWBgNVBAMTD1Rlc3QgVFNBIC0gVFUgNg=="\
              }\
            ]\
            """;
    private static final String SERIALIZED_DOCUMENT_ANNOTATIONS_EXAMPLE =
            """
            [\
              {\
                "id":"94f097da-aff4-49f7-8a6e-e02277638f7a",\
                "page":1,\
                "width":199,\
                "height":70,\
                "x":312,\
                "y":699,\
                "pageRotation":90,\
                "rectangleOrigin":"BOTTOM_LEFT",\
                "signatureNumber":1\
              },\
              {\
                "id":"b420fb26-e944-4079-9160-8119dbf502a9",\
                "page":2,\
                "width":299,\
                "height":170,\
                "x":512,\
                "y":499,\
                "pageRotation":180,\
                "rectangleOrigin":"BOTTOM_LEFT",\
                "signatureNumber":2\
              }\
            ]\
            """;

    private final ModelMapper modelMapper = new ModelMapper();

    private @Autowired ObjectMapper objectMapper;
    private @Autowired ContentServiceProperties contentServiceProperties;

    private AlfrescoService alfrescoService;
    private ModelMapper alfrescoModelMapper;


//    @Mock
//    private RestTemplate restTemplate = mock(RestTemplate.class);
//
//    @InjectMocks
//    private AlfrescoService alfrescoService = new AlfrescoService("localhost", 9000, "admin", "admin");


    @BeforeEach
    void init() {
        alfrescoService = new AlfrescoService(contentServiceProperties, objectMapper, null);
        alfrescoModelMapper = alfrescoService.generateAlfrescoModelMapper();
    }


    @Test
    void computePathPlaceholders() {

        String pathTenant00 = AlfrescoService.computePathPlaceholders(Tenant.builder().id("tenant0").name("Tenant 0").index(0L).build());
        assertNotNull(pathTenant00);
        assertTrue(pathTenant00.matches("authorities/0/tenant0/folders/\\d+/\\d+-\\d+/\\d+-\\d+"));

        String pathTenant1K = AlfrescoService.computePathPlaceholders(Tenant.builder().id("tenant1001").name("Tenant 1001").index(1001L).build());
        assertNotNull(pathTenant1K);
        assertTrue(pathTenant1K.matches("authorities/1/tenant1001/folders/\\d+/\\d+-\\d+/\\d+-\\d+"));
    }


//    @Before public void generateHeaders() throws JsonProcessingException {
//        AlfrescoTicketModel ticketModel = new AlfrescoTicketModel();
//        HashMap<String, String> entry = new HashMap<>();
//        entry.put("id", "bearer_token");
//        ticketModel.setEntry(entry);
//
//        ResponseEntity<String> ticket = new ResponseEntity<>(mapper.writeValueAsString(ticketModel), HttpStatus.OK);
//
//        Mockito.when(restTemplate.exchange(
//                ArgumentMatchers.any(),
//                ArgumentMatchers.eq(HttpMethod.POST),
//                ArgumentMatchers.any(HttpEntity.class),
//                ArgumentMatchers.eq(String.class)))
//                .thenReturn(ticket);
//
//        alfrescoService.generateHeaders();
//    }
//
//
//    @Test public void test4xxErrorCode() {
//        Mockito.when(restTemplate.exchange(
//                ArgumentMatchers.any(),
//                ArgumentMatchers.eq(HttpMethod.POST),
//                ArgumentMatchers.any(HttpEntity.class),
//                ArgumentMatchers.eq(String.class)))
//                .thenThrow(new HttpClientErrorException(HttpStatus.UNAUTHORIZED));
//
//        alfrescoService.generateHeaders();
//    }
//
//
//    @Test public void testPopulateInfos() {
//        // Empty infos
//        alfrescoService.populateInfos(new String[]{});
//        // Only one info
//        alfrescoService.populateInfos(new String[]{"admin"});
//        // Double info
//        alfrescoService.populateInfos(new String[]{"admin", "admin"});
//    }
//
//
//    @Test public void testCreateContent() throws JsonProcessingException {
//        MockMultipartFile file = new MockMultipartFile("file", "filename.txt", "text/plain", "some text".getBytes());
//
//        AlfrescoEntryModel mockedResponse = new AlfrescoEntryModel();
//        HashMap<String, Object> entry = new HashMap<>();
//        entry.put("id", UUID.randomUUID().toString());
//        mockedResponse.setEntry(entry);
//
//        ResponseEntity<String> nodeCreatedResponse = new ResponseEntity<>(mapper.writeValueAsString(mockedResponse), HttpStatus.OK);
//
//        Mockito.when(restTemplate.exchange(
//                ArgumentMatchers.any(),
//                ArgumentMatchers.any(),
//                ArgumentMatchers.any(HttpEntity.class),
//                ArgumentMatchers.eq(String.class)))
//                .thenReturn(nodeCreatedResponse);
//
//        alfrescoService.createContent(file);
//    }


    @Nested
    class GenerateTransformExtractorMapper {


        @Nested
        class PdfSignatureToSignatureInfo {


            private static void assertDeepEquals(SignatureInformation libObject,
                                                 ValidatedSignatureInformation internalObject) {
                // assertEquals(Optional.ofNullable(libObject.getPosition()).map(PdfSignaturePosition::getX).orElse(null), internalObject.getX());
                // assertEquals(Optional.ofNullable(libObject.getPosition()).map(PdfSignaturePosition::getY).orElse(null), internalObject.getY());
                assertEquals(libObject.getPrincipalIssuer(), internalObject.getPrincipalIssuer());
                assertEquals(libObject.getPrincipalSubjectIssuer(), internalObject.getPrincipalSubjectIssuer());
                assertEquals(libObject.getCertificateEndDate(), internalObject.getCertificateEndDate());
            }


            @Test
            void full() {

                var libPosition = new PdfSignaturePosition();
                libPosition.setPage(new Random().nextInt(99));
                libPosition.setX(new Random().nextFloat(999));
                libPosition.setY(new Random().nextFloat(999));

                var libObject = new SignatureInformation();
                libObject.setPosition(libPosition);
                libObject.setPrincipalIssuer(RandomStringUtils.insecure().nextAlphanumeric(255));
                libObject.setPrincipalSubjectIssuer(RandomStringUtils.insecure().nextAlphanumeric(255));
                libObject.setCertificateEndDate(new Random().nextLong(999999));

                ValidatedSignatureInformation internalObject = modelMapper.map(libObject, ValidatedSignatureInformation.class);
                assertNotNull(internalObject);
                assertDeepEquals(libObject, internalObject);
            }


            @Test
            void empty() {

                var libObject = new SignatureInformation();

                ValidatedSignatureInformation internalObject = modelMapper.map(libObject, ValidatedSignatureInformation.class);
                assertNotNull(internalObject);
                assertDeepEquals(libObject, internalObject);
            }


        }


        @Nested
        class NodeToDocument {


            @SuppressWarnings("unchecked")
            private static void assertDeepEquals(Node libObject, Document internalObject) {
                assertEquals(libObject.getId(), internalObject.getId());
                assertEquals(libObject.getName(), internalObject.getName());
                assertEquals(
                        Optional.ofNullable(libObject.getContent())
                                .map(ContentInfo::getSizeInBytes)
                                .orElse(-1L),
                        internalObject.getContentLength()
                );
                assertEquals(
                        Optional.ofNullable(libObject.getContent())
                                .map(ContentInfo::getMimeType)
                                .orElse(null),
                        Objects.toString(internalObject.getMediaType(), null)
                );
                assertEquals(
                        Optional.ofNullable(libObject.getProperties())
                                .map(map -> MapUtils.getString((Map<String, Object>) map, PROPERTY_PDF_VERSION))
                                .map(string -> string
                                        // Lazy enum parse
                                        .replace("V", "")
                                        .replace("_", ".")
                                )
                                .orElse(null),
                        Objects.toString(internalObject.getMediaVersion(), null)
                );
                assertEquals(
                        Optional.ofNullable(libObject.getProperties())
                                .map(map -> MapUtils.getString((Map<String, Object>) map, PROPERTY_INDEX))
                                .map(intString -> {
                                    try {
                                        return Integer.parseInt(intString);
                                    } catch (NumberFormatException e) {
                                        return 0;
                                    }
                                })
                                .orElse(0),
                        internalObject.getIndex()
                );
                assertEquals(
                        Optional.ofNullable(libObject.getProperties())
                                .map(map -> MapUtils.getBoolean((Map<String, Object>) map, PROPERTY_DELETABLE))
                                .orElse(false),
                        internalObject.isDeletable()
                );
                assertEquals(
                        Optional.ofNullable(libObject.getProperties())
                                .map(map -> MapUtils.getString((Map<String, Object>) map, PROPERTY_CHECKSUM_VALUE))
                                .orElse(null),
                        internalObject.getChecksumValue()
                );
                assertEquals(
                        Optional.ofNullable(libObject.getProperties())
                                .map(map -> MapUtils.getBoolean((Map<String, Object>) map, PROPERTY_IS_MAIN_DOCUMENT))
                                .orElse(false),
                        internalObject.isMainDocument()
                );

                if (Optional.ofNullable(libObject.getProperties())
                        .map(map -> MapUtils.getString((Map<String, Object>) map, PROPERTY_SIGNATURE_TAGS))
                        .filter(signatures -> StringUtils.equals(signatures, SERIALIZED_TAGS_EXAMPLE))
                        .isPresent()) {
                    assertNotNull(internalObject.getSignatureTags());
                    assertEquals(2, internalObject.getSignatureTags().size());
                } else {
                    assertNull(internalObject.getSignatureTags());
                }

                if (Optional.ofNullable(libObject.getProperties())
                        .map(map -> MapUtils.getString((Map<String, Object>) map, PROPERTY_SEAL_TAGS))
                        .filter(signatures -> StringUtils.equals(signatures, SERIALIZED_TAGS_EXAMPLE))
                        .isPresent()) {
                    assertNotNull(internalObject.getSealTags());
                    assertEquals(2, internalObject.getSealTags().size());
                } else {
                    assertNull(internalObject.getSealTags());
                }

                if (Optional.ofNullable(libObject.getProperties())
                        .map(map -> MapUtils.getString((Map<String, Object>) map, PROPERTY_SIGNATURES))
                        .filter(signatures -> StringUtils.equals(signatures, SERIALIZED_DOCUMENT_SIGNATURES_EXAMPLE))
                        .isPresent()) {
                    assertNotNull(internalObject.getEmbeddedSignatureInfos());
                    assertEquals(2, internalObject.getEmbeddedSignatureInfos().size());
                } else {
                    assertNull(internalObject.getEmbeddedSignatureInfos());
                }

                if (Optional.ofNullable(libObject.getProperties())
                        .map(map -> MapUtils.getString((Map<String, Object>) map, PROPERTY_ANNOTATIONS))
                        .filter(signatures -> StringUtils.equals(signatures, SERIALIZED_DOCUMENT_ANNOTATIONS_EXAMPLE))
                        .isPresent()) {
                    assertNotNull(internalObject.getSignaturePlacementAnnotations());
                    assertEquals(2, internalObject.getSignaturePlacementAnnotations().size());
                } else {
                    assertEquals(internalObject.getSignaturePlacementAnnotations().size(), 0);
                }
            }


            @Test
            void full() {

                ContentInfo contentInfo = new ContentInfo();
                contentInfo.setSizeInBytes(new Random().nextLong(99999L));
                contentInfo.setMimeType(APPLICATION_XML_VALUE);

                Map<String, Object> propertiesMap = new HashMap<>();
                propertiesMap.put(PROPERTY_PDF_VERSION, V1_4);
                propertiesMap.put(PROPERTY_INDEX, new Random().nextInt(99));
                propertiesMap.put(PROPERTY_DELETABLE, true);
                propertiesMap.put(PROPERTY_CHECKSUM_VALUE, DigestUtils.sha256Hex(RandomStringUtils.insecure().nextAlphanumeric(128)));
                propertiesMap.put(PROPERTY_SIGNATURE_TAGS, SERIALIZED_TAGS_EXAMPLE);
                propertiesMap.put(PROPERTY_SEAL_TAGS, SERIALIZED_TAGS_EXAMPLE);
                propertiesMap.put(PROPERTY_SIGNATURES, SERIALIZED_DOCUMENT_SIGNATURES_EXAMPLE);
                propertiesMap.put(PROPERTY_ANNOTATIONS, SERIALIZED_DOCUMENT_ANNOTATIONS_EXAMPLE);

                Node libObject = new Node();
                libObject.setId(UUID.randomUUID().toString());
                libObject.setName(RandomStringUtils.insecure().nextAlphanumeric(128));
                libObject.setContent(contentInfo);
                libObject.setProperties(propertiesMap);

                Document internalObject = alfrescoModelMapper.map(libObject, Document.class);
                assertNotNull(internalObject);
                assertDeepEquals(libObject, internalObject);
            }


            @Test
            void empty() {

                Node libObject = new Node();

                Document internalObject = alfrescoModelMapper.map(libObject, Document.class);
                assertNotNull(internalObject);
                assertDeepEquals(libObject, internalObject);
            }

        }


        @Nested
        class NodeChildAssociationToDocument {


            @SuppressWarnings("unchecked")
            private static void assertDeepEquals(NodeChildAssociation libObject, Document internalObject) {
                assertEquals(libObject.getId(), internalObject.getId());
                assertEquals(libObject.getName(), internalObject.getName());
                assertEquals(
                        Optional.ofNullable(libObject.getContent())
                                .map(ContentInfo::getSizeInBytes)
                                .orElse(-1L),
                        internalObject.getContentLength()
                );
                assertEquals(
                        Optional.ofNullable(libObject.getContent())
                                .map(ContentInfo::getMimeType)
                                .orElse(null),
                        Objects.toString(internalObject.getMediaType(), null)
                );
                assertEquals(
                        Optional.ofNullable(libObject.getProperties())
                                .map(map -> MapUtils.getString((Map<String, Object>) map, PROPERTY_PDF_VERSION))
                                .map(string -> string
                                        // Lazy enum parse
                                        .replace("V", "")
                                        .replace("_", ".")
                                )
                                .orElse(null),
                        Objects.toString(internalObject.getMediaVersion(), null)
                );
                assertEquals(
                        Optional.ofNullable(libObject.getProperties())
                                .map(map -> MapUtils.getString((Map<String, Object>) map, PROPERTY_INDEX))
                                .map(intString -> {
                                    try {
                                        return Integer.parseInt(intString);
                                    } catch (NumberFormatException e) {
                                        return 0;
                                    }
                                })
                                .orElse(0),
                        internalObject.getIndex()
                );
                assertEquals(
                        Optional.ofNullable(libObject.getProperties())
                                .map(map -> MapUtils.getBoolean((Map<String, Object>) map, PROPERTY_DELETABLE))
                                .orElse(false),
                        internalObject.isDeletable()
                );
                assertEquals(
                        Optional.ofNullable(libObject.getProperties())
                                .map(map -> MapUtils.getString((Map<String, Object>) map, PROPERTY_CHECKSUM_VALUE))
                                .orElse(null),
                        internalObject.getChecksumValue()
                );
                assertEquals(
                        Optional.ofNullable(libObject.getProperties())
                                .map(map -> MapUtils.getBoolean((Map<String, Object>) map, PROPERTY_IS_MAIN_DOCUMENT))
                                .orElse(false),
                        internalObject.isMainDocument()
                );

                if (Optional.ofNullable(libObject.getProperties())
                        .map(map -> MapUtils.getString((Map<String, Object>) map, PROPERTY_SIGNATURE_TAGS))
                        .filter(signatures -> StringUtils.equals(signatures, SERIALIZED_TAGS_EXAMPLE))
                        .isPresent()) {
                    assertNotNull(internalObject.getSignatureTags());
                    assertEquals(2, internalObject.getSignatureTags().size());
                } else {
                    assertNull(internalObject.getSignatureTags());
                }

                if (Optional.ofNullable(libObject.getProperties())
                        .map(map -> MapUtils.getString((Map<String, Object>) map, PROPERTY_SEAL_TAGS))
                        .filter(signatures -> StringUtils.equals(signatures, SERIALIZED_TAGS_EXAMPLE))
                        .isPresent()) {
                    assertNotNull(internalObject.getSealTags());
                    assertEquals(2, internalObject.getSealTags().size());
                } else {
                    assertNull(internalObject.getSealTags());
                }

                if (Optional.ofNullable(libObject.getProperties())
                        .map(map -> MapUtils.getString((Map<String, Object>) map, PROPERTY_SIGNATURES))
                        .filter(signatures -> StringUtils.equals(signatures, SERIALIZED_DOCUMENT_SIGNATURES_EXAMPLE))
                        .isPresent()) {
                    assertNotNull(internalObject.getEmbeddedSignatureInfos());
                    assertEquals(2, internalObject.getEmbeddedSignatureInfos().size());
                } else {
                    assertNull(internalObject.getEmbeddedSignatureInfos());
                }

                if (Optional.ofNullable(libObject.getProperties())
                        .map(map -> MapUtils.getString((Map<String, Object>) map, PROPERTY_ANNOTATIONS))
                        .filter(signatures -> StringUtils.equals(signatures, SERIALIZED_DOCUMENT_ANNOTATIONS_EXAMPLE))
                        .isPresent()) {
                    assertNotNull(internalObject.getSignaturePlacementAnnotations());
                    assertEquals(2, internalObject.getSignaturePlacementAnnotations().size());
                } else {
                    assertEquals(internalObject.getSignaturePlacementAnnotations().size(), 0);
                }
            }


            @Test
            void full() {

                ContentInfo contentInfo = new ContentInfo();
                contentInfo.setSizeInBytes(new Random().nextLong(99999L));
                contentInfo.setMimeType(APPLICATION_XML_VALUE);

                Map<String, Object> propertiesMap = new HashMap<>();
                propertiesMap.put(PROPERTY_PDF_VERSION, V1_4);
                propertiesMap.put(PROPERTY_INDEX, new Random().nextInt(99));
                propertiesMap.put(PROPERTY_DELETABLE, true);
                propertiesMap.put(PROPERTY_CHECKSUM_VALUE, DigestUtils.sha256Hex(RandomStringUtils.insecure().nextAlphanumeric(128)));
                propertiesMap.put(PROPERTY_SIGNATURE_TAGS, SERIALIZED_TAGS_EXAMPLE);
                propertiesMap.put(PROPERTY_SEAL_TAGS, SERIALIZED_TAGS_EXAMPLE);
                propertiesMap.put(PROPERTY_SIGNATURES, SERIALIZED_DOCUMENT_SIGNATURES_EXAMPLE);
                propertiesMap.put(PROPERTY_ANNOTATIONS, SERIALIZED_DOCUMENT_ANNOTATIONS_EXAMPLE);

                NodeChildAssociation libObject = new NodeChildAssociation();
                libObject.setId(UUID.randomUUID().toString());
                libObject.setName(RandomStringUtils.insecure().nextAlphanumeric(128));
                libObject.setContent(contentInfo);
                libObject.setProperties(propertiesMap);

                Document internalObject = alfrescoModelMapper.map(libObject, Document.class);
                assertNotNull(internalObject);
                assertDeepEquals(libObject, internalObject);
            }


            @Test
            void empty() {

                NodeChildAssociation libObject = new NodeChildAssociation();

                Document internalObject = alfrescoModelMapper.map(libObject, Document.class);
                assertNotNull(internalObject);
                assertDeepEquals(libObject, internalObject);
            }

        }


        @Nested
        class AlfrescoNodeToDocument {


            @SuppressWarnings("unchecked")
            private static void assertDeepEquals(AlfrescoNode libObject, Document internalObject) {
                assertEquals(libObject.getId(), internalObject.getId());
                assertEquals(libObject.getName(), internalObject.getName());
                assertEquals(
                        Optional.ofNullable(libObject.getContent())
                                .map(ContentInfo::getSizeInBytes)
                                .orElse(-1L),
                        internalObject.getContentLength()
                );
                assertEquals(
                        Optional.ofNullable(libObject.getContent())
                                .map(ContentInfo::getMimeType)
                                .orElse(null),
                        Objects.toString(internalObject.getMediaType(), null)
                );
                assertEquals(
                        Optional.ofNullable(libObject.getProperties())
                                .map(map -> MapUtils.getString(map, PROPERTY_PDF_VERSION))
                                .map(string -> string
                                        // Lazy enum parse
                                        .replace("V", "")
                                        .replace("_", ".")
                                )
                                .orElse(null),
                        Objects.toString(internalObject.getMediaVersion(), null)
                );
                assertEquals(
                        Optional.ofNullable(libObject.getProperties())
                                .map(map -> MapUtils.getString(map, PROPERTY_INDEX))
                                .map(intString -> {
                                    try {
                                        return Integer.parseInt(intString);
                                    } catch (NumberFormatException e) {
                                        return 0;
                                    }
                                })
                                .orElse(0),
                        internalObject.getIndex()
                );
                assertEquals(
                        Optional.ofNullable(libObject.getProperties())
                                .map(map -> MapUtils.getBoolean(map, PROPERTY_DELETABLE))
                                .orElse(false),
                        internalObject.isDeletable()
                );
                assertEquals(
                        Optional.ofNullable(libObject.getProperties())
                                .map(map -> MapUtils.getString(map, PROPERTY_CHECKSUM_VALUE))
                                .orElse(null),
                        internalObject.getChecksumValue()
                );
                assertEquals(
                        Optional.ofNullable(libObject.getProperties())
                                .map(map -> MapUtils.getBoolean(map, PROPERTY_IS_MAIN_DOCUMENT))
                                .orElse(false),
                        internalObject.isMainDocument()
                );

                if (Optional.ofNullable(libObject.getProperties())
                        .map(map -> MapUtils.getString(map, PROPERTY_SIGNATURE_TAGS))
                        .filter(signatures -> StringUtils.equals(signatures, SERIALIZED_TAGS_EXAMPLE))
                        .isPresent()) {
                    assertNotNull(internalObject.getSignatureTags());
                    assertEquals(2, internalObject.getSignatureTags().size());
                } else {
                    assertNull(internalObject.getSignatureTags());
                }

                if (Optional.ofNullable(libObject.getProperties())
                        .map(map -> MapUtils.getString(map, PROPERTY_SEAL_TAGS))
                        .filter(signatures -> StringUtils.equals(signatures, SERIALIZED_TAGS_EXAMPLE))
                        .isPresent()) {
                    assertNotNull(internalObject.getSealTags());
                    assertEquals(2, internalObject.getSealTags().size());
                } else {
                    assertNull(internalObject.getSealTags());
                }

                if (Optional.ofNullable(libObject.getProperties())
                        .map(map -> MapUtils.getString(map, PROPERTY_SIGNATURES))
                        .filter(signatures -> StringUtils.equals(signatures, SERIALIZED_DOCUMENT_SIGNATURES_EXAMPLE))
                        .isPresent()) {
                    assertNotNull(internalObject.getEmbeddedSignatureInfos());
                    assertEquals(2, internalObject.getEmbeddedSignatureInfos().size());
                } else {
                    assertNull(internalObject.getEmbeddedSignatureInfos());
                }

                if (Optional.ofNullable(libObject.getProperties())
                        .map(map -> MapUtils.getString(map, PROPERTY_ANNOTATIONS))
                        .filter(signatures -> StringUtils.equals(signatures, SERIALIZED_DOCUMENT_ANNOTATIONS_EXAMPLE))
                        .isPresent()) {
                    assertNotNull(internalObject.getSignaturePlacementAnnotations());
                    assertEquals(2, internalObject.getSignaturePlacementAnnotations().size());
                } else {
                    assertEquals(internalObject.getSignaturePlacementAnnotations().size(), 0);
                }
            }


            @Test
            void full() {

                ContentInfo contentInfo = new ContentInfo();
                contentInfo.setSizeInBytes(new Random().nextLong(99999L));
                contentInfo.setMimeType(APPLICATION_XML_VALUE);

                Map<String, Object> propertiesMap = new HashMap<>();
                propertiesMap.put(PROPERTY_PDF_VERSION, V1_4);
                propertiesMap.put(PROPERTY_INDEX, new Random().nextInt(99));
                propertiesMap.put(PROPERTY_DELETABLE, true);
                propertiesMap.put(PROPERTY_CHECKSUM_VALUE, DigestUtils.sha256Hex(RandomStringUtils.insecure().nextAlphanumeric(128)));
                propertiesMap.put(PROPERTY_SIGNATURE_TAGS, SERIALIZED_TAGS_EXAMPLE);
                propertiesMap.put(PROPERTY_SEAL_TAGS, SERIALIZED_TAGS_EXAMPLE);
                propertiesMap.put(PROPERTY_SIGNATURES, SERIALIZED_DOCUMENT_SIGNATURES_EXAMPLE);
                propertiesMap.put(PROPERTY_ANNOTATIONS, SERIALIZED_DOCUMENT_ANNOTATIONS_EXAMPLE);

                AlfrescoNode libObject = new AlfrescoNode();
                libObject.setId(UUID.randomUUID().toString());
                libObject.setName(RandomStringUtils.insecure().nextAlphanumeric(128));
                libObject.setContent(contentInfo);
                libObject.setProperties(propertiesMap);

                Document internalObject = alfrescoModelMapper.map(libObject, Document.class);
                assertNotNull(internalObject);
                assertDeepEquals(libObject, internalObject);
            }


            @Test
            void empty() {

                AlfrescoNode libObject = new AlfrescoNode();

                Document internalObject = alfrescoModelMapper.map(libObject, Document.class);
                assertNotNull(internalObject);
                assertDeepEquals(libObject, internalObject);
            }

        }


    }


}
