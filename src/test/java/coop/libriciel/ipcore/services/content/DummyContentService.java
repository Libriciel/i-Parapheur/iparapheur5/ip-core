/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.content;

import coop.libriciel.ipcore.model.content.Document;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.model.content.SignatureProof;
import coop.libriciel.ipcore.model.content.ValidatedSignatureInformation;
import coop.libriciel.ipcore.model.database.TemplateType;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.pdfstamp.Layer;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.UUID;

import static coop.libriciel.ipcore.utils.TextUtilsTest.DUMMY_SERVICE;
import static java.util.Collections.emptyList;
import static org.apache.commons.lang3.StringUtils.EMPTY;


@Service(ContentServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = ContentServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = DUMMY_SERVICE)
public class DummyContentService implements ContentServiceInterface {


    @Override
    public @NotNull String createTenant(@NotNull Tenant tenant) {return UUID.randomUUID().toString();}


    @Override
    public void deleteTenant(@NotNull Tenant tenant) {}


    @Override
    public @NotNull String createUserData(@NotNull String userId, int userDataGroupIndex) {
        return UUID.randomUUID().toString();
    }


    @Override
    public void deleteUserData(@NotNull String contentId) {}


    // <editor-fold desc="Signature image CRUD">


    @Override
    public @NotNull String createUserSignatureImage(@NotNull String userDataContentId, @NotNull DocumentBuffer file) {
        return UUID.randomUUID().toString();
    }


    @Override
    public @NotNull String createSealCertificateSignatureImage(@NotNull Tenant tenant, @NotNull String sealCertificateId, @NotNull DocumentBuffer file) {
        return UUID.randomUUID().toString();
    }


    @Override
    public @NotNull DocumentBuffer getSignatureImage(@NotNull String signatureNodeId) {
        return new DocumentBuffer();
    }


    @Override
    public String updateSignatureImage(@NotNull String imageNodeId, @NotNull DocumentBuffer file) {return imageNodeId;}


    @Override
    public void deleteSignatureImage(@NotNull String signatureNodeId) {}


    // </editor-fold desc="Signature image CRUD">


    @Override
    public @NotNull String createFolder(@Nullable Tenant tenant) {
        return UUID.randomUUID().toString();
    }


    @Override
    public void deleteFolder(@NotNull Folder folder) {}


    @Override
    public @NotNull String createPremisDocument(@NotNull Folder folder, @NotNull String premis) {return UUID.randomUUID().toString();}


    @Override
    public void updateEmbeddedSignatureInformation(@NotNull String nodeId, @NotNull List<ValidatedSignatureInformation> signatureInfos) {}


    @Override
    public void updateDetachedSignatureInformation(@NotNull String detachedSignatureId, @NotNull ValidatedSignatureInformation signatureInfo) {}


    @Override
    public String updateDocumentInternalProperties(@NotNull Document document) {
        return null;
    }


    @Override
    public void removeFirstSignaturePlacementAnnotation(List<Document> mainDocumentList) {}


    // <editor-fold desc="Document CRUDL">


    @Override
    public @NotNull String createDocument(@NotNull String tenantId, @NotNull Folder folder, @NotNull DocumentBuffer file) {
        return UUID.randomUUID().toString();
    }


    @Override
    public @NotNull List<SignatureProof> getSignatureProofList(@NotNull String folderContentId) {
        return emptyList();
    }


    @Override
    public @NotNull Document getDocumentInfo(@NotNull String documentId) {return new Document();}


    @Override
    public @NotNull SignatureProof getSignatureProof(@NotNull String documentId) {
        return new SignatureProof();
    }


    @Override
    public void updateDocument(@NotNull String documentId, @NotNull DocumentBuffer documentBuffer, boolean isFullDocumentSwap) {}


    @Override
    public void deleteDocument(@NotNull String documentId) {}


    @Override
    public void populateFolderWithAllDocumentTypes(@NotNull Folder folder) {}


    @Override
    public @NotNull List<Document> getDocumentList(@NotNull String folderContentId) {
        return emptyList();
    }


    // </editor-fold desc="Document CRUDL">


    @Override
    public @NotNull DocumentBuffer retrieveContent(@NotNull String documentId) {
        return new DocumentBuffer();
    }


    @Override
    public @NotNull InputStream retrievePipedDocument(@NotNull String documentId) {return new ByteArrayInputStream(EMPTY.getBytes());}


    @Override
    public @NotNull String createDetachedSignature(@NotNull String folderContentId, @Nullable Document targetDocument,
                                                   @NotNull DocumentBuffer file, @Nullable Task task) {
        return UUID.randomUUID().toString();
    }


    @Override
    public @NotNull String createSignatureProof(@NotNull String folderContentId,
                                                @NotNull DocumentBuffer documentBuffer,
                                                @NotNull Boolean isInternal,
                                                @Nullable String errorMessage,
                                                @Nullable String taskId) {
        return UUID.randomUUID().toString();
    }


    @Override
    public @NotNull String createPdfVisual(@NotNull Folder folder, @NotNull String targetDocument, @NotNull DocumentBuffer documentBuffer) {
        return UUID.randomUUID().toString();
    }


    // <editor-fold desc="Templates CRUD">


    @Override
    public @NotNull String createCustomTemplate(@NotNull Tenant tenant, @NotNull TemplateType templateType, @NotNull String templateValue) {
        return UUID.randomUUID().toString();
    }


    @Override
    public @NotNull DocumentBuffer getCustomTemplate(@NotNull Tenant tenant, @NotNull TemplateType templateType) {return new DocumentBuffer();}


    @Override
    public void editCustomTemplate(@NotNull Tenant tenant, @NotNull TemplateType templateType, @NotNull String templateValue) {}


    @Override
    public void deleteCustomTemplate(@NotNull Tenant tenant, @NotNull TemplateType templateType) {}


    // </editor-fold desc="Templates CRUD">


    // <editor-fold desc="Layer image CRUD">


    @Override
    public @NotNull String createLayerImage(@NotNull Tenant tenant, @NotNull Layer layer, @NotNull DocumentBuffer file) {
        return UUID.randomUUID().toString();
    }


    @Override
    public void deleteLayerImage(@NotNull String layerImageId) {}


    // </editor-fold desc="Layer image CRUD">


}
