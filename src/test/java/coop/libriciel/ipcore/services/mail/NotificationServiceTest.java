/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.mail;

import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.mail.MailContent;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.model.workflow.Task;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.auth.KeycloakService;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import coop.libriciel.ipcore.services.database.TenantRepository;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.services.scheduler.SchedulerServiceInterface;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import freemarker.template.Configuration;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Date;
import java.util.List;

import static coop.libriciel.ipcore.model.workflow.Action.SIGNATURE;
import static coop.libriciel.ipcore.model.workflow.NotificationType.ACTION_PERFORMED;
import static coop.libriciel.ipcore.utils.FileUtils.MAIL_TEMPLATE_PATH;
import static coop.libriciel.ipcore.utils.TextUtilsTest.DUMMY_SERVICE;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;


@SpringBootTest
@ExtendWith(SpringExtension.class)
class NotificationServiceTest {


    // <editor-fold desc="Dummy test objects">


    private final Tenant tenant = Tenant.builder()
            .id("tenantId")
            .name("Nîmes")
            .build();

    private final DeskRepresentation desk = new DeskRepresentation("deskId", "Employé");

    private final User sender = User.builder()
            .id("userId")
            .firstName("First name éè")
            .lastName("Last name éè")
            .userName("username")
            .build();

    private final Task task = Task.builder()
            .desks(singletonList(desk))
            .action(SIGNATURE)
            .date(new Date(99999L))
            .publicAnnotation("Multiline\nTest éè")
            .build();

    private final Folder folder = Folder.builder()
            .id("folderId")
            .name("Folder éè")
            .stepList(singletonList(task))
            .build();

    private final List<MailContent> contentList = singletonList(new MailContent(tenant, folder, task, desk, ACTION_PERFORMED, false, null, false, null, null));


    // </editor-fold desc="Dummy test objects">


    private @Mock AuthServiceInterface authServiceMock = mock(KeycloakService.class);
    private @Mock JavaMailSender javaMailSender = mock(JavaMailSender.class);
    private @Mock PostponedNotificationCacheRepository notificationCacheRepository = mock(PostponedNotificationCacheRepository.class);
    private @Mock PermissionServiceInterface permissionService = mock(PermissionServiceInterface.class);
    private @Mock SchedulerServiceInterface schedulerServiceInterface = mock(SchedulerServiceInterface.class);
    private @Mock TenantRepository tenantRepository = mock(TenantRepository.class);
    private @Mock WorkflowServiceInterface workflowService = mock(WorkflowServiceInterface.class);
    private @Mock ContentServiceInterface contentService = mock(ContentServiceInterface.class);

    private @Value(MAIL_TEMPLATE_PATH + "mail_notification_single.ftl") Resource defaultMailNotificationSingleTemplateResource;
    private @Value(MAIL_TEMPLATE_PATH + "mail_notification_digest.ftl") Resource defaultMailNotificationDigestTemplateResource;
    private @Value(MAIL_TEMPLATE_PATH + "mail_action_send.ftl") Resource defaultMailActionSendTemplateResource;

    private @Autowired Configuration configuration;
    private @Autowired ModelMapper modelMapper;

    private final NotificationServiceProperties properties = new NotificationServiceProperties(
            DUMMY_SERVICE,
            "noreply@dom.local", 1025,
            "dummy", "pass", true,
            true, true,
            "dummy@dom.invalid",
            "https://iparapheur.dom.local",
            "Footer éè", "[iparapheur éè]"
    );

    private final NotificationService notificationService = new NotificationService(
            authServiceMock,
            configuration,
            contentService,
            javaMailSender,
            modelMapper,
            properties,
            notificationCacheRepository,
            permissionService,
            schedulerServiceInterface,
            tenantRepository,
            workflowService);


    @Test
    void prepareFolderMessage_accentuatedChars_defaultMailNotificationSingle() {
        String result = notificationService.prepareFolderMessage(sender, contentList, "éè\nMultiline\nTest", defaultMailNotificationSingleTemplateResource);
        // System.out.println(result);
        assertTrue(result.contains("Folder &eacute;&egrave;"));
        assertFalse(result.contains("Folder éè"));
        assertTrue(result.contains("Employ&eacute;"));
        assertFalse(result.contains("Employé"));
        assertTrue(result.contains("First name &eacute;&egrave;"));
        assertFalse(result.contains("First name éè"));
        assertTrue(result.contains("Last name &eacute;&egrave;"));
        assertFalse(result.contains("Last name éè"));
        assertTrue(result.contains("Footer &eacute;&egrave;"));
        assertFalse(result.contains("Footer éè"));
    }


    @Test
    void prepareFolderMessage_accentuatedChars_defaultMailNotificationDigest() {
        String result = notificationService.prepareFolderMessage(sender, contentList, "éè\nMultiline\nTest", defaultMailNotificationDigestTemplateResource);
        // System.out.println(result);
        assertTrue(result.contains("Folder &eacute;&egrave;"));
        assertFalse(result.contains("Folder éè"));
        assertFalse(result.contains("Employé"));
        assertFalse(result.contains("First name éè"));
        assertFalse(result.contains("Last name éè"));
        assertTrue(result.contains("Footer &eacute;&egrave;"));
        assertFalse(result.contains("Footer éè"));
    }


    @Test
    void prepareFolderMessage_accentuatedChars_defaultMailActionSend() {
        String result = notificationService.prepareFolderMessage(sender, contentList, "éè\nMultiline\nTest", defaultMailActionSendTemplateResource);
        // System.out.println(result);
        assertTrue(result.contains("Folder &eacute;&egrave;"));
        assertFalse(result.contains("Folder éè"));
        assertFalse(result.contains("Employé"));
        assertTrue(result.contains("First name &eacute;&egrave;"));
        assertFalse(result.contains("First name éè"));
        assertTrue(result.contains("Last name &eacute;&egrave;"));
        assertFalse(result.contains("Last name éè"));
        assertTrue(result.contains("&eacute;&egrave;<br>Multiline<br>Test"));
        assertFalse(result.contains("éè\\nMultiline\\nTest"));
    }


}
