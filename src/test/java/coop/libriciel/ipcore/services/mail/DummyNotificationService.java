/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.mail;

import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.crypto.SealCertificateRepresentation;
import coop.libriciel.ipcore.model.database.TenantRepresentation;
import coop.libriciel.ipcore.model.mail.MailContent;
import coop.libriciel.ipcore.model.mail.MailNotification;
import coop.libriciel.ipcore.model.mail.MailTarget;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static coop.libriciel.ipcore.utils.TextUtilsTest.DUMMY_SERVICE;


@Service(NotificationServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = NotificationServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = DUMMY_SERVICE)
public class DummyNotificationService implements NotificationServiceInterface {


    @Override
    public void processNotification(@NotNull MailNotification notification, @Nullable User user) {}


    @Override
    public void computePendingMails(@NotNull String userId) {}


    @Override
    public void mailExpiredCertificates(@NotNull List<User> targetUsers,
                                        @NotNull TenantRepresentation tenant,
                                        @NotNull List<SealCertificateRepresentation> certificates) {}


    @Override
    public void mailFolder(@NotNull List<MailTarget> targetList,
                           @NotNull Resource mailTemplate,
                           @NotNull List<MailContent> contentList,
                           @Nullable User sender,
                           @Nullable String object,
                           @Nullable String message,
                           @Nullable InputStream mergedDocsInputStream,
                           @Nullable String attachementFilename) {}


    @Override
    public void resetNotificationScheduler(String userId, String newNotificationFrequency) {}


    @Override
    public Map<String, String> getDesksToNotifyByDelegation(String tenantId, Set<String> directDesksToNotify) {
        return Collections.EMPTY_MAP;
    }

}
