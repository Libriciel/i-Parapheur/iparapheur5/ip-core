/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.signatureValidation;

import coop.libriciel.ipcore.model.signatureValidation.ValidationServiceConfiguration;
import coop.libriciel.ipcore.model.content.DocumentBuffer;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Map;

import static coop.libriciel.ipcore.utils.TextUtilsTest.DUMMY_SERVICE;


@Log4j2
@Service(SignatureValidationServiceInterface.BEAN_NAME)
@ConditionalOnProperty(name = SignatureValidationServiceInterface.PREFERENCES_PROVIDER_KEY, havingValue = DUMMY_SERVICE)
public class DummySignatureValidationService implements SignatureValidationServiceInterface {

    @Override
    public Map<String, String> getSignatureValidation(DocumentBuffer document, Collection<DocumentBuffer> originalFiles) {return null;}


    @Override
    public void testConfiguration(ValidationServiceConfiguration validationServiceConfiguration) {
    }


    @Override
    public ValidationServiceConfiguration getSignatureValidationConfiguration() {
        return null;
    }
}
