/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.services.transformation;

import coop.libriciel.ipcore.model.content.DocumentBuffer;
import coop.libriciel.ipcore.utils.RequestUtils;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;

import java.io.*;
import java.nio.file.Files;

import static coop.libriciel.ipcore.services.content.ContentServiceInterface.FILE_TRANSFER_BUFFER_SIZE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.core.io.buffer.DataBufferUtils.readInputStream;


class AlfrescoTransformAllInOneServiceTest {


    private static final String TEST_PNG_IMAGE_RESOURCE_NAME = "test_image.png";

    private final ClassLoader classLoader = getClass().getClassLoader();


    @Test
    void imageToPdf() throws IOException {

        DocumentBuffer mockedDocumentBuffer = new DocumentBuffer();
        try (InputStream imageResource = classLoader.getResourceAsStream(TEST_PNG_IMAGE_RESOURCE_NAME)) {

            mockedDocumentBuffer.setContentFlux(readInputStream(() -> imageResource, new DefaultDataBufferFactory(), FILE_TRANSFER_BUFFER_SIZE));
            mockedDocumentBuffer.setName(TEST_PNG_IMAGE_RESOURCE_NAME);

            DocumentBuffer result = AlfrescoTransformAllInOneService.imageToPdf(mockedDocumentBuffer);
            assertEquals("test_image.pdf", result.getName());

            String resultFileDestination = "build/test-results/AlfrescoTransformAllInOneServiceTest_" + result.getName();
            // TODO : Use more modern Path and Files
            File resultFile = new File(resultFileDestination);
            resultFile.getParentFile().mkdirs();
            try (InputStream inputStream = RequestUtils.bufferToInputStream(result);
                 OutputStream outputStream = new FileOutputStream(resultFile)) {
                IOUtils.copyLarge(inputStream, outputStream);
            }

            assertTrue(resultFile.exists());
            assertTrue(Files.size(resultFile.toPath()) > 1000);
            assertTrue(resultFile.delete());
        }
    }


}
