/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.business.auth;

import coop.libriciel.ipcore.model.auth.Desk;
import coop.libriciel.ipcore.model.auth.DeskRepresentation;
import coop.libriciel.ipcore.model.auth.User;
import coop.libriciel.ipcore.model.auth.requests.DeskDto;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.services.auth.AuthServiceInterface;
import coop.libriciel.ipcore.services.auth.KeycloakService;
import coop.libriciel.ipcore.services.database.SubtypeRepository;
import coop.libriciel.ipcore.services.database.TypeRepository;
import coop.libriciel.ipcore.services.permission.KeycloakResourceService;
import coop.libriciel.ipcore.services.permission.PermissionServiceInterface;
import coop.libriciel.ipcore.services.workflow.IpWorkflowService;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import coop.libriciel.ipcore.utils.PaginatedList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.Locale;
import java.util.UUID;

import static coop.libriciel.ipcore.utils.LocalizedStatusException.getMessageForLocale;
import static coop.libriciel.ipcore.utils.PaginatedList.emptyPaginatedList;
import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;


class DeskBusinessServiceTest {


    private final String tenantId = "fakeTenantId";
    private final String deskId = "fakeDeskId";

    private DeskBusinessService deskBusinessService;

    private @Mock AuthServiceInterface authServiceMock = mock(KeycloakService.class);
    private @Mock PermissionServiceInterface permissionServiceMock = mock(KeycloakResourceService.class);
    private @Mock SubtypeRepository subtypeRepositoryMock = mock(SubtypeRepository.class);
    private @Mock TypeRepository typeRepositoryMock = mock(TypeRepository.class);
    private @Mock UserBusinessService userBusinessServiceMock = mock(UserBusinessService.class);
    private @Mock WorkflowServiceInterface workflowServiceMock = mock(IpWorkflowService.class);


    @BeforeEach
    void setup() {
        deskBusinessService = new DeskBusinessService(
                authServiceMock,
                new ModelMapper(),
                permissionServiceMock,
                subtypeRepositoryMock,
                typeRepositoryMock,
                userBusinessServiceMock,
                workflowServiceMock
        );
    }


    @Nested
    class UpdateDeskUsers {


        @Test
        void empty() {

            DeskDto request = new DeskDto();
            request.setOwnerIds(emptyList());

            when(authServiceMock.listUsersFromDesk(eq(tenantId), eq(deskId), anyInt(), anyInt()))
                    .thenReturn(emptyPaginatedList());

            when(authServiceMock.getServiceAccountUserId()).thenReturn("service-account-user-id");

            deskBusinessService.updateDeskUsers(request, tenantId, deskId);

            verify(authServiceMock, times(1)).addUsersToDesk(deskId, List.of("service-account-user-id"));
            verify(authServiceMock, times(1)).removeUsersFromDesk(deskId, emptyList());
        }


        @Test
        void addOne() {

            DeskDto request = new DeskDto();
            request.setOwnerIds(List.of("1"));

            when(authServiceMock.listUsersFromDesk(eq(tenantId), eq(deskId), anyInt(), anyInt()))
                    .thenReturn(emptyPaginatedList());

            when(authServiceMock.getServiceAccountUserId()).thenReturn("service-account-user-id");

            deskBusinessService.updateDeskUsers(request, tenantId, deskId);

            verify(authServiceMock, times(1)).addUsersToDesk(deskId, List.of("1", "service-account-user-id"));
            verify(authServiceMock, times(1)).removeUsersFromDesk(deskId, emptyList());
        }


        @Test
        void removeOne() {

            DeskDto request = new DeskDto();
            request.setOwnerIds(List.of("1"));

            when(authServiceMock.listUsersFromDesk(eq(tenantId), eq(deskId), anyInt(), anyInt()))
                    .thenReturn(PaginatedList.of(new User("1"), new User("2")));

            when(authServiceMock.getServiceAccountUserId()).thenReturn("service-account-user-id");

            deskBusinessService.updateDeskUsers(request, tenantId, deskId);

            verify(authServiceMock, times(1)).addUsersToDesk(deskId, List.of("service-account-user-id"));
            verify(authServiceMock, times(1)).removeUsersFromDesk(deskId, List.of("2"));
        }


        @Test
        void addAndRemove() {

            DeskDto request = new DeskDto();
            request.setOwnerIds(List.of("1", "2", "3", "4"));

            when(authServiceMock.listUsersFromDesk(eq(tenantId), eq(deskId), anyInt(), anyInt()))
                    .thenReturn(PaginatedList.of(new User("1"), new User("3"), new User("5")));

            when(authServiceMock.getServiceAccountUserId()).thenReturn("service-account-user-id");

            deskBusinessService.updateDeskUsers(request, tenantId, deskId);

            verify(authServiceMock, times(1)).addUsersToDesk(deskId, List.of("2", "4", "service-account-user-id"));
            verify(authServiceMock, times(1)).removeUsersFromDesk(deskId, List.of("5"));
        }


    }


    @Nested
    class UpdateAssociatedDesks {


        @Test
        void empty() {
            DeskDto request = new DeskDto();
            request.setAssociatedDeskIds(emptyList());

            when(permissionServiceMock.getAssociatedDeskIds(tenantId, deskId))
                    .thenReturn(emptyList());

            deskBusinessService.updateDeskAssociatedDesks(request, tenantId, deskId);

            verify(permissionServiceMock, times(1)).associateDesks(tenantId, deskId, emptyList());
            verify(permissionServiceMock, times(1)).removeAssociatedDesks(tenantId, deskId, emptyList());
        }


        @Test
        void addOne() {
            DeskDto request = new DeskDto();
            request.setAssociatedDeskIds(List.of("1"));

            when(permissionServiceMock.getAssociatedDeskIds(tenantId, deskId))
                    .thenReturn(emptyList());

            deskBusinessService.updateDeskAssociatedDesks(request, tenantId, deskId);

            verify(permissionServiceMock, times(1)).associateDesks(tenantId, deskId, List.of("1"));
            verify(permissionServiceMock, times(1)).removeAssociatedDesks(tenantId, deskId, emptyList());
        }


        @Test
        void removeOne() {
            DeskDto request = new DeskDto();
            request.setAssociatedDeskIds(List.of("1"));

            when(permissionServiceMock.getAssociatedDeskIds(tenantId, deskId))
                    .thenReturn(List.of("1", "2"));

            deskBusinessService.updateDeskAssociatedDesks(request, tenantId, deskId);

            verify(permissionServiceMock, times(1)).associateDesks(tenantId, deskId, emptyList());
            verify(permissionServiceMock, times(1)).removeAssociatedDesks(tenantId, deskId, List.of("2"));
        }


        @Test
        void addAndRemove() {
            DeskDto request = new DeskDto();
            request.setAssociatedDeskIds(List.of("1", "2", "3", "4"));

            when(permissionServiceMock.getAssociatedDeskIds(tenantId, deskId))
                    .thenReturn(List.of("1", "3", "5"));

            deskBusinessService.updateDeskAssociatedDesks(request, tenantId, deskId);

            verify(permissionServiceMock, times(1)).associateDesks(tenantId, deskId, List.of("2", "4"));
            verify(permissionServiceMock, times(1)).removeAssociatedDesks(tenantId, deskId, List.of("5"));
        }


    }


    @Nested
    class UpdateFilterableMetadata {


        @Test
        void empty() {
            DeskDto request = new DeskDto();
            request.setFilterableMetadataIds(emptyList());

            when(permissionServiceMock.getFilterableMetadataIds(tenantId, deskId))
                    .thenReturn(emptyList());

            deskBusinessService.updateDeskFilterableMetadata(request, tenantId, deskId);

            verify(permissionServiceMock, times(1)).addFilterableMetadata(tenantId, deskId, emptyList());
            verify(permissionServiceMock, times(1)).removeFilterableMetadata(tenantId, deskId, emptyList());
        }


        @Test
        void addOne() {
            DeskDto request = new DeskDto();
            request.setFilterableMetadataIds(List.of("1"));

            when(permissionServiceMock.getFilterableMetadataIds(tenantId, deskId))
                    .thenReturn(emptyList());

            deskBusinessService.updateDeskFilterableMetadata(request, tenantId, deskId);

            verify(permissionServiceMock, times(1)).addFilterableMetadata(tenantId, deskId, List.of("1"));
            verify(permissionServiceMock, times(1)).removeFilterableMetadata(tenantId, deskId, emptyList());
        }


        @Test
        void removeOne() {
            DeskDto request = new DeskDto();
            request.setFilterableMetadataIds(List.of("1"));

            when(permissionServiceMock.getFilterableMetadataIds(tenantId, deskId))
                    .thenReturn(List.of("1", "2"));

            deskBusinessService.updateDeskFilterableMetadata(request, tenantId, deskId);

            verify(permissionServiceMock, times(1)).addFilterableMetadata(tenantId, deskId, emptyList());
            verify(permissionServiceMock, times(1)).removeFilterableMetadata(tenantId, deskId, List.of("2"));
        }


        @Test
        void remove() {
            DeskDto request = new DeskDto();
            request.setFilterableMetadataIds(List.of("1", "2", "3", "4"));

            when(permissionServiceMock.getFilterableMetadataIds(tenantId, deskId))
                    .thenReturn(List.of("1", "3", "5"));

            deskBusinessService.updateDeskFilterableMetadata(request, tenantId, deskId);

            verify(permissionServiceMock, times(1)).addFilterableMetadata(tenantId, deskId, List.of("2", "4"));
            verify(permissionServiceMock, times(1)).removeFilterableMetadata(tenantId, deskId, List.of("5"));
        }

    }


    @Nested
    class UpdateSupervisors {


        @Test
        void empty() {
            DeskDto request = new DeskDto();
            request.setSupervisorIds(emptyList());

            when(permissionServiceMock.getSupervisorIdsFromDesk(tenantId, deskId))
                    .thenReturn(emptyList());

            deskBusinessService.updateDeskSupervisors(request, tenantId, deskId);

            verify(permissionServiceMock, times(1)).addSupervisorsToDesk(tenantId, deskId, emptyList());
            verify(permissionServiceMock, times(1)).removeSupervisorsFromDesk(tenantId, deskId, emptyList());
        }


        @Test
        void addOne() {
            DeskDto request = new DeskDto();
            request.setSupervisorIds(List.of("1"));

            when(permissionServiceMock.getSupervisorIdsFromDesk(tenantId, deskId))
                    .thenReturn(emptyList());

            deskBusinessService.updateDeskSupervisors(request, tenantId, deskId);

            verify(permissionServiceMock, times(1)).addSupervisorsToDesk(tenantId, deskId, List.of("1"));
            verify(permissionServiceMock, times(1)).removeSupervisorsFromDesk(tenantId, deskId, emptyList());
        }


        @Test
        void removeOne() {
            DeskDto request = new DeskDto();
            request.setSupervisorIds(List.of("1"));

            when(permissionServiceMock.getSupervisorIdsFromDesk(tenantId, deskId))
                    .thenReturn(List.of("1", "2"));

            deskBusinessService.updateDeskSupervisors(request, tenantId, deskId);

            verify(permissionServiceMock, times(1)).addSupervisorsToDesk(tenantId, deskId, emptyList());
            verify(permissionServiceMock, times(1)).removeSupervisorsFromDesk(tenantId, deskId, List.of("2"));
        }


        @Test
        void addAndRemove() {
            DeskDto request = new DeskDto();
            request.setSupervisorIds(List.of("1", "2", "3", "4"));

            when(permissionServiceMock.getSupervisorIdsFromDesk(tenantId, deskId))
                    .thenReturn(List.of("1", "3", "5"));

            deskBusinessService.updateDeskSupervisors(request, tenantId, deskId);

            verify(permissionServiceMock, times(1)).addSupervisorsToDesk(tenantId, deskId, List.of("2", "4"));
            verify(permissionServiceMock, times(1)).removeSupervisorsFromDesk(tenantId, deskId, List.of("5"));
        }


    }


    @Nested
    class UpdateDeskDelegations {


        @Test
        void empty() {
            DeskDto request = new DeskDto();
            request.setDelegationManagerIds(emptyList());

            when(permissionServiceMock.getDelegationManagerIdsFromDesk(tenantId, deskId))
                    .thenReturn(emptyList());

            deskBusinessService.updateDeskDelegations(request, tenantId, deskId);

            verify(permissionServiceMock, times(1)).addDelegationManagersToDesk(tenantId, deskId, emptyList());
            verify(permissionServiceMock, times(1)).removeDelegationManagersFromDesk(tenantId, deskId, emptyList());
        }


        @Test
        void addOne() {
            DeskDto request = new DeskDto();
            request.setDelegationManagerIds(List.of("1"));

            when(permissionServiceMock.getDelegationManagerIdsFromDesk(tenantId, deskId))
                    .thenReturn(emptyList());

            deskBusinessService.updateDeskDelegations(request, tenantId, deskId);

            verify(permissionServiceMock, times(1)).addDelegationManagersToDesk(tenantId, deskId, List.of("1"));
            verify(permissionServiceMock, times(1)).removeDelegationManagersFromDesk(tenantId, deskId, emptyList());
        }


        @Test
        void removeOne() {
            DeskDto request = new DeskDto();
            request.setDelegationManagerIds(List.of("1"));

            when(permissionServiceMock.getDelegationManagerIdsFromDesk(tenantId, deskId))
                    .thenReturn(List.of("1", "2"));

            deskBusinessService.updateDeskDelegations(request, tenantId, deskId);

            verify(permissionServiceMock, times(1)).addDelegationManagersToDesk(tenantId, deskId, emptyList());
            verify(permissionServiceMock, times(1)).removeDelegationManagersFromDesk(tenantId, deskId, List.of("2"));
        }


        @Test
        void addAndRemove() {
            DeskDto request = new DeskDto();
            request.setDelegationManagerIds(List.of("1", "2", "3", "4"));

            when(permissionServiceMock.getDelegationManagerIdsFromDesk(tenantId, deskId))
                    .thenReturn(List.of("1", "3", "5"));

            deskBusinessService.updateDeskDelegations(request, tenantId, deskId);

            verify(permissionServiceMock, times(1)).addDelegationManagersToDesk(tenantId, deskId, List.of("2", "4"));
            verify(permissionServiceMock, times(1)).removeDelegationManagersFromDesk(tenantId, deskId, List.of("5"));
        }


    }


    @Test
    void testCreateDeskWithSameInsensitiveCaseNameFail() {
        Tenant defaultTenant = Tenant.builder().id("tenant01").deskLimit(100).userLimit(100).build();
        String deskname = "Bureaux";
        String deskshortname = "Brx";

        Desk desk = new Desk();
        desk.setName(deskname);
        desk.setShortName(deskshortname);
        Mockito.when(authServiceMock.listDesks(any(), any(), any(), eq(false)))
                .thenReturn(new PageImpl<>(List.of(desk)));
        Mockito.when(authServiceMock.searchDesks(any(), any(), any(), any(), eq(false)))
                .thenReturn(new PageImpl<>(List.of(desk)));

        LocalizedStatusException exception = assertThrows(
                LocalizedStatusException.class,
                () -> deskBusinessService.createDeskIntegrityChecks(
                        new DeskDto() {{
                            setName(deskname.toUpperCase());
                            setShortName(deskshortname);
                        }},
                        defaultTenant
                )
        );

        String expectedMessage = getMessageForLocale("message.already_existing_desk_title", Locale.getDefault());
        assertEquals(
                HttpStatus.CONFLICT + " \"" + expectedMessage + "\"",
                exception.getLocalizedMessage()
        );
    }


    @Test
    void testCreateDeskWithSameInsensitiveCaseShortNameFail() {
        Tenant defaultTenant = Tenant.builder().id("tenant01").deskLimit(100).userLimit(100).build();
        String deskname = "Bureaux";
        String deskshortname = "Brx";

        Desk desk = new Desk();
        desk.setName(deskname);
        desk.setShortName(deskshortname);
        Mockito.when(authServiceMock.listDesks(any(), any(), any(), eq(false)))
                .thenReturn(new PageImpl<>(List.of(desk)));
        Mockito.when(authServiceMock.searchDesks(any(), any(), any(), any(), eq(false)))
                .thenReturn(new PageImpl<>(List.of(desk)));

        Mockito.when(authServiceMock.findDeskByShortName(any(), any()))
                .thenReturn(new PageImpl<>(List.of(new DeskRepresentation(UUID.randomUUID().toString(), deskname))));

        LocalizedStatusException exception = assertThrows(
                LocalizedStatusException.class,
                () -> this.deskBusinessService.createDeskIntegrityChecks(
                        new DeskDto() {{
                            setName(deskname + "1");
                            setShortName(deskshortname.toUpperCase());
                        }},
                        defaultTenant
                )
        );

        String expectedMessage = getMessageForLocale("message.already_existing_desk_short_name", Locale.getDefault());
        assertEquals(
                HttpStatus.CONFLICT + " \"" + expectedMessage + "\"",
                exception.getLocalizedMessage()
        );
    }


    @Test
    void testCreateDeskIntegrityChecksFailWhenTenantLimitReached() {

        Tenant dummyTenant = Tenant.builder().id(UUID.randomUUID().toString()).build();
        dummyTenant.setDeskLimit(1);

        Desk dummyDesk = new Desk();
        dummyDesk.setId(UUID.randomUUID().toString());
        dummyDesk.setTenantId(dummyTenant.getId());

        Mockito.when(authServiceMock.listDesks(any(), any(), any(), eq(false)))
                .thenReturn(new PageImpl<>(List.of(dummyDesk)));

        LocalizedStatusException exception = assertThrows(
                LocalizedStatusException.class,
                () -> this.deskBusinessService.createDeskIntegrityChecks(new DeskDto(), dummyTenant)
        );
        assertEquals(
                HttpStatus.INSUFFICIENT_STORAGE + " \""
                        + getMessageForLocale("message.already_n_desks_maximum_reached", Locale.getDefault(), dummyTenant.getDeskLimit())
                        + "\"",
                exception.getMessage()
        );
    }


}
