/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.business.signatureproof;

import coop.libriciel.ipcore.model.content.ValidatedSignatureInformation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;


class SignatureReportBusinessServiceTest {


    private Document embeddedDocument;
    private Document detachedDocument;
    private Map<String, String> errorsMap = new HashMap<>();
    private String xmlReportEmbeddedExample = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
            + "<ns2:DetailedReport xmlns:ns2=\"http://www.w3.org/2000/09/xmldsig#\">"
            + "    <errors>"
            + "        <signatureErrors signatureId=\"Signature2\">"
            + "            <error>INVALID_SIGNER_CERTIFICATE_PATH</error>"
            + "        </signatureErrors>"
            + "    </errors>"
            + "    <signatureReports>"
            + "        <signatureReport>"
            + "            <identifier>Signature1</identifier>"
            + "            <status>NO_ERROR_FOUND</status>"
            + "            <eIDASLevel>QESIG</eIDASLevel>"
            + "            <claimedDate>2023-11-13T08:33:43.000Z</claimedDate>"
            + "            <signerCertificate>"
            + "                <subjectDN>CN=PIERRE</subjectDN>"
            + "                <issuerDN>CN=CertEurope eID User</issuerDN>"
            + "                <notBefore>2023-09-21T13:52:00.000Z</notBefore>"
            + "                <notAfter>2026-09-21T13:52:00.000Z</notAfter>"
            + "            </signerCertificate>"
            + "        </signatureReport>"
            + "        <signatureReport>"
            + "            <identifier>Signature2</identifier>"
            + "            <status>ERROR_FOUND</status>"
            + "            <eIDASLevel>NA</eIDASLevel>"
            + "            <claimedDate>2024-06-13T07:36:57.000Z</claimedDate>"
            + "            <signerCertificate>"
            + "                <subjectDN>CN=[TEST] YOUSIGN cachet</subjectDN>"
            + "                <issuerDN>CN=YOUSIGN SAS - QUALIFIED SEAL2 CA</issuerDN>"
            + "                <notBefore>2023-10-02T07:18:11.000Z</notBefore>"
            + "                <notAfter>2026-10-02T07:18:11.000Z</notAfter>"
            + "            </signerCertificate>"
            + "        </signatureReport>"
            + "    </signatureReports>"
            + "</ns2:DetailedReport>";

    private String xmlReportDetachedExample = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
            + "<ns2:DetailedReport xmlns:ns2=\"http://www.w3.org/2000/09/xmldsig#\">"
            + "    <errors>"
            + "        <signatureErrors signatureId=\"Signature2\">"
            + "            <error>INVALID_SIGNER_CERTIFICATE_PATH</error>"
            + "        </signatureErrors>"
            + "    </errors>"
            + "    <signatureReports>"
            + "        <signatureReport>"
            + "            <identifier>Signature2</identifier>"
            + "            <status>ERROR_FOUND</status>"
            + "            <eIDASLevel>NA</eIDASLevel>"
            + "            <claimedDate>2024-06-13T07:36:57.000Z</claimedDate>"
            + "            <signerCertificate>"
            + "                <subjectDN>CN=[TEST] YOUSIGN cachet</subjectDN>"
            + "                <issuerDN>CN=YOUSIGN SAS - QUALIFIED SEAL2 CA</issuerDN>"
            + "                <notBefore>2023-10-02T07:18:11.000Z</notBefore>"
            + "                <notAfter>2026-10-02T07:18:11.000Z</notAfter>"
            + "            </signerCertificate>"
            + "        </signatureReport>"
            + "    </signatureReports>"
            + "</ns2:DetailedReport>";


    @BeforeEach
    void setUp() throws ParserConfigurationException, IOException, SAXException {

        byte[] xmlEmbeddedBytes = xmlReportEmbeddedExample.getBytes(StandardCharsets.UTF_8);
        byte[] xmlDetachedBytes = xmlReportDetachedExample.getBytes(StandardCharsets.UTF_8);
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

        try (ByteArrayInputStream embeddedInputStream = new ByteArrayInputStream(xmlEmbeddedBytes);
             ByteArrayInputStream detachedInputStream = new ByteArrayInputStream(xmlDetachedBytes)) {
            embeddedDocument = builder.parse(embeddedInputStream);
            detachedDocument = builder.parse(detachedInputStream);
        }

        errorsMap.put("Signature2", "INVALID_SIGNER_CERTIFICATE_PATH");
    }


    @Test
    void getErrorsInMap() {

        assertEquals(errorsMap, SignatureReportBusinessService.extractXmlErrors(embeddedDocument));
    }


    @Test
    void extractXmlDataAndUpdateSignatureInfosWithEmbeddedSignatures() {

        List<ValidatedSignatureInformation> expectedInfos = new ArrayList<>();
        long date = 1699864423;
        expectedInfos.add(new ValidatedSignatureInformation(date * 1000,
                null,
                "PIERRE",
                "CertEurope eID User",
                true,
                Instant.parse("2023-09-21T13:52:00.000Z").getEpochSecond() * 1000,
                Instant.parse("2026-09-21T13:52:00.000Z").getEpochSecond() * 1000,
                "QESIG",
                null
        ));

        long date2 = 1718264217;
        expectedInfos.add(new ValidatedSignatureInformation(date2 * 1000,
                null,
                "[TEST] YOUSIGN cachet",
                "YOUSIGN SAS - QUALIFIED SEAL2 CA",
                false,
                Instant.parse("2023-10-02T07:18:11.000Z").getEpochSecond() * 1000,
                Instant.parse("2026-10-02T07:18:11.000Z").getEpochSecond() * 1000,
                "NA",
                "INVALID_SIGNER_CERTIFICATE_PATH"
        ));

        ValidatedSignatureInformation signatureInfo = new ValidatedSignatureInformation();
        signatureInfo.setSignatureDateTime(date * 1000);
        ValidatedSignatureInformation signatureInfo2 = new ValidatedSignatureInformation();
        signatureInfo2.setSignatureDateTime(date2 * 1000);
        List<ValidatedSignatureInformation> signatureInfoList = List.of(signatureInfo, signatureInfo2);
        SignatureReportBusinessService.extractXmlDataAndUpdateSignatureInfos(embeddedDocument, errorsMap, signatureInfoList);

        assertEquals(expectedInfos, signatureInfoList);
    }


    @Test
    void extractXmlDataAndUpdateSignatureInfosWithdetachedSignature() {

        long date = 1718264217;
        ValidatedSignatureInformation expected = new ValidatedSignatureInformation(date * 1000,
                null,
                "[TEST] YOUSIGN cachet",
                "YOUSIGN SAS - QUALIFIED SEAL2 CA",
                false,
                Instant.parse("2023-10-02T07:18:11.000Z").getEpochSecond() * 1000,
                Instant.parse("2026-10-02T07:18:11.000Z").getEpochSecond() * 1000,
                "NA",
                "INVALID_SIGNER_CERTIFICATE_PATH"
        );

        ValidatedSignatureInformation signatureInfo = new ValidatedSignatureInformation();
        SignatureReportBusinessService.extractXmlDataAndUpdateSignatureInfos(detachedDocument, errorsMap, List.of(signatureInfo));

        assertEquals(expected, signatureInfo);
    }
}
