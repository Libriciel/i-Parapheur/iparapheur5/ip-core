/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.business.content;

import coop.libriciel.ipcore.services.content.AlfrescoService;
import coop.libriciel.ipcore.services.content.ContentServiceInterface;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;


class DocumentBusinessServiceTest {


    private DocumentBusinessService documentBusinessService;

    private @Mock ContentServiceInterface contentServiceMock = mock(AlfrescoService.class);
    private final ClassLoader classLoader = getClass().getClassLoader();


    @BeforeEach
    void setup() {
        documentBusinessService = new DocumentBusinessService(contentServiceMock);
    }


    @Test
    void mergeDocumentsByIds() throws IOException {

        int documentMerged = 30;
        List<InputStream> referencedInstances = new ArrayList<>();

        when(contentServiceMock.retrievePipedDocument(any()))
                .thenAnswer(invocation -> {
                    InputStream inputStream = classLoader.getResourceAsStream("pdf/signature_tag.pdf");
                    assertNotNull(inputStream);
                    referencedInstances.add(inputStream);
                    return inputStream;
                });

        List<String> randomIds = IntStream.range(0, documentMerged)
                .mapToObj(i -> UUID.randomUUID().toString())
                .toList();

        long start = System.currentTimeMillis();
        try (InputStream result = documentBusinessService.mergeDocumentsByIds(randomIds)) {
            long end = System.currentTimeMillis();
            System.out.println("mergeDocumentsByIds took " + (end - start) + "ms");

            assertEquals(208906, result.available());

            //noinspection resource
            verify(contentServiceMock, times(documentMerged)).retrievePipedDocument(any());

            // Any call to `available` should throw an error if the stream has been closed.
            // This should be make with some Mockito `spy` on the `close` method, but it does not work for some reason.
            // The streams are actually closed, though.
            referencedInstances.forEach(inputStream -> assertThrows(IOException.class, inputStream::available));
        }
    }


}
