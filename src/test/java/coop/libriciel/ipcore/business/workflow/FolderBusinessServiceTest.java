/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.business.workflow;

import com.itextpdf.html2pdf.ConverterProperties;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.services.stats.NoneStatsService;
import coop.libriciel.ipcore.services.workflow.IpWorkflowService;
import coop.libriciel.ipcore.services.workflow.WorkflowServiceInterface;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Locale;
import java.util.UUID;

import static coop.libriciel.ipcore.utils.LocalizedStatusException.getMessageForLocale;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.INSUFFICIENT_STORAGE;


@SpringBootTest
@ExtendWith(SpringExtension.class)
class FolderBusinessServiceTest {


    @Autowired
    private FolderBusinessService folderBusinessService;


    /**
     * For some reason, the default {@link ConverterProperties#setBaseUri} with the "classpath:/" value does not work on Unit-tests.
     * We have to manually fix it here.
     * TODO: Check if it is still needed, though...
     */
    @BeforeEach
    void setUp() {
        folderBusinessService.setHtmlToPdfConverterProperties(new ConverterProperties());
    }


    @Nested
    class CheckCreateFolderLimitations {


        private FolderBusinessService folderBusinessService;

        private @Mock WorkflowServiceInterface workflowServiceMock = mock(IpWorkflowService.class);


        @BeforeEach
        void setUp() {
            folderBusinessService = new FolderBusinessService(
                    null, null, null, null, null, null,
                    null, null, null, null, null, null, new NoneStatsService(), null,
                    null, null, null, workflowServiceMock
            );
        }


        @Test
        void success() {

            Tenant dummyTenant = new Tenant();
            dummyTenant.setId(UUID.randomUUID().toString());
            dummyTenant.setFolderLimit(1000);

            when(workflowServiceMock.listFolders(anyString(), eq(PageRequest.of(0, 1)), eq(null), eq(null), eq(null), eq(null), eq(null)))
                    .thenReturn(new PageImpl<>(new ArrayList<>(), PageRequest.of(0, 1), 10L));

            assertDoesNotThrow(() -> folderBusinessService.checkCreateFolderLimitations(dummyTenant));
        }


        @Test
        void limitReached() {

            Tenant dummyTenant = new Tenant();
            dummyTenant.setId(UUID.randomUUID().toString());
            dummyTenant.setFolderLimit(1000);

            when(workflowServiceMock.listFolders(anyString(), eq(PageRequest.of(0, 1)), eq(null), eq(null), eq(null), eq(null), eq(null)))
                    .thenReturn(new PageImpl<>(new ArrayList<>(), PageRequest.of(0, 1), 1000L));

            LocalizedStatusException exception = assertThrows(
                    LocalizedStatusException.class,
                    () -> folderBusinessService.checkCreateFolderLimitations(dummyTenant)
            );

            assertEquals(
                    "%s \"%s\"".formatted(
                            INSUFFICIENT_STORAGE,
                            getMessageForLocale("message.already_n_folders_maximum_reached", Locale.getDefault(), dummyTenant.getFolderLimit())
                    ),
                    exception.getMessage()
            );
        }


    }


    @Test
    void generateDocketPdfInputStream() throws IOException {

        Folder folder = new Folder();
        folder.setName("Test folder");

        try (InputStream dockerInputStream = folderBusinessService.getDefaultDocketResource().getInputStream();
             ByteArrayInputStream resultBais = folderBusinessService.generateDocketPdfInputStream(folder, dockerInputStream)) {

            assertNotNull(resultBais);
            assertTrue(resultBais.available() > 1000L); // CI and local tests produce slightly different sizes

            String docketPdfFilePath = "build/test-results/intTest_" + getClass().getSimpleName() + "_generateDocketPdfInputStream.pdf";
            IOUtils.copy(resultBais, new FileOutputStream(docketPdfFilePath));
        }
    }


}
