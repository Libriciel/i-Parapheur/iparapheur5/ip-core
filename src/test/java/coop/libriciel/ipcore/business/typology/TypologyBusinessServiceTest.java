/*
 * iparapheur Core
 * Copyright (C) 2018-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package coop.libriciel.ipcore.business.typology;

import coop.libriciel.ipcore.model.database.Subtype;
import coop.libriciel.ipcore.model.database.Tenant;
import coop.libriciel.ipcore.model.database.Type;
import coop.libriciel.ipcore.model.workflow.Folder;
import coop.libriciel.ipcore.services.database.SubtypeRepository;
import coop.libriciel.ipcore.services.database.TenantRepository;
import coop.libriciel.ipcore.services.database.TypeRepository;
import coop.libriciel.ipcore.utils.LocalizedStatusException;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
@ExtendWith(SpringExtension.class)
class TypologyBusinessServiceTest {


    private static final String EXAMPLE_TENANT_ID = "tenant_01";


    private @Autowired TypologyBusinessService typologyBusinessService;
    private @Autowired TypeRepository typeRepository;
    private @Autowired TenantRepository tenantRepository;
    private @Autowired SubtypeRepository subtypeRepository;


    @BeforeEach
    void setUp() {

        Tenant tenant = Tenant.builder().id(EXAMPLE_TENANT_ID).build();
        tenantRepository.save(tenant);

        Type type01 = Type.builder().tenant(tenant).id("type01").build();
        Type type02 = Type.builder().tenant(tenant).id("type02").build();
        typeRepository.save(type01);
        typeRepository.save(type02);

        subtypeRepository.save(Subtype.builder().tenant(tenant).id("subtype01").parentType(type01).build());
    }


    @AfterEach
    void cleanup() {
        typeRepository.deleteAll();
        subtypeRepository.deleteAll();
        tenantRepository.deleteAll();
    }


    @Nested
    class updateTypology {


        @Test
        @Transactional
        void integrityCheck() {

            Folder folder = new Folder();
            List<Folder> folderSingleton = singletonList(folder);

            folder.setType(null);
            folder.setSubtype(null);
            assertDoesNotThrow(() -> typologyBusinessService.updateTypology(EXAMPLE_TENANT_ID, folderSingleton, true));
            assertDoesNotThrow(() -> typologyBusinessService.updateTypology(EXAMPLE_TENANT_ID, folderSingleton, false));

            folder.setType(new Type("non-existing-id"));
            folder.setSubtype(null);
            assertThrows(LocalizedStatusException.class, () -> typologyBusinessService.updateTypology(EXAMPLE_TENANT_ID, folderSingleton, true));
            assertDoesNotThrow(() -> typologyBusinessService.updateTypology(EXAMPLE_TENANT_ID, folderSingleton, false));

            folder.setType(null);
            folder.setSubtype(new Subtype("non-existing-id"));
            assertThrows(LocalizedStatusException.class, () -> typologyBusinessService.updateTypology(EXAMPLE_TENANT_ID, folderSingleton, true));
            assertDoesNotThrow(() -> typologyBusinessService.updateTypology(EXAMPLE_TENANT_ID, folderSingleton, false));

            folder.setType(new Type("type01"));
            folder.setSubtype(new Subtype("non-existing-id"));
            assertThrows(LocalizedStatusException.class, () -> typologyBusinessService.updateTypology(EXAMPLE_TENANT_ID, folderSingleton, true));
            assertDoesNotThrow(() -> typologyBusinessService.updateTypology(EXAMPLE_TENANT_ID, folderSingleton, false));

            folder.setType(new Type("non-existing-id"));
            folder.setSubtype(new Subtype("subtype01"));
            assertThrows(LocalizedStatusException.class, () -> typologyBusinessService.updateTypology(EXAMPLE_TENANT_ID, folderSingleton, true));
            assertDoesNotThrow(() -> typologyBusinessService.updateTypology(EXAMPLE_TENANT_ID, folderSingleton, false));

            folder.setType(new Type("type01"));
            folder.setSubtype(new Subtype("subtype01"));
            assertDoesNotThrow(() -> typologyBusinessService.updateTypology(EXAMPLE_TENANT_ID, folderSingleton, true));
            assertDoesNotThrow(() -> typologyBusinessService.updateTypology(EXAMPLE_TENANT_ID, folderSingleton, false));
        }


    }


}
