#!/bin/ash

#
# iparapheur Core
# Copyright (C) 2018-2025 Libriciel-SCOP
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-only
#

echo "running ip-core entrypoint script"

if [[ -n "$*" ]]; then
  echo "running additional commands : $@"
  /bin/bash -c "$*"
fi

echo "running main command"

java -XX:+UseContainerSupport -XX:MaxRAMPercentage=75.0 \
     -Djavax.net.ssl.trustStore=/truststore.jks -Djavax.net.ssl.trustStoreType=jks -Djavax.net.ssl.trustStorePassword=${TRUSTSTORE_PASSWORD:?} \
     $JAVA_OPTS \
     -Djava.security.egd=file:/dev/./urandom \
     -jar /core.jar
