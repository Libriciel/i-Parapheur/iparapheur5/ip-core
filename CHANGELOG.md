Change Log
==========

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres
to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]
[Unreleased]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core


## [1.10.6] - 2024-01-08
[1.10.6]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/-/tags/1.10.6
### Changed
- Auto-placement regex on legacy selection script


## [0.10.8] - 2022-05-09
[0.10.8]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/-/tags/0.10.8
### Changed
- Raise buffer size when calling Keycloak's permission API


## [0.10.7] - 2022-05-09
[0.10.7]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/-/tags/0.10.7
### Added
- Refused external signature trigger rejection of the folder
### Improved
- Print folder is now less likely to induce deadlock of webclient (implying 504 errors on most requests)


## [0.5.0] - 2022-02-04
[0.5.0]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/-/tags/0.5.0
### Added
- Workflow Selection Script maximum size.
- signature and seal tags detection for stamp position
### Fixed
- External Signature configurations are now requested with the tenant.
- Premis file can now be created without detached signatures
- Simple delegations and time-based delegations


## [0.4.5] - 2022-01-20
[0.4.5]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/-/tags/0.4.5
### Fixed
- Sentry
### Changed
- Swagger2 to OAS3


## [0.4.4] - 2022-01-12
[0.4.4]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/-/tags/0.4.4
### Fixed
- Make the max number of main document a global property fixed in conf file
- Fix one subtype API parameter


## [0.4.3] - 2021-12-29
[0.4.3]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/-/tags/0.4.3
### Added
- Changelog
- Readme
- Dockerfile
- New content service : Alfresco
- New workflow service : IP-Workflow
- New authentication service : Keycloak
- New crypto service : IP-Crypto
- New stats service : External Matomo
- New stats service : Internal Matomo
- New stats service : Generic HTTP link
- Swagger 2
