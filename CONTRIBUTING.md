Contributing
============

## Importing project

### Setup in IntelliJ:

- Preferences → Plugins...
- Search and install `Lombok`
- File → Open...
- Open the `build.gradle` file
- Enable Auto-import
- Go to Preferences → Build, Execution, Deployment → Compiler → Annotation Processors
- Check Enable annotation processing

### Code style and project structure

The `.idea` folder has been committed. It should bring the appropriate code-style.  
Obviously, every modified file should be auto-formatted before any git push.  
No hook would reject a mis-formatted file for now... But if everything goes bananas, we may set it up.

## Unit tests

TODO

## DB patch

Liquibase has been chosen to patch the DB schema on-the-fly.

* Production instance should be in SpringBoot DB-schema validate only
* Dev instances can conveniently be in DB-schema update

**Troubleshoot**:  
The version of liquibase provided as snap under ubuntu 20.04 does not contain the required postgresql drivers...
One has to retrieve the latest postgresql driver's jar () and reference is path in liquibase.properties by adding this line:

```
classpath=<absolute_jar_path>/postgresql-X.Y.Z.jar
```

Note: It is possible that the package provided directly on liquibase's site already bundles said driver, avoiding the hassle.

### Generating a reference snapshot

First thing first, we have to make a new reference snapshot, that will be used for the diff.  
(Maybe we should automate it in some kind of CI task on tag...)

```bash
liquibase --outputFile=db-snapshots/upcoming.ipcore.version.json snapshot
```

With the version of liquibase provided as snap under ubuntu 20.04:
```bash
liquibase --outputFile=db-snapshots/upcoming.ipcore.version.json snapshot --snapshotFormat=json
```

### Generating a Liquibase-changelog patch

Using the brand new reference, we can generate the diff from a old one.

```
liquibase \
      --url=offline:postgres\?snapshot=db-snapshots/previous.ipcore.version.json \
      --referenceUrl=offline:postgres\?snapshot=db-snapshots/upcoming.ipcore.version.json \
      diff-changelog 
```

(Pro-tip for testing: A `diff` instead of `diffChangeLog` command will just print a "shorter" log)

The `db.changelog-temp.yaml` changelog generated will be kind of pretty sloppy, we can clean it a little:

* Checking that there isn't an suspicious amount of drop table.
* Checking that there isn't any automatically generated names.
* Merging every changes in a single array.
  This can be done automatically by just running the script `changelog-cruncher.py`,
  which will generate a file named `db.changelog-crunched.yaml` with all the changes merged in one changeset
* Setting the main author to a more appropriate `Libriciel-SCOP`.
* Setting the `changeset` id to a properly human-readable `x.x.x` one.

Then append it to the existing `src/main/resources/db/changelog/db.changelog-master.yaml` file.  
Warning: This is a reference file. Already existing patches should never be modified.  
Liquibase performs some md5 checksums in those, it will trigger errors.  
Always go for an incremental update.

Once the reference file is packaged in a Jar, the patch will be deployed in any other production server.  
Commit the reference snapshot, get rid of the temporary Liquibase changelog.

## Generated libraries

See the dedicated documentation for the generation:  
https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/Documentations/techniques/ip-doc-api-rest#librairie-cliente

On every tags, libraries should be tagged as the `ip-core v1.x.x`, that will be used in the web client.  
On every iparapheur v5.X release, the exact same code generation shall be tagged as a `iparapheur v5.x.x`, that will be used by external sources.

Every library shall be tagged with a MIT license.

### Premis library

The Premis 3.0 XSD was used to generate a library.

That was a one-in-a-lifetime compilation, we won't make any project versioning on it.
To re-create the lib we can generate the classes :

```bash
xjc premis3.0.xsd
```

Create an empty Gradle project, paste the generated java classes in it,  
and update the `build.gradle` with this configuration:

```groovy

group = 'gov.loc.premis.v3'
version = '3.0.n'

// ...

jar {
    from sourceSets.main.allSource
}

dependencies {

    // ...
    implementation group: 'com.fasterxml.jackson.module', name: 'jackson-module-jaxb-annotations', version: '2.15.2'
    // ...

}
```

Then a few commands to upload the jar:  
(The `libriciel-nexus-releases` is a login parameter that can be copy/paste from the `mvn-deploy-to-nexus` shared CI job)

```bash
gradle build
cd ./build/libs/
mvn deploy:deploy-file \
    -DgroupId=gov.loc \        
    -DartifactId=premis \        
    -Dversion=3.0.n \    
    -Dpackaging=jar \        
    -Dfile=premis-lib-3.0.n.jar \        
    -DrepositoryId=libriciel-nexus-releases \        
    -Durl=https://nexus.libriciel.fr/repository/maven-releases
```

### OpenAPI references

Since we don't want modifications on the standard library, a reference file is set on the test resources.  
Any delta between this file and the current API one will be considered as an error in a dedicated CI job.

The OpenApi plugin generates some randomly sorted JSON files, it has to be sort to ease the Git diff evaluation.

To update the reference file, this commands should do :
```bash
rm build/docs/ip-core-openapi-*.json
./gradlew clean generateOpenApiDocs
jq --sort-keys . build/docs/ip-core-openapi-standard.json > src/test/resources/ip-core-openapi-standard.json
jq --sort-keys . build/docs/ip-core-openapi-provisioning.json > src/test/resources/ip-core-openapi-provisioning.json
```

### TypeScript/Angular

#### Preparation

```bash
echo "//registry.npmjs.org/:_authToken=${NPMJS_TOKEN}" > ~/.npmrc
```

#### Deploy

Versioning should follow the common NPM rules:
- `1.2.3` on tags
- `0.0.0-YYYYMMDDHHmmss-SNAPSHOT` on develop/branches versions.

Generate, following the documentation, and publish the actual lib on the official NPM.js public repository:

```bash
npm publish --access=public dist
```

### Java

#### Preparation

The `~/.m2/settings.xml` file should declare some login variables:
```xml

<settings>
    <servers>
        <server>
            <id>libriciel-nexus-releases</id>
            <username>MAVEN_REPO_LOGIN</username>
            <password>MAVEN_REPO_PASS</password>
        </server>
        <server>
            <id>libriciel-nexus-snapshots</id>
            <username>MAVEN_REPO_LOGIN</username>
            <password>MAVEN_REPO_PASS</password>
        </server>
    </servers>
</settings>

```

#### Deploy

Versioning should follow the common Nexus rules:

- `1.2.3` on tags
- `0.0.0-SNAPSHOT` on develop/branches versions

Generate, following the documentation, and publish the actual lib on the internal Nexus repository:

```bash
mvn source:jar \
    javadoc:jar \
    -Dmaven.test.skip=true \
    -DaltDeploymentRepository=libriciel-nexus-snapshots::default::https://nexus.libriciel.fr/repository/maven-snapshots/ \
    -Drelease=false \
    deploy
```
